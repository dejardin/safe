#define EXTERN extern
#include "gdaq_SAFE.h"

void linearity_Monacal()
{
  Double_t tref=2150.;
  Double_t fit_duration=200;
  Int_t G12=1, color=kBlue;
  if(asic.DTU_force_G1[daq.channel_number[0]])G12=0;

  char gname[132],pname[132];
  double dv=1200./4096.;
  double dv_inj=1115./4096.;
  double Cinj=20.; // in pF
  double dac_INL=0.1; 
  double Vmax=1200.;
  double Vref=1200.;
  Double_t gain[N_CHANNEL];

  printf("Opening file %s for analysis\n",daq.last_fname);
  TFile *infile=new TFile(daq.last_fname);
  Int_t loc_gain, step, dac_val, nstep;
// P_data_G10_55_-5_3450_230726_111435.root
  sscanf(daq.last_fname,"data/TP_data_G%d_%d_%d_%d",&loc_gain,&nstep,&step,&dac_val);
  printf("Gain found : %d, start %d, step %d, nstep %d\n",loc_gain,dac_val,step,nstep);
  if(loc_gain==1){G12=0; color=kRed;}
  gettimeofday(&tv,NULL);

// Retreive data :
  TH1D *hstep[N_CHANNEL][100];
  Int_t nbin;
  Double_t dt;

  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    for(int istep=0; istep<nstep; istep++)
    {
      TProfile *tp;
      sprintf(pname,"ch_%d_step_%d_%d",ich,istep,dac_val+step*istep);
      gDirectory->GetObject(pname,tp);
      nbin=tp->GetNbinsX();
      dt=tp->GetBinWidth(1);
      sprintf(pname,"hch_%d_step_%d_%d",ich,istep,dac_val+step*istep);
      hstep[ich][istep]=new TH1D(pname,pname,nbin,0.,nbin*dt);
      for(Int_t j=0; j<nbin; j++)
      {
        Double_t y2;
        y2=tp->GetBinContent(j+1);
        hstep[ich][istep]->SetBinContent(j+1,y2+0.001);
      }
      Double_t loc_max=tp->GetMaximum();
      if(loc_max<3000.)tref=tp->GetBinCenter(tp->GetMaximumBin());
    }
  }
    
  TF1 *f0;
  TGraphErrors *tg1[N_CHANNEL], *tg2[N_CHANNEL], *tg3[N_CHANNEL], *tg4[N_CHANNEL];
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    tg1[ich]=new TGraphErrors();
    tg2[ich]=new TGraphErrors();
    tg3[ich]=new TGraphErrors();
    tg4[ich]=new TGraphErrors();
    sprintf(gname,"ch_%d_gain_curve",ich);
    tg1[ich]->SetName(gname);
    sprintf(gname,"ch_%d_residual",ich);
    tg2[ich]->SetName(gname);
    sprintf(gname,"ch_%d_INL",ich);
    tg3[ich]->SetName(gname);
    sprintf(gname,"ch_%d_gain_error",ich);
    tg4[ich]->SetName(gname);
  }
  
  TCanvas *tcc,*tc0,*tc1,*tc2,*tc3,*tc4;
  tcc=new TCanvas("temp","temp");
  tc4=new TCanvas("cc4","cc4");
  tc0=new TCanvas("cc0","cc0");
  tc1=new TCanvas("cc1","cc1");
  tc2=new TCanvas("cc2","cc2");
  tc3=new TCanvas("cc3","cc3");
  tcc->SetTitle("temp canvas");
  tc0->SetTitle("Current injection pulse response");
  tc1->SetTitle("Amplitude vs I_inj");
  tc2->SetTitle("Residual vs amplitude");
  tc3->SetTitle("INL vs amplitude");
  tc4->SetTitle("Gain error vs amplitude");
  gStyle->SetOptStat(0);
  tcc->Update();
  tc0->Update();
  tc1->Update();
  tc2->Update();
  tc3->Update();
  tc4->Update();

  
  TF1 *fmax=new TF1("fmax","[1]*(x-[0])*(x-[0])+[2]",0.,100000.);
  TF1 *f1=new TF1("f1","[0]+[1]*x",-1000.,25000.);
  TF1 *f2=new TF1("f2","[0]+[1]*x+[2]*x*x",10.,1050.);
  TF1 *f3=new TF1("f3","[0]+x*([1]+x*([2]+x*[3]))",10.,1050.);
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Double_t x0,x1,x2,y1,y2;
    Int_t ich=daq.channel_number[i]-1;
    for(int istep=0; istep<nstep; istep++)
    {
      TH1D *tp=hstep[ich][istep];
      sprintf(pname,"ch_%d_step_%d_%d",ich,istep,dac_val+step*istep);
      //printf("Search for histogram %s\n",pname);
      //gDirectory->GetObject(pname,tp);
      tp->SetTitle("Monacal : Charge injection pulse response");
      tp->SetMaximum(4096);
      tp->SetMinimum(0.);
      tp->SetMarkerStyle(20);
      tp->SetMarkerColor(color);
      tp->SetMarkerSize(0.3);
      tp->SetLineColor(color);

      tcc->cd();
      tp->Fit("pol0","Q","",0.,400.);
      f0=tp->GetFunction("pol0");
      double ped=f0->GetParameter(0);
      tp->Fit("pol0","Q","",tref-fit_duration/2.,tref+fit_duration/2.);
      f0=tp->GetFunction("pol0");
      Double_t qmax=f0->GetParameter(0);
      fmax->SetParameter(0,tref);
      fmax->SetParameter(1,0.);
      fmax->SetParameter(2,qmax);
      tp->Fit(fmax,"Q","",tref-fit_duration/2.,tref+fit_duration/2.);
      qmax=fmax->GetParameter(2);
      double val=(qmax-ped)*dv;
      double inj=fabs(dv_inj*step)*istep*Cinj; // Injection current in fC
      tg1[ich]->SetPoint(istep,inj,val);
      if(istep==0){x1=inj,y1=val;}
      if(val<0.){x1=inj,y1=val;}
      if(val<3000.){x2=inj,y2=val;}
      printf("step %d : DAC %d, Inj %e, ped %e, qmax %e, val %e\n",istep,dac_val+step*istep,inj,ped,qmax,val);
      tcc->Update();

      tc0->cd();
      tc0->Update();
      if(i==0 && istep==0)
      {
        tp->DrawClone();
        gPad->SetGridx();
        gPad->SetGridy();
        tp->GetXaxis()->SetTitle("time [ns]");
        tp->GetYaxis()->SetTitle("amplitude [mV]");
      }
      else if((istep%4)==0)
      {
        tp->DrawClone("same");
      }
      tc0->Update();
      //return;
    }
    gain[ich]=(y2-y1)/(x2-x1);
    x0=(x1*y2-x2*y1)/(y2-y1);
    
    for(int istep=0; istep<nstep; istep++)
    {
      double x,y;
      tg1[ich]->GetPoint(istep,x,y);
      tg1[ich]->SetPoint(istep,x-x0,y);
    }

    tc1->cd();
    tc1->Update();
    tg1[ich]->SetMarkerStyle(20);
    tg1[ich]->SetMarkerSize(1.);
    tg1[ich]->SetMarkerColor(color);
    tg1[ich]->SetLineColor(color);
    tg1[ich]->SetMaximum(Vmax);
    tg1[ich]->SetMinimum(-50.);
    if(i==0)
    {
      tg1[ich]->Draw("alp");
      tg1[ich]->GetXaxis()->SetTitle("Injected charge [#fC]");
      tg1[ich]->GetXaxis()->SetLabelFont(62);
      tg1[ich]->GetXaxis()->SetLabelSize(0.05);
      tg1[ich]->GetXaxis()->SetTitleFont(62);
      tg1[ich]->GetXaxis()->SetTitleSize(0.05);
      tg1[ich]->GetYaxis()->SetTitle("Monacal amplitude [mV]");
      tg1[ich]->GetYaxis()->SetLabelFont(62);
      tg1[ich]->GetYaxis()->SetLabelSize(0.05);
      tg1[ich]->GetYaxis()->SetTitleFont(62);
      tg1[ich]->GetYaxis()->SetTitleSize(0.05);
      tg1[ich]->GetYaxis()->SetTitleOffset(1.00);
      if(G12==1)
        tg1[ich]->GetXaxis()->SetLimits(-50.,1600.);
      else
        tg1[ich]->GetXaxis()->SetLimits(-50.,20000.);
      tg1[ich]->DrawClone("alp");
    }
    else
      tg1[ich]->DrawClone("lp");
    gPad->SetGridx();
    gPad->SetGridy();
    tc1->Update();

    tcc->cd();
    tcc->Update();
    f1->SetParameter(1,gain[ich]);
    if(G12==1)
      //tg1[ich]->Fit(f1,"Q","",-25.,230.);
      tg1[ich]->Fit(f1,"Q","",50.,1050.);
    else
      tg1[ich]->Fit(f1,"Q","",100.,14000.);
    gain[ich]=f1->GetParameter(1);
  // Possible error from DAC
    double e_inj=dv*dac_INL*Cinj;
    printf("Monacal gain : %e mV/fC, DAC error on injected charge : %e fC -> %e mV \n",gain[ich],e_inj,gain[ich]*e_inj);
    tcc->Update();
    tc2->cd();
    tc2->Update();
    for(int istep=0; istep<nstep; istep++)
    {
      double x,y;
      tg1[ich]->GetPoint(istep,x,y);
      tg1[ich]->SetPointError(istep,e_inj,e_inj*gain[ich]);
      double resi=y-f1->Eval(x);
      tg2[ich]->SetPoint(istep,y,resi);
      tg2[ich]->SetPointError(istep,e_inj*gain[ich]/100.,e_inj*gain[ich]);
    }
    tg2[ich]->SetMarkerStyle(20);
    tg2[ich]->SetMarkerSize(1.);
    tg2[ich]->SetMarkerColor(color);
    tg2[ich]->SetLineColor(color);
    if(G12==1)
    {
      tg2[ich]->SetMaximum(5.);
      tg2[ich]->SetMinimum(-5.);
    }
    else
    {
      tg2[ich]->SetMaximum(5.);
      tg2[ich]->SetMinimum(-5.);
    }
    tg2[ich]->GetXaxis()->SetLimits(-50.,Vmax);
    tg2[ich]->GetYaxis()->SetTitle("Linearity residual [mV]");
    tg2[ich]->GetYaxis()->SetLabelFont(62);
    tg2[ich]->GetYaxis()->SetLabelSize(0.05);
    tg2[ich]->GetYaxis()->SetTitleFont(62);
    tg2[ich]->GetYaxis()->SetTitleSize(0.05);
    tg2[ich]->GetYaxis()->SetTitleOffset(1.00);
    tg2[ich]->GetXaxis()->SetTitle("Monacal amplitude [mV]");
    tg2[ich]->GetXaxis()->SetLabelFont(62);
    tg2[ich]->GetXaxis()->SetLabelSize(0.05);
    tg2[ich]->GetXaxis()->SetTitleFont(62);
    tg2[ich]->GetXaxis()->SetTitleSize(0.05);
    if(i==0)
      tg2[ich]->DrawClone("alp");
    else
      tg2[ich]->DrawClone("lp");
    gPad->SetGridx();
    gPad->SetGridy();
    tc2->Update();
    
    for(int istep=0; istep<nstep; istep++)
    {
      double x,y, ex,ey;
      tg2[ich]->GetPoint(istep,x,y);
      ex=tg2[ich]->GetErrorX(istep);
      ey=tg2[ich]->GetErrorY(istep);
      tg3[ich]->SetPoint(istep,x,y/Vref*100.);
      tg3[ich]->SetPointError(istep,ex,ey/Vref*100.);
      tg4[ich]->SetPoint(istep,x,y/x*100.);
      tg4[ich]->SetPointError(istep,ex,ey/x*100.);
    }
    tc3->cd();
    tc3->Update();
    tg3[ich]->SetMarkerStyle(20);
    tg3[ich]->SetMarkerSize(1.);
    tg3[ich]->SetMarkerColor(color);
    tg3[ich]->SetLineColor(color);
    if(G12==1)
    {
      tg3[ich]->SetMaximum(1.0);
      tg3[ich]->SetMinimum(-1.0);
    }
    else
    {
      tg3[ich]->SetMaximum(0.5);
      tg3[ich]->SetMinimum(-0.5);
    }
    tg3[ich]->SetTitle("INL vs amplitude");
    tg3[ich]->GetXaxis()->SetLimits(-50.,Vmax);
    tg3[ich]->GetYaxis()->SetTitle("INL full scale [%]");
    tg3[ich]->GetYaxis()->SetLabelFont(62);
    tg3[ich]->GetYaxis()->SetLabelSize(0.05);
    tg3[ich]->GetYaxis()->SetTitleFont(62);
    tg3[ich]->GetYaxis()->SetTitleSize(0.05);
    tg3[ich]->GetYaxis()->SetTitleOffset(1.00);
    tg3[ich]->GetXaxis()->SetTitle("Monacal amplitude [mV]");
    tg3[ich]->GetXaxis()->SetLabelFont(62);
    tg3[ich]->GetXaxis()->SetLabelSize(0.05);
    tg3[ich]->GetXaxis()->SetTitleFont(62);
    tg3[ich]->GetXaxis()->SetTitleSize(0.05);
    f2->SetParameter(0,0.);
    f2->SetParameter(1,0.);
    f2->SetParameter(2,0.);
    f3->SetParameter(0,0.);
    f3->SetParameter(1,0.);
    f3->SetParameter(2,0.);
    f3->SetParameter(3,0.);
    TF1 *ffit=f3;
    if(G12)ffit=f2;
    tg3[ich]->Fit(ffit,"","",10.,1050.);
    Double_t INL_max=ffit->GetMaximum();
    Double_t INL_min=ffit->GetMinimum();
    asic.INL[1-G12][ich]=(INL_max-INL_min)/2.;
    if(i==0)
      tg3[ich]->DrawClone("alp");
    else
      tg3[ich]->DrawClone("lp");
    gPad->SetGridx();
    gPad->SetGridy();
    tc3->Update();

    tc4->cd();
    tc4->Update();
    tg4[ich]->SetMarkerStyle(20);
    tg4[ich]->SetMarkerSize(1.);
    tg4[ich]->SetMarkerColor(color);
    tg4[ich]->SetLineColor(color);
    if(G12==1)
    {
      tg4[ich]->SetMaximum(5.0);
      tg4[ich]->SetMinimum(-5.0);
    }
    else
    {
      tg4[ich]->SetMaximum(5.0);
      tg4[ich]->SetMinimum(-5.0);
    }
    tg4[ich]->GetXaxis()->SetLimits(-50.,Vmax);
    tg4[ich]->GetYaxis()->SetTitle("Gain error [%]");
    tg4[ich]->GetYaxis()->SetLabelFont(62);
    tg4[ich]->GetYaxis()->SetLabelSize(0.05);
    tg4[ich]->GetYaxis()->SetTitleFont(62);
    tg4[ich]->GetYaxis()->SetTitleSize(0.05);
    tg4[ich]->GetYaxis()->SetTitleOffset(1.00);
    tg4[ich]->GetXaxis()->SetTitle("Monacal amplitude [mV]");
    tg4[ich]->GetXaxis()->SetLabelFont(62);
    tg4[ich]->GetXaxis()->SetLabelSize(0.05);
    tg4[ich]->GetXaxis()->SetTitleFont(62);
    tg4[ich]->GetXaxis()->SetTitleSize(0.05);
    if(i==0)
      tg4[ich]->DrawClone("alp");
    else
      tg4[ich]->DrawClone("lp");
    gPad->SetGridx();
    gPad->SetGridy();
    tc4->Update();
  }

  sprintf(pname,"%s",daq.last_fname);
  char *pos=strstr(pname,".root");
  sprintf(pos,"_analized.root");
  printf("Output file : %s\n",pname);
  TFile *tfout=new TFile(pname,"recreate");
  tcc->Write();
  tc0->Write();
  tc1->Write();
  tc2->Write();
  tc3->Write();
  tc4->Write();
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    tg1[ich]->Write();
    tg2[ich]->Write();
    tg3[ich]->Write();
    tg4[ich]->Write();
  }
  tfout->Close();
  tfout->~TFile();
  sprintf(pos,"_analized.txt");
  FILE *fout=fopen(pname,"w+");
  fprintf(fout,"%ld.%6.6ld : %s\n",tv.tv_sec,tv.tv_usec,daq.comment);

  printf("Gains : ");
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    printf("%e, ",gain[ich]);
    fprintf(fout,"%e, ",gain[ich]);
  }
  printf("\n");
  fprintf(fout,"\n");
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    printf("%e, ",asic.INL[1-G12][ich]);
    fprintf(fout,"%e, ",asic.INL[1-G12][ich]);
  }
  printf("\n");
  fprintf(fout,"\n");
  for(int istep=0; istep<nstep; istep++)
  {
    for(int i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i]-1;
      double x,y;
      tg1[ich]->GetPoint(istep,x,y);
      //if(ich==0)fprintf(fout,"%e",x);
      fprintf(fout,"%e %e ",x,y);
    }
    fprintf(fout,"\n");
  }
  fclose(fout);

  usleep(1000000);
  //printf("OK pour passer a la suite ? ");
  //system("stty raw");
  //char cdum=getchar();
  //system("stty -raw");
  //printf("\n ");

  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    tg1[ich]->~TGraph();
    tg2[ich]->~TGraph();
    tg3[ich]->~TGraph();
    tg4[ich]->~TGraph();
    for(int istep=0; istep<nstep; istep++)
    {
      hstep[ich][istep]->~TH1D();
    }
  }
  tcc->~TCanvas();
  tc0->~TCanvas();
  tc1->~TCanvas();
  tc2->~TCanvas();
  tc3->~TCanvas();
  tc4->~TCanvas();
  f1->~TF1();
  f2->~TF1();
  f3->~TF1();
  fmax->~TF1();
  infile->Close();
  infile->~TFile();
}
