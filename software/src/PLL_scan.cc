#define EXTERN extern
#include "gdaq_SAFE.h"

void PLL_scan(void)
{
  uhal::HwInterface hw=devices.front();

  ValWord<uint32_t> address, reg;
  UInt_t command;
  ValWord<uint32_t> free_mem;
  GtkWidget *widget;
  char widget_name[80], widget_content[80], reg_name[80];

  UInt_t sync_pattern=0;

  Int_t pll_conf;
  Int_t pll_conf1_min=0;
  Int_t pll_conf1_max=0x3f;
  Int_t pll_conf2_min=0;
  Int_t pll_conf2_max=0x7;

// Set which VFEs to scan :
  printf("Do PLL scan for VFEs : 0x%x\n",daq.VFE_pattern);
  hw.getNode("VFE_PATTERN").write(daq.VFE_pattern);
  hw.dispatch();

// First put PLLs in force mode for V2.0 :
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    asic.DTU_force_PLL[ich]=TRUE;
    update_LiTEDTU_reg(ich,17,3);
  }

  reg = hw.getNode("DTU_SYNC_PATTERN").read();
  hw.dispatch();
  printf("Expected Sync patterns : 0x%8.8x, test mode %d\n",reg.value(),daq.DTU_test_mode);

// Put LiTE-DTUs in sync mode
  send_ReSync_command(LiTEDTU_SYNC_MODE);
  usleep(1000);

  Int_t first_good_conf[5]={-1,-1,-1,-1,-1}, last_good_conf[5]={-1,-1,-1,-1,-1};
  Int_t conf[512], nconf;
  printf("PLL sync status for conf :\n");
  nconf=0;
  for(Int_t pll_conf1=pll_conf1_min; pll_conf1<=pll_conf1_max; pll_conf1=(pll_conf1<<1)+1)
  {
    for(Int_t pll_conf2=pll_conf2_min; pll_conf2<=pll_conf2_max; pll_conf2++)
    {
      conf[nconf]=(pll_conf1<<3)|pll_conf2;
      nconf++;
    }
  }
  nconf=0;
  for(Int_t pll_conf1=pll_conf1_min; pll_conf1<=pll_conf1_max; pll_conf1=(pll_conf1<<1)+1)
  {
    for(Int_t pll_conf2=pll_conf2_min; pll_conf2<=pll_conf2_max; pll_conf2++)
    {
      pll_conf=(pll_conf1<<3)|pll_conf2;
      for(Int_t i=0; i<daq.n_active_channel; i++)
      {
        Int_t ich=daq.channel_number[i]-1;
        asic.DTU_PLL_conf[ich]=pll_conf;
        if(ich==daq.current_channel-1)
        {
          sprintf(widget_name,"1_DTU_PLL_value");
          widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
          sprintf(widget_content,"%x",pll_conf);
          gtk_entry_set_text((GtkEntry*)widget,widget_content);
          while (gtk_events_pending()) gtk_main_iteration(); // Update menus
        }
        update_LiTEDTU_reg(ich,15,3);
        update_LiTEDTU_reg(ich,16,3);
      }

// Let some time for PLL to stabilize ?
      usleep(100000);

// Once the PLL is set, reset ISERDES instances and get ready for synchronization
      command=DELAY_TAP_DIR*1 | DELAY_RESET*1;
      hw.getNode("DELAY_CTRL1").write(command);
      hw.dispatch();
      usleep(1000);

// Launch link synchronization in test mode :
      command=DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1ffffffu;
      hw.getNode("DELAY_CTRL1").write(command);
      hw.dispatch();
      usleep(1000);
      command=DELAY_TAP_DIR*1;
      hw.getNode("DELAY_CTRL1").write(command);
      hw.dispatch();

// And look at sync status :
      printf("pll_conf 0x%3.3x, 0x%2.2x, 0x%1.1x : ",pll_conf,pll_conf1,pll_conf2);
      for(Int_t ivfe=0; ivfe<N_VFE; ivfe++)
      //for(Int_t i=0; i<daq.n_active_channel; i++)
      {
        //Int_t ich=daq.channel_number[i]-1;
        //if(!asic.channel_status[ich])continue;
        if (!asic.VFE_status[ivfe]) continue;
        //Int_t ivfe=ich/5;
        sprintf(reg_name,"DELAY_CTRL%d",ivfe+1);
        //printf("ch %d vfe %d, reg %s\n",ich,ivfe,reg_name);
        reg = hw.getNode(reg_name).read();
        hw.dispatch();
        //printf("sync status VFE %d : 0x%8.8x, VFE  sync %d, FE sync %d, ch1 0x%2.2x, ch2 0x%2.2x, ch3 0x%2.2x, ch4 0x%2.2x, ch5 0x%2.2x\n",
        //        ivfe+1,reg.value(), (reg.value()>>31)&1, (reg.value()>>30)&1,
        //       (reg.value()>>0)&0x3f,(reg.value()>>6)&0x3f,(reg.value()>>12)&0x3f,(reg.value()>>18)&0x3f,(reg.value()>>24)&0x3f);
        for(Int_t ich=0; ich<5; ich++)
        {
          if((daq.eLink_active&(1<<(ivfe*5+ich)))==0)continue;
          if(((reg.value()>>(ich*6))&0x3f)!=0x1f)
          {
            printf(" X");
            if(first_good_conf[ich]==-1)first_good_conf[ich]=nconf;
            last_good_conf[ich]=nconf;
          }
          else
          {
            printf(" -");
          }
        }
        nconf++;
      }
        printf("\n");
    }
  }
 
// Update DTU registers through GUI :
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    Int_t pll_conf=0x1f;
    if(first_good_conf[ich]>=0)
    {
      Int_t iconf=(first_good_conf[ich]+last_good_conf[ich])/2;
      printf("iconf good : %d\n",iconf);
      if(iconf<0 || iconf>255) iconf=64;
      pll_conf=conf[iconf];
      asic.DTU_PLL_conf[ich]=pll_conf;
    }
    if(ich==daq.current_channel-1)
    {
      sprintf(widget_name,"1_DTU_PLL_value");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      sprintf(widget_content,"%x",pll_conf);
      gtk_entry_set_text((GtkEntry*)widget,widget_content);
      gtk_widget_activate(widget);
      sprintf(widget_name,"1_DTU_force_PLL");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
    }
    update_LiTEDTU_reg(ich,15,3);
    update_LiTEDTU_reg(ich,16,3);
  }
  //send_ReSync_command(LiTEDTU_NORMAL_MODE);

// Re-init VFE board after scan :
  bool scan_Vref=daq.CATIA_scan_Vref;
  daq.CATIA_scan_Vref=FALSE;
  init_VFEs();
  daq.CATIA_scan_Vref=scan_Vref;
}
