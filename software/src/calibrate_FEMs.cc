#define EXTERN extern
#include "gdaq_SAFE.h"
#define NSAMPLE_PED_EVT 4094
#define NSAMPLE_TP_EVT  400
#define NSAMPLE_AWG_EVT 400

void calibrate_FEMs(void)
{
  uhal::HwInterface hw=devices.front();
  GtkWidget* widget;
  char widget_text[80], widget_name[80],reg_name[32];
  GtkTextBuffer *buffer;
  GtkTextIter start, end;


  for(int igain=0; igain<2; igain++)
  {
    for(int ich=0; ich<N_CHANNEL; ich++)
    {
      asic.rms[igain][ich]=0.;
      asic.INL[igain][ich]=0.;
      asic.gain[igain][ich]=0.;
    }
  }
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_init_VFE");
  g_assert(widget);
  gtk_button_clicked((GtkButton*)widget);
  open_results();
  update_results();
  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before leaving

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_comment");
  g_assert (widget);
  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(widget));
  g_assert (buffer);
  gtk_text_buffer_get_start_iter(buffer, &start);
  gtk_text_buffer_get_end_iter(buffer, &end);
  gchar *comment= gtk_text_buffer_get_text (buffer, &start, &end,FALSE);
  strcpy(daq.comment,comment);

// Take pedestal runs in G10 and G1 with 4094 samples 
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ped_event");
  g_assert(widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_nevent");
  g_assert(widget);
  gtk_entry_set_text((GtkEntry*)widget,"100");
  gtk_widget_activate(widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_nsample");
  g_assert(widget);
  sprintf(widget_text,"%d",NSAMPLE_PED_EVT);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  for(int igain=0; igain<2; igain++)
  {
    for(int i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i]-1;
// Select active channel :
      sprintf(widget_name,"0_channel_%d",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert(widget);
      gtk_button_clicked((GtkButton*)widget);
// Remove LiTE-DTU gain selection
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_force_G10");
      g_assert(widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,1-igain);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_force_G1");
      g_assert(widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,igain);
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before leaving
    }
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_take_run");
    g_assert(widget);
    gtk_button_clicked((GtkButton*)widget);
    if(!daq.calibration_running)
    {
// Ask to stop calibration procedure
      printf("Abort button pressed 1 !\n");
      break;
    }
    ana_ped_Monacal();
    update_results();
    if(!daq.calibration_running)
    {
// Ask to stop calibration procedure
      printf("Abort button pressed 2 !\n");
      break;
    }
  }
  if(!daq.calibration_running) return;

// Take linearity runs in G10 and G1 with 400 samples 
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_event");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_nevent");
  g_assert(widget);
  gtk_entry_set_text((GtkEntry*)widget,"100");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_nsample");
  gtk_widget_activate(widget);
  g_assert(widget);
  sprintf(widget_text,"%d",NSAMPLE_TP_EVT);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_level");
  g_assert(widget);
  Int_t TP_level_ref;
  sscanf(gtk_entry_get_text((GtkEntry*)widget),"%d",&TP_level_ref);
  for(int igain=0; igain<2; igain++)
  {
    for(int i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i]-1;
  // Select active channel :
      sprintf(widget_name,"0_channel_%d",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert(widget);
      gtk_button_clicked((GtkButton*)widget);
// Activate both DACs
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_DAC1");
      g_assert(widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_DAC2");
      g_assert(widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  // Remove LiTE-DTU gain selection
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_force_G10"); // GtkToggleButton
      g_assert(widget);
      //gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,1-igain);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_force_G1"); // GtkToggleButton
      g_assert(widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,igain);
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before leaving
    }
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_level");
    g_assert(widget);
    sprintf(widget_text,"%d",TP_level_ref);
    gtk_entry_set_text((GtkEntry*)widget,widget_text);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_step");
    g_assert(widget);
    sprintf(widget_text,"%d",-5-45*igain);
    gtk_entry_set_text((GtkEntry*)widget,widget_text);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_n_TP_steps");
    g_assert(widget);
    sprintf(widget_text,"%d",65);
    gtk_entry_set_text((GtkEntry*)widget,widget_text);
    gtk_widget_activate(widget);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_take_run");
    g_assert(widget);
    gtk_button_clicked((GtkButton*)widget);
    if(!daq.calibration_running)
    {
// Ask to stop calibration procedure
      printf("Abort button pressed 3 !\n");
      break;
    }
    linearity_Monacal();
    update_results();
    if(!daq.calibration_running)
    {
// Ask to stop calibration procedure
      printf("Abort button pressed 4 !\n");
      break;
    }
  }
  if(!daq.calibration_running) return;

// Take AWG runs in G10 and G1 with 400 samples for gain calibration
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_laser_event");
  g_assert (widget);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_nevent");
  g_assert(widget);
  gtk_entry_set_text((GtkEntry*)widget,"100");
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_nsample");
  gtk_widget_activate(widget);
  g_assert(widget);
  sprintf(widget_text,"%d",NSAMPLE_AWG_EVT);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask");
  gtk_widget_activate(widget);
  g_assert(widget);
  Int_t trigger_mask=0;
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    trigger_mask |= 1<<ich;
  }
  sprintf(widget_text,"%x",trigger_mask);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  for(int igain=0; igain<2; igain++)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_threshold");
    gtk_widget_activate(widget);
    g_assert(widget);
    sprintf(widget_text,"%d",200+4095*igain);
    gtk_entry_set_text((GtkEntry*)widget,widget_text);
    gtk_widget_activate(widget);
    for(int i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i]-1;
  // Select active channel :
      sprintf(widget_name,"0_channel_%d",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert(widget);
      gtk_button_clicked((GtkButton*)widget);
  // Remove LiTE-DTU gain selection
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_force_G10"); // GtkToggleButton
      g_assert(widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_force_G1"); // GtkToggleButton
      g_assert(widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,igain);
      while (gtk_events_pending()) gtk_main_iteration(); // Update menus before leaving
    }
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_take_run");
    g_assert(widget);
    gtk_button_clicked((GtkButton*)widget);
    if(!daq.calibration_running)
    {
// Ask to stop calibration procedure
      printf("Abort button pressed 5 !\n");
      break;
    }
    ana_las_Monacal();
    update_results();
    if(!daq.calibration_running)
    {
// Ask to stop calibration procedure
      printf("Abort button pressed 6 !\n");
      break;
    }
  }
  if(!daq.calibration_running) return;

// Restore Button content
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_calibrate_FEMs");
  g_assert (widget);
  gtk_button_clicked((GtkButton*)widget);
}
