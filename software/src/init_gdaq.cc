#define EXTERN extern
#include "gdaq_SAFE.h"

void init_gDAQ()
{

  UInt_t command;
  ValWord<uint32_t> address;
  for(Int_t ich=0; ich<N_CHANNEL; ich++) daq.event[ich]=(short int*)malloc(sizeof(short int)*5*NSAMPLE_MAX);
  for(Int_t ich=0; ich<N_CHANNEL; ich++) daq.gain[ich]=(short int*)malloc(sizeof(short int)*5*NSAMPLE_MAX);
  for(Int_t ich=0; ich<N_CHANNEL; ich++) daq.fevent[ich]=(double*)malloc(sizeof(double)*5*NSAMPLE_MAX);
  daq.XADC_Temp[0]=0x11;
  daq.XADC_Temp[1]=0x13;
  daq.XADC_Temp[2]=0x18;
  daq.XADC_Temp[3]=0x15;
  daq.XADC_Temp[4]=0x19;
  daq.XADC_leak[0]=0x10;
  daq.XADC_leak[1]=0x1A;
  daq.XADC_leak[2]=0x12;
  daq.XADC_leak[3]=0x14;
  daq.XADC_leak[4]=0x1B;

// Define defaults for laser runs :
  daq.delay_auto_tune     = 1;

  Int_t resync_phase      = 0;
  daq.dump_data           = FALSE;
  daq.resync_idle_pattern = 0x00;
  daq.fifo_mode           = 1;
  daq.daq_running         = FALSE;
  daq.CATIA_Vcal_out      = FALSE;

  printf("Start DAQ with SAFE : %d\n", daq.SAFE_number);
  printf("Will address catias : %d with subaddress 3\n",(1<<daq.I2C_shift_dev_number));
  printf("            and DTU : %d with subaddresses 0,1,2\n",(1<<daq.I2C_shift_dev_number));

  printf("Parameters : \n");
  printf("Read ADCs for :\n");
  printf("  %d events \n",daq.nevent);
  printf("  %d samples \n",daq.nsample);
  printf("  trigger type  : %d (0=pedestal, 1=TP, 2=laser/AWG, 3=fpga pulse)\n",daq.trigger_type);
  printf("  trigger loop  : %d (0=triggered from FE, 1=self triggered DAQ from data)\n",daq.self_trigger_loop);
  printf("  self trigger  : %d (1=internal generated trigger if signal > threshold), mode : %d\n",daq.self_trigger, daq.self_trigger_mode);
  printf("  threshold     : %d (minimal signal amplitude to generate self trigger)\n",daq.self_trigger_threshold);
  printf("  mask          : 0x%x (channel mask to generate self triggers)\n",daq.self_trigger_mask);

  //Int_t loc_argc=1;
  //char *loc_argv[10];
  //for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  //sprintf(loc_argv[0],"test");
  //TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  //gStyle->SetPadGridX(kTRUE);
  //gStyle->SetPadGridY(kTRUE);

  char hname[80];
  for(int ich=0; ich<N_CHANNEL; ich++)
  {
    printf("ich = %d\n",ich);
    daq.tg[ich] = new TGraph();
    daq.tg[ich]->SetMarkerStyle(20);
    daq.tg[ich]->SetMarkerSize(0.5);
    daq.tg[ich]->SetMarkerColor(kBlue);
    sprintf(hname,"ch%d_G10_samples",ich+1);
    daq.tg[ich]->SetTitle(hname);
    daq.tg[ich]->SetName(hname);
    daq.tg[ich]->GetYaxis()->SetTitleFont(62);
    daq.tg[ich]->GetYaxis()->SetTitleSize(0.05);
    daq.tg[ich]->GetYaxis()->SetTitleOffset(1.);
    daq.tg[ich]->GetYaxis()->SetLabelSize(0.05);
    daq.tg[ich]->GetXaxis()->SetLabelFont(62);
    daq.tg[ich]->GetXaxis()->SetLabelSize(0.05);
    daq.tg[ich]->GetXaxis()->SetTitleSize(0.05);

    daq.tg_g1[ich] = new TGraph();
    daq.tg_g1[ich]->SetMarkerStyle(20);
    daq.tg_g1[ich]->SetMarkerSize(0.5);
    daq.tg_g1[ich]->SetMarkerColor(kRed);
    sprintf(hname,"ch%d_samples",ich+1);
    daq.tg[ich]->SetTitle(hname);
    daq.tg[ich]->SetName(hname);
    daq.tg_g1[ich]->GetYaxis()->SetTitleFont(62);
    daq.tg_g1[ich]->GetYaxis()->SetTitleSize(0.05);
    daq.tg_g1[ich]->GetYaxis()->SetTitleOffset(1.);
    daq.tg_g1[ich]->GetYaxis()->SetLabelSize(0.05);
    daq.tg_g1[ich]->GetXaxis()->SetLabelFont(62);
    daq.tg_g1[ich]->GetXaxis()->SetLabelSize(0.05);
    daq.tg_g1[ich]->GetXaxis()->SetTitleSize(0.05);
    daq.ped_G10[ich]=-1.;
    daq.ped_G1[ich]=-1.;
    daq.tg[ich]->SetLineColor(kBlue);
    daq.tg_g1[ich]->SetLineColor(kBlue);
  }
  if(daq.use_LMB)
  {
    for(int iADC=0; iADC<N_LMB_ADC; iADC++)
    {
      printf("iADC = %d\n",iADC);
      daq.tg_LMB[iADC] = new TGraph();
      daq.tg_LMB[iADC]->SetMarkerStyle(20);
      daq.tg_LMB[iADC]->SetMarkerSize(0.5);
      daq.tg_LMB[iADC]->SetMarkerColor(kBlue);
      sprintf(hname,"ADC%d_samples",iADC+1);
      daq.tg_LMB[iADC]->SetTitle(hname);
      daq.tg_LMB[iADC]->SetName(hname);
      daq.tg_LMB[iADC]->GetYaxis()->SetTitleFont(62);
      daq.tg_LMB[iADC]->GetYaxis()->SetTitleSize(0.05);
      daq.tg_LMB[iADC]->GetYaxis()->SetTitleOffset(1.);
      daq.tg_LMB[iADC]->GetYaxis()->SetLabelSize(0.05);
      daq.tg_LMB[iADC]->GetXaxis()->SetLabelFont(62);
      daq.tg_LMB[iADC]->GetXaxis()->SetLabelSize(0.05);
      daq.tg_LMB[iADC]->GetXaxis()->SetTitleSize(0.05);

      daq.tg[iADC]->SetLineColor(kBlue);
    }
  }

  if(daq.c1!=NULL)daq.c1->Update();

// Check that SAFE board is reachable :
  Int_t iret, ntry=0;
  char sys_call[132];
  sprintf(sys_call,"ping -c 1 -W 1 10.0.0.%d",daq.SAFE_number+128);
  do
  {
    iret=system(sys_call);
    if(iret!=0)
    {
      Int_t errno;
      GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW(gtk_data.main_window),
                                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                                  GTK_MESSAGE_ERROR,
                                                  GTK_BUTTONS_CLOSE,
      "SAFE board not reachable,\nplease check the network connection.",
                                  NULL, g_strerror (errno));
      gtk_window_set_title(GTK_WINDOW(dialog), "Error");
      gtk_dialog_run (GTK_DIALOG (dialog));
      gtk_widget_destroy (dialog);
      //printf("SAFE board %d not reachable, please check the networ connection.\n",daq.SAFE_number);
      //system("stty raw");
      //char cdum=getchar();
      //system("stty -raw");
    }
  } while(iret!=0 && ntry++<2);

  ConnectionManager manager ( "file://xml/SAFE/connection_file.xml" );
  char safe_str[80];
  sprintf(safe_str,"safe.udp.%d",daq.SAFE_number);
  //uhal::HwInterface hw=manager.getDevice( safe_str );
  devices.push_back(manager.getDevice( safe_str ));
  uhal::HwInterface hw=devices.front();

  ValWord<uint32_t> free_mem;
  ValWord<uint32_t> trig_reg;
  ValWord<uint32_t> mask_reg;
  ValWord<uint32_t> delays;
  ValWord<uint32_t> reg;
  ValWord<uint32_t> debug1_reg[32];
  ValWord<uint32_t> debug2_reg[32];

// Init stage :
  hw.getNode("SAFE_CTRL").write(RESET*1);
  hw.dispatch();
  usleep(1000);

// Read FW version to check :
  reg = hw.getNode("FW_VER").read();
  hw.dispatch();
  char fw_version[80], widget_text[80];
  daq.firmware_version=reg.value();
  sprintf(fw_version,"FW version:\n %8.8x",daq.firmware_version);
  GtkWidget* widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_FW_version");
  g_assert (widget);
  gtk_label_set_text((GtkLabel*)widget,fw_version);

// Switch to triggered mode + external trigger :
//  command= 
//             SELF_TRIGGER_MODE     *daq.self_trigger_mode                |
//            (SELF_TRIGGER_THRESHOLD*(daq.self_trigger_threshold&0x1FFF))  |
//             SELF_TRIGGER          *daq.self_trigger                     |
//             SELF_TRIGGER_LOOP     *daq.self_trigger_loop                |
//             FIFO_MODE             *(daq.fifo_mode&1)                    |
//             RESET                 *0;
//  hw.getNode("SAFE_CTRL").write(command);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask");
  g_assert (widget);
  sprintf(widget_text,"%7.7x",daq.self_trigger_mask);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_threshold");
  g_assert (widget);
  sprintf(widget_text,"%d",daq.self_trigger_threshold&0x1FFF);
  gtk_entry_set_text((GtkEntry*)widget,widget_text);
  gtk_widget_activate(widget);

// Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
  command = ((daq.nsample+1)<<16)+CAPTURE_STOP;
  hw.getNode("CAP_CTRL").write(command);
// Add laser latency before catching data ~ 40 us
  hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*daq.sw_DAQ_delay | HW_DAQ_DELAY*daq.hw_DAQ_delay);
// Switch ON FE-adapter LEDs
  command = GENE_BC0*daq.gen_BC0 + GENE_WTE*daq.gen_WTE + TP_MODE*0 + LED_ON*1 + GENE_TP*0 + GENE_TRIGGER*0;
  hw.getNode("FW_VER").write(command);
  hw.dispatch();

// Reset the reading base address :
  hw.getNode("CAP_ADDRESS").write(0);
// Start DAQ :
  command = ((daq.nsample+1)<<16)+CAPTURE_START;
  hw.getNode("CAP_CTRL").write(command);
// Read back delay values :
  delays=hw.getNode("TRIG_DELAY").read();
// Read back the read/write base address
  address = hw.getNode("CAP_ADDRESS").read();
  free_mem = hw.getNode("CAP_FREE").read();
  trig_reg = hw.getNode("SAFE_CTRL").read();
  mask_reg = hw.getNode("TRIGGER_MASK").read();
  hw.dispatch();

  //firmware_version=0x21083100;
  printf("Firmware version      : %8.8x\n",daq.firmware_version);
  printf("Delays                : %8.8x\n",delays.value());
  printf("Initial R/W addresses : 0x%8.8x\n", address.value());
  printf("Free memory           : 0x%8.8x\n", free_mem.value());
  printf("Trigger mode          : 0x%8.8x\n", trig_reg.value());
  printf("Trigger mask          : 0x%8.8x\n", mask_reg.value());
  printf("Active links          : 0x%2.2x\n", daq.eLink_active);
  daq.old_read_address=address.value()&0xffff;
  if(daq.old_read_address==NSAMPLE_MAX-1)daq.old_read_address=-1;

// TP trigger setting :
  command=(daq.TP_delay<<16) | (daq.TP_width&0xffff);
  printf("TP trigger with %d clocks width and %d clocks delay : %x\n",daq.TP_width,daq.TP_delay,command);
  hw.getNode("CALIB_CTRL").write(command);
  hw.dispatch();

// Set DTU resync idle pattern and switch LV ON if defined by default
  update_VFE_control(0);
  hw.getNode("RESYNC_IDLE").write(RESYNC_IDLE_PATTERN*daq.resync_idle_pattern );
  hw.dispatch();
  usleep(200);
}
