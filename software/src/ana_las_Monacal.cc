#define EXTERN extern
#include "gdaq_SAFE.h"

#define NLOOP 1
#define NSTEP 50
#define NSAMPLE_FFT 64
#define SHAPE_SIZE 10000

void ana_las_Monacal(void)
{
  Int_t F_sampling=160;
  if(daq.MEM_mode)F_sampling=80;
  Double_t Cinj=10.e-12; // 10 pF injection capacitor
  Double_t Vinj=0.1;     // 300 mV pulse
  Double_t Qinj=Vinj*Cinj*1.e15; // Injected charge in fC
  double loc_dt=1000./F_sampling;
  int id;
  short int *sevent[N_CHANNEL];
  double *event[N_CHANNEL];
  Double_t gain[N_CHANNEL];
  char hname[250];
  //double dv=1750./16384.; // AD9642
  double dv=1200./4096.; // LiTE-DTU
  gStyle->SetOptStat(0);
  TProfile *pshape[N_CHANNEL];
  TH1D *hshape[N_CHANNEL],*hshapen[N_CHANNEL];

  Int_t loc_gain, G12=1, color=kBlue;
  sscanf(daq.last_fname,"data/laser_data_G%d",&loc_gain);
  printf("Gain found : %d\n",loc_gain);
  if(loc_gain==1){G12=0; color=kRed;}
  for(Int_t ich=0; ich<N_CHANNEL; ich++)
  {
    asic.gain[1-G12][ich]=0.;
  }
  gettimeofday(&tv,NULL);

  printf("Opening %s for analysis\n",daq.last_fname);
  TFile *infile=new TFile(daq.last_fname);
  TTree *tdata=(TTree*)infile->Get("tdata");
  tdata->SetBranchAddress("id",&id);

// Try to get frame size :
  char title[80];
  sprintf(title,"%s",tdata->GetBranch("ch0")->GetTitle());
  int nsample=0;
  sscanf(title,"ch0[%d]/S",&nsample);
  printf("Setting branches for %d samples\n",nsample);
  for(int ich=0; ich<N_CHANNEL; ich++)
  {
    sevent[ich]=(short int*) malloc(nsample*sizeof(short int));
    event[ich]=(double*) malloc(nsample*sizeof(double));
    char bname[80];
    sprintf(bname,"ch%d",ich);
    tdata->SetBranchAddress(bname,sevent[ich]);
  }
  if(daq.MEM_mode)nsample=(nsample*2)/5;

  Double_t tmax=loc_dt*nsample;
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    sprintf(hname,"ch_%d_step_0_%4.4d",ich,daq.TP_level);
    pshape[ich]=(TProfile*)gDirectory->GetObjectUnchecked(hname);
    sprintf(hname,"pshape_ch%d",ich);
    pshape[ich]->SetName(hname);
    pshape[ich]->SetTitle(hname);
    sprintf(hname,"hshape_ch%d",ich);
    hshape[ich]=new TH1D(hname,hname,nsample,0.,tmax);
    sprintf(hname,"normalized_shape_ch%d",ich);
    hshapen[ich]=new TH1D(hname,hname,nsample,0.,tmax);
    pshape[ich]->Fit("pol0","Q","",0.,100.);
    TF1 *p0=pshape[ich]->GetFunction("pol0");
    Double_t ped=0.;
    if(p0!=NULL) ped=p0->GetParameter(0);

    Double_t tpeak=pshape[ich]->GetBinCenter(pshape[ich]->GetMaximumBin());
    pshape[ich]->Fit("pol2","Q","",tpeak-100.,tpeak+100.);
    TF1 *p2=pshape[ich]->GetFunction("pol2");
    Double_t qmax=1.;
    if(p2!=NULL) qmax=p2->GetMaximum()-ped;
    for(Int_t is=0; is<nsample; is++)
    {
      Double_t val=pshape[ich]->GetBinContent(is+1);
      hshape[ich]->SetBinContent(is+1,val-ped);
      hshapen[ich]->SetBinContent(is+1,(val-ped)/qmax);
    }
    gain[ich]=qmax*dv/Qinj; // mV/fC
  }

  sprintf(hname,"%s",daq.last_fname);
  char *pos=strstr(hname,".root");
  sprintf(pos,"_analized.root");
  printf("Output file : %s\n",hname);
  TFile *tfout=new TFile(hname,"recreate");

  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    pshape[ich]->SetMarkerColor(color);
    pshape[ich]->SetLineColor(color);
    pshape[ich]->Write();
    hshape[ich]->SetMarkerColor(color);
    hshape[ich]->SetLineColor(color);
    hshape[ich]->Write();
    hshapen[ich]->SetMarkerColor(color);
    hshapen[ich]->SetLineColor(color);
    hshapen[ich]->Write();
  }
  tfout->Close();
  tfout-> ~TFile();

  sprintf(pos,"_analized.txt");
  printf("Gains : ");
  FILE *fout=fopen(hname,"w+");
  fprintf(fout,"%ld.%6.6ld : %s\n",tv.tv_sec,tv.tv_usec,daq.comment);

  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    printf("%e ",gain[ich]);
    fprintf(fout,"%e ",gain[ich]);
    asic.gain[1-G12][ich]=gain[ich];
  }
  printf(" mV/fC\n");
  fprintf(fout,"\n");
  fclose(fout);
  for(int ich=0; ich<N_CHANNEL; ich++)
  {
    free((void*)sevent[ich]);
    free((void*)event[ich]);
  }
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    hshape[ich]->~TH1D();
    hshapen[ich]->~TH1D();
  }
  infile->Close();
  infile->~TFile();
}
