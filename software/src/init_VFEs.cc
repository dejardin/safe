#define EXTERN extern
#include "gdaq_SAFE.h"

void init_VFEs()
{
  UInt_t device_number, val;
  UInt_t I2C_data, command;
  FILE *fcal;
  UInt_t iret=0;
  ValWord<uint32_t> reg;
  Int_t ADC_reg_val[2][76];
  GtkWidget *widget;
  char widget_name[80], widget_text[80];
  char reg_name[80];

  uhal::HwInterface hw=devices.front();

// Switch ON and wait for current statibilization :
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_LV");
  g_assert (widget);
// Switch power ON if not yet done:
  if(!gtk_toggle_button_get_active((GtkToggleButton*)widget))
  {
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
    while (gtk_events_pending()) gtk_main_iteration(); // Update menus
    usleep(1000000);
  }

// Update SAFE control register :
  update_trigger();

// Reset VFE board
  update_VFE_control(1);

  printf("Prepare LiTEDTU for safe running (generate ReSync start sequence)\n");

// create VFE bit pattern for I2C and ReSync access : 
  daq.VFE_pattern=0;
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    Int_t bus=asic.I2C_bus[ich];
    daq.VFE_pattern |= 1<<(bus-1);
  }
  hw.getNode("VFE_PATTERN").write(daq.VFE_pattern);
  reg=hw.getNode("VFE_PATTERN").read();
  hw.dispatch();
  printf("Start initialization with VFE pattern 0x%x\n",reg.value());

// DTU Resync init sequence:
// Reset I2C
// Load I2C registers
// For LiTEDTU version >=2.0, put outputs in sync mode with 0x0ccccccf
  printf("Reset DTU_I2C\n");
  send_ReSync_command(LiTEDTU_I2C_reset);
  //printf("Send PLL_init signal\n");
  printf("Reset ADCH/ADCL \n");
  send_ReSync_command((LiTEDTU_PLL_reset<<12) || (LiTEDTU_NORMAL_MODE<<8) | (LiTEDTU_ADCL_reset<<4) | LiTEDTU_ADCH_reset);
  printf("Reset DTU/DTUTestUnit on VFE pattern : 0x%x\n",reg.value());
  send_ReSync_command(LiTEDTU_ADCTestUnit_reset);
  send_ReSync_command(LiTEDTU_DTU_reset);
  usleep(100000);

  printf("I2C_shift_dev_number : %d\n",daq.I2C_shift_dev_number);
// Load default DTU registers
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.DTU_status[ich] || asic.DTU_version[ich]==0)continue;
    printf("Load default DTU register content for channel %d\n",ich+1);

    Int_t num=asic.I2C_address[ich];
    Int_t bus=asic.I2C_bus[ich];
    device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    //printf("I2C access with %d %d %d\n",bus,num,device_number);
    update_LiTEDTU_reg(ich,0,3);
    update_LiTEDTU_reg(ich,1,3);

// G10 and G1 baseline subtraction :
// For LMB, it is done automatically when we send a I2C_reset but we read beack the value
    if (!daq.use_LMB)
    {
      update_LiTEDTU_reg(ich,5,3);
      update_LiTEDTU_reg(ich,6,3);
    }
    else
    {
      update_LiTEDTU_reg(ich,5,2);
      update_LiTEDTU_reg(ich,6,2);
    }
    printf("BL G10 %d, G1 %d\n",asic.DTU_BL_G10[ich],asic.DTU_BL_G1[ich]);
// 00 9f [Main control registers]
// 01 03 [ADC control registers]
// 02 04 [CLPS drivers control]
// 03 20 [CLPS drivers pre-emphasis control]
// 04 00 [CLPS receivers control]
// 05 00 [Baseline subtraction H]
// 06 00 [Baseline subtraction L]
// 07 ff [ADC saturation value registers]
// 08 0f [Baseline subtraction H]
// 09 00 [Status register 0 - READ ONLY]
// 0a 3c [Status register 1 - READ ONLY]
// 0b 88 [Bias control register 0]
// 0c 44 [Bias control register 1]
// 0d 55 [Bias control register 2]
// 0e 55 [Bias control register 3]
// 0f 00 [Bias control register 4]
// 10 3c [Bias control register 5]
// 11 32 [PLL control register 0]
// 12 88 [PLL control register 1]
// 13 88 [PLL control register 2]
// 14 00 [PLL control register 3]
// 15 0c [Synchronization pattern 31-24]
// 16 cc [Synchronization pattern 23-16]
// 17 cc [Synchronization pattern 15-8]
// 18 cf [Synchronization pattern 7-0]
// 19 00 [Test pulse duration]
    iret=I2C_RW(hw, device_number, 18, 0x88, 0, 1, 0);
    iret=I2C_RW(hw, device_number, 19, 0x88, 0, 1, 0);
    iret=I2C_RW(hw, device_number, 20, 0x00, 0, 1, 0);
// PLL reg 17 :
    iret=I2C_RW(hw, device_number, 17, 0, 0, 2, daq.debug_I2C);
    printf("Default PLL %d content of register 0x11 : 0x%2.2x\n",ich,iret&0xFF);
    printf("Prepare PLL %d for self-tuning\n", ich);
    update_LiTEDTU_reg(ich,17,3);

// PLL setting, usefull if we force PLL:
    update_LiTEDTU_reg(ich,16,3);
    update_LiTEDTU_reg(ich,15,3);
// Pre-emphasis:
    update_LiTEDTU_reg(ich,2,3);
    update_LiTEDTU_reg(ich,3,3);
// Override default sync patterns
    update_LiTEDTU_reg(ich,21,3);
    update_LiTEDTU_reg(ich,22,3);
    update_LiTEDTU_reg(ich,23,3);
    update_LiTEDTU_reg(ich,24,3);
    usleep(10000);

// TP duration
    update_LiTEDTU_reg(ich,25,3);

    update_LiTEDTU_reg(ich,7,3);
    update_LiTEDTU_reg(ich,8,3);
  }
  send_ReSync_command(LiTEDTU_PLL_reset);
  usleep(500000);

  reg = hw.getNode("PLL_LOCK").read();
  hw.dispatch();
  UInt_t PLL_lock=reg.value();
  printf("PLL lock values : 0x%8.8x",PLL_lock);
  for(Int_t i=0; i<N_CHANNEL; i++)printf(", %d",(PLL_lock>>i)&1);
  printf("\n");
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.DTU_status[ich] || asic.DTU_version[ich]==0)continue;
    printf("I pass here 2 !\n");

    Int_t num=asic.I2C_address[ich];
    Int_t bus=asic.I2C_bus[ich];
    device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    iret=I2C_RW(hw, device_number, 9, 0, 0, 2, daq.debug_I2C);
    printf("PLL status registers ch %d, addr %d, reg 0x09 : 0x%2.2x, ",ich+1, num, iret&0xff);
    iret=I2C_RW(hw, device_number, 10, 0, 0, 2, daq.debug_I2C);
    printf("0x0a : 0x%2.2x\n",iret&0xff);
  }

// Once the PLL is set, reset ISERDES instances and get ready for synchronization
  hw.getNode("DELAY_CTRL1").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
  hw.dispatch();
  usleep(1000);

// Launch link synchronization with LiTE-DTU-v2.0 using sync patterns :
  if(daq.DTU_test_mode==0)
  {
// First put in LiTe-DTUs in "normal" mode (no sync patterns in 80 MHz mode)
    hw.getNode("DTU_SYNC_PATTERN").write(0x0ccccccf); // Default synchro pattern with LiTEDTU-V2.0
    reg = hw.getNode("DTU_SYNC_PATTERN").read();
    hw.dispatch();
    send_ReSync_command(LiTEDTU_SYNC_MODE);
    usleep(1000);
    printf("Expected Sync patterns : 0x%8.8x\n",reg.value());
    hw.getNode("DELAY_CTRL1").write(DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1ffffff);
    //hw.getNode("DELAY_CTRL1").write(DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1);
    hw.dispatch();
    usleep(1000);
    hw.getNode("DELAY_CTRL1").write(DELAY_TAP_DIR*1);
    hw.dispatch();
    printf("After Sync patterns : 0x%8.8x\n",reg.value());
    send_ReSync_command(LiTEDTU_NORMAL_MODE);
    usleep(1000);
  }
  else // In test mode, we synchronize on headers
  {
    hw.getNode("DELAY_CTRL1").write(DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1ffffff);
    hw.dispatch();
    usleep(1000);
    hw.getNode("DELAY_CTRL1").write(DELAY_TAP_DIR*1);
    hw.dispatch();
  }
  for(Int_t ivfe=0; ivfe<N_VFE; ivfe++)
  {
    sprintf(reg_name,"DELAY_CTRL%d",ivfe+1);
    reg = hw.getNode(reg_name).read();
    hw.dispatch();
    printf("Get sync status for VFE %d : 0x%8.8x, VFE  sync %d, FE sync %d, ch1 0x%2.2x, ch2 0x%2.2x, ch3 0x%2.2x, ch4 0x%2.2x, ch5 0x%2.2x\n",
            ivfe+1,reg.value(), (reg.value()>>31)&1, (reg.value()>>30)&1,
           (reg.value()>>0)&0x3f,(reg.value()>>6)&0x3f,(reg.value()>>12)&0x3f,(reg.value()>>18)&0x3f,(reg.value()>>24)&0x3f);
  }
  reg = hw.getNode("SYNC").read();
  hw.dispatch();
  printf("Sync process duration : %d\n",reg.value());

// Set CATIA output mux for calibation :
// and read Vref value with XADC.
// First : disconnect temp measurement
// Next : Set Output mux
// Then : Set Vref_mux for measurement

// Read XADC register 0x40 and set the requested average to 1 in XADC
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  unsigned loc_ave=ave.value()&0xffff;
  printf("Old config register 0x40 content : %x\n",loc_ave);

  loc_ave=0x8000;
  command=DRP_WRb*1 | (0x40<<16) | loc_ave;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();
  command=DRP_WRb*0 | (0x40<<16);
  hw.getNode("DRP_XADC").write(command);
  ave  = hw.getNode("DRP_XADC").read();
  hw.dispatch();
  loc_ave=ave.value()&0xffff;
  printf("New config register 0x40 content : %x\n",loc_ave);
  command=DRP_WRb*1 | (0x42<<16) | 0x0400;
  hw.getNode("DRP_XADC").write(command);
  hw.dispatch();

// Read  FPGA and CATIA temperatures
  get_temp();

// If we want to scan Vref values, switch OFF everything first :
  Int_t CATIA_Vcal_out_loc=daq.CATIA_Vcal_out;
  daq.CATIA_Vcal_out=TRUE;
  for(int i=0; i<daq.n_active_channel; i++) 
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.CATIA_status[ich]) continue;
    printf("I pass here 3 !\n");
// Set CATIA outputs with calibration levels with no Vref output, no Temp out
    asic.CATIA_temp_out[ich]=0;
    asic.CATIA_temp_X5[ich]=0;
    asic.CATIA_Vref_out[ich]=0;
    iret=update_CATIA_reg(ich,1);
    iret=update_CATIA_reg(ich,5);
  }

  for(int i=0; i<daq.n_active_channel; i++) 
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.CATIA_status[ich]) continue;
    printf("I pass here 4 !\n");

// Set pedestal values, gain and LPF
    iret=update_CATIA_reg(ich,3);
    if(daq.CATIA_scan_Vref)
    {
      printf("Scan CATIA setting to reach Vref_reg = %.3f\n",asic.CATIA_Vref_ref[ich]);
// present Vref to XADC
// Internal multiplexer for version >= 2.0 (selected by I2C)
      printf("Channel %d : internal mux for Vref measurement\n",ich);
      printf("Present Vref on Temp line\n");
      asic.CATIA_Vref_out[ich]=1;
      iret=update_CATIA_reg(ich,5);
      usleep(100000); // Let some time for calibration voltages to stabilize

// And read Vref value on temp line :
      ValWord<uint32_t> temp;
      Int_t average=128,n_Vref=15;
      printf("CATIA %d : default Vref value from XADC %d = ",ich+1,asic.CATIA_XADC_channel[ich]);
      Double_t Vref_best=0., dVref=99999.;
      Int_t Vref_best_setting=0;
      for(int imeas=0; imeas<n_Vref; imeas++)
      {
        double ave_val=0.;
        Int_t loc_meas=imeas;
        if(imeas>0)loc_meas=imeas+1;
        asic.CATIA_Vref_val[ich]=loc_meas;
        val=update_CATIA_reg(ich,6);
        usleep(10000);

        for(int iave=0; iave<average; iave++)
        {
          command=DRP_WRb*0 | (daq.XADC_Temp[asic.CATIA_XADC_channel[ich]-1]<<16);
          hw.getNode("DRP_XADC").write(command);
          temp  = hw.getNode("DRP_XADC").read();
          hw.dispatch();
          double loc_val=double((temp.value()&0xffff)>>4)/4096.;
          //printf("%f\n",loc_val);
          ave_val+=loc_val;
        }
        ave_val/=average;
        ave_val/=daq.Tsensor_divider;
        if(fabs(ave_val-asic.CATIA_Vref_ref[ich])<dVref)
        {
          dVref=fabs(ave_val-asic.CATIA_Vref_ref[ich]);
          Vref_best_setting=loc_meas;
          Vref_best=ave_val;
        }
        printf(" %.2f",ave_val*1000.);
      }
      asic.CATIA_Vref_val[ich]=Vref_best_setting;
      printf(" mV, best=%.2f, DAC=%d\n",Vref_best*1000.,asic.CATIA_Vref_val[ich]);

// Put back CATIA output in high Z :
      asic.CATIA_Vref_out[ich]=0;
      iret=update_CATIA_reg(ich,5);
    }
    val=update_CATIA_reg(ich,6);
  }
  printf("Current channel : %d\n",daq.current_channel);
  update_channel();
  usleep(10000);

// Calibrate ADC with optimized Vref:
  calib_ADC();

// Reset Test unit to get samples in right order :
  send_ReSync_command(LiTEDTU_ADCTestUnit_reset);

  if(daq.use_ref_ADC_calib)
  {
    printf("Loading ADC calibration coefficient from ref file\n");
    fcal=fopen("ref_VFE_calib_reg.dat","r");
    for(int ich=0; ich<N_CHANNEL; ich++)
    {
      Int_t bus=asic.I2C_bus[ich];
      for(int ireg=0; ireg<75; ireg++)
      {
        fscanf(fcal,"%d %d\n",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
      }
      for(int iADC=0; iADC<2; iADC++)
      {
        Int_t num=asic.I2C_address[ich];
        int device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+iADC;
        for(int ireg=0; ireg<75; ireg++)
        {
          iret=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, daq.debug_I2C);
        }
      }
    }
    fclose(fcal);
  }

// Restore CAL mux in normal position after calibration
  daq.CATIA_Vcal_out=CATIA_Vcal_out_loc;
  for(Int_t i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    if(!asic.CATIA_status[ich]) continue;
    printf("I pass here 5 !\n");
    printf("Restore Vcal_out %d for channel %d : %d\n",daq.CATIA_Vcal_out,i,ich);
    update_CATIA_reg(ich,1);
    update_CATIA_reg(ich,3);
    update_CATIA_reg(ich,4);
    update_CATIA_reg(ich,5);
    update_CATIA_reg(ich,6);
  }
  update_trigger();
  get_CRC_errors(1);
}
