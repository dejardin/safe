#define EXTERN extern
#include "gdaq_SAFE.h"

void get_CRC_errors(int reset)
{
  uhal::HwInterface hw=devices.front();
  GtkWidget* widget;
  char widget_text[80], widget_name[80],reg_name[32];

// Reset CRC error counters if requested and only when the pedestals are set correctly.
// When FIFO is full, we have CRC errors since samples are missing :
  if(reset==1)
  {
    hw.getNode("FW_VER").write(GENE_BC0*daq.gen_BC0 + GENE_WTE*daq.gen_WTE + LED_ON*daq.led_ON + CRC_RESET*1);
    hw.dispatch();
  }

// Read SAFE register for CRC errors
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    sprintf(reg_name,"CRC_%d",ich+1);
    ValWord<uint32_t> crc=hw.getNode(reg_name).read();
    hw.dispatch();
    asic.DTU_CRC_errors[ich]=crc.value();
    if(daq.debug_DAQ)printf("Channel %d : CRC errors %d\n",ich+1,asic.DTU_CRC_errors[ich]);
  }
  update_channel();
  while (gtk_events_pending()) gtk_main_iteration(); // Update menus before leaving
}
