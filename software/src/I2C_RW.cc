#include "uhal/uhal.hpp"
#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define I2C_DEVICE      (1<<23)
#define I2C_REG         (1<<16)
#define I2C_BUSY        (1<<3)
#define I2C_ERROR       (1<<4)
#define NRETRY_MAX      100

using namespace uhal;

unsigned int I2C_RW ( uhal::HwInterface hw, unsigned int device_number, unsigned int register_number, unsigned int register_data, int I2C_long, int RW, bool debug )
{
  unsigned int I2C_busy=1;
  unsigned int I2C_error=0;
  char reg_name[32];
  int retry=0;
  int bus=device_number/1000; // Which VFE to address (1 to 5) ?
  device_number-=bus*1000;
  if(bus<1 || bus >5)
  {
    printf("Invalid bus number : %d\n",bus);
    return 0;
  }
  int read=(RW>>1)&1;
  int write=RW&1;
  unsigned int reg_val=0;
  ValWord<uint32_t> busy,reg;
  ValWord<uint32_t> n_ack, ack_pat[6], ack_lsb, ack_msb;

  if(debug) printf("\nI2C access W %d, R %d : bus %d, device 0x%x, register %d, data %d, long %d\n",write, read, bus, device_number, register_number, register_data, I2C_long);
// I2C address : 0xyz000, with xyz=001 (DTU_num=1) or 010 (DTU_num=2), shifted left by 3 each with 3 I2C sub-address (0,1,2 as lsb)
  unsigned int  command;
  if(write==1)
  {
    command=I2C_RWb*0 | I2C_LONG*I2C_long | (device_number&0x7F)*I2C_DEVICE | (register_number&0x7F)*I2C_REG | (register_data&0xFFFF);
    if(debug) printf("Write I2C : bus %d, device 0x%x, register %d, data %d, long %d command : 0x%x\n",
                       bus, device_number, register_number, register_data, I2C_long,command);
    unsigned int loc_pattern=1<<(bus-1);
    hw.getNode("VFE_PATTERN").write(loc_pattern);
    reg=hw.getNode("VFE_PATTERN").read();
    hw.dispatch();
    if(debug) printf("Start I2C write transaction with VFE pattern 0x%x\n",reg.value());
    hw.getNode("I2C1_CTRL").write(command);
    hw.dispatch();

// Wait for transaction end :
    I2C_busy=1;
    retry=0;
    while(I2C_busy==1 && retry<NRETRY_MAX)
    {
      busy = hw.getNode("VFE_CTRL").read();
      hw.dispatch();
      I2C_busy =(busy.value()>>I2C_BUSY)&1;
      I2C_error=(busy.value()>>I2C_ERROR)&1;
      retry++;
    }
    n_ack=hw.getNode("I2C1_N_ACK").read();
    hw.dispatch();

    if(debug)
    {
// Spy ack signals :
      ack_pat[0]=hw.getNode("I2C_ACK_1").read();
      ack_pat[1]=hw.getNode("I2C_ACK_2").read();
      ack_pat[2]=hw.getNode("I2C_ACK_3").read();
      ack_pat[3]=hw.getNode("I2C_ACK_4").read();
      ack_pat[4]=hw.getNode("I2C_ACK_5").read();
      ack_pat[5]=hw.getNode("I2C_ACK_6").read();
      hw.dispatch();
      printf("I2C ack : 0x%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",ack_pat[5].value(),ack_pat[4].value(),ack_pat[3].value(),ack_pat[2].value(),ack_pat[1].value(),ack_pat[0].value());
      int nack_loc=0;
      int ack=0, prev_ack=1;
      for(int iw=5; iw>=0; iw--)
      {
        for(int ib=31; ib>=0; ib--)
        {
          ack=(ack_pat[iw].value()>>ib)&1;
          if(ack==1 && prev_ack==1)
            continue;
          else if (ack==1 && prev_ack==0)
            nack_loc++;
          prev_ack=ack;
        }
      }
      printf("N acknowledge seen : %d. In FW : %d\n",nack_loc,n_ack.value())&0xff;
    }
  }
  reg_val=0;
  if(read==1)
  {
    if(debug)
      printf("Read I2C : bus %d, device 0x%x, register %d, long %d\n", bus, device_number, register_number, I2C_long);
    command=I2C_RWb*1 | I2C_LONG*I2C_long | (device_number&0x7F)*I2C_DEVICE | (register_number&0x7F)*I2C_REG;
    hw.getNode("VFE_PATTERN").write(1<<(bus-1));
    reg=hw.getNode("VFE_PATTERN").read();
    hw.dispatch();
    if(debug>0) printf("Start I2C read transaction with VFE pattern 0x%x\n",reg.value());
    hw.getNode("I2C1_CTRL").write(command);
    hw.dispatch();

// Wait for transaction end :
    I2C_busy=1;
    retry=0;
    while(I2C_busy==1 && retry<NRETRY_MAX)
    {
      busy = hw.getNode("VFE_CTRL").read();
      hw.dispatch();
      I2C_busy =(busy.value()>>(I2C_BUSY))&1;
      I2C_error=(busy.value()>>(I2C_ERROR))&1;
      retry++;
    }
    sprintf(reg_name,"I2C%d_CTRL",bus);
    reg=hw.getNode(reg_name).read();
    sprintf(reg_name,"I2C%d_N_ACK",bus);
    n_ack=hw.getNode(reg_name).read();
    hw.dispatch();
    reg_val=reg.value();

    if(debug)
    {
      printf("Value read : 0x%.8x, busy 0x%x\n",reg_val,I2C_busy);
  // Spy ack signals :
      ack_pat[0]=hw.getNode("I2C_ACK_1").read();
      ack_pat[1]=hw.getNode("I2C_ACK_2").read();
      ack_pat[2]=hw.getNode("I2C_ACK_3").read();
      ack_pat[3]=hw.getNode("I2C_ACK_4").read();
      ack_pat[4]=hw.getNode("I2C_ACK_5").read();
      ack_pat[5]=hw.getNode("I2C_ACK_6").read();
      hw.dispatch();
      printf("I2C ack : 0x%8.8x %8.8x %8.8x %8.8x %8.8x %8.8x\n",ack_pat[5].value(), ack_pat[4].value(),ack_pat[3].value(),ack_pat[2].value(),ack_pat[1].value(),ack_pat[0].value());
      int nack_loc=0;
      int ack=0, prev_ack=1;
      for(int iw=5; iw>=0; iw--)
      {
        for(int ib=31; ib>=0; ib--)
        {
          ack=(ack_pat[iw].value()>>ib)&1;
          if(ack==1 && prev_ack==1)
            continue;
          else if (ack==1 && prev_ack==0)
            nack_loc++;
          prev_ack=ack;
        }
      }
      printf("N acknowledge seen : %d, in FW : %d\n",nack_loc,n_ack.value()&0xff);
    }
  }

  int i_ack=(n_ack.value())&0xff;
  return (reg_val&0x7FFFFF)|(I2C_error<<31)|(i_ack<<23); 
}
