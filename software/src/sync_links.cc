#define EXTERN extern
#include "gdaq_SAFE.h"
void synchronize_links(void)
{
  uhal::HwInterface hw=devices.front();
  UInt_t command;
  ValWord<uint32_t> reg;
  char reg_name[80];

  reg = hw.getNode("DTU_SYNC_PATTERN").read();
  hw.dispatch();
  printf("Expected Sync patterns : 0x%8.8x\n",reg.value());
// Put LiTE-DTUs in sync mode
  send_ReSync_command(LiTEDTU_SYNC_MODE);
  usleep(1000);

  update_VFE_control(0);

// Set which VFEs to synchronize :
  hw.getNode("VFE_PATTERN").write(daq.VFE_pattern);
// Reset iDelay lines
  command=DELAY_TAP_DIR*1 | DELAY_RESET*1;
  hw.getNode("DELAY_CTRL1").write(command);
  hw.dispatch();
  for(Int_t i=0; i<N_CHANNEL; i++)
  {
    daq.delay_val[i]=0;
    daq.bitslip_val[i]=0;
    daq.byteslip_val[i]=0;
  }

// Launch links synchronization :
  command=DELAY_TAP_DIR*1 | START_IDELAY_SYNC*0x1ffffffu;
  hw.getNode("DELAY_CTRL1").write(command);
  hw.dispatch();
  usleep(1000);
  command=DELAY_TAP_DIR*1;
  hw.getNode("DELAY_CTRL1").write(command);
  hw.dispatch();
  for(Int_t ivfe=0; ivfe<N_VFE; ivfe++)
  {
    sprintf(reg_name,"DELAY_CTRL%d",ivfe+1);
    reg = hw.getNode(reg_name).read();
    hw.dispatch();
    printf("Get sync status VFE %d : 0x%8.8x, 0x%2.2x 0x%2.2x 0x%2.2x 0x%2.2x 0x%2.2x\n", ivfe+1,reg.value(),
           (reg.value()>>0)&0x3f,(reg.value()>>6)&0x3f,(reg.value()>>12)&0x3f,(reg.value()>>18)&0x3f,(reg.value()>>24)&0x3f);
  }
  reg = hw.getNode("PLL_LOCK").read();
  hw.dispatch();
  UInt_t PLL_lock=reg.value();
  printf("PLL lock values : 0x%x",PLL_lock);
  for(Int_t i=0; i<N_CHANNEL; i++)printf(", %d",(PLL_lock>>i)&1);
  printf("\n");


// Put LiTE-DTUs in normal mode
  update_VFE_control(0);
  send_ReSync_command(LiTEDTU_NORMAL_MODE);
  usleep(1000);
}
