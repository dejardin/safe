#define EXTERN extern
#include <gtk/gtk.h>
#include "gdaq_SAFE.h"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "limits.h"

void read_config(GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;
  GtkFileFilter *filter;
  dialog = gtk_file_chooser_dialog_new ("Open File",
                      NULL,
                      GTK_FILE_CHOOSER_ACTION_OPEN,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                      NULL);

  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_filter_set_name(filter, "xml files");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);

// Get working directory :
  char cwd[PATH_MAX];
  if (getcwd(cwd, sizeof(cwd)) != NULL) printf("Current working dir: %s\n", cwd);
  strcat(cwd,"/xml/");
  printf("Defaults xml dir: %s\n", cwd);
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog),cwd);

  gtk_widget_show_all(dialog);
  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    char *filename;

    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (dialog));
    strcpy(daq.xml_filename,filename);
    read_conf_from_xml_file (filename);
    FILE *fd_xml=fopen(".last_xml_file","w+");
    fprintf(fd_xml,"%s",daq.xml_filename);
    fclose(fd_xml);
    g_free (filename);

    char fname[132],fname2[111];
    strncpy(fname2,daq.xml_filename,110);
    sprintf(fname,"gdaq_VFE.exe : %s",fname2);
    gtk_window_set_title((GtkWindow*)gtk_data.main_window,fname);
  }

  gtk_widget_destroy (dialog);
  return;
}
void save_config(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Save current config in %s\n",daq.xml_filename);
  write_conf_to_xml_file(daq.xml_filename);
  return;
}

void save_config_as(GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;
  GtkFileFilter *filter;
  dialog = gtk_file_chooser_dialog_new ("Save File",
                      NULL,
                      GTK_FILE_CHOOSER_ACTION_SAVE,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                      NULL);

  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_filter_set_name(filter, "xml files");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter);
  printf("Current file : %s\n",daq.xml_filename);
  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), daq.xml_filename);

// Get working directory :
  char cwd[PATH_MAX];
  if (getcwd(cwd, sizeof(cwd)) != NULL) printf("Current working dir: %s\n", cwd);
  strcat(cwd,"/xml/");
  printf("Defaults xml dir: %s\n", cwd);
  gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog),cwd);

  gtk_widget_show_all(dialog);
  if (gtk_dialog_run(GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
  {
    char *filename;
    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER (dialog));
    printf("Select file : %s\n",filename);
    strcpy(daq.xml_filename,filename);
    FILE *fd_xml=fopen(".last_xml_file","w+");
    fprintf(fd_xml,"%s",daq.xml_filename);
    fclose(fd_xml);
    g_free (filename);
    write_conf_to_xml_file(daq.xml_filename);

    char fname[132],fname2[111];
    strncpy(fname2,daq.xml_filename,110);
    sprintf(fname,"gdaq_VFE.exe : %s",fname2);
    gtk_window_set_title((GtkWindow*)gtk_data.main_window,fname);
  }

  gtk_widget_destroy (dialog);
  return;
}

void read_conf_from_xml_file(char *filename)
{
  xmlChar *child1_name, *child1_status, *child1_id, *child1_value, *child1_level;
  xmlChar *child2_name, *child2_status, *child2_id, *child2_value, *child2_level;
  xmlChar *child3_name, *child3_status, *child3_id, *child3_value, *child3_level;
  xmlChar *child4_name, *child4_status, *child4_id, *child4_value, *child4_level;
  xmlDocPtr doc;
  xmlNodePtr top, level_1, level_2, level_3, level_4;
  Int_t loc_VFE, loc_ich;

  char fname[132], widget_name[80], content[132];
  GtkWidget *widget;
  daq.SAFE_number=0;
  daq.use_LMB = FALSE;
  daq.resync_phase=0;
  asic.ADC_dv=1200./4096.; // Default value for LiTE-DTU

// disable channels y defaults :
  for(Int_t ich=0; ich<N_CHANNEL; ich++)
  {
    asic.channel_status[ich]=false;
    asic.CATIA_status[ich]=false;
    asic.DTU_status[ich]=false;
    asic.I2C_bus[ich]=-1;
    asic.I2C_address[ich]=-1;
    asic.CATIA_reg_def[ich][0]=0x00;
    asic.CATIA_reg_def[ich][1]=0x02;
    asic.CATIA_reg_def[ich][2]=0x00;
    asic.CATIA_reg_def[ich][3]=0xE083;
    asic.CATIA_reg_def[ich][4]=0xFFFF;
    asic.CATIA_reg_def[ich][5]=0xFFFF;
    asic.CATIA_reg_def[ich][6]=0x6F;
    //asic.DTU_invert_clock[ich]=0;
    asic.DTU_dither[ich]=FALSE;
    asic.DTU_nsample_calib_x4[ich]=FALSE;
    asic.DTU_global_test[ich]=FALSE;
    asic.DTU_G1_window16[ich]=TRUE;
    asic.CATIA_DAC_buf_off[ich]=0;
    asic.CATIA_temp_out[ich]=0;
    asic.CATIA_temp_X5[ich]=0;
    asic.CATIA_Vref_out[ich]=0;
    asic.CATIA_Vref_ref[ich]= 1.000;                             // Target Vref value for calibration level setting
  }
  for(Int_t ivfe=0; ivfe<N_VFE; ivfe++)
  {
    asic.VFE_status[ivfe]=false;
    sprintf(widget_name,"0_VFE_%d",ivfe+1);
    printf("%s %d\n",widget_name,N_VFE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    for(Int_t ich=ivfe*5; ich<(ivfe+1)*5; ich++)
    {
      sprintf(widget_name,"0_channel_%d",ich+1);
      g_assert (widget);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      gtk_widget_set_sensitive(widget,FALSE);
    }
  }

  daq.n_active_channel=0;
  daq.eLink_active=0;
  if(filename==NULL)
    sprintf(fname,"xml/config_SAFE.xml");
  else
    strcpy(fname,filename);
  printf("Read conf from xml file %s\n",fname);

  doc = xmlParseFile(fname);
  if (doc == NULL )
  {
    fprintf(stderr,"Document not parsed successfully. \n");
    return;
  }

  top = xmlDocGetRootElement(doc);
  if (top == NULL)
  {
    fprintf(stderr,"empty document\n");
    xmlFreeDoc(doc);
    return;
  }

  printf("Start to parse document : %s\n",fname);
  if (xmlStrcmp(top->name, (const xmlChar *) "safe"))
  {
    fprintf(stderr,"document of the wrong type, root node != safe");
    xmlFreeDoc(doc);
    return;
  }
  else
  {
    printf("Starting point : %s\n",top->name);
  }

  level_1 = top->xmlChildrenNode;
  while (level_1 != NULL)
  {
    child1_name=xmlGetProp(level_1,(const xmlChar *)"name");
    child1_value=xmlGetProp(level_1,(const xmlChar *)"value");
    child1_level=xmlGetProp(level_1,(const xmlChar *)"level");
    child1_id=xmlGetProp(level_1,(const xmlChar *)"id");
    child1_status=xmlGetProp(level_1,(const xmlChar *)"status");
    printf("level1 : %s %s : value %s level %s id %s status %s\n",level_1->name,child1_name, child1_value, child1_level, child1_id, child1_status);
    if ((!xmlStrcmp(level_1->name, (const xmlChar *)"object")))
    {
      printf("Start to parse object : %s\n",xmlGetProp(level_1,(const xmlChar *)"name"));

// DAQ settings :
      if ((!xmlStrcmp(child1_name, (const xmlChar *)"DAQ")))
      {
        level_2 = level_1->xmlChildrenNode;
        while (level_2 != NULL)
        {
          //child2_value = xmlNodeListGetString(doc, level_2->xmlChildrenNode, 1);
          child2_name=xmlGetProp(level_2,(const xmlChar *)"name");
          child2_value=xmlGetProp(level_2,(const xmlChar *)"value");
          child2_level=xmlGetProp(level_2,(const xmlChar *)"level");
          child2_id=xmlGetProp(level_2,(const xmlChar *)"id");
          child2_status=xmlGetProp(level_2,(const xmlChar *)"status");
          printf("level2 : %s %s : value %s level %s id %s status %s\n",level_2->name,child2_name, child2_value, child2_level, child2_id, child2_status);
          if ((!xmlStrcmp(level_2->name, (const xmlChar *)"property")))
          {
            if(!xmlStrcmp(child2_name, (const xmlChar *)"SAFE"))
            {
              sscanf((const char*)child2_value,"%d",&daq.SAFE_number);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_SAFE_number");
              g_assert (widget);
              sprintf(content,"%d",daq.SAFE_number);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"use_LMB"))
            {
              daq.use_LMB=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              printf("Use LMB : %d\n",daq.use_LMB);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"LV"))
            {
              daq.LV_ON=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_LV");
              g_assert (widget);
              if(!daq.LV_ON)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_widget_set_name(widget,"red_button");
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_widget_set_name(widget,"green_button");
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"resync_phase"))
            {
              sscanf((const char*)child2_value,"%d",&daq.resync_phase);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_ReSync_phase");
              g_assert (widget);
              sprintf(content,"%d",daq.resync_phase);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"test_mode"))
            {
              sscanf((const char*)child2_value,"%d",&daq.DTU_test_mode);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_DTU_test_mode");
              g_assert (widget);
              if(daq.DTU_test_mode==0)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"I2C_shift_dev_number"))
            {
              sscanf((const char*)child2_value,"%d",&daq.I2C_shift_dev_number);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"debug_DAQ"))
            {
              daq.debug_DAQ=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_debug_DAQ");
              g_assert (widget);
              if(!daq.debug_DAQ)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"debug_I2C"))
            {
              daq.debug_I2C=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_debug_I2C");
              g_assert (widget);
              if(!daq.debug_I2C)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"debug_GTK"))
            {
              daq.debug_GTK=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_debug_I2C");
              g_assert (widget);
              if(!daq.debug_I2C)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"debug_draw"))
            {
              daq.debug_draw=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_debug_draw");
              g_assert (widget);
              if(!daq.debug_draw)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"zoom"))
            {
              daq.zoom_draw=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_zoom_draw");
              g_assert (widget);
              if(!daq.zoom_draw)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"corgain"))
            {
              daq.corgain=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"wait_for_ever"))
            {
              daq.wait_for_ever=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"I2C_lpGBT"))
            {
              daq.I2C_lpGBT_mode=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_I2C_protocol");
              g_assert (widget);
              if(!daq.I2C_lpGBT_mode)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"HW_delay"))
            {
              sscanf((const char*)child2_value,"%d",&daq.hw_DAQ_delay);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_HW_delay");
              g_assert (widget);
              sprintf(content,"%d",daq.hw_DAQ_delay);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"SW_delay"))
            {
              sscanf((const char*)child2_value,"%d",&daq.sw_DAQ_delay);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_SW_delay");
              g_assert (widget);
              sprintf(content,"%d",daq.sw_DAQ_delay);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"TP_delay"))
            {
              sscanf((const char*)child2_value,"%d",&daq.TP_delay);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_TP_delay");
              g_assert (widget);
              sprintf(content,"%d",daq.TP_delay);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"TP_width"))
            {
              sscanf((const char*)child2_value,"%d",&daq.TP_width);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_TP_width");
              g_assert (widget);
              sprintf(content,"%d",daq.TP_width);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"TP_level"))
            {
              sscanf((const char*)child2_value,"%d",&daq.TP_level);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_level");
              g_assert (widget);
              sprintf(content,"%d",daq.TP_level);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"TP_step"))
            {
              sscanf((const char*)child2_value,"%d",&daq.TP_step);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_step");
              g_assert (widget);
              sprintf(content,"%d",daq.TP_step);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"n_TP_step"))
            {
              sscanf((const char*)child2_value,"%d",&daq.n_TP_step);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_n_TP_steps");
              g_assert (widget);
              sprintf(content,"%d",daq.n_TP_step);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"nevent"))
            {
              sscanf((const char*)child2_value,"%d",&daq.nevent);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_nevent");
              g_assert (widget);
              sprintf(content,"%d",daq.nevent);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"nsample"))
            {
              sscanf((const char*)child2_value,"%d",&daq.nsample);
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_nsample");
              g_assert (widget);
              sprintf(content,"%d",daq.nsample);
              gtk_entry_set_text((GtkEntry*)widget,content);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"gen_BC0"))
            {
              daq.gen_BC0=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gen_BC0");
              gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.gen_BC0);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.gen_BC0);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"gen_WTE"))
            {
              daq.gen_WTE=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_gen_WTE");
              gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.gen_WTE);
              gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.gen_WTE);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"CATIA_scan_Vref"))
            {
              daq.CATIA_scan_Vref=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"CATIA_Vcal_out"))
            {
              daq.CATIA_Vcal_out=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"Tsensor_divider"))
            {
              sscanf((const char*)child2_value,"%lf",&daq.Tsensor_divider);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"PLL_override_Vc_bit"))
            {
              asic.DTU_override_Vc_bit[0]=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              for(Int_t i=1; i<N_CHANNEL; asic.DTU_override_Vc_bit[i++]=asic.DTU_override_Vc_bit[0]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"use_MEM_mode"))
            {
              daq.MEM_mode=!xmlStrcasecmp(child2_status,(const xmlChar *)"ON");
              widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"2_MEM_mode");
              g_assert (widget);
              if(!daq.MEM_mode)
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
              }
              else
              {
                gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
                gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
              }
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"PLL_override_Vc_bit"))
            {
              asic.DTU_override_Vc_bit[0]=!xmlStrcasecmp(child2_value,(const xmlChar *)"ON");
              for(Int_t i=1; i<N_CHANNEL; asic.DTU_override_Vc_bit[i++]=asic.DTU_override_Vc_bit[0]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"CATIA_reg1_def"))
            {
              sscanf((const char*)child2_value,"0x%x",&asic.CATIA_reg_def[0][1]);
              for(Int_t i=1; i<N_CHANNEL; asic.CATIA_reg_def[i++][1]=asic.CATIA_reg_def[0][1]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"CATIA_reg2_def"))
            {
              sscanf((const char*)child2_value,"0x%x",&asic.CATIA_reg_def[0][2]);
              for(Int_t i=1; i<N_CHANNEL; asic.CATIA_reg_def[i++][2]=asic.CATIA_reg_def[0][2]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"CATIA_reg3_def"))
            {
              sscanf((const char*)child2_value,"0x%x",&asic.CATIA_reg_def[0][3]);
              for(Int_t i=1; i<N_CHANNEL; asic.CATIA_reg_def[i++][3]=asic.CATIA_reg_def[0][3]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"CATIA_reg4_def"))
            {
              sscanf((const char*)child2_value,"0x%x",&asic.CATIA_reg_def[0][4]);
              for(Int_t i=1; i<N_CHANNEL; asic.CATIA_reg_def[i++][4]=asic.CATIA_reg_def[0][4]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"CATIA_reg5_def"))
            {
              sscanf((const char*)child2_value,"0x%x",&asic.CATIA_reg_def[0][5]);
              for(Int_t i=1; i<N_CHANNEL; asic.CATIA_reg_def[i++][5]=asic.CATIA_reg_def[0][5]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"CATIA_reg6_def"))
            {
              sscanf((const char*)child2_value,"0x%x",&asic.CATIA_reg_def[0][6]);
              for(Int_t i=1; i<N_CHANNEL; asic.CATIA_reg_def[i++][6]=asic.CATIA_reg_def[0][6]);
            }
            else if(!xmlStrcmp(child2_name, (const xmlChar *)"ADC_dv"))
            {
              sscanf((const char*)child2_value,"%lf",&asic.ADC_dv);
            }
          }
          level_2 = level_2->next;
          if(!child2_name)xmlFree(child2_name);
          if(!child2_value)xmlFree(child2_value);
          if(!child2_status)xmlFree(child2_status);
          if(!child2_id)xmlFree(child2_id);
          if(!child2_level)xmlFree(child2_level);
        }
        printf("with debug flags : DAQ %d, I2C %d, GTK %d, draw %d\n",daq.debug_DAQ,daq.debug_I2C,daq.debug_GTK,daq.debug_draw);
        printf("and MEM_mode set to %d\n",daq.MEM_mode);
      }

// Channel settings :
      else if (!xmlStrcmp(child1_name, (const xmlChar *)"VFE"))
      {
        sscanf((const char*)child1_id,"%d",&loc_VFE);
        if(loc_VFE<1 || loc_VFE>5) continue;
        if(!xmlStrcasecmp(child1_status,(const xmlChar *)"ON"))
        {
          asic.VFE_status[loc_VFE-1]=TRUE;
        }
        printf("VFE id %d, status %s\n",loc_VFE,child1_status);

        level_2 = xmlFirstElementChild(level_1);
        while(level_2 != NULL)
        {
          child2_name=xmlGetProp(level_2,(const xmlChar *)"name");
          child2_value=xmlGetProp(level_2,(const xmlChar *)"value");
          child2_level=xmlGetProp(level_2,(const xmlChar *)"level");
          child2_id=xmlGetProp(level_2,(const xmlChar *)"id");
          child2_status=xmlGetProp(level_2,(const xmlChar *)"status");
          printf("level2 : %s %s : value %s level %s id %s status %s\n",level_2->name,child2_name, child2_value, child2_level, child2_id, child2_status);
          if ((!xmlStrcmp(level_2->name, (const xmlChar *)"object")))
          {
            if (!xmlStrcmp(child2_name, (const xmlChar *)"channel"))
            {
              sscanf((const char*)child2_id,"%d",&loc_ich);
              if(loc_ich<1 || loc_ich>5) continue;
              daq.current_channel=(loc_VFE-1)*5+loc_ich;
              if(!xmlStrcasecmp(child2_status,(const xmlChar *)"ON"))
              {
                asic.I2C_bus[daq.current_channel-1]=loc_VFE;
                asic.I2C_address[daq.current_channel-1]=loc_ich;
                daq.channel_number[daq.n_active_channel++]=daq.current_channel;
                asic.channel_status[daq.current_channel-1]=TRUE;
              }

              level_3 = xmlFirstElementChild(level_2);
              while(level_3 != NULL)
              {
                child3_name=xmlGetProp(level_3,(const xmlChar *)"name");
                child3_value=xmlGetProp(level_3,(const xmlChar *)"value");
                child3_level=xmlGetProp(level_3,(const xmlChar *)"level");
                child3_id=xmlGetProp(level_3,(const xmlChar *)"id");
                child3_status=xmlGetProp(level_3,(const xmlChar *)"status");
                printf("level3 : %s %s : value %s level %s id %s status %s\n",level_3->name, child3_name, child3_value, child3_level, child3_id, child3_status);
                if ((!xmlStrcmp(level_3->name, (const xmlChar *)"property")))
                {
                  if(!xmlStrcmp(child3_name,(const xmlChar *)"I2C_bus"))
                  {
                    sscanf((const char*)child3_value,"%d",&asic.I2C_bus[daq.current_channel-1]);
                  }
                  if(!xmlStrcmp(child3_name,(const xmlChar *)"I2C_address"))
                  {
                    sscanf((const char*)child3_value,"%d",&asic.I2C_address[daq.current_channel-1]);
                  }
                }
                else if ((!xmlStrcmp(level_3->name, (const xmlChar *)"object")))
                {
                  if(!xmlStrcmp(child3_name,(const xmlChar *)"CATIA"))
                  {
                    asic.CATIA_status[daq.current_channel-1]=!xmlStrcmp(child3_status,(const xmlChar *)"ON");
                  }
                  if(!xmlStrcmp(child3_name,(const xmlChar *)"LiTEDTU"))
                  {
                    asic.DTU_status[daq.current_channel-1]=!xmlStrcmp(child3_status,(const xmlChar *)"ON");
                    if(asic.DTU_status[daq.current_channel-1])
                    {
                      asic.DTU_eLink[daq.current_channel-1]=TRUE;
                      daq.eLink_active|=(1<<(daq.current_channel-1));
                    }
                    else
                      asic.DTU_eLink[daq.current_channel-1]=FALSE;
                  }
                  level_4 = level_3->xmlChildrenNode;
                  while (level_4 != NULL)
                  {
                    if ((!xmlStrcmp(level_4->name, (const xmlChar *)"property")))
                    {
                      //child4_value=xmlNodeListGetString(doc, level_4->xmlChildrenNode, 1);
                      child4_name  =xmlGetProp(level_4,(const xmlChar *)"name");
                      child4_status=xmlGetProp(level_4,(const xmlChar *)"status");
                      child4_id    =xmlGetProp(level_4,(const xmlChar *)"id");
                      child4_value =xmlGetProp(level_4,(const xmlChar *)"value");
                      child4_level =xmlGetProp(level_4,(const xmlChar *)"level");
                      printf("level4 : %s, property %s : value %s level %s id %s status %s\n",level_4->name,child4_name, child4_value, child4_level, child4_id, child4_status);
                      if(!xmlStrcmp(child3_name,(const xmlChar *)"CATIA"))
                      {
                        if(!xmlStrcmp(child4_name,(const xmlChar *)"version"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.CATIA_version[daq.current_channel-1]);
                        }
                        if(!xmlStrcmp(child4_name,(const xmlChar *)"XADC_channel"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.CATIA_XADC_channel[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"ped_G10"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.CATIA_ped_G10[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"ped_G1"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.CATIA_ped_G1[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"Vref_target"))
                        {
                          sscanf((const char*)child4_value,"%lf",&asic.CATIA_Vref_ref[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"Vref"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.CATIA_Vref_val[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"gain"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.CATIA_gain[daq.current_channel-1]);
                          if(asic.CATIA_gain[daq.current_channel-1]==2)asic.CATIA_gain[daq.current_channel-1]++;
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"LPF35"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.CATIA_LPF35[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"DAC1"))
                        {
                          asic.CATIA_DAC1_status[daq.current_channel-1]=!xmlStrcasecmp(child4_status,(const xmlChar *)"ON");
                          sscanf((const char*)child4_value,"%d",&asic.CATIA_DAC1_val[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"DAC2"))
                        {
                          asic.CATIA_DAC2_status[daq.current_channel-1]=!xmlStrcasecmp(child4_status,(const xmlChar *)"ON");
                          sscanf((const char*)child4_value,"%d",&asic.CATIA_DAC2_val[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"SEU_corr"))
                        {
                          asic.CATIA_SEU_corr[daq.current_channel-1]=!xmlStrcasecmp(child4_status,(const xmlChar *)"ON");
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"Temp_offset"))
                        {
                          sscanf((const char*)child4_value,"%lf",&asic.CATIA_Temp_offset[daq.current_channel-1]);
                        }
                      }
                      else if(!xmlStrcmp(child3_name,(const xmlChar *)"LiTEDTU"))
                      {
                        if(!xmlStrcmp(child4_name,(const xmlChar *)"version"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.DTU_version[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"BL_G10"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.DTU_BL_G10[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"BL_G1"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.DTU_BL_G1[daq.current_channel-1]);
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"force_G10"))
                        {
                          asic.DTU_force_G10[daq.current_channel-1]=!xmlStrcasecmp(child4_status,(const xmlChar *)"ON");
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"force_G1"))
                        {
                          asic.DTU_force_G1[daq.current_channel-1]=!xmlStrcasecmp(child4_status,(const xmlChar *)"ON");
                        }
                        else if(!xmlStrcmp(child4_name,(const xmlChar *)"force_PLL"))
                        {
                          asic.DTU_force_PLL[daq.current_channel-1]=!xmlStrcasecmp(child4_status,(const xmlChar *)"ON");
                          sscanf((const char*)child4_value,"0x%x",&asic.DTU_PLL_conf[daq.current_channel-1]);
                          asic.DTU_PLL_conf[daq.current_channel-1] &= 0x01ff;
                          asic.DTU_PLL_conf1[daq.current_channel-1]= asic.DTU_PLL_conf[daq.current_channel-1]&0x7;
                          asic.DTU_PLL_conf2[daq.current_channel-1]= asic.DTU_PLL_conf[daq.current_channel-1]>>3;
                        }
                        else if(!xmlStrcmp(child4_name, (const xmlChar *)"PLL_clock_out"))
                        {
                          asic.DTU_clk_out[daq.current_channel-1]=!xmlStrcasecmp(child4_status,(const xmlChar *)"ON");
                        }
                        else if(!xmlStrcmp(child4_name, (const xmlChar *)"VCO_rail_mode"))
                        {
                          asic.DTU_VCO_rail_mode[daq.current_channel-1]=!xmlStrcasecmp(child4_status,(const xmlChar *)"ON");
                        }
                        else if(!xmlStrcmp(child4_name, (const xmlChar *)"bias_ctrl_override"))
                        {
                          asic.DTU_bias_ctrl_override[daq.current_channel-1]=!xmlStrcasecmp(child4_status,(const xmlChar *)"ON");
                        }
                        else if(!xmlStrcmp(child4_name, (const xmlChar *)"driver_current"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.DTU_driver_current[daq.current_channel-1]);
                          if(asic.DTU_driver_current[daq.current_channel-1]>7)asic.DTU_driver_current[daq.current_channel-1]=7;
                        }
                        else if(!xmlStrcmp(child4_name, (const xmlChar *)"PE_width"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.DTU_PE_width[daq.current_channel-1]);
                          if(asic.DTU_PE_width[daq.current_channel-1]>7)asic.DTU_PE_width[daq.current_channel-1]=7;
                        }
                        else if(!xmlStrcmp(child4_name, (const xmlChar *)"PE_strength"))
                        {
                          sscanf((const char*)child4_value,"%d",&asic.DTU_PE_strength[daq.current_channel-1]);
                          if(asic.DTU_PE_strength[daq.current_channel-1]>7)asic.DTU_PE_strength[daq.current_channel-1]=7;
                        }
                      }

                      if(!child4_name)xmlFree(child4_name);
                      if(!child4_value)xmlFree(child4_value);
                      if(!child4_level)xmlFree(child4_level);
                      if(!child4_id)xmlFree(child4_id);
                      if(!child4_status)xmlFree(child4_status);
                    }
                    level_4 = level_4->next;
                  }
                }
                level_3 = level_3->next;
                if(!child3_name)xmlFree(child3_name);
                if(!child3_value)xmlFree(child3_value);
                if(!child3_level)xmlFree(child3_level);
                if(!child3_id)xmlFree(child3_id);
                if(!child3_status)xmlFree(child3_status);
              }
            }
            printf("Load config for channel %d : CATIA version : %.1f, LiTE-DTU version %.1f, bs10 0x%3.3x, bs1 0x%3.3x, pll 0x%3.3x=0x%2.2x 0x%1.1x, Vref %d, CATIA gain %d, CATIA LPF %d, force DTU PLL %d\n",
               daq.current_channel,asic.CATIA_version[daq.current_channel-1]/10.,asic.DTU_version[daq.current_channel-1]/10.,
               asic.DTU_BL_G10[daq.current_channel-1],   asic.DTU_BL_G1[daq.current_channel-1],
               asic.DTU_PLL_conf[daq.current_channel-1],  asic.DTU_PLL_conf1[daq.current_channel-1],asic.DTU_PLL_conf2[daq.current_channel-1],
               asic.CATIA_Vref_val[daq.current_channel-1],asic.CATIA_gain[daq.current_channel-1],   asic.CATIA_LPF35[daq.current_channel-1], 
               asic.DTU_force_PLL[daq.current_channel-1]);
          }
          level_2=level_2->next;
          if(!child2_name)xmlFree(child2_name);
          if(!child2_value)xmlFree(child2_value);
          if(!child2_level)xmlFree(child2_level);
          if(!child2_id)xmlFree(child2_id);
          if(!child2_status)xmlFree(child2_status);
        };
      }
    }
    level_1 = level_1->next;
    if(!child1_name)xmlFree(child1_name);
    if(!child1_value)xmlFree(child1_value);
    if(!child1_level)xmlFree(child1_level);
    if(!child1_id)xmlFree(child1_id);
    if(!child1_status)xmlFree(child1_status);
  }
  xmlFreeDoc(doc);

  for(Int_t i=0; i<N_VFE; i++)
  {
    sprintf(widget_name,"0_VFE_%d",i+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.VFE_status[i]);
  }
  printf("Will use I2C_shift_dev_number %d\n",daq.I2C_shift_dev_number);
  update_trigger();

// Position current channel on first active one or channel 1 :
  daq.current_channel=-1;
  for(Int_t i=0; i<N_CHANNEL; i++)
  {
    if(asic.channel_status[i] && daq.current_channel<0)
    {
      daq.current_channel=i+1;
      break;
    }
  }
}

void write_conf_to_xml_file(char *filename)
{
  char status[80];

  FILE*fd_out=fopen(filename,"w+");
  fprintf(fd_out,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  fprintf(fd_out,"<safe>\n");
  fprintf(fd_out,"  <object name=\"DAQ\">\n");
  fprintf(fd_out,"    <property name=\"SAFE\" value=\"%d\"></property>\n",daq.SAFE_number);
  sprintf(status,"OFF");
  if(daq.use_LMB) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"use_LMB\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.LV_ON) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"LV\" status=\"%s\"></property>\n",status);
  fprintf(fd_out,"    <property name=\"resync_phase\" value=\"%d\"></property>\n",daq.resync_phase);
  fprintf(fd_out,"    <property name=\"test_mode\" value=\"%d\"></property>\n",daq.DTU_test_mode);
  fprintf(fd_out,"    <property name=\"I2C_shift_dev_number\" value=\"%d\"></property>\n",daq.I2C_shift_dev_number);
  sprintf(status,"OFF");
  if(daq.debug_DAQ) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"debug_DAQ\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.debug_I2C) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"debug_I2C\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.debug_GTK) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"debug_GTK\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.debug_draw) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"debug_draw\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.zoom_draw) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"zoom_draw\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.corgain) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"corgain\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.wait_for_ever) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"wait_for_ever\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.I2C_lpGBT_mode) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"I2C_lpGBT\" status=\"%s\"></property>\n",status);
  fprintf(fd_out,"    <property name=\"HW_delay\" value=\"%d\"></property>\n",daq.hw_DAQ_delay);
  fprintf(fd_out,"    <property name=\"SW_delay\" value=\"%d\"></property>\n",daq.sw_DAQ_delay);
  fprintf(fd_out,"    <property name=\"TP_delay\" value=\"%d\"></property>\n",daq.TP_delay);
  fprintf(fd_out,"    <property name=\"TP_width\" value=\"%d\"></property>\n",daq.TP_width);
  fprintf(fd_out,"    <property name=\"TP_level\" value=\"%d\"></property>\n",daq.TP_level);
  fprintf(fd_out,"    <property name=\"TP_step\" value=\"%d\"></property>\n",daq.TP_step);
  fprintf(fd_out,"    <property name=\"n_TP_step\" value=\"%d\"></property>\n",daq.n_TP_step);
  fprintf(fd_out,"    <property name=\"nevent\" value=\"%d\"></property>\n",daq.nevent);
  fprintf(fd_out,"    <property name=\"nsample\" value=\"%d\"></property>\n",daq.nsample);
  sprintf(status,"OFF");
  if(daq.gen_BC0) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"gen_BC0\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.gen_WTE) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"gen_WTE\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.CATIA_scan_Vref) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"CATIA_scan_Vref\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.CATIA_Vcal_out) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"CATIA_Vcal_out\" status=\"%s\"></property>\n",status);
  fprintf(fd_out,"    <property name=\"Tsensor_divider\" value=\"%.3f\"></property>\n",daq.Tsensor_divider);
  sprintf(status,"OFF");
  if(asic.DTU_override_Vc_bit[0]==1) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"PLL_override_Vc_bit\" status=\"%s\"></property>\n",status);
  sprintf(status,"OFF");
  if(daq.MEM_mode) sprintf(status,"ON");
  fprintf(fd_out,"    <property name=\"use_MEM_mode\" status=\"%s\"></property>\n",status);
  fprintf(fd_out,"    <property name=\"CATIA_reg1_def\" value=\"0x%2.2x\"></property>\n",asic.CATIA_reg_def[0][1]);
  fprintf(fd_out,"    <property name=\"CATIA_reg2_def\" value=\"0x%2.2x\"></property>\n",asic.CATIA_reg_def[0][2]);
  fprintf(fd_out,"    <property name=\"CATIA_reg3_def\" value=\"0x%4.4x\"></property>\n",asic.CATIA_reg_def[0][3]);
  fprintf(fd_out,"    <property name=\"CATIA_reg4_def\" value=\"0x%4.4x\"></property>\n",asic.CATIA_reg_def[0][4]);
  fprintf(fd_out,"    <property name=\"CATIA_reg5_def\" value=\"0x%4.4x\"></property>\n",asic.CATIA_reg_def[0][5]);
  fprintf(fd_out,"    <property name=\"CATIA_reg6_def\" value=\"0x%2.2x\"></property>\n",asic.CATIA_reg_def[0][6]);
  fprintf(fd_out,"    <property name=\"ADC_dv\" value=\"%.5f\"></property>\n",asic.ADC_dv);
  fprintf(fd_out,"  </object>\n");
  for(Int_t ivfe=0; ivfe<5; ivfe++)
  {
    sprintf(status,"OFF");
    if(asic.VFE_status[ivfe]) sprintf(status,"ON");
    fprintf(fd_out,"  <object name=\"VFE\" id=\"%d\" status=\"%s\">\n",ivfe+1,status);
    for(Int_t i=0; i<5; i++)
    {
      Int_t ich=ivfe*5+i;
      sprintf(status,"OFF");
      if(asic.channel_status[ich]) sprintf(status,"ON");
      fprintf(fd_out,"    <object name=\"channel\" id=\"%d\" status=\"%s\">\n",i+1,status);
      fprintf(fd_out,"      <property name=\"I2C_bus\" value=\"%d\"></property>\n",asic.I2C_bus[ich]);
      fprintf(fd_out,"      <property name=\"I2C_address\" value=\"%d\"></property>\n",asic.I2C_address[ich]);
      sprintf(status,"OFF");
      if(asic.CATIA_status[ich]) sprintf(status,"ON");
      fprintf(fd_out,"      <object name=\"CATIA\" status=\"%s\">\n",status);
      fprintf(fd_out,"        <property name=\"version\" value=\"%d\"></property>\n",asic.CATIA_version[ich]);
      fprintf(fd_out,"        <property name=\"XADC_channel\" value=\"%d\"></property>\n",asic.CATIA_XADC_channel[ich]);
      fprintf(fd_out,"        <property name=\"ped_G10\" value=\"%d\"></property>\n",asic.CATIA_ped_G10[ich]);
      fprintf(fd_out,"        <property name=\"ped_G1\" value=\"%d\"></property>\n",asic.CATIA_ped_G1[ich]);
      fprintf(fd_out,"        <property name=\"Vref_target\" value=\"%.2f\"></property>\n",asic.CATIA_Vref_ref[ich]);
      fprintf(fd_out,"        <property name=\"Vref\" value=\"%d\"></property>\n",asic.CATIA_Vref_val[ich]);
      fprintf(fd_out,"        <property name=\"gain\" value=\"%d\"></property>\n",asic.CATIA_gain[ich]);
      fprintf(fd_out,"        <property name=\"LPF35\" value=\"%d\"></property>\n",asic.CATIA_LPF35[ich]);
      sprintf(status,"OFF");
      if(asic.CATIA_DAC1_status[ich]) sprintf(status,"ON");
      fprintf(fd_out,"        <property name=\"DAC1\" status=\"%s\" value=\"%d\"></property>\n",status,asic.CATIA_DAC1_val[ich]);
      sprintf(status,"OFF");
      if(asic.CATIA_DAC2_status[ich]) sprintf(status,"ON");
      fprintf(fd_out,"        <property name=\"DAC2\" status=\"%s\" value=\"%d\"></property>\n",status,asic.CATIA_DAC2_val[ich]);
      sprintf(status,"OFF");
      if(asic.CATIA_SEU_corr[ich]) sprintf(status,"ON");
      fprintf(fd_out,"        <property name=\"SEU_corr\" status=\"%s\"></property>\n",status);
      fprintf(fd_out,"        <property name=\"Temp_offset\" value=\"%.3f\"></property>\n",asic.CATIA_Temp_offset[ich]);
      fprintf(fd_out,"      </object>\n");
      sprintf(status,"OFF");
      if(asic.DTU_status[ich]) sprintf(status,"ON");
      fprintf(fd_out,"      <object name=\"LiTEDTU\" status=\"%s\">\n",status);
      fprintf(fd_out,"        <property name=\"version\" value=\"%d\"></property>\n",asic.DTU_version[ich]);
      fprintf(fd_out,"        <property name=\"BL_G10\" value=\"%d\"></property>\n",asic.DTU_BL_G10[ich]);
      fprintf(fd_out,"        <property name=\"BL_G1\" value=\"%d\"></property>\n",asic.DTU_BL_G1[ich]);
      sprintf(status,"OFF");
      if(asic.DTU_force_G10[ich]) sprintf(status,"ON");
      fprintf(fd_out,"        <property name=\"force_G10\" status=\"%s\"></property>\n",status);
      sprintf(status,"OFF");
      if(asic.DTU_force_G1[ich]) sprintf(status,"ON");
      fprintf(fd_out,"        <property name=\"force_G1\" status=\"%s\"></property>\n",status);
      sprintf(status,"OFF");
      if(asic.DTU_force_PLL[ich]) sprintf(status,"ON");
      fprintf(fd_out,"        <property name=\"force_PLL\" status=\"%s\" value=\"0x%x\"></property>\n",status,asic.DTU_PLL_conf[ich]);
      sprintf(status,"OFF");
      if(asic.DTU_clk_out[ich]) sprintf(status,"ON");
      fprintf(fd_out,"        <property name=\"PLL_clock_out\" status=\"%s\"></property>\n",status);
      sprintf(status,"OFF");
      if(asic.DTU_VCO_rail_mode[ich]) sprintf(status,"ON");
      fprintf(fd_out,"        <property name=\"VCO_rail_mode\" status=\"%s\"></property>\n",status);
      sprintf(status,"OFF");
      if(asic.DTU_bias_ctrl_override[ich]) sprintf(status,"ON");
      fprintf(fd_out,"        <property name=\"bias_ctrl_override\" status=\"%s\"></property>\n",status);
      fprintf(fd_out,"        <property name=\"driver_current\" value=\"%d\"></property>\n",asic.DTU_driver_current[ich]);
      fprintf(fd_out,"        <property name=\"PE_width\" value=\"%d\"></property>\n",asic.DTU_PE_width[ich]);
      fprintf(fd_out,"        <property name=\"PE_strength\" value=\"%d\"></property>\n",asic.DTU_PE_strength[ich]);
      fprintf(fd_out,"      </object>\n");
      fprintf(fd_out,"    </object>\n");
    }
    fprintf(fd_out,"  </object>\n");
  }
  fprintf(fd_out,"</safe>\n");
  fclose(fd_out);
}
