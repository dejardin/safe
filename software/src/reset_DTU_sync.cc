#define EXTERN extern
#include "gdaq_SAFE.h"
void reset_DTU_sync(void)
{
  uhal::HwInterface hw=devices.front();
  UInt_t command=
    SELF_TRIGGER_MODE      *daq.self_trigger_mode               |
    (SELF_TRIGGER_THRESHOLD*(daq.self_trigger_threshold&0x1FFF))|
    SELF_TRIGGER           *daq.self_trigger                    |
    SELF_TRIGGER_LOOP      *daq.self_trigger_loop               |
    FIFO_MODE              *(daq.fifo_mode&1)                   | // Always DAQ on trigger
    RESET                  *0;
  hw.getNode("SAFE_CTRL").write(command);
  hw.getNode("TRIGGER_MASK").write(daq.self_trigger_mask&0x1FFFFFF);
  hw.dispatch();
  command=DELAY_TAP_DIR*1 | DELAY_RESET*1;
  hw.getNode("DELAY_CTRL1").write(command);
  hw.dispatch();
  printf("Get lock status of IDELAY input stages : ");
  ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL1").read();
  hw.dispatch();
  printf("0x%8.8x\n",reg.value());
  for(Int_t i=0; i<N_CHANNEL; i++)
  {
    daq.delay_val[i]=0;
    daq.bitslip_val[i]=0;
    daq.byteslip_val[i]=0;
  }
}
