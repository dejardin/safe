#define EXTERN extern
#include "gdaq_SAFE.h"

void ana_ped_Monacal(void)
{
  Int_t NMSPS=160;
  if(daq.MEM_mode)NMSPS=80.;
  Double_t noise_max=5.;
  Int_t LPF=0;
  Double_t G12=856.;
  Double_t G1 =70.;
  Int_t loc_ch[N_CHANNEL]={ 5,10,15,20,25,
                           24,19,14, 9, 4,
                            3, 8,13,18,23,
                           22,17,12, 7, 2,
                            1, 6,11,16,21};


  char hname[256];
  printf("Opening file %s for analysis\n",daq.last_fname);
  TFile *infile=new TFile(daq.last_fname);
  Int_t loc_gain;
  sscanf(daq.last_fname,"data/ped_data_G%d",&loc_gain);
  printf("Gain found : %d\n",loc_gain);
  if(loc_gain==10)noise_max*=2.;
  printf("Getting tree\n");
  TTree *tdata=(TTree*)infile->Get("tdata");
// Try to get frame size :
  char title[80];
  sprintf(title,"%s",tdata->GetBranch("ch0")->GetTitle());
  int nsample=0;
  sscanf(title,"ch0[%d]/S",&nsample);
  printf("Setting branches for %d samples, ",nsample);
  ULong64_t timestamp;
  int id;
  short int *sevent[N_CHANNEL];
  for(int ich=0; ich<N_CHANNEL; ich++){sevent[ich]=(short int*) malloc(nsample*sizeof(short int));}
  Double_t *event[N_CHANNEL];
  for(int ich=0; ich<N_CHANNEL; ich++){event[ich]=(double*) malloc(nsample*sizeof(double));}

  tdata->SetBranchAddress("timestamp",&timestamp);
  for(Int_t ich=0; ich<N_CHANNEL; ich++)
  {
    char bname[80];
    sprintf(bname,"ch%d",ich);
    tdata->SetBranchAddress(bname,sevent[ich]);
    asic.rms[1-loc_gain/10][ich]=0.;
  }
  int nevt=(Int_t)tdata->GetEntries();
  gettimeofday(&tv,NULL);

  if(daq.MEM_mode)nsample=nsample/5*2;
  printf("with only %d samples used\n",nsample);
  Double_t pi=2.l*asin(1.l);
  Double_t dt=1.e-6/NMSPS;
  Double_t tmax=nsample*dt;
  Double_t fmax=1/dt;
  Double_t df=1./tmax;
  Double_t dv=1.200/4096.;
  printf("dt %e, tmax %e, fmax %e, df %e, dv %e\n",dt,tmax,fmax,df,dv);
  gStyle->SetOptStat(0);

// Cut-off frequency :
  Int_t BW_order=LPF/100;
  Double_t BW_ref=(LPF-BW_order*100)/10.*1.e6;
  Double_t BW_eff=2./dt*tan(BW_ref*2.*pi*dt/2.)/2./pi;
  Double_t oc=2.*pi*BW_eff;
  Double_t g1=oc*(dt/2.);
  Double_t g2=g1*g1;
  Double_t g4=g2*g2;
  printf("%dth order Butterworth filter: requested cut-off frequency : %.2f MHz, effective cutoff frequency : %.2f MHz\n",BW_order,BW_ref/1.e6,BW_eff/1.e6);

// Butterworth filter order 1
// B(s)=1/((s/oc)+1)
// s=(2/dt)*(1-z-1)/(1+z-1)
// B(z)=g*(n0+n1*z)/(d0+d1*z)
  Double_t bw1_n0=1.;
  Double_t bw1_n1=1.;
  Double_t bw1_d0=g1+1.;
  Double_t bw1_d1=g1-1.;

// Butterworth filter order 2
// B(s)=1/((s/oc)^2+sqrt(2)*(s/oc)+1)
// s=(2/dt)*(1-z-1)/(1+z-1)
// B(z)=g^2*(n0+n1*z-1+n2*z-2)/(d0+d1*z-1+d2*z-2)
  Double_t o1=sqrt(2), o2=0.;
  Double_t bw2_n0=1.;
  Double_t bw2_n1=2.;
  Double_t bw2_n2=1.;
  Double_t bw2_d0=1.+o1*g1+g2;
  Double_t bw2_d1=2.*(g2-1.);
  Double_t bw2_d2=1.-o1*g1+g2;

// Butterworth filter order 4
// B(s)=1/[((s/oc)^2+o1*(s/oc)+1)*((s/oc)^2+o2*(s/oc)+1)]
// B(z)=g^4*(n0+n1*z-1+n2*z-2+n3*z-3+n4*z-4)/(d0+d1*z-1+d2*z-2+d3*z-3+d4*z-4)
  o1=0.7654; o2=1.8478;
  Double_t bw4_n0=1.;
  Double_t bw4_n1=4.;
  Double_t bw4_n2=6.;
  Double_t bw4_n3=4.;
  Double_t bw4_n4=1.;
  Double_t bw4_a1=1+o1*g1+g2;
  Double_t bw4_b1=2.*(g2-1.);
  Double_t bw4_c1=1-o1*g1+g2;
  Double_t bw4_a2=1+o2*g1+g2;
  Double_t bw4_b2=2.*(g2-1.);
  Double_t bw4_c2=1-o2*g1+g2;
  Double_t bw4_d0=bw4_a1*bw4_a2;
  Double_t bw4_d1=bw4_a1*bw4_b2+bw4_b1*bw4_a2;
  Double_t bw4_d2=bw4_a1*bw4_c2+bw4_b1*bw4_b2+bw4_c1*bw4_a2;
  Double_t bw4_d3=bw4_b1*bw4_c2+bw4_c1*bw4_b2;
  Double_t bw4_d4=bw4_c1*bw4_c2;

  Double_t y4=0.,y3=0.,y2=0.,y1=0.,y0=0.;
  Double_t x4=0.,x3=0.,x2=0.,x1=0.,x0=0.;

// Step response BW1 :
  Double_t yref1=1.;
  for(int i=0; i<200; i++)
  {
    x0=0.;
    if(i>10)x0=1.;
    y0=g1/bw1_d0*(bw1_n0*x0+bw1_n1*x1)-bw1_d1/bw1_d0*y1;
    x1=x0;
    y1=y0;
    yref1=y0;
    printf("%.2f ",y0);
  }
  printf("\nButterworth1 total gain : %f\n",yref1);

// Step response BW2 :
  y4=0.; y3=0.; y2=0.; y1=0., y0=0.;
  x4=0.; x3=0.; x2=0.; x1=0., x0=0.;
  Double_t yref2=1.;
  for(int i=0; i<200; i++)
  {
    x0=0.;
    if(i>10)x0=1.;
    y0=g2/bw2_d0*(bw2_n0*x0+bw2_n1*x1+bw2_n2*x2)-bw2_d1/bw2_d0*y1-bw2_d2/bw2_d0*y2;
    x2=x1; x1=x0;
    y2=y1; y1=y0;
    yref2=y0;
    printf("%.2f ",y0);
  }
  printf("\nButterworth2 total gain : %f\n",yref2);

  y4=0.; y3=0.; y2=0.; y1=0., y0=0.;
  x4=0.; x3=0.; x2=0.; x1=0., x0=0.;
// Step response :
  Double_t yref4=1.;
  for(int i=0; i<200; i++)
  {
    x0=0.;
    if(i>10)x0=1.;
    y0=g4/bw4_d0*(bw4_n0*x0+bw4_n1*x1+bw4_n2*x2+bw4_n3*x3+bw4_n4*x4)-bw4_d1/bw4_d0*y1-bw4_d2/bw4_d0*y2-bw4_d3/bw4_d0*y3-bw4_d4/bw4_d0*y4;
    x4=x3; x3=x2; x2=x1; x1=x0;
    y4=y3; y3=y2; y2=y1; y1=y0;
    yref4=y0;
    printf("%.2f ",y0);
  }
  printf("\nButterworth4 total gain : %f, %f\n",yref4,g4*(bw4_n0+bw4_n1+bw4_n2+bw4_n3+bw4_n4)/(bw4_d0+bw4_d1+bw4_d2+bw4_d3+bw4_d4));

  TGraph *tg_timestamp = new TGraph();
  tg_timestamp->SetTitle("gtimestamp");
  tg_timestamp->SetName("gtimestamp");
  TGraph *tg[N_CHANNEL];
  TH1D *hmean[N_CHANNEL], *hrms[N_CHANNEL], *hped[N_CHANNEL];
  TProfile *pmod[N_CHANNEL];
  TH1D *hmod[N_CHANNEL],*hmod_folded[N_CHANNEL],*hmod_loc[N_CHANNEL];
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    tg[ich]->SetMarkerColor(kRed);
    tg[ich]->SetLineColor(kRed);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
    sprintf(hname,"ped_ch%d",ich);
    hped[ich]=new TH1D(hname,hname,100,-5.,5.);
    sprintf(hname,"pmod_ch%d",ich);
    pmod[ich]=new TProfile(hname,hname,nsample,0.,fmax);
    pmod[ich]->SetLineColor(kBlue);
    sprintf(hname,"mod_ch%d",ich);
    hmod[ich]=new TH1D(hname,hname,nsample/2,0.,fmax/2.);
    hmod[ich]->SetLineColor(kBlue);
    sprintf(hname,"mod_ch%d_folded",ich);
    hmod_folded[ich]=new TH1D(hname,hname,nsample/2,0.,fmax/2.);
    hmod_folded[ich]->SetLineColor(kCyan);
    sprintf(hname,"mod_loc_ch%d",ich);
    hmod_loc[ich]=new TH1D(hname,hname,nsample,0.,fmax);
    hmod_loc[ich]->SetLineColor(kRed);
  }

  Double_t delta_t=0., old_t=-1.;
  //nevt=2;
  Double_t qmax_max=0.;
  printf("Start analysis of %d events\n",nevt);

  Double_t *rex=(double *)malloc(nsample*sizeof(double));
  Double_t *imx=(double *)malloc(nsample*sizeof(double));
  Double_t *rex_bw4=(double *)malloc(nsample*sizeof(double));
  Double_t *imx_bw4=(double *)malloc(nsample*sizeof(double));
  Double_t *rey=(double *)malloc(nsample*sizeof(double));
  Double_t *imy=(double *)malloc(nsample*sizeof(double));
  Double_t *rey_bw4=(double *)malloc(nsample*sizeof(double));
  Double_t *imy_bw4=(double *)malloc(nsample*sizeof(double));
  Double_t *mod=(double *)malloc(nsample*sizeof(double));
  Double_t *phase=(double *)malloc(nsample*sizeof(double));
  Double_t *mod_bw4=(double *)malloc(nsample*sizeof(double));
  Double_t *phase_bw4=(double *)malloc(nsample*sizeof(double));

  int size = nsample;
  TVirtualFFT *fft_f = TVirtualFFT::FFT(1, &size, "C2CF M K");
  TCanvas *cmod;
  TCanvas *cframe=new TCanvas("sample_frame","sample frame",1000,0,700,500);
  sprintf(hname,"noise_spectrum");
  cmod=new TCanvas(hname,hname,800,800);
  cmod->Divide(5,5);
  cmod->Update();

  TF1 *fexpo=new TF1("fexpo","[0]+[1]*exp(-x*[2])",-1.e-3,1.e-3);
  TF1 *fpol1=new TF1("fpol1","[0]+[1]*x",-0.,1.e-3);
  fexpo->SetNpx(10000);
  Double_t rms[N_CHANNEL], hf_rms[N_CHANNEL], vhf_rms[N_CHANNEL];
  for(int ievt=0; ievt<nevt; ievt++)
  {
    if((ievt%100)==0) printf("analyse evt : %d\n",ievt);
    tdata->GetEntry(ievt);
    //if(daq.debug_DAQ)printf("timestamp : %16.16llx\n",timestamp);
    Double_t t_event=6.25e-9*timestamp;
    if(old_t>0.) delta_t=t_event-old_t;
    old_t=t_event;
    tg_timestamp->SetPoint(ievt,t_event,delta_t);

    //printf("event %d, rms = ",ievt);
    for(int i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i]-1;
      Double_t mean=0.;
      for(int is=0; is<nsample; is++)
      {
        event[ich][is]=dv*sevent[ich][is];
        mean+=event[ich][is];
      }
      mean/=nsample;
      rms[ich]=0.;
      Double_t first_val=0., last_val=0.;
      for(int is=0; is<nsample; is++)
      {
        event[ich][is]-=mean;
        rex[is]=event[ich][is];
        imx[is]=0.;
        rms[ich]+=event[ich][is]*event[ich][is];
        tg[ich]->SetPoint(is,dt*is,event[ich][is]);
        if(is==0)        first_val=event[ich][is];
        if(is==nsample-1)last_val =event[ich][is];
      }

// BW1 :
      if(LPF>0 && BW_order==1)
      {
        y1=0.; y0=0.;
        x1=0.; x0=0.;
        for(int is=0; is<nsample; is++)
        {
          Double_t x0=rex[is];
          y0=g1/bw1_d0*(bw1_n0*x0+bw1_n1*x1)-bw1_d1/bw1_d0*y1;
          x1=x0;
          y1=y0;
          rex[is]=y0;
        }
      }

// BW2 :
      if(LPF>0 && BW_order==2)
      {
        y2=0.; y1=0.; y0=0.;
        x2=0.; x1=0.; x0=0.;
        for(int is=0; is<nsample; is++)
        {
          Double_t x0=rex[is];
          y0=g2/bw2_d0*(bw2_n0*x0+bw2_n1*x1+bw2_n2*x2)-bw2_d1/bw2_d0*y1-bw2_d2/bw2_d0*y2;
          x2=x1; x1=x0;
          y2=y1; y1=y0;
          rex[is]=y0;
        }
      }

// BW4 :
      if(LPF>0 && BW_order==4)
      {
        y4=0.; y3=0.; y2=0.; y1=0.; y0=0.;
        x4=0.; x3=0.; x2=0.; x1=0.; x0=0.;
        for(int is=0; is<nsample; is++)
        {
          Double_t x0=rex[is];
          y0=g4/bw4_d0*(bw4_n0*x0+bw4_n1*x1+bw4_n2*x2+bw4_n3*x3+bw4_n4*x4)-bw4_d1/bw4_d0*y1-bw4_d2/bw4_d0*y2-bw4_d3/bw4_d0*y3-bw4_d4/bw4_d0*y4;
          x4=x3; x3=x2; x2=x1; x1=x0;
          y4=y3; y3=y2; y2=y1; y1=y0;
          rex[is]=y0;
        }
      }

      rms[ich]=sqrt(rms[ich]/nsample);
      //if(rms[ich]>1.8e-3)continue;
      fft_f->SetPointsComplex(rex, imx);
      fft_f->Transform();
      fft_f->GetPointsComplex(rey, imy);
      rms[ich]=0.;
      hf_rms[ich]=0.;
      vhf_rms[ich]=0.;
      for(int i=0; i<nsample; i++)
      {
        Double_t f=df*i;
        rey[i]/=nsample;
        imy[i]/=nsample;
        mod[i]=rey[i]*rey[i]+imy[i]*imy[i]; 
        int ibad=0;
        //if(f>37e6 && f<40e6)ibad=1;
        //if(f>74e6 && f<86e6)ibad=1;
        //if(f>120e6 && f<123e6)ibad=1;
        if(ibad==0)
        if(i>0)rms[ich]+=mod[i];
        if(f>250000. && f<fmax-250000.) hf_rms[ich]+=mod[i];
        if(f>1500000. && f<fmax-1500000.) vhf_rms[ich]+=mod[i];
        mod[i]=sqrt(mod[i])/sqrt(df); // [V/sqrt(Hz)]
        phase[i]=atan2(imy[i],rey[i]);
        hmod_loc[ich]->SetBinContent(i+1,mod[i]);
      }
      rms[ich]=sqrt(rms[ich]);
      hf_rms[ich]=sqrt(hf_rms[ich]);
      vhf_rms[ich]=sqrt(vhf_rms[ich]);
      for(int i=0; i<nsample; i++)
      {
        Double_t f=df*i;
        //if(rms[ich]<2.0e-3)pmod[ich]->Fill(df*i,mod[i]);
        pmod[ich]->Fill(f,mod[i]*mod[i]); // We sum the power spectra
      }
      //printf("%.3e %.3e %.3e, ",rms[ich],hf_rms[ich], vhf_rms[ich]);
      //cmod->cd(loc_ch[ich]);
      //hmod_loc[ich]->Draw();
      //hmod_loc[ich]->GetXaxis()->SetRangeUser(1.e4,fmax/2.);
      //hmod_loc[ich]->SetMinimum(0.);
      //hmod_loc[ich]->SetMaximum(300.);
      //gPad->SetGridx();
      //gPad->SetGridy();
      //gPad->SetLogx();
      //pmod[ich]->Draw("same");
      //if(ich==4)cmod->Update();
    }
    //printf("\n");
    if(daq.debug_DAQ && ievt==0)
    {
      cframe->cd();
      tg[daq.debug_DAQ]->SetMaximum(0.005);
      tg[daq.debug_DAQ]->SetMinimum(-.005);
      tg[daq.debug_DAQ]->Draw("alp");
      gPad->SetGridx();
      gPad->SetGridy();
      tg[daq.debug_DAQ]->GetXaxis()->SetTitle("time [ns]");
      tg[daq.debug_DAQ]->GetYaxis()->SetTitle("amplitude [mV]");
      cframe->Update();
      //return;
    }
  }
  printf("final, rms (uV) = ");
  //TLatex *tex=new TLatex();
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    Double_t mod;
    rms[ich]=0.;
    hf_rms[ich]=0.;
    vhf_rms[ich]=0.;
    for(int is=0; is<=nsample/2; is++)
    {
      Double_t f=df*is;
      mod=pmod[ich]->GetBinContent(is+1);
      hmod[ich]->SetBinContent(is+1,sqrt(mod*2.)*1.e6); // We fold positive and negative frequencies and convert to uV/sqrt(Hz)
      hmod_folded[ich]->Fill(f,mod*2.); // We fold positive and negative frequencies
      if(is>0)rms[ich]+=mod*df*2.;
      if(f>250000.) hf_rms[ich]+=mod*df*2.;
      if(f>1500000.) vhf_rms[ich]+=mod*df*2.;
    }
    for(int is=0; is<=nsample/2; is++)
    {
      Double_t mod=hmod_folded[ich]->GetBinContent(is+1);
      hmod_folded[ich]->SetBinContent(is+1,sqrt(mod)*1.e6); // uV/sqrt(Hz)
      hmod_folded[ich]->SetBinError(is+1,0.);
    }
    rms[ich]=sqrt(rms[ich]);
    asic.rms[1-loc_gain/10][ich]=rms[ich];
    hf_rms[ich]=sqrt(hf_rms[ich]);
    vhf_rms[ich]=sqrt(vhf_rms[ich]);
    printf("%.3e %.3e %.3e, ",rms[ich]*1.e6,hf_rms[ich]*1.e6,vhf_rms[ich]*1.e6);
    cmod->cd(loc_ch[ich]);
    cmod->Update();
    hmod[ich]->Draw();
    //hmod_folded[ich]->Draw("same");
    sprintf(title,"NDS ch %d",ich);
    hmod[ich]->SetTitle(title);
    hmod[ich]->SetTitleSize(0.05);
    hmod[ich]->SetTitleFont(62);
    hmod[ich]->GetXaxis()->SetRangeUser(1.e4,fmax/2.);
    hmod[ich]->GetXaxis()->SetTitle("frequency [Hz]");
    hmod[ich]->GetXaxis()->SetTitleSize(0.05);
    hmod[ich]->GetXaxis()->SetLabelSize(0.05);
    hmod[ich]->GetXaxis()->SetTitleFont(62);
    hmod[ich]->GetXaxis()->SetLabelFont(62);
    hmod[ich]->GetYaxis()->SetTitle("noise density [nV/#sqrt{Hz}]");
    hmod[ich]->GetYaxis()->SetTitleSize(0.05);
    hmod[ich]->GetYaxis()->SetLabelSize(0.05);
    hmod[ich]->GetYaxis()->SetTitleOffset(1.1);
    hmod[ich]->GetYaxis()->SetTitleFont(62);
    hmod[ich]->GetYaxis()->SetLabelFont(62);
    hmod[ich]->SetMinimum(0.);
    hmod[ich]->SetMaximum(noise_max);
    sprintf(title,"Total noise = %.0f #muV",rms[ich]*1.e6);
    //tex->DrawLatexNDC(0.5,0.85,title);
    sprintf(title,"HF noise     = %.0f #muV",hf_rms[ich]*1.e6);
    //tex->DrawLatexNDC(0.5,0.80,title);
    //sprintf(title,"VHF noise = %.0f #muV",vhf_rms[ich]*1.e6);
    //tex->DrawLatexNDC(0.5,0.75,title);
    gPad->SetGridx();
    gPad->SetGridy();
    gPad->SetLogx();
    gPad->Modified();
    cmod->Update();
  }
  printf("\n");
  printf("final, rms (fC) = ");
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    Double_t Gain=G12;
    printf("%.3e %.3e %.3e, ",rms[ich]/Gain*1.e9,hf_rms[ich]/Gain*1.e9,vhf_rms[ich]/Gain*1.e9);
  }
  printf("\n");
  sprintf(hname,"%s",daq.last_fname);
  char *pos=strstr(hname,".root");
  sprintf(pos,"_analized.root");
  printf("Output file : %s\n",hname);
  TFile *tfout=new TFile(hname,"recreate");
  cmod->Write();
  sprintf(pos,"_analized.txt");
  FILE *fout=fopen(hname,"w+");
  fprintf(fout,"%ld.%6.6ld : %s\n",tv.tv_sec,tv.tv_usec,daq.comment);
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    fprintf(fout,"%.3e %.3e ",rms[ich],hf_rms[ich]);
    hmod[ich]->Write();
    hmod_folded[ich]->Write();
  }
  fprintf(fout,"\n");
  for(int is=0; is<=nsample/2; is++)
  {
    Double_t f=df*is;
    fprintf(fout,"%e",f);
    for(int i=0; i<daq.n_active_channel; i++)
    {
      Int_t ich=daq.channel_number[i]-1;
      Double_t mod=hmod[ich]->GetBinContent(is+1);
      fprintf(fout," %e",mod);
    }
    fprintf(fout,"\n");
  }
  tfout->Close();
  fclose(fout);
  
  for(int ich=0; ich<N_CHANNEL; ich++){free((void*)sevent[ich]);}
  for(int ich=0; ich<N_CHANNEL; ich++){free((void*)event[ich]);}
  free((void*)rex);
  free((void*)imx);
  free((void*)rex_bw4);
  free((void*)imx_bw4);
  free((void*)rey);
  free((void*)imy);
  free((void*)rey_bw4);
  free((void*)imy_bw4);
  free((void*)mod);
  free((void*)phase);
  free((void*)mod_bw4);
  free((void*)phase_bw4);
  usleep(1000000);
  //printf("OK to continue ? ");
  //system("stty raw");
  //char cdum=getchar();
  //system("stty -raw");
  //printf("\n ");
  tdata->~TTree();
  tfout->~TFile();
  cmod->~TCanvas();
  cframe->~TCanvas();
  tg_timestamp->~TGraph();
  for(int i=0; i<daq.n_active_channel; i++)
  {
    Int_t ich=daq.channel_number[i]-1;
    tg[ich]->~TGraph();
    hmean[ich]->~TH1D();
    hrms[ich]->~TH1D();
    hped[ich]->~TH1D();
    hmod[ich]->~TH1D();
    hmod_folded[ich]->~TH1D();
    hmod_loc[ich]->~TH1D();
    pmod[ich]->~TProfile();
  }
  fft_f->~TVirtualFFT();
  fexpo->~TF1();
  fpol1->~TF1();
  infile->Close();
  infile->~TFile();
}
