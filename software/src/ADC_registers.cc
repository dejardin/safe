#define EXTERN extern
#include "gdaq_SAFE.h"
void dump_ADC_registers(void)
{
  Int_t ADC_reg_val[2][N_ADC_REG];
  UInt_t device_number;
  uhal::HwInterface hw=devices.front();
  FILE *fcal=fopen("last_VFE_calib_reg.dat","w+");
  for(int ich=0; ich<N_CHANNEL; ich++)
  {
    Int_t bus=asic.I2C_bus[ich];
	for(int iADC=0; iADC<2; iADC++)
	{
      Int_t num=asic.I2C_address[ich];
	  device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+iADC;
	  for(int ireg=0; ireg<N_ADC_REG; ireg++)
	  {
		ADC_reg_val[iADC][ireg]=I2C_RW(hw, device_number, ireg, 0, 0, 2, daq.debug_I2C);
	  }
	}
	for(int ireg=0; ireg<N_ADC_REG; ireg++)
	{
	  printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
	  fprintf(fcal,"%d %d\n",ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
	}
  }
  fclose(fcal);
}
void load_ADC_registers(void)
{
  Int_t ADC_reg_val[2][N_ADC_REG];
  UInt_t device_number, iret;
  uhal::HwInterface hw=devices.front();
  FILE *fcal=fopen("last_VFE_calib_reg.dat","r");
  for(int ich=0; ich<N_CHANNEL; ich++)
  {
    Int_t bus=asic.I2C_bus[ich];
    for(int ireg=0; ireg<N_ADC_REG; ireg++)
    {
      fscanf(fcal,"%d %d\n",&ADC_reg_val[0][ireg], &ADC_reg_val[1][ireg]);
	  printf("Register %d : 0x%2.2x 0x%2.2x\n",ireg,ADC_reg_val[0][ireg]&0xff, ADC_reg_val[1][ireg]&0xff);
    }
    for(int iADC=0; iADC<2; iADC++)
    {
      Int_t num=asic.I2C_address[ich];
      device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+iADC;
      for(int ireg=0; ireg<N_ADC_REG; ireg++)
      {
        iret=I2C_RW(hw, device_number, ireg, ADC_reg_val[iADC][ireg], 0, 1, daq.debug_I2C);
      }
    }
  }
}
