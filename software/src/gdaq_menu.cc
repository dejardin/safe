#define EXTERN extern
#include <gtk/gtk.h>
#include <TSystem.h>
#include "gdaq_SAFE.h"

void callback_about (GtkMenuItem *menuitem, gpointer user_data)
{
  GtkWidget *dialog = NULL;

  dialog =  gtk_about_dialog_new ();

  /* Pour l'exemple on va rendre la fenêtre a propos" modale par rapport a la fenêtre principale. */
  gtk_window_set_transient_for (GTK_WINDOW(dialog), GTK_WINDOW(gtk_builder_get_object (gtk_data.builder, "MainWindow")));

  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

void delete_event(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Signal delete !\n");
  end_of_run();
}

void my_exit(GtkMenuItem *menuitem, gpointer user_data)
{
  printf("Signal destroy !\n");
  end_of_run();
}

void select_VFE(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget=NULL;
  char widget_name[80];
  Int_t ivfe;
  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  sscanf(name,"0_VFE_%d",&ivfe);
  for(Int_t ich=(ivfe-1)*5; ich<ivfe*5; ich++)
  {
    sprintf(widget_name,"0_channel_%d",ich+1);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
    g_assert (widget);
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      asic.VFE_status[ivfe-1]=TRUE;
      gtk_widget_set_sensitive(widget,TRUE);
      if(asic.channel_status[ich])
        gtk_widget_set_name(widget,"green_button");
      else
        gtk_widget_set_name(widget,"red_button");
    }
    else
    {
      asic.VFE_status[ivfe-1]=FALSE;
      gtk_widget_set_name(widget,"hidden_button");
      gtk_widget_set_sensitive(widget,FALSE);
      if(daq.current_channel==ich+1)gtk_widget_hide((GtkWidget*)gtk_data.channel_window);
    }
  }
  update_active_channel();
}

void open_channel(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget=NULL;
  char content[80];
  GtkWindow *window=gtk_data.channel_window;
  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  sscanf(name,"0_channel_%d",&daq.current_channel);
  Int_t ich=daq.current_channel-1;
  printf("Open channel %d : status %d\n",daq.current_channel,asic.channel_status[ich]);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_channel_number");
  sprintf(content,"Channel %2.2d properties",daq.current_channel);
  gtk_label_set_text((GtkLabel*)widget,content);
  printf("Open window to tune channel %d parameters\n",daq.current_channel);
  gtk_widget_show_all((GtkWidget*)gtk_data.channel_window);
  gtk_window_move(window,gtk_data.window_xpos0+gtk_data.window_xoffset+gtk_data.window_width[0],
                         gtk_data.window_ypos0+gtk_data.window_yoffset+gtk_data.window_height[3]);
  gtk_window_present(window);
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_channel_active");
  g_assert(widget);
  if(!(asic.channel_status[ich] & gtk_switch_get_state((GtkSwitch*)widget)))
    gtk_switch_set_state((GtkSwitch*)widget,asic.channel_status[ich]);
  update_channel();
}
void close_channel(GtkWidget *widget, gpointer user_data)
{
  printf("Close channel window\n");
  gtk_widget_hide((GtkWidget*)gtk_data.channel_window);
}
void update_channel(void)
{
  GtkWidget *widget=NULL;
  char content[80];

  if(daq.current_channel<1 || daq.current_channel>N_CHANNEL) return;
  Int_t ich=daq.current_channel-1;
  Int_t ivfe=(daq.current_channel-1)/5;

// Fill records with actual values for this channel :
  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_channel_active"); // GtkSwitch
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.VFE_status[ivfe]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_Temp");
  g_assert (widget);
  sprintf(content,"Temp : %4.1f C",asic.CATIA_Temp_val[ich]);
  gtk_label_set_text((GtkLabel*)widget,content);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_I2C_bus"); // GtkEntry
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich]);
  sprintf(content,"%d",asic.I2C_bus[ich]);
  gtk_entry_set_text((GtkEntry*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_I2C_address"); // GtkEntry
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich]);
  sprintf(content,"%d",asic.I2C_address[ich]);
  gtk_entry_set_text((GtkEntry*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_active"); // GtkToggleButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich]);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.CATIA_status[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_version"); // GtkEntry
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  sprintf(content,"%d",asic.CATIA_version[ich]);
  gtk_entry_set_text((GtkEntry*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_Vref_value"); // GtkSpinButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  gtk_spin_button_set_value((GtkSpinButton*)widget,asic.CATIA_Vref_val[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_ped_G10"); // GtkSpinButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  gtk_spin_button_set_value((GtkSpinButton*)widget,asic.CATIA_ped_G10[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_ped_G1"); // GtkSpinButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  gtk_spin_button_set_value((GtkSpinButton*)widget,asic.CATIA_ped_G1[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_gain"); // GtkButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  if(asic.CATIA_gain[ich]==0)
  {
    sprintf(content,"Gain\n470");
  }
  else if(asic.CATIA_gain[ich]==1 || asic.CATIA_gain[ich]==2)
  {
    sprintf(content,"Gain\n380");
  }
  else
  {
    sprintf(content,"Gain\n320");
  }
  gtk_button_set_label((GtkButton*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_LPF35"); // GtkButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  if(asic.CATIA_LPF35[ich]==0)
    sprintf(content,"LPF\n50");
  else
    sprintf(content,"LPF\n35");
  gtk_button_set_label((GtkButton*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_DAC1"); // GtkToggleButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.CATIA_DAC1_status[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_DAC1_value"); // GtkEntry
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich] & asic.CATIA_DAC1_status[ich]);
  sprintf(content,"%d",asic.CATIA_DAC1_val[ich]);
  gtk_entry_set_text((GtkEntry*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_DAC2"); // GtkToggleButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.CATIA_DAC2_status[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_DAC2_value"); // GtkEntry
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich] & asic.CATIA_DAC2_status[ich]);
  sprintf(content,"%d",asic.CATIA_DAC2_val[ich]);
  gtk_entry_set_text((GtkEntry*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_Rconv_TP"); // GtkToggleButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  if(asic.CATIA_Rconv[ich]==0)
  {
    sprintf(content,"Rconv TP\n   2471");
    gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
  }
  else
  {
    sprintf(content,"Rconv TP\n   270");
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  gtk_button_set_label((GtkButton*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CATIA_SEU_correction"); // GtkToggleButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.CATIA_status[ich]);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.CATIA_SEU_corr[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_active"); // GtkToggleButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich]);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.DTU_status[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_version"); // GtkEntry
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.DTU_status[ich]);
  sprintf(content,"%d",asic.DTU_version[ich]);
  gtk_entry_set_text((GtkEntry*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_BL_G10"); // GtkEntry
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.DTU_status[ich]);
  sprintf(content,"%d",asic.DTU_BL_G10[ich]);
  gtk_entry_set_text((GtkEntry*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_BL_G1"); // GtkEntry
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.DTU_status[ich]);
  sprintf(content,"%d",asic.DTU_BL_G1[ich]);
  gtk_entry_set_text((GtkEntry*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_force_G10"); // GtkToggleButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.DTU_status[ich]);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.DTU_force_G10[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_force_G1"); // GtkToggleButton
  g_assert(widget);
  gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.DTU_status[ich]);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.DTU_force_G1[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_force_PLL"); // GtkToggleButton
  g_assert(widget);
  if(asic.DTU_status[ich] && asic.DTU_version[ich]>0)
    gtk_widget_set_sensitive(widget,asic.channel_status[ich]);
  else
    gtk_widget_set_sensitive(widget,FALSE);
  gtk_toggle_button_set_active((GtkToggleButton*)widget,asic.DTU_force_PLL[ich]);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_DTU_PLL_value"); // GtkEntry
  g_assert(widget);
  if(asic.DTU_status[ich] && asic.DTU_version[ich]>0)
    gtk_widget_set_sensitive(widget,asic.channel_status[ich] & asic.DTU_force_PLL[ich]);
  else
    gtk_widget_set_sensitive(widget,FALSE);
  sprintf(content,"%x",asic.DTU_PLL_conf[ich]);
  gtk_entry_set_text((GtkEntry*)widget,content);

  widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_CRC_errors_value"); // GtkLabel
  g_assert(widget);
  sprintf(content,"%x",asic.DTU_CRC_errors[ich]);
  gtk_label_set_text((GtkLabel*)widget,content);
}

void update_active_channel(void)
{
  GtkWidget *widget;
  char widget_name[132];

// Update active channels list
  daq.n_active_channel=0;
  for(Int_t ivfe=1; ivfe<=5; ivfe++)
  {
    if(daq.debug_DAQ)printf("vfe %d :\n",ivfe);
    for(Int_t i=1; i<=5; i++)
    {
      Int_t ich=(ivfe-1)*5+i-1;
      if(asic.VFE_status[ivfe-1])
      {
        if(daq.debug_DAQ)printf("ch %d %d %d\n",i,ich,asic.channel_status[ich]);
        if(asic.channel_status[ich])
        {
          daq.channel_number[daq.n_active_channel++]=ich+1;
          if(asic.DTU_eLink[ich])daq.eLink_active |= (1<<ich);
        }
      }
      sprintf(widget_name,"5_calibration_results_%d",ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert(widget);
      gtk_widget_set_sensitive(widget,asic.channel_status[ich]&asic.VFE_status[ivfe-1]);
    }
  }
  if(daq.debug_DAQ)printf("n_active_channel : %d\n",daq.n_active_channel);
  for(Int_t i=0; i<daq.n_active_channel; i++){if(daq.debug_DAQ)printf("%d ",daq.channel_number[i]);}
  if(daq.debug_DAQ)printf("\n");


}

void open_DAQ(GtkWidget *button, gpointer user_data)
{
  GtkWindow *window=gtk_data.DAQ_window;
  if(daq.debug_GTK) printf("Action on DAQ window : %d\n",gtk_widget_is_visible((GtkWidget*)window));
  if(gtk_widget_is_visible((GtkWidget*)window))
  {
    if(daq.debug_GTK) printf("Hide\n");
    gtk_widget_hide((GtkWidget*)window);
  }
  else
  {
    if(daq.debug_GTK) printf("Show\n");
    gtk_widget_show_all((GtkWidget*)window);
    gtk_window_move(window,gtk_data.window_xpos0,
                           gtk_data.window_ypos0+gtk_data.window_yoffset+gtk_data.window_height[0]);
    gtk_window_present(window);
  }
}
void close_DAQ(GtkWidget *widget, gpointer user_data)
{
  if(daq.debug_GTK) printf("Close DAQ window\n");
  gtk_widget_hide((GtkWidget*)gtk_data.DAQ_window);
}

void open_trigger(GtkWidget *button, gpointer user_data)
{
  GtkWindow *window=gtk_data.trigger_window;
  if(daq.debug_GTK) printf("Open trigger window\n");
  gtk_window_set_keep_above(window,FALSE);
  if(gtk_widget_is_visible((GtkWidget*)window))
    gtk_widget_hide((GtkWidget*)window);
  else
  {
    gtk_widget_show_all((GtkWidget*)window);
    gtk_window_get_size(window,&gtk_data.window_width[3],&gtk_data.window_height[3]);
    gtk_window_move(window,gtk_data.window_xpos0+gtk_data.window_xoffset*1+gtk_data.window_width[0],
                           gtk_data.window_ypos0);
    gtk_window_present(window);
  }
}
void close_trigger(GtkWidget *widget, gpointer user_data)
{
  if(daq.debug_GTK) printf("Close trigger window\n");
  gtk_widget_hide((GtkWidget*)gtk_data.trigger_window);
}

void open_actions(GtkWidget *button, gpointer user_data)
{
  GtkWindow *window=gtk_data.actions_window;
  if(gtk_widget_is_visible((GtkWidget*)window))
    gtk_widget_hide((GtkWidget*)window);
  else
  {
    gtk_widget_show_all((GtkWidget*)window);
    gtk_window_get_size(window,&gtk_data.window_width[4],&gtk_data.window_height[4]);
    gtk_window_move(window,gtk_data.window_xpos0+gtk_data.window_xoffset*0+gtk_data.window_width[1],
                           gtk_data.window_ypos0+gtk_data.window_yoffset*1+gtk_data.window_height[0]);
    gtk_window_present(window);
  }
}
void close_actions(GtkWidget *widget, gpointer user_data)
{
  if(daq.debug_GTK) printf("Close actions window\n");
  gtk_widget_hide((GtkWidget*)gtk_data.actions_window);
}

void open_results(void)
{
  GtkWindow *window=gtk_data.results_window;
  gtk_widget_show_all((GtkWidget*)window);
  gtk_window_get_size(window,&gtk_data.window_width[5],&gtk_data.window_height[5]);
  gtk_window_move(window,gtk_data.window_xpos0,
                         gtk_data.window_ypos0+gtk_data.window_yoffset+gtk_data.window_height[1]);
  gtk_window_present(window);
}
void update_results(void)
{
  GtkWidget *widget=NULL;
  char widget_name[80];
  char widget_text[80];
  for(int i=0; i<daq.n_active_channel; i++)
  {
    for(Int_t igain=0; igain<2; igain++)
    {
      Int_t ich=daq.channel_number[i]-1;
      sprintf(widget_name,"5_G%d_noise_%d",10-igain*9,ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert(widget);
      sprintf(widget_text,"%.3f",asic.rms[igain][ich]*1000.);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
      sprintf(widget_name,"5_G%d_INL_%d",10-igain*9,ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert(widget);
      sprintf(widget_text,"%.3f",asic.INL[igain][ich]);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
      sprintf(widget_name,"5_G%d_gain_%d",10-igain*9,ich+1);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,widget_name);
      g_assert(widget);
      sprintf(widget_text,"%.1f",asic.gain[igain][ich]*1000.);
      gtk_label_set_text((GtkLabel*)widget,widget_text);
    }
  }
}
void close_results(GtkWidget *widget, gpointer user_data)
{
  if(daq.debug_GTK) printf("Close results window\n");
  gtk_widget_hide((GtkWidget*)gtk_data.results_window);
}

void close_map(GtkWidget *widget, gpointer user_data)
{
  if(daq.debug_GTK) printf("Close MEM map window\n");
  gtk_widget_hide((GtkWidget*)gtk_data.map_window);
}

void button_clicked(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget=NULL;
  GtkWidget *image=NULL;
  GtkWidget *event_box=NULL;
  char widget_name[80];
  char content[80];
  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  if(daq.debug_GTK)printf("Button %s clicked !\n",name);
  Int_t ich=daq.current_channel-1;

  if(strcmp(name,"1_channel_done")==0)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"1_channel_window");
    gtk_widget_hide(widget);
  }
  else if(strcmp(name,"0_MEM_map")==0)
  {
    gtk_widget_show_all((GtkWidget*)gtk_data.map_window);
    gtk_window_present(gtk_data.map_window);
  }
  else if(strcmp(name,"0_power_LV")==0)
  {
    if(daq.debug_GTK) printf("Switch LV toggle button\n");
    daq.LV_ON=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.LV_ON)
    {
      gtk_widget_set_name(button,"green_button");
      sprintf(content,"LV\nON");
    }
    else
    {
      gtk_widget_set_name(button,"red_button");
      sprintf(content," LV\nOFF");
    }
    gtk_button_set_label((GtkButton*)button,content);
    update_VFE_control(1);
  }
  else if(strcmp(name,"1_CATIA_active")==0)
  {
    asic.CATIA_status[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
    for(Int_t ireg=1; ireg<6; ireg++)update_CATIA_reg(ich,ireg);
    update_channel();
  }
  else if(strcmp(name,"1_CATIA_gain")==0)
  {
    if(asic.CATIA_gain[ich]==0)
      asic.CATIA_gain[ich]=3;
    else if(asic.CATIA_gain[ich]==1)
      asic.CATIA_gain[ich]=0;
    else
      asic.CATIA_gain[ich]=1;

    update_CATIA_reg(ich,3);
    update_channel();
  }
  else if(strcmp(name,"1_CATIA_LPF35")==0)
  {
    asic.CATIA_LPF35[ich]=0;
    if(gtk_toggle_button_get_active((GtkToggleButton*)button)) asic.CATIA_LPF35[ich]=1;
    update_CATIA_reg(ich,3);
    update_channel();
  }
  else if(strcmp(name,"1_CATIA_DAC1")==0)
  {
    asic.CATIA_DAC1_status[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
    update_CATIA_reg(ich,4);
    update_channel();
  }
  else if(strcmp(name,"1_CATIA_DAC2")==0)
  {
    asic.CATIA_DAC2_status[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
    update_CATIA_reg(ich,4);
    update_channel();
  }
  else if(strcmp(name,"1_CATIA_Rconv_TP")==0)
  {
    asic.CATIA_Rconv[ich]=0;
    if(gtk_toggle_button_get_active((GtkToggleButton*)button)) asic.CATIA_Rconv[ich]=1;
    update_CATIA_reg(ich,6);
    update_channel();
  }
  else if(strcmp(name,"1_CATIA_SEU_correction")==0)
  {
    asic.CATIA_SEU_corr[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
    update_CATIA_reg(ich,1);
    update_channel();
  }
  else if(strcmp(name,"1_DTU_active")==0)
  {
    asic.DTU_status[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.debug_GTK) printf("New DTU status : %d\n",asic.DTU_status[ich]);
    if(asic.DTU_status[ich])
    {
      for(Int_t ireg=0; ireg<=25; ireg++)update_LiTEDTU_reg(ich,ireg,3);
    }
    update_channel();
  }
  else if(strcmp(name,"1_DTU_force_G10")==0)
  {
    asic.DTU_force_G10[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
    update_LiTEDTU_reg(ich,1,3);
    update_channel();
  }
  else if(strcmp(name,"1_DTU_force_G1")==0)
  {
    asic.DTU_force_G1[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
    update_LiTEDTU_reg(ich,1,3);
    update_channel();
  }
  else if(strcmp(name,"1_DTU_force_PLL")==0)
  {
    asic.DTU_force_PLL[ich]=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.debug_GTK)printf("Channel %d force PLL : %d\n",ich,asic.DTU_force_PLL[ich]);
    update_LiTEDTU_reg(ich,17,3);
    update_channel();
  }
  else if(strcmp(name,"1_CRC_errors")==0)
  {
    get_CRC_errors(0);
  }
  else if(strcmp(name,"2_debug_DAQ")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
      daq.debug_DAQ=TRUE;
    else
      daq.debug_DAQ=FALSE;
    if(daq.debug_GTK)printf("New debug_DAQ : %d\n",daq.debug_DAQ);
  }
  else if(strcmp(name,"2_debug_I2C")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
      daq.debug_I2C=TRUE;
    else
      daq.debug_I2C=FALSE;
    if(daq.debug_GTK)printf("New debug_I2C : %d\n",daq.debug_I2C);
  }
  else if(strcmp(name,"2_debug_GTK")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
      daq.debug_GTK=TRUE;
    else
      daq.debug_GTK=FALSE;
    if(daq.debug_GTK)printf("New debug_GTK : %d\n",daq.debug_GTK);
  }
  else if(strcmp(name,"2_debug_draw")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.debug_draw=TRUE;
      if(daq.debug_GTK) printf("Set debug_draw to 1 : %d\n",daq.debug_draw);
      if(daq.c1==NULL)
      {
        daq.c1=new TCanvas("c1","c1",1120,0,800,800);
        daq.c1->Divide(5,5);
        daq.c1->Update();
      }
      if(daq.c2==NULL && daq.use_LMB)
      {
        daq.c2=new TCanvas("c2","c2",1320,0,600,800);
        daq.c2->Divide(1,3);
        daq.c2->Update();
      }
    }
    else
    {
      daq.debug_draw=FALSE;
      if(daq.debug_GTK)
      {
        printf("destroy draw window !\n");
        printf("Set debug_draw to 0 : %d\n",daq.debug_draw);
      }
      if(daq.c1!=NULL)
      {
        daq.c1->~TCanvas();
        gSystem->ProcessEvents();
        daq.c1=NULL;
      }
      if(daq.c2!=NULL)
      {
        daq.c2->~TCanvas();
        gSystem->ProcessEvents();
        daq.c2=NULL;
      }
    }
  }
  else if(strcmp(name,"2_zoom")==0)
  {
    daq.zoom_draw=gtk_toggle_button_get_active((GtkToggleButton*)button);
  }
  else if(strcmp(name,"2_MEM_mode")==0)
  {
    daq.MEM_mode=gtk_toggle_button_get_active((GtkToggleButton*)button);
    sprintf(content,"MEM mode OFF");
    if(daq.MEM_mode)sprintf(content,"MEM mode  ON");
    gtk_button_set_label((GtkButton*)button,content);
    if(daq.debug_GTK)
    {
      if(daq.MEM_mode)printf("Set MEM_mode to TRUE\n");
      else printf("Set MEM_mode to FALSE\n");
    }
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t jch=daq.channel_number[i]-1;
      update_LiTEDTU_reg(jch,1,3);
    }
    update_VFE_control(0);
  }
  else if(strcmp(name,"2_I2C_protocol")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.I2C_lpGBT_mode=1;
      gtk_button_set_label((GtkButton*)button,"   I2C  lpGBT  ");
    }
    else
    {
      daq.I2C_lpGBT_mode=0;
      gtk_button_set_label((GtkButton*)button,"   I2C standard");
    }
    if(daq.debug_GTK) printf("I2C set to standard: %d\n",daq.I2C_lpGBT_mode);
    update_VFE_control(0);
  }
  else if(strcmp(name,"2_PLL_loop")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      for(Int_t i=0; i<daq.n_active_channel; i++)
      {
        Int_t jch=daq.channel_number[i]-1;
        asic.DTU_override_Vc_bit[jch]=1;
      }
      gtk_button_set_label((GtkButton*)button,"PLL loop open ");
    }
    else
    {
      for(Int_t i=0; i<daq.n_active_channel; i++)
      {
        Int_t jch=daq.channel_number[i]-1;
        asic.DTU_override_Vc_bit[jch]=0;
      }
      gtk_button_set_label((GtkButton*)button,"PLL loop closed");
    }
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t jch=daq.channel_number[i]-1;
      update_LiTEDTU_reg(jch,17,3);
    }
  }
  else if(strcmp(name,"2_DTU_test_mode")==0)
  {
    const char *label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK)printf("DTU_test_mode button, old : %s, new : ",label);
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      gtk_button_set_label((GtkButton*)button,"DTU test mode ON ");
      daq.DTU_test_mode=1;
    }
    else
    {
      gtk_button_set_label((GtkButton*)button,"DTU test mode OFF");
      daq.DTU_test_mode=0;
    }
    label=gtk_button_get_label((GtkButton*)button);
    if(daq.debug_GTK)printf("%s\n",label);
    update_VFE_control(0);
  }
  else if(strcmp(name,"0_ped_event")==0)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ped_event");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  else if(strcmp(name,"3_ped_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      if(daq.debug_GTK) printf("Set trigger for pedestal events\n");
      daq.trigger_type=0;
      daq.self_trigger=0;
      update_trigger();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ReSync_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_laser_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate soft_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    }
  }
  else if(strcmp(name,"0_TP_event")==0)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_event");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  else if(strcmp(name,"3_TP_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      if(daq.debug_GTK) printf("Set trigger for test-pulse events\n");
      daq.trigger_type=1;
      daq.self_trigger=0;
      update_trigger();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ReSync_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ped_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_laser_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate soft_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    }
  }
  else if(strcmp(name,"0_laser_event")==0)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_laser_event");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  else if(strcmp(name,"3_laser_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      if(daq.debug_GTK) printf("Set trigger for external pulses\n");
      daq.trigger_type=2;
      daq.self_trigger=1;
      daq.self_trigger_loop=1;
      update_trigger();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ReSync_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ped_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate self_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
    }
  }
  else if(strcmp(name,"0_ReSync_event")==0)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ReSync_event");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
  }
  else if(strcmp(name,"3_ReSync_event")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      if(daq.debug_GTK) printf("Set trigger for ReSync events\n");
      daq.trigger_type=5;
      daq.self_trigger=0;
      update_trigger();
// inactivate other trigger_type :
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_TP_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ped_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_laser_event");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
// activate soft_trigger mode
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_trigger_mode");
      g_assert (widget);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,TRUE);
      gtk_toggle_button_set_active((GtkToggleButton*)widget,FALSE);
    }
  }
  else if(strcmp(name,"3_trigger_mode")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.self_trigger=1;
      gtk_button_set_label((GtkButton*)button,"Trigger mode:\n        self");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mode");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_threshold");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask_label");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
    else
    {
      daq.self_trigger=0;
      gtk_button_set_label((GtkButton*)button,"Trigger mode:\n        soft");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mode");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_threshold");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask_label");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_self_trigger_mask");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);
    }
    update_trigger();
  }
  else if(strcmp(name,"3_self_trigger_mode")==0)
  {
    if(gtk_toggle_button_get_active((GtkToggleButton*)button))
    {
      daq.self_trigger_mode=1;
      gtk_button_set_label((GtkButton*)button,"Trigger threshold:\n         delta");
    }
    else
    {
      daq.self_trigger_mode=0;
      gtk_button_set_label((GtkButton*)button,"Trigger threshold:\n       absolute");
    }
    update_trigger();
  }
  else if(strcmp(name,"3_gen_BC0")==0)
  {
    daq.gen_BC0=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.daq_initialized>=0)
    {
      UInt_t command = GENE_BC0*daq.gen_BC0 + GENE_WTE*daq.gen_WTE + LED_ON*daq.led_ON;
      uhal::HwInterface hw=devices.front();
      printf("Set BC0 generation to %d : %8.8x\n",daq.gen_BC0, command);
      hw.getNode("FW_VER").write(command);
      hw.dispatch();
    }
  }
  else if(strcmp(name,"3_gen_WTE")==0)
  {
    daq.gen_WTE=gtk_toggle_button_get_active((GtkToggleButton*)button);
    if(daq.daq_initialized>=0)
    {
      UInt_t command = GENE_BC0*daq.gen_BC0 + GENE_WTE*daq.gen_WTE + LED_ON*daq.led_ON;
      uhal::HwInterface hw=devices.front();
      printf("Set WTE generation to %d : %8.8x\n",daq.gen_WTE,command);
      hw.getNode("FW_VER").write(command);
      hw.dispatch();
    }
  }
  else if(strcmp(&name[2],"calibrate_FEMs")==0)
  {
    // Create contact with FEAD board :
    if(daq.daq_initialized>=0)
    {
      if(!daq.calibration_running)
      {
        gtk_button_set_label((GtkButton*)button,"Abort");
        daq.calibration_running=TRUE;
        calibrate_FEMs();
      }
      else
      {
// Stop calibration procedure
        gtk_button_set_label((GtkButton*)button,"calibrate\n   FEMs");
        daq.daq_running=FALSE;
        daq.calibration_running=FALSE;
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_take_run");
        g_assert (widget);
        sprintf(content,"Take run");
        gtk_button_set_label((GtkButton*)widget,content);
// Re-enable "get_event"
        widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_event");
        g_assert (widget);
        gtk_widget_set_sensitive(widget,TRUE);
      }
    }
  }
  else if(strcmp(&name[2],"init_VFE")==0)
  {
    // Create contact with FEAD board :
    if(daq.daq_initialized>=0)
    {
      init_VFEs();
    }
  }
  else if(strcmp(&name[2],"reset_SAFE")==0)
  {
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("SAFE_CTRL").write(1);
      hw.dispatch();
    }
  }
  else if(strcmp(&name[2],"get_temp")==0)
  {
    get_temp();
  }
  else if(strcmp(&name[2],"init_DAQ")==0)
  {
    // Create contact with FEAD board :
    if(daq.daq_initialized<0)
    {
      daq.daq_initialized=0;
      init_gDAQ();
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_init_DAQ");
      g_assert (widget);
      gtk_button_set_label((GtkButton*)widget,"DAQ initialized");
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_outputs");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_CATIA_outputs");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_ADC_calibration");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_ADC_calibration");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_synchronize_links");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_synchronize_links");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_line_delays");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_reset_line_delays");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_DTU");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_reset_DTU");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_DTU_test_unit");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_reset_DTU_test_unit");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_reset_DTU_I2C");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_reset_DTU_I2C");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_dump_ADC_registers");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_dump_ADC_registers");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_load_ADC_registers");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_load_ADC_registers");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_optimize_pedestals");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_optimize_pedestals");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_PLL_scan");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_PLL_scan");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_init_VFE");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_init_VFE");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_power_LV");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_tap_slip");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_bit_slip");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_byte_slip");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_temp");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_temp");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_calibrate_FEMs");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"3_ReSync_command");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
// Retreive ReSync data and load it in SAFE board;
      sscanf(gtk_entry_get_text((GtkEntry*)widget),"%x",&daq.resync_data);
      sprintf(content,"%x",daq.resync_data);
      gtk_entry_set_text((GtkEntry*)widget,content);
      uhal::HwInterface hw=devices.front();
      hw.getNode("DTU_RESYNC").write(daq.resync_data);
      hw.dispatch();
    }
    else
    {
      gtk_toggle_button_set_active((GtkToggleButton*)button,TRUE);
    }
  }
  else if(strcmp(&name[2],"take_run")==0)
  {
    if(!daq.daq_running)
    {
      printf("Take run\n");
      daq.daq_running=TRUE;
      sprintf(content,"Abort");
      gtk_button_set_label((GtkButton*)button,content);
// Disable "get_event"
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_event");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,FALSE);

      take_run();
    }
    else
    {
      daq.daq_running=FALSE;
      sprintf(content,"Take run");
      gtk_button_set_label((GtkButton*)button,content);
// Re-enable "get_event"
      widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_event");
      g_assert (widget);
      gtk_widget_set_sensitive(widget,TRUE);
    }
  }
  else if(strcmp(&name[2],"get_event")==0)
  {
    if(daq.debug_DAQ)printf("Get event\n");
    get_single_event();
  }
  else if(strcmp(&name[2],"PLL_scan")==0)
  {
    PLL_scan();
  }
  else if(strcmp(name,"0_CATIA_outputs")==0)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_CATIA_outputs");
    g_assert (widget);
    gtk_toggle_button_set_active((GtkToggleButton*)widget,!daq.CATIA_Vcal_out);
  }
  else if(strcmp(name,"4_CATIA_outputs")==0)
  {
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_CATIA_outputs");
    if(daq.CATIA_Vcal_out)
    {
      if(daq.debug_GTK)printf("Switch CATIA outputs to physics signals\n");
      daq.CATIA_Vcal_out=FALSE;
      gtk_button_set_label((GtkButton*)button,"CATIA outputs:\n      Physics");
      gtk_menu_item_set_label((GtkMenuItem*)widget,"CATIA outputs: Physics");
    }
    else
    {
      if(daq.debug_GTK)printf("Switch CATIA outputs to calibration levels\n");
      daq.CATIA_Vcal_out=TRUE;
      gtk_button_set_label((GtkButton*)button,"CATIA outputs:\n    Calibration");
      gtk_menu_item_set_label((GtkMenuItem*)widget,"CATIA outputs: Calibration");
    }
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t jch=daq.channel_number[i]-1;
      if(daq.debug_GTK)printf("Update Vcal_out %d for %d %d\n",daq.CATIA_Vcal_out,i,jch);
      update_CATIA_reg(jch,5);
    }
  }
  else if(strcmp(&name[2],"optimize_pedestals")==0)
  {
    optimize_pedestals();
  }
  else if(strcmp(&name[2],"ADC_calibration")==0)
  {
    calib_ADC();
  }
  else if(strcmp(&name[2],"synchronize_links")==0)
  {
    synchronize_links();
  }
  else if(strcmp(&name[2],"reset_line_delays")==0)
  {
    uhal::HwInterface hw=devices.front();
    hw.getNode("DELAY_CTRL1").write(DELAY_TAP_DIR*1 | DELAY_RESET*1);
    hw.dispatch();
    for(Int_t i=0; i<N_CHANNEL; i++)
    {
      daq.delay_val[i]=0;
      daq.bitslip_val[i]=0;
      daq.byteslip_val[i]=0;
    }
  }
  else if(strcmp(&name[2],"reset_DTU")==0)
  {
    send_ReSync_command(LiTEDTU_DTU_reset);
  }
  else if(strcmp(&name[2],"reset_DTU_test_unit")==0)
  {
    send_ReSync_command(LiTEDTU_ADCTestUnit_reset);
  }
  else if(strcmp(&name[2],"reset_DTU_I2C")==0)
  {
    send_ReSync_command(LiTEDTU_I2C_reset);
  }
  else if(strcmp(name,"4_tap_slip")==0)
  {
    daq.delay_val[ich]=daq.delay_val[ich]+1;
    if(daq.delay_val[ich]>=32)daq.delay_val[ich]-=32;
    uhal::HwInterface hw=devices.front();
    hw.getNode("TAPSLIP").write(1<<ich);
    hw.dispatch();
    sprintf(content,"tap slip\n    (%d)",daq.delay_val[ich]);
    gtk_button_set_label((GtkButton*)button,content);
  }
  else if(strcmp(name,"4_bit_slip")==0)
  {
    daq.bitslip_val[ich]=(daq.bitslip_val[ich]+1)%8;
    uhal::HwInterface hw=devices.front();
    hw.getNode("BITSLIP").write(1<<ich);
    hw.dispatch();
    sprintf(content,"bit slip\n    (%d)",daq.bitslip_val[ich]);
    gtk_button_set_label((GtkButton*)button,content);
  }
  else if(strcmp(name,"4_byte_slip")==0)
  {
    daq.byteslip_val[ich]=(daq.byteslip_val[ich]+1)%4;
    uhal::HwInterface hw=devices.front();
    hw.getNode("BYTESLIP").write(1<<ich);
    hw.dispatch();
    sprintf(content,"byte slip\n     (%d)",daq.byteslip_val[ich]);
    gtk_button_set_label((GtkButton*)button,content);
  }
  if(daq.daq_initialized==0 && daq.trigger_type>=0)
  {
    daq.daq_initialized++;
// Release other actions :
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_get_event");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_get_event");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"0_take_run");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
    widget=(GtkWidget*) gtk_builder_get_object(gtk_data.builder,"4_take_run");
    g_assert (widget);
    gtk_widget_set_sensitive(widget,TRUE);
  }
}

void switch_toggled(GtkWidget *button, gpointer user_data)
{
  if(daq.debug_GTK) printf("Switch toggle button for channel %d\n",daq.current_channel);
  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  char object_name[80];
  sprintf(object_name,"0_channel_%d",daq.current_channel);
  GtkWidget* widget = (GtkWidget*)gtk_builder_get_object (gtk_data.builder, object_name);
  g_assert (widget);

  Int_t ich=daq.current_channel-1;
  if(!(asic.channel_status[ich] & gtk_switch_get_active((GtkSwitch*)button)))
  {
    asic.channel_status[ich]=gtk_switch_get_active((GtkSwitch*)button);
    gtk_switch_set_state((GtkSwitch*)button,asic.channel_status[ich]);
    if(asic.channel_status[ich])
      gtk_widget_set_name(widget,"green_button");
    else
      gtk_widget_set_name(widget,"red_button");
    if(daq.debug_GTK)printf("New channel %d status : %d\n",daq.current_channel,asic.channel_status[ich]);
  }
  update_channel();
  update_active_channel();
  if(asic.channel_status[ich])
  {
    for(Int_t ireg=0; ireg<=6; ireg++) update_CATIA_reg(ich,ireg);
    for(Int_t ireg=0; ireg<=25; ireg++) update_LiTEDTU_reg(ich,ireg,3);
  }
}

void Entry_changed(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget;
  printf("Should not be there !\n");
}

void Entry_modified(GtkWidget *button, gpointer user_data)
{
  GtkWidget *widget;

  const char *name=gtk_buildable_get_name((GtkBuildable*)button);
  int device_number, iret;
  char text[80], widget_name[80];
  Int_t ich=daq.current_channel-1;

  if(daq.debug_GTK)printf("Entry %s modified : channel %d, new value %s\n",name,ich, gtk_entry_get_text((GtkEntry*)button));
  if(strcmp(name,"1_I2C_bus")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.I2C_bus[ich]);
  }
  else if(strcmp(name,"1_I2C_address")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.I2C_address[ich]);
  }
  else if(strcmp(name,"1_CATIA_version")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.CATIA_version[ich]);
  }
  else if(strcmp(name,"1_CATIA_Vref_value")==0)
  {
    Int_t Vref_old=asic.CATIA_Vref_val[ich];
    asic.CATIA_Vref_val[ich]=rint(gtk_spin_button_get_value((GtkSpinButton*)button));
    if(asic.CATIA_Vref_val[ich]==1)
    {
      if(Vref_old==0)asic.CATIA_Vref_val[ich]=2;
      else           asic.CATIA_Vref_val[ich]=0;
    }
    gtk_spin_button_set_value((GtkSpinButton*)button,(double)asic.CATIA_Vref_val[ich]);
    update_CATIA_reg(ich,6);
  }
  else if(strcmp(name,"1_CATIA_ped_G10")==0)
  {
    asic.CATIA_ped_G10[ich]=rint(gtk_spin_button_get_value((GtkSpinButton*)button));
    update_CATIA_reg(ich,3);
  }
  else if(strcmp(name,"1_CATIA_ped_G1")==0)
  {
    asic.CATIA_ped_G1[ich]=rint(gtk_spin_button_get_value((GtkSpinButton*)button));
    update_CATIA_reg(ich,3);
  }
  else if(strcmp(name,"1_CATIA_DAC1_val")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.CATIA_DAC1_val[ich]);
    update_CATIA_reg(ich,4);
  }
  else if(strcmp(name,"1_CATIA_DAC2_val")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.CATIA_DAC2_val[ich]);
    update_CATIA_reg(ich,5);
  }
  else if(strcmp(name,"1_DTU_version")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DTU_version[ich]);
  }
  else if(strcmp(name,"1_DTU_BL_G10")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DTU_BL_G10[ich]);
    if(asic.DTU_BL_G10[ich]<0)asic.DTU_BL_G10[ich]=0;
    if(asic.DTU_BL_G10[ich]>255)asic.DTU_BL_G10[ich]=255;
    update_LiTEDTU_reg(ich,5,3); // Program BL in LiTE-DTU
    update_LiTEDTU_reg(ich,7,3); // And change switching gain level accordingly
    update_LiTEDTU_reg(ich,8,3);
    sprintf(text,"%d",asic.DTU_BL_G10[ich]);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"1_DTU_BL_G1")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&asic.DTU_BL_G1[ich]);
    if(asic.DTU_BL_G1[ich]<0)asic.DTU_BL_G1[ich]=0;
    if(asic.DTU_BL_G1[ich]>255)asic.DTU_BL_G1[ich]=255;
    update_LiTEDTU_reg(ich,6,3);
    sprintf(text,"%d",asic.DTU_BL_G1[ich]);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"1_DTU_PLL_value")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&asic.DTU_PLL_conf[ich]);
    asic.DTU_PLL_conf[ich] &= 0x01ff;
    sprintf(text,"%x",asic.DTU_PLL_conf[ich]);
    gtk_entry_set_text((GtkEntry*)button,text);
    asic.DTU_PLL_conf2[ich]= asic.DTU_PLL_conf[ich]&0x7;
    asic.DTU_PLL_conf1[ich]= asic.DTU_PLL_conf[ich]>>3;
    update_LiTEDTU_reg(ich,15,3);
    update_LiTEDTU_reg(ich,16,3);
  }
  else if(strcmp(name,"2_SAFE_number")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.SAFE_number);
    if(daq.debug_GTK)printf("New SAFE board %d\n",daq.SAFE_number);
  }
  else if(strcmp(name,"2_nsample")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.nsample);
    if(daq.nsample<=0)daq.nsample=1;
    if(daq.nsample>NSAMPLE_MAX-2)daq.nsample=NSAMPLE_MAX-2;
    sprintf(text,"%d",daq.nsample);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"2_HW_delay")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.hw_DAQ_delay);
    if(daq.hw_DAQ_delay<0)daq.hw_DAQ_delay=0;
    if(daq.hw_DAQ_delay>512)daq.hw_DAQ_delay=512;
    sprintf(text,"%d",daq.hw_DAQ_delay);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*daq.sw_DAQ_delay | HW_DAQ_DELAY*daq.hw_DAQ_delay);
      hw.dispatch();
    }
  }
  else if(strcmp(name,"2_SW_delay")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.sw_DAQ_delay);
    if(daq.sw_DAQ_delay<0)daq.sw_DAQ_delay=0;
    if(daq.sw_DAQ_delay>512)daq.sw_DAQ_delay=512;
    sprintf(text,"%d",daq.sw_DAQ_delay);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("TRIG_DELAY").write(SW_DAQ_DELAY*daq.sw_DAQ_delay | HW_DAQ_DELAY*daq.hw_DAQ_delay);
      hw.dispatch();
    }
  }
  else if(strcmp(name,"2_TP_delay")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.TP_delay);
    if(daq.TP_delay<0)daq.TP_delay=0;
    sprintf(text,"%d",daq.TP_delay);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("CALIB_CTRL").write((daq.TP_delay<<16) | (daq.TP_width&0xffff));
      hw.dispatch();
    }
  }
  else if(strcmp(name,"2_TP_width")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.TP_width);
    if(daq.TP_width<1)daq.TP_width=1;
    if(daq.TP_width>255)daq.TP_width=255;
    sprintf(text,"%d",daq.TP_width);
    gtk_entry_set_text((GtkEntry*)button,text);
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t jch=daq.channel_number[i]-1;
      update_LiTEDTU_reg(jch,25,3);
    }
  }
  else if(strcmp(name,"2_ReSync_phase")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.resync_phase);
    daq.resync_phase&=0x1F;
    sprintf(text,"%2.2x",daq.resync_phase);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_DAQ_settings();
  }
  else if(strcmp(name,"3_ReSync_command")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.resync_data);
    send_ReSync_command(daq.resync_data);
  }
  else if(strcmp(name,"3_nevent")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.nevent);
    if(daq.nevent<-1)daq.nevent=-1;
    if(daq.nevent==0)daq.nevent=1;
    sprintf(text,"%d",daq.nevent);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"3_self_trigger_threshold")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.self_trigger_threshold);
    if(daq.self_trigger_threshold<0)daq.self_trigger_threshold=0;
    if(daq.self_trigger_threshold>8191)daq.self_trigger_threshold=8191;
    sprintf(text,"%d",daq.self_trigger_threshold);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_trigger();
  }
  else if(strcmp(name,"3_self_trigger_mask")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%x",&daq.self_trigger_mask);
    daq.self_trigger_mask&=0x1FFFFFF;
    sprintf(text,"%x",daq.self_trigger_mask);
    gtk_entry_set_text((GtkEntry*)button,text);
    update_trigger();
  }
  else if(strcmp(name,"3_TP_level")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.TP_level);
    if(daq.TP_level<0)daq.TP_level=0;
    if(daq.TP_level>4095)daq.TP_level=4095;
    sprintf(text,"%d",daq.TP_level);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.debug_GTK)printf("Update %d channels :\n",daq.n_active_channel);
    for(Int_t i=0; i<daq.n_active_channel; i++)
    {
      Int_t jch=daq.channel_number[i]-1;
      if(!asic.CATIA_status[jch])continue;
      if(daq.debug_GTK)printf("%d ",jch+1);
      asic.CATIA_DAC1_val[jch]=daq.TP_level;
      asic.CATIA_DAC2_val[jch]=daq.TP_level;
      update_CATIA_reg(jch,4);
      update_CATIA_reg(jch,5);
    }
    if(daq.debug_GTK)printf("\n");
    update_channel();
    usleep(500000);
  }
  else if(strcmp(name,"3_TP_step")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.TP_step);
    sprintf(text,"%d",daq.TP_step);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"3_n_TP_steps")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.n_TP_step);
    if(daq.n_TP_step<=1)daq.n_TP_step=1;
    sprintf(text,"%d",daq.n_TP_step);
    gtk_entry_set_text((GtkEntry*)button,text);
  }
  else if(strcmp(name,"3_WTE_pos")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.WTE_pos);
    sprintf(text,"%d",daq.WTE_pos);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("WTE_POS").write(daq.WTE_pos);
      hw.dispatch();
    }
  }
  else if(strcmp(name,"3_ReSync_pos")==0)
  {
    sscanf(gtk_entry_get_text((GtkEntry*)button),"%d",&daq.ReSync_pos);
    sprintf(text,"%d",daq.ReSync_pos);
    gtk_entry_set_text((GtkEntry*)button,text);
    if(daq.daq_initialized>=0)
    {
      uhal::HwInterface hw=devices.front();
      hw.getNode("RESYNC_POS").write(daq.ReSync_pos);
      hw.dispatch();
    }
  }
}

UInt_t update_LiTEDTU_reg(Int_t ich, Int_t reg, Int_t RW)
{
  UInt_t iret=0;
  if(!asic.DTU_status[ich] || asic.DTU_version[ich]==0) return -1;
  if(daq.daq_initialized>=0 && ich>=0 && ich<N_CHANNEL)
  {
    uhal::HwInterface hw=devices.front();
    UInt_t I2C_data=0;
    Int_t bus=asic.I2C_bus[ich];
    Int_t num=asic.I2C_address[ich];
    Int_t device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+2;      // DTU sub-address
    //printf("I2C_RW for bus %d, ich %d, i2c_add %d, device_number %d\n",bus,ich,num,device_number); 
    if(reg==0)
    {
      // Enable outputs
      I2C_data=0x81;
      if(asic.DTU_clk_out[ich])I2C_data|=0x50;
      if(daq.DTU_test_mode==1)
        I2C_data|=0x0F;
      else
        I2C_data|=0x01;
      iret=I2C_RW(hw, device_number, 0, I2C_data, 0, RW, daq.debug_I2C);
      if(daq.debug_DAQ)printf("1- ch %d, Reg0 content : W 0x%2.2x, R 0x%2.2x\n",ich, I2C_data, iret&0xff);
    }
    else if(reg==1)
    {
  // ADCs ON and Force/free gain, G1 window :
      I2C_data=0x03; // ADC H&L ON 
      if(daq.MEM_mode) I2C_data|=0x20; // MEM is using 80 MHz mode
      if(asic.DTU_force_G1[ich])
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data|0xc0, 0, RW, daq.debug_I2C);
      }
      else if(asic.DTU_force_G10[ich])
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data|0x40, 0, RW, daq.debug_I2C);
      }
      else if(asic.DTU_G1_window16[ich]==1)
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data|0x80, 0, RW, daq.debug_I2C);
      }
      else
      {
        iret=I2C_RW(hw, device_number, reg, I2C_data, 0, RW, daq.debug_I2C);
      }
      if(daq.debug_DAQ)printf("Ch %d, bus %d, add %d : Reg 1 content 0x%2.2x\n",ich, bus, num, iret&0xff);
    }
    else if(reg==2) // Driver strenth
      iret=I2C_RW(hw, device_number, reg, asic.DTU_driver_current[ich], 0, RW, daq.debug_I2C);
    else if(reg==3) // pre-amphasis control
    {
      I2C_data=0;
      if(asic.DTU_PE_strength[ich]>=0) I2C_data=0x80 | (asic.DTU_PE_width[ich]<<3) | (asic.DTU_PE_strength[ich]);
      iret=I2C_RW(hw, device_number, reg, I2C_data, 0, RW, daq.debug_I2C);
    }
    else if(reg==5) // G10 baseline
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_BL_G10[ich], 0, RW, daq.debug_I2C);
      if(RW==2) asic.DTU_BL_G10[ich]=iret&0xff;
    }
    else if(reg==6) // G1 baseline
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_BL_G1[ich], 0, RW, daq.debug_I2C);
      if(RW==2) asic.DTU_BL_G1[ich]=iret&0xff;
    }
    else if(reg==7)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, I2C_data&0xff, 0, RW, daq.debug_I2C);
    }
    else if(reg==8)
    {
      I2C_data=0xff0-asic.DTU_BL_G10[ich]; // switch gain at 4080 - substrated baseline
      iret=I2C_RW(hw, device_number, reg, (I2C_data>>8)&0xf, 0, RW, daq.debug_I2C);
    }
    else if(reg==15)
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      iret&=0xFF;
      iret=I2C_RW(hw, device_number, reg, (iret&0xFE)|(((asic.DTU_PLL_conf[ich]&0x100)>>8)&1), 0, RW, daq.debug_I2C);
    }
    else if(reg==16)
    {
      iret=I2C_RW(hw, device_number, reg, asic.DTU_PLL_conf[ich]&0xFF, 0, RW, daq.debug_I2C);
    }
    else if(reg==17)
    {
      iret=I2C_RW(hw, device_number, reg, 0, 0, 2, daq.debug_I2C);
      //iret=(iret&0xf2) | (asic.DTU_force_PLL[ich]<<3) | (asic.DTU_force_PLL[ich]<<2) | (1<<1) | (asic.DTU_override_Vc_bit[ich]<<0);
      iret=(iret&0xf4) | (asic.DTU_force_PLL[ich]<<3) | (1<<2) | (asic.DTU_VCO_rail_mode[ich]<<1) | (asic.DTU_override_Vc_bit[ich]<<0);
      iret=I2C_RW(hw, device_number, reg, iret, 0, RW, daq.debug_I2C);
    }
    else if(reg==21)
    {
      iret=I2C_RW(hw, device_number, reg, 0x0c, 0, RW, daq.debug_I2C);
    }
    else if(reg==22)
    {
      iret=I2C_RW(hw, device_number, reg, 0xcc, 0, RW, daq.debug_I2C);
    }
    else if(reg==23)
    {
      iret=I2C_RW(hw, device_number, reg, 0xcc, 0, RW, daq.debug_I2C);
    }
    else if(reg==24)
    {
      iret=I2C_RW(hw, device_number, reg, 0xcf, 0, RW, daq.debug_I2C);
    }
    else if(reg==25)
    {
      iret=I2C_RW(hw, device_number, reg, daq.TP_width, 0, RW, daq.debug_I2C);
    }
  }
  return iret;
}

UInt_t update_CATIA_reg(Int_t ich, Int_t reg)
{
  UInt_t iret=0;
  if(!asic.CATIA_status[ich]) return -1;
  if(daq.daq_initialized>=0 && ich>=0 && ich<N_CHANNEL)
  {
    uhal::HwInterface hw=devices.front();
    UInt_t I2C_data=0, I2C_long=0;
    
    Int_t bus=asic.I2C_bus[ich];
    Int_t num=asic.I2C_address[ich];
    if(num<=0)return -1;
    Int_t device_number=bus*1000+(num<<daq.I2C_shift_dev_number)+3;

    Int_t loc_DAC1_status=0, loc_DAC2_status=0;
    if(asic.CATIA_DAC1_status[ich])loc_DAC1_status=1;
    if(asic.CATIA_DAC2_status[ich])loc_DAC2_status=1;

    if(reg==1)
    {
      I2C_data=(asic.CATIA_temp_X5[ich]<<4) | (asic.CATIA_temp_out[ich]<<3) | (asic.CATIA_SEU_corr[ich]<<1);
      I2C_long=0;
    }
    else if(reg==3)
    {
// Set pedestal values, gain and LPF
      I2C_data=asic.CATIA_gain[ich]<<14 | asic.CATIA_ped_G1[ich]<<8 | asic.CATIA_ped_G10[ich]<<2 | asic.CATIA_LPF35[ich]<<1 | 1;
      I2C_long=1;
    }
    else if(reg==4)
    {
      I2C_data=asic.CATIA_DAC_buf_off[ich]<<15 | loc_DAC2_status<<13 | loc_DAC1_status<<12 | asic.CATIA_DAC1_val[ich];
      I2C_long=1;
    }
    else if(reg==5)
    {
      I2C_data=(1-asic.CATIA_Vref_out[ich])<<14;
      Int_t Vcal_out=0;
      if(daq.CATIA_Vcal_out)Vcal_out=3;
      I2C_data |= Vcal_out<<12 | asic.CATIA_DAC2_val[ich];
      I2C_long=1;
    }
    else if(reg==6)
    {
      I2C_data=asic.CATIA_Vref_val[ich]<<4 | asic.CATIA_Rconv[ich]<<2 | 0xb;
      I2C_long=0;
    }
    if(reg>0 && reg<=6)
    {
      iret=I2C_RW(hw, device_number, reg, I2C_data, I2C_long, 3, daq.debug_I2C);
      asic.CATIA_reg_loc[ich][reg]=I2C_data;
    }
  }
  return iret;
}

void update_VFE_control(Int_t pwup_reset)
{
  Int_t pwup_reset_loc=0;
  if(pwup_reset>0) pwup_reset_loc=1;
  if(daq.daq_initialized>=0)
  {
    uhal::HwInterface hw=devices.front();
    if(daq.debug_GTK)printf("Update VFE settings :\n");
    UInt_t VFE_control;
    VFE_control= PWUP_RESETB*pwup_reset_loc | DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | VFE_LV*daq.LV_ON | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
    hw.getNode("VFE_CTRL").write(VFE_control);
    if(pwup_reset>0)
    {
      VFE_control= DTU_TEST_MODE*daq.DTU_test_mode | DTU_MEM_MODE*daq.MEM_mode | VFE_LV*daq.LV_ON | I2C_LPGBT_MODE*daq.I2C_lpGBT_mode;
      hw.getNode("VFE_CTRL").write(VFE_control);
    }
    UInt_t clk_setting= RESYNC_PHASE*daq.resync_phase | eLINK_ACTIVE*daq.eLink_active;
    hw.getNode("CLK_SETTING").write(clk_setting);
    hw.dispatch();
  }
}
void update_trigger(void)
{
  if(daq.daq_initialized>=0)
  {
    printf("Update trigger settings :\n");
    printf("self trigger           : %d\n",daq.self_trigger);
    printf("self trigger loop      : %d\n",daq.self_trigger_loop);
    printf("self trigger mode      : %d\n",daq.self_trigger_mode);
    printf("self trigger threshold : %d\n",daq.self_trigger_threshold);
    printf("self trigger mask      : %7.7x\n",daq.self_trigger_mask);
  // Switch to triggered mode + external trigger :
    UInt_t command=
               SELF_TRIGGER_MODE     *daq.self_trigger_mode                |
              (SELF_TRIGGER_THRESHOLD*(daq.self_trigger_threshold&0x1FFF)) |
               SELF_TRIGGER          *daq.self_trigger                     |
               SELF_TRIGGER_LOOP     *daq.self_trigger_loop                |
               FIFO_MODE             *(daq.fifo_mode&1)                    |
               RESET                 *0;
    uhal::HwInterface hw=devices.front();
    hw.getNode("SAFE_CTRL").write(command);
    hw.getNode("TRIGGER_MASK").write(daq.self_trigger_mask&0x1FFFFFF);
    hw.dispatch();
  }
}
void update_DAQ_settings(void)
{
  if(daq.daq_initialized>=0)
  {
    printf("Update DAQ settings :\n");
    printf("eLinks        : 0x%7.7x\n",daq.eLink_active);
    printf("ReSync phases : 0x%2.2x\n",daq.resync_phase);
    UInt_t command=daq.resync_phase*RESYNC_PHASE | eLINK_ACTIVE*daq.eLink_active;
    uhal::HwInterface hw=devices.front();
    hw.getNode("CLK_SETTING").write(command);
    hw.dispatch();
  }
}

