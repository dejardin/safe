void Display_tresol()
{
  double N[5]={0.92,0.87,0.79,0.89,1.03};
  double q[6][5]={{1183., 188.8, 914.1, 190.3, 233.4},
                  {772.2, 123.2, 595.8, 124.3, 152.2},
                  {391.3,  62.6, 301.9,  63.1,  77.0},
                  {226.4,  36.4, 174.8,  36.7,  44.5},
                  {125.2,  20.2,  96.5,  20.5,  24.5},
                  { 83.6,  13.5,  64.3,  13.8,  16.4}};
  double dt[6][4]={{  8.7, 26.0, 26.9, 24.4},
                   { 16.2, 39.3, 41.2, 37.4},
                   { 22.8, 76.7, 81.8, 73.5},
                   { 35.2,125.5,134.4,123.4},
                   { 61.5,220.9,216.9,210.6},
                   { 91.0,306.1,287.5,273.6}};
  TGraphErrors *tg=new TGraphErrors();
  int n=0;
  for(int imeas=0; imeas<6; imeas++)
  {
    for(int i=0; i<4; i++)
    {
      int i0=2;
      int i1=i;
      if(i>=2)i1++;
      double A_eq=sqrt(2/(1./pow(q[imeas][i0]/N[i0],2)+1./pow(q[imeas][i1]/N[i1],2)));
      tg->SetPointError(n,1.,dt[imeas][i]/sqrt(2.)/100.);
      tg->SetPoint(n++,A_eq, dt[imeas][i]/sqrt(2.));
    }
  }
  tg->SetMarkerStyle(20);
  tg->SetMarkerSize(1.0);
  tg->SetMarkerColor(kRed);
  tg->SetLineColor(kRed);
  tg->Draw("ap");
  TF1 *f1=new TF1("f1","sqrt([0]*[0]/x/x+[1]*[1])",0.1,2000.);
  f1->SetParameter(0,5000.);
  f1->SetParameter(1,7.);
  tg->Fit(f1,"","",25.,2000.);
}
