#include <TApplication.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TF1.h>
#include <TProfile.h>
#include <TRandom.h>
#include <TTree.h>
#include <TFile.h>

#define NLOOP 5
#define NSAMPLE 100
#define TSTEP 0.2

int cur_ch;
double shape[5][3000];

double fshape(double *x, double *par)
{
  double t0=par[0];
  double qmax=par[1];
  double ped=par[2];
  double t=x[0]-t0;
  int i=(int)(t/TSTEP);
  double y=0.;
  if(i>0 && i<3000)
  {
    double t1=((double)i)*TSTEP;
    double t2=((double)i+1)*TSTEP;
    double y1=shape[cur_ch][i];
    double y2=shape[cur_ch][i+1];
    y=y1+(y2-y1)/(t2-t1)*(t-t1);
  } 
  //t=x[0]-t0+206.;
  //i=(int)(t/TSTEP);
  double y_ped=0.;
  if(i>0 && i<3000)
  {
    double t1=((double)i)*TSTEP;
    double t2=((double)i+1)*TSTEP;
    double y1=shape[cur_ch][i];
    double y2=shape[cur_ch][i+1];
    y_ped=y1+(y2-y1)/(t2-t1)*(t-t1);
  } 
  return ped+y_ped+y*qmax;
}

void ana_laser (int ivfe, int debug=0, int version=0, int ch_draw=2)
{
  int id;
  short int *sevent[5];
  double *event[5];
  char hname[250];
  double dv=1750./16384.;
  int ch_ref=2;
  int ch_min=0;
  int ch_max=4;
  TRandom *tr=new TRandom();
  char fname[132];

  //TCanvas *c1=new TCanvas("c1","c1",800.,1000.);
  //c1->Divide(2,3);
  TCanvas *c1=new TCanvas();
  TGraph *tg[5];
  TGraph *tg_timestamp = new TGraph();
  tg_timestamp->SetTitle("gtimestamp");
  tg_timestamp->SetName("gtimestamp");
  TH1D *hdeltat=new TH1D("delta_t","delta_t",1000,0.,0.01);
  TH1D *hmean[5], *hrms[5], *hdt[5], *hdq[5], *hped[5], *hq[5];
  TProfile *pdq[5], *pdt[5], *pq[5];
  TH1D *hchi2=new TH1D("chi2","chi2",1000,0.,250.);
  for(int ich=0; ich<5; ich++)
  {
    sprintf(hname,"dt%d",ich);
    hdt[ich]=new TH1D(hname,hname,1000.,-1.0,1.0);
    sprintf(hname,"dq%d",ich);
    hdq[ich]=new TH1D(hname,hname,2000.,0.0,2.0);
    sprintf(hname,"dt%d_vs_t",ich);
    pdt[ich]=new TProfile(hname,hname,100.,0.,6.25);
    sprintf(hname,"dq%d_vs_t",ich);
    pdq[ich]=new TProfile(hname,hname,100.,0.,6.25);
  }
  for(int ich=0; ich<5; ich++)
  {
    sprintf(hname,"q%d_vs_t",ich);
    pq[ich]=new TProfile(hname,hname,100.,0.,6.25);
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
    sprintf(hname,"ped_ch%d",ich);
    hped[ich]=new TH1D(hname,hname,100,-5.,5.);
    sprintf(hname,"q%d",ich);
    hq[ich]=new TH1D(hname,hname,2000,0.,2000.);
  }
  tg[0]->SetLineColor(kRed);
  tg[1]->SetLineColor(kYellow);
  tg[2]->SetLineColor(kBlue);
  tg[3]->SetLineColor(kMagenta);
  tg[4]->SetLineColor(kCyan);
  tg[0]->SetMarkerColor(kRed);
  tg[1]->SetMarkerColor(kYellow);
  tg[2]->SetMarkerColor(kBlue);
  tg[3]->SetMarkerColor(kMagenta);
  tg[4]->SetMarkerColor(kCyan);
  c1->Update();
  //TF1 *f1=new TF1("f1","[0]+[1]*exp(-(x-[2])*(x-[2])/2/[3]/[3])",0.,10000.);
  TF1 *f1=new TF1("f1","(x<[2]-[4]*[3])?0.:[0]+[1]*(x-[2]+[4]*[3])*exp(-(x-[2])*(x-[2])/2./[3]/[3])",0.,1000.);
  TF1 *fs=new TF1("fs",fshape,0.,10000.,3);
  TProfile *pshape[5][6];
  TH1D *hshape[5];
  for(int ich=0; ich<5; ich++)
  {
    for(int irange=0; irange<6; irange++)
    {
      sprintf(hname,"pshape_ch%d_%4.4d_%4.4dmV",ich,irange*200,(irange+1)*200);
      pshape[ich][irange]=new TProfile(hname,hname,3000,0.,3000*TSTEP);
    }
    sprintf(hname,"shape_ch%d",ich);
    hshape[ich]=new TH1D(hname,hname,3000,0.,3000*TSTEP);
  }

  ULong64_t timestamp;
  double delta_t, old_t=-1.;
  if(version==0)
    sprintf(fname,"/data/cms/ecal/fe/vfe_ADC160_asic_ETH/test/vfe_%2.2d_laser.root",ivfe);
  else
    sprintf(fname,"/data/cms/ecal/fe/vfe_ADC160_asic_ETH/test/vfe_%2.2d_%d_laser.root",ivfe,version);
  printf("Opening %s for analysis\n",fname);
  TFile *infile=new TFile(fname);
  TTree *tdata=(TTree*)infile->Get("data");
  tdata->SetBranchAddress("id",&id);
  tdata->SetBranchAddress("timestamp",&timestamp);

// Try to get frame size :
  char title[80];
  sprintf(title,"%s",tdata->GetBranch("ch0")->GetTitle());
  int nsample=0;
  sscanf(title,"ch0[%d]/S",&nsample);
  printf("Setting branches for %d samples\n",nsample);

  for(int ich=0; ich<5; ich++)
  {
    sevent[ich]=(short int*) malloc(nsample*sizeof(short int));
    event[ich]=(double*) malloc(nsample*sizeof(double));
    printf("Channel %d\n",ich);
    char bname[80];
    sprintf(bname,"ch%d",ich);
    tdata->SetBranchAddress(bname,sevent[ich]);
    //printf("sample %d\n",is);
  }
  int nevt=(Int_t)tdata->GetEntries();

  printf("Start analysis of %d events\n",nevt);
  int imax_ref[5]={-1,-1,-1,-1,-1};
  for(int iloop=0; iloop<NLOOP; iloop++)
  {
    hchi2->Reset();
    for( int ich=0; ich<5; ich++)
    {
      for(int irange=0; irange<6; irange++)
      {
        pshape[ich][irange]->Reset();
      }
    }
    printf("Start shape building loop %d\n",iloop);
    for(int ievt=0; ievt<nevt; ievt++)
    {
      if((ievt%1000)==0) printf("analyse evt : %d\n",ievt);
      tdata->GetEntry(ievt);
      if(debug>0)printf("timestamp : %16.16llx\n",timestamp);
      double t_event=6.25e-9*timestamp;
      if(iloop==0)
      {
        if(old_t>0. && iloop==0)
        {
          delta_t=t_event-old_t;
          hdeltat->Fill(delta_t);
        }
        old_t=t_event;
        tg_timestamp->SetPoint(ievt,t_event,delta_t);
      }
      char cdum;
      double qmax[5],tmax[5];
      int imax[5];
      for( int ich=0; ich<5; ich++)
      {
        qmax[ich]=0.;
        imax[ich]=0;
        tmax[ich]=0.;
        double ped=0.;
        for(int is=0; is<NSAMPLE; is++)
        {
          event[ich][is]=dv*sevent[ich][is];
          if(is<10)ped+=event[ich][is];
        }
        ped/=10.;
        for(int is=0; is<NSAMPLE; is++)
        {
          event[ich][is]-=ped;
          if(event[ich][is]>qmax[ich])
          {
            qmax[ich]=event[ich][is];
            imax[ich]=is;
          }
        }
        if(debug>0)printf("ich : %d, max = %d %f\n",ich,imax[ich],qmax[ich]);
        if(imax_ref[ich]<0)imax_ref[ich]=imax[ich];
      }
      if(qmax[ch_ref]>10.)
      {
        double tmin_fit, tmax_fit;
        for(int ich=ch_min; ich<=ch_max; ich++)
        {
          cur_ch=ich;
          //c1->cd(ich+1);
          for(int is=0; is<NSAMPLE; is++)
          {
            double t=6.25*is;
            tg[ich]->SetPoint(is,t,event[ich][is]);
          }
          if(iloop==0)
          {
            f1->FixParameter(0,0.);
            f1->SetParameter(1,qmax[ich]/8.45);
            f1->SetParameter(2,6.25*imax[ich]);
            //f1->SetParameter(3,6.3);
            f1->FixParameter(3,7.3);
            f1->FixParameter(4,1.);
            tmin_fit=(imax[ich]-2)*6.25-1.;
            tmax_fit=(imax[ich]+2)*6.25+1.;
            if(event[ich][imax[ich]+2]<qmax[ich]/10.)tmax_fit=(imax[ich]+1)*6.25+1.;
            if(debug==0)
              tg[ich]->Fit(f1,"Q","",tmin_fit,tmax_fit);
            else
              tg[ich]->Fit(f1,"","",tmin_fit,tmax_fit);
            tmax[ich]=f1->GetParameter(2);
            qmax[ich]=f1->GetParameter(1);
            double chi2=f1->GetChisquare();
            hchi2->Fill(chi2);
          }
          else
          {
            fs->SetParameter(0,(imax[ich]-imax_ref[ich])*6.25);
            //fs->SetParameter(0,0.);
            fs->SetParameter(1,qmax[ich]);
            fs->FixParameter(2,0.);
            tmin_fit=(imax[ich]-2)*6.25-1.;
            tmax_fit=(imax[ich]+3)*6.25+1.;
            if(debug==0)
              tg[ich]->Fit(fs,"Q","",tmin_fit,tmax_fit);
            else
              tg[ich]->Fit(fs,"","",tmin_fit,tmax_fit);
            tmax[ich]=fs->GetParameter(0)+imax_ref[ich]*6.25;
            qmax[ich]=fs->GetParameter(1);
            double chi2=fs->GetChisquare();
            hchi2->Fill(chi2);
          }
          //if(debug>0 && iloop>0)
          if(debug>0)
          {
            printf("ich %d\n",ich);
            printf("max %d, max_ref %d %f\n",imax[ich],imax_ref[ich], imax_ref[ich]*TSTEP);
            printf("t0 %f %f\n",imax[ich]*6.25-imax_ref[ich]*TSTEP, tmax[ich]);
            printf("tmin_fit %f, tmax_fit %f\n",tmin_fit,tmax_fit);
            if(ich==ch_draw)
            {
              tg[ich]->Draw("alp");
              tg[ich]->GetXaxis()->SetLimits(50.,250.);
              //fs->SetParameter(0,tmax[ich]-50.);
              //fs->SetParameter(1,qmax[ich]);
              //fs->Draw("same");
            }
          }
        }
        double phase=tmax[2]-imax_ref[2]*6.25;
        printf("phase : %e %d %e\n",tmax[2],imax_ref[2],phase);
        //if(debug >0 && iloop>0)
        if(debug >0)
        {
          c1->Update();
          system("stty raw");
          cdum=getchar();
          system("stty -raw");
          if(cdum=='q' || cdum=='Q') break;
        }
        double t0=0.0;
        double norm=0.0;
        for(int ich=ch_min; ich<=ch_max; ich++)
        {
          t0+=tmax[ich]*qmax[ich];
          norm+=qmax[ich];
        }
        t0/=norm;
        if(t0< 80. || t0>220.)
        {
          printf("Strange timing for event %d : %fi, skip it\n",ievt,t0);
          continue;
        }
        //t0=tr->Gaus(t0,0.10)-50.;
        t0=tr->Gaus(0.,0.01+0.1*(4-iloop));
        //t0=0.;
        norm=1.;
        for(int ich=0; ich<5; ich++)
        {
          pq[ich]->Fill(phase,qmax[ich]);
          hq[ich]->Fill(qmax[ich]);
          int irange=qmax[ich]/200.;
          if(irange<0)irange=0;
          if(irange>5)irange=5;
          printf("qmax %e mV, range %d\n",qmax[ich],irange);
          for(int is=0; is<NSAMPLE; is++)
          {
            double t=6.25*is-phase+t0;
            if(iloop==0)
              pshape[ich][irange]->Fill(t,event[ich][is]);
            else
              pshape[ich][irange]->Fill(t,event[ich][is]/qmax[ich]);
          }
        }
      }
    }
    sprintf(hname,"/data/cms/ecal/fe/vfe_ADC160_asic_ETH/test/vfe_%2.2d_laser_analysis_%2.2d.root",ivfe,iloop);
    TFile *fout=new TFile(hname,"recreate");
    for(int ich=0; ich<5; ich++)
    {
      for(int irange=0; irange<6; irange++)
      {
        pshape[ich][irange]->SetMarkerColor(iloop+1);
        pshape[ich][irange]->SetLineColor(iloop+1);
        pshape[ich][irange]->Write();
      }
      hped[ich]->Write();
      pq[ich]->Write();
      hq[ich]->Write();
      double qmax=0.;
      for(int i=0; i<3000; i++)
      {
        shape[ich][i]=pshape[ich][2]->GetBinContent(i+1);
        if(shape[ich][i]>qmax)qmax=shape[ich][i];
      }
      for(int i=0; i<3000; i++)
      {
        shape[ich][i]/=qmax;
        hshape[ich]->SetBinContent(i+1,shape[ich][i]);
      }
      hshape[ich]->SetMarkerColor(iloop+1);
      hshape[ich]->SetLineColor(iloop+1);
      hshape[ich]->Write();
    }
    tg_timestamp->Write();
    hdeltat->Write();
    hchi2->Write();
    printf("Mean Chi2 : %e\n",hchi2->GetMean());
    fout->Close();
  }
}
