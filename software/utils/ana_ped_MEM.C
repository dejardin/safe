#include <TCanvas.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TF1.h>
#include <TProfile.h>
#include <TRandom.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TVirtualFFT.h>

#define NSAMPLE_MAX 26624
//#define NMSPS 160
//#define NMSPS 250

int cur_ch;

void ana_ped_MEM (char *fname, int debug=-1, Int_t NMSPS=80, Double_t noise_max=3500, Int_t convert=1, Int_t LPF=0, Int_t G12=1)
{
  int nch=5;
  int n_active_channel=2;
  int channel_number[5]={0,3,-1,-1,-1};
  int bad_ch[5]={0};
  Double_t G12_val=12.8;
  Double_t G1=66.7e-3*1.e12; // V/C
  Int_t gLoc[5]={1,-1,-1,2,-1};

  char hname[256], htitle[256];
  printf("Opening file %s for analysis\n",fname);
  TFile *infile=new TFile(fname);
  printf("Getting tree\n");
  TTree *tdata=(TTree*)infile->Get("tdata");
  TCanvas *tc_rms;
  gDirectory->GetObject("rms",tc_rms);
// Try to get frame size :
  char title[80];
  int nsample=0,  nsample_max, nsample_tot=0, nsample_fft=0;
  sprintf(title,"%s",tdata->GetBranch("ch0")->GetTitle());
  sscanf(title,"ch0[%d]/S",&nsample_max);
  printf("Setting branches for %d samples\n", nsample_max);
  ULong64_t timestamp;
  int id;
  short int *sevent[nch];
  for(int ich=0; ich<nch; ich++){sevent[ich]=(short int*) malloc(nsample_max*sizeof(short int));}
  Double_t *event[nch];
  for(int ich=0; ich<nch; ich++){event[ich]=(double*) malloc(nsample_max*sizeof(double));}

  tdata->SetBranchAddress("id",&id);
  tdata->SetBranchAddress("timestamp",&timestamp);
  for(int ich=0; ich<nch; ich++)
  {
    printf("Channel %d\n",ich);
    char bname[80];
    sprintf(bname,"ch%d",ich);
    tdata->SetBranchAddress(bname,sevent[ich]);
    //printf("sample %d\n",is);
  }
  int nevt=(Int_t)tdata->GetEntries();
  nsample_tot=nsample_max;
  for(int ievt=1; ievt<nevt; ievt++)
  {
    tdata->GetEntry(ievt);
    //printf("Event %d : Found ",ievt);
    for(int ich=0; ich<nch; ich++)
    {
      for(nsample=0; sevent[ich][nsample]>=0 && nsample<nsample_max; nsample++);
      //printf("%d ",nsample);
      if(nsample==0)
        bad_ch[ich]=1;
      else if(nsample<nsample_tot)
        nsample_tot=nsample;
    }
    //printf("valid samples\n");
  }
  printf("Will process with %d samples per event\n",nsample_tot);

  Double_t pi=2.l*asin(1.l);
  Double_t dt=1.e-6/NMSPS;
  Double_t tmax=nsample_tot*dt;
  Double_t fmax=1/dt;
  Double_t df=1./tmax;
  Double_t dv=1.2/4096.;
  printf("dt %e, tmax %e, fmax %e, df %e Hz, dv %e mV\n",dt,tmax,fmax,df,dv*1000.);
  //dv=1.;
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(111);

// Cut-off frequency :
  Int_t BW_order=LPF/100;
  Double_t BW_ref=(LPF-BW_order*100)*1.e5;
  Double_t BW_eff=2./dt*tan(BW_ref*2.*pi*dt/2.)/2./pi;
  Double_t oc=2.*pi*BW_eff;
  Double_t g1=oc*(dt/2.);
  Double_t g2=g1*g1;
  Double_t g4=g2*g2;
  printf("%dth order Butterworth filter: requested cut-off frequency : %.2f MHz, effective cutoff frequency : %.2f MHz\n",BW_order,BW_ref/1.e6,BW_eff/1.e6);

// Butterworth filter order 1
// B(s)=1/((s/oc)+1)
// s=(2/dt)*(1-z-1)/(1+z-1)
// B(z)=g*(n0+n1*z)/(d0+d1*z)
  Double_t bw1_n0=1.;
  Double_t bw1_n1=1.;
  Double_t bw1_d0=g1+1.;
  Double_t bw1_d1=g1-1.;

// Butterworth filter order 2
// B(s)=1/((s/oc)^2+sqrt(2)*(s/oc)+1)
// s=(2/dt)*(1-z-1)/(1+z-1)
// B(z)=g^2*(n0+n1*z-1+n2*z-2)/(d0+d1*z-1+d2*z-2)
  Double_t o1=sqrt(2), o2=0.;
  Double_t bw2_n0=1.;
  Double_t bw2_n1=2.;
  Double_t bw2_n2=1.;
  Double_t bw2_d0=1.+o1*g1+g2;
  Double_t bw2_d1=2.*(g2-1.);
  Double_t bw2_d2=1.-o1*g1+g2;

// Butterworth filter order 4
// B(s)=1/[((s/oc)^2+o1*(s/oc)+1)*((s/oc)^2+o2*(s/oc)+1)]
// B(z)=g^4*(n0+n1*z-1+n2*z-2+n3*z-3+n4*z-4)/(d0+d1*z-1+d2*z-2+d3*z-3+d4*z-4)
  o1=0.7654; o2=1.8478;
  Double_t bw4_n0=1.;
  Double_t bw4_n1=4.;
  Double_t bw4_n2=6.;
  Double_t bw4_n3=4.;
  Double_t bw4_n4=1.;
  Double_t bw4_a1=1+o1*g1+g2;
  Double_t bw4_b1=2.*(g2-1.);
  Double_t bw4_c1=1-o1*g1+g2;
  Double_t bw4_a2=1+o2*g1+g2;
  Double_t bw4_b2=2.*(g2-1.);
  Double_t bw4_c2=1-o2*g1+g2;
  Double_t bw4_d0=bw4_a1*bw4_a2;
  Double_t bw4_d1=bw4_a1*bw4_b2+bw4_b1*bw4_a2;
  Double_t bw4_d2=bw4_a1*bw4_c2+bw4_b1*bw4_b2+bw4_c1*bw4_a2;
  Double_t bw4_d3=bw4_b1*bw4_c2+bw4_c1*bw4_b2;
  Double_t bw4_d4=bw4_c1*bw4_c2;

  Double_t y4=0.,y3=0.,y2=0.,y1=0.,y0=0.;
  Double_t x4=0.,x3=0.,x2=0.,x1=0.,x0=0.;

// Step response BW1 :
  Double_t yref1=1.;
  for(int i=0; i<200; i++)
  {
    x0=0.;
    if(i>10)x0=1.;
    y0=g1/bw1_d0*(bw1_n0*x0+bw1_n1*x1)-bw1_d1/bw1_d0*y1;
    x1=x0;
    y1=y0;
    yref1=y0;
    printf("%.2f ",y0);
  }
  printf("\nButterworth1 total gain : %f\n",yref1);

// Step response BW2 :
  y4=0.; y3=0.; y2=0.; y1=0., y0=0.;
  x4=0.; x3=0.; x2=0.; x1=0., x0=0.;
  Double_t yref2=1.;
  for(int i=0; i<200; i++)
  {
    x0=0.;
    if(i>10)x0=1.;
    y0=g2/bw2_d0*(bw2_n0*x0+bw2_n1*x1+bw2_n2*x2)-bw2_d1/bw2_d0*y1-bw2_d2/bw2_d0*y2;
    x2=x1; x1=x0;
    y2=y1; y1=y0;
    yref2=y0;
    printf("%.2f ",y0);
  }
  printf("\nButterworth2 total gain : %f\n",yref2);

  y4=0.; y3=0.; y2=0.; y1=0., y0=0.;
  x4=0.; x3=0.; x2=0.; x1=0., x0=0.;
// Step response :
  Double_t yref4=1.;
  for(int i=0; i<200; i++)
  {
    x0=0.;
    if(i>10)x0=1.;
    y0=g4/bw4_d0*(bw4_n0*x0+bw4_n1*x1+bw4_n2*x2+bw4_n3*x3+bw4_n4*x4)-bw4_d1/bw4_d0*y1-bw4_d2/bw4_d0*y2-bw4_d3/bw4_d0*y3-bw4_d4/bw4_d0*y4;
    x4=x3; x3=x2; x2=x1; x1=x0;
    y4=y3; y3=y2; y2=y1; y1=y0;
    yref4=y0;
    printf("%.2f ",y0);
  }
  printf("\nButterworth4 total gain : %f, %f\n",yref4,g4*(bw4_n0+bw4_n1+bw4_n2+bw4_n3+bw4_n4)/(bw4_d0+bw4_d1+bw4_d2+bw4_d3+bw4_d4));

  TGraph *tg_timestamp = new TGraph();
  tg_timestamp->SetTitle("gtimestamp");
  tg_timestamp->SetName("gtimestamp");
  TGraph *tg[nch];
  TH1D *hmean[nch], *hrms[nch];
  TProfile *pmod[nch];
  TH1D *hmod[nch],*hmod_loc[nch];
  for(int ich=0; ich<nch; ich++)
  {
    nsample=nsample_tot;
    nsample_fft=nsample_tot;
    dt=1.e-6/NMSPS;
    fmax=1./dt;
    if(ich<2)
      sprintf(htitle,"PNA");
    else
      sprintf(htitle,"PNB");

    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    tg[ich]->SetMarkerColor(kRed);
    tg[ich]->SetLineColor(kRed);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,htitle,4096,0.,1.2);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,htitle,200,0.,2.e-3);
    sprintf(hname,"pmod_ch%d",ich);
    pmod[ich]=new TProfile(hname,htitle,nsample,0.,fmax);
    pmod[ich]->SetLineColor(kBlue);
    sprintf(hname,"mod_ch%d",ich);
    hmod[ich]=new TH1D(hname,htitle,nsample/2,0.,fmax/2.);
    hmod[ich]->SetLineColor(kBlue);
    sprintf(hname,"mod_loc_ch%d",ich);
    hmod_loc[ich]=new TH1D(hname,htitle,nsample,0.,fmax);
    hmod_loc[ich]->SetLineColor(kRed);
  }

  Double_t delta_t=0., old_t=-1.;
  //nevt=2;
  Double_t qmax_max=0.;
  printf("Start analysis of %d events\n",nevt);

  nsample=nsample_tot;
  Double_t *rex=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *imx=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *rey=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *imy=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *mod=(double *)malloc(nsample_tot*sizeof(double));
  Double_t *phase=(double *)malloc(nsample_tot*sizeof(double));

  TCanvas *cframe=new TCanvas("sample_frame","sample frame",1000,0,700,500);
  cframe->Update();
  sprintf(hname,"noise_spectrum");
  TCanvas *cmod=new TCanvas(hname,hname,1200,500);
  cmod->Divide(2,1);
  cmod->Update();
  TVirtualFFT *fft_f1 = TVirtualFFT::FFT(1, &nsample_tot, "C2CFORWARD ES K");
  nsample_fft=nsample_tot;
  nsample_fft=(nsample_fft/4)*4;
  TVirtualFFT *fft_f3 = TVirtualFFT::FFT(1, &nsample_fft, "C2CFORWARD ES K");

  TF1 *fexpo=new TF1("fexpo","[0]+[1]*exp(-x*[2])",-1.e-3,2.e-3);
  TF1 *fpol1=new TF1("fpol1","[0]+[1]*x",-0.,2.e-3);
  fexpo->SetNpx(10000);
  Double_t rms[nch], hf_rms[nch], vhf_rms[nch];
  Double_t mean[nch];
  for(int ievt=0; ievt<nevt; ievt++)
  {
    tdata->GetEntry(ievt);
    if(debug>=0)printf("timestamp : %16.16llx\n",timestamp);
    Double_t t_event=6.25e-9*timestamp;
    if(old_t>0.) delta_t=t_event-old_t;
    old_t=t_event;
    tg_timestamp->SetPoint(ievt,t_event,delta_t);

    for( int i=0; i<n_active_channel; i++)
    {
      int ich=channel_number[i];
      nsample=nsample_tot;
      dt=1.e-6/NMSPS;
      fmax=1./dt;
      df=fmax/nsample;

      mean[ich]=0.;
      for(int is=0; is<nsample; is++)
      {
        event[ich][is]=dv*sevent[ich][is];
        mean[ich]+=event[ich][is];
      }
      mean[ich]/=nsample;
      hmean[ich]->Fill(mean[ich]);
      rms[ich]=0.;
      for(int is=0; is<nsample; is++)
      {
        event[ich][is]-=mean[ich];
        rex[is]=event[ich][is];
        imx[is]=0.;
        rms[ich]+=event[ich][is]*event[ich][is];
        Double_t t=dt*is;
        tg[ich]->SetPoint(is,t,event[ich][is]);
      }

      rms[ich]=sqrt(rms[ich]/nsample);
      printf("Channel %d : mean=%.1f lsb, %.1f mV, rms_t=%e V, ",ich,mean[ich]/dv,mean[ich]*1000.,rms[ich]);
      //if(rms[ich]>5.e-3)continue;
      hrms[ich]->Fill(rms[ich]);

// BW1 :
      if(LPF>0 && BW_order==1)
      {   
        y1=0.; y0=0.;
        x1=0.; x0=0.;
        for(int is=0; is<nsample; is++)
        {
          Double_t x0=rex[is];
          y0=g1/bw1_d0*(bw1_n0*x0+bw1_n1*x1)-bw1_d1/bw1_d0*y1;
          x1=x0;
          y1=y0;
          rex[is]=y0;
        }
      }   

// BW2 :
      if(LPF>0 && BW_order==2)
      {   
        y2=0.; y1=0.; y0=0.;
        x2=0.; x1=0.; x0=0.;
        for(int is=0; is<nsample; is++)
        {
          Double_t x0=rex[is];
          y0=g2/bw2_d0*(bw2_n0*x0+bw2_n1*x1+bw2_n2*x2)-bw2_d1/bw2_d0*y1-bw2_d2/bw2_d0*y2;
          x2=x1; x1=x0;
          y2=y1; y1=y0;
          rex[is]=y0;
        }
      }   

// BW4 :
      if(LPF>0 && BW_order==4)
      {   
        y4=0.; y3=0.; y2=0.; y1=0.; y0=0.;
        x4=0.; x3=0.; x2=0.; x1=0.; x0=0.;
        for(int is=0; is<nsample; is++)
        {
          Double_t x0=rex[is];
          y0=g4/bw4_d0*(bw4_n0*x0+bw4_n1*x1+bw4_n2*x2+bw4_n3*x3+bw4_n4*x4)-bw4_d1/bw4_d0*y1-bw4_d2/bw4_d0*y2-bw4_d3/bw4_d0*y3-bw4_d4/bw4_d0*y4;
          x4=x3; x3=x2; x2=x1; x1=x0;
          y4=y3; y3=y2; y2=y1; y1=y0;
          rex[is]=y0;
        }

      }
 
      //if(rms[ich]>1.8e-3)continue;
      fft_f1->SetPointsComplex(rex, imx);
      fft_f1->Transform();
      fft_f1->GetPointsComplex(rey, imy);

      rms[ich]=0.;
      hf_rms[ich]=0.;
      vhf_rms[ich]=0.;
      for(int is=0; is<nsample; is++)
      {
        Double_t f=df*(0.5+is);
        rey[is]/=nsample;
        imy[is]/=nsample;
        mod[is]=rey[is]*rey[is]+imy[is]*imy[is]; 
        int ibad=0;
        //if(f>37e6 && f<40e6)ibad=1;
        //if(f>74e6 && f<86e6)ibad=1;
        //if(f>120e6 && f<123e6)ibad=1;
        if(ibad==0)
        if(is>0)rms[ich]+=mod[is];
        if(f>250000. && f<fmax-250000.) hf_rms[ich]+=mod[is];
        if(f>1500000. && f<fmax-1500000.) vhf_rms[ich]+=mod[is];
        mod[is]=sqrt(mod[is])/sqrt(df); // [V/sqrt(Hz)]
        phase[is]=atan2(imy[is],rey[is]);
        hmod_loc[ich]->SetBinContent(is+1,mod[is]);
        //if(rms[ich]<2.0e-3)pmod[ich]->Fill(df*is,mod[is]);
        pmod[ich]->Fill(f,mod[is]*mod[is]); // We sum the power spectra
      }
      rms[ich]=sqrt(rms[ich]);
      hf_rms[ich]=sqrt(hf_rms[ich]);
      vhf_rms[ich]=sqrt(vhf_rms[ich]);
      printf("rms_f_hf_vhf %.3e %.3e %.3e\n",rms[ich],hf_rms[ich], vhf_rms[ich]);
    }

    if(debug>=0 || ievt==1)
    {
      cframe->cd();
      cframe->Update();
      tg[0]->DrawClone("alp");
      gPad->SetGridx();
      gPad->SetGridy();
      tg[0]->GetXaxis()->SetTitle("time [ns]");
      tg[0]->GetYaxis()->SetTitle("amplitude [mV]");
      cframe->Update();
    }
  }

  TLatex *tex=new TLatex();
  printf("Final : \n");
  for( int i=0; i<n_active_channel; i++)
  {
    int ich=channel_number[i];
    printf("ch%d, rms (uV) = ",ich);
    nsample=nsample_tot;
    dt=1.e-6/NMSPS;
    fmax=1./dt;
    df=fmax/nsample;

    Double_t mod;
    rms[ich]=0.;
    hf_rms[ich]=0.;
    vhf_rms[ich]=0.;
    for(int is=0; is<=nsample/2; is++)
    {
      Double_t f=df*is;
      mod=pmod[ich]->GetBinContent(is+1);
      if(convert==1)
      {
        if(G12==1)
          hmod[ich]->SetBinContent(is+1,sqrt(mod*2.)/G1/G12_val*1.e15); // We fold positive and negative frequencies fC/sqrt(Hz)
        else
          hmod[ich]->SetBinContent(is+1,sqrt(mod*2.)/G1*1.e15); // We fold positive and negative frequencies fC/sqrt(Hz)
      }
      else
        hmod[ich]->SetBinContent(is+1,sqrt(mod*2.)*1.e9); // We fold positive and negative frequencies nV/sqrt(Hz)
      if(is>0)rms[ich]+=mod*df*2.;
      if(f>250000.) hf_rms[ich]+=mod*df*2.;
      if(f>1500000.) vhf_rms[ich]+=mod*df*2.;
    }
    rms[ich]=sqrt(rms[ich]);
    hf_rms[ich]=sqrt(hf_rms[ich]);
    vhf_rms[ich]=sqrt(vhf_rms[ich]);
    printf("%.3e %.3e %.3e, ",rms[ich]*1.e6,hf_rms[ich]*1.e6,vhf_rms[ich]*1.e6);
    cmod->cd(gLoc[ich]);
    cmod->Update();
    hmod[ich]->Draw();
    //sprintf(title,"NDS ch %d",ich);
    //hmod[ich]->SetTitle(title);
    hmod[ich]->SetLineColor(kBlue);
    hmod[ich]->SetLineWidth(2.);
    hmod[ich]->SetTitleSize(0.05);
    hmod[ich]->SetTitleFont(62);
    hmod[ich]->GetXaxis()->SetRangeUser(df,fmax/2.);
    //hmod[ich]->GetXaxis()->SetRangeUser(2.e3,1.e7);
    hmod[ich]->GetXaxis()->SetTitle("frequency [Hz]");
    hmod[ich]->GetXaxis()->SetTitleSize(0.05);
    hmod[ich]->GetXaxis()->SetLabelSize(0.05);
    hmod[ich]->GetXaxis()->SetTitleFont(62);
    hmod[ich]->GetXaxis()->SetLabelFont(62);
    if(convert==1)
      hmod[ich]->GetYaxis()->SetTitle("noise density [fC/#sqrt{Hz}]");
    else
      hmod[ich]->GetYaxis()->SetTitle("noise density [nV/#sqrt{Hz}]");
    hmod[ich]->GetYaxis()->SetTitleSize(0.05);
    hmod[ich]->GetYaxis()->SetLabelSize(0.05);
    hmod[ich]->GetYaxis()->SetTitleOffset(1.1);
    hmod[ich]->GetYaxis()->SetTitleFont(62);
    hmod[ich]->GetYaxis()->SetLabelFont(62);
    hmod[ich]->SetMinimum(0.);
    if(G12==1)
    {
      hmod[ich]->SetMaximum(noise_max);
      sprintf(title,"Total noise = %.0f #muV, %.2f fC",rms[ich]*1.e6,rms[ich]/G12_val/G1*1.e15);
    }
    else
    {
      hmod[ich]->SetMaximum(noise_max);
      sprintf(title,"Total noise = %.0f #muV, %.1f fC",rms[ich]*1.e6,rms[ich]/G1*1.e15);
    }
    tex->DrawLatexNDC(0.35,0.85,title);
    //sprintf(title,"HF noise     = %.0f #muV",hf_rms[ich]*1.e6);
    //tex->DrawLatexNDC(0.5,0.80,title);
    //sprintf(title,"VHF noise = %.0f #muV",vhf_rms[ich]*1.e6);
    //tex->DrawLatexNDC(0.5,0.75,title);
    gPad->SetGridx();
    gPad->SetGridy();
    gPad->SetLogx();
    gPad->Modified();
    cmod->Update();
    printf("\n");
  }

  nsample_fft=nsample_tot;

  sprintf(hname,"%s",fname);
  char *pos=strstr(hname,".root");
  sprintf(pos,"_analized.root");
  printf("Output file : %s\n",hname);
  TFile *tfout=new TFile(hname,"recreate");
  sprintf(pos,"_analized.txt");
  FILE *fout=fopen(hname,"w+");
  cframe->Write();
  //tc_rms->Write();
  cmod->Write();
  for(int i=0; i<n_active_channel; i++)
  {
    int ich=channel_number[i];
    hmean[ich]->Write();
    hrms[ich]->Write();
    hmod[ich]->Write();
    pmod[ich]->Write();
    fprintf(fout,"%.3e %.3e ",rms[ich],hf_rms[ich]);
  }
  fprintf(fout,"\n");
  tfout->Close();
  fclose(fout);
}
