#include <TApplication.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TF1.h>
#include <TProfile.h>
#include <TRandom.h>
#include <TTree.h>
#include <TFile.h>
#include <TStyle.h>
#include <TVirtualFFT.h>

#define NLOOP 1
#define NSTEP 50
#define NSAMPLE_FFT 64
#define SHAPE_SIZE 10000

void ana_laser (char *fname, Int_t F_sampling=160, int debug=0, int ch_draw=2, Double_t E_ref=40.)
{
  double loc_fmax=F_sampling;
  double loc_dt=1000./F_sampling;
  int id;
  int ievt0=0;
  short int *sevent[5];
  double *event[5];
  char hname[250];
  double dv=1750./16384.; // AD9642
  int ch_ref=3;
  int ch_min=3;
  int ch_max=4;
  TRandom *tr=new TRandom();
  //char fname[132];
  double pi=asin(1.)*2.;
  gStyle->SetOptStat(0);
  TProfile *pshape[5];
  TH1D *hshape[5],*hshapen[5];
  Int_t active[5]={0,0,0,1,1};
  Int_t ch_first;
  for(ch_first=0; active[ch_first]==0; ch_first++);


  TCanvas *c1=new TCanvas("c1","c1",1000.,800.);
  c1->Divide(2,2);

  printf("Opening %s for analysis\n",fname);
  TFile *infile=new TFile(fname);
  TTree *tdata=(TTree*)infile->Get("data");
  tdata->SetBranchAddress("id",&id);

// Try to get frame size :
  char title[80];
  sprintf(title,"%s",tdata->GetBranch("ch0")->GetTitle());
  int nsample=0;
  sscanf(title,"ch0[%d]/S",&nsample);
  printf("Setting branches for %d samples\n",nsample);
  for(int ich=0; ich<5; ich++)
  {
    sevent[ich]=(short int*) malloc(nsample*sizeof(short int));
    event[ich]=(double*) malloc(nsample*sizeof(double));
    printf("Channel %d\n",ich);
    char bname[80];
    sprintf(bname,"ch%d",ich);
    tdata->SetBranchAddress(bname,sevent[ich]);
    //printf("sample %d\n",is);
  }

  Double_t tmax=loc_dt*nsample;
  for(int ich=0; ich<5; ich++)
  {
    if(active[ich]==0)continue;
    sprintf(hname,"ch_%d_step_0_3200",ich);
    pshape[ich]=(TProfile*)gDirectory->GetObjectUnchecked(hname);
    sprintf(hname,"pshape_ch%d",ich);
    pshape[ich]->SetName(hname);
    pshape[ich]->SetTitle(hname);
    sprintf(hname,"hshape_ch%d",ich);
    hshape[ich]=new TH1D(hname,hname,nsample,0.,tmax);
    sprintf(hname,"normalized_shape_ch%d",ich);
    hshapen[ich]=new TH1D(hname,hname,nsample,0.,tmax);
    pshape[ich]->Fit("pol0","Q","",0.,600.);
    TF1 *p0=pshape[ich]->GetFunction("pol0");
    Double_t ped=p0->GetParameter(0);
    Double_t qmax=0.;
    for(Int_t i=0; i<nsample; i++)
    {
      Double_t val=pshape[ich]->GetBinContent(i+1);
      hshape[ich]->SetBinContent(i+1,val-ped);
      if(val-ped>qmax)qmax=val-ped;
    }
    for(Int_t i=0; i<nsample; i++)
    {
      Double_t val=hshape[ich]->GetBinContent(i+1);
      hshapen[ich]->SetBinContent(i+1,val/qmax);
    }
  }

  int nevt=(Int_t)tdata->GetEntries();
  //nevt=200;


  sprintf(hname,"%s",fname);
  char *pos=strstr(hname,".root");
  sprintf(pos,"_analized.root");
  printf("Output file : %s\n",hname);
  TFile *tfout=new TFile(hname,"recreate");

  for(int ich=0; ich<5; ich++)
  {
    if(active[ich]==0)continue;
    pshape[ich]->SetMarkerColor(kRed);
    pshape[ich]->SetLineColor(kRed);
    pshape[ich]->Write();
    hshape[ich]->SetMarkerColor(kRed);
    hshape[ich]->SetLineColor(kRed);
    hshape[ich]->Write();
    hshapen[ich]->SetMarkerColor(kRed);
    hshapen[ich]->SetLineColor(kRed);
    hshapen[ich]->Write();
  }

  tfout->Close();

  //sprintf(pos,"_analized.txt");
  //FILE *fout=fopen(hname,"w+");
  //for(int ich=0; ich<5; ich++) fprintf(fout,"%e ",f3db[ich]);
  //fprintf(fout,"\n");
  //for(int ich=0; ich<5; ich++) fprintf(fout,"%e ",trise[ich]);
  //fprintf(fout,"\n");
  //fclose(fout);
  
}
