double my_sine(double *x, double *par)
{
  double ped[4], amp[4],f,phase;
  static double pi=asin(1.l)*2.l;
  double t=x[0];
  int it=int(t/6.25e-9);
  int isub4=it%4;
  int isub2=it%2;
  ped[0]=par[0];
  ped[1]=par[1];
  ped[2]=par[2];
  ped[3]=par[3];
  amp[0]=par[4];
  amp[1]=par[5];
  amp[2]=par[6];
  amp[3]=par[7];
  f=par[8];
  phase=par[9]/180.*pi;
  double y=ped[isub4]+amp[isub4]*sin(2.*pi*f*t+phase);
  return y;
}

void ana_DTU(TString fname, double f=500.e3, int correct=0, int type=2, int MC=0, double thermal_noise=0.0004, double apperture_jitter=1.e-12)
{
  char *line=NULL;
  size_t len;
  int eof=0;

  FILE *fin=NULL;
  fin=fopen(fname.Data(),"r");
  if(fin==NULL)
  {
    printf("Unable to open file : %s, stop here !\n",fname.Data());
    return -1;
  }
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(111);
  TRandom *tr=new TRandom();
  int n=0, val;
  double t=0.;
  double dt=6.25e-9;
  double dv=1.2/4096;
  TGraphErrors *tg_MC=new TGraphErrors();
  TGraphErrors *tg=new TGraphErrors();
  TGraphErrors *tge_orig=new TGraphErrors();
  TGraphErrors *tge=new TGraphErrors();
  TGraphErrors *tge1=new TGraphErrors();
  TGraphErrors *tge2=new TGraphErrors();
  TGraphErrors *tgo=new TGraphErrors();
  TGraphErrors *tgo1=new TGraphErrors();
  TGraphErrors *tgo2=new TGraphErrors();
  TGraph *tg_res=new TGraph();
  TGraph *tgr_orig=new TGraph();
  TH1D *tpd=new TH1D("DNL_vs_amplitude","DNL vs amplitude",4096,0.,4096.);
  TProfile *tpi=new TProfile("INL_vs_amplitude","INL vs amplitude",4096,0.,4096.,"");
  TProfile *tps=new TProfile("sresidual_vs_amplitude","sresidual vs amplitude",40,0.,1.2,"S");
  TProfile *tpv=new TProfile("residual_vs_dvdt","residual vs dvdt",200,0.,2.e8,"S");
  TH1D *hs=new TH1D("error_vs_amplitude","Error vs amplitude",40,0.,1.2);
  TH1D *hv=new TH1D("error_vs_dvdt","Error vs dvdt",200,0,2.e8);
  TH1D *hres=new TH1D("samples_residuals","Even samples residuals",1000,-10e-3,10.e-3);
  double tmax=0.;
  TCanvas *ctemp=new TCanvas();
  ctemp->cd();
  ctemp->Update();
  double max=0., min=1.2;
  while((eof=getline(&line, &len, fin)) != EOF)
  {
    val=0;
    if(line[0]=='#') continue;
    for(int i=0; i<12; i++)
    {
      val=(val<<1);
      if(line[i]=='1')val++;
    }
    //if(line[0]=='1')val |= 0xfffff000;
    tg->SetPoint(n,t,dv*val);
    tg->SetPointError(n,5.e-12,dv);
    if((n%4)==0)
    {
      tge_orig->SetPoint(n/2,t,dv*val);
      tge_orig->SetPointError(n/2,5.e-12,1.5*dv);
      tge->SetPoint(n/2,t,dv*val);
      tge->SetPointError(n/2,5.e-12,1.5*dv);
      tge1->SetPoint(n/4,t,dv*val);
      tge1->SetPointError(n/4,5.e-12,1.5*dv);
    }
    else if((n%4)==1)
    {
      tgo->SetPoint(n/2,t,dv*val);
      tgo->SetPointError(n/2,5.e-12,dv);
      tgo1->SetPoint(n/4,t,dv*val);
      tgo1->SetPointError(n/4,5.e-12,dv);
    }
    else if((n%4)==2)
    {
      tge_orig->SetPoint(n/2,t,dv*val);
      tge_orig->SetPointError(n/2,5.e-12,1.5*dv);
      tge->SetPoint(n/2,t,dv*val);
      tge->SetPointError(n/2,5.e-12,1.5*dv);
      tge2->SetPoint(n/4,t,dv*val);
      tge2->SetPointError(n/4,5.e-12,1.5*dv);
    }
    else if((n%4)==3)
    {
      tgo->SetPoint(n/2,t,dv*val);
      tgo->SetPointError(n/2,5.e-12,dv);
      tgo2->SetPoint(n/4,t,dv*val);
      tgo2->SetPointError(n/4,5.e-12,dv);
    }
    if(dv*val>max)max=dv*val;
    if(dv*val<min)min=dv*val;
    tmax=t;
    n++;
    t+=dt;
    //printf("%s : %d\n",line,val);
  }
  fclose(fin);
  tg->SetMarkerStyle(20);
  tg->SetMarkerSize(0.1);
  tg->SetMarkerColor(kRed);
  tg->SetLineColor(kRed);
  tge->SetMarkerStyle(20);
  tge->SetMarkerSize(0.2);
  tge->SetMarkerColor(kRed);
  tge->SetLineColor(kRed);
  tgo->SetMarkerStyle(20);
  tgo->SetMarkerSize(0.2);
  tgo->SetMarkerColor(kBlue);
  tgo->SetLineColor(kBlue);

  double pi=2.0l*asin(1.0l);
  TF1 *f1_1=new TF1("my_f1",my_sine,0.,2.e-3,10);
  f1_1->SetNpx(10000);
  TF1 *f1,*f1_2;
  if(f>0.01)
    f1_2=new TF1("f1","[0]+[1]*sin(2.*[4]*[2]*x+[3]/180.*[4])",0.,2.e-3);
  else
    f1_2=new TF1("f1","[0]",0.,1.e-3);
  f1=f1_2;
  f1->SetNpx(10000);
  f1->SetParameter(0,0.6);
  f1->SetParameter(1,0.6);
  f1->SetParameter(2,f);
  f1->SetParameter(3,0.);
  f1->FixParameter(4,pi);
  TF1 *f2=new TF1("f1","[0]*2.*[3]*[1]*cos(2.*[3]*[1]*x+[2]/180.*[3])",0.,2.e-3);
  f2->SetNpx(10000);
  TF1 *f1n;
  f1n=new TF1("f1n","[0]+[1]*sin(2.*[4]*[2]*x+[3]/180.*[4])+[5]*sin(2*[4]*[6]*x+[7]/180.*[4])",0.,2.e-3);

// Analyse and clean odd samples :
  int no=tgo->GetN();
  f1->FixParameter(0,(max+min)/2.);
  f1->FixParameter(1,(max-min)/2.);
  f1->FixParameter(2,f);
  f1->SetParLimits(3,-270.,270.);
  f1->SetParameter(3,0.);
  tgo->Fit(f1,"Q","",0.,100./f);
  f1->ReleaseParameter(2);
  tgo->Fit(f1,"Q","",0.,100./f);
  double pedo=f1->GetParameter(0);
  double ampo;
  double phaseo=f1->GetParameter(3);
  if(phaseo> 180.)phaseo-=360.;
  if(phaseo<-180.)phaseo+=360.;
  f1->SetParameter(3,phaseo);
  f1->SetParLimits(3,phaseo-2.,phaseo+2.);
  f1->ReleaseParameter(0);
  f1->ReleaseParameter(1);
  tgo->Fit(f1,"Q","",0.,tmax);
  pedo=f1->GetParameter(0);
  if(f>0.01)
  {
    ampo=f1->GetParameter(1);
    phaseo=f1->GetParameter(3);
    f=f1->GetParameter(2);
  }
  else
  {
    ampo=1.;
    f=0.;
    phaseo=0.;
  }
  printf("Odd samples : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f deg\n",pedo,ampo,f,phaseo);
  tgo1->Fit(f1,"Q","",0.,tmax);
  double pedo1=f1->GetParameter(0);
  double ampo1=f1->GetParameter(1);
  double fo1=f1->GetParameter(2);
  double phaseo1=f1->GetParameter(3);
  printf("Odd samples 1 : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pedo1,ampo1,fo1,phaseo1);
  tgo2->Fit(f1,"Q","",0.,tmax);
  double pedo2=f1->GetParameter(0);
  double ampo2=f1->GetParameter(1);
  double fo2=f1->GetParameter(2);
  double phaseo2=f1->GetParameter(3);
  printf("Odd samples 3 : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pedo2,ampo2,fo2,phaseo2);
  printf("Delta phase odd 3 vs 1 : %e s\n",(phaseo2-phaseo1)/180*pi/(2*pi*f));
  printf("Delta Vcm odd 3 vs 1 : %e V\n",pedo2-pedo1);
  printf("Delta gain odd 3 vs 1 : %e\n",ampo2/ampo1);
  if(correct>1)
  {
    for(int i=0; i<n; i++)
    {
      double t,y,et,ey;
      if((i%4)==1)
      {
        tgo1->GetPoint(i/4,t,y);
        ey=tgo1->GetErrorY(i/4);
        tgo->SetPoint(i/2,t,y);
        tgo->SetPointError(i/2,et,ey);
      }
      else if ((i%4)==3)
      {
        tgo2->GetPoint(i/4,t,y);
        ey=tgo2->GetErrorY(i/4);
        tgo->SetPoint(i/2,t,(y-pedo2)*ampo1/ampo2+pedo1);
        tgo->SetPointError(i/2,et,ey);
      }
    }
  }
  tgo->Fit(f1,"Q","",0.,tmax);
  pedo=f1->GetParameter(0);
  if(f>0.01)
  {
    ampo=f1->GetParameter(1);
    phaseo=f1->GetParameter(3);
    f=f1->GetParameter(2);
  }
  else
  {
    ampo=1.;
    f=0.;
    phaseo=0.;
  }
  printf("Odd samples corrected : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pedo,ampo,f,phaseo);
// Compute residuals
  for(int i=0; i<no; i++)
  {
    double t,y,et,ey;
    tgo->GetPoint(i,t,y);
    double val=f1->Eval(t);
    double dy=y-val;
    hres->Fill(dy);
  }

// Analyse and clean even samples :
  int ne=tge->GetN();
  f1->FixParameter(0,(max+min)/2.);
  f1->FixParameter(1,(max-min)/2.);
  f1->FixParameter(2,f);
  f1->SetParLimits(3,-270.,270.);
  f1->SetParameter(3,0.);
  tge->Fit(f1,"Q","",0.,100./f);
  f1->ReleaseParameter(2);
  tge->Fit(f1,"Q","",0.,100./f);
  double pede=f1->GetParameter(0);
  double ampe;
  double phasee=f1->GetParameter(3);
  if(phasee> 180.)phasee-=360.;
  if(phasee<-180.)phasee+=360.;
  f1->SetParameter(3,phasee);
  f1->SetParLimits(3,phasee-2.,phasee+2.);
  f1->ReleaseParameter(0);
  f1->ReleaseParameter(1);
  tge->Fit(f1,"Q","",0.,tmax);
  pede=f1->GetParameter(0);
  if(f>0.01)
  {
    ampe=f1->GetParameter(1);
    phasee=f1->GetParameter(3);
    f=f1->GetParameter(2);
  }
  else
  {
    ampe=1.;
    f=0.;
    phasee=0.;
  }
  printf("Even samples : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pede,ampe,f,phasee);

  tge1->Fit(f1,"Q","",0.,tmax);
  double pede1=f1->GetParameter(0);
  double ampe1=f1->GetParameter(1);
  double fe1=f1->GetParameter(2);
  double phasee1=f1->GetParameter(3);
  printf("Even samples 0 : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pede1,ampe1,fe1,phasee1);
  tge2->Fit(f1,"Q","",0.,tmax);
  double pede2=f1->GetParameter(0);
  double ampe2=f1->GetParameter(1);
  double fe2=f1->GetParameter(2);
  double phasee2=f1->GetParameter(3);
  printf("Even samples 2 : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pede2,ampe2,fe2,phasee2);
  printf("Delta phase even 2 vs 0 : %e s\n",(phasee2-phasee1)/180*pi/(2*pi*f));
  printf("Delta Vcm even 2 vs 0 : %e V\n",pede2-pede1);
  printf("Delta gain even 2 vs 0 : %e\n",ampe2/ampe1);
  if(correct>1)
  {
    for(int i=0; i<n; i++)
    {
      double t,y,et,ey;
      if((i%4)==0)
      {
        tge1->GetPoint(i/4,t,y);
        ey=tge1->GetErrorY(i/4);
        tge->SetPoint(i/2,t,y);
        tge->SetPointError(i/2,et,ey);
      }
      else if ((i%4)==2)
      {
        tge2->GetPoint(i/4,t,y);
        ey=tge2->GetErrorY(i/4);
        tge->SetPoint(i/2,t,(y-pede2)*ampe1/ampe2+pede1);
        tge->SetPointError(i/2,et,ey);
      }
    }
  }
  tge->Fit(f1,"","",0.,tmax);
  pede=f1->GetParameter(0);
  if(f>0.01)
  {
    ampe=f1->GetParameter(1);
    phasee=f1->GetParameter(3);
    f=f1->GetParameter(2);
  }
  else
  {
    ampe=1.;
    f=0.;
    phasee=0.;
  }
  //tge->Draw("alp");
  //return;
  printf("Even samples corrected : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",pede,ampe,f,phasee);
// Compute residuals
  hres->Reset();
  for(int i=0; i<ne; i++)
  {
    double t,y,et,ey;
    tge->GetPoint(i,t,y);
    double val=f1->Eval(t);
    double dy=y-val;
    hres->Fill(dy);
  }
  // o*t+p0=o*(t+p0/o)=o*(t+p0/(2pif)
  double dphase=(phaseo-phasee)/180*pi/(2*pi*f);
  printf("Delta phase : %e s\n",dphase);
  printf("Delta Vcm : %e V\n",pedo-pede);
  printf("Delta gain : %e\n",ampo/ampe);

  if(correct>0)
  {
    for(int i=0; i<n; i++)
    {
      double t,y,et,ey;
      if(i%2==0)
      {
        tge->GetPoint(i/2,t,y);
        ey=tge->GetErrorY(i/2);
        tg->SetPoint(i,t,y);
        tg->SetPointError(i,et,ey);
      }
      else
      {
        tgo->GetPoint(i/2,t,y);
        ey=tgo->GetErrorY(i/2);
        tg->SetPoint(i,t,(y-pedo)*ampe/ampo+pede);
        tg->SetPointError(i,et,ey);
      }
    }
  }
  f1->FixParameter(0,(max+min)/2.);
  f1->FixParameter(1,(max-min)/2.);
  f1->FixParameter(2,f);
  f1->SetParLimits(3,-270.,270.);
  f1->SetParameter(3,0.);
  tg->Fit(f1,"Q","",0.,100./f);
  f1->ReleaseParameter(2);
  tg->Fit(f1,"Q","",0.,100./f);
  double ped=f1->GetParameter(0);
  double amp;
  double phaset=f1->GetParameter(3);
  if(phaset> 180.)phaset-=360.;
  if(phaset<-180.)phaset+=360.;
  f1->SetParameter(3,phaset);
  f1->SetParLimits(3,phaset-2.,phaset+2.);
  f1->ReleaseParameter(0);
  f1->ReleaseParameter(1);
  tg->Fit(f1,"Q","",0.,tmax);
  if(f>0.01)
  {
    amp=f1->GetParameter(1);
    f=f1->GetParameter(2);
    phaset=f1->GetParameter(3);
  }
  else
  {
    amp=1.;
    f=0.;
    phaset=0.;
  }
  printf("All samples : pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",ped,amp,f,phaset);
  if(correct==-99)
  {
    f1n->FixParameter(0,f1->GetParameter(0));
    f1n->FixParameter(1,f1->GetParameter(1));
    f1n->FixParameter(2,f1->GetParameter(2));
    f1n->FixParameter(3,f1->GetParameter(3));
    f1n->FixParameter(4,f1->GetParameter(4));
    f1n->SetParameter(5,1.);
    f1n->SetParameter(6,40.e6);
    f1n->SetParameter(7,0.);
    tg->Fit(f1n,"","",0.,tmax);
    //tg->Draw("alp");
    //return;
  }
  f1_1->SetParameter(0,ped);
  f1_1->SetParameter(1,ped);
  f1_1->SetParameter(2,ped);
  f1_1->SetParameter(3,ped);
  f1_1->SetParameter(4,amp);
  f1_1->SetParameter(5,amp);
  f1_1->SetParameter(6,amp);
  f1_1->SetParameter(7,amp);
  f1_1->SetParameter(8,f);
  f1_1->SetParameter(9,phaset);
  tg->Fit(f1_1,"","",0.,tmax);
  f1=f1_1;

  TGraphErrors *tg_loc;
  int n_loc;

  if(type==0)
  {
    tg_loc=tge;
    n_loc=ne;
  }
  else if (type==1)
  {
    tg_loc=tgo;
    n_loc=no;
  }
  else
  {
    tg_loc=tg;
    n_loc=n;
  }
  ctemp->Update();
  TCanvas *cres_orig=new TCanvas();
  cres_orig->cd();
  cres_orig->Update();
  tgr_orig->SetMarkerStyle(20);
  tgr_orig->SetMarkerSize(0.2);
  tgr_orig->SetMarkerColor(kRed);
  tgr_orig->SetLineColor(kRed);
  tgr_orig->Draw("alp");
  cres_orig->Update();
  TCanvas *c1_orig=new TCanvas();
  c1_orig->cd();
  c1_orig->Update();
  tg_loc->DrawClone("alp");
  //tgo->Draw("lp");
  c1_orig->Update();
  TCanvas *c1=new TCanvas();
  c1->cd();
  c1->Update();
  tg_loc->Draw("alp");
  c1->Update();

// Compute residuals
  hres->Reset();
  //tg_loc->Fit(f1,"Q","",0.,tmax);
  for(int i=0; i<n_loc; i++)
  {
    double t,y,et,ey;
    tg_loc->GetPoint(i,t,y);
    double val=f1->Eval(t);
    double dy=y-val;
    hres->Fill(dy);
    tg_res->SetPoint(i,t,dy);
  }
  TCanvas *c2=new TCanvas();
  c2->cd();
  c2->Update();
  tg_res->SetMarkerStyle(20);
  tg_res->SetMarkerSize(0.2);
  tg_res->SetMarkerColor(kRed);
  tg_res->SetLineColor(kRed);
  tg_res->Draw("alp");
  c2->Update();
  TCanvas *c3=new TCanvas();
  c3->cd();
  c3->Update();
  hres->SetLineColor(kRed);
  hres->DrawCopy();
  c3->Update();
  hres->Reset();

// Redo fit on requested samples (even, odd or both)
  tg_loc->Fit(f1,"Q","",0.,tmax);
  ped=f1_2->GetParameter(0);
  if(f>0.01)
  {
    amp=f1_2->GetParameter(1);
    f=f1_2->GetParameter(2);
    phaset=f1_2->GetParameter(3);
  }
  else
  {
    amp=1.;
    f=0.;
    phaset=0.;
  }
  printf("Selected fit, pedestal : %.6f, amplitude %.6f, frequency %.6f, phase %.3f\n",ped,amp,f,phaset);

  c3->cd();
  hres->SetLineColor(kGreen);
  hres->DrawCopy("same");
  c3->Update();

  if(f>0.01)
  {
    f2->SetParameter(0,f1_2->GetParameter(1));
    f2->SetParameter(1,f1_2->GetParameter(2));
    f2->SetParameter(2,f1_2->GetParameter(3));
    f2->SetParameter(3,f1_2->GetParameter(4));
  }
  else
  {
    f2->SetParameter(0,0.);
    f2->SetParameter(1,0.);
    f2->SetParameter(2,0.);
    f2->SetParameter(3,pi);
  }

  hres->Reset();
  for(int i=0; i<n_loc; i++)
  {
    double t,y,et,ey;
    tg_loc->GetPoint(i,t,y);
    double val=f1->Eval(t);
    double dy=y-val;
    hres->Fill(dy);
  }
  double rms_cut=hres->GetRMS()*3.;
  for(int i=0; i<n_loc; i++)
  {
    double t,y,et,ey;
    tg_loc->GetPoint(i,t,y);
    double val=f1->Eval(t);
    double dy=y-val;
// simulate noisy, jittery 12 bit ADC
    double t_MC=t+tr->Gaus(0.,apperture_jitter);
    double val_MC=f1->Eval(t_MC);
    val_MC+=tr->Gaus(0.,thermal_noise);
    val_MC=int((val_MC+dv/2.)/dv)*dv;
    tg_MC->SetPoint(i,t,val_MC);
    if(MC==1) dy=val_MC-val;
    tg_res->SetPoint(i,t,dy);
    double dvdt=fabs(f2->Eval(t));
    if(fabs(dy<3.*rms_cut))
    {
      tpi->Fill(y/dv,dy/dv);
      tps->Fill(y,dy);
      tpv->Fill(dvdt,dy);
    }
  }
  int nbin=tps->GetNbinsX();
  for(int i=0; i<nbin; i++)
  {
    double rms=tps->GetBinError(i+1);
    hs->SetBinContent(i+1,rms);
  }
  nbin=tpv->GetNbinsX();
  int nref=0;
  double dvref=tpv->GetBinError(nref+1);
  for(int i=0; i<nbin; i++)
  {
    double rms=tpv->GetBinError(i+1);
    hv->SetBinContent(i+1,rms);
  }

  c2->cd();
  tg_res->SetMarkerColor(kBlue);
  tg_res->SetLineColor(kBlue);
  tg_res->Draw("lp");
  c2->Update();
  c3->cd();
  hres->SetLineColor(kBlue);
  hres->DrawCopy("same");
  printf("residual RMS : %f, enob = %.2f bits\n",hres->GetRMS(),log(1.2/(hres->GetRMS()*sqrt(12)))/log(2));
  c3->Update();
  TCanvas *c4=new TCanvas("INL_DNL","INL_DNL", 800,1080);
  c4->Divide(1,2);
  c4->cd(1);
  tpi->SetLineColor(kBlue);
  tpi->SetMaximum(2.);
  tpi->SetMinimum(-2.);
  tpi->Draw("");
  gPad->SetGridx();
  gPad->SetGridy();
  tpi->GetXaxis()->SetTitle("amplitude [lsb]");
  tpi->GetXaxis()->SetTitleSize(0.05);
  tpi->GetXaxis()->SetTitleFont(62);
  tpi->GetXaxis()->SetLabelSize(0.05);
  tpi->GetXaxis()->SetLabelFont(62);
  tpi->GetYaxis()->SetTitle("INL [lsb]");
  tpi->GetYaxis()->SetTitleOffset(0.9);
  tpi->GetYaxis()->SetTitleSize(0.05);
  tpi->GetYaxis()->SetTitleFont(62);
  tpi->GetYaxis()->SetLabelSize(0.05);
  tpi->GetYaxis()->SetLabelFont(62);
  tpi->GetYaxis()->SetTitle("INL [lsb]");
  double y_old=tpi->GetBinContent(1);
  double ey=tpi->GetBinError(1);
  tpd->SetBinContent(1,y_old);
  tpd->SetBinError(1,ey);
  for(int i=1; i<4096; i++)
  {
    double y=tpi->GetBinContent(i+1);
    double ey=tpi->GetBinError(i+1);
    tpd->SetBinContent(i+1,y-y_old);
    tpd->SetBinError(i+1,ey);
    y_old=y;
  }
  c4->cd(2);
  tpd->SetLineColor(kBlue);
  tpd->SetMaximum(2.);
  tpd->SetMinimum(-2.);
  tpd->Draw("");
  gPad->SetGridx();
  gPad->SetGridy();
  tpd->GetXaxis()->SetTitle("amplitude [lsb]");
  tpd->GetXaxis()->SetTitleSize(0.05);
  tpd->GetXaxis()->SetTitleFont(62);
  tpd->GetXaxis()->SetLabelSize(0.05);
  tpd->GetXaxis()->SetLabelFont(62);
  tpd->GetYaxis()->SetTitle("DNL [lsb]");
  tpd->GetYaxis()->SetTitleOffset(0.9);
  tpd->GetYaxis()->SetTitleSize(0.05);
  tpd->GetYaxis()->SetTitleFont(62);
  tpd->GetYaxis()->SetLabelSize(0.05);
  tpd->GetYaxis()->SetLabelFont(62);
  tpd->GetYaxis()->SetTitle("INL [lsb]");

  c4->Update();
  TCanvas *c5=new TCanvas();
  hs->SetLineColor(kBlue);
  hs->Draw("");
  c5->Update();
  TCanvas *c6=new TCanvas();
  TF1 *f3=new TF1("f3","sqrt([0]*[0]+[1]*[1]*x*x)",0.,1.e9);
  f3->SetParameter(0,0.0004);
  f3->SetParameter(1,16.e-12);
  hv->SetLineColor(kBlue);
  hv->Fit(f3,"","");
  hv->SetMinimum(0.);
  hv->SetMaximum(0.003);
  hv->GetXaxis()->SetRangeUser(0.,180.e6);
  gPad->SetGridx();
  gPad->SetGridy();
  hv->GetXaxis()->SetLabelSize(0.05);
  hv->GetXaxis()->SetLabelFont(62);
  hv->GetXaxis()->SetTitleSize(0.05);
  hv->GetXaxis()->SetTitleFont(62);
  hv->GetXaxis()->SetTitle("dV/dt [V/s]");
  hv->GetYaxis()->SetLabelSize(0.04);
  hv->GetYaxis()->SetLabelFont(62);
  hv->GetYaxis()->SetTitleSize(0.05);
  hv->GetYaxis()->SetTitleFont(62);
  hv->GetYaxis()->SetTitleOffset(1.00);
  hv->GetYaxis()->SetTitle("measurement error [V]");
  double th_noise=f3->GetParameter(0);
  printf("Thermal noise : %f, enob = %.2f bits\n",th_noise,log(1.2/(th_noise*sqrt(12)))/log(2));
  c6->Update();
  
  TF1 *fHann=new TF1("Hann","(x>0 && x<[0])?0.5-0.5*cos(2*3.141592654*x/[0]):0.",0.,1.e-3);
  TF1 *fHamming=new TF1("Hamming","(x>0 && x<[0])?0.54-0.46*cos(2*3.141592654*x/[0]):0.",0.,1.e-3);
  TF1 *fflat=new TF1("Flat","(x>0 && x<[0])?1.:0.",0.,1.e-3);
  double Hann_cor=1.63;
  double flat_cor=1.00;
  double Hamming_cor=1.59;
  fHann->SetParameter(0,tmax);
  fHamming->SetParameter(0,tmax);
  fflat->SetParameter(0,tmax);
  int size = n_loc;
  TVirtualFFT *fft_f = TVirtualFFT::FFT(1, &size, "C2CF M K");
  TCanvas *cmod=new TCanvas();
  TCanvas *cphase=new TCanvas();
  Double_t *mod=(double *)malloc(n_loc*sizeof(double));
  Double_t *phase=(double *)malloc(n_loc*sizeof(double));
  Double_t *rex=(double *)malloc(n_loc*sizeof(double));
  Double_t *imx=(double *)malloc(n_loc*sizeof(double));
  Double_t *rey=(double *)malloc(n_loc*sizeof(double));
  Double_t *imy=(double *)malloc(n_loc*sizeof(double));
  for(int i=0; i<n_loc; i++)
  {
    double t,y;
    tg_res->GetPoint(i,t,y);
    rex[i]=y*fHann->Eval(t);
    imx[i]=0.;
  }
  fft_f->SetPointsComplex(rex, imx);
  fft_f->Transform();
  fft_f->GetPointsComplex(rey, imy);
  double df=1./tmax;
  double fmax=n_loc*df;
  TH1D *hmod=new TH1D("noise_modulus","noise_modulus",n_loc,0.,fmax);
  TH1D *hphase=new TH1D("noise_phase","noise_phase",n_loc,0.,fmax);
  double rms_fft=0.;
  for(int i=0; i<n_loc; i++)
  {
    Double_t f=df*i;
    rey[i]/=n_loc;
    imy[i]/=n_loc;
    mod[i]=sqrt((rey[i]*rey[i]+imy[i]*imy[i])/df)*Hann_cor;
    phase[i]=atan2(imy[i],rey[i]);
    //if(i>0 && fabs(f-40.e6)>0.1e6)rms_fft+=mod[i]*mod[i]*df;
    if(i>0)rms_fft+=mod[i]*mod[i]*df;
    hmod->SetBinContent(i+1,mod[i]);
    hphase->SetBinContent(i+1,phase[i]);
  }
  rms_fft=sqrt(rms_fft);
  cmod->cd();
  hmod->Draw();
  hmod->GetXaxis()->SetRangeUser(0.,fmax/2.);
  gPad->SetGridx();
  gPad->SetGridy();
  hmod->GetXaxis()->SetLabelSize(0.05);
  hmod->GetXaxis()->SetLabelFont(62);
  hmod->GetXaxis()->SetTitleSize(0.05);
  hmod->GetXaxis()->SetTitleFont(62);
  hmod->GetXaxis()->SetTitle("frequency [Hz]");
  hmod->GetYaxis()->SetLabelSize(0.04);
  hmod->GetYaxis()->SetLabelFont(62);
  hmod->GetYaxis()->SetTitleSize(0.05);
  hmod->GetYaxis()->SetTitleFont(62);
  hmod->GetYaxis()->SetTitleOffset(1.00);
  hmod->GetYaxis()->SetTitle("noise [V/#sqrt{Hz}]");
  printf("RMS from FFT : %f, enob = %.2f bits\n",rms_fft,log(1.2/(rms_fft*sqrt(12)))/log(2));
  cmod->Update();
  cphase->cd();
  hphase->Draw();
  hphase->GetXaxis()->SetRangeUser(0.,fmax/2.);
  gPad->SetGridx();
  gPad->SetGridy();
  cphase->Update();

  for(int i=0; i<size; i++)
  {
    double t,y;
    tg_loc->GetPoint(i,t,y);
    if(MC==1) tg_MC->GetPoint(i,t,y);
    rex[i]=y*fHann->Eval(t);
    imx[i]=0.;
  }
  fft_f->SetPointsComplex(rex, imx);
  fft_f->Transform();
  fft_f->GetPointsComplex(rey, imy);
  TH1D *hmods=new TH1D("signal_modulus","signal_modulus",size,0.,fmax);
  double mod_max=-1000.;
  for(int i=0; i<size; i++)
  {
    Double_t f=df*i;
    rey[i]/=size;
    imy[i]/=size;
    mod[i]=sqrt(rey[i]*rey[i]+imy[i]*imy[i])*Hann_cor;
    mod[i]=20.*log10(mod[i]);
    if(i>5 && i<size-5 && mod[i]>mod_max)mod_max=mod[i];
  }
  for(int i=0; i<size; i++)
  {
    mod[i]-=mod_max;
    hmods->SetBinContent(i+1,mod[i]);
  }
  TCanvas *csig=new TCanvas();
  hmods->Draw();
  hmods->GetXaxis()->SetRangeUser(0.,fmax/2.);
  gPad->SetGridx();
  gPad->SetGridy();
  hmods->GetXaxis()->SetLabelSize(0.05);
  hmods->GetXaxis()->SetLabelFont(62);
  hmods->GetXaxis()->SetTitleSize(0.05);
  hmods->GetXaxis()->SetTitleFont(62);
  hmods->GetXaxis()->SetTitle("frequency [Hz]");
  hmods->GetYaxis()->SetLabelSize(0.04);
  hmods->GetYaxis()->SetLabelFont(62);
  hmods->GetYaxis()->SetTitleSize(0.05);
  hmods->GetYaxis()->SetTitleFont(62);
  hmods->GetYaxis()->SetTitleOffset(1.00);
  hmods->GetYaxis()->SetTitle("normalized modulus [dB]");
  csig->Update();


  char fnout[132];
  sprintf(fnout,"histo/ana_%.3f.root",f);
  TFile *tfout=new TFile(fnout,"recreate");
  tge->Write();
  tge_orig->Write();
  tgo->Write();
  tg_MC->Write();
  tg->Write();
  tg_res->Write();
  tgr_orig->Write();
  tpi->Write();
  tpd->Write();
  tpv->Write();
  hs->Write();
  hv->Write();
  hres->Write();
  hmod->Write();
  hmods->Write();
  tfout->Close();
}
