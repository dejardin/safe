#include <stdio.h>
#include <stdlib.h>
#include "TPad.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TProfile.h"
void Display_calib(int seq=1)
{
  long int time_stamp[32]={
1580736096,
1580736405,

1580804772,
1580804987,

1580892279,
1580892449,

1580979792,
1580979943,

1581064523,
1581064674,

1581325507,
1581325654,

1581410405,
1581410629,

1581502430,
1581502565,

1581583410,
1581583564,
//1581584131,

1581668945,
1581669084,

1581929088,
1581929403,

1582018750,
1582019186,

1582117172,
1582117380,
//1582117630,

1582186826,
1582187003,

1582276775,
1582276963,
//1582277158,

1582535583,
1582535734
}

  Int_t color=kBlue;
  char *line=NULL;
  size_t len;
  char fname[132];
  Double_t Temp[11];
  Int_t nVref;
  Int_t nDAC;
  Double_t DAC_val[65], Vdac[10][65];
  Double_t Vdac_slope[10];
  Int_t nPed;
  Double_t Ped_val[5][32];
  Double_t Ped_offset[5], Ped_slope[5];
  Double_t Noise_G1[4][5], Noise_G10[4][5];
  Double_t INL_min[5],INL_max[5];
  Double_t CATIA_gain[2][5];
  Double_t tpos[5], tjitter[5], rt[5], ert[5] ;
  TH1D *hPed_offset_G1=new TH1D("Ped_offset_G1","Ped_offset_G1",100,-620.,-520.);
  hPed_offset_G1->SetLineColor(color);
  hPed_offset_G1->SetLineWidth(2);
  TH1D *hPed_offset_G10=new TH1D("Ped_offset_G10","Ped_offset_G10",100,-620.,-520.);
  hPed_offset_G10->SetLineColor(color);
  hPed_offset_G10->SetLineWidth(2);
  TH1D *hPed_slope_G1=new TH1D("Ped_slope_G1","Ped_slope_G1",100,-3.,-2.);
  hPed_slope_G1->SetLineColor(color);
  hPed_slope_G1->SetLineWidth(2);
  TH1D *hPed_slope_G10=new TH1D("Ped_slope_G10","Ped_slope_G10",100,-3.,-2.);
  hPed_slope_G10->SetLineColor(color);
  hPed_slope_G10->SetLineWidth(2);
  TH1D *hDAC1_slope=new TH1D("DAC1_slope","DAC1_slope",100,0.20,0.30);
  hDAC1_slope->SetLineColor(color);
  hDAC1_slope->SetLineWidth(2);
  TH1D *hDAC2_slope=new TH1D("DAC2_slope","DAC2_slope",100,0.20,0.30);
  hDAC2_slope->SetLineColor(color);
  hDAC2_slope->SetLineWidth(2);
  TH1D *hG10=new TH1D("G10","G10",100,9.,12.);
  hG10->SetLineColor(color);
  hG10->SetLineWidth(2);
  TH1D *hR400=new TH1D("R400","R400",100,400.,550.);
  hR400->SetLineColor(color);
  hR400->SetLineWidth(2);
  TH1D *hR500=new TH1D("R500","R500",100,500.,700.);
  hR500->SetLineColor(color);
  hR500->SetLineWidth(2);
  TH1D *hRconv_G1=new TH1D("Rconv_G1","Rconv_G1",100,260.,300.);
  hRconv_G1->SetLineColor(color);
  hRconv_G1->SetLineWidth(2);
  TH1D *hRconv_G10=new TH1D("Rconv_G10","Rconv_G10",100,2200.,2600.);
  hRconv_G10->SetLineColor(color);
  hRconv_G10->SetLineWidth(2);
  TH1D *hGain_G1_R500=new TH1D("Gain_G1_R500","Gain_G1_R500",100,500.,700.);
  hGain_G1_R500->SetLineColor(color);
  hGain_G1_R500->SetLineWidth(2);
  TH1D *hGain_G1_R400=new TH1D("Gain_G1_R400","Gain_G1_R400",100,400.,600.);
  hGain_G1_R400->SetLineColor(color);
  hGain_G1_R400->SetLineWidth(2);
  TH1D *hGain_G10_R500=new TH1D("Gain_G10_R500","Gain_G10_R500",100,5000.,7000.);
  hGain_G10_R500->SetLineColor(color);
  hGain_G10_R500->SetLineWidth(2);
  TH1D *hGain_G10_R400=new TH1D("Gain_G10_R400","Gain_G10_R400",100,4000.,6000.);
  hGain_G10_R400->SetLineColor(color);
  hGain_G10_R400->SetLineWidth(2);
  TH1D *hINL_G1=new TH1D("INL_G1","INL_G1",100,0.,2.);
  hINL_G1->SetLineColor(color);
  hINL_G1->SetLineWidth(2);
  TH1D *hINL_G10=new TH1D("INL_G10","INL_G10",100,0.,2.);
  hINL_G10->SetLineColor(color);
  hINL_G10->SetLineWidth(2);
  TH1D *hoNoise_G1_R400_LPF35=new TH1D("Output_noise_G1_R400_LPF35","Output_noise_G1_R400_LPF35",100,0.,400.);
  hoNoise_G1_R400_LPF35->SetLineColor(color);
  hoNoise_G1_R400_LPF35->SetLineWidth(2);
  TH1D *hoNoise_G10_R400_LPF35=new TH1D("Output_noise_G10_R400_LPF35","Output_noise_G10_R400_LPF35",100,0.,2000.);
  hoNoise_G10_R400_LPF35->SetLineColor(color);
  hoNoise_G10_R400_LPF35->SetLineWidth(2);
  TH1D *hiNoise_G1_R400_LPF35=new TH1D("Input_noise_G1_R400_LPF35","Input_noise_G1_R400_LPF35",100,300.,600.);
  hiNoise_G1_R400_LPF35->SetLineColor(color);
  hiNoise_G1_R400_LPF35->SetLineWidth(2);
  TH1D *hiNoise_G10_R400_LPF35=new TH1D("Input_noise_G10_R400_LPF35","Input_noise_G10_R400_LPF35",100,100.,300.);
  hiNoise_G10_R400_LPF35->SetLineColor(color);
  hiNoise_G10_R400_LPF35->SetLineWidth(2);

  FILE *fd=NULL;
  Int_t eof,num=1;
  for(int imeas=0; imeas<16; imeas++)
  {
    Int_t ts=time_stamp[imeas*2+seq];
    sprintf(fname,"data/VFE_irrad/VFE_irrad_%d.txt",ts);
    printf("Look for %s file\n",fname);
    fd=fopen(fname,"r");
    if(fd==NULL){num--; break;}
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<11; i++)sscanf(line,"%lf",&Temp[i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<11; i++)sscanf(line,"%lf",&Temp[i]);
    sscanf(line,"%lf %lf %lf",&Temp[0],&Temp[1], &Temp[3]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%d",&nVref);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nVref; i++)sscanf(line,"%lf",&Vref[i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%d",&nDAC);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nDAC; i++)sscanf(line,"%lf",&DAC_val[i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nDAC; i++)sscanf(line,"%lf",&Vdac[0][i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nDAC; i++)sscanf(line,"%lf",&Vdac[1][i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf",&Vdac_slope[0], &Vdac_slope[1]);
    hDAC1_slope->Fill(Vdac_slope[0]);
    hDAC2_slope->Fill(Vdac_slope[1]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%d",&nPed);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nPed; i++)sscanf(line,"%lf",&Ped_val[0][i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    for(Int_t i=0; i<nPed; i++)sscanf(line,"%lf",&Ped_val[1][i]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf",&Ped_offset[0], &Ped_slope[0], &Ped_offset[1], &Ped_slope[1]);
    hPed_offset_G1->Fill(Ped_offset[0]);
    hPed_offset_G10->Fill(Ped_offset[1]);
    hPed_slope_G1->Fill(Ped_slope[0]);
    hPed_slope_G10->Fill(Ped_slope[1]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf %lf %lf %lf %lf",
           &Noise_G1[0][0], &Noise_G1[0][1], &Noise_G1[1][0], &Noise_G1[1][1],
           &Noise_G10[0][0], &Noise_G10[0][1], &Noise_G10[1][0], &Noise_G10[1][1]);
    hoNoise_G1_R400_LPF35->Fill(Noise_G1[1][1]); // uV RMS
    hoNoise_G10_R400_LPF35->Fill(Noise_G10[1][1]); // uV RMS
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf",&INL_min[0], &INL_max[0], &INL_min[1], &INL_max[1]);
    hINL_G1->Fill((INL_max[0]-INL_min[0])/2.*1000.);
    hINL_G10->Fill((INL_max[1]-INL_min[1])/2.*1000.);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf",&CATIA_gain[0][0], &CATIA_gain[0][1], &CATIA_gain[1][0], &CATIA_gain[1][1]);
    hGain_G1_R500->Fill(CATIA_gain[0][0]);
    hGain_G1_R400->Fill(CATIA_gain[0][1]);
    hGain_G10_R500->Fill(CATIA_gain[1][0]);
    hGain_G10_R400->Fill(CATIA_gain[1][1]);
    do((eof=getline(&line, &len, fd)) != EOF);while(line[0]=='#');
    sscanf(line,"%lf %lf %lf %lf %lf",&G10oG1, &R500, &R400, &Rconv_G1, &Rconv_G10);
    hG10->Fill(G10oG1);
    hR500->Fill(R500);
    hR400->Fill(R400);
    hRconv_G1->Fill(Rconv_G1);
    hRconv_G10->Fill(Rconv_G10);
    hiNoise_G1_R400_LPF35->Fill(Noise_G1[1][1]/CATIA_gain[0][1]*1000.); // nA RMS
    hiNoise_G10_R400_LPF35->Fill(Noise_G10[1][1]/CATIA_gain[1][1]*1000.); // nA RMS

    fclose(fd);
    fd=NULL;
    num++;
  }
  printf("End of scan : %d chips seen\n",num);
  sprintf(fname,"data/CATIA_calib/CATIA_calib_rev%d.root",rev);
  TFile *tf=new TFile(fname,"recreate");
  hG10->Write();
  hR400->Write();
  hR500->Write();
  hRconv_G1->Write();
  hRconv_G10->Write();
  hGain_G1_R500->Write();
  hGain_G1_R400->Write();
  hGain_G10_R500->Write();
  hGain_G10_R400->Write();
  hINL_G1->Write();
  hINL_G10->Write();
  hoNoise_G1_R400_LPF35->Write();
  hoNoise_G10_R400_LPF35->Write();
  hiNoise_G1_R400_LPF35->Write();
  hiNoise_G10_R400_LPF35->Write();
  hDAC1_slope->Write();
  hDAC2_slope->Write();
  hPed_offset_G1->Write();
  hPed_offset_G10->Write();
  hPed_slope_G1->Write();
  hPed_slope_G10->Write();
  tf->Close();

}
