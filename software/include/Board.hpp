#include "uhal/uhal.hpp"
#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>


#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define I2C_DEVICE      (1<<23)
#define I2C_REG         (1<<16)
#define NRETRY_MAX      100

using namespace uhal;

class board
{
  public:
    Int_t debug;

// VICEPP wide stuff :
    Int_t VICEPP_Number;
    Int_t VICEPP_eLinkActive;
    uhal::HwInterface hw;
    Int_t clock_source; // 0=local clock, 1=clock from FE, -1=default clock from switch
    Int_t Reset_VICEPP();
    Int_t Reset_delays();
    Int_t Set_AutoSync();

// VFE wide stuff (mainly ReSync commands) :
    Int_t Reset_VFE();
    Int_t VFE_Number;
    Int_t Reset_TestUnit();
    Int_t Reset_I2CUnit();
    Int_t Reset_ADC();
    Int_t Calibrate_ADC();

// CATIA specific stuff :
    Int_t CATIA_type[5];
    Int_t CATIA_ped_G10[5];
    Int_t CATIA_ped_G1[5];
    Int_t CATIA_LPF[5];
    Int_t CATIA_gain[5];
    Int_t CATIA_temp_out[5];
    Int_t CATIA_Reg1[5];
    Int_t CATIA_Reg2[5];
    Int_t CATIA_Reg3[5];
    Int_t CATIA_Reg4[5];
    Int_t CATIA_Reg5[5];
    Int_t CATIA_Reg6[5];

// LiTE-DTU specific stuff :
    Int_t LiTEDTU_type[5];
    Int_t LiTEDTU_BaseLine_G10[5];
    Int_t LiTEDTU_BaseLine_G1[5];
    Int_t LiTEDTU_eLink_active[5];

// I2C settings :
    Int_t I2C_shift_dev_number; // I2C_address = (chip# << I2C_shift_dev_number)+(0/1/2/3)
    UInt_t I2C_RW(uhal::HwInterface hw, UInt_t device_number, UInt_t register_number, UInt_t register_data, UInt_t I2C_long, Int_t RW, Int_t debug )

// Resets :
}

UInt_t board::I2C_RW ( uhal::HwInterface hw, unsigned int device_number, unsigned int register_number, unsigned int register_data, int I2C_long, int RW, int debug )
{
  unsigned int I2C_busy=1;
  unsigned int I2C_error=0;
  int retry=0;
  int type=device_number/1000; // 0=LVRB, 1=CATIA, 2=DTU
  device_number-=type*1000;
  int read=(RW>>1)&1;
  int write=RW&1;
  ValWord<uint32_t> busy,reg;
  ValWord<uint32_t> n_ack, ack_pat[6], ack_lsb, ack_msb;

  if(debug>0) printf("\nI2C access W %d, R %d : type %d, device 0x%x, register %d, data %d, long %d\n",write, read,type, device_number, register_number, register_data, I2C_long);
// I2C address : 0xyz000, with xyz=001 (DTU_num=1) or 010 (DTU_num=2), shifted left by 3 each with 3 I2C sub-address (0,1,2 as lsb)
  unsigned int  command;
  if(write==1)
  {
    command=I2C_RWb*0 | I2C_LONG*I2C_long | (device_number&0x7F)*I2C_DEVICE | (register_number&0x7F)*I2C_REG | (register_data&0xFFFF);
    if(debug>0)
      printf("Write I2C : type %d, device 0x%x, register %d, data %d, long %d command : 0x%x\n",type, device_number, register_number, register_data, I2C_long,command);
    if(type==0)
      hw.getNode("LVRB_CTRL").write(command);
    else if(type==1)
      hw.getNode("CATIA_CTRL").write(command);
    else if(type==2)
      hw.getNode("DTU_CTRL").write(command);
    hw.dispatch();
    if(debug>1)
      printf("Write I2C : done\n");

// Wait for transaction end :
    I2C_busy=1;
    retry=0;
    while(I2C_busy==1 && retry<NRETRY_MAX)
    {
      busy = hw.getNode("VFE_CTRL").read();
      hw.dispatch();
      I2C_busy =(busy.value()>>(type+2))&1;
      I2C_error=(busy.value()>>(type+5))&1;
      if(debug>1)printf("Busy 0x%x\n",busy.value());
      if(type==0)usleep(1000);
      retry++;
    }
    if(retry==NRETRY_MAX)
    {
// Look at which state the transaction stucked :
      busy = hw.getNode("DEBUG1").read();
      hw.dispatch();
      if(debug>0)printf("I2C transaction stuck at step 0x%4.4x\n",(busy.value()>>16)&0x7FFF);
    }

    if(debug>0)
    {
      printf("I2C : Write device 0x%x, type %d, register %d, data %d : Command = 0x%x : \n",device_number, type, register_number, register_data,command);
      if(type>=0)
      {
        printf("ACK spy = 0x%.8x %.8x : 0b",ack_msb.value(), ack_lsb.value());
        unsigned int loc_ack;
        loc_ack=ack_msb.value();
        for(int i=0; i<32; i++)
        {
          if((loc_ack&0x80000000)>0) 
            printf("1");
          else
            printf("0");
          if((i%4)==3) printf(" ");
          loc_ack<<=1;
        }
        loc_ack=ack_lsb.value();
        for(int i=0; i<32; i++)
        {
          if((loc_ack&0x80000000)>0) 
            printf("1");
          else
            printf("0");
          if((i%4)==3) printf(" ");
          loc_ack<<=1;
        }
        printf("\n");
      }
    }
  }
  if(read==1)
  {
    if(debug>0)
      printf("Read I2C : type %d, device 0x%x, register %d, long %d\n",type, device_number, register_number, I2C_long);
    command=I2C_RWb*1 | I2C_LONG*I2C_long | (device_number&0x7F)*I2C_DEVICE | (register_number&0x7F)*I2C_REG;
    if(type==0)
      hw.getNode("LVRB_CTRL").write(command);
    else if(type==1)
      hw.getNode("CATIA_CTRL").write(command);
    else if(type==2)
      hw.getNode("DTU_CTRL").write(command);
    hw.dispatch();

// Wait for transaction end :
    I2C_busy=1;
    retry=0;
    while(I2C_busy==1 && retry<NRETRY_MAX)
    {
      busy = hw.getNode("VFE_CTRL").read();
      hw.dispatch();
      I2C_busy =(busy.value()>>(type+2))&1;
      I2C_error=(busy.value()>>(type+5))&1;
      if(debug>1)printf("Busy 0x%x\n",busy.value());
      if(type==0)usleep(1000);
      retry++;
    }
    if(retry==NRETRY_MAX)
    {
// Look at which state the transaction stucked :
      busy = hw.getNode("DEBUG1").read();
      hw.dispatch();
      if(debug>0)printf("I2C transaction stuck at step 0x%4.4x\n",(busy.value()>>16)&0x7FFF);
    }
    if(type==0)
      reg=hw.getNode("LVRB_CTRL").read();
    else if(type==1)
      reg=hw.getNode("CATIA_CTRL").read();
    else if(type==2)
      reg=hw.getNode("DTU_CTRL").read();
    hw.dispatch();

    if(debug>0)
    {
      printf("I2C : Read device 0x%x, type %d, register %d. Command = 0x%x : ",device_number, type, register_number,command);
      printf("Value read : 0x%.8x, busy 0x%x\n",reg.value(),I2C_busy);
      if(type>=0)
      {
        printf("I2C : ACK spy = 0x%.8x %.8x : 0b",ack_msb.value(), ack_lsb.value());
        unsigned int loc_ack;
        loc_ack=ack_msb.value();
        for(int i=0; i<32; i++)
        {
          if((loc_ack&0x80000000)>0) 
            printf("1");
          else
            printf("0");
          if((i%4)==3) printf(" ");
          loc_ack<<=1;
        }
        loc_ack=ack_lsb.value();
        for(int i=0; i<32; i++)
        {
          if((loc_ack&0x80000000)>0) 
            printf("1");
          else
            printf("0");
          if((i%4)==3) printf(" ");
          loc_ack<<=1;
        }
        printf("\n");
      }
    }
  }

  if(type==0)
    reg=hw.getNode("LVRB_CTRL").read();
  else if(type==1)
    reg=hw.getNode("CATIA_CTRL").read();
  else if(type==2)
    reg=hw.getNode("DTU_CTRL").read();
  n_ack=hw.getNode("I2C_N_ACK").read();
  hw.dispatch();
  //printf("N_ack : 0x%8.8x\n",n_ack.value());
  int i_ack=n_ack.value()>>(type*8);
  i_ack&=0xff;
  return (reg.value()&0x7FFFFF)|(I2C_error<<31)|(i_ack<<23); 
}
