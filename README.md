# SAFE project (Stand-Alone FE):

## hardware : Kicad files to produce the SAFE PCB.<br>
- schematics based on FEAD board schematics.<br>
- Read 25 e-links <br>
- Drive 5 ReSync links and 5 I2C bus (one for each VFE)<br>
- Drive Power supplies on LVRB (PG and EN lines) <br>
- Read CATIA and APD temperature lines<br>
- Read PLL-lock lines and drive test-mode<br>
- Don't look at SEU and CalBusy signals

- pcb : 8 layers

- fab files

## firmware : Vivado files to generate the firmware
- Based on firmware developped for FEAD board.
- Use IPbus firmware from cern. Make sure you have the latest version before compiling :
  - cd firmware
  - git clone https://github.com/ipbus/ipbus-firmware.git
  - cd ipbus-firmware
  - git checkout v1.9
- If you have already made a git pull of the project, you can :
  - git submodule update --init
  - and verfiy the version : git submodule status
- Already implemented :
  - 25 eLinks
  - 5 ReSync lines
  - 5 I2C bus
  - XADC on various analog signals
  - Capture incoming ADC samples on 25 channels (max 4095 samples per event)
  - Read back with IPbus
- It seems that there are no timing failures neither DRC errors

## software : Code samples to access the SAFE board 
- You need to install :
  - the cactus project libraries
  - glade
  - GTK3
- A new GUI is under development.

Nothink is perfect, so any feedback is welcome.<br>

M.D.

Initial creation : 2022/09/28 <br>
Last Modification : 2023/01/23
