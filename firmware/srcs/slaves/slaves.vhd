-- The ipbus slaves live in this entity - modify according to requirements
--
-- Ports can be added to give ipbus slaves access to the chip top level.
--
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.ipbus.ALL;
use work.SAFE_IO.all;

entity slaves is
  port(
    pwup_rst      : in  std_logic;
    DAQ_busy      : out std_logic;
    ipb_clk       : in  std_logic;
    ipb_in        : in  ipb_wbus;
    ipb_out       : out ipb_rbus;
    SAFE_Monitor  : in  SAFE_Monitor_t;
    SAFE_Control  : out SAFE_Control_t;
    VFE_Monitor   : inout  VFE_Monitor_t;
    VFE_Control   : out VFE_Control_t;
    clk_40       : in  std_logic;
    clk_160       : in  std_logic;
    clk_shift_reg : in  std_logic;
    clk_memory    : in  std_logic;
    ch1_in        : in  std_logic_vector(31 downto 0);
    ch2_in        : in  std_logic_vector(31 downto 0);
    ch3_in        : in  std_logic_vector(31 downto 0);
    ch4_in        : in  std_logic_vector(31 downto 0);
    ch5_in        : in  std_logic_vector(31 downto 0);
    ch1_out       : out std_logic_vector(31 downto 0);
    ch2_out       : out std_logic_vector(31 downto 0);
    ch3_out       : out std_logic_vector(31 downto 0);
    ch4_out       : out std_logic_vector(31 downto 0);
    ch5_out       : out std_logic_vector(31 downto 0);    
    trig_local    : out std_logic;
    trig_signal   : in  std_logic;
    CRC_reset     : in  std_logic;
    CRC_error     : out UInt32_t(5 downto 1);
    APD_temp_in   : in  std_logic;
    APD_temp_ref  : in  std_logic;
    Vdummy_in     : in  std_logic;
    Vdummy_ref    : in  std_logic;
    CATIA_temp_in : in  std_logic;
    CATIA_temp_ref: in  std_logic;
    V2P5_in       : in  std_logic;
    V2P5_ref      : in  std_logic;
    V1P2_in       : in  std_logic;
    V1P2_ref      : in  std_logic;
    Switch        : in  std_logic_vector(3 downto 0)
    );
end slaves;

architecture rtl of slaves is

  component SAFE_ctrlreg is
    port (
--      sig_monitor : out std_logic_vector(3 downto 0);
      clk           : in  std_logic;
      reset         : in  std_logic;
      ipbus_in      : in  ipb_wbus;
      ipbus_out     : out ipb_rbus;
      SAFE_monitor  : in  SAFE_Monitor_t;
      SAFE_control  : out SAFE_Control_t;
      VFE_monitor   : inout  VFE_Monitor_t;
      VFE_control   : out VFE_Control_t;
      APD_temp_in   : in std_logic;
      APD_temp_ref  : in std_logic;
      Vdummy_in     : in std_logic;
      Vdummy_ref    : in std_logic;
      CATIA_temp_in : in std_logic;
      CATIA_temp_ref: in std_logic;
      V2P5_in       : in std_logic;
      V2P5_ref      : in std_logic;
      V1P2_in       : in std_logic;
      V1P2_ref      : in std_logic;
      Switch        : in std_logic_vector(3 downto 0)
    );
  end component SAFE_ctrlreg;
  
  component VFE_Capture is
    port (
      DAQ_busy          : out STD_LOGIC;
      clk               : in  STD_LOGIC;
      reset             : in  STD_LOGIC;
      ipbus_in          : in  ipb_wbus;
      ipbus_out         : out ipb_rbus;
      clk_40            : in  std_logic;
      clk_160           : in  std_logic;
      clk_shift_reg     : in  std_logic;
      clk_memory        : in  std_logic;
      ch1_in            : in  std_logic_vector(31 downto 0);
      ch2_in            : in  std_logic_vector(31 downto 0);
      ch3_in            : in  std_logic_vector(31 downto 0);
      ch4_in            : in  std_logic_vector(31 downto 0);
      ch5_in            : in  std_logic_vector(31 downto 0);
      trig_local        : out std_logic;
      trig_self         : in  std_logic;
      trig_self_mode    : in  std_logic;
      trig_self_mask    : in  std_logic_vector(4 downto 0);
      trig_self_thres   : in  std_logic_vector(11 downto 0);
      FIFO_mode         : in  std_logic;
      ADC_test_mode     : in  std_logic;
      ADC_MEM_mode      : in  std_logic;
      ADC_invert_data   : in  std_logic;
      trig_signal       : in  std_logic;
      CRC_reset         : in  std_logic;
      CRC_error         : out UInt32_t(5 downto 1)
    );
  end component VFE_Capture;
  
  component FE_data is
    port (
      clk_ipbus    : in  STD_LOGIC;
      reset        : in  STD_LOGIC;
      ipbus_in     : in  ipb_wbus;
      ipbus_out    : out ipb_rbus;
      clk          : in  std_logic;
      ch1_captured : in  std_logic_vector(31 downto 0);
      ch2_captured : in  std_logic_vector(31 downto 0);
      ch3_captured : in  std_logic_vector(31 downto 0);
      ch4_captured : in  std_logic_vector(31 downto 0);
      ch5_captured : in  std_logic_vector(31 downto 0);
      ch1_out      : out std_logic_vector(31 downto 0);
      ch2_out      : out std_logic_vector(31 downto 0);
      ch3_out      : out std_logic_vector(31 downto 0);
      ch4_out      : out std_logic_vector(31 downto 0);
      ch5_out      : out std_logic_vector(31 downto 0));
  end component FE_data;

  
  constant NSLV: positive := 3;
  signal ipbw: ipb_wbus_array(NSLV-1 downto 0);
  signal ipbr, ipbr_d: ipb_rbus_array(NSLV-1 downto 0);
  signal ctrl_reg: std_logic_vector(31 downto 0);
  signal inj_ctrl, inj_stat: std_logic_vector(63 downto 0);

begin

  fabric: entity work.ipbus_fabric
    generic map(NSLV => NSLV)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );

-- Slave 0: id / rst reg
  slave0: entity work.SAFE_ctrlreg
    port map (
      clk            => ipb_clk,
      reset          => pwup_rst,
      ipbus_in       => ipbw(0),
      ipbus_out      => ipbr(0),
      SAFE_monitor   => SAFE_Monitor,
      SAFE_control   => SAFE_Control,
      VFE_monitor    => VFE_Monitor,
      VFE_control    => VFE_Control,
      APD_temp_in    => APD_temp_in,
      APD_temp_ref   => APD_temp_ref,
      Vdummy_in      => Vdummy_in,
      Vdummy_ref     => Vdummy_ref,
      CATIA_temp_in  => CATIA_temp_in,
      CATIA_temp_ref => CATIA_temp_ref,
      V2P5_in        => V2P5_in,
      V2P5_ref       => V2P5_ref,
      V1P2_in        => V1P2_in,
      V1P2_ref       => V1P2_ref,
      Switch         => Switch
      );
  
-- Slave 1: VFE capture
  slave1: entity work.VFE_capture
    port map(
      DAQ_busy          => DAQ_busy,
      clk_ipbus         => ipb_clk,
      reset             => pwup_rst,
      ipbus_in          => ipbw(1),
      ipbus_out         => ipbr(1),
      clk_40            => clk_40,
      clk_160           => clk_160,
      clk_shift_reg     => clk_shift_reg,
      clk_memory        => clk_memory,
      ch1_in            => ch1_in,
      ch2_in            => ch2_in,
      ch3_in            => ch3_in,
      ch4_in            => ch4_in,
      ch5_in            => ch5_in,
      trig_local        => trig_local,
      trig_self         => SAFE_Monitor.trig_self,
      trig_self_mode    => SAFE_Monitor.trig_self_mode,
      trig_self_mask    => SAFE_Monitor.trig_self_mask,
      trig_self_thres   => SAFE_Monitor.trig_self_thres,
      FIFO_mode         => SAFE_Monitor.FIFO_mode,
      ADC_test_mode     => VFE_Monitor.ADC_test_mode,
      ADC_MEM_mode      => VFE_Monitor.ADC_MEM_mode,
      ADC_invert_data   => VFE_Monitor.ADC_invert_data,
      trig_signal       => trig_signal,
      CRC_reset         => CRC_reset,
      CRC_error         => CRC_error
      );

end rtl;
