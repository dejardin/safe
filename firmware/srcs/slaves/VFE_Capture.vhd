library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.SAFE_IO.all;

entity VFE_Capture is
  generic(
   MAX_RAM_ADDRESS        : unsigned(14 downto 0) := "110"&x"7ff"
  );
  port(
    DAQ_busy          : out std_logic;
    clk_ipbus         : in  std_logic;
    reset             : in  std_logic;
    ipbus_in          : in  ipb_wbus;
    ipbus_out         : out ipb_rbus;
    clk_40            : in std_logic;
    clk_160           : in std_logic;
    clk_shift_reg     : in std_logic;
    clk_memory        : in std_logic;
    ch1_in            : in std_logic_vector(31 downto 0);
    ch2_in            : in std_logic_vector(31 downto 0);
    ch3_in            : in std_logic_vector(31 downto 0);
    ch4_in            : in std_logic_vector(31 downto 0);
    ch5_in            : in std_logic_vector(31 downto 0);
    trig_local        : out std_logic;
    trig_self         : in std_logic;
    trig_self_mode    : in std_logic;
    trig_self_mask    : in std_logic_vector(4 downto 0);
    trig_self_thres   : in std_logic_vector(11 downto 0);
    FIFO_mode         : in std_logic;
    ADC_test_mode     : in std_logic;
    ADC_MEM_mode      : in std_logic;
    ADC_invert_data   : in std_logic;
    trig_signal       : in std_logic;
    CRC_reset         : in std_logic;
    CRC_error         : out UInt32_t(5 downto 1)
  );

end VFE_Capture;

architecture rtl of VFE_Capture is

--  component ila_0 is
--    port (
--      clk    : in std_logic;
--      probe0 : in std_logic_vector (0 to 0);
--      probe1 : in std_logic_vector (0 to 0);
--      probe2 : in std_logic_vector (0 to 0);
--      probe3 : in std_logic_vector (7 downto 0)
--    );
--  end component ila_0;


--  component pacd is
--    port (
--      iPulseA : in  std_logic;
--      iClkA   : in  std_logic;
--      iRSTAn  : in  std_logic;
--      iClkB   : in  std_logic;
--      iRSTBn  : in  std_logic;
--      oPulseB : out std_logic
--    );
--  end component pacd;

  component blk_mem_gen_0 is -- 16k samples of 28 bits
    port (
      clka  : in  std_logic;
      ena   : in  std_logic;
      wea   : in  std_logic_vector (0 to 0);
      addra : in  std_logic_vector (14 downto 0);
      dina  : in  std_logic_vector (159 downto 0);
      clkb  : in  std_logic;
      enb   : in  std_logic;
      addrb : in  std_logic_vector (14 downto 0);
      doutb : out std_logic_vector (159 downto 0)
    );
  end component blk_mem_gen_0;

  component c_shift_ram_0 IS
  port (
    d   : IN STD_LOGIC_VECTOR(159 downto 0);
    clk : IN STD_LOGIC;
    q   : OUT STD_LOGIC_VECTOR(159 downto 0)
  );
  end component c_shift_ram_0;

  signal sel : integer;
  signal ack : std_logic;

  signal ipbus_sample_address     : std_logic_vector(15 downto 0) := x"0000";

  signal channel_vfe_data_in      : std_logic_vector(159 downto 0) := (others =>'0');
  signal channel_vfe_data_prev    : std_logic_vector(159 downto 0) := (others =>'0');
  signal channel_vfe_data_delayed : std_logic_vector(159 downto 0) := (others =>'0');
  signal channel_ram_data_in      : std_logic_vector(159 downto 0) := (others =>'0');
  signal channel_ram_data_out     : std_logic_vector(159 downto 0) := (others =>'0');

  signal write_address         : unsigned(14 downto 0) := MAX_RAM_ADDRESS;  -- write address
  signal read_address          : unsigned(14 downto 0) := MAX_RAM_ADDRESS;  -- read address
  signal capture_write         : std_logic             := '0';
  signal frame_length          : unsigned(14 downto 0) := "000"&x"001";

  signal capture_running       : std_logic := '0';  -- capturing data
  signal capture_start         : std_logic := '0';  -- set capturing data to '1'
  signal capture_pause         : std_logic := '0';  -- hold capturing data
  signal capture_stop          : std_logic := '0';  -- set capturing data to '0'
  
--Strobes from the IPBUS clock domain to capture domain
  signal capture_start_strobe  : std_logic := '0';
  signal capture_start_strobe1 : std_logic := '0';
  signal capture_start_strobe2 : std_logic := '0';
  signal capture_stop_strobe   : std_logic := '0';
  signal capture_stop_strobe1  : std_logic := '0';
  signal capture_stop_strobe2  : std_logic := '0';
  
-- Bit 13 is to tell that we have data from the compressed stream.
-- We could have from 1 to 5 samples in the same LHC clock.
-- This is required to compute a trigger from decoded data.
  type crc_data_t is array (integer range <>) of std_logic_vector(11 downto 0);
  signal crc_local             : crc_data_t(5 downto 1)       := (others => (others => '0'));

  signal loc_crc_error         : UInt32_t(5 downto 1)         := (others => (others => '0'));
  signal crc_valid             : std_logic_vector(5 downto 1) := (others => '0');

  type deco_data_t is array (integer range <>) of std_logic_vector(13 downto 0);
  signal ch1_deco              : deco_data_t(5 downto 0)       := (others => (others => '0'));
  signal ch2_deco              : deco_data_t(5 downto 0)       := (others => (others => '0'));
  signal ch3_deco              : deco_data_t(5 downto 0)       := (others => (others => '0'));
  signal ch4_deco              : deco_data_t(5 downto 0)       := (others => (others => '0'));
  signal ch5_deco              : deco_data_t(5 downto 0)       := (others => (others => '0'));
  
  type val_data_t is array (integer range <>) of unsigned(14 downto 0);
  signal ch1_val               : val_data_t(5 downto 0)        := (others => (others => '0'));
  signal ch2_val               : val_data_t(5 downto 0)        := (others => (others => '0'));
  signal ch3_val               : val_data_t(5 downto 0)        := (others => (others => '0'));
  signal ch4_val               : val_data_t(5 downto 0)        := (others => (others => '0'));
  signal ch5_val               : val_data_t(5 downto 0)        := (others => (others => '0'));
  signal trig_thres            : unsigned(15 downto 0)         := (others => '0');
  signal ch1_delta_val         : signed(15 downto 0)           := (others => '0');
  signal ch2_delta_val         : signed(15 downto 0)           := (others => '0');
  signal ch3_delta_val         : signed(15 downto 0)           := (others => '0');
  signal ch4_delta_val         : signed(15 downto 0)           := (others => '0');
  signal ch5_delta_val         : signed(15 downto 0)           := (others => '0');
  signal idle_pattern          : std_logic_vector(5 downto 1)  := (others => '0');
  signal signal_sample         : std_logic_vector(5 downto 1)  := (others => '0');
  signal sync_error            : std_logic_vector(5 downto 1)  := (others => '0');
  signal signal_error          : std_logic_vector(5 downto 1)  := (others => '0');
  signal DTU_frame_error       : std_logic_vector(5 downto 1)  := (others => '0');
  signal DTU_frame_delimiter   : std_logic_vector(5 downto 1)  := (others => '0');

  signal idle_counter          : UInt8_t(5 downto 1)           := (others => (others => '0'));
  signal signal_counter        : UInt8_t(5 downto 1)           := (others => (others => '0'));
  signal DTU_frame_length      : UInt8_t(5 downto 1)           := (others => (others => '0'));
  signal loc_DTU_frame_length  : UInt8_t(5 downto 1)           := (others => (others => '0'));

  --* State type of the writing process
  type   write_type is (write_start, write_idle, write_running);
  signal write_state : write_type := write_start;

  --* State type of the ipbus process
  type   ipbus_rw_type is (ipbus_rw_start, ipbus_rw_idle, ipbus_write_start, ipbus_read_start, ipbus_delay, ipbus_read, ipbus_ack, ipbus_finished);
  signal ipbus_rw_state : ipbus_rw_type := ipbus_rw_start;
  type cycle_state is (first, second, third, fourth, fifth, sixth);
  signal read_cycle : cycle_state := first;
  type busy_type is (not_busy, stay_busy, busy);
  signal busy_state : busy_type := not_busy;
  
  signal channel_ram_data_read : std_logic    := '0';
  signal ram_full              : std_logic    := '0';
  signal trig_sig_del1         : std_logic    := '0';
  signal trig_sig_del2         : std_logic    := '0';
  signal trig_short            : std_logic    := '0';
  signal nsample               : unsigned(14 downto 0) := "000"&x"000";
  signal free_ram              : unsigned(14 downto 0) := MAX_RAM_ADDRESS;  -- Available ram
  signal ram_read              : std_logic := '0';
  signal ram_read_del1         : std_logic := '0';
  signal ram_read_del2         : std_logic := '0';
  signal ram_read_short        : std_logic := '0';
  signal clka_ram              : std_logic := '0';
  signal clkb_ram              : std_logic := '0';
  signal clk_sequence          : std_logic := '0';

  signal time_stamp            : unsigned(63 downto 0);
  signal trig_width            : unsigned(3 downto 0);
  
  function CRC_calc(data : std_logic_vector; crc : std_logic_vector) return std_logic_vector is
    variable loc_crc : std_logic_vector(11 downto 0) := (others => '0');
  begin
   loc_crc(0) := data(30) xor data(29) xor data(26) xor data(25) xor data(24) xor data(23) xor data(22) xor data(17) xor data(16) xor data(15) xor data(14) xor data(13) xor data(12) xor data(11) xor data(8) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(2) xor data(1) xor data(0) xor crc(2) xor crc(3) xor crc(4) xor crc(5) xor crc(6) xor crc(9) xor crc(10) ;
   loc_crc(1) := data(31) xor data(29) xor data(27) xor data(22) xor data(18) xor data(11) xor data(9) xor data(0) xor crc(2) xor crc(7) xor crc(9) xor crc(11) ;
   loc_crc(2) := data(29) xor data(28) xor data(26) xor data(25) xor data(24) xor data(22) xor data(19) xor data(17) xor data(16) xor data(15) xor data(14) xor data(13) xor data(11) xor data(10) xor data(8) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(2) xor data(0) xor crc(2) xor crc(4) xor crc(5) xor crc(6) xor crc(8) xor crc(9) ;
   loc_crc(3) := data(27) xor data(24) xor data(22) xor data(20) xor data(18) xor data(13) xor data(9) xor data(2) xor data(0) xor crc(0) xor crc(2) xor crc(4) xor crc(7) ;
   loc_crc(4) := data(28) xor data(25) xor data(23) xor data(21) xor data(19) xor data(14) xor data(10) xor data(3) xor data(1) xor crc(1) xor crc(3) xor crc(5) xor crc(8) ;
   loc_crc(5) := data(29) xor data(26) xor data(24) xor data(22) xor data(20) xor data(15) xor data(11) xor data(4) xor data(2) xor crc(0) xor crc(2) xor crc(4) xor crc(6) xor crc(9) ;
   loc_crc(6) := data(30) xor data(27) xor data(25) xor data(23) xor data(21) xor data(16) xor data(12) xor data(5) xor data(3) xor crc(1) xor crc(3) xor crc(5) xor crc(7) xor crc(10) ;
   loc_crc(7) := data(31) xor data(28) xor data(26) xor data(24) xor data(22) xor data(17) xor data(13) xor data(6) xor data(4) xor crc(2) xor crc(4) xor crc(6) xor crc(8) xor crc(11) ;
   loc_crc(8) := data(29) xor data(27) xor data(25) xor data(23) xor data(18) xor data(14) xor data(7) xor data(5) xor crc(3) xor crc(5) xor crc(7) xor crc(9) ;
   loc_crc(9) := data(30) xor data(28) xor data(26) xor data(24) xor data(19) xor data(15) xor data(8) xor data(6) xor crc(4) xor crc(6) xor crc(8) xor crc(10) ;
   loc_crc(10) := data(31) xor data(29) xor data(27) xor data(25) xor data(20) xor data(16) xor data(9) xor data(7) xor crc(0) xor crc(5) xor crc(7) xor crc(9) xor crc(11) ;
   loc_crc(11) := data(29) xor data(28) xor data(25) xor data(24) xor data(23) xor data(22) xor data(21) xor data(16) xor data(15) xor data(14) xor data(13) xor data(12) xor data(11) xor data(10) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(2) xor data(1) xor data(0) xor crc(1) xor crc(2) xor crc(3) xor crc(4) xor crc(5) xor crc(8) xor crc(9);
   return loc_crc;
  end function;

begin
-- We receive 32 bit words synchronous with the LHC clock
  clk_sequence <= clk_40;
  CRC_error    <= loc_CRC_error;
  
  Inst_VFE_data : if not FOR_SEU generate

  -------------------------------------------------------------------------------
  -- Capture of incomming channel data into rams for readout
  -------------------------------------------------------------------------------
  ch1_val(1)      <= unsigned(ch1_deco(1)(12 downto 0));
  ch1_val(2)      <= unsigned(ch1_deco(2)(12 downto 0));
  ch1_val(3)      <= unsigned(ch1_deco(3)(12 downto 0));
  ch1_val(4)      <= unsigned(ch1_deco(4)(12 downto 0));
  ch1_val(5)      <= unsigned(ch1_deco(5)(12 downto 0));
  ch2_val(1)      <= unsigned(ch2_deco(1)(12 downto 0));
  ch2_val(2)      <= unsigned(ch2_deco(2)(12 downto 0));
  ch2_val(3)      <= unsigned(ch2_deco(3)(12 downto 0));
  ch2_val(4)      <= unsigned(ch2_deco(4)(12 downto 0));
  ch2_val(5)      <= unsigned(ch2_deco(5)(12 downto 0));
  ch3_val(1)      <= unsigned(ch3_deco(1)(12 downto 0));
  ch3_val(2)      <= unsigned(ch3_deco(2)(12 downto 0));
  ch3_val(3)      <= unsigned(ch3_deco(3)(12 downto 0));
  ch3_val(4)      <= unsigned(ch3_deco(4)(12 downto 0));
  ch3_val(5)      <= unsigned(ch3_deco(5)(12 downto 0));
  ch4_val(1)      <= unsigned(ch4_deco(1)(12 downto 0));
  ch4_val(2)      <= unsigned(ch4_deco(2)(12 downto 0));
  ch4_val(3)      <= unsigned(ch4_deco(3)(12 downto 0));
  ch4_val(4)      <= unsigned(ch4_deco(4)(12 downto 0));
  ch4_val(5)      <= unsigned(ch4_deco(5)(12 downto 0));
  ch5_val(1)      <= unsigned(ch5_deco(1)(12 downto 0));
  ch5_val(2)      <= unsigned(ch5_deco(2)(12 downto 0));
  ch5_val(3)      <= unsigned(ch5_deco(3)(12 downto 0));
  ch5_val(4)      <= unsigned(ch5_deco(4)(12 downto 0));
  ch5_val(5)      <= unsigned(ch5_deco(5)(12 downto 0));
  trig_thres      <= "0000"&unsigned(trig_self_thres);

  compute_crc : process (clk_sequence, CRC_reset) is
  begin
    if CRC_reset ='1' then
      CRC_local          <= (others => (others => '0'));
      loc_CRC_error      <= (others => (others => '0'));
      CRC_valid          <= (others => '0');
    elsif rising_edge(clk_sequence) then
      if ch1_in(31 downto 28) = "1101" then
        if crc_valid(1) = '0' then
          crc_valid(1)   <= '1';
        else
          if CRC_local(1) /= ch1_in(19 downto 8) then
            loc_CRC_error(1) <= loc_CRC_error(1) + 1;
          end if;
        end if;
        CRC_local(1)     <= (others => '0');
      elsif ch1_in(31 downto 30) /= "11" then
        crc_local(1) <= CRC_calc(ch1_in, crc_local(1));
      end if;
      if ch2_in(31 downto 28) = "1101" then
        if crc_valid(2) = '0' then
          crc_valid(2)   <= '1';
        else
          if CRC_local(2) /= ch2_in(19 downto 8) then
            loc_CRC_error(2) <= loc_CRC_error(2) + 1;
          end if;
        end if;
        crc_local(2) <= (others => '0');
      elsif ch2_in(31 downto 30) /= "11" then
        crc_local(2) <= CRC_calc(ch2_in, crc_local(2));
      end if;
      if ch3_in(31 downto 28) = "1101" then
        if crc_valid(3) = '0' then
          crc_valid(3)   <= '1';
        else
          if CRC_local(3) /= ch3_in(19 downto 8) then
            loc_CRC_error(3) <= loc_CRC_error(3) + 1;
          end if;
        end if;
        crc_local(3) <= (others => '0');
      elsif ch3_in(31 downto 30) /= "11" then
        crc_local(3) <= CRC_calc(ch3_in, crc_local(3));
      end if;
      if ch4_in(31 downto 28) = "1101" then
        if crc_valid(4) = '0' then
          crc_valid(4)   <= '1';
        else
          if CRC_local(4) /= ch4_in(19 downto 8) then
            loc_CRC_error(4) <= loc_CRC_error(4) + 1;
          end if;
        end if;
        crc_local(4) <= (others => '0');
      elsif ch4_in(31 downto 30) /= "11" then
        crc_local(4) <= CRC_calc(ch4_in, crc_local(4));
      end if;
      if ch5_in(31 downto 28) = "1101" then
        if crc_valid(5) = '0' then
          crc_valid(5)   <= '1';
        else
          if CRC_local(5) /= ch5_in(19 downto 8) then
            loc_CRC_error(5) <= loc_CRC_error(5) + 1;
          end if;
        end if;
        crc_local(5) <= (others => '0');
      elsif ch5_in(31 downto 30) /= "11" then
        crc_local(5) <= CRC_calc(ch5_in, crc_local(5));
      end if;
    end if;
  end process compute_crc;


  decode_local_data : process (clk_sequence, reset) is
  begin
    if reset ='1' then
      ch1_deco(1)         <= (others => '0');
      ch1_deco(2)         <= (others => '0');
      ch1_deco(3)         <= (others => '0');
      ch1_deco(4)         <= (others => '0');
      ch1_deco(5)         <= (others => '0');
      ch2_deco(1)         <= (others => '0');
      ch2_deco(2)         <= (others => '0');
      ch2_deco(3)         <= (others => '0');
      ch2_deco(4)         <= (others => '0');
      ch2_deco(5)         <= (others => '0');
      ch3_deco(1)         <= (others => '0');
      ch3_deco(2)         <= (others => '0');
      ch3_deco(3)         <= (others => '0');
      ch3_deco(4)         <= (others => '0');
      ch3_deco(5)         <= (others => '0');
      ch4_deco(1)         <= (others => '0');
      ch4_deco(2)         <= (others => '0');
      ch4_deco(3)         <= (others => '0');
      ch4_deco(4)         <= (others => '0');
      ch4_deco(5)         <= (others => '0');
      ch5_deco(1)         <= (others => '0');
      ch5_deco(2)         <= (others => '0');
      ch5_deco(3)         <= (others => '0');
      ch5_deco(4)         <= (others => '0');
      ch5_deco(5)         <= (others => '0');
      DTU_frame_delimiter <= (others => '0');
      idle_pattern        <= (others => '0');
      signal_sample       <= (others => '0');
    elsif rising_edge(clk_sequence) then
      idle_pattern        <= (others => '0');
      signal_sample       <= (others => '0');
      if ADC_test_mode = '0' then
-- Channel 1 decoding :
        ch1_deco(1)   <= (others => '0');
        ch1_deco(2)   <= (others => '0');
        ch1_deco(3)   <= (others => '0');
        ch1_deco(4)   <= (others => '0');
        ch1_deco(5)   <= (others => '0');
        case ch1_in(31 downto 30) is
        when "01" => -- Baseline mode 5 samples per word
          ch1_deco(1)   <= "10000000"&ch1_in(5 downto 0);
          ch1_deco(2)   <= "10000000"&ch1_in(11 downto 6);
          ch1_deco(3)   <= "10000000"&ch1_in(17 downto 12);
          ch1_deco(4)   <= "10000000"&ch1_in(23 downto 18);
          ch1_deco(5)   <= "10000000"&ch1_in(29 downto 24);
          loc_DTU_frame_length(1) <= loc_DTU_frame_length(1)+1;
        when "10" => -- Baseline mode, at most 4 samples per word
          if ch1_in(26 downto 24) = "001" then
            ch1_deco(1) <= "10000000"&ch1_in(5 downto 0);
          end if;
          if ch1_in(26 downto 24) = "010" then
            ch1_deco(1) <= "10000000"&ch1_in(5 downto 0);
            ch1_deco(2) <= "10000000"&ch1_in(11 downto 6);
          end if;
          if ch1_in(26 downto 24) = "011" then
            ch1_deco(1) <= "10000000"&ch1_in(5 downto 0);
            ch1_deco(2) <= "10000000"&ch1_in(11 downto 6);
            ch1_deco(3) <= "10000000"&ch1_in(17 downto 12);
          end if;
          if ch1_in(26 downto 24) = "011" then
            ch1_deco(1) <= "10000000"&ch1_in(5 downto 0);
            ch1_deco(2) <= "10000000"&ch1_in(11 downto 6);
            ch1_deco(3) <= "10000000"&ch1_in(17 downto 12);
            ch1_deco(4) <= "10000000"&ch1_in(23 downto 18);
          end if;
          loc_DTU_frame_length(1) <= loc_DTU_frame_length(1)+1;
        when "00" => -- signal mode : 1 or 2 damples per word
          if ADC_MEM_mode = '0' then
            if ch1_in(29 downto 26) = "1010" then
              ch1_deco(1) <= "1"&ch1_in(12 downto 0);
              ch1_deco(2) <= "1"&ch1_in(25 downto 13);
            elsif ch1_in(29 downto 26) = "1011" then
              ch1_deco(1) <= "1"&ch1_in(12 downto 0);
            end if;
            signal_sample(1) <= '1';
            loc_DTU_frame_length(1) <= loc_DTU_frame_length(1)+1;
          else -- Official 80 MHz mode
            if ch1_in(29 downto 28) = "01" then
              ch1_deco(1) <= "1"&ch1_in(12 downto  0); -- ADCH(i)
              ch1_deco(2) <= "1"&ch1_in(25 downto 13); -- ADCH(i+2)
            elsif ch1_in(29 downto 28) = "00" then
              ch1_deco(1) <= (others => '0'); -- BC0
              ch1_deco(2) <= "1"&ch1_in(25 downto 13); -- ADCH(i+2)
            elsif ch1_in(29 downto 28) = "11" then
              ch1_deco(1) <= "1"&ch1_in(12 downto  0); -- ADCH(i)
              ch1_deco(2) <= (others => '0'); -- BC0
            end if;
          end if;
        when "11" => -- synchro frame or frame delimiter
          if ch1_in=x"EAAAAAAA" then
            idle_pattern(1) <= '1';
          end if;
          if ch1_in(31 downto 28)="1101" then
            DTU_frame_length(1)     <= loc_DTU_frame_length(1);
            loc_DTU_frame_length(1) <= (others => '0');
          end if;
        when others => -- link not synchro
        end case;

-- Channel 2 decoding :
        ch2_deco(1)   <= (others => '0');
        ch2_deco(2)   <= (others => '0');
        ch2_deco(3)   <= (others => '0');
        ch2_deco(4)   <= (others => '0');
        ch2_deco(5)   <= (others => '0');
        case ch2_in(31 downto 30) is
        when "01" => -- Baseline mode 5 samples per word
          ch2_deco(1)   <= "10000000"&ch2_in(5 downto 0);
          ch2_deco(2)   <= "10000000"&ch2_in(11 downto 6);
          ch2_deco(3)   <= "10000000"&ch2_in(17 downto 12);
          ch2_deco(4)   <= "10000000"&ch2_in(23 downto 18);
          ch2_deco(5)   <= "10000000"&ch2_in(29 downto 24);
          loc_DTU_frame_length(2) <= loc_DTU_frame_length(2)+1;
        when "10" => -- Baseline mode, at most 4 samples per word
          if ch2_in(26 downto 24) = "001" then
            ch2_deco(1) <= "10000000"&ch2_in(5 downto 0);
          end if;
          if ch2_in(26 downto 24) = "010" then
            ch2_deco(1) <= "10000000"&ch2_in(5 downto 0);
            ch2_deco(2) <= "10000000"&ch2_in(11 downto 6);
          end if;
          if ch2_in(26 downto 24) = "011" then
            ch2_deco(1) <= "10000000"&ch2_in(5 downto 0);
            ch2_deco(2) <= "10000000"&ch2_in(11 downto 6);
            ch2_deco(3) <= "10000000"&ch2_in(17 downto 12);
          end if;
          if ch2_in(26 downto 24) = "100" then
            ch2_deco(1) <= "10000000"&ch2_in(5 downto 0);
            ch2_deco(2) <= "10000000"&ch2_in(11 downto 6);
            ch2_deco(3) <= "10000000"&ch2_in(17 downto 12);
            ch2_deco(4) <= "10000000"&ch2_in(23 downto 18);
          end if;
          loc_DTU_frame_length(2) <= loc_DTU_frame_length(2)+1;
        when "00" => -- signal mode : 1 or 2 damples per word
          if ADC_MEM_mode = '0' then
            if ch2_in(29 downto 26) = "1010" then
              ch2_deco(1) <= "1"&ch2_in(12 downto 0);
              ch2_deco(2) <= "1"&ch2_in(25 downto 13);
            elsif ch2_in(29 downto 26) = "1011" then
              ch2_deco(1) <= "1"&ch2_in(12 downto 0);
            end if;
            signal_sample(2) <= '1';
            loc_DTU_frame_length(2) <= loc_DTU_frame_length(2)+1;
          else -- Official 80 MHz mode
            if ch2_in(29 downto 28) = "01" then
              ch2_deco(1) <= "1"&ch2_in(12 downto  0); -- ADCH(i)
              ch2_deco(2) <= "1"&ch2_in(25 downto 13); -- ADCH(i+2)
            elsif ch2_in(29 downto 28) = "00" then
              ch2_deco(1) <= (others => '0'); -- BC0
              ch2_deco(2) <= "1"&ch2_in(25 downto 13); -- ADCH(i+2)
            elsif ch2_in(29 downto 28) = "11" then
              ch2_deco(1) <= "1"&ch2_in(12 downto  0); -- ADCH(i)
              ch2_deco(2) <= (others => '0'); -- BC0
            end if;
          end if;
        when "11" => -- synchro frame or frame delimiter
          if ch2_in=x"EAAAAAAA" then
            idle_pattern(2) <= '1';
          end if;
          if ch2_in(31 downto 28)="1101" then
            DTU_frame_length(2)     <= loc_DTU_frame_length(2);
            loc_DTU_frame_length(2) <= (others => '0');
          end if;
        when others => -- link not synchro
        end case;

-- Channel 3 decoding :
        ch3_deco(1)   <= (others => '0');
        ch3_deco(2)   <= (others => '0');
        ch3_deco(3)   <= (others => '0');
        ch3_deco(4)   <= (others => '0');
        ch3_deco(5)   <= (others => '0');
        case ch3_in(31 downto 30) is
        when "01" => -- Baseline mode 5 samples per word
          ch3_deco(1)   <= "10000000"&ch3_in(5 downto 0);
          ch3_deco(2)   <= "10000000"&ch3_in(11 downto 6);
          ch3_deco(3)   <= "10000000"&ch3_in(17 downto 12);
          ch3_deco(4)   <= "10000000"&ch3_in(23 downto 18);
          ch3_deco(5)   <= "10000000"&ch3_in(29 downto 24);
          loc_DTU_frame_length(3) <= loc_DTU_frame_length(3)+1;
        when "10" => -- Baseline mode, at most 4 samples per word
          if ch3_in(26 downto 24) = "001" then
            ch3_deco(1) <= "10000000"&ch3_in(5 downto 0);
          end if;
          if ch3_in(26 downto 24) = "010" then
            ch3_deco(1) <= "10000000"&ch3_in(5 downto 0);
            ch3_deco(2) <= "10000000"&ch3_in(11 downto 6);
          end if;
          if ch3_in(26 downto 24) = "011" then
            ch3_deco(1) <= "10000000"&ch3_in(5 downto 0);
            ch3_deco(2) <= "10000000"&ch3_in(11 downto 6);
            ch3_deco(3) <= "10000000"&ch3_in(17 downto 12);
          end if;
          if ch3_in(26 downto 24) = "100" then
            ch3_deco(1) <= "10000000"&ch3_in(5 downto 0);
            ch3_deco(2) <= "10000000"&ch3_in(11 downto 6);
            ch3_deco(3) <= "10000000"&ch3_in(17 downto 12);
            ch3_deco(4) <= "10000000"&ch3_in(23 downto 18);
          end if;
          loc_DTU_frame_length(3) <= loc_DTU_frame_length(3)+1;
        when "00" => -- signal mode : 1 or 2 damples per word
          if ADC_MEM_mode = '0' then
            if ch3_in(29 downto 26) = "1010" then
              ch3_deco(1) <= "1"&ch3_in(12 downto 0);
              ch3_deco(2) <= "1"&ch3_in(25 downto 13);
            elsif ch3_in(29 downto 26) = "1011" then
              ch3_deco(1) <= "1"&ch3_in(12 downto 0);
            end if;
            signal_sample(3) <= '1';
            loc_DTU_frame_length(3) <= loc_DTU_frame_length(3)+1;
          else -- Official 80 MHz mode
            if ch3_in(29 downto 28) = "01" then
              ch3_deco(1) <= "1"&ch3_in(12 downto  0); -- ADCH(i)
              ch3_deco(2) <= "1"&ch3_in(25 downto 13); -- ADCH(i+2)
            elsif ch3_in(29 downto 28) = "00" then
              ch3_deco(1) <= (others => '0'); -- BC0
              ch3_deco(2) <= "1"&ch3_in(25 downto 13); -- ADCH(i+2)
            elsif ch3_in(29 downto 28) = "11" then
              ch3_deco(1) <= "1"&ch3_in(12 downto  0); -- ADCH(i)
              ch3_deco(2) <= (others => '0'); -- BC0
            end if;
          end if;
        when "11" => -- synchro frame or frame delimiter
          if ch3_in=x"EAAAAAAA" then
            idle_pattern(3) <= '1';
          end if;
          if ch3_in(31 downto 28)="1101" then
            DTU_frame_length(3)     <= loc_DTU_frame_length(3);
            loc_DTU_frame_length(3) <= (others => '0');
          end if;
        when others => -- link not synchro
        end case;

-- Channel 4 decoding :
        ch4_deco(1)   <= (others => '0');
        ch4_deco(2)   <= (others => '0');
        ch4_deco(3)   <= (others => '0');
        ch4_deco(4)   <= (others => '0');
        ch4_deco(5)   <= (others => '0');
        case ch4_in(31 downto 30) is
        when "01" => -- Baseline mode 5 samples per word
          ch4_deco(1)   <= "10000000"&ch4_in(5 downto 0);
          ch4_deco(2)   <= "10000000"&ch4_in(11 downto 6);
          ch4_deco(3)   <= "10000000"&ch4_in(17 downto 12);
          ch4_deco(4)   <= "10000000"&ch4_in(23 downto 18);
          ch4_deco(5)   <= "10000000"&ch4_in(29 downto 24);
          loc_DTU_frame_length(4) <= loc_DTU_frame_length(4)+1;
        when "10" => -- Baseline mode, at most 4 samples per word
          if ch4_in(26 downto 24) = "001" then
            ch4_deco(1) <= "10000000"&ch4_in(5 downto 0);
          end if;
          if ch4_in(26 downto 24) = "010" then
            ch4_deco(1) <= "10000000"&ch4_in(5 downto 0);
            ch4_deco(2) <= "10000000"&ch4_in(11 downto 6);
          end if;
          if ch4_in(26 downto 24) = "011" then
            ch4_deco(1) <= "10000000"&ch4_in(5 downto 0);
            ch4_deco(2) <= "10000000"&ch4_in(11 downto 6);
            ch4_deco(3) <= "10000000"&ch4_in(17 downto 12);
          end if;
          if ch4_in(26 downto 24) = "100" then
            ch4_deco(1) <= "10000000"&ch4_in(5 downto 0);
            ch4_deco(2) <= "10000000"&ch4_in(11 downto 6);
            ch4_deco(3) <= "10000000"&ch4_in(17 downto 12);
            ch4_deco(4) <= "10000000"&ch4_in(23 downto 18);
          end if;
          loc_DTU_frame_length(4) <= loc_DTU_frame_length(4)+1;
        when "00" => -- signal mode : 1 or 2 damples per word
          if ADC_MEM_mode = '0' then
            if ch4_in(29 downto 26) = "1010" then
              ch4_deco(1) <= "1"&ch4_in(12 downto 0);
              ch4_deco(2) <= "1"&ch4_in(25 downto 13);
            elsif ch4_in(29 downto 26) = "1011" then
              ch4_deco(1) <= "1"&ch4_in(12 downto 0);
            end if;
            signal_sample(4) <= '1';
            loc_DTU_frame_length(4) <= loc_DTU_frame_length(4)+1;
          else -- Official 80 MHz mode
            if ch4_in(29 downto 28) = "01" then
              ch4_deco(1) <= "1"&ch4_in(12 downto  0); -- ADCH(i)
              ch4_deco(2) <= "1"&ch4_in(25 downto 13); -- ADCH(i+2)
            elsif ch4_in(29 downto 28) = "00" then
              ch4_deco(1) <= (others => '0'); -- BC0
              ch4_deco(2) <= "1"&ch4_in(25 downto 13); -- ADCH(i+2)
            elsif ch4_in(29 downto 28) = "11" then
              ch4_deco(1) <= "1"&ch4_in(12 downto  0); -- ADCH(i)
              ch4_deco(2) <= (others => '0'); -- BC0
            end if;
          end if;
        when "11" => -- synchro frame or frame delimiter
          if ch4_in=x"EAAAAAAA" then
            idle_pattern(4) <= '1';
          end if;
          if ch4_in(31 downto 28)="1101" then
            DTU_frame_length(4)     <= loc_DTU_frame_length(4);
            loc_DTU_frame_length(4) <= (others => '0');
          end if;
        when others => -- link not synchro
        end case;

-- Channel 5 decoding :
        ch5_deco(1) <= (others => '0');
        ch5_deco(2) <= (others => '0');
        ch5_deco(3) <= (others => '0');
        ch5_deco(4) <= (others => '0');
        ch5_deco(5) <= (others => '0');
        case ch5_in(31 downto 30) is
        when "01" => -- Baseline mode 5 samples per word
          ch5_deco(1)   <= "10000000"&ch5_in(5 downto 0);
          ch5_deco(2)   <= "10000000"&ch5_in(11 downto 6);
          ch5_deco(3)   <= "10000000"&ch5_in(17 downto 12);
          ch5_deco(4)   <= "10000000"&ch5_in(23 downto 18);
          ch5_deco(5)   <= "10000000"&ch5_in(29 downto 24);
          loc_DTU_frame_length(5) <= loc_DTU_frame_length(5)+1;
        when "10" => -- Baseline mode, at most 4 samples per word
          if ch5_in(26 downto 24) = "001" then
            ch5_deco(1) <= x"10000000"&ch5_in(5 downto 0);
          end if;
          if ch5_in(26 downto 24) = "010" then
            ch5_deco(1) <= x"10000000"&ch5_in(5 downto 0);
            ch5_deco(2) <= x"10000000"&ch5_in(11 downto 6);
          end if;
          if ch5_in(26 downto 24) = "011" then
            ch5_deco(1) <= x"10000000"&ch5_in(5 downto 0);
            ch5_deco(2) <= x"10000000"&ch5_in(11 downto 6);
            ch5_deco(3) <= x"10000000"&ch5_in(17 downto 12);
          end if;
          if ch5_in(26 downto 24) = "100" then
            ch5_deco(1) <= x"10000000"&ch5_in(5 downto 0);
            ch5_deco(2) <= x"10000000"&ch5_in(11 downto 6);
            ch5_deco(3) <= x"10000000"&ch5_in(17 downto 12);
            ch5_deco(4) <= x"10000000"&ch5_in(23 downto 18);
          end if;
          loc_DTU_frame_length(5) <= loc_DTU_frame_length(5)+1;
        when "00" => -- signal mode : 1 or 2 damples per word
          if ADC_MEM_mode = '0' then
          if ch5_in(29 downto 26) = "1010" then
              ch5_deco(1) <= "1"&ch5_in(12 downto 0);
              ch5_deco(2) <= "1"&ch5_in(25 downto 13);
            elsif ch5_in(29 downto 26) = "1011" then
              ch5_deco(1) <= "1"&ch5_in(12 downto 0);
            end if;
            signal_sample(5) <= '1';
            loc_DTU_frame_length(5) <= loc_DTU_frame_length(5)+1;
          else -- Official 80 MHz mode
            if ch5_in(29 downto 28) = "01" then
              ch5_deco(1) <= "1"&ch5_in(12 downto  0); -- ADCH(i)
              ch5_deco(2) <= "1"&ch5_in(25 downto 13); -- ADCH(i+2)
            elsif ch5_in(29 downto 28) = "00" then
              ch5_deco(1) <= (others => '0'); -- BC0
              ch5_deco(2) <= "1"&ch5_in(25 downto 13); -- ADCH(i+2)
            elsif ch5_in(29 downto 28) = "11" then
              ch5_deco(1) <= "1"&ch5_in(12 downto  0); -- ADCH(i)
              ch5_deco(2) <= (others => '0'); -- BC0
            end if;
            ch5_deco(3) <= (others => '0');
            ch5_deco(4) <= (others => '0');
            ch5_deco(5) <= (others => '0');
          end if;
        when "11" => -- synchro frame or frame delimiter
          if ch5_in=x"EAAAAAAA" then
            idle_pattern(5) <= '1';
          end if;
          if ch5_in(31 downto 28)="1101" then
            DTU_frame_length(5)     <= loc_DTU_frame_length(5);
            loc_DTU_frame_length(5) <= (others => '0');
          end if;
        when others => -- link not synchro
        end case;
      else -- We are in test mode : Only data for CH1 et CH2 on 4 eLinks
        if ADC_MEM_mode = '0' then
          if ADC_invert_data = '0' then
            ch1_deco(1) <= "10"&ch2_in(11 downto  0); -- ADCH(i)
            ch1_deco(2) <= "10"&ch1_in(11 downto  0); -- ADCH(1+1)
            ch1_deco(3) <= "10"&ch2_in(27 downto 16); -- ADCH(i+2)
            ch1_deco(4) <= "10"&ch1_in(27 downto 16); -- ADCH(i+3)
            ch2_deco(1) <= "10"&ch4_in(11 downto  0); -- ADCL(i)
            ch2_deco(2) <= "10"&ch3_in(11 downto  0); -- ADCL(i+1)
            ch2_deco(3) <= "10"&ch4_in(27 downto 16); -- ADCL(i+2)
            ch2_deco(4) <= "10"&ch3_in(27 downto 16); -- ADCL(i+3)
          else
            ch1_deco(1) <= "10"&(not ch2_in(11 downto  0)); -- ADCH(i)
            ch1_deco(2) <= "10"&(not ch1_in(11 downto  0)); -- ADCH(1+1)
            ch1_deco(3) <= "10"&(not ch2_in(27 downto 16)); -- ADCH(i+2)
            ch1_deco(4) <= "10"&(not ch1_in(27 downto 16)); -- ADCH(i+3)
            ch2_deco(1) <= "10"&(not ch4_in(11 downto  0)); -- ADCL(i)
            ch2_deco(2) <= "10"&(not ch3_in(11 downto  0)); -- ADCL(i+1)
            ch2_deco(3) <= "10"&(not ch4_in(27 downto 16)); -- ADCL(i+2)
            ch2_deco(4) <= "10"&(not ch3_in(27 downto 16)); -- ADCL(i+3)
          end if;
          ch1_deco(5) <= (others => '0');
          ch2_deco(5) <= (others => '0');
          ch3_deco(1) <= (others => '0');
          ch3_deco(2) <= (others => '0');
          ch3_deco(3) <= (others => '0');
          ch3_deco(4) <= (others => '0');
          ch3_deco(5) <= (others => '0');
          ch4_deco(1) <= (others => '0');
          ch4_deco(2) <= (others => '0');
          ch4_deco(3) <= (others => '0');
          ch4_deco(4) <= (others => '0');
          ch4_deco(5) <= (others => '0');
          ch5_deco(1) <= (others => '0');
          ch5_deco(2) <= (others => '0');
          ch5_deco(3) <= (others => '0');
          ch5_deco(4) <= (others => '0');
          ch5_deco(5) <= (others => '0');
        else
          if ADC_invert_data = '0' then
            ch1_deco(1) <= "10"&ch1_in(11 downto  0); -- ADCH(i)
            ch1_deco(2) <= "10"&ch1_in(27 downto 16); -- ADCH(i+2)
            ch2_deco(1) <= "10"&ch2_in(11 downto  0); -- ADCL(i)
            ch2_deco(2) <= "10"&ch2_in(27 downto 16); -- ADCL(i+2)
            ch4_deco(1) <= "10"&ch4_in(11 downto  0); -- ADCH(i)
            ch4_deco(2) <= "10"&ch4_in(27 downto 16); -- ADCH(i+2)
            ch5_deco(1) <= "10"&ch5_in(11 downto  0); -- ADCL(i)
            ch5_deco(2) <= "10"&ch5_in(27 downto 16); -- ADCL(i+2)
          else
            ch1_deco(1) <= "10"&(not ch1_in(11 downto  0)); -- ADCH(i)
            ch1_deco(2) <= "10"&(not ch1_in(27 downto 16)); -- ADCH(i+2)
            ch2_deco(1) <= "10"&(not ch2_in(11 downto  0)); -- ADCL(i)
            ch2_deco(2) <= "10"&(not ch2_in(27 downto 16)); -- ADCL(i+2)
            ch4_deco(1) <= "10"&(not ch4_in(11 downto  0)); -- ADCH(i)
            ch4_deco(2) <= "10"&(not ch4_in(27 downto 16)); -- ADCH(i+2)
            ch5_deco(1) <= "10"&(not ch5_in(11 downto  0)); -- ADCL(i)
            ch5_deco(2) <= "10"&(not ch5_in(27 downto 16)); -- ADCL(i+2)
          end if;
          ch1_deco(3) <= (others => '0');
          ch1_deco(4) <= (others => '0');
          ch1_deco(5) <= (others => '0');
          ch2_deco(3) <= (others => '0');
          ch2_deco(4) <= (others => '0');
          ch2_deco(5) <= (others => '0');
          ch3_deco(1) <= (others => '0');
          ch3_deco(2) <= (others => '0');
          ch3_deco(3) <= (others => '0');
          ch3_deco(4) <= (others => '0');
          ch3_deco(5) <= (others => '0');
          ch4_deco(3) <= (others => '0');
          ch4_deco(4) <= (others => '0');
          ch4_deco(5) <= (others => '0');
          ch5_deco(3) <= (others => '0');
          ch5_deco(4) <= (others => '0');
          ch5_deco(5) <= (others => '0');
        end if;
      end if;
    end if;
  end process decode_local_data;

-- Control that we have idle patterns from times to times, which means that the links are still in sync
-- Count also the number of consecutive "signal" words, which could indicate a drift or wrong pedestals
  watch_dog : process (clk_sequence, reset) is
  variable clock_counter        : unsigned(7 downto 0) := (others => '0');
  variable loc_idle_counter     : UInt8_t(5 downto 1)  := (others => (others => '0'));
  variable loc_signal_counter   : UInt8_t(5 downto 1)  := (others => (others => '0'));

  begin
    if reset ='1' then
      clock_counter        := (others => '0');
      sync_error           <= (others => '0');
      signal_error         <= (others => '0');
      DTU_frame_error      <= (others => '0');
      loc_signal_counter   := (others => (others => '0'));
      loc_idle_counter     := (others => (others => '0'));
    elsif rising_edge(clk_sequence) then
      if clock_counter = 0 then
        loc_signal_counter := (others => (others => '0'));
        loc_idle_counter   := (others => (others => '0'));
      end if;
      if clock_counter < x"40" then
        clock_counter := clock_counter + 1;
        if signal_sample(1) = '1' then
          loc_signal_counter(1) := loc_signal_counter(1)+1;
        end if;
        if signal_sample(2) = '1' then
          loc_signal_counter(2) := loc_signal_counter(2)+1;
        end if;
        if signal_sample(3) = '1' then
          loc_signal_counter(3) := loc_signal_counter(3)+1;
        end if;
        if signal_sample(4) = '1' then
          loc_signal_counter(4) := loc_signal_counter(4)+1;
        end if;
        if signal_sample(5) = '1' then
          loc_signal_counter(5) := loc_signal_counter(5)+1;
        end if;
        if idle_pattern(1) = '1' then
          loc_idle_counter(1) := loc_idle_counter(1)+1;
        end if;
        if idle_pattern(2) = '1' then
          loc_idle_counter(2) := loc_idle_counter(2)+1;
        end if;
        if idle_pattern(3) = '1' then
          loc_idle_counter(3) := loc_idle_counter(3)+1;
        end if;
        if idle_pattern(4) = '1' then
          loc_idle_counter(4) := loc_idle_counter(4)+1;
        end if;
        if idle_pattern(5) = '1' then
          loc_idle_counter(5) := loc_idle_counter(5)+1;
        end if;
      else
        clock_counter     := (others => '0');
        idle_counter(1)   <= loc_idle_counter(1);
        idle_counter(2)   <= loc_idle_counter(2);
        idle_counter(3)   <= loc_idle_counter(3);
        idle_counter(4)   <= loc_idle_counter(4);
        idle_counter(5)   <= loc_idle_counter(5);
        signal_counter(1) <= loc_signal_counter(1);
        signal_counter(2) <= loc_signal_counter(2);
        signal_counter(3) <= loc_signal_counter(3);
        signal_counter(4) <= loc_signal_counter(4);
        signal_counter(5) <= loc_signal_counter(5);
      end if;

      sync_error          <= (others => '0');
      signal_error        <= (others => '0');
      if idle_counter(1) < 8 then
        sync_error(1) <= '1';
      end if;
      if idle_counter(2) < 8 then
        sync_error(2) <= '1';
      end if;
      if idle_counter(3) < 8 then
        sync_error(3) <= '1';
      end if;
      if idle_counter(4) < 8 then
        sync_error(4) <= '1';
      end if;
      if idle_counter(5) < 8 then
        sync_error(5) <= '1';
      end if;
      if signal_counter(1) > 32 then
        signal_error(1) <= '1';
      end if;
      if signal_counter(2) > 32 then
        signal_error(2) <= '1';
      end if;
      if signal_counter(3) > 32 then
        signal_error(3) <= '1';
      end if;
      if signal_counter(4) > 32 then
        signal_error(4) <= '1';
      end if;
      if signal_counter(5) > 32 then
        signal_error(5) <= '1';
      end if;

      DTU_frame_error <= (others => '1');
      if DTU_frame_length(1) = x"32" then
        DTU_frame_error(1) <= '0';
      end if;
      if DTU_frame_length(2) = x"32" then
        DTU_frame_error(2) <= '0';
      end if;
      if DTU_frame_length(3) = x"32" then
        DTU_frame_error(3) <= '0';
      end if;
      if DTU_frame_length(4) = x"32" then
        DTU_frame_error(4) <= '0';
      end if;
      if DTU_frame_length(5) = x"32" then
        DTU_frame_error(5) <= '0';
      end if;

    end if;
  end process;
  
  gen_local_trigger : process (clk_sequence, trig_self, reset) is
  begin
    if reset ='1' then
      trig_local <= '0';
      trig_width <= (others => '0');
      ch1_deco(0) <= (others => '0');
      ch2_deco(0) <= (others => '0');
      ch3_deco(0) <= (others => '0');
      ch4_deco(0) <= (others => '0');
      ch5_deco(0) <= (others => '0');
    elsif rising_edge(clk_sequence) then
      if trig_self = '1' and trig_width = 0 and write_state = write_idle and capture_running = '1' then -- Wait for the end of previous trigger has been processed
        if (((ch1_deco(1)(13)='1' and (ch1_val(1)  > trig_thres) and trig_self_mask(0)='1') or
             (ch1_deco(2)(13)='1' and (ch1_val(2)  > trig_thres) and trig_self_mask(0)='1') or
             (ch1_deco(3)(13)='1' and (ch1_val(3)  > trig_thres) and trig_self_mask(0)='1') or
             (ch1_deco(4)(13)='1' and (ch1_val(4)  > trig_thres) and trig_self_mask(0)='1') or
             (ch1_deco(5)(13)='1' and (ch1_val(5)  > trig_thres) and trig_self_mask(0)='1') or
             (ch2_deco(1)(13)='1' and (ch2_val(1)  > trig_thres) and trig_self_mask(1)='1') or
             (ch2_deco(2)(13)='1' and (ch2_val(2)  > trig_thres) and trig_self_mask(1)='1') or
             (ch2_deco(3)(13)='1' and (ch2_val(3)  > trig_thres) and trig_self_mask(1)='1') or
             (ch2_deco(4)(13)='1' and (ch2_val(4)  > trig_thres) and trig_self_mask(1)='1') or
             (ch2_deco(5)(13)='1' and (ch2_val(5)  > trig_thres) and trig_self_mask(1)='1') or
             (ch3_deco(1)(13)='1' and (ch3_val(1)  > trig_thres) and trig_self_mask(2)='1') or
             (ch3_deco(2)(13)='1' and (ch3_val(2)  > trig_thres) and trig_self_mask(2)='1') or
             (ch3_deco(3)(13)='1' and (ch3_val(3)  > trig_thres) and trig_self_mask(2)='1') or
             (ch3_deco(4)(13)='1' and (ch3_val(4)  > trig_thres) and trig_self_mask(2)='1') or
             (ch3_deco(5)(13)='1' and (ch3_val(5)  > trig_thres) and trig_self_mask(2)='1') or
             (ch4_deco(1)(13)='1' and (ch4_val(1)  > trig_thres) and trig_self_mask(3)='1') or
             (ch4_deco(2)(13)='1' and (ch4_val(2)  > trig_thres) and trig_self_mask(3)='1') or
             (ch4_deco(3)(13)='1' and (ch4_val(3)  > trig_thres) and trig_self_mask(3)='1') or
             (ch4_deco(4)(13)='1' and (ch4_val(4)  > trig_thres) and trig_self_mask(3)='1') or
             (ch4_deco(5)(13)='1' and (ch4_val(5)  > trig_thres) and trig_self_mask(3)='1') or
             (ch5_deco(1)(13)='1' and (ch5_val(1)  > trig_thres) and trig_self_mask(4)='1') or
             (ch5_deco(2)(13)='1' and (ch5_val(2)  > trig_thres) and trig_self_mask(4)='1') or
             (ch5_deco(3)(13)='1' and (ch5_val(3)  > trig_thres) and trig_self_mask(4)='1') or
             (ch5_deco(4)(13)='1' and (ch5_val(4)  > trig_thres) and trig_self_mask(4)='1') or
             (ch5_deco(5)(13)='1' and (ch5_val(5)  > trig_thres) and trig_self_mask(4)='1')) and trig_self_mode = '0') 
           or
           (((abs(ch1_delta_val)>signed(trig_thres) and trig_self_mask(0)='1') or
             (abs(ch2_delta_val)>signed(trig_thres) and trig_self_mask(1)='1') or
             (abs(ch3_delta_val)>signed(trig_thres) and trig_self_mask(2)='1') or
             (abs(ch4_delta_val)>signed(trig_thres) and trig_self_mask(3)='1') or
             (abs(ch5_delta_val)>signed(trig_thres) and trig_self_mask(4)='1')) and trig_self_mode = '1')
        then
          trig_width <= (others => '1');
          trig_local <= '1';
        else
          trig_local <= '0';
        end if;
      elsif trig_width > 0 then
        trig_width <= trig_width-1;
      else
        trig_local <= '0';
      end if;
      ch1_delta_val      <= (others => '0');
      ch2_delta_val      <= (others => '0');
      ch3_delta_val      <= (others => '0');
      ch4_delta_val      <= (others => '0');
      ch5_delta_val      <= (others => '0');
      if ch1_deco(0)(13)='1' and ch1_deco(1)(13)='1' then
        ch1_delta_val <= signed('0'&ch1_val(1))-signed('0'&ch1_val(0));
      end if;
      if ch1_deco(0)(13)='1' and ch1_deco(2)(13)='1' and ch1_val(2)>ch1_val(1) then
        ch1_delta_val <= signed('0'&ch1_val(2))-signed('0'&ch1_val(0));
      end if;
      if ch1_deco(0)(13)='1' and ch1_deco(3)(13)='1' and ch1_val(3)>ch1_val(2) and ch1_val(3)>ch1_val(1) then
        ch1_delta_val <= signed('0'&ch1_val(3))-signed('0'&ch1_val(0));
      end if;
      if ch1_deco(0)(13)='1' and ch1_deco(4)(13)='1' and ch1_val(4)>ch1_val(3) and ch1_val(4)>ch1_val(2) and ch1_val(4)>ch1_val(1) then
        ch1_delta_val <= signed('0'&ch1_val(4))-signed('0'&ch1_val(0));
      end if;
      if ch1_deco(0)(13)='1' and ch1_deco(5)(13)='1' and ch1_val(5)>ch1_val(4) and ch1_val(5)>ch1_val(3) and ch1_val(5)>ch1_val(2) and ch1_val(5)>ch1_val(1) then
        ch1_delta_val <= signed('0'&ch1_val(5))-signed('0'&ch1_val(0));
      end if;
      
      if ch2_deco(0)(13)='1' and ch2_deco(1)(13)='1' then
        ch2_delta_val <= signed('0'&ch2_val(1))-signed('0'&ch2_val(0));
      end if;
      if ch2_deco(0)(13)='1' and ch2_deco(2)(13)='1' and ch2_val(2)>ch2_val(1) then
        ch2_delta_val <= signed('0'&ch2_val(2))-signed('0'&ch2_val(0));
      end if;
      if ch2_deco(0)(13)='1' and ch2_deco(3)(13)='1' and ch2_val(3)>ch2_val(2) and ch2_val(3)>ch2_val(1) then
        ch2_delta_val <= signed('0'&ch2_val(3))-signed('0'&ch2_val(0));
      end if;
      if ch2_deco(0)(13)='1' and ch2_deco(4)(13)='1' and ch2_val(4)>ch2_val(3) and ch2_val(4)>ch2_val(2) and ch2_val(4)>ch2_val(1) then
        ch2_delta_val <= signed('0'&ch2_val(4))-signed('0'&ch2_val(0));
      end if;
      if ch2_deco(0)(13)='1' and ch2_deco(5)(13)='1' and ch2_val(5)>ch2_val(4) and ch2_val(5)>ch2_val(3) and ch2_val(5)>ch2_val(2) and ch2_val(5)>ch2_val(1) then
        ch2_delta_val <= signed('0'&ch2_val(5))-signed('0'&ch2_val(0));
      end if;
      
      if ch3_deco(0)(13)='1' and ch3_deco(1)(13)='1' then
        ch3_delta_val <= signed('0'&ch3_val(1))-signed('0'&ch3_val(0));
      end if;
      if ch3_deco(0)(13)='1' and ch3_deco(2)(13)='1' and ch3_val(2)>ch3_val(1) then
        ch3_delta_val <= signed('0'&ch3_val(2))-signed('0'&ch3_val(0));
      end if;
      if ch3_deco(0)(13)='1' and ch3_deco(3)(13)='1' and ch3_val(3)>ch3_val(2) and ch3_val(3)>ch3_val(1) then
        ch3_delta_val <= signed('0'&ch3_val(3))-signed('0'&ch3_val(0));
      end if;
      if ch3_deco(0)(13)='1' and ch3_deco(4)(13)='1' and ch3_val(4)>ch3_val(3) and ch3_val(4)>ch3_val(2) and ch3_val(4)>ch3_val(1) then
        ch3_delta_val <= signed('0'&ch3_val(4))-signed('0'&ch3_val(0));
      end if;
      if ch3_deco(0)(13)='1' and ch3_deco(5)(13)='1' and ch3_val(5)>ch3_val(4) and ch3_val(5)>ch3_val(3) and ch3_val(5)>ch3_val(2) and ch3_val(5)>ch3_val(1) then
        ch3_delta_val <= signed('0'&ch3_val(5))-signed('0'&ch3_val(0));
      end if;
      
      if ch4_deco(0)(13)='1' and ch4_deco(1)(13)='1' then
        ch4_delta_val <= signed('0'&ch4_val(1))-signed('0'&ch4_val(0));
      end if;
      if ch4_deco(0)(13)='1' and ch4_deco(2)(13)='1' and ch4_val(2)>ch1_val(1) then
        ch4_delta_val <= signed('0'&ch4_val(2))-signed('0'&ch4_val(0));
      end if;
      if ch4_deco(0)(13)='1' and ch4_deco(3)(13)='1' and ch4_val(3)>ch1_val(2) and ch4_val(3)>ch4_val(1) then
        ch4_delta_val <= signed('0'&ch4_val(3))-signed('0'&ch4_val(0));
      end if;
      if ch4_deco(0)(13)='1' and ch4_deco(4)(13)='1' and ch4_val(4)>ch1_val(3) and ch4_val(4)>ch4_val(2) and ch4_val(4)>ch4_val(1) then
        ch4_delta_val <= signed('0'&ch4_val(4))-signed('0'&ch4_val(0));
      end if;
      if ch4_deco(0)(13)='1' and ch4_deco(5)(13)='1' and ch4_val(5)>ch1_val(4) and ch4_val(5)>ch4_val(3) and ch4_val(5)>ch4_val(2) and ch4_val(5)>ch4_val(1) then
        ch4_delta_val <= signed('0'&ch4_val(5))-signed('0'&ch4_val(0));
      end if;
      
      if ch5_deco(0)(13)='1' and ch5_deco(1)(13)='1' then
        ch5_delta_val <= signed('0'&ch5_val(1))-signed('0'&ch5_val(0));
      end if;
      if ch5_deco(0)(13)='1' and ch5_deco(2)(13)='1' and ch5_val(2)>ch5_val(1) then
        ch5_delta_val <= signed('0'&ch5_val(2))-signed('0'&ch5_val(0));
      end if;
      if ch5_deco(0)(13)='1' and ch5_deco(3)(13)='1' and ch5_val(3)>ch5_val(2) and ch5_val(3)>ch5_val(1) then
        ch5_delta_val <= signed('0'&ch5_val(3))-signed('0'&ch5_val(0));
      end if;
      if ch5_deco(0)(13)='1' and ch5_deco(4)(13)='1' and ch5_val(4)>ch5_val(3) and ch5_val(4)>ch5_val(2) and ch5_val(4)>ch5_val(1) then
        ch5_delta_val <= signed('0'&ch5_val(4))-signed('0'&ch5_val(0));
      end if;
      if ch5_deco(0)(13)='1' and ch5_deco(5)(13)='1' and ch5_val(5)>ch5_val(4) and ch5_val(5)>ch5_val(3) and ch5_val(5)>ch5_val(2) and ch5_val(5)>ch5_val(1) then
        ch5_delta_val <= signed('0'&ch5_val(5))-signed('0'&ch5_val(0));
      end if;

      if ch1_deco(5)(13)='1' then
        ch1_val(0)      <= ch1_val(5);
        ch1_deco(0)(13) <= '1';
      elsif ch1_deco(4)(13)='1' then
        ch1_val(0)      <= ch1_val(4);
        ch1_deco(0)(13) <= '1';
      elsif ch1_deco(3)(13)='1' then
        ch1_val(0)      <= ch1_val(3);
        ch1_deco(0)(13) <= '1';
      elsif ch1_deco(2)(13)='1' then
        ch1_val(0)      <= ch1_val(2);
        ch1_deco(0)(13) <= '1';
      elsif ch1_deco(1)(13)='1' then
        ch1_val(0)      <= ch1_val(1);
        ch1_deco(0)(13) <= '1';
      end if;
      
      if ch2_deco(5)(13)='1' then
        ch2_val(0)      <= ch2_val(5);
        ch2_deco(0)(13) <= '1';
      elsif ch2_deco(4)(13)='1' then
        ch2_val(0)      <= ch2_val(4);
        ch2_deco(0)(13) <= '1';
      elsif ch2_deco(3)(13)='1' then
        ch2_val(0)      <= ch2_val(3);
        ch2_deco(0)(13) <= '1';
      elsif ch2_deco(2)(13)='1' then
        ch2_val(0)      <= ch2_val(2);
        ch2_deco(0)(13) <= '1';
      elsif ch2_deco(1)(13)='1' then
        ch2_val(0)      <= ch2_val(1);
        ch2_deco(0)(13) <= '1';
      end if;
      
      if ch3_deco(5)(13)='1' then
        ch3_val(0)      <= ch3_val(5);
        ch3_deco(0)(13) <= '1';
      elsif ch3_deco(4)(13)='1' then
        ch3_val(0)      <= ch3_val(4);
        ch3_deco(0)(13) <= '1';
      elsif ch3_deco(3)(13)='1' then
        ch3_val(0)      <= ch3_val(3);
        ch3_deco(0)(13) <= '1';
      elsif ch3_deco(2)(13)='1' then
        ch3_val(0)      <= ch3_val(2);
        ch3_deco(0)(13) <= '1';
      elsif ch3_deco(1)(13)='1' then
        ch3_val(0)      <= ch3_val(1);
        ch3_deco(0)(13) <= '1';
      end if;
      
      if ch4_deco(5)(13)='1' then
        ch4_val(0)      <= ch4_val(5);
        ch4_deco(0)(13) <= '1';
      elsif ch4_deco(4)(13)='1' then
        ch4_val(0)      <= ch4_val(4);
        ch4_deco(0)(13) <= '1';
      elsif ch4_deco(3)(13)='1' then
        ch4_val(0)      <= ch4_val(3);
        ch4_deco(0)(13) <= '1';
      elsif ch4_deco(2)(13)='1' then
        ch4_val(0)      <= ch4_val(2);
        ch4_deco(0)(13) <= '1';
      elsif ch4_deco(1)(13)='1' then
        ch4_val(0)      <= ch4_val(1);
        ch4_deco(0)(13) <= '1';
      end if;
      
      if ch5_deco(5)(13)='1' then
        ch5_val(0)      <= ch5_val(5);
        ch5_deco(0)(13) <= '1';
      elsif ch5_deco(4)(13)='1' then
        ch5_val(0)      <= ch5_val(4);
        ch5_deco(0)(13) <= '1';
      elsif ch5_deco(3)(13)='1' then
        ch5_val(0)      <= ch5_val(3);
        ch5_deco(0)(13) <= '1';
      elsif ch5_deco(2)(13)='1' then
        ch5_val(0)      <= ch5_val(2);
        ch5_deco(0)(13) <= '1';
      elsif ch5_deco(1)(13)='1' then
        ch5_val(0)      <= ch5_val(1);
        ch5_deco(0)(13) <= '1';
      end if;
      
    end if;
  end process gen_local_trigger;

-- Shift register to delay the input data waiting for trigger
  channel_vfe_data_in(31  downto 0)   <= ch1_in;
  channel_vfe_data_in(63  downto 32)  <= ch2_in;
  channel_vfe_data_in(95  downto 64)  <= ch3_in;
  channel_vfe_data_in(127 downto 96)  <= ch4_in;
  channel_vfe_data_in(159 downto 128) <= ch5_in;
  inst_c_shift_ram_0 : c_shift_ram_0
  port map (
    d   => channel_vfe_data_in,
    clk => clk_shift_reg,
    q   => channel_vfe_data_delayed
  );

  clka_ram <= clk_sequence;
  clkb_ram <= not clk_ipbus;
  -- Dual port channel rams for capture and readout
  inst_blk_mem_gen_0 : blk_mem_gen_0
  port map (
    clka   => clka_ram,
    ena    => capture_running,
    wea(0) => capture_write,
    addra  => std_logic_vector(write_address),
    dina   => channel_ram_data_in,
    clkb   => clka_ram,
--    enb    => channel_ram_data_read,
    enb    => '1',
    addrb  => std_logic_vector(read_address),
    doutb  => channel_ram_data_out
  );

  -- Capture control (triggering, addressing)
  trig_sig_del1  <= trig_signal   when Rising_Edge(clk_sequence)  else trig_sig_del1;
  trig_sig_del2  <= trig_sig_del1 when Rising_Edge(clk_sequence)  else trig_sig_del2;
  trig_short     <= trig_sig_del1 and not trig_sig_del2;

  -- Synchronize ram_read signal with capture clock to increase available memory for capture
  ram_read_del1  <= ram_read      when Rising_Edge(clk_sequence)  else ram_read_del1;
  ram_read_del2  <= ram_read_del1 when Rising_Edge(clk_sequence)  else ram_read_del2;
  ram_read_short <= ram_read_del1 and not ram_read_del2;

  capture_data : process (clk_sequence, reset) is
  variable delay : unsigned(7 downto 0) := x"00";
  begin  -- process capture_counter
    if reset = '1' then
      DAQ_busy                <= '0';
      capture_write           <= '0';
      capture_running         <= '0';
      write_address           <= MAX_RAM_ADDRESS;
      free_ram                <= MAX_RAM_ADDRESS;
      ram_full                <= '0';
      delay                   := x"00";
      write_state             <= write_start;
    elsif rising_edge(clk_sequence) then
      capture_write           <= '0';
      case write_state is
      when write_start =>
        DAQ_busy              <= '0';
        nsample               <= (others => '0');
        if delay /= x"FF" then
          delay               := delay+1;
        else
          write_state         <= write_idle;
        end if;
      when write_idle =>
        DAQ_busy              <= '0';
        if capture_start = '1' then   --start control
          capture_running     <= '1';
          write_address       <= MAX_RAM_ADDRESS;
          free_ram            <= MAX_RAM_ADDRESS;
          ram_full            <= '0';
        end if;
        if capture_stop = '1' then
          capture_running     <= '0';
        end if;
        if free_ram < frame_length  then
          ram_full            <= '1';
        else
          ram_full            <= '0';
        end if;
        if capture_running = '1' and trig_short = '1' then
          if FIFO_mode = '0' then   --Reset write address if single event mode
            write_address     <= MAX_RAM_ADDRESS;
            free_ram          <= MAX_RAM_ADDRESS;
            ram_full          <= '0';
            nsample           <= (others=> '0');
            write_state       <= write_running;
          end if;
          if ram_full = '0' then
            nsample           <= (others => '0');
            write_state       <= write_running;
          end if;
        end if;
        if ram_read_short = '1' and free_ram /= MAX_RAM_ADDRESS then
          free_ram            <= free_ram + 1;
        end if;
      when write_running =>
        DAQ_busy              <= '1';
        capture_write <= '1';
        if nsample = "000"&x"000" then
          channel_ram_data_in(159 downto 64) <= (others => '1');
          channel_ram_data_in(63 downto 0)  <= std_logic_vector(time_stamp);
        else
          channel_ram_data_in(159 downto 0)  <= channel_vfe_data_delayed;
--          channel_ram_data_in(79 downto 0) <= x"F0F0F0F0F0F0F0F0F0F0";
        end if;
        if nsample = (frame_length-1) then
          write_state         <= write_idle;
        end if;
        nsample               <= nsample + 1;
        if write_address = MAX_RAM_ADDRESS then
          write_address       <= (others => '0');
        else
          write_address       <= write_address+1;
        end if;
        if ram_read_short = '0' then
          free_ram            <= free_ram - 1;
        end if;

        if capture_stop = '1' then
          capture_running     <= '0';
          write_state <= write_idle;
        end if;
      end case;
    end if;
  end process capture_data;

-- Running clk counter to get a timestamp
  clk_counter : process(reset, capture_start, clk_sequence)
  begin
    if reset = '1' or capture_start = '1' then
      time_stamp <= x"0000000000000000";
    elsif rising_edge(clk_sequence) then
      time_stamp <= time_stamp+1;
    end if;
  end process clk_counter;

  end generate;
  
-- Process ipbus read/writes
  ipbus_data : process(reset, clk_ipbus)
  begin
    if reset = '1' then
      read_address          <= MAX_RAM_ADDRESS;
      read_cycle            <= first;
      ipbus_rw_state        <= ipbus_rw_start;
      channel_ram_data_read <= '0';
      ram_read              <= '0';
      ack                   <= '0';
    elsif rising_edge(clk_ipbus) then
      capture_start_strobe  <= '0';
      capture_stop_strobe   <= '0';
      channel_ram_data_read <= '0';
      ram_read              <= '0';
      ack                   <= '0';
      ipbus_state_machine : case ipbus_rw_state is
      when ipbus_rw_start =>
        read_address   <= MAX_RAM_ADDRESS;
        read_cycle     <= first;
        ipbus_rw_state <= ipbus_rw_idle;
      when ipbus_rw_idle =>
        if ipbus_in.ipb_strobe = '1' then
          if ipbus_in.ipb_write = '1' then
            ipbus_rw_state <= ipbus_write_start;
          else
            ipbus_rw_state <= ipbus_read_start;
          end if;
        end if;
      when ipbus_write_start =>
        case ipbus_in.ipb_addr(15 downto 0) is
          when x"0000" => --control registers
            capture_start_strobe <= ipbus_in.ipb_wdata(0);
            capture_stop_strobe  <= ipbus_in.ipb_wdata(1);
            frame_length         <= unsigned(ipbus_in.ipb_wdata(30 downto 16));
          when x"0001" => --Start read address
            read_address <= unsigned(ipbus_in.ipb_wdata(14 downto 0));
            read_cycle   <= first;
          when others => null;
        end case;
        ipbus_rw_state <= ipbus_ack;
      when ipbus_read_start =>
        ipbus_out.ipb_rdata <= (others => '0'); 
        decode : case ipbus_in.ipb_addr(15) is
        when '0' =>          --control registers
          case ipbus_in.ipb_addr(1 downto 0) is
          when "00" =>
            ipbus_out.ipb_rdata <= "0"&std_logic_vector(frame_length) & x"000" & "00" & capture_stop_strobe & capture_start_strobe;
          when "01" =>
            ipbus_out.ipb_rdata <= "0"&std_logic_vector(write_address) & "0"&std_logic_vector(read_address);
          when "10" =>
            ipbus_out.ipb_rdata <= x"0000"&"0"&std_logic_vector(free_ram);
          when others => null;
          end case;
          ipbus_rw_state <= ipbus_ack;
        when '1' => -- Captured data
          if (read_address = write_address and read_cycle = first) or
             (free_ram = MAX_RAM_ADDRESS) then
            ipbus_out.ipb_rdata <= x"DEADBEEF";
            ipbus_rw_state      <= ipbus_ack;
          else
            if read_cycle = first then
              if read_address = MAX_RAM_ADDRESS then
                read_address <= (others => '0');
              else
                read_address <= read_address + 1;
              end if;
            end if;
            ipbus_rw_state      <= ipbus_read;
          end if;          
        when others =>
          ipbus_out.ipb_rdata <= x"DEADBEEF";
          ipbus_rw_state <= ipbus_ack;
        end case decode;
      when ipbus_read =>
        dispatch : case read_cycle is
        when first =>
          ipbus_out.ipb_rdata(31 downto 0) <= "000"&DTU_frame_error&"000"&signal_error&"0"&std_logic_vector(read_address(14 downto 0));
          read_cycle <= second;
        when second =>
          ipbus_out.ipb_rdata(31 downto 0)  <= channel_ram_data_out(31 downto 0);
          read_cycle <= third;
        when third =>
          ipbus_out.ipb_rdata(31 downto 0)  <= channel_ram_data_out(63 downto 32);
          read_cycle <= fourth;
        when fourth =>
          ipbus_out.ipb_rdata(31 downto 0)  <= channel_ram_data_out(95 downto 64);
          read_cycle <= fifth;
        when fifth =>
          ipbus_out.ipb_rdata(31 downto 0)  <= channel_ram_data_out(127 downto 96);
          read_cycle <= sixth;
        when sixth =>
          ipbus_out.ipb_rdata(31 downto 0)  <= channel_ram_data_out(159 downto 128);
          ram_read       <= '1';
          read_cycle <= first;
        when others =>
          read_cycle <= first;
        end case dispatch;
        ipbus_rw_state   <= ipbus_ack;
      when ipbus_ack =>
        if capture_start_strobe = '1' then -- Reset read address when we restart capture
          read_address <= MAX_RAM_ADDRESS;
          read_cycle   <= first;
        end if;
        ack          <= '1';
        ipbus_rw_state   <= ipbus_finished;
      when ipbus_finished =>
        ack          <= '0';
        if FIFO_mode = '0' then
          read_address   <= MAX_RAM_ADDRESS;
          read_cycle     <= first;
        end if;
        ipbus_rw_state   <= ipbus_rw_idle;
      when others =>
        ack          <= '0';
        ipbus_rw_state   <= ipbus_rw_idle;
      end case ipbus_state_machine;
    end if;
  end process ipbus_data;

  capture_start_strobe1 <= capture_start_strobe  when Rising_Edge(clk_sequence)  else capture_start_strobe1;
  capture_start_strobe2 <= capture_start_strobe1 when Rising_Edge(clk_sequence)  else capture_start_strobe2;
  capture_start         <= capture_start_strobe1 and not capture_start_strobe2;

  capture_stop_strobe1 <= capture_stop_strobe  when Rising_Edge(clk_sequence)  else capture_stop_strobe1;
  capture_stop_strobe2 <= capture_stop_strobe1 when Rising_Edge(clk_sequence)  else capture_stop_strobe2;
  capture_stop         <= capture_stop_strobe1 and not capture_stop_strobe2;

--  pacd_1 : pacd
--    port map (
--      iPulseA => capture_start_strobe,
--      iClkA   => clk_ipbus,
--      iRSTAn  => '1',
--      iClkB   => clk_cap,
--      iRSTBn  => '1',
--      oPulseB => capture_start
--    );
--  pacd_2 : pacd
--    port map (
--      iPulseA => capture_stop_strobe,
--      iClkA   => clk_ipbus,
--      iRSTAn  => '1',
--      iClkB   => clk_cap,
--      iRSTBn  => '1',
--      oPulseB => capture_stop
--    );

  ipbus_out.ipb_ack <= ack;
  ipbus_out.ipb_err <= '0';

--  ila_0_2: ila_0
--    port map (
--      clk    => clk_cap,
--      probe0(0) => capture_running,
--      probe1(0) => capture_start,
--      probe2(0) => capture_stop,
--      probe3 => std_logic_vector(capture_address(7 downto 0)));
--  ila_0_2 : ila_0
--    port map (
--      clk       => clk_ipbus,
--      probe0(0) => ipbus_in.ipb_strobe,
--      probe1(0) => channel_ram_data_out(channel_index)(0),
--      probe2(0) => channel_ram_data_out(channel_index)(1),
--      probe3    => std_logic_vector(ipbus_sample_address(7 downto 0)));

end rtl;
