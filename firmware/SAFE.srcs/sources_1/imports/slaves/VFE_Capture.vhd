library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.SAFE_IO.all;

entity VFE_Capture is
  generic(
   ADDR_LENGTH            : integer := 12;
   MAX_RAM_ADDRESS        : unsigned(ADDR_LENGTH-1 downto 0) := (others => '1')
  );
  port(
    DAQ_busy          : out std_logic;
    clk_ipbus         : in  std_logic;
    reset             : in  std_logic;
    ipbus_in          : in  ipb_wbus;
    ipbus_out         : out ipb_rbus;
    clk_40            : in std_logic;
    clk_shift_reg     : in std_logic;
    ch_in             : in Word_t(Nb_of_Lines downto 1);
    trig_local        : out std_logic;
    trig_self         : in std_logic;
    trig_self_mode    : in std_logic;
    trig_self_mask    : in std_logic_vector(Nb_of_Lines downto 1);
    trig_self_thres   : in std_logic_vector(12 downto 0);
    FIFO_mode         : in std_logic;
    DTU_test_mode     : in std_logic;
    DTU_MEM_mode      : in std_logic;
    trig_signal       : in std_logic;
    CRC_reset         : in std_logic;
    CRC_error         : out UInt32_t(Nb_of_Lines downto 1)
  );

end VFE_Capture;

architecture rtl of VFE_Capture is

  component blk_mem_gen_0 is -- 4k samples of 160 bits *5
    port (
      clka  : in  std_logic;
      ena   : in  std_logic;
      wea   : in  std_logic_vector (0 to 0);
      addra : in  std_logic_vector (ADDR_LENGTH-1 downto 0);
      dina  : in  std_logic_vector (159 downto 0);
      clkb  : in  std_logic;
      enb   : in  std_logic;
      addrb : in  std_logic_vector (ADDR_LENGTH-1 downto 0);
      doutb : out std_logic_vector (159 downto 0)
    );
  end component blk_mem_gen_0;

  component c_shift_ram_0 IS
  port (
    d   : IN STD_LOGIC_VECTOR(159 downto 0);
    clk : IN STD_LOGIC;
    q   : OUT STD_LOGIC_VECTOR(159 downto 0)
  );
  end component c_shift_ram_0;

  signal sel : integer;
  signal ack : std_logic;

  signal ipbus_sample_address     : std_logic_vector(15 downto 0) := x"0000";

  type reg_data_t is array (integer range <>) of std_logic_vector(159 downto 0);

  signal vfe_data_in           : reg_data_t(Nb_of_VFE downto 1) := (others => (others =>'0'));
  signal vfe_data_prev         : reg_data_t(Nb_of_VFE downto 1) := (others => (others =>'0'));
  signal vfe_data_delayed      : reg_data_t(Nb_of_VFE downto 1) := (others => (others =>'0'));
  signal ram_data_in           : reg_data_t(Nb_of_VFE downto 1) := (others => (others =>'0'));
  signal ram_data_out          : reg_data_t(Nb_of_VFE downto 1) := (others => (others =>'0'));

  signal write_address         : unsigned(ADDR_LENGTH-1 downto 0) := MAX_RAM_ADDRESS;  -- write address
  signal read_address          : unsigned(ADDR_LENGTH-1 downto 0) := MAX_RAM_ADDRESS;  -- read address
  signal capture_write         : std_logic             := '0';
  signal frame_length          : unsigned(ADDR_LENGTH-1 downto 0) := (others => '0');

  signal capture_running       : std_logic := '0';  -- capturing data
  signal capture_start         : std_logic := '0';  -- set capturing data to '1'
  signal capture_pause         : std_logic := '0';  -- hold capturing data
  signal capture_stop          : std_logic := '0';  -- set capturing data to '0'
  
--Strobes from the IPBUS clock domain to capture domain
  signal capture_start_strobe  : std_logic := '0';
  signal capture_start_strobe1 : std_logic := '0';
  signal capture_start_strobe2 : std_logic := '0';
  signal capture_stop_strobe   : std_logic := '0';
  signal capture_stop_strobe1  : std_logic := '0';
  signal capture_stop_strobe2  : std_logic := '0';
  
-- Bit 13 is to tell that we have data from the compressed stream.
-- We could have from 1 to 5 samples in the same LHC clock.
-- This is required to compute a trigger from decoded data.
  type crc_data_t is array (integer range <>) of std_logic_vector(11 downto 0);
  signal crc_local             : crc_data_t(Nb_of_Lines downto 1)       := (others => (others => '0'));

  signal loc_crc_error         : UInt32_t(Nb_of_Lines downto 1)         := (others => (others => '0'));
  signal crc_valid             : std_logic_vector(Nb_of_Lines downto 1) := (others => '0');

  -- Max 5 samples per DTU word :
  type sample_t          is array (integer range 0 to 5) of std_logic_vector(13 downto 0);
  type channel_samples_t is array (integer range <>)     of sample_t;
  signal ch_deco               : channel_samples_t(Nb_of_Lines downto 1) := (others => (others => (others => '0')));
  
  type delta_data_t      is array (integer range <>)     of signed(13 downto 0);
  signal ch_delta_val          : delta_data_t(Nb_of_Lines downto 1)      := (others => (others => '0'));
  signal idle_pattern          : std_logic_vector(Nb_of_Lines downto 1)  := (others => '0');
  signal signal_sample         : std_logic_vector(Nb_of_Lines downto 1)  := (others => '0');
  signal sync_error            : std_logic_vector(Nb_of_Lines downto 1)  := (others => '0');
  signal signal_error          : std_logic_vector(Nb_of_Lines downto 1)  := (others => '0');
  signal DTU_frame_error       : std_logic_vector(Nb_of_Lines downto 1)  := (others => '0');
  signal DTU_frame_delimiter   : std_logic_vector(Nb_of_Lines downto 1)  := (others => '0');

  signal idle_counter          : UInt8_t(Nb_of_Lines downto 1)           := (others => (others => '0'));
  signal signal_counter        : UInt8_t(Nb_of_Lines downto 1)           := (others => (others => '0'));
  signal DTU_frame_length      : UInt8_t(Nb_of_Lines downto 1)           := (others => (others => '0'));
  signal loc_DTU_frame_length  : UInt8_t(Nb_of_Lines downto 1)           := (others => (others => '0'));

  --* State type of the writing process
  type   write_type is (write_start, write_idle, write_running);
  signal write_state : write_type := write_start;

  --* State type of the ipbus process
  type   ipbus_rw_type is (ipbus_rw_start, ipbus_rw_idle, ipbus_write_start, ipbus_read_start, ipbus_delay, ipbus_read, ipbus_ack, ipbus_finished);
  signal ipbus_rw_state : ipbus_rw_type := ipbus_rw_start;
  type cycle_state is (first, second, third, fourth, fifth, sixth);
  signal read_cycle : cycle_state := first;
  type busy_type is (not_busy, stay_busy, busy);
  signal busy_state : busy_type := not_busy;
  
  signal channel_ram_data_read : std_logic    := '0';
  signal ram_full              : std_logic    := '0';
  signal trig_sig_del1         : std_logic    := '0';
  signal trig_sig_del2         : std_logic    := '0';
  signal trig_short            : std_logic    := '0';
  signal nsample               : unsigned(ADDR_LENGTH-1 downto 0) := (others => '0');
  signal free_ram              : unsigned(ADDR_LENGTH-1 downto 0) := MAX_RAM_ADDRESS;  -- Available ram
  signal ram_read              : std_logic := '0';
  signal ram_read_del1         : std_logic := '0';
  signal ram_read_del2         : std_logic := '0';
  signal ram_read_short        : std_logic := '0';
  signal clka_ram              : std_logic := '0';
  signal clkb_ram              : std_logic := '0';
  signal clk_sequence          : std_logic := '0';

  signal time_stamp            : unsigned(63 downto 0);
  signal trig_width            : unsigned(3 downto 0);
  
  function CRC_calc(data : std_logic_vector; crc : std_logic_vector) return std_logic_vector is
    variable loc_crc : std_logic_vector(11 downto 0) := (others => '0');
  begin
   loc_crc(0) := data(30) xor data(29) xor data(26) xor data(25) xor data(24) xor data(23) xor data(22) xor data(17) xor data(16) xor data(15) xor data(14) xor data(13) xor data(12) xor data(11) xor data(8) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(2) xor data(1) xor data(0) xor crc(2) xor crc(3) xor crc(4) xor crc(5) xor crc(6) xor crc(9) xor crc(10) ;
   loc_crc(1) := data(31) xor data(29) xor data(27) xor data(22) xor data(18) xor data(11) xor data(9) xor data(0) xor crc(2) xor crc(7) xor crc(9) xor crc(11) ;
   loc_crc(2) := data(29) xor data(28) xor data(26) xor data(25) xor data(24) xor data(22) xor data(19) xor data(17) xor data(16) xor data(15) xor data(14) xor data(13) xor data(11) xor data(10) xor data(8) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(2) xor data(0) xor crc(2) xor crc(4) xor crc(5) xor crc(6) xor crc(8) xor crc(9) ;
   loc_crc(3) := data(27) xor data(24) xor data(22) xor data(20) xor data(18) xor data(13) xor data(9) xor data(2) xor data(0) xor crc(0) xor crc(2) xor crc(4) xor crc(7) ;
   loc_crc(4) := data(28) xor data(25) xor data(23) xor data(21) xor data(19) xor data(14) xor data(10) xor data(3) xor data(1) xor crc(1) xor crc(3) xor crc(5) xor crc(8) ;
   loc_crc(5) := data(29) xor data(26) xor data(24) xor data(22) xor data(20) xor data(15) xor data(11) xor data(4) xor data(2) xor crc(0) xor crc(2) xor crc(4) xor crc(6) xor crc(9) ;
   loc_crc(6) := data(30) xor data(27) xor data(25) xor data(23) xor data(21) xor data(16) xor data(12) xor data(5) xor data(3) xor crc(1) xor crc(3) xor crc(5) xor crc(7) xor crc(10) ;
   loc_crc(7) := data(31) xor data(28) xor data(26) xor data(24) xor data(22) xor data(17) xor data(13) xor data(6) xor data(4) xor crc(2) xor crc(4) xor crc(6) xor crc(8) xor crc(11) ;
   loc_crc(8) := data(29) xor data(27) xor data(25) xor data(23) xor data(18) xor data(14) xor data(7) xor data(5) xor crc(3) xor crc(5) xor crc(7) xor crc(9) ;
   loc_crc(9) := data(30) xor data(28) xor data(26) xor data(24) xor data(19) xor data(15) xor data(8) xor data(6) xor crc(4) xor crc(6) xor crc(8) xor crc(10) ;
   loc_crc(10) := data(31) xor data(29) xor data(27) xor data(25) xor data(20) xor data(16) xor data(9) xor data(7) xor crc(0) xor crc(5) xor crc(7) xor crc(9) xor crc(11) ;
   loc_crc(11) := data(29) xor data(28) xor data(25) xor data(24) xor data(23) xor data(22) xor data(21) xor data(16) xor data(15) xor data(14) xor data(13) xor data(12) xor data(11) xor data(10) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(2) xor data(1) xor data(0) xor crc(1) xor crc(2) xor crc(3) xor crc(4) xor crc(5) xor crc(8) xor crc(9);
   return loc_crc;
  end function;
begin

-- We receive 32 bit words synchronous with the LHC clock
  clk_sequence <= clk_40;
  CRC_error    <= loc_CRC_error;
  
  -------------------------------------------------------------------------------
  -- Capture of incomming channel data into rams for readout
  -------------------------------------------------------------------------------
  compute_crc : process (clk_sequence, CRC_reset) is
  begin
    if CRC_reset ='1' then
      CRC_local          <= (others => (others => '0'));
      loc_CRC_error      <= (others => (others => '0'));
      CRC_valid          <= (others => '0');
    elsif rising_edge(clk_sequence) then
      for i in 1 to Nb_of_Lines loop
        if ch_in(i)(31 downto 28) = "1101" then
          if crc_valid(i) = '0' then
            crc_valid(i)   <= '1';
          else
            if CRC_local(i) /= ch_in(i)(19 downto 8) then
              loc_CRC_error(i) <= loc_CRC_error(i) + 1;
            end if;
          end if;
          CRC_local(i)     <= (others => '0');
        elsif ch_in(i)(31 downto 30) /= "11" then
          crc_local(i) <= CRC_calc(ch_in(i), crc_local(i));
        end if;
      end loop;
    end if;
  end process compute_crc;

  decode_local_data : process (clk_sequence, reset) is
  begin
    if reset ='1' then
      for i in 1 to Nb_of_Lines loop
        ch_deco(i)(1)   <= (others => '0');
        ch_deco(i)(2)   <= (others => '0');
        ch_deco(i)(3)   <= (others => '0');
        ch_deco(i)(4)   <= (others => '0');
        ch_deco(i)(5)   <= (others => '0');
      end loop;
      DTU_frame_delimiter <= (others => '0');
      idle_pattern        <= (others => '0');
      signal_sample       <= (others => '0');
    elsif rising_edge(clk_sequence) then
      idle_pattern        <= (others => '0');
      signal_sample       <= (others => '0');
      if DTU_test_mode = '0' then
-- Channel decoding in test mode :
        for i in 1 to Nb_of_Lines loop
          ch_deco(i)(1)   <= (others => '0');
          ch_deco(i)(2)   <= (others => '0');
          ch_deco(i)(3)   <= (others => '0');
          ch_deco(i)(4)   <= (others => '0');
          ch_deco(i)(5)   <= (others => '0');
          case ch_in(i)(31 downto 30) is
          when "01" => -- Baseline mode 5 samples per word
            ch_deco(i)(1) <= "10000000"&ch_in(i)( 5 downto 0);
            ch_deco(i)(2) <= "10000000"&ch_in(i)(11 downto 6);
            ch_deco(i)(3) <= "10000000"&ch_in(i)(17 downto 12);
            ch_deco(i)(4) <= "10000000"&ch_in(i)(23 downto 18);
            ch_deco(i)(5) <= "10000000"&ch_in(i)(29 downto 24);
            loc_DTU_frame_length(i) <= loc_DTU_frame_length(i)+1;
          when "10" => -- Baseline mode, at most 4 samples per word
            if ch_in(i)(26 downto 24) = "001" then
              ch_deco(i)(1) <= "10000000"&ch_in(i)(5 downto 0);
            end if;
            if ch_in(i)(26 downto 24) = "010" then
              ch_deco(i)(1) <= "10000000"&ch_in(i)(5 downto 0);
              ch_deco(i)(2) <= "10000000"&ch_in(i)(11 downto 6);
            end if;
            if ch_in(i)(26 downto 24) = "011" then
              ch_deco(i)(1) <= "10000000"&ch_in(i)( 5 downto 0);
              ch_deco(i)(2) <= "10000000"&ch_in(i)(11 downto 6);
              ch_deco(i)(3) <= "10000000"&ch_in(i)(17 downto 12);
            end if;
            if ch_in(i)(26 downto 24) = "100" then
              ch_deco(i)(1) <= "10000000"&ch_in(i)( 5 downto 0);
              ch_deco(i)(2) <= "10000000"&ch_in(i)(11 downto 6);
              ch_deco(i)(3) <= "10000000"&ch_in(i)(17 downto 12);
              ch_deco(i)(4) <= "10000000"&ch_in(i)(23 downto 18);
            end if;
            loc_DTU_frame_length(i) <= loc_DTU_frame_length(i)+1;
          when "00" => -- signal mode : 1 or 2 damples per word
            if DTU_MEM_mode = '0' then
              if ch_in(i)(29 downto 26) = "1010" then
                ch_deco(i)(1) <= "1"&ch_in(i)(12 downto 0);
                ch_deco(i)(2) <= "1"&ch_in(i)(25 downto 13);
              elsif ch_in(i)(29 downto 26) = "1011" then
                ch_deco(i)(1) <= "1"&ch_in(i)(12 downto 0);
              end if;
              signal_sample(i) <= '1';
              loc_DTU_frame_length(i) <= loc_DTU_frame_length(i)+1;
            else -- Official 80 MHz mode
              if ch_in(i)(29 downto 28) = "01" then
                ch_deco(i)(1) <= "1"&ch_in(i)(12 downto  0); -- ADCH(i)
                ch_deco(i)(2) <= "1"&ch_in(i)(25 downto 13); -- ADCH(i+2)
              elsif ch_in(i)(29 downto 28) = "00" then
                ch_deco(i)(1) <= (others => '0'); -- BC0
                ch_deco(i)(2) <= "1"&ch_in(i)(25 downto 13); -- ADCH(i+2)
              elsif ch_in(i)(29 downto 28) = "11" then
                ch_deco(i)(1) <= "1"&ch_in(i)(12 downto  0); -- ADCH(i)
                ch_deco(i)(2) <= (others => '0'); -- BC0
              end if;
            end if;
          when "11" => -- synchro frame or frame delimiter
            if ch_in(i)(31 downto 16)=x"EAAA" then
              idle_pattern(i) <= '1';
            end if;
            if ch_in(i)(31 downto 28)="1101" then
              DTU_frame_length(i)     <= loc_DTU_frame_length(i);
              loc_DTU_frame_length(i) <= (others => '0');
            end if;
          when others => -- link not synchro
          end case;
        end loop;
      else -- We are in test mode : Only data even sample of G10 line
        for i in 1 to Nb_of_Lines loop
          ch_deco(i)(1) <= "10"&ch_in(i)(11 downto  0); -- ADCH(i)
          ch_deco(i)(2) <= "10"&ch_in(i)(27 downto 16); -- ADCH(i+2)
          ch_deco(i)(3) <= (others => '0');
          ch_deco(i)(4) <= (others => '0');
          ch_deco(i)(5) <= (others => '0');
        end loop;
      end if;
    end if;
  end process decode_local_data;

-- Control that we have idle patterns from times to times, which means that the links are still in sync
-- Count also the number of consecutive "signal" words, which could indicate a drift or wrong pedestals
  watch_dog : process (clk_sequence, reset) is
  variable clock_counter        : unsigned(7 downto 0) := (others => '0');
  variable loc_idle_counter     : UInt8_t(Nb_of_Lines downto 1)  := (others => (others => '0'));
  variable loc_signal_counter   : UInt8_t(Nb_of_Lines downto 1)  := (others => (others => '0'));

  begin
    if reset ='1' then
      clock_counter        := (others => '0');
      sync_error           <= (others => '0');
      signal_error         <= (others => '0');
      DTU_frame_error      <= (others => '0');
      loc_signal_counter   := (others => (others => '0'));
      loc_idle_counter     := (others => (others => '0'));
    elsif rising_edge(clk_sequence) then
      if clock_counter = 0 then
        loc_signal_counter := (others => (others => '0'));
        loc_idle_counter   := (others => (others => '0'));
      end if;
      if clock_counter < x"40" then
        clock_counter := clock_counter + 1;
        for i in 1 to Nb_of_Lines loop
          if signal_sample(i) = '1' then
            loc_signal_counter(i) := loc_signal_counter(i)+1;
          end if;
          if idle_pattern(i) = '1' then
            loc_idle_counter(i) := loc_idle_counter(i)+1;
          end if;
        end loop;
      else
        clock_counter     := (others => '0');
        for i in 1 to Nb_of_Lines loop
          idle_counter(i)   <= loc_idle_counter(i);
          signal_counter(i) <= loc_signal_counter(i);
        end loop;
      end if;

      sync_error          <= (others => '0');
      signal_error        <= (others => '0');
      DTU_frame_error     <= (others => '1');
      for i in 1 to Nb_of_Lines loop
        if idle_counter(i) < 8 then
          sync_error(i) <= '1';
        end if;
        if signal_counter(i) > 32 then
          signal_error(i) <= '1';
        end if;
        if DTU_frame_length(i) = x"32" then
          DTU_frame_error(i) <= '0';
        end if;
      end loop;
    end if;
  end process;
  
  gen_local_trigger : process (clk_sequence, trig_self, reset) is
  variable trig_cond : std_logic_vector(nb_of_lines downto 1) := (others => '0');
  begin
    if reset ='1' then
      trig_local <= '0';
      trig_width <= (others => '0');
      for i in 1 to Nb_of_Lines loop
        ch_deco(i)(0) <= (others => '0');
      end loop;
    elsif rising_edge(clk_sequence) then
      if trig_self = '1' and trig_width = 0 and write_state = write_idle and capture_running = '1' then -- Wait for the end of previous trigger has been processed
        for i in 1 to Nb_of_Lines loop
          if trig_self_mask(i)='1' then
            if trig_self_mode = '0' then
              if (ch_deco(i)(1)(13)='1' and (unsigned(ch_deco(i)(1)(12 downto 0)) > unsigned(trig_self_thres))) or
                 (ch_deco(i)(2)(13)='1' and (unsigned(ch_deco(i)(2)(12 downto 0)) > unsigned(trig_self_thres))) or
                 (ch_deco(i)(3)(13)='1' and (unsigned(ch_deco(i)(3)(12 downto 0)) > unsigned(trig_self_thres))) or
                 (ch_deco(i)(4)(13)='1' and (unsigned(ch_deco(i)(4)(12 downto 0)) > unsigned(trig_self_thres))) or
                 (ch_deco(i)(5)(13)='1' and (unsigned(ch_deco(i)(5)(12 downto 0)) > unsigned(trig_self_thres))) then
                trig_cond(i) := '1';
              else
                trig_cond(i) := '0';
              end if;
            else
              if ch_delta_val(i)>signed('0'&trig_self_thres) then
                trig_cond(i) := '1';
              else
                trig_cond(i) := '0';
              end if;
            end if;
          else
            trig_cond(i) := '0';
          end if;
        end loop;
        if (or trig_cond) = '1' then
          trig_width <= (others => '1');
          trig_local <= '1';
        else
          trig_local <= '0';
        end if;
      elsif trig_width > 0 then
        trig_width   <= trig_width-1;
      else
        trig_local   <= '0';
      end if;
      ch_delta_val       <= (others => (others => '0'));
      for i in 1 to Nb_of_Lines loop
        if ch_deco(i)(0)(13)='1' and ch_deco(i)(1)(13)='1' then
          ch_delta_val(i) <= signed('0'&ch_deco(i)(1)(12 downto 0))-signed('0'&ch_deco(i)(0)(12 downto 0));
        end if;
        if ch_deco(i)(0)(13)='1' and ch_deco(i)(2)(13)='1' and ch_deco(i)(2)>ch_deco(i)(1) then
          ch_delta_val(i) <= signed('0'&ch_deco(i)(2)(12 downto 0))-signed('0'&ch_deco(i)(0)(12 downto 0));
        end if;
        if ch_deco(i)(0)(13)='1' and ch_deco(i)(3)(13)='1' and ch_deco(i)(3)>ch_deco(i)(2) and ch_deco(i)(3)>ch_deco(i)(1) then
          ch_delta_val(i) <= signed('0'&ch_deco(i)(3)(12 downto 0))-signed('0'&ch_deco(i)(0)(12 downto 0));
        end if;
        if ch_deco(i)(0)(13)='1' and ch_deco(i)(4)(13)='1' and ch_deco(i)(4)>ch_deco(i)(3) and ch_deco(i)(4)>ch_deco(i)(2) and ch_deco(i)(4)>ch_deco(i)(1) then
          ch_delta_val(i) <= signed('0'&ch_deco(i)(4)(12 downto 0))-signed('0'&ch_deco(i)(0)(12 downto 0));
        end if;
        if ch_deco(i)(0)(13)='1' and ch_deco(i)(5)(13)='1' and ch_deco(i)(5)>ch_deco(i)(4) and ch_deco(i)(5)>ch_deco(i)(3) and ch_deco(i)(5)>ch_deco(i)(2) and ch_deco(i)(5)>ch_deco(i)(1) then
          ch_delta_val(i) <= signed('0'&ch_deco(i)(5)(12 downto 0))-signed('0'&ch_deco(i)(0)(12 downto 0));
        end if;

        if ch_deco(i)(5)(13)='1' then
          ch_deco(i)(0)      <= ch_deco(i)(5);
        elsif ch_deco(i)(4)(13)='1' then
          ch_deco(i)(0)      <= ch_deco(i)(4);
        elsif ch_deco(i)(3)(13)='1' then
          ch_deco(i)(0)      <= ch_deco(i)(3);
        elsif ch_deco(i)(2)(13)='1' then
          ch_deco(i)(0)      <= ch_deco(i)(2);
        elsif ch_deco(i)(1)(13)='1' then
          ch_deco(i)(0)      <= ch_deco(i)(1);
        end if;
      end loop;
    end if;
  end process gen_local_trigger;

-- Shift register to delay the input data waiting for trigger
-- Dual port channel rams for capture and readout
  clka_ram <= clk_sequence;
  clkb_ram <= not clk_ipbus;
  Shift_registers : for i in 1 to Nb_of_VFE generate
  begin
    vfe_data_in(i)(31  downto 0)   <= ch_in((i-1)*5+1);
    vfe_data_in(i)(63  downto 32)  <= ch_in((i-1)*5+2);
    vfe_data_in(i)(95  downto 64)  <= ch_in((i-1)*5+3);
    vfe_data_in(i)(127 downto 96)  <= ch_in((i-1)*5+4);
    vfe_data_in(i)(159 downto 128) <= ch_in((i-1)*5+5);
    inst_c_shift_ram_0 : c_shift_ram_0
    port map (
      d   => vfe_data_in(i),
      clk => clk_shift_reg,
      q   => vfe_data_delayed(i)
    );
    inst_blk_mem_gen_0 : blk_mem_gen_0
    port map (
      clka   => clka_ram,
      ena    => capture_running,
      wea(0) => capture_write,
      addra  => std_logic_vector(write_address),
      dina   => ram_data_in(i),
      clkb   => clka_ram,
  --    enb    => channel_ram_data_read,
      enb    => '1',
      addrb  => std_logic_vector(read_address),
      doutb  => ram_data_out(i)
    );
  end generate;

  -- Capture control (triggering, addressing)
  trig_sig_del1  <= trig_signal   when Rising_Edge(clk_sequence)  else trig_sig_del1;
  trig_sig_del2  <= trig_sig_del1 when Rising_Edge(clk_sequence)  else trig_sig_del2;
  trig_short     <= trig_sig_del1 and not trig_sig_del2;

  -- Synchronize ram_read signal with capture clock to increase available memory for capture
  ram_read_del1  <= ram_read      when Rising_Edge(clk_sequence)  else ram_read_del1;
  ram_read_del2  <= ram_read_del1 when Rising_Edge(clk_sequence)  else ram_read_del2;
  ram_read_short <= ram_read_del1 and not ram_read_del2;

  capture_data : process (clk_sequence, reset) is
  variable delay : unsigned(7 downto 0) := x"00";
  begin  -- process capture_counter
    if reset = '1' then
      DAQ_busy                <= '0';
--      capture_write           <= '0';
--      capture_running         <= '0';
--      write_address           <= MAX_RAM_ADDRESS;
      free_ram                <= MAX_RAM_ADDRESS;
      ram_full                <= '0';
      delay                   := x"00";
      write_state             <= write_start;
    elsif rising_edge(clk_sequence) then
      capture_write           <= '0';
      case write_state is
      when write_start =>
        DAQ_busy              <= '0';
        nsample               <= (others => '0');
        write_address         <= MAX_RAM_ADDRESS;
        capture_write         <= '0';
        capture_running       <= '0';
        if delay /= x"FF" then
          delay               := delay+1;
        else
          write_state         <= write_idle;
        end if;
      when write_idle =>
        DAQ_busy              <= '0';
        if capture_start = '1' then   --start control
          capture_running     <= '1';
          write_address       <= MAX_RAM_ADDRESS;
          free_ram            <= MAX_RAM_ADDRESS;
          ram_full            <= '0';
        end if;
        if capture_stop = '1' then
          capture_running     <= '0';
        end if;
        if free_ram < frame_length  then
          ram_full            <= '1';
        else
          ram_full            <= '0';
        end if;
        if capture_running = '1' and trig_short = '1' then
          if FIFO_mode = '0' then   --Reset write address if single event mode
            write_address     <= MAX_RAM_ADDRESS;
            free_ram          <= MAX_RAM_ADDRESS;
            ram_full          <= '0';
            nsample           <= (others=> '0');
            write_state       <= write_running;
          end if;
          if ram_full = '0' then
            nsample           <= (others => '0');
            write_state       <= write_running;
          end if;
        end if;
        if ram_read_short = '1' and free_ram /= MAX_RAM_ADDRESS then
          free_ram            <= free_ram + 1;
        end if;
      when write_running =>
        DAQ_busy              <= '1';
        capture_write <= '1';
        if nsample = 0 then
          for i in 1 to Nb_of_VFE loop
            ram_data_in(i)(159 downto 64) <= (others => '1');
            ram_data_in(i)(63 downto 0)   <= std_logic_vector(time_stamp);
          end loop;
        else
          for i in 1 to Nb_of_VFE loop
            ram_data_in(i)                <= vfe_data_delayed(i);
          end loop;
        end if;
        if nsample = (frame_length-1) then
          write_state         <= write_idle;
        end if;
        nsample               <= nsample + 1;
        if write_address = MAX_RAM_ADDRESS then
          write_address       <= (others => '0');
        else
          write_address       <= write_address+1;
        end if;
        if ram_read_short = '0' then
          free_ram            <= free_ram - 1;
        end if;

        if capture_stop = '1' then
          capture_running     <= '0';
          write_state <= write_idle;
        end if;
      end case;
    end if;
  end process capture_data;

-- Running clk counter to get a timestamp
  clk_counter : process(reset, capture_start, clk_sequence)
  begin
    if reset = '1' or capture_start = '1' then
      time_stamp <= (others => '0');
    elsif rising_edge(clk_sequence) then
      time_stamp <= time_stamp+1;
    end if;
  end process clk_counter;
  
-- Process ipbus read/writes
  ipbus_data : process(reset, clk_ipbus)
  variable iVFE : natural range 1 to Nb_of_VFE := 1;
  begin
    if reset = '1' then
--      read_address          <= MAX_RAM_ADDRESS;
--      ack                   <= '0';
      read_cycle            <= first;
      ipbus_rw_state        <= ipbus_rw_start;
      channel_ram_data_read <= '0';
      ram_read              <= '0';
      frame_length          <= (others => '0');
    elsif rising_edge(clk_ipbus) then
      capture_start_strobe  <= '0';
      capture_stop_strobe   <= '0';
      channel_ram_data_read <= '0';
      ram_read              <= '0';
      ipbus_state_machine : case ipbus_rw_state is
      when ipbus_rw_start =>
        read_address        <= MAX_RAM_ADDRESS;
        read_cycle          <= first;
        ack                 <= '0';
        ipbus_rw_state <= ipbus_rw_idle;
      when ipbus_rw_idle =>
        if ipbus_in.ipb_strobe = '1' then
          if ipbus_in.ipb_write = '1' then
            ipbus_rw_state  <= ipbus_write_start;
          else
            ipbus_rw_state  <= ipbus_read_start;
          end if;
        end if;
      when ipbus_write_start =>
        case ipbus_in.ipb_addr(15 downto 0) is
          when x"0000" => --control registers
            capture_start_strobe <= ipbus_in.ipb_wdata(0);
            capture_stop_strobe  <= ipbus_in.ipb_wdata(1);
            frame_length         <= unsigned(ipbus_in.ipb_wdata(ADDR_LENGTH+15 downto 16));
          when x"0001" => --Start read address
            read_address <= unsigned(ipbus_in.ipb_wdata(ADDR_LENGTH-1 downto 0));
            read_cycle   <= first;
          when others => null;
        end case;
        ipbus_rw_state <= ipbus_ack;
      when ipbus_read_start =>
        ipbus_out.ipb_rdata <= (others => '0'); 
        decode : case ipbus_in.ipb_addr(15) is
        when '0' =>
          case ipbus_in.ipb_addr(1 downto 0) is
          when "00" =>       --CAP_CTRL
            ipbus_out.ipb_rdata(15+ADDR_LENGTH downto 16) <= std_logic_vector(frame_length);
            ipbus_out.ipb_rdata(1 downto 0)               <= capture_stop_strobe & capture_start_strobe;
          when "01" =>       -- CAP_ADDRESS
            ipbus_out.ipb_rdata(15+ADDR_LENGTH downto 16) <= std_logic_vector(write_address);
            ipbus_out.ipb_rdata(ADDR_LENGTH-1 downto 0)   <= std_logic_vector(read_address);
          when "10" =>       -- CAP_FREE
            ipbus_out.ipb_rdata(ADDR_LENGTH-1 downto 0)   <= std_logic_vector(free_ram);
          when others => null;
          end case;
          ipbus_rw_state <= ipbus_ack;
        when '1' => -- Captured data
          if (read_address = write_address and read_cycle = first) or
             (free_ram = MAX_RAM_ADDRESS) then
            ipbus_out.ipb_rdata <= x"DEADBEEF";
            ipbus_rw_state      <= ipbus_ack;
          else
            if read_cycle = first then
              if read_address = MAX_RAM_ADDRESS then
                read_address <= (others => '0');
              else
                read_address <= read_address + 1;
              end if;
              iVFE                := 1;
            end if;
            ipbus_rw_state      <= ipbus_read;
          end if;          
        when others =>
          ipbus_out.ipb_rdata <= x"DEADBEEF";
          ipbus_rw_state <= ipbus_ack;
        end case decode;
      when ipbus_read =>
        dispatch : case read_cycle is
        when first =>
          ipbus_out.ipb_rdata(31 downto 16)           <= "000"&DTU_frame_error(5 downto 1)&"000"&signal_error(5 downto 1);
          ipbus_out.ipb_rdata(ADDR_LENGTH-1 downto 0) <= std_logic_vector(read_address);
          read_cycle <= second;
        when second =>
          ipbus_out.ipb_rdata  <= ram_data_out(iVFE)(31 downto 0);
          read_cycle <= third;
        when third =>
          ipbus_out.ipb_rdata  <= ram_data_out(iVFE)(63 downto 32);
          read_cycle <= fourth;
        when fourth =>
          ipbus_out.ipb_rdata  <= ram_data_out(iVFE)(95 downto 64);
          read_cycle <= fifth;
        when fifth =>
          ipbus_out.ipb_rdata  <= ram_data_out(iVFE)(127 downto 96);
          read_cycle <= sixth;
        when sixth =>
          ipbus_out.ipb_rdata  <= ram_data_out(iVFE)(159 downto 128);
          if iVFE < 5 then
            iVFE         := iVFE+1;
            read_cycle   <= second;
          else
            ram_read     <= '1';
            read_cycle   <= first;
          end if;
        when others =>
          read_cycle     <= first;
        end case dispatch;
        ipbus_rw_state   <= ipbus_ack;
      when ipbus_ack =>
        if capture_start_strobe = '1' then -- Reset read address when we restart capture
          read_address   <= MAX_RAM_ADDRESS;
          read_cycle     <= first;
        end if;
        ack              <= '1';
        ipbus_rw_state   <= ipbus_finished;
      when ipbus_finished =>
        ack              <= '0';
        if FIFO_mode = '0' then
          read_address   <= MAX_RAM_ADDRESS;
          read_cycle     <= first;
        end if;
        ipbus_rw_state   <= ipbus_rw_idle;
      when others =>
        ack              <= '0';
        ipbus_rw_state   <= ipbus_rw_idle;
      end case ipbus_state_machine;
    end if;
  end process ipbus_data;

  capture_start_strobe1 <= capture_start_strobe  when Rising_Edge(clk_sequence)  else capture_start_strobe1;
  capture_start_strobe2 <= capture_start_strobe1 when Rising_Edge(clk_sequence)  else capture_start_strobe2;
  capture_start         <= capture_start_strobe1 and not capture_start_strobe2;

  capture_stop_strobe1 <= capture_stop_strobe  when Rising_Edge(clk_sequence)  else capture_stop_strobe1;
  capture_stop_strobe2 <= capture_stop_strobe1 when Rising_Edge(clk_sequence)  else capture_stop_strobe2;
  capture_stop         <= capture_stop_strobe1 and not capture_stop_strobe2;

--  pacd_1 : pacd
--    port map (
--      iPulseA => capture_start_strobe,
--      iClkA   => clk_ipbus,
--      iRSTAn  => '1',
--      iClkB   => clk_cap,
--      iRSTBn  => '1',
--      oPulseB => capture_start
--    );
--  pacd_2 : pacd
--    port map (
--      iPulseA => capture_stop_strobe,
--      iClkA   => clk_ipbus,
--      iRSTAn  => '1',
--      iClkB   => clk_cap,
--      iRSTBn  => '1',
--      oPulseB => capture_stop
--    );

  ipbus_out.ipb_ack <= ack;
  ipbus_out.ipb_err <= '0';

--  ila_0_2: ila_0
--    port map (
--      clk    => clk_cap,
--      probe0(0) => capture_running,
--      probe1(0) => capture_start,
--      probe2(0) => capture_stop,
--      probe3 => std_logic_vector(capture_address(7 downto 0)));
--  ila_0_2 : ila_0
--    port map (
--      clk       => clk_ipbus,
--      probe0(0) => ipbus_in.ipb_strobe,
--      probe1(0) => channel_ram_data_out(channel_index)(0),
--      probe2(0) => channel_ram_data_out(channel_index)(1),
--      probe3    => std_logic_vector(ipbus_sample_address(7 downto 0)));

end rtl;
