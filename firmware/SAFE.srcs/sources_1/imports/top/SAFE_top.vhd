-- Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 1412921 Wed Nov 18 09:44:32 MST 2015
-- Date        : Mon Apr 25 14:31:29 2016
-- Host        : volta running 64-bit Debian GNU/Linux testing/unstable
-- Command     : write_vhdl -mode pin_planning -force -port_diff_buffers
--               /home/dan/work/CMS/cms-ecal-vfe-adapter/SAFE/testing/option2_484/io_1.vhd
-- Design      : ios
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
---- The following library declaration should be present if 
---- instantiating any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.SAFE_IO.all;

entity SAFE is
  generic (
    -- Temporary trick to generate weird clock from FPGA (need to change also xdc file to get LVDS_25)
    USE_GPIO_WITH_DBG  : boolean  := not USE_LVRB;
    USE_GPIO_WITH_LVR  : boolean  := USE_LVRB
  );

  port (
  ch_in_P           : in    std_logic_vector(25 downto 1);
  ch_in_N           : in    std_logic_vector(25 downto 1);
  PllLock_in        : in    std_logic_vector(25 downto 1);
  Pwup_reset_out    : out   std_logic_vector(5 downto 1);
  ReSync_out_P      : out   std_logic_vector(5 downto 1);
  ReSync_out_N      : out   std_logic_vector(5 downto 1);
  APD_temp_in       : in    std_logic_vector(5 downto 1);
  APD_temp_ref      : in    std_logic_vector(5 downto 1);
  CATIA_temp_in     : in    std_logic_vector(5 downto 1);
  CATIA_temp_ref    : in    std_logic_vector(5 downto 1);
  Test_mode_out     : out   std_logic_vector(5 downto 1);

  I2C_sda_VFE       : inout std_logic_vector(5 downto 1);
  I2C_scl_VFE       : inout std_logic_vector(5 downto 1);

  CLK_160_P         : in    std_logic;
  CLK_160_N         : in    std_logic;

  GbE_refclk_N      : in    std_logic;
  GbE_refclk_P      : in    std_logic;
  GbE_RXN           : in    std_logic;
  GbE_RXP           : in    std_logic;
  GbE_TXN           : out   std_logic;
  GbE_TXP           : out   std_logic;

  SFP_Present       : in    std_logic;
  SFP_LOS           : in    std_logic;
  SFP_Tx_Fault      : in    std_logic;
  SFP_SCL           : out   std_logic;
  SFP_SDA           : inout std_logic;

  PG1_in            : in    std_logic;
  PG3_in            : in    std_logic;
  PG4_in            : in    std_logic;
  PG6_in            : in    std_logic;
  EN1_out           : out   std_logic;
  EN3_out           : out   std_logic;
  EN4_out           : out   std_logic;
  EN6_out           : out   std_logic;

  LED               : out   std_logic_vector(3 downto 0);
  GPIO              : out   std_logic_vector(3 downto 0);
  ADDR              : in    std_logic_vector(3 downto 0)   -- Pins opened on FPGA. Pulled down = "0000"
  );
end SAFE;

architecture rtl of SAFE is

component clk_generator_iserdes is
  port (
    clk_160_in      : in  std_logic;
    clk_20          : out std_logic;
    clk_40          : out std_logic;
    clk_160         : out std_logic;
    clk_640         : out std_logic;
    reset           : in  std_logic;
    locked          : out std_logic);
end component clk_generator_iserdes;

component clk_generator_delay_ctrl is
  port (
    clk_160_in      : in  std_logic;
    clk_delay_ctrl  : out std_logic;
    reset           : in  std_logic;
    locked          : out std_logic);
end component clk_generator_delay_ctrl;

component gig_ethernet_pcs_pma_0 is
  port (
    gtrefclk_p             : in  std_logic;
    gtrefclk_n             : in  std_logic;
    gtrefclk_out           : out std_logic;
    gtrefclk_bufg_out      : out std_logic;
    txp                    : out std_logic;
    txn                    : out std_logic;
    rxp                    : in  std_logic;
    rxn                    : in  std_logic;
    resetdone              : out std_logic;
    userclk_out            : out std_logic;
    userclk2_out           : out std_logic;
    rxuserclk_out          : out std_logic;
    rxuserclk2_out         : out std_logic;
    pma_reset_out          : out std_logic;
    mmcm_locked_out        : out std_logic;
    independent_clock_bufg : in  std_logic;
    gmii_txd               : in  std_logic_vector(7 downto 0);
    gmii_tx_en             : in  std_logic;
    gmii_tx_er             : in  std_logic;
    gmii_rxd               : out std_logic_vector(7 downto 0);
    gmii_rx_dv             : out std_logic;
    gmii_rx_er             : out std_logic;
    gmii_isolate           : out std_logic;
    configuration_vector   : in  std_logic_vector(4 downto 0);
    status_vector          : out std_logic_vector(15 downto 0);
    reset                  : in  std_logic;
    signal_detect          : in  std_logic;
    gt0_qplloutclk_out     : out std_logic;
    gt0_qplloutrefclk_out  : out std_logic
);
end component gig_ethernet_pcs_pma_0;

component tri_mode_ethernet_mac_0 is
  port (
    gtx_clk                 : in  std_logic;
      -- asynchronous reset
    glbl_rstn               : in  std_logic;
    rx_axi_rstn             : in  std_logic;
    tx_axi_rstn             : in  std_logic;
    rx_statistics_vector    : out std_logic_vector(27 downto 0);
    rx_statistics_valid     : out std_logic;
    rx_mac_aclk             : out std_logic;
    rx_reset                : out std_logic;
    rx_axis_mac_tdata       : out std_logic_vector(7 downto 0);
    rx_axis_mac_tvalid      : out std_logic;
    rx_axis_mac_tlast       : out std_logic;
    rx_axis_mac_tuser       : out std_logic;
    tx_ifg_delay            : in  std_logic_vector(7 downto 0);

    tx_statistics_vector    : out std_logic_vector(31 downto 0);
    tx_statistics_valid     : out std_logic;
    tx_mac_aclk             : out std_logic;
    tx_reset                : out std_logic;
    tx_axis_mac_tdata       : in  std_logic_vector(7 downto 0);
    tx_axis_mac_tvalid      : in  std_logic;
    tx_axis_mac_tlast       : in  std_logic;
    tx_axis_mac_tuser       : in  std_logic_vector(0 downto 0);
    tx_axis_mac_tready      : out std_logic;
    pause_req               : in  std_logic;
    pause_val               : in  std_logic_vector(15 downto 0);
    speedis100              : out std_logic;
    speedis10100            : out std_logic;
--    rx_enable                  => rx_enable,
--    tx_enable                  => tx_enable,
--    tx_ifg_delay               => tx_ifg_delay,
--    gmii_tx_clk                => gmii_tx_clk,
--    gmii_rx_clk                => gmii_rx_clk,
--    mii_tx_clk                 => mii_tx_clk,

    gmii_txd                : out std_logic_vector(7 downto 0);
    gmii_tx_en              : out std_logic;
    gmii_tx_er              : out std_logic;
    gmii_rxd                : in  std_logic_vector(7 downto 0);
    gmii_rx_dv              : in  std_logic;
    gmii_rx_er              : in  std_logic;
    rx_configuration_vector : in  std_logic_vector(79 downto 0);
    tx_configuration_vector : in  std_logic_vector(79 downto 0));
end component tri_mode_ethernet_mac_0;

component ipbus_ctrl is
  generic(
    MAC_CFG       : ipb_mac_cfg                   := EXTERNAL;
    IP_CFG        : ipb_ip_cfg                    := EXTERNAL;
    BUFWIDTH      : natural                       := 4;
    INTERNALWIDTH : natural                       := 1;
    ADDRWIDTH     : natural                       := 11;
    IPBUSPORT     : std_logic_vector(15 downto 0) := x"C351";
    SECONDARYPORT : std_logic                     := '0';
    N_OOB         : natural                       := 0
  );
  port (
    mac_clk      : in  std_logic;
    rst_macclk   : in  std_logic;
    ipb_clk      : in  std_logic;
    rst_ipb      : in  std_logic;
    mac_rx_data  : in  std_logic_vector(7 downto 0);
    mac_rx_valid : in  std_logic;
    mac_rx_last  : in  std_logic;
    mac_rx_error : in  std_logic;
    mac_tx_data  : out std_logic_vector(7 downto 0);
    mac_tx_valid : out std_logic;
    mac_tx_last  : out std_logic;
    mac_tx_error : out std_logic;
    mac_tx_ready : in  std_logic;
    ipb_out      : out ipb_wbus;
    ipb_in       : in  ipb_rbus;
    ipb_req      : out std_logic;
    ipb_grant    : in  std_logic                                := '1';
    mac_addr     : in  std_logic_vector(47 downto 0)            := X"000000000000";
    ip_addr      : in  std_logic_vector(31 downto 0)            := X"00000000";
    enable       : in  std_logic                                := '1';
    RARP_select  : in  std_logic                                := '0';
    pkt          : out std_logic;
    pkt_oob      : out std_logic;
    oob_in       : in  ipbus_trans_in_array(N_OOB - 1 downto 0) := (others => ('0', X"00000000", '0'));
    oob_out      : out ipbus_trans_out_array(N_OOB - 1 downto 0)
	);
end component ipbus_ctrl;

signal gmii_txd            : std_logic_vector(7 downto 0);
signal gmii_tx_en          : std_logic;
signal gmii_tx_er          : std_logic;
signal gmii_rxd            : std_logic_vector(7 downto 0);
signal gmii_rx_dv          : std_logic;
signal gmii_rx_er          : std_logic;
signal GbE_user_clk        : std_logic;
signal fake_packet_counter : integer range 0 to 1536 := 0;

signal mac_tx_data, mac_rx_data                                                         : std_logic_vector(7 downto 0);
signal mac_tx_valid, mac_tx_last, mac_tx_ready, mac_rx_valid, mac_rx_last, mac_rx_error : std_logic;
signal mac_tx_error                                                                     : std_logic_vector(0 downto 0);
signal ipb_master_out                                                                   : ipb_wbus;
signal ipb_master_in                                                                    : ipb_rbus;
signal mac_addr                                                                         : std_logic_vector(47 downto 0);
signal ip_addr                                                                          : std_logic_vector(31 downto 0);
signal pkt, pkt_oob                                                                     : std_logic;
signal frame_size              : std_logic_vector(15 downto 0);
signal rx_configuration_vector : std_logic_vector(79 downto 0);
signal tx_configuration_vector : std_logic_vector(79 downto 0);

--Array mappings to simplify instances
signal captured_stream : word_t(Nb_of_Lines downto 1);

signal loc_clk_in          : std_logic := '0';
signal Clk_ipbus           : std_logic := '0';
signal Clk_Gb_eth          : std_logic := '0';
signal Clk_I2C             : std_logic := '0';
signal System_clk          : std_logic := '0';
signal Sequence_clk        : std_logic := '0';
signal shift_reg_clk       : std_logic := '0';
signal memory_clk          : std_logic := '0';
signal Resync_clk          : std_logic := '0';
signal Clk_delay_ctrl      : std_logic := '0';
signal Clk_delay_ctrl_loc  : std_logic := '0';
signal iserdes_clock_locked: std_logic := '0';
signal delay_clock_locked  : std_logic := '0';
signal MMCM_locked         : std_logic := '0';
signal delay_locked_debug  : std_logic := '0';

signal HP_Clk_in           : std_logic := '0';
signal HP_Clk_160          : std_logic := '0';
signal HP_Clk_20           : std_logic := '0';
signal HP_Clk_40           : std_logic := '0';
signal HP_Clk_640          : std_logic := '0';

signal I2C_data_VFE        : std_logic_vector(15 downto 0) := (others => '0');
signal dbg_i2c_clk         : std_logic;
signal dbg_sda             : std_logic;
signal dbg_scl             : std_logic;

--Monitoring
signal SAFE_Monitor           : SAFE_Monitor_t;
signal SAFE_Control           : SAFE_Control_t  := DEFAULT_SAFE_Control;
signal VFE_Monitor            : VFE_Monitor_t;
signal VFE_Control            : VFE_Control_t   := DEFAULT_VFE_Control;
signal led_counter            : unsigned(31 downto 0) := (others => '0');
signal pwup_reset             : std_logic := '1';
signal VFE_reset              : std_logic := '0'; -- Pwup_resetb sent to VFE
signal SAFE_reset             : std_logic := '0';
signal CLK_reset              : std_logic := '0';
signal trig_signal            : std_logic := '0'; -- Single event trigger
signal trig_software          : std_logic := '0'; -- single event trigger received on ipbus
signal trig_sw_width          : unsigned(3 downto 0) := (others => '0');
signal trig_out               : std_logic := '0'; -- output signal generated from trig_software after some delay
signal trig_in                : std_logic := '0'; -- single event  trigger receives on GPIO pin
signal trig_hardware          : std_logic := '0'; -- input signal generated from trig_hardware after some delay
signal trig_hw_width          : unsigned(3 downto 0) := (others => '0');
signal trig_local             : std_logic := '0'; -- trigger signal generated looking at input data (required trig_self=1)
signal trig_local_outb        : std_logic := 'Z'; -- trigger signal generated looking at input data (required trig_self=1) sent to FE
signal trig_in_prev1          : std_logic := '0';
signal trig_in_prev2          : std_logic := '0';
signal trigger_prev1          : std_logic := '0';
signal trigger_prev2          : std_logic := '0';
signal delay_out_counter      : unsigned(15 downto 0) := x"0000";
signal start_out_counter      : std_logic := '0';
signal delay_in_counter       : unsigned(15 downto 0) := x"0000";
signal start_in_counter       : std_logic := '0';
signal WTE                    : std_logic := '0';
signal WWTE                   : std_logic := '0';
signal DAQ_busy               : std_logic := '0';
signal BC0                    : std_logic := '0';
signal Test_enable            : std_logic := '0';
signal ipbus_counter          : std_logic_vector(2 downto 0) := "000";
signal clk_counter            : unsigned(13 downto 0) := (others => '0');
signal orbit_counter          : unsigned(31 downto 0) := (others => '0');
signal orbit_counter_WTE      : unsigned(7 downto 0)  := (others => '0');
signal AWG_trigger_out        : std_logic :='0';
signal AWG_trigger_del        : std_logic :='0';
signal AWG_trigger_short      : std_logic :='0';
signal AWG_trigger_width      : unsigned(3 downto 0) := (others => '0');

signal DTU_start_Resync       : std_logic := '0';
signal DTU_start_Resync_del   : std_logic := '0';
signal WDTU_start_Resync      : std_logic := '0';
signal WTP_trigger            : std_logic := '0';
signal TP_trigger             : std_logic := '0';
signal TP_trigger_merged      : std_logic := '0';
signal TP_trigger_prev        : std_logic := '0';
signal start_TP_delay         : std_logic := '0';
signal TP_duration_counter    : unsigned(15 downto 0) := x"0000";
signal TP_delay_counter       : unsigned(15 downto 0) := x"0000";
signal TP_DAQ_start           : std_logic := '0';
signal TP_DAQ_start_width     : unsigned(1 downto 0) := "00";

signal do_IDELAY_sync         : std_logic := '0';
signal idelay_sync_counter    : unsigned(15 downto 0) := (others => '0');
signal sig_monitor            : std_logic_vector(3 downto 0) := "0000";

signal ReSync_out             : std_logic_vector(Nb_of_VFE downto 1) := (others => '0');
signal ReSync_out_R           : std_logic_vector(Nb_of_VFE downto 1) := (others => '0');
signal ReSync_out_F           : std_logic_vector(Nb_of_VFE downto 1) := (others => '0');
signal ReSync_out_loc         : std_logic_vector(Nb_of_VFE downto 1) := (others => '0');

signal probe                  : Byte_t(7 downto 0);

attribute mark_debug : string;
attribute keep : string;
attribute mark_debug of MMCM_locked  : signal is "true";

begin

  GPIO(0) <= BC0;
  GPIO(1) <= 'Z';
  GPIO(2) <= 'Z';
  GPIO(3) <= AWG_trigger_out;

  Test_mode_out(1)                 <= VFE_Monitor.DTU_test_mode;
  Test_mode_out(2)                 <= VFE_Monitor.DTU_test_mode;
  Test_mode_out(3)                 <= VFE_Monitor.DTU_test_mode;
  Test_mode_out(4)                 <= VFE_Monitor.DTU_test_mode;
  Test_mode_out(5)                 <= VFE_Monitor.DTU_test_mode;

  VFE_monitor.VFE_LV(1)            <= PG1_in;
  VFE_monitor.VFE_LV(2)            <= '0';
  VFE_monitor.VFE_LV(3)            <= PG3_in;
  VFE_monitor.VFE_LV(4)            <= PG4_in;
  VFE_monitor.VFE_LV(5)            <= '0';
  VFE_monitor.VFE_LV(6)            <= PG6_in;
  EN1_out                          <= VFE_control.VFE_LV(1);
  EN3_out                          <= VFE_control.VFE_LV(3);
  EN4_out                          <= VFE_control.VFE_LV(4);
  EN6_out                          <= VFE_control.VFE_LV(6);

--  reset                            <= pwup_reset or SAFE_Monitor.reset;
  VFE_reset                        <= Pwup_reset or VFE_monitor.VFE_reset;
  Pwup_reset_out(1)                <= not VFE_reset;
  Pwup_reset_out(2)                <= not VFE_reset;
  Pwup_reset_out(3)                <= not VFE_reset;
  Pwup_reset_out(4)                <= not VFE_reset;
  Pwup_reset_out(5)                <= not VFE_reset;
  CLK_reset                        <= '0';
  SAFE_reset                       <= Pwup_reset or SAFE_Monitor.reset or VFE_monitor.I2C_reset;
  
  gen_pwup_reset : process (system_clk)
  variable counter : unsigned(15 downto 0) := (others => '0');
  begin  -- process test_counter
--    if system_clock_locked = '0' then
--      counter    := (others => '0');
    if rising_edge(system_clk) then
      pwup_reset <= '1';
      if counter =  x"FFFF" then
        pwup_reset <= '0';
      else
        counter := counter + 1;
      end if;
    end if;
  end process gen_pwup_reset;

--  DCIRESET_inst : DCIRESET
--  port map (
--    LOCKED => SAFE_monitor.DCI_locked, -- 1-bit output: LOCK status output
--    RST    => pwup_reset               -- 1-bit input: Active-high asynchronous reset input
--  );
  SAFE_monitor.DCI_locked          <= '1';

-- SAFE monitoring :
  SAFE_Monitor.firmware_ver        <= x"24071201";
  SAFE_Monitor.board_SN            <= ADDR;
  SAFE_Monitor.resync_clock_phase  <= SAFE_Control.resync_clock_phase;
  SAFE_Monitor.clock_reset         <= SAFE_Control.clock_reset;
  SAFE_Monitor.reset               <= SAFE_Control.reset;
  SAFE_Monitor.LED_on              <= SAFE_Control.LED_on;
  SAFE_Monitor.FIFO_mode           <= SAFE_Control.FIFO_mode;
  SAFE_Monitor.trig_loop           <= SAFE_Control.trig_loop;
  SAFE_Monitor.trig_self           <= SAFE_Control.trig_self;
  SAFE_Monitor.trig_self_mode      <= SAFE_Control.trig_self_mode;
  SAFE_Monitor.trig_self_thres     <= SAFE_Control.trig_self_thres;
  SAFE_Monitor.trig_self_mask      <= SAFE_Control.trig_self_mask;
  SAFE_Monitor.trigger_SW_delay    <= SAFE_Control.trigger_SW_delay;
  SAFE_Monitor.trigger_HW_delay    <= SAFE_Control.trigger_HW_delay;
  SAFE_Monitor.TP_duration         <= SAFE_Control.TP_duration;
  SAFE_Monitor.TP_delay            <= SAFE_Control.TP_delay;
  SAFE_Monitor.TP_dummyb           <= SAFE_Control.TP_dummyb;
  SAFE_Monitor.start_IDELAY_sync   <= SAFE_Control.start_IDELAY_sync;
  SAFE_Monitor.WTE_command_pos     <= SAFE_Control.WTE_command_pos;
  SAFE_Monitor.ReSync_command_pos  <= SAFE_Control.ReSync_command_pos;
  SAFE_Monitor.clock_locked        <= MMCM_locked;
  --SAFE_Monitor.Debug1              <= (others => (others => '0'));

-- VFE monitoring
  VFE_monitor.I2C_access_VFE       <= VFE_control.I2C_access_VFE;
  VFE_monitor.I2C_lpGBT_mode       <= VFE_control.I2C_lpGBT_mode;
  VFE_monitor.I2C_R_Wb             <= VFE_control.I2C_R_Wb;
  VFE_monitor.I2C_Reset            <= VFE_control.I2C_Reset;
  VFE_monitor.I2C_Reg_number       <= VFE_Control.I2C_Reg_number;
  VFE_monitor.I2C_Device_number    <= VFE_Control.I2C_Device_number;
  VFE_monitor.I2C_VFE_pattern      <= VFE_Control.I2C_VFE_pattern;
  VFE_Monitor.I2C_long_transfer    <= VFE_control.I2C_long_transfer;
  VFE_Monitor.DTU_Resync_idle      <= VFE_control.DTU_Resync_idle;
  VFE_Monitor.DTU_Resync_data      <= VFE_control.DTU_Resync_data;
  VFE_Monitor.DTU_Sync_pattern     <= VFE_control.DTU_Sync_pattern;
  VFE_Monitor.DTU_test_mode        <= VFE_control.DTU_test_mode;
  VFE_Monitor.DTU_MEM_mode         <= VFE_control.DTU_MEM_mode;

  VFE_monitor.eLink_active         <= VFE_control.eLink_active;
  VFE_monitor.VFE_reset            <= VFE_control.VFE_reset;
  --VFE_monitor.I2C_n_ack_LVRB       <= x"FF";
  VFE_Monitor.DTU_PLL_lock         <= PllLock_in;

  ReSync_obuf : for i in 1 to Nb_of_VFE generate
  begin
    ReSync_out_obuf : OBUFDS port map (O => ReSync_out_P(i), OB => ReSync_out_N(i), I => ReSync_out(i));
  end generate;
    
  sig_monitor(0)       <= sequence_clk;
  sig_monitor(2)       <= SAFE_control.tapslip_DTU_map(1);
  sig_monitor(3)       <= SAFE_control.IO_reset;
--  trig_signal          <= calib_DAQ_start or (trig_software and SAFE_Monitor.trig_loop) or (trig_hardware and not SAFE_Monitor.trig_loop);
  trig_signal          <= TP_DAQ_start or trig_software or trig_hardware;
  trig_in              <= trig_local and SAFE_monitor.trig_loop;

  TP_trigger_merged    <= SAFE_Control.TP_trigger;
  
  hold_ReSync_commands : process(MMCM_locked, pwup_reset, sequence_clk, TP_trigger_merged)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      DTU_start_Resync      <= '0';
      WDTU_start_ReSync     <= '0';
      WTP_trigger           <= '0';
      TP_trigger            <= '0';
      start_TP_delay        <= '0';
      TP_DAQ_start          <= '0';
      TP_duration_counter  <= (others => '0');
      TP_delay_counter     <= (others => '0');
      TP_DAQ_start_width    <= "00";
    elsif rising_edge(sequence_clk) then
-- Hold ReSync commands not to interfere with BC0 :
      DTU_start_ReSync_del  <= VFE_control.DTU_Start_ReSync;
      TP_trigger_prev       <= TP_trigger_merged;
      if VFE_control.DTU_start_ReSync='1' and DTU_Start_ReSync_del='0' then
        WDTU_Start_ReSync   <= '1';
      end if;
      if WDTU_Start_Resync = '1' then
        if clk_counter=SAFE_monitor.ReSync_command_pos then
          DTU_start_ReSync  <= '1';
          WDTU_Start_ReSync <= '0';
        end if;
      else
        DTU_start_ReSync    <= '0';
      end if;
-- Hold TP commands not to interfere with BC0 :
      if TP_trigger_merged='1' and TP_trigger_prev='0' then
        WTP_trigger          <= '1';
        TP_DAQ_start         <= '0';
        TP_duration_counter  <= (others => '0');
        TP_delay_counter     <= (others => '0');
        TP_DAQ_start_width   <= "00";
      end if;
      if WTP_trigger = '1' then
--        if clk_counter>1 and clk_counter<14200 and VFE_monitor.ReSync_busy='0' then  -- Don't send TP trigger around BC0 
        if clk_counter=SAFE_monitor.ReSync_command_pos then
          TP_trigger         <= '1';
          start_TP_delay     <= '1';
          WTP_trigger        <= '0';
        end if;
      end if;
      if TP_trigger = '1' then
        if TP_duration_counter < unsigned(SAFE_monitor.TP_duration) then        
          TP_duration_counter <= TP_duration_counter+1;
        else
          TP_trigger         <= '0';
        end if;
      end if;
      if start_TP_delay = '1' then
        if TP_delay_counter < unsigned(SAFE_monitor.TP_delay) then
          TP_delay_counter    <= TP_delay_counter+1;
        else
          TP_DAQ_start        <= '1';
          start_TP_delay      <= '0';
        end if;
      end if;
      if TP_DAQ_start = '1' then
        if TP_DAQ_start_width < "11" then
          TP_DAQ_start_width  <= TP_DAQ_start_width+1;
        else
          TP_DAQ_start        <= '0';
        end if;
      end if;
    end if;
  end process hold_ReSync_commands;

  gen_trig_out : process(MMCM_locked, pwup_reset, sequence_clk, SAFE_Control.trigger)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then
      delay_out_counter     <= x"0000";
      start_out_counter     <= '0';
      trig_software         <= '0';
      trig_sw_width         <= (others => '0');
    elsif rising_edge(sequence_clk) then
      trigger_prev1         <= SAFE_Control.trigger;
      trigger_prev2         <= trigger_prev1;
      if trigger_prev1='1' and trigger_prev2='0' then
        start_out_counter   <= '1';
        delay_out_counter   <= x"0000";
      end if;
      if start_out_counter = '1' then
        if delay_out_counter < unsigned(SAFE_control.trigger_SW_delay) then
          delay_out_counter <= delay_out_counter+1;
        else
          trig_software     <= '1';
          trig_sw_width     <= (others => '0');
          start_out_counter <= '0';
        end if;
      end if;
      if trig_software = '1' then
        if trig_sw_width < x"4" then
          trig_sw_width     <= trig_sw_width+1;
        else
          trig_software     <= '0';
        end if;
      end if;
    end if;
  end process gen_trig_out;

  gen_BC0_WTE : process(MMCM_locked, pwup_reset, sequence_clk)
  variable WTE_width   : unsigned(7 downto 0);
  begin
    if MMCM_locked='0'or pwup_reset='1' then
      clk_counter           <= (others => '0');
      orbit_counter         <= (others => '0');
      orbit_counter_WTE     <= (others => '0');
      WTE                   <= '0';
      BC0                   <= '0';
      Test_enable           <= '0';
    elsif rising_edge(sequence_clk) then
      if clk_counter < 14255 then                    -- LHC orbit = 3564 clock at 40 MHz
        clk_counter         <= clk_counter+1;
      else
        BC0                 <= SAFE_control.gen_BC0;
        clk_counter         <= (others => '0');
        orbit_counter       <= orbit_counter+1;
        if orbit_counter_WTE < 111 then              -- 100 Hz ~ 1 orbit out of 112
          orbit_counter_WTE <= orbit_counter_WTE+1;
        else
          orbit_counter_WTE <= (others => '0');
        end if;
      end if;
      if BC0 = '1' and clk_counter > 4 then
        BC0                 <= '0';
      end if;
      if orbit_counter_WTE = 0 and clk_counter = SAFE_monitor.WTE_command_pos then
        WTE                 <= SAFE_control.gen_WTE;
      end if;
      if clk_counter = SAFE_monitor.WTE_command_pos+16 then
        WTE                 <= '0';
      end if;
    end if;
  end process gen_BC0_WTE;
  
  gen_trig_in : process(MMCM_locked, pwup_reset, sequence_clk, trig_in)
  begin
    if MMCM_locked='0' or pwup_reset = '1' then
      delay_in_counter      <= x"0000";
      start_in_counter      <= '0';
      trig_hardware         <= '0';
      trig_in_Prev1         <= '0';
      trig_in_Prev2         <= '0';
      trig_hw_width         <= (others => '0');
    elsif rising_edge(sequence_clk) then
      trig_in_prev1         <= trig_in;
      trig_in_prev2         <= trig_in_prev1;
      if trig_in_prev1='1' and trig_in_prev2='0' then
        start_in_counter    <= '1';
        delay_in_counter    <= x"0000";
      end if;
      if start_in_counter = '1' then
        if delay_in_counter < unsigned(SAFE_monitor.trigger_HW_delay) then
          delay_in_counter    <= delay_in_counter+1;
        else
          trig_hardware       <= '1';
          trig_hw_width       <= (others => '0');
          start_in_counter    <= '0';
        end if;
      end if;
      if trig_hardware = '1' then
        if trig_hw_width < x"4" then
          trig_hw_width         <= trig_hw_width+1;
        else
          trig_hardware       <= '0';
        end if;
      end if;
    end if;
  end process gen_trig_in;
  
  gen_AWG_trigger : process(MMCM_locked, pwup_reset, sequence_clk)
  begin
    if MMCM_locked = '0' or pwup_reset='1' then 
      AWG_trigger_out       <= '0'; 
      AWG_trigger_width     <= (others => '0');
    elsif rising_edge(sequence_clk) then 
      AWG_trigger_del       <= SAFE_control.AWG_trigger;
      if SAFE_control.AWG_trigger='1' and AWG_trigger_del='0' then 
        AWG_trigger_out     <= '1'; 
        AWG_trigger_width   <= (others => '0');
      end if;
      if AWG_trigger_out = '1' then 
        if AWG_trigger_width /= x"F" then 
          AWG_trigger_width <= AWG_trigger_width+'1';
        else 
          AWG_trigger_out   <= '0'; 
        end if;
      end if;             
    end if;
  end process gen_AWG_trigger;

-------------------------------------------------------------------------------
-- Clocking
---- Clk port from local oscillator
  clk_IBUFGDS :    unisim.vcomponents.IBUFGDS generic map(DIFF_TERM => TRUE) port map (O => loc_clk_in, I  => CLK_160_P, IB => CLK_160_N);
  HP_CLK_in <= loc_clk_in;

  Inst_clk_delay_ctrl : clk_generator_delay_ctrl
  port map
  (
    clk_160_in      => HP_clk_in,
    clk_delay_ctrl  => clk_delay_ctrl,
    reset           => CLK_reset,
    locked          => delay_clock_locked
  );
  clk_Gb_eth        <= Clk_delay_ctrl;

  Inst_clk_generator_iserdes : clk_generator_iserdes
  port map
  (
    clk_160_in      => HP_Clk_in,
    clk_20          => HP_clk_20,
    clk_40          => HP_Clk_40,
    clk_160         => HP_Clk_160,
    clk_640         => HP_Clk_640,
    reset           => CLK_reset,
    locked          => iserdes_clock_locked
  );

  MMCM_locked       <= iserdes_clock_locked and delay_clock_locked;

--clock for ADC and VFE capture
  Clk_IPbus         <= HP_CLK_20;
  system_clk        <= HP_CLK_160;
  sequence_clk      <= HP_CLK_160;
  shift_reg_clk     <= HP_CLK_160;
  memory_clk        <= HP_CLK_160;
  resync_clk        <= HP_CLK_160;
  clk_I2C           <= HP_CLK_160;

--  LED(0) <= SAFE_control.LED_on and (reset and SAFE_monitor.LED_on);
--  LED(0) <= SAFE_control.LED_on and reset;
--  LED(2) <= SAFE_control.LED_on and SAFE_monitor.delay_locked;
--  LED(0) <= SAFE_control.LED_on and trig_local;

--  LED(0) <= SAFE_monitor.Sync_link_busy;
--  LED(1) <= MMCM_locked;
    LED(0) <= SAFE_control.LED_on and VFE_control.I2C_VFE_pattern(4);
    LED(1) <= SAFE_control.LED_on and VFE_control.I2C_VFE_pattern(5);
  --LED(3) <= SAFE_control.LED_on and Pwup_reset;

--            VFE_monitor.ADC_test_mode or CalBusy_delayed;
--  LED(1) <= SAFE_control.LED_on and (SAFE_monitor.Link_idelay_sync(1) or VFE_monitor.LVRB_busy);
--  LED(2) <= SAFE_control.LED_on and (SAFE_monitor.Link_byte_sync(1) or VFE_monitor.CATIA_busy);
--  LED(3) <= SAFE_control.LED_on and (SAFE_monitor.Link_bit_sync(1) or VFE_monitor.DTU_busy);
--  LED(1) <= SAFE_control.LED_on and SAFE_monitor.Link_idelay_sync(1);
--  LED(2) <= SAFE_control.LED_on and SAFE_monitor.Link_byte_sync(1);
--  LED(3) <= SAFE_control.LED_on and SAFE_monitor.Link_bit_sync(1);
--  LED(3) <= SAFE_control.LED_on and VFE_monitor.I2C_n_ack_DTU(0);

  -------------------------------------------------------------------------------
  -- VFE and FE interfaces
  -------------------------------------------------------------------------------  
  Inst_VFE_Input : entity work.VFE_Input
  port map (
    IO_reset            => SAFE_control.IO_reset,
    delay_reset         => SAFE_control.delay_reset,
    tapslip_DTU_map     => SAFE_control.tapslip_DTU_map,
    bitslip_DTU_map     => SAFE_control.bitslip_DTU_map,
    byteslip_DTU_map    => SAFE_control.byteslip_DTU_map,
    delay_tap_dir       => SAFE_control.delay_tap_dir,
  
    delay_locked        => SAFE_monitor.delay_locked,
    VFE_synchronized    => SAFE_monitor.VFE_synchronized,
    Sync_link_idelay_OK => SAFE_monitor.Sync_link_idelay_OK,
    Sync_link_byte_OK   => SAFE_monitor.Sync_link_byte_OK,
    Sync_link_bit_OK    => SAFE_monitor.Sync_link_bit_OK,
    Link_idelay_pos     => SAFE_monitor.Link_idelay_pos,
    Sync_duration       => SAFE_monitor.Sync_duration,
    Sync_link_OK        => SAFE_monitor.Sync_link_error,
    Sync_link_error     => SAFE_monitor.Sync_link_OK,
    start_IDELAY_sync   => SAFE_monitor.start_idelay_sync,
    Sync_link_busy      => SAFE_monitor.Sync_link_busy,
    
    eLink_active        => VFE_monitor.eLink_active,
    DTU_MEM_mode        => VFE_monitor.DTU_MEM_mode,
    DTU_test_mode       => VFE_monitor.DTU_test_mode,
    DTU_Sync_pattern    => VFE_monitor.DTU_Sync_pattern,
    
    MMCM_locked         => MMCM_locked,
    clk_delay_ctrl      => Clk_delay_ctrl,
    Clk_160             => HP_Clk_160,
    Clk_640             => HP_Clk_640,
    
    ch_in_P             => ch_in_P,
    ch_in_N             => ch_in_N,
    ch_captured_stream  => captured_stream,
    debug               => SAFE_monitor.debug
  );

  Inst_VFE_Ctrl : entity work.VFE_Ctrl
  port map (
    I2C_access          => VFE_control.I2C_Access_VFE,
    I2C_R_Wb            => VFE_control.I2C_R_Wb,
    I2C_VFE_pattern     => VFE_control.I2C_VFE_pattern,
    I2C_Device_number   => VFE_control.I2C_Device_number,
    I2C_Reg_number      => VFE_control.I2C_Reg_number,
    I2C_long_transfer   => VFE_control.I2C_long_transfer,
    I2C_Reg_data_in     => VFE_control.I2C_Reg_data,
    I2C_lpGBT_mode      => VFE_control.I2C_lpGBT_mode,

    I2C_busy_out        => VFE_monitor.I2C_VFE_busy,
    I2C_error           => VFE_monitor.I2C_VFE_error,
    I2C_reg_data_out    => VFE_monitor.I2C_Reg_data_VFE,
    I2C_n_ack           => VFE_monitor.I2C_n_ack_VFE,

    DTU_start_Resync    => DTU_start_Resync,
    DTU_ReSync_idle     => VFE_control.DTU_ReSync_idle,
    DTU_ReSync_data     => VFE_control.DTU_ReSync_data,
    ReSync_busy_out     => VFE_monitor.ReSync_busy,

    reset               => SAFE_reset,
    BC0                 => BC0,
    WTE                 => WTE,
    TE                  => Test_enable,
    I2C_clk             => clk_I2C,
    I2C_scl             => I2C_scl_VFE,
    I2C_sda             => I2C_sda_VFE,
    I2C_ack_spy         => VFE_monitor.I2C_ack_spy,

    sequence_clk        => sequence_clk,
    ReSync_DTU          => resync_out_loc
  );

  ReSync_out_R          <= ReSync_out_loc when rising_edge(sequence_clk) else ReSync_out_R;
  ReSync_out_F          <= ReSync_out_loc when falling_edge(sequence_clk) else ReSync_out_F;
  Resync_inst : for i in 1 to Nb_of_VFE generate 
  begin
    ReSync_out(i)       <= ReSync_out_R(i) when SAFE_monitor.resync_clock_phase(i)='1' else
                           ReSync_out_F(i);
  end generate;

  -------------------------------------------------------------------------------
  -- Networking and IPBUS interface
  -------------------------------------------------------------------------------
  ip_addr  <= x"0A00008"&ADDR;                             -- 10.0.0.(128..255)
  mac_addr <= x"060264BEEF0"&not(ADDR); -- 06:02:64:BE:EF:XX

  Inst_gig_ethernet_pcs_pma_0 : gig_ethernet_pcs_pma_0
  port map (
    gtrefclk_p             => GbE_refclk_P,
    gtrefclk_n             => GbE_refclk_N,
    gtrefclk_out           => open,
    gtrefclk_bufg_out      => open,
    txp                    => Gbe_TxP,
    txn                    => Gbe_TxN,
    rxp                    => Gbe_RxP,
    rxn                    => Gbe_RxN,
    resetdone              => open,     --GbE_ready
    userclk_out            => open,
    userclk2_out           => GbE_user_clk,
    rxuserclk_out          => open,
    rxuserclk2_out         => open,
    pma_reset_out          => open,
--    mmcm_locked_out        => LED(0),
--    independent_clock_bufg => sequence_clk,--CLK_LHC,
    independent_clock_bufg => CLK_Gb_eth,--CLK_LHC,
    gmii_txd               => gmii_txd,
    gmii_tx_en             => gmii_tx_en,
    gmii_tx_er             => gmii_tx_er,
    gmii_rxd               => gmii_rxd,
    gmii_rx_dv             => gmii_rx_dv,
    gmii_rx_er             => gmii_rx_er,
    gmii_isolate           => open,
    configuration_vector   => "00000",  --"00001",
    status_vector          => open,
    reset                  => '0',
    signal_detect          => '1',
    gt0_qplloutclk_out     => open,
    gt0_qplloutrefclk_out  => open
  );

  frame_size              <= x"05EE";
--  rx_configuration_vector <= mac_addr & frame_size & 'X' & '0' & "10" & "0X00X0000010";
--  tx_configuration_vector <= mac_addr & frame_size & 'X' & '0' & "10" & "XXX0X0000010";
  rx_configuration_vector <= X"0000_0000_0000_0000_0812";
  tx_configuration_vector <= X"0000_0000_0000_0000_0012";
  Inst_tri_mode_ethernet_mac : tri_mode_ethernet_mac_0
  port map (
    gtx_clk                 => GbE_user_clk,
    glbl_rstn               => '1',
    rx_axi_rstn             => '1',
    tx_axi_rstn             => '1',
    rx_statistics_vector    => open,
    rx_statistics_valid     => open,
    rx_mac_aclk             => open,
    rx_reset                => open,
    rx_axis_mac_tdata       => mac_rx_data,
    rx_axis_mac_tvalid      => mac_rx_valid,
    rx_axis_mac_tlast       => mac_rx_last,
    rx_axis_mac_tuser       => mac_rx_error,
    tx_ifg_delay            => x"00",  --x"04",
    tx_statistics_vector    => open,
    tx_statistics_valid     => open,
    tx_mac_aclk             => open,
    tx_reset                => open,
    tx_axis_mac_tdata       => mac_tx_data,
    tx_axis_mac_tvalid      => mac_tx_valid,
    tx_axis_mac_tlast       => mac_tx_last,
    tx_axis_mac_tuser       => mac_tx_error,
    tx_axis_mac_tready      => mac_tx_ready,
    pause_req               => '0',
    pause_val               => (others => '0'),
    speedis100              => open,
    speedis10100            => open,
    gmii_txd                => gmii_txd,
    gmii_tx_en              => gmii_tx_en,
    gmii_tx_er              => gmii_tx_er,
    gmii_rxd                => gmii_rxd,
    gmii_rx_dv              => gmii_rx_dv,
    gmii_rx_er              => gmii_rx_er,
    rx_configuration_vector => rx_configuration_vector,
    tx_configuration_vector => tx_configuration_vector
  );

  inst_ipbus : ipbus_ctrl
  port map(
    mac_clk      => GbE_user_clk,
    rst_macclk   => '0',
    ipb_clk      => clk_ipbus,
    rst_ipb      => '0',
    mac_rx_data  => mac_rx_data,
    mac_rx_valid => mac_rx_valid,
    mac_rx_last  => mac_rx_last,
    mac_rx_error => mac_rx_error,
    mac_tx_data  => mac_tx_data,
    mac_tx_valid => mac_tx_valid,
    mac_tx_last  => mac_tx_last,
    mac_tx_error => mac_tx_error(0),
    mac_tx_ready => mac_tx_ready,
    ipb_out      => ipb_master_out,
    ipb_in       => ipb_master_in,
    mac_addr     => mac_addr,
    ip_addr      => ip_addr,
    pkt_oob      => pkt_oob,
    pkt          => pkt
  );

  -------------------------------------------------------------------------------
  -- Translation from VFE to FE
  -------------------------------------------------------------------------------  

  inst_slaves : entity work.slaves
  port map(
    pwup_rst      => pwup_reset,
    DAQ_busy      => DAQ_busy,
    ipb_clk       => clk_ipbus,
    ipb_in        => ipb_master_out,
    ipb_out       => ipb_master_in,
    SAFE_Monitor  => SAFE_Monitor,
    SAFE_Control  => SAFE_Control,
    VFE_Monitor   => VFE_Monitor,
    VFE_Control   => VFE_Control,
    clk_40        => HP_Clk_40,
    clk_160       => HP_Clk_160,
    clk_shift_reg => shift_reg_clk,
    clk_memory    => memory_clk,
    ch_in         => captured_stream,
    trig_local    => trig_local,
    trig_signal   => trig_signal,
    CRC_reset     => SAFE_Control.CRC_reset,
    CRC_error     => SAFE_Monitor.CRC_error,
    APD_temp_in   => APD_temp_in,
    APD_temp_ref  => APD_temp_ref,
    CATIA_temp_in => CATIA_temp_in,
    CATIA_temp_ref=> CATIA_temp_ref
  );

--  LED      <= ADDR;
  test_counter : process (sequence_clk)
  begin  -- process test_counter
    if rising_edge(sequence_clk) then
      led_counter <= led_counter + 1;
--      LED(0) <= led_counter(0) and SAFE_Monitor.LED_on;
--      LED(1) <= led_counter(8) and SAFE_Monitor.LED_on;
--      LED(2) <= led_counter(16) and SAFE_Monitor.LED_on;
--      LED(3) <= led_counter(24) and SAFE_Monitor.LED_on;
    end if;
  end process test_counter;

--  GPIO     <= "ZZZZ";--i2c_clk_in&SFP_LOS & SFP_TX_Fault & SFP_Present;
  --JTAG_OUT <= JTAG_IN;

  -- SFP i2c bus
  SFP_SCL     <= 'Z';
  SFP_SDA     <= 'Z';

end rtl;
