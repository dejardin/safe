// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2024.1 (lin64) Build 5076996 Wed May 22 18:36:09 MDT 2024
// Date        : Fri Jul 12 09:11:01 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ clk_generator_iserdes_stub.v
// Design      : clk_generator_iserdes
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk_20, clk_40, clk_160, clk_640, reset, locked, 
  clk_160_in)
/* synthesis syn_black_box black_box_pad_pin="reset,locked,clk_160_in" */
/* synthesis syn_force_seq_prim="clk_20" */
/* synthesis syn_force_seq_prim="clk_40" */
/* synthesis syn_force_seq_prim="clk_160" */
/* synthesis syn_force_seq_prim="clk_640" */;
  output clk_20 /* synthesis syn_isclock = 1 */;
  output clk_40 /* synthesis syn_isclock = 1 */;
  output clk_160 /* synthesis syn_isclock = 1 */;
  output clk_640 /* synthesis syn_isclock = 1 */;
  input reset;
  output locked;
  input clk_160_in;
endmodule
