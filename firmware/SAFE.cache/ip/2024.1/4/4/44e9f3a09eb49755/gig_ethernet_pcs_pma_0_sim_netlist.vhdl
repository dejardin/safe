-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2024.1 (lin64) Build 5076996 Wed May 22 18:36:09 MDT 2024
-- Date        : Fri Jul 12 09:13:07 2024
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gig_ethernet_pcs_pma_0_sim_netlist.vhdl
-- Design      : gig_ethernet_pcs_pma_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  port (
    gtxe2_i_0 : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_2 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    cpll_pd0_i : in STD_LOGIC;
    cpllreset_in : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_8 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_11 : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  signal gtxe2_i_n_0 : STD_LOGIC;
  signal gtxe2_i_n_10 : STD_LOGIC;
  signal gtxe2_i_n_16 : STD_LOGIC;
  signal gtxe2_i_n_170 : STD_LOGIC;
  signal gtxe2_i_n_171 : STD_LOGIC;
  signal gtxe2_i_n_172 : STD_LOGIC;
  signal gtxe2_i_n_173 : STD_LOGIC;
  signal gtxe2_i_n_174 : STD_LOGIC;
  signal gtxe2_i_n_175 : STD_LOGIC;
  signal gtxe2_i_n_176 : STD_LOGIC;
  signal gtxe2_i_n_177 : STD_LOGIC;
  signal gtxe2_i_n_178 : STD_LOGIC;
  signal gtxe2_i_n_179 : STD_LOGIC;
  signal gtxe2_i_n_180 : STD_LOGIC;
  signal gtxe2_i_n_181 : STD_LOGIC;
  signal gtxe2_i_n_182 : STD_LOGIC;
  signal gtxe2_i_n_183 : STD_LOGIC;
  signal gtxe2_i_n_184 : STD_LOGIC;
  signal gtxe2_i_n_27 : STD_LOGIC;
  signal gtxe2_i_n_3 : STD_LOGIC;
  signal gtxe2_i_n_38 : STD_LOGIC;
  signal gtxe2_i_n_39 : STD_LOGIC;
  signal gtxe2_i_n_4 : STD_LOGIC;
  signal gtxe2_i_n_46 : STD_LOGIC;
  signal gtxe2_i_n_47 : STD_LOGIC;
  signal gtxe2_i_n_48 : STD_LOGIC;
  signal gtxe2_i_n_49 : STD_LOGIC;
  signal gtxe2_i_n_50 : STD_LOGIC;
  signal gtxe2_i_n_51 : STD_LOGIC;
  signal gtxe2_i_n_52 : STD_LOGIC;
  signal gtxe2_i_n_53 : STD_LOGIC;
  signal gtxe2_i_n_54 : STD_LOGIC;
  signal gtxe2_i_n_55 : STD_LOGIC;
  signal gtxe2_i_n_56 : STD_LOGIC;
  signal gtxe2_i_n_57 : STD_LOGIC;
  signal gtxe2_i_n_58 : STD_LOGIC;
  signal gtxe2_i_n_59 : STD_LOGIC;
  signal gtxe2_i_n_60 : STD_LOGIC;
  signal gtxe2_i_n_61 : STD_LOGIC;
  signal gtxe2_i_n_81 : STD_LOGIC;
  signal gtxe2_i_n_83 : STD_LOGIC;
  signal gtxe2_i_n_84 : STD_LOGIC;
  signal gtxe2_i_n_9 : STD_LOGIC;
  signal NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PHYSTATUS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDATAVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXELECIDLE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHARISK_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHBONDO_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 16 );
  signal NLW_gtxe2_i_RXDISPERR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXHEADER_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXSTATUS_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_TSTOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_i : label is "PRIMITIVE";
begin
gtxe2_i: unisim.vcomponents.GTXE2_CHANNEL
    generic map(
      ALIGN_COMMA_DOUBLE => "FALSE",
      ALIGN_COMMA_ENABLE => B"0001111111",
      ALIGN_COMMA_WORD => 2,
      ALIGN_MCOMMA_DET => "TRUE",
      ALIGN_MCOMMA_VALUE => B"1010000011",
      ALIGN_PCOMMA_DET => "TRUE",
      ALIGN_PCOMMA_VALUE => B"0101111100",
      CBCC_DATA_SOURCE_SEL => "DECODED",
      CHAN_BOND_KEEP_ALIGN => "FALSE",
      CHAN_BOND_MAX_SKEW => 1,
      CHAN_BOND_SEQ_1_1 => B"0000000000",
      CHAN_BOND_SEQ_1_2 => B"0000000000",
      CHAN_BOND_SEQ_1_3 => B"0000000000",
      CHAN_BOND_SEQ_1_4 => B"0000000000",
      CHAN_BOND_SEQ_1_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_1 => B"0000000000",
      CHAN_BOND_SEQ_2_2 => B"0000000000",
      CHAN_BOND_SEQ_2_3 => B"0000000000",
      CHAN_BOND_SEQ_2_4 => B"0000000000",
      CHAN_BOND_SEQ_2_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_USE => "FALSE",
      CHAN_BOND_SEQ_LEN => 1,
      CLK_CORRECT_USE => "TRUE",
      CLK_COR_KEEP_IDLE => "FALSE",
      CLK_COR_MAX_LAT => 36,
      CLK_COR_MIN_LAT => 33,
      CLK_COR_PRECEDENCE => "TRUE",
      CLK_COR_REPEAT_WAIT => 0,
      CLK_COR_SEQ_1_1 => B"0110111100",
      CLK_COR_SEQ_1_2 => B"0001010000",
      CLK_COR_SEQ_1_3 => B"0000000000",
      CLK_COR_SEQ_1_4 => B"0000000000",
      CLK_COR_SEQ_1_ENABLE => B"1111",
      CLK_COR_SEQ_2_1 => B"0110111100",
      CLK_COR_SEQ_2_2 => B"0010110101",
      CLK_COR_SEQ_2_3 => B"0000000000",
      CLK_COR_SEQ_2_4 => B"0000000000",
      CLK_COR_SEQ_2_ENABLE => B"1111",
      CLK_COR_SEQ_2_USE => "TRUE",
      CLK_COR_SEQ_LEN => 2,
      CPLL_CFG => X"BC07DC",
      CPLL_FBDIV => 4,
      CPLL_FBDIV_45 => 5,
      CPLL_INIT_CFG => X"00001E",
      CPLL_LOCK_CFG => X"01E8",
      CPLL_REFCLK_DIV => 1,
      DEC_MCOMMA_DETECT => "TRUE",
      DEC_PCOMMA_DETECT => "TRUE",
      DEC_VALID_COMMA_ONLY => "FALSE",
      DMONITOR_CFG => X"000A00",
      ES_CONTROL => B"000000",
      ES_ERRDET_EN => "FALSE",
      ES_EYE_SCAN_EN => "TRUE",
      ES_HORZ_OFFSET => X"000",
      ES_PMA_CFG => B"0000000000",
      ES_PRESCALE => B"00000",
      ES_QUALIFIER => X"00000000000000000000",
      ES_QUAL_MASK => X"00000000000000000000",
      ES_SDATA_MASK => X"00000000000000000000",
      ES_VERT_OFFSET => B"000000000",
      FTS_DESKEW_SEQ_ENABLE => B"1111",
      FTS_LANE_DESKEW_CFG => B"1111",
      FTS_LANE_DESKEW_EN => "FALSE",
      GEARBOX_MODE => B"000",
      IS_CPLLLOCKDETCLK_INVERTED => '0',
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_RXUSRCLK2_INVERTED => '0',
      IS_RXUSRCLK_INVERTED => '0',
      IS_TXPHDLYTSTCLK_INVERTED => '0',
      IS_TXUSRCLK2_INVERTED => '0',
      IS_TXUSRCLK_INVERTED => '0',
      OUTREFCLK_SEL_INV => B"11",
      PCS_PCIE_EN => "FALSE",
      PCS_RSVD_ATTR => X"000000000000",
      PD_TRANS_TIME_FROM_P2 => X"03C",
      PD_TRANS_TIME_NONE_P2 => X"19",
      PD_TRANS_TIME_TO_P2 => X"64",
      PMA_RSV => X"00018480",
      PMA_RSV2 => X"2050",
      PMA_RSV3 => B"00",
      PMA_RSV4 => X"00000000",
      RXBUFRESET_TIME => B"00001",
      RXBUF_ADDR_MODE => "FULL",
      RXBUF_EIDLE_HI_CNT => B"1000",
      RXBUF_EIDLE_LO_CNT => B"0000",
      RXBUF_EN => "TRUE",
      RXBUF_RESET_ON_CB_CHANGE => "TRUE",
      RXBUF_RESET_ON_COMMAALIGN => "FALSE",
      RXBUF_RESET_ON_EIDLE => "FALSE",
      RXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      RXBUF_THRESH_OVFLW => 61,
      RXBUF_THRESH_OVRD => "FALSE",
      RXBUF_THRESH_UNDFLW => 8,
      RXCDRFREQRESET_TIME => B"00001",
      RXCDRPHRESET_TIME => B"00001",
      RXCDR_CFG => X"03000023FF10100020",
      RXCDR_FR_RESET_ON_EIDLE => '0',
      RXCDR_HOLD_DURING_EIDLE => '0',
      RXCDR_LOCK_CFG => B"010101",
      RXCDR_PH_RESET_ON_EIDLE => '0',
      RXDFELPMRESET_TIME => B"0001111",
      RXDLY_CFG => X"001F",
      RXDLY_LCFG => X"030",
      RXDLY_TAP_CFG => X"0000",
      RXGEARBOX_EN => "FALSE",
      RXISCANRESET_TIME => B"00001",
      RXLPM_HF_CFG => B"00000011110000",
      RXLPM_LF_CFG => B"00000011110000",
      RXOOB_CFG => B"0000110",
      RXOUT_DIV => 4,
      RXPCSRESET_TIME => B"00001",
      RXPHDLY_CFG => X"084020",
      RXPH_CFG => X"000000",
      RXPH_MONITOR_SEL => B"00000",
      RXPMARESET_TIME => B"00011",
      RXPRBS_ERR_LOOPBACK => '0',
      RXSLIDE_AUTO_WAIT => 7,
      RXSLIDE_MODE => "OFF",
      RX_BIAS_CFG => B"000000000100",
      RX_BUFFER_CFG => B"000000",
      RX_CLK25_DIV => 5,
      RX_CLKMUX_PD => '1',
      RX_CM_SEL => B"11",
      RX_CM_TRIM => B"010",
      RX_DATA_WIDTH => 20,
      RX_DDI_SEL => B"000000",
      RX_DEBUG_CFG => B"000000000000",
      RX_DEFER_RESET_BUF_EN => "TRUE",
      RX_DFE_GAIN_CFG => X"020FEA",
      RX_DFE_H2_CFG => B"000000000000",
      RX_DFE_H3_CFG => B"000001000000",
      RX_DFE_H4_CFG => B"00011110000",
      RX_DFE_H5_CFG => B"00011100000",
      RX_DFE_KL_CFG => B"0000011111110",
      RX_DFE_KL_CFG2 => X"301148AC",
      RX_DFE_LPM_CFG => X"0904",
      RX_DFE_LPM_HOLD_DURING_EIDLE => '0',
      RX_DFE_UT_CFG => B"10001111000000000",
      RX_DFE_VP_CFG => B"00011111100000011",
      RX_DFE_XYD_CFG => B"0000000000000",
      RX_DISPERR_SEQ_MATCH => "TRUE",
      RX_INT_DATAWIDTH => 0,
      RX_OS_CFG => B"0000010000000",
      RX_SIG_VALID_DLY => 10,
      RX_XCLK_SEL => "RXREC",
      SAS_MAX_COM => 64,
      SAS_MIN_COM => 36,
      SATA_BURST_SEQ_LEN => B"0101",
      SATA_BURST_VAL => B"100",
      SATA_CPLL_CFG => "VCO_3000MHZ",
      SATA_EIDLE_VAL => B"100",
      SATA_MAX_BURST => 8,
      SATA_MAX_INIT => 21,
      SATA_MAX_WAKE => 7,
      SATA_MIN_BURST => 4,
      SATA_MIN_INIT => 12,
      SATA_MIN_WAKE => 4,
      SHOW_REALIGN_COMMA => "TRUE",
      SIM_CPLLREFCLK_SEL => B"001",
      SIM_RECEIVER_DETECT_PASS => "TRUE",
      SIM_RESET_SPEEDUP => "TRUE",
      SIM_TX_EIDLE_DRIVE_LEVEL => "X",
      SIM_VERSION => "4.0",
      TERM_RCAL_CFG => B"10000",
      TERM_RCAL_OVRD => '0',
      TRANS_TIME_RATE => X"0E",
      TST_RSV => X"00000000",
      TXBUF_EN => "TRUE",
      TXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      TXDLY_CFG => X"001F",
      TXDLY_LCFG => X"030",
      TXDLY_TAP_CFG => X"0000",
      TXGEARBOX_EN => "FALSE",
      TXOUT_DIV => 4,
      TXPCSRESET_TIME => B"00001",
      TXPHDLY_CFG => X"084020",
      TXPH_CFG => X"0780",
      TXPH_MONITOR_SEL => B"00000",
      TXPMARESET_TIME => B"00001",
      TX_CLK25_DIV => 5,
      TX_CLKMUX_PD => '1',
      TX_DATA_WIDTH => 20,
      TX_DEEMPH0 => B"00000",
      TX_DEEMPH1 => B"00000",
      TX_DRIVE_MODE => "DIRECT",
      TX_EIDLE_ASSERT_DELAY => B"110",
      TX_EIDLE_DEASSERT_DELAY => B"100",
      TX_INT_DATAWIDTH => 0,
      TX_LOOPBACK_DRIVE_HIZ => "FALSE",
      TX_MAINCURSOR_SEL => '0',
      TX_MARGIN_FULL_0 => B"1001110",
      TX_MARGIN_FULL_1 => B"1001001",
      TX_MARGIN_FULL_2 => B"1000101",
      TX_MARGIN_FULL_3 => B"1000010",
      TX_MARGIN_FULL_4 => B"1000000",
      TX_MARGIN_LOW_0 => B"1000110",
      TX_MARGIN_LOW_1 => B"1000100",
      TX_MARGIN_LOW_2 => B"1000010",
      TX_MARGIN_LOW_3 => B"1000000",
      TX_MARGIN_LOW_4 => B"1000000",
      TX_PREDRIVER_MODE => '0',
      TX_QPI_STATUS_EN => '0',
      TX_RXDETECT_CFG => X"1832",
      TX_RXDETECT_REF => B"100",
      TX_XCLK_SEL => "TXOUT",
      UCODEER_CLR => '0'
    )
        port map (
      CFGRESET => '0',
      CLKRSVD(3 downto 0) => B"0000",
      CPLLFBCLKLOST => gtxe2_i_n_0,
      CPLLLOCK => gtxe2_i_0,
      CPLLLOCKDETCLK => independent_clock_bufg,
      CPLLLOCKEN => '1',
      CPLLPD => cpll_pd0_i,
      CPLLREFCLKLOST => gt0_cpllrefclklost_i,
      CPLLREFCLKSEL(2 downto 0) => B"001",
      CPLLRESET => cpllreset_in,
      DMONITOROUT(7) => gtxe2_i_n_177,
      DMONITOROUT(6) => gtxe2_i_n_178,
      DMONITOROUT(5) => gtxe2_i_n_179,
      DMONITOROUT(4) => gtxe2_i_n_180,
      DMONITOROUT(3) => gtxe2_i_n_181,
      DMONITOROUT(2) => gtxe2_i_n_182,
      DMONITOROUT(1) => gtxe2_i_n_183,
      DMONITOROUT(0) => gtxe2_i_n_184,
      DRPADDR(8 downto 0) => B"000000000",
      DRPCLK => gtrefclk_bufg,
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15) => gtxe2_i_n_46,
      DRPDO(14) => gtxe2_i_n_47,
      DRPDO(13) => gtxe2_i_n_48,
      DRPDO(12) => gtxe2_i_n_49,
      DRPDO(11) => gtxe2_i_n_50,
      DRPDO(10) => gtxe2_i_n_51,
      DRPDO(9) => gtxe2_i_n_52,
      DRPDO(8) => gtxe2_i_n_53,
      DRPDO(7) => gtxe2_i_n_54,
      DRPDO(6) => gtxe2_i_n_55,
      DRPDO(5) => gtxe2_i_n_56,
      DRPDO(4) => gtxe2_i_n_57,
      DRPDO(3) => gtxe2_i_n_58,
      DRPDO(2) => gtxe2_i_n_59,
      DRPDO(1) => gtxe2_i_n_60,
      DRPDO(0) => gtxe2_i_n_61,
      DRPEN => '0',
      DRPRDY => gtxe2_i_n_3,
      DRPWE => '0',
      EYESCANDATAERROR => gtxe2_i_n_4,
      EYESCANMODE => '0',
      EYESCANRESET => '0',
      EYESCANTRIGGER => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTREFCLKMONITOR => NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED,
      GTRESETSEL => '0',
      GTRSVD(15 downto 0) => B"0000000000000000",
      GTRXRESET => SR(0),
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      GTTXRESET => gt0_gttxreset_in0_out,
      GTXRXN => rxn,
      GTXRXP => rxp,
      GTXTXN => txn,
      GTXTXP => txp,
      LOOPBACK(2 downto 0) => B"000",
      PCSRSVDIN(15 downto 0) => B"0000000000000000",
      PCSRSVDIN2(4 downto 0) => B"00000",
      PCSRSVDOUT(15 downto 0) => NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED(15 downto 0),
      PHYSTATUS => NLW_gtxe2_i_PHYSTATUS_UNCONNECTED,
      PMARSVDIN(4 downto 0) => B"00000",
      PMARSVDIN2(4 downto 0) => B"00000",
      QPLLCLK => gt0_qplloutclk_out,
      QPLLREFCLK => gt0_qplloutrefclk_out,
      RESETOVRD => '0',
      RX8B10BEN => '1',
      RXBUFRESET => '0',
      RXBUFSTATUS(2) => RXBUFSTATUS(0),
      RXBUFSTATUS(1) => gtxe2_i_n_83,
      RXBUFSTATUS(0) => gtxe2_i_n_84,
      RXBYTEISALIGNED => gtxe2_i_n_9,
      RXBYTEREALIGN => gtxe2_i_n_10,
      RXCDRFREQRESET => '0',
      RXCDRHOLD => '0',
      RXCDRLOCK => NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED,
      RXCDROVRDEN => '0',
      RXCDRRESET => '0',
      RXCDRRESETRSV => '0',
      RXCHANBONDSEQ => NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED,
      RXCHANISALIGNED => NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED,
      RXCHANREALIGN => NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED,
      RXCHARISCOMMA(7 downto 2) => NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED(7 downto 2),
      RXCHARISCOMMA(1 downto 0) => gtxe2_i_4(1 downto 0),
      RXCHARISK(7 downto 2) => NLW_gtxe2_i_RXCHARISK_UNCONNECTED(7 downto 2),
      RXCHARISK(1 downto 0) => gtxe2_i_5(1 downto 0),
      RXCHBONDEN => '0',
      RXCHBONDI(4 downto 0) => B"00000",
      RXCHBONDLEVEL(2 downto 0) => B"000",
      RXCHBONDMASTER => '0',
      RXCHBONDO(4 downto 0) => NLW_gtxe2_i_RXCHBONDO_UNCONNECTED(4 downto 0),
      RXCHBONDSLAVE => '0',
      RXCLKCORCNT(1 downto 0) => D(1 downto 0),
      RXCOMINITDET => NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED,
      RXCOMMADET => gtxe2_i_n_16,
      RXCOMMADETEN => '1',
      RXCOMSASDET => NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED,
      RXCOMWAKEDET => NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED,
      RXDATA(63 downto 16) => NLW_gtxe2_i_RXDATA_UNCONNECTED(63 downto 16),
      RXDATA(15 downto 0) => gtxe2_i_3(15 downto 0),
      RXDATAVALID => NLW_gtxe2_i_RXDATAVALID_UNCONNECTED,
      RXDDIEN => '0',
      RXDFEAGCHOLD => '0',
      RXDFEAGCOVRDEN => '0',
      RXDFECM1EN => '0',
      RXDFELFHOLD => '0',
      RXDFELFOVRDEN => '0',
      RXDFELPMRESET => '0',
      RXDFETAP2HOLD => '0',
      RXDFETAP2OVRDEN => '0',
      RXDFETAP3HOLD => '0',
      RXDFETAP3OVRDEN => '0',
      RXDFETAP4HOLD => '0',
      RXDFETAP4OVRDEN => '0',
      RXDFETAP5HOLD => '0',
      RXDFETAP5OVRDEN => '0',
      RXDFEUTHOLD => '0',
      RXDFEUTOVRDEN => '0',
      RXDFEVPHOLD => '0',
      RXDFEVPOVRDEN => '0',
      RXDFEVSEN => '0',
      RXDFEXYDEN => '1',
      RXDFEXYDHOLD => '0',
      RXDFEXYDOVRDEN => '0',
      RXDISPERR(7 downto 2) => NLW_gtxe2_i_RXDISPERR_UNCONNECTED(7 downto 2),
      RXDISPERR(1 downto 0) => gtxe2_i_6(1 downto 0),
      RXDLYBYPASS => '1',
      RXDLYEN => '0',
      RXDLYOVRDEN => '0',
      RXDLYSRESET => '0',
      RXDLYSRESETDONE => NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED,
      RXELECIDLE => NLW_gtxe2_i_RXELECIDLE_UNCONNECTED,
      RXELECIDLEMODE(1 downto 0) => B"11",
      RXGEARBOXSLIP => '0',
      RXHEADER(2 downto 0) => NLW_gtxe2_i_RXHEADER_UNCONNECTED(2 downto 0),
      RXHEADERVALID => NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED,
      RXLPMEN => '1',
      RXLPMHFHOLD => '0',
      RXLPMHFOVRDEN => '0',
      RXLPMLFHOLD => '0',
      RXLPMLFKLOVRDEN => '0',
      RXMCOMMAALIGNEN => reset_out,
      RXMONITOROUT(6) => gtxe2_i_n_170,
      RXMONITOROUT(5) => gtxe2_i_n_171,
      RXMONITOROUT(4) => gtxe2_i_n_172,
      RXMONITOROUT(3) => gtxe2_i_n_173,
      RXMONITOROUT(2) => gtxe2_i_n_174,
      RXMONITOROUT(1) => gtxe2_i_n_175,
      RXMONITOROUT(0) => gtxe2_i_n_176,
      RXMONITORSEL(1 downto 0) => B"00",
      RXNOTINTABLE(7 downto 2) => NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED(7 downto 2),
      RXNOTINTABLE(1 downto 0) => gtxe2_i_7(1 downto 0),
      RXOOBRESET => '0',
      RXOSHOLD => '0',
      RXOSOVRDEN => '0',
      RXOUTCLK => rxoutclk,
      RXOUTCLKFABRIC => NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED,
      RXOUTCLKPCS => NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED,
      RXOUTCLKSEL(2 downto 0) => B"010",
      RXPCOMMAALIGNEN => reset_out,
      RXPCSRESET => reset,
      RXPD(1) => RXPD(0),
      RXPD(0) => RXPD(0),
      RXPHALIGN => '0',
      RXPHALIGNDONE => NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED,
      RXPHALIGNEN => '0',
      RXPHDLYPD => '0',
      RXPHDLYRESET => '0',
      RXPHMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED(4 downto 0),
      RXPHOVRDEN => '0',
      RXPHSLIPMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED(4 downto 0),
      RXPMARESET => '0',
      RXPOLARITY => '0',
      RXPRBSCNTRESET => '0',
      RXPRBSERR => gtxe2_i_n_27,
      RXPRBSSEL(2 downto 0) => B"000",
      RXQPIEN => '0',
      RXQPISENN => NLW_gtxe2_i_RXQPISENN_UNCONNECTED,
      RXQPISENP => NLW_gtxe2_i_RXQPISENP_UNCONNECTED,
      RXRATE(2 downto 0) => B"000",
      RXRATEDONE => NLW_gtxe2_i_RXRATEDONE_UNCONNECTED,
      RXRESETDONE => gtxe2_i_1,
      RXSLIDE => '0',
      RXSTARTOFSEQ => NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED,
      RXSTATUS(2 downto 0) => NLW_gtxe2_i_RXSTATUS_UNCONNECTED(2 downto 0),
      RXSYSCLKSEL(1 downto 0) => B"00",
      RXUSERRDY => gt0_rxuserrdy_t,
      RXUSRCLK => gtxe2_i_8,
      RXUSRCLK2 => gtxe2_i_8,
      RXVALID => NLW_gtxe2_i_RXVALID_UNCONNECTED,
      SETERRSTATUS => '0',
      TSTIN(19 downto 0) => B"11111111111111111111",
      TSTOUT(9 downto 0) => NLW_gtxe2_i_TSTOUT_UNCONNECTED(9 downto 0),
      TX8B10BBYPASS(7 downto 0) => B"00000000",
      TX8B10BEN => '1',
      TXBUFDIFFCTRL(2 downto 0) => B"100",
      TXBUFSTATUS(1) => TXBUFSTATUS(0),
      TXBUFSTATUS(0) => gtxe2_i_n_81,
      TXCHARDISPMODE(7 downto 2) => B"000000",
      TXCHARDISPMODE(1 downto 0) => gtxe2_i_9(1 downto 0),
      TXCHARDISPVAL(7 downto 2) => B"000000",
      TXCHARDISPVAL(1 downto 0) => gtxe2_i_10(1 downto 0),
      TXCHARISK(7 downto 2) => B"000000",
      TXCHARISK(1 downto 0) => gtxe2_i_11(1 downto 0),
      TXCOMFINISH => NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED,
      TXCOMINIT => '0',
      TXCOMSAS => '0',
      TXCOMWAKE => '0',
      TXDATA(63 downto 16) => B"000000000000000000000000000000000000000000000000",
      TXDATA(15 downto 0) => Q(15 downto 0),
      TXDEEMPH => '0',
      TXDETECTRX => '0',
      TXDIFFCTRL(3 downto 0) => B"1000",
      TXDIFFPD => '0',
      TXDLYBYPASS => '1',
      TXDLYEN => '0',
      TXDLYHOLD => '0',
      TXDLYOVRDEN => '0',
      TXDLYSRESET => '0',
      TXDLYSRESETDONE => NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED,
      TXDLYUPDOWN => '0',
      TXELECIDLE => TXPD(0),
      TXGEARBOXREADY => NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED,
      TXHEADER(2 downto 0) => B"000",
      TXINHIBIT => '0',
      TXMAINCURSOR(6 downto 0) => B"0000000",
      TXMARGIN(2 downto 0) => B"000",
      TXOUTCLK => txoutclk,
      TXOUTCLKFABRIC => gtxe2_i_n_38,
      TXOUTCLKPCS => gtxe2_i_n_39,
      TXOUTCLKSEL(2 downto 0) => B"100",
      TXPCSRESET => '0',
      TXPD(1) => TXPD(0),
      TXPD(0) => TXPD(0),
      TXPDELECIDLEMODE => '0',
      TXPHALIGN => '0',
      TXPHALIGNDONE => NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED,
      TXPHALIGNEN => '0',
      TXPHDLYPD => '0',
      TXPHDLYRESET => '0',
      TXPHDLYTSTCLK => '0',
      TXPHINIT => '0',
      TXPHINITDONE => NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED,
      TXPHOVRDEN => '0',
      TXPISOPD => '0',
      TXPMARESET => '0',
      TXPOLARITY => '0',
      TXPOSTCURSOR(4 downto 0) => B"00000",
      TXPOSTCURSORINV => '0',
      TXPRBSFORCEERR => '0',
      TXPRBSSEL(2 downto 0) => B"000",
      TXPRECURSOR(4 downto 0) => B"00000",
      TXPRECURSORINV => '0',
      TXQPIBIASEN => '0',
      TXQPISENN => NLW_gtxe2_i_TXQPISENN_UNCONNECTED,
      TXQPISENP => NLW_gtxe2_i_TXQPISENP_UNCONNECTED,
      TXQPISTRONGPDOWN => '0',
      TXQPIWEAKPUP => '0',
      TXRATE(2 downto 0) => B"000",
      TXRATEDONE => NLW_gtxe2_i_TXRATEDONE_UNCONNECTED,
      TXRESETDONE => gtxe2_i_2,
      TXSEQUENCE(6 downto 0) => B"0000000",
      TXSTARTSEQ => '0',
      TXSWING => '0',
      TXSYSCLKSEL(1 downto 0) => B"00",
      TXUSERRDY => gt0_txuserrdy_t,
      TXUSRCLK => gtxe2_i_8,
      TXUSRCLK2 => gtxe2_i_8
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking is
  port (
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg : out STD_LOGIC;
    mmcm_locked : out STD_LOGIC;
    userclk : out STD_LOGIC;
    userclk2 : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    txoutclk : in STD_LOGIC;
    mmcm_reset : in STD_LOGIC;
    rxoutclk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking is
  signal clkfbout : STD_LOGIC;
  signal clkout0 : STD_LOGIC;
  signal clkout1 : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal txoutclk_bufg : STD_LOGIC;
  signal NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of bufg_gtrefclk : label is "PRIMITIVE";
  attribute box_type of bufg_txoutclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk2 : label is "PRIMITIVE";
  attribute box_type of ibufds_gtrefclk : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
  attribute box_type of rxrecclkbufg : label is "PRIMITIVE";
begin
  gtrefclk_out <= \^gtrefclk_out\;
bufg_gtrefclk: unisim.vcomponents.BUFG
     port map (
      I => \^gtrefclk_out\,
      O => gtrefclk_bufg
    );
bufg_txoutclk: unisim.vcomponents.BUFG
     port map (
      I => txoutclk,
      O => txoutclk_bufg
    );
bufg_userclk: unisim.vcomponents.BUFG
     port map (
      I => clkout1,
      O => userclk
    );
bufg_userclk2: unisim.vcomponents.BUFG
     port map (
      I => clkout0,
      O => userclk2
    );
ibufds_gtrefclk: unisim.vcomponents.IBUFDS_GTE2
    generic map(
      CLKCM_CFG => true,
      CLKRCV_TRST => true,
      CLKSWING_CFG => B"11"
    )
        port map (
      CEB => '0',
      I => gtrefclk_p,
      IB => gtrefclk_n,
      O => \^gtrefclk_out\,
      ODIV2 => NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 16.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 16.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 8.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 16,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "INTERNAL",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.000000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout,
      CLKFBOUT => clkfbout,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => txoutclk_bufg,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clkout0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clkout1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => mmcm_locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => mmcm_reset
    );
rxrecclkbufg: unisim.vcomponents.BUFG
     port map (
      I => rxoutclk,
      O => rxuserclk2_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing is
  port (
    cpll_pd0_i : out STD_LOGIC;
    cpllreset_in : out STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gt0_cpllreset_t : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing is
  signal cpll_reset_out : STD_LOGIC;
  signal \cpllpd_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[94]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[126]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[95]_srl32_n_1\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name : string;
  attribute srl_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 ";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \cpllpd_wait_reg[95]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 ";
  attribute equivalent_register_removal of \cpllreset_wait_reg[127]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 ";
begin
\cpllpd_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[31]_srl32_n_1\
    );
\cpllpd_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[63]_srl32_n_1\
    );
\cpllpd_wait_reg[94]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[63]_srl32_n_1\,
      Q => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q31 => \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\
    );
\cpllpd_wait_reg[95]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q => cpll_pd0_i,
      R => '0'
    );
\cpllreset_wait_reg[126]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[95]_srl32_n_1\,
      Q => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q31 => \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\
    );
\cpllreset_wait_reg[127]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q => cpll_reset_out,
      R => '0'
    );
\cpllreset_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"000000FF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[31]_srl32_n_1\
    );
\cpllreset_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[63]_srl32_n_1\
    );
\cpllreset_wait_reg[95]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[63]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[95]_srl32_n_1\
    );
gtxe2_i_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cpll_reset_out,
      I1 => gt0_cpllreset_t,
      O => cpllreset_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common is
  port (
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common is
  signal gtxe2_common_i_n_2 : STD_LOGIC;
  signal gtxe2_common_i_n_5 : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_common_i : label is "PRIMITIVE";
begin
gtxe2_common_i: unisim.vcomponents.GTXE2_COMMON
    generic map(
      BIAS_CFG => X"0000040000001000",
      COMMON_CFG => X"00000000",
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_QPLLLOCKDETCLK_INVERTED => '0',
      QPLL_CFG => X"06801C1",
      QPLL_CLKOUT_CFG => B"0000",
      QPLL_COARSE_FREQ_OVRD => B"010000",
      QPLL_COARSE_FREQ_OVRD_EN => '0',
      QPLL_CP => B"0000011111",
      QPLL_CP_MONITOR_EN => '0',
      QPLL_DMONITOR_SEL => '0',
      QPLL_FBDIV => B"0000100000",
      QPLL_FBDIV_MONITOR_EN => '0',
      QPLL_FBDIV_RATIO => '1',
      QPLL_INIT_CFG => X"000006",
      QPLL_LOCK_CFG => X"21E8",
      QPLL_LPF => B"1111",
      QPLL_REFCLK_DIV => 1,
      SIM_QPLLREFCLK_SEL => B"001",
      SIM_RESET_SPEEDUP => "FALSE",
      SIM_VERSION => "4.0"
    )
        port map (
      BGBYPASSB => '1',
      BGMONITORENB => '1',
      BGPDB => '1',
      BGRCALOVRD(4 downto 0) => B"11111",
      DRPADDR(7 downto 0) => B"00000000",
      DRPCLK => '0',
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15 downto 0) => NLW_gtxe2_common_i_DRPDO_UNCONNECTED(15 downto 0),
      DRPEN => '0',
      DRPRDY => NLW_gtxe2_common_i_DRPRDY_UNCONNECTED,
      DRPWE => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      PMARSVD(7 downto 0) => B"00000000",
      QPLLDMONITOR(7 downto 0) => NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED(7 downto 0),
      QPLLFBCLKLOST => NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED,
      QPLLLOCK => gtxe2_common_i_n_2,
      QPLLLOCKDETCLK => independent_clock_bufg,
      QPLLLOCKEN => '1',
      QPLLOUTCLK => gt0_qplloutclk_out,
      QPLLOUTREFCLK => gt0_qplloutrefclk_out,
      QPLLOUTRESET => '0',
      QPLLPD => '1',
      QPLLREFCLKLOST => gtxe2_common_i_n_5,
      QPLLREFCLKSEL(2 downto 0) => B"001",
      QPLLRESET => \out\(0),
      QPLLRSVD1(15 downto 0) => B"0000000000000000",
      QPLLRSVD2(4 downto 0) => B"11111",
      RCALENB => '1',
      REFCLKOUTMONITOR => NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync is
  port (
    reset_out : out STD_LOGIC;
    CLK : in STD_LOGIC;
    enablealign : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => '0',
      PRE => enablealign,
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg1,
      PRE => enablealign,
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg2,
      PRE => enablealign,
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg3,
      PRE => enablealign,
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg4,
      PRE => enablealign,
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => SR(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => SR(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => SR(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => SR(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => SR(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    reset_sync5_0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer is
  port (
    reset : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer is
  signal \counter_stg1[5]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg1[5]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg1_reg : STD_LOGIC_VECTOR ( 5 to 5 );
  signal \counter_stg1_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \counter_stg2[0]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg2_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg2_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal counter_stg30 : STD_LOGIC;
  signal \counter_stg3[0]_i_3_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_4_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_5_n_0\ : STD_LOGIC;
  signal counter_stg3_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg3_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal eqOp : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal reset0 : STD_LOGIC;
  signal reset_i_2_n_0 : STD_LOGIC;
  signal reset_i_3_n_0 : STD_LOGIC;
  signal reset_i_4_n_0 : STD_LOGIC;
  signal reset_i_5_n_0 : STD_LOGIC;
  signal reset_i_6_n_0 : STD_LOGIC;
  signal \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \counter_stg1[0]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[1]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[2]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[3]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[4]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \counter_stg1[5]_i_3\ : label is "soft_lutpair71";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[8]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[8]_i_1\ : label is 11;
begin
\counter_stg1[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      O => plusOp(0)
    );
\counter_stg1[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      I1 => \counter_stg1_reg__0\(1),
      O => plusOp(1)
    );
\counter_stg1[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \counter_stg1_reg__0\(1),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(2),
      O => plusOp(2)
    );
\counter_stg1[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \counter_stg1_reg__0\(2),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(1),
      I3 => \counter_stg1_reg__0\(3),
      O => plusOp(3)
    );
\counter_stg1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => plusOp(4)
    );
\counter_stg1[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF2000"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      I3 => \counter_stg1[5]_i_3_n_0\,
      I4 => data_out,
      O => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => plusOp(5)
    );
\counter_stg1[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => \counter_stg1[5]_i_3_n_0\
    );
\counter_stg1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(0),
      Q => \counter_stg1_reg__0\(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(1),
      Q => \counter_stg1_reg__0\(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(2),
      Q => \counter_stg1_reg__0\(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(3),
      Q => \counter_stg1_reg__0\(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(4),
      Q => \counter_stg1_reg__0\(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(5),
      Q => counter_stg1_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => eqOp
    );
\counter_stg2[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg2_reg(0),
      O => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_7\,
      Q => counter_stg2_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg2_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg2_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg2_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg2_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg2_reg[0]_i_2_n_4\,
      O(2) => \counter_stg2_reg[0]_i_2_n_5\,
      O(1) => \counter_stg2_reg[0]_i_2_n_6\,
      O(0) => \counter_stg2_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg2_reg(3 downto 1),
      S(0) => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_5\,
      Q => counter_stg2_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_4\,
      Q => counter_stg2_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_6\,
      Q => counter_stg2_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_5\,
      Q => counter_stg2_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_4\,
      Q => counter_stg2_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_7\,
      Q => counter_stg2_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg2_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg2_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[4]_i_1_n_4\,
      O(2) => \counter_stg2_reg[4]_i_1_n_5\,
      O(1) => \counter_stg2_reg[4]_i_1_n_6\,
      O(0) => \counter_stg2_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(7 downto 4)
    );
\counter_stg2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_6\,
      Q => counter_stg2_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_5\,
      Q => counter_stg2_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_4\,
      Q => counter_stg2_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_7\,
      Q => counter_stg2_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg2_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[8]_i_1_n_4\,
      O(2) => \counter_stg2_reg[8]_i_1_n_5\,
      O(1) => \counter_stg2_reg[8]_i_1_n_6\,
      O(0) => \counter_stg2_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(11 downto 8)
    );
\counter_stg2_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_6\,
      Q => counter_stg2_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \counter_stg3[0]_i_3_n_0\,
      I1 => \counter_stg3[0]_i_4_n_0\,
      I2 => counter_stg2_reg(0),
      I3 => \counter_stg1[5]_i_3_n_0\,
      O => counter_stg30
    );
\counter_stg3[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => \counter_stg3[0]_i_3_n_0\
    );
\counter_stg3[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(9),
      I1 => counter_stg2_reg(10),
      I2 => counter_stg2_reg(7),
      I3 => counter_stg2_reg(8),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => \counter_stg3[0]_i_4_n_0\
    );
\counter_stg3[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg3_reg(0),
      O => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_7\,
      Q => counter_stg3_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg3_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg3_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg3_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg3_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg3_reg[0]_i_2_n_4\,
      O(2) => \counter_stg3_reg[0]_i_2_n_5\,
      O(1) => \counter_stg3_reg[0]_i_2_n_6\,
      O(0) => \counter_stg3_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg3_reg(3 downto 1),
      S(0) => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_5\,
      Q => counter_stg3_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_4\,
      Q => counter_stg3_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_6\,
      Q => counter_stg3_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_5\,
      Q => counter_stg3_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_4\,
      Q => counter_stg3_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_7\,
      Q => counter_stg3_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg3_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg3_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[4]_i_1_n_4\,
      O(2) => \counter_stg3_reg[4]_i_1_n_5\,
      O(1) => \counter_stg3_reg[4]_i_1_n_6\,
      O(0) => \counter_stg3_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(7 downto 4)
    );
\counter_stg3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_6\,
      Q => counter_stg3_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_5\,
      Q => counter_stg3_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_4\,
      Q => counter_stg3_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_7\,
      Q => counter_stg3_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg3_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[8]_i_1_n_4\,
      O(2) => \counter_stg3_reg[8]_i_1_n_5\,
      O(1) => \counter_stg3_reg[8]_i_1_n_6\,
      O(0) => \counter_stg3_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(11 downto 8)
    );
\counter_stg3_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_6\,
      Q => counter_stg3_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
reset_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      O => reset0
    );
reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001000000000"
    )
        port map (
      I0 => counter_stg3_reg(9),
      I1 => counter_stg3_reg(10),
      I2 => counter_stg3_reg(7),
      I3 => counter_stg3_reg(8),
      I4 => counter_stg2_reg(0),
      I5 => counter_stg3_reg(11),
      O => reset_i_2_n_0
    );
reset_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => reset_i_4_n_0,
      I1 => reset_i_5_n_0,
      I2 => reset_i_6_n_0,
      O => reset_i_3_n_0
    );
reset_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => reset_i_4_n_0
    );
reset_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => counter_stg2_reg(10),
      I1 => counter_stg2_reg(9),
      I2 => counter_stg2_reg(8),
      I3 => counter_stg2_reg(7),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => reset_i_5_n_0
    );
reset_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => counter_stg3_reg(4),
      I1 => counter_stg3_reg(3),
      I2 => counter_stg3_reg(1),
      I3 => counter_stg3_reg(2),
      I4 => counter_stg3_reg(6),
      I5 => counter_stg3_reg(5),
      O => reset_i_6_n_0
    );
reset_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset0,
      Q => reset,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    reset : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets is
  signal pma_reset_pipe : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg : string;
  attribute async_reg of pma_reset_pipe : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \pma_reset_pipe_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[1]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[2]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[3]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[3]\ : label is "yes";
begin
  \out\(0) <= pma_reset_pipe(3);
\pma_reset_pipe_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset,
      Q => pma_reset_pipe(0)
    );
\pma_reset_pipe_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(0),
      PRE => reset,
      Q => pma_reset_pipe(1)
    );
\pma_reset_pipe_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(1),
      PRE => reset,
      Q => pma_reset_pipe(2)
    );
\pma_reset_pipe_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(2),
      PRE => reset,
      Q => pma_reset_pipe(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 is
  port (
    resetdone : out STD_LOGIC;
    resetdone_0 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 is
  signal data_out : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
resetdone_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => data_out,
      I1 => resetdone_0,
      O => resetdone
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxresetdone_s3 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 is
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"008F0080"
    )
        port map (
      I0 => Q(0),
      I1 => rxresetdone_s3,
      I2 => Q(1),
      I3 => Q(2),
      I4 => cplllock_sync,
      O => \FSM_sequential_rx_state_reg[1]\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg : in STD_LOGIC;
    reset_time_out_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    reset_time_out_reg_2 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[1]_0\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_0 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_1 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_1\ : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_2\ : in STD_LOGIC;
    time_out_wait_bypass_s3 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[3]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_3\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_2 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_3 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 is
  signal \FSM_sequential_rx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal data_valid_sync : STD_LOGIC;
  signal reset_time_out_i_2_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_3_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_4_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[0]_i_3\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_8\ : label is "soft_lutpair41";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEFEFEF"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_2\,
      I1 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(3),
      O => D(0)
    );
\FSM_sequential_rx_state[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => Q(3),
      I1 => reset_time_out_reg_2,
      I2 => data_valid_sync,
      I3 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[0]_i_3_n_0\
    );
\FSM_sequential_rx_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF24200400"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(3),
      I3 => Q(2),
      I4 => \FSM_sequential_rx_state[1]_i_2_n_0\,
      I5 => \FSM_sequential_rx_state_reg[1]_0\,
      O => D(1)
    );
\FSM_sequential_rx_state[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => data_valid_sync,
      I1 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[1]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]\,
      I1 => \FSM_sequential_rx_state[3]_i_4_n_0\,
      I2 => Q(0),
      I3 => reset_time_out_reg,
      I4 => \FSM_sequential_rx_state[3]_i_6_n_0\,
      I5 => \FSM_sequential_rx_state_reg[0]_0\,
      O => E(0)
    );
\FSM_sequential_rx_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFCCC0C4C4"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => Q(3),
      I2 => Q(1),
      I3 => \FSM_sequential_rx_state[3]_i_8_n_0\,
      I4 => Q(0),
      I5 => \FSM_sequential_rx_state_reg[3]\,
      O => D(2)
    );
\FSM_sequential_rx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAEFEA"
    )
        port map (
      I0 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I1 => \FSM_sequential_rx_state_reg[0]_1\,
      I2 => Q(2),
      I3 => \FSM_sequential_rx_state_reg[0]_3\,
      I4 => Q(0),
      I5 => Q(1),
      O => \FSM_sequential_rx_state[3]_i_4_n_0\
    );
\FSM_sequential_rx_state[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0CE20CCC"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => Q(3),
      I2 => data_valid_sync,
      I3 => Q(1),
      I4 => Q(0),
      O => \FSM_sequential_rx_state[3]_i_6_n_0\
    );
\FSM_sequential_rx_state[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_1,
      I1 => data_valid_sync,
      I2 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state[3]_i_8_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_out,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_valid_sync,
      R => '0'
    );
\reset_time_out_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEFFFFFEEEF0000"
    )
        port map (
      I0 => reset_time_out_i_2_n_0,
      I1 => reset_time_out_reg,
      I2 => reset_time_out_reg_0,
      I3 => Q(1),
      I4 => reset_time_out_reg_1,
      I5 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state_reg[1]\
    );
reset_time_out_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FF30E0E0FF30202"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_1\,
      I1 => Q(0),
      I2 => Q(1),
      I3 => data_valid_sync,
      I4 => Q(3),
      I5 => mmcm_lock_reclocked,
      O => reset_time_out_i_2_n_0
    );
rx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABA8"
    )
        port map (
      I0 => rx_fsm_reset_done_int,
      I1 => rx_fsm_reset_done_int_i_3_n_0,
      I2 => rx_fsm_reset_done_int_i_4_n_0,
      I3 => data_in,
      O => rx_fsm_reset_done_int_reg
    );
rx_fsm_reset_done_int_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00040000"
    )
        port map (
      I0 => Q(0),
      I1 => data_valid_sync,
      I2 => Q(2),
      I3 => reset_time_out_reg_2,
      I4 => rx_fsm_reset_done_int_reg_2,
      O => rx_fsm_reset_done_int
    );
rx_fsm_reset_done_int_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400040004040400"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_0,
      I1 => Q(3),
      I2 => Q(2),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_1,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_3_n_0
    );
rx_fsm_reset_done_int_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000808080008"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_3,
      I1 => Q(1),
      I2 => Q(0),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_2,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 is
  port (
    data_out : out STD_LOGIC;
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => status_vector(0),
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 is
  port (
    reset_time_out_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg_0 : in STD_LOGIC;
    reset_time_out : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_1\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_3\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_4\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_5\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_6\ : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 is
  signal \FSM_sequential_tx_state[3]_i_5_n_0\ : STD_LOGIC;
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal \reset_time_out_i_3__0_n_0\ : STD_LOGIC;
  signal \reset_time_out_i_4__0_n_0\ : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_tx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]\,
      I1 => \FSM_sequential_tx_state_reg[0]_0\,
      I2 => \FSM_sequential_tx_state[3]_i_5_n_0\,
      I3 => \FSM_sequential_tx_state_reg[0]_1\,
      I4 => \FSM_sequential_tx_state_reg[0]_2\,
      I5 => \FSM_sequential_tx_state_reg[0]_3\,
      O => E(0)
    );
\FSM_sequential_tx_state[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000F00008"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]_4\,
      I1 => \FSM_sequential_tx_state_reg[0]_5\,
      I2 => cplllock_sync,
      I3 => Q(2),
      I4 => Q(1),
      I5 => \FSM_sequential_tx_state_reg[0]_6\,
      O => \FSM_sequential_tx_state[3]_i_5_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
reset_time_out_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => reset_time_out_reg_0,
      I1 => \reset_time_out_i_3__0_n_0\,
      I2 => \reset_time_out_i_4__0_n_0\,
      I3 => reset_time_out,
      O => reset_time_out_reg
    );
\reset_time_out_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020002000F000200"
    )
        port map (
      I0 => cplllock_sync,
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(0),
      I4 => mmcm_lock_reclocked,
      I5 => Q(1),
      O => \reset_time_out_i_3__0_n_0\
    );
\reset_time_out_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0505FF040505F504"
    )
        port map (
      I0 => Q(1),
      I1 => reset_time_out_reg_1,
      I2 => Q(2),
      I3 => Q(0),
      I4 => Q(3),
      I5 => cplllock_sync,
      O => \reset_time_out_i_4__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
afmoZr9skqIZsiBY7liE8HhN5PT9cSTlE7b7FkfYEnmAz8/sYewHAx0YfvnPtVZDeaUQ1+SA/YSK
81BVyxiW6Q1+w7v5E1h+YJTA02rq79x+8OQbPa0mLIjdXTtNmx+BnF2Qrq3QGTPNoSFginwNNM6R
DHZuBkJnITzBkDYC/to=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
oFhR7iEHB5cYBaPdDcFYa+fK3Gj/00Z1GiQ4ZVkb1MGu+Q6dRi7Lgvhbwd39WT10uvi5m95gOjlX
gGa0fqW2XuRf5YGzXsxqVh0xVmshpf7o2GLRr2CmqpGBtEUVv4LbXJ4WiaEo9wVMVr2rBg6MtuPB
CQBdJcTka6nWZivdNblYGd53J9DObofIqJ9W/NUzFP/DmzVmGbctoa4amp6KUecoKxnO4tESoQzm
lz7s+Zslv3tCv5wVOwV3SAR+Y5ul9hQX8OljyF82Z31ipd+bzojhPg9RhmrvIMbFqzM9eU82pEIT
IZzSVKYMd1P6t1qORfwdgodKT0ZApoyFVxfVeQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
FJ5kCtzgKdagetGF6bdHAPKIu2cq7BcnQFRTWMN8j6rUq4ZaiydIPtRo6ybDhCzgm0ZRPth8Lgd5
+HXk3g/SdsTLZoOUuXnEz+TkfRJdu8kdJ+0y8sJMNK21IEW3pSWsj/n4toksSjLJ1wENuoq4XqDK
9VQo0Gobmx7OOqOYFuY=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
d/VWmC3TdLGZzbtN4vTxv6kYGkcDx9TNbvTz1ALcTDtGFLEUmm0Z0/puE1NDYgBTJquGDZ+j8d2H
/il9HfJ/2n/hTxPKUXm83KvLOZ3eaRdjaEk1wHy+UsjHJ1VztyOuT/nrMaT3sUq1kpP7wkC5I2EK
lQZbp4ngkuiSjWCad10Yoj0TX6DfE58qXjaybvWsINo7H6gMzi4JvYHWbBnCYQS/RkvaQvSsne0i
YvfJuYy63HVFs+Afh6hOO34nbqzXvlsXI7SIicsCUu21AgLUNio4IsL+Uj92NEPjaJnw2wTXrMuH
A1a3i03K7vrZ94AIVwtSXRwfN4q33BWP9bjYdQ==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3xYA7hcw+q7P0QSxtus9heaAij7kiNzMgPuCXFQTIhWPWN0atyWMkDqoxytTvKyCd0IM0n0DheIN
XngnbZmTQpitsZh5f/e8iD/+iWxl+7S7I6wyIivHhUA/yP87+KG/vFGPXu7WexVyPmZuOgSS7sS/
Fnk+QS1l3qltq84DH59g9EBTV7YkcgLEdICkCR0tgP3K8aSVjCptgXzA0onYGrkQESEdw8nPfEvR
g/5lKhHOmmE6kTh/XfO6eQpuQBKOlD7RaVWrPxnTkM9lxrjRZZb3OSfaBLMKhMGrOdEY4RIqfHMG
MBOH8gAUoIRrgyLztGTWc+RsxUUYHq9Hwixqkg==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RyOZOz+qJ5WH/47JnOQuMFPCUAyXetmhoeJiNflULv2xv5JJeXWSOURz6kOeJ5/Jbz2KA/psbOVw
rwZKAlTJoEVMq47lcrgzkIZPdllrnwbNl8hiziEb/YEq2vvpC9EJzY7LlRbQINiQY3hAzP5B/s+N
lYRpAUjOhEst2xZEiFz6cr7zAMz2jn7lHWYuvu75NGUv8JJ7PZ6vUISUeHOPW9ljH0TH4CnOaTpM
Mey1hosrMsp78mwEmy3l4n56Khrq2FOUzGZbi5lhqEebDEU/90LVUpUSzOkhJ03OfvAgBuj9ejqz
WgZRD6DpCk8BVUByKAGJqQZsJ0NcEnE8/btwhQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Q98gl70eQ6J1AYIClxRNKl48Tz7BoxVypGMCmvUy8HqLl3jjFsIYy9amcj+/0Qts3PcCHOutO4e+
ptR4cjDneD+41DiKHcGc2sgfdKq4hG60mda13C9qtjKCuBhg+Zg1jHi3TOh4bVtsLuydPMwaILNj
7ORZo/jzBQ4OUPNFy2tkOMJArdeTOIuydxhUFtpy6FAABlc1SE1Mer24GTLYw0pJtOOJOX61uJuc
ghuBuHaXV8kExCP3IU1qx/oyfIpY9TQiqLKEf0YUrmEzBzV+JXQqn3Jom9PXTay5YJ+PqraK9iFt
ai1YIDay5F0ncLDXiT7OMRFjFG2mL+C96DFEmg==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
DgXXbyNB3XMOH/gmmWb6uBYjn7XQuHgaTP3eav9Y+qTkta/ItNcuqONFgEZl6duKUkQI/YVDFLf5
JwZHmI7U9IACSpdRAsITpVXnabTJhDmcKIxi5anNhIeSW9RerBcxlMYtuoI5FgWJKz6h+0yZmes6
xYMyXTT3gJS5ttPc1n6kBHNZpxPgl/Yv979A0/ZxYkRTfuWrCyaS5T08AajGwOyC+2sH4FjL2xLX
uR9a04wQespOimJ+InC0FAfYhqUcjs5b18x0hSfPhe4G0xKNvmpukNOhIflD4bBgKXl0kqXnpatt
7ejKBpSWKRjk/M7rYOxErj8EahuRFH07pBR9b3XyXF3MfQBwAzV2GIz+A/XzIUDQOcBbojfGwrVA
2HhHfn8sKZB5ktpwF8MBjdlwrtn+kdQ8ux0ZNrgndrvw4nS4TWxZtQeRc+wfb7Q/lIzw6Pc+Ifnn
b7UJlJvNr5ZF2XNxuHGz5L19UYgB0gpH8xWvYpn0Od797z3FfYIiGMNP

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
YW19UgddTwwnJVcciGeEKvH9ZuBWuoo4LgtOhQvaoLIGVBAQAVfk5VY0pzvluZUn2+81fwbTPSLA
O+/RYs39A761jlViEBPYfmojlgjyiOTK9aHXNLsD3WBxfTXGOS3fHhhwgCmiZ0OncriPRHcNAK7N
4cyj3anKb5y4g9gzt9YeJ7T/zkLQuzT4zqnb9ijTisbH6HxpAVXI1ydKtGzVETw+s55Jb0gVXyGi
Q1hVe4oSDBTETiD85J2/A1KF+2DHKkzOJhr53qxvjND2l/LqsA0G/4J35IdLcmuYWAoCJS7cHkTk
wnGOjEnNbP+gboLu5UrXAFwXCo+jsZLVqIzGqg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
58apVjNnALRGpDI2bjJhSWJPQlj57Kat42daGg5JiM8yXDkjxm/BmzBRp9oBmOurwCnZOZcKlj3m
HlexbUqO32l40m0mMLkLzlGYPsduAUijMIb7DMl5HUf9XunE4fC69CyB0KyecsJZI9Ru4XxZrniR
CYqqjLtNjfFEppiRGRN6bXqv2T5Vgbh9qrMDIrkZnrIUB7RBd6xTPZ0xU1V6/f7Oftp0kyRv3C0z
HF7RFTnZ4y90YOsvuCM5CoAyyE/0gTXvBlB4icz61XmHsIlwfUu/xC6yAitX3dikrJ3DWeMC5Soe
vAifs/1pf/61AlzL055eu8EpDBQRH32ikamDmA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GkhzmJz9o93IJx+1EnrQfOwa/OOrgHQT2+XhwLsfJNRk9bxhLz472g/aTSOruRXBYVSa7twJJ6+r
lf0nvm4VQfQ9bG+fbYjOsryrSSKlth3/Bq54UcBz0wLrrf59Jt2KtKCOUQojHAIinG8CLxMBgQJR
wqwzAYtmffGsCYbobk0Y4IPkjQhtQamQd1BTI8ZwvF5zPN8kBYuoFLMFqwet6s+kcH/tQSrB0o/K
lHILn0b8uovehAHNWllP4phB4MeAOgMZbbsniJgqPQEhSd4nOUZs4WnWtYOsf+rCLliHTc9c6HIS
0u5f7mdvO+uC8Pi/nwNFr026nnYUX3YcG4zdBQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 119184)
`protect data_block
GBOmcsox0I5FPXJEerPEQrbT+oSPJp3R+j+IyDP4b1bfnvwnLbx0cXUVmFwl4wnPJ8ANkNxGb7RS
qOAbsaSIDtKUWBD7lS/tdywNTZqZvZbe/hCpvNvxV7F6B7Ge8uZJF3VBS1aI3UKzNiuESekvsOox
cLW5217OtIl7j6gkEKESYelT6Xiv22I3cje3QRcRujLdg9xcLmryIMzqUNgtjjsW+C8Rk9F0VtsJ
+EPIBhvR2/5RvFthQn7JiPY10PE4TtO2K2zkc7zKiHSPTYCl5C0E0ywSx7ZjBRjZ7hp0aZgDr3cZ
gEvgBM73xXVHBT5ZTEHASXPPko8dmnE7cbdDQ04KojbnUCcv5NKaAWJnLj784UUItkwJI9q4VeJx
8WmtKrhKDCukHxPVs2vxuxn5qWK2ZesSkazQa8RpCfMdDKZKflW+vigmDGC2mN4MSx5cuZOdEDzL
ChUv52ZkP4vui44w41O/9EUCnZah/x2e83K8Z2+kAf9f4KPaqRao7QzcTJYS3sqTuztwt1qvA7II
/txGTo6XjMeAs2zL3g0Uwm3G40i9DBg/qU7BBK6CvT1aI5CMSXZHkfcPNKfJ9cMo0I2uI6TG930Z
r40W4VgjgIVUIrtqSB1dCRXXkGNC8GMJ2hgIPqWCh8HuLjGjBteEbwvgofV1YvU1DX5MBAziF/wp
V/s8s3SyF5RZOZttJasvajFAEO4zk7qGuf5Y7O3pJbi4vzWmnb5INcvIF3eDY5kKgJhPFTMXFvs0
c4TedwLoA8IzT7lOWTm5jl0iVa7kPiLiZJsqx0b37Beoj7mxw9yVhSH+qm2ovpNUnO+wgYc347r2
xl3ALyJOCrk5VQO+5vui6fmkbVcl2yOmVqJGNSDSYtOt3FPp8qm/Sxf7dWbHjDAYbV/RExVc80i3
sIk8jQBpLGThwX1azLlWwqaoYpvMQ/4fbr6UEqgor3wqwRkSLGFrQl3MWPJkBD1Qbv8oEsVjFsoM
q+OTU8WFvkl5/H7mE0A8/CsccHonOaC0jMmifJ82yAEXns2AeDwkL/7EIgJG4PnuUz/xk0uJJN/b
lwS3Om69tgOD4HFnwyu/dMMBDV4oZvi/9YPYeopdG33/kitxWH2gmOkwNwIzSj4aNEVapMI1QDwq
9NPvNhGXvxRgKIVk6zOOMQkg21dHECEhy7Gu2aiWNkkUGUat4OF/dh+UC/8UV9+3qEmKo/uSOVH7
4xuVMFMBG5Ir4VMSOtXoUf5zDOrSVhNPSGuwJrNJwqZP28Ci7pQZNDEuoKt2Ae42F3ZNaudvFIHy
qjTa8QhaQkjpndiPzjF8CGn5UqgQTFiBb0+GI0R4JROOKCmR6Rcmk21ZK9vSU1z6g6XAP+fQHW1o
UaiODnFliVWqDAOEOg1wHFkcMHCjKHY3l03TaugKpJzX66z8h1IuiTm3gl7SQ5TgrVbhWl2XSr1f
qCxhIQINXbGCXe8c8dxzaq99TJEVPHt7wInAhn788AdMPjWO1E4jITfVRe/apwtfGjClHFBu7Nx1
ytz5SUPt9jHqkO63q4l1eKMZW72veQ3NNE/8U4iC19UpoqyuaewjVDtZxK0FENCJf68keXKT6sr0
+JVYUzsB7sjsSibJyr+n+pbxiQaBeH9yVp/aFTfgQg0PRggyh9DWqSA0NoQeI6/TnE899CezGnI1
WlGK88l6PE86RMYPIbVXRRpi9EA47mte2ENXvoiiEHZg1tQgCZclJHvh6UwiApL8d49poRX2toUx
P2SSJtuOXXsZnjqkcSoCUGd71WhtsxTWD6zV7R7qRtpo9XLWNpV8hOnUFi+je+Nj4eAeSECp3DBo
jesKUU3jr3mJ3mxIuD9jdE+xUhsqyHGNeecaq8vzyoB05zfRbsyz74YF7LvK9C4pCdhJ1wg+AYnB
7MoBjTSBXCcUZxSsdk1zTg9wrGE4SIGcM2hh9NuvmV/wvq9KLnvDWHtFT8Irp0E63nBjcMJsBN7F
oCn2vz8aA8QNPEfj2zoAE3wOlhOSx8DvcULorQusQAjf6Oz6nCRdYCXgGjUtU0CwncNEB4J5Dh8U
z+IsLKvbJ7b5xjW3xR5rJEmqojY2mJgnFpPoXNQfHaIxPxIKmAle7VIR6TpzOxXkqGElhOK1Yebs
jYVkpiaLubxo6zhafrBj0sa/Jjw4zFG8qAHCdHRJ4P7P5oN/5VgNXW0n3i9khRO2eJZz7bACb2cr
x3oBpQwN5PiVUvE6SxF8O5hgvm/RCk+TF/aN73PYwFdmmljQZ6IdhPNYvm+7fLxrkoD7x3J+exN2
zFURakmI4QGzYu2qYDOh+1ueu8Xsk8yUZ6gQca1uLR7KngLFtDQoFcLcz+zt3wC+b6PdNZBAMCjt
4b24nxqWAuY7MEWwpIllO/cvz31uDJBkxvEXhaKYT3reg0K5VfMjqnCj2XLrCeyFtND7mdzRaiw/
aEpg13qD8YCF6IFD31zUClwi8BAck3piQZs4W0ZUZrjf4LVs+nRZIZUk/NCKseS/YLdhR8tkPpri
JHRfRm/sVhUZ3hu9jGLrn0rtvKwL27FSTW6SbeXQ1b0asv5mWv6UG8xJxu+Xag9JS4RhWyvQQpT9
52hBPsrJaeLryvszdNipn2a4U9akF1HNIsmKSnLlkS31W3tuYUyP70b0lDtxMpw9NXfYvQsX14e9
vemKeGe8KcuFsOMnGbeCWk21SxxvbQwS3+6W+BAZ08hO/7oMBwhVCSPcg8j+u6aaUB+3ZCgOOiR/
9LBi/u+nVNGojuDDedTFxQtFXoIGZBng4WbAQT0geSYz/Xhya6EXV4FgO+LUEfR1eeAMoh/MNzu+
g909Rz2PKXWBGr57WRV2PEtkqP1vr2rTSp8ZSvQMxCc7H5BJIeJYOk5lV1rUz5nbSDwcWUUgoezM
B1kih4K/3ZkekxUuDfaT+lrG06dfsKZ+mqL+uDrGl2ySdPbWa0HkxIWmhgDBV7kGCHL8H7sIR1MJ
6C+Wctqr5YoVtzxtiW/Im+/Qg9NMc/I9wwlLcy5gM8T2tJKZ2lCwYkDw5+5NGwDbemExqce6hVXI
WebNM7dqbws239ChppvqUd+MMcd+xdVKUdgo9rgkwlSwnCOm3rY6zYrsYHlTORRnc/PygcFptItP
ZZDTp1kbpQ48kJWLMxNW9Fj95tTrk581+9MEUGcDA1GwMe2eJ/elFxBWrCAdQbged6zluiJIyNcM
6eiSpnCYrPxXOg85D22QkAxHUXXq3uaoicyoYFkTTXlxqrpDbitgqV+mj5NsvScfwXlZ3qVag7IB
J0q7AVJrxKzt+JNX3q3QnCy3ekdVP5cvECRaVipC1RxMR33u/UaV4unUkw/qeVFmLFtgQ2OIQLTi
tK6v+566pzCSqrYN14k4vfZgoCRgOzEU581UgoxLSI7tnDx4kRYvSc4Rv3/zPeLFBEZt2QN+gw7r
Oi8kcziAIVtjq4m6BQ4iM6t58N3W8xp0YK6EQrDK0EoF3ZLN6Hwnx+N+Pxw+Dndy1FkbGld3uZWR
HFiaYz1hCMiVSeqC7WI+w9lMmAS16tqONdtk2iYvgcLuJ0i/WAdSuqcXDFQLYUgR2YaYHlsEf1sQ
OhkNpCxJTY7XbFLoRm2QlkK9PVFQDO5dXgg1LYQGuNA/VPWmC+8DOutfbJJfS+Yki5PcWmNZEsLn
ZMmDORv7XOQsfYPly/Vco1C23KyFqVimQou6DEQQe/L6m64izi4PjDRuoXzjnjBBe4ZTLAfVINKr
Ea1H25nGu41IJNAPiGX8Otc73lTElnyEbafGfDYERm1fr1Z7vJ64phn1Lt8VaCrVod7xFF7zY+tv
UeL8o8mu0R+eeZs/6u5rbvo203gOoj5GNgwU17LNntQNG9DpmMzgKhjyXiHUOJ0KHNfJ55oCvVgQ
Bc0P3KLMDc2YbVHXonMqZ1No6kKEYsJ4QpkfSQLDkA6iE8C5AgfV2n+tpZS5yA+uf1AlPZttCnBS
Prx7kx+qsE6R6kFEJOd7o1fD/sZmLcT1LR/uARIgewj5bFDhjt2+j9/YCMmZrvIhKhTXkEDFazq9
7UfJylo6Ce2nEUYsZlJ5E22mM2yUWkHGNSSqK0mPmM4LdcLjHNsXB5lNO5khoCc44ZMiTnOJJvwd
3QXMh1CzVpeW8w5EaGC72L1r2AXuyFgQvrGMbcbdktU546g4dTVlaGjG/BVnYNiJZiD4VZkLg/gW
Ti+9CcpkNvJsM+lMTyAioDJ5UV/JQ7qfjmOmP3mxV/MXvw51uoB8Y38pbXiQvKC2PAx9LsR/rjOv
FtcKEANcD8GOijkt+7e1etDH++uOmBPVNCoQMp15TteaKlXSfJvenfFqER3Pa996WnIr7xqd5M1g
iGgw30nT8Wa48fdAQh5fXQecuEOy67sYT6xCtu7cxWGhi35h4jzv4+9gPND6kLMGXaByEQIfs5Hd
tGIagsktag1cJcN5z03i+B93BKil8plv8cEYLeolNzl+7t9dMUZVG5HCsSP+40by59R9LXbrzign
KtSyQD1Ec1uy+P7Y/Y4TrOnc0tCHDUv1k065bzzcoHqsmvsUngTn51hxVOdSFlBps214ars5z0n6
Dkf5mEI2UX9Uys5zcChPZA19zbaFy4AeL/iaxsh9KOi0bUtCbLzXurngQQdfWGSFWYu+1YahTNUz
EtcsFXgmVEx3aS0zLOo6V4HAvkIIhZL6ckibVIHVM+wHBAS0Pq3NMq4mmmTTef3UNzDMT69C95Cp
sTByy2jW/HHm6BNYr0o8I0Qjo+f+s50L8Wd8J5VdKTTWN8EBkt57s66ZhZ1fFVinAbDs2npZcWPu
V3K/QGkYkhx/c4MO8a9ZnqM2cY9SCjQu2yvCkTa4ZG0DoThxgSRw08dNjcqLydF0K0E9froLstxE
AdMURsuxZ5hnKaGdjKT0vZ3LMGr0EzZT2NDnQ8UEsBeNPwtskUuJIVfbzHZYRA0Iej9iFZBGNlbt
zTAQeSg6IkdE0pBgW6rOgtjKbJ+TCSCKl5XfqiYHZKTLVCgMQ46WJVhWe0StgHvrEMEq4+qqPmMG
k+US2vBXE+ovtX9fDiSuVEEr3phr/ia79n83nlBN3VHxl9qH1GO3Uju4BRYZXE+6yewS8Yq44qnd
TC/3WJID++u/yMKAmCTJtlJTA0vDpM6flRRxe5ebzolpz64OGvvmraOHfCvmGum0ymXIpjDwJjxK
74EIsq7DoYYIysp6p/DYqOoQIFGVO3SwyKHEY9lxNoYMDQifQkYcjArJXKwAkHJc5dRKdv5nvp1v
T/v4EzYFuTRj7wbQIhdUAlofgVCuD+x0umBhEwVFy80EzDxgcoWkE3So78QPOfo+k8a5OtB9pz9i
wJtMgzmY6K/uRQe1fFk0MiNCXDnHl0u+wrGEj1v+tn5TKigCxkCeIirXJaOvPyhU79VqMx0pcjC+
dGo6QyBO1AztERXbDsXimOYCSVOnH/Tjq2JFtMYv2mM0/nZ/VxEPdPC6Rh2PF6EIcyQY9G2Gord0
TcoFd+vHQ5zIgDTN6XCGKac+eyuovp4Xo7xhyZCjT5yyfOWdLKijNIqcTko/qNDOGbaa7bXFfn+E
8+VrSA2KEFykg8QhQsVe9KxOYwcmfD+CswCQnodl62WRNYpYuHpZ0JK+uhNZ8CTdEcnVHl+0kgYs
9y7NFSVFgDcwP0NI5fjNJU9/FduWAdSca1bJcjVqwPLZ0TBzRcbuJe4d2HXVS0N3PqTB1CB2a2eR
OfkET+hDnlR2mRt2m6h2Jp6tkkXs/DiLUNygJS+kemrZURXeGrRKO9s4cb6vdg2ZvnSvba8XAdKJ
7l/CeTPG4B8C89e7U0WT3THiI4nXAIQ/uii0/pMLb2LhCrvGlVzXgA001BF0CAHpabU2oh92AF1W
tgHMFnAgcYE9jI7LA/41m0MKqEEKeMIC+58waJz83oxQLu0r6PPf2c/IOWh4LCPK4g/MILUGSz4S
uHBG37sQRY1kLhFUnAg+4mwkWL6XDitcvzm3QsUlSeCmqa5x2UiHGLF69H1qCcUjP4QNN2mWQYGr
li3pnHtmaSy2R5Tq5+Y0TIJNdeQmge8I8EmyePDTvwY0aCos/SSXYGZxw2I3kFwFBwSNhabMTdXm
nfrm5ViTTcJwhwXAp2HoCEWEI9cBnJ/Qou98o/RSB9LDGBrHgZBVT22zIzUNSfBSrh7jnoEgDzlM
biXtIEV4EYzI+z3kgoiY+rW02BkvgZrY6eRQggvqvDXlctZ4QLLY0SdU/Ny+ujMQpSawlyvMDsxn
c3/s5X1NsYVXiXy9sCry0J6xcLmFlz8Fk///GaOnuzpZL4wS01IZO1LHOrbkIRVq6JX/pXVEZmC9
np3O7Asg5sqLL0K94bzAWIax5St/YQGQ7ky/JXRuAyigkXe5V3LWVs4gLpk2X/u6hv7O63kpYRjF
WEIYxB7VrBa+Oj0/bsrvIyjAa2XSwK+Ig6CbrAgO8mF9hF7ymx+1tAPinP9Rws/YRFwPqgdAIyH/
uIzVOJXDZjV+fILwaq4k3A5JCsQfBLgvi94HecOGorFaYZAEbombJpyvmSGJyl3eTNa0UIFip/bv
tfktiNP+ZEa7xQxT//ecrgQ6osz/w4VVIdtbfa5DiWPHvNI3fbktDjgUVGhI+qTXC2f1JMFdUO1r
TvyVRqBllFeP++oKim9bt06iD6NKUtRQwQXeafroE6D/XkeykyRYetZjOaCEfuwKAFvxzd2LkwVp
ToJMgS4SCh9PY7JtGdBtiS2Qnl5tMankN3OpjyswcgFrh5z+Uw+yGKxPIUbx3sECxqXQG0Ae9Gya
cY/bD1OavqI8FX7bawq/kntBTfk4Mb739FAwT06nt4OyJsov+bOcbi2Rp1hJENZO3P/DvCvvg5jR
dt6H4ocq/9HJx+xg90GE6A1qZWx6cij3a4myKP69eq4ood+HdnnyrG28YSDx9Wgtil/7WcPkQl4z
yjiutnTzdQwFtTbYnHmACfpN7c2OOuZYeWLqKb6wylmG7eBeALH4a9ChxlDdmswPnvM0D4Wb5PZq
5No8dNyVyF1VMzl43rWIw3DSLcqVG82WtdLRWXG0OyncFGBSQ4Ax8M4hZFT25xVfzyQrSth8EIDk
9usgIClYAW9JjbRM4KYsYlu4XOQekbZ/xrxat+KAZ5FoTdZXstVqiumGbzIOIdNdoPCSrnaNDxZL
riN/FOpRtirAnXVyoXRy3L+jsdWq1KBu4olkzficzZXtzNxIyp7AHcxbe5bApnz85AVBJ1eb+7m4
gTvbfpV4EFp6pHjkLSRALr4r8PK7IC9P7BjnBQwMkwFJ3teyYpHFvPSocOSRbLi96LOMcNOkbD1n
aPIR7vUkde9334k/aXUmCqXfmtK1pia4IcE5erjz0QnpuWqWDyA/DSCg/MjVSs8CrQkVEoJYVOSl
u07yszJrGjf+2adTvHuYN0J2gPtIpqGygxnnmf6naOW948UJlWFVsEvopO4C0qdEf9OpAsR0UONG
nCa+CjhlaFQWiSXptMC3ir5nFdTUm6yOExK7QcPOeB79trO4Lnxg0+g7HyxS1hEr7eBk3B2jj9ck
/h3z1OBpDjbB4Ngky/h65KnEZDeG6mdsjUgXfP2Dj0MWXluEZ+dbTBPBbSCBMB2xberni5EFmzzV
VL8isKbKo+DTBwnRvQQn/3YdRaiBxIPYcnO39C18XkHG3siRk8pQVZG38zcCFycZSQj3E09L0LuB
R5aFc0notYirUfKxbGFWD1aoNLASD9gHM/D+g5f8wsQ2Goste4DIjcXBITOf3mHI87gM65olrJeD
0uHp8mdmDDhIRSMOXqgFItbPFyjWczNCHjQHuT94XnS39pmdjbOlDq5C8EJrd6o6ggpGPlNdT75b
rp9KYityqArx2i8Gb1E4mniA72CFPT38DhEo5hNXW2tj4AqDT4Di4rOOLYLQ/CExhVyDJQ8A2PZL
vMOHbEdkG93m/sWrtOsPJQbmQjBc31QCsz+uCWX8OFjTGShaXhQC7EP+1utN2eytQgQChIAFaZY9
hu3Bt2xtfYp685f3fykhH2OEp979TS8O9ByB9QiEEQfwk7PRzLlUrbjFrrZIv2O/rhCNZIT1StTm
WRQ0jPoBiefc02nhlJLL+J9pnyJLeOFyFfBmTVyf2Nj0mEYCbMIl67zgUa7D2+1xv8W5DjV7yujS
RgFZcicvgTnk9UX9uCLTi4/I8YlQ5XfVhMrFP0KIo4T06vLzAF5nL10arXA7NRZjKc5E5uKX1hAA
Srtn1mubTSEXqGml3Q3UFbfLquf7bMhi/3xz22t/Q9+/BC5bXE7diR89UATyrenq27UDhJXS7oGd
ElsLFrbxxxYPRpvi7yFE86brjjDFRB3xpvZYe6XsZm7gHo1R7iWitgSj75p7iSs9va26zugG1KqU
qzAPjGVoma8pNP+lsIrAPPWG/lZPWCNxVbFjgcL+vw/Ew2405C9/tsZflIDyjmjtcfNZR5Gx1n5H
Y2eXisgAg4d5Epjye6ZJBosOdJd3/2GXh6UTHNzPw3deYprlBbz57UPKTVA1symiZvJz4ROAe3Cz
7VrYyEbUYZ8Y0vNI39TzyEnIq7toR0atwCVK8zRxU3V+6NK0lQ8HjtfoVfBi/bHiennko12+kwqh
bI3yx7jViXAuxuUh2tODOBjxcI8BwOC4CE3fEgyPPwU4UMDvJwFlYQoNkwhP2MLMC6UyFoasS3wD
NTtdWPsXM+OyTWEkmVJf9BPY62jPZzVAXBp4+PhP5G9fya4q0pju9tHwxpquAqreRkUW94a7yrIp
NGEO3diDI9cMMSX0kff6d8A7RmiX+xD1Ehf3mu5prz+78tuzzdNCgmWbkP79tCNHoVJlhaUS1Upo
O69qaZMBXNdcbNt1qYvbQNStzFfY/YfGmcXM6FkBM331Vim7L3ZQnFwj125/qtHBjbtMvhNu27Ci
//dXf+K7jyxM/dXjD/Hhh38ywm/GPBa9DF4u7MdKmBfGrrACxv61onujJDbVxjU2ylu4oBnof6zb
H0Y+81NAQLFZJt2whWnpW6bcNp+VY/nyr+ihg7yNV0orFBLvDooB/JOvH15I6HueZQIu7mAJZ7ad
s6gtWihezQMNfobIqGnbiX0+7q7WGKYuO5nPBTx8oU+RKQJbBbayOtkhaCQpxrE/XKAqONyKFM6i
5oTOTaARvemvwVUBbOJiV7SAgnwmhkUFJlhuK7e/bRu8vSkOO+LAaKirLBbrgItbYx0/X2RK9yEG
ArHEamrepFF0u+56mEhbY3GCwvqLq05aWukaXIQNX7F+3p8CWE6w8KZCXX6cEjUxXL7Ro9FYCg45
NzcxH8we4jg0yB2NOpjujzqpnaNNZUdygqPbJC+wwu64epgxUbNzEdzHZh0RcTOdPd+IbC/i3kyQ
a8dWFgHJdGm/Cekg5TvT01cvFSi1l556nIfoRpu8pnDI3JWUi54/TByFCuXPofYFRDShjwFzk5US
IFe+Uo/8V+QtALcx5M8O8SMS10dt0zaEJs1lNGQDZWvB//bfaYxZhRNYg35TSG8Wz+RKkB1ZhnHU
uHA1GMHdGaZf8XKY7xPvY+owq2KNHHyCJ7cq68M5dT4mYyUD96r7i5krSUBje9iqtpl0AuiRz0xE
sJiyMlbScDhb4Df9K5daqYNgh4f9EajEWddWtFWt4+JQEzGJ2qsqpnErBfGjopXt+Cyvn5ISTCHi
WTifLZGmHvdaFbXxPniB/SPqB43Z+iMc9WpL77wpnLvxIPiyRSx5myGiJEShcx2zmN4fccy+PEkD
lW/WsagiFVpTBUlfYmAb4EBtvvt87f4HgSnTD3y3E+/aEGkCItdgtIQ7b1HiUBaUdBS0CX4AA+8r
8cLdJJYLIskyuberxxtybSbr6V+amAEFbBKuFa3/F+S9De+26hJgXztPvwF6DHBq15XA1ZPMu2/4
Lb67MSAbhMd+J8aFUEe4p+cFpsbahDdHyOB7fV+KCI7LCK+ueboyueq2AwmUdgWIfm9+SZ4hrcgo
lLsPM1lgJcdjo6ZnMnhiWSQQJ604+HxMYlmPljjKue8iso7kkwTP6NAFYHuhLwNUNaGj09ygUJDX
IVxvxGLC/xoJYXQJuUSEpPRHx4VuDiYNDlU4M77kul+gQMwIGj8BU5J1L5Cb5eEgsRlrkPsvzy7/
EXtIlZm5BnRfrYwBGkw3RandnGW16ttcoDPgOJ5HU3EAlriXYONTBdWsZrlaDsNj61U4Vvpxx7TR
VWxfeYn9t+WzAqXwvxTTgTQRO2yHAgJphsYscG30rIVWV8WGmmTBdlSTJnhkgjrBRTAzyFoRbZxQ
gPMSEpMqLa1hvTH2iuRa16fBo3og9r4ff3udR7J5KnzGRlAwdiCD7lF7O3YPpCQov3l0RedF+N26
+mhSA9/onR6IgiH60CXgocOlMLWYP4QGMYrY/7t/SX0KrqQ1t0gYz6Q9qohxqb/uNBOAIBV/XylF
VySeoQIa1yvKix07mDeNUhwex5JTlT8yUxhyt2i2RC+Bi6NsjTmS5WI4LsdhIBkQ57dz86VxcKK9
N1Ph0XMXN4+ClHvFgeka73o2zPojUWCemuvtnEEtdthlDCDRgr6mqkL6hgsttIoJiATPaGyam5OM
BrgsCWjzeQwJUF1Sn5cl/+Jm2K9bWM2UQG2bZvwxE63max0Z/WuDdMPmmzy9zdfnlHqukDUQHlgA
onFuhH+v9Knw2H6cRCivSD6yN3pn17uisvGwvCmXKIFm2TLfDYAKoP37QQ8Q30meGhANVngrG/E8
eOOsAV09OiA/AKmUZ+L18KdW7Up1ydZ0v6shNivW0418ww4peQQlOqmOjtMZ6Hyy1Wvdg42kZBUF
XxmFqwttg1eekC4PLPvuh83x53rfx3/NOCJOq47kq07wObdWcKLVe/XFjeCVFBLsVoyQg3B/shAU
I/yDj7dbQLOENd52TozDajUfip8zKL7qY9uG4sCDRl9XeMrkgsp9p3RHqLekxePQAud3BViU8DEX
3lBzFU071woMB1RNG+StXAar1z+dDE6Oy1KE7kwjZAPuBy1HypcxoyYSR0YfPlDsAY/8a9axEg6q
AVcKwTgVbEOc9r9FJ3AvKMUpkSIqUP/faTBWDgKVGjuph1blxxNHIP3W9FBPgoyCkKtoz/6lnDLq
tSvLB98mp/0VD0gZN9vyXZLK/pp23sN9u6RNNyt+LnJ73GtBF6WvXQad49uUd7RaH33IRqC1LS0v
n5v4YFdcGsTar+4TPtdxegRoP1RRN2Xa7wcCAcAxeWrOLDYxp3JYz3/FFOu3fqL3b9y4hwywDT/h
BMkKATOICKFG3WXKBwZ0anwRIkF4t5pAU/dpIw9S0PSXCb1nLpr8k5e6f1Hk4WPZ7mcnjMCu8Knd
ngcO+D4vmE4OeXVF68Ms5+FXi/ZgXAQGjltLyZrUQieEjBuotR25w1oDxRgCtIxnOnR9mvwdfYhk
d3vd2tzpUnZ3+8K0Jd+gwKbZiSGn+UQ+DDGvKGBRQUemfCsIBN6T9p7PWKiST48hkOMoKWx/D86h
hCDNIkACFj0PhWQwZTYk6zjLevqR6by+JU/jfzeV113IQ6GgSwWHtunT02AkgUM18EtOyXgtGWLw
fs0vmrOU52IHK2MQB0iTv3Bnjh7K5VgjD3fZkkhfin0sLLoci7w0lsytMPutpvNwl8U9Tj7jHRHM
g0uGkiIMPDjj8KAsvYKUtwbfj1j8sC31PqhRN+w0SdUpIY8XG7C20mx/cSfywMX9Lfh72C94pHsT
P4ULlfn0cadjfucJ3sS3TrlAfZpeAnKcFDeFXwy/j/ZnUplghYpH47SlpPvraN9yLxvIBROfCOaB
Ch0eMCBBy+//XmkzIYXSThNEc35mPH3QUWVwoXlUMpYDCQvJSJfUNNbxEiZFWWCtn8dzEJh37yXv
79IJHKy8VpaDu1fAniYZzvsS9qB8ooW5tPxPmCA+rT5XTD42esN9qf+/uiafGWRYKW+j5N46QgEt
wpkrpT26TZbKuQ1TQBdr/O2Fd8PN50GrLopHKufDLZ7hgLnraS2kpDCBnMYLKVSzgMUwUOx8XBxd
CWGgfypW3RyWZshdoLZTsJ8QvPgM2g13FB5hrv4+ZmEEzSMGWxNlhKzGt6PBIGUVn+wQ8b2qf05G
B9a0bD1EsqrbqJZq6XBPlLR64kmOKoaJoDiiWjtO8NCjwDFWnzClRiFPDD05rpUuw9EZAvrcHhZK
Cdz8uw4sl+VdRF/R4TAMbaKFCnTS/5uRhCahRhQkXJwpWyn5OPky8zqajA9UyA5wZ1xgWBqxTj5N
JTUPPET+TVUgS0ZnW0G4sC8xTPH4QfGAH5FwkAM36fwLkaJh02rmafZIFWDUv3ISlje8gz1Zp2cM
2qzwRGsviwHeyu/dM5Pz24L2u4OwWR2MJUP7+/1meg1KZDlpiCuwW6GRH1zMZ8gAIMubFF7j6S9I
16Ux3poqgKqLa2kgNm9V+TV0WjwqBF3TZIKK6oLMD8cctm28LYefZgHvZuC8wKaIY7f16D4rSYMu
jOgHTtXMw0HeOFm3CNFZ74P5J8nMSwAF0Ll1n2uGV+Wn/Rmo8HxZq5ZVwBlMXbV7pqmorp+a04m1
PyUuDGJQIi7ZrimD+nLQ0mgUIDbzBuMXX3SWJmvOWZnCcMZ9/xa63MaP96NrRgEI1Q8FtbYYFHyF
4rB+EbLc5rUQylgN/3262JDH51DEsxN28XkAEcvlZr9waWp8/R5515FLEoqUtAzwC6zempORnJj+
PoZTlyLizK4/bzLcbyMFmbS3Yu6AKuemRGmQeaXW8KRboRHJI8BHoXAgMXztpt9xWiK/Jr2EP3J5
/UET6Z/G0rkss07bbLHkiUp1wEZ2vRSyLV4iZGCyDMjNbVFMKhGaP5BlMEdvGt3rELUjkUVPRLAA
9ruydeKto88xm/4KhP4ziqgIBEpvae8fn1ISPEV9pEIk+/PuRqEAf5cZw02IBPPNnk0u3776A+pM
uM6H30HUImUEEChB6jiRTC580/rZUEf8bU1OUP0ZzP6MAW2vTEGSEDHKBiP71JxVloYfaI4tSfcF
lVKUt6oHe/VefOGp7N/u4s9RxF2tTUtSuJZneSJG6b+LeVQnf7MIOhsJIHVzUuGEJfADD0zzlqSy
WdfCtNkNAn44YkFirNc4FCZBZxVi/ylR18vN1Mn8twc4/xW6k/pPSsfjsex6FzMSdQ22oHA1vspn
JXu+SagIBFV5RVlgbDQCGjpcPZb02OOFyQ5DcrUqJjDImTZxu+PjXhjaHbelmIENK4PdeVh54EZi
4+Cn7bdibsiwnlBI56udsHbrTkDAeb8JYvrtNF7GFklQJ6M9FG8K/0ItGVW8s8X9Kw20PvcjiV/k
GOmhngk2wT2sqRjR+VSZ5Em7bOgXntzJeH9/PFj1rqf7R2OI1IrEJGg9kcvID1BdX2TaFmVQuA06
9CmLNxLkk6+Trd5f54uUWf70H6bQOOsk5oRMq3/aCkmvBAQ7BVEEaI6+46UwhvxEQ4sPCTjj1Ji0
y8LOn0HSN2b2JTmMBODDX+ULkBLbj0/ZEx+YLwwD3YC6XNxLwBUbsJRMBZCludpU1jXD3K8PsZbK
EBnj57RhjZq/eZCEooz/8tCOXvKuFv4JbHEuWwm9ZR7jYTgzgb41HdhIzfzoRJbsRko0M7C4Cs7K
bm28l4BkOE33Pvqk9aXmKZM6zbDneG+T97qqL/95wBjHbLq+60hPCgIBjO0MyF+8ku/3cC+RLzK4
hLwU4wBaEUVSZdJHtGIIZs8ro+/40hDyQkev6f2j0i1j6BdH0+D2fKAmN3dd3wO13AMoBe6WXiUh
484Lt3xjjHnYRX6GPpd/M5yZyUrg8DzKKNCLK2LxsG95teLhdRL0PCDJJgSiqzRIW5R9qB0LN9sl
zciy4NiqH6OwDXZWSsPOe4XwtRm2v5tP1TJveRUrv712euFNq/olKqsVVvbjUzzlGFRDCOnbFVv3
Dq48s7+3pA1zWiJWttHrXlMZbvx3VkOQ9ezyFzK0VXVIlZyv0sgl8Ez3BIhIx9cIZL1zmKPYGgJt
4AfBViz2NOCb02kJPnkY41g3mSspVsZyoHICP4gT/L2vt/DM+MFBni6AeAksc03Jt95PmvWixg5S
P7xyD4SE0MvROopPhx8I5WmeLsumuFI73fLpLUzSu7W8/d2+y1D2jVjE0u8UD9USrgAKuLjFVbbc
Uqapp7fx9Dajzzyi3bSHxpkNfekfMTvgTjYN7b0S/JMlxGY7w4dPi8EVMNi81FNJG3sV/qoVrdE9
ga39VB1VLklWBPnIH7R1qdCUe7I5x/p9DfR01DqEKL7wnYmxZdZNfx10yPf8Hjx96IKykZVN+9vs
fQgzVtJ/YFge6A+sv9W05lFM6uSdZgtTPu8g3eA1xajsm/JaYVePknblb5ncExFD7EQWprauOBOC
pLpCMDyOs1iXtI5m1abkutM2U9NQh4gWFgkbC31Da1HWYN9k8tBYQjXdjJZOBlSfXYy0E3SaXJ2m
q32QR6n2jFJYNyHNzi5TGrbulbUDCXWeTCo8pfwbCX3wH/8uc9qei/+ZYCnsd5OAEriP65iZeSCv
xUE5ktWiWIhcPJfMnyQzko5eimQIqlIIU7528wqv84g2vzapPsBfjfRR49wtNRBIpiG6/b0qHs3p
EU7qYTCZFcFTzvmUORGKUg+IGk7m2OBRgLQ+PUcahNg3FanV1Ybjow/HBDzMwzWyNAIGcqnO6OeS
Y3AwCwaXWmR+6cFL9vzK+tLc6icjt774whT2aD39kMKTZkF8BNab9Keg89KfOVW9USzRKhW7uoKs
m7quuP4LNsevuyL9O73oYIRy47IlPBsRE5pOTmPKtmPOvL7OP0hcf07Yrh77KS1tB2uV0/SFgH59
aix8ccPZqkFgC149pwtRH21QjKdzLeFO5w0WVtrz2r6aSDwXt86upL1mNtG6WVSTc/c4pwNKkhZ7
LvTyg7aiu7MRG6gXLmZHQf/Nnk40VX5nMZV916lCXokh+IQEvbvLU+NIrOX0ZMNAM7nADniCkd9M
qxn/6rfl252tYdJkN9/lnsLwwvuVOAa/Y3JUckjbo06LbQkLlf/cMnUM9KFd5U2GVOVV8KEReAME
/J4ul6i1fJgz+nfH+Eams/SxPM0Xja2pVoRW0tqBvlmk4IvP0ZvEb08bxwAjeTSU4Kgqlc6rFatv
6nR4E85YL69jsbhfOVTOeDbD9Xc7z2INwA3P6qRS8GwDjwPdeeVWhZ9Ys5ZfLBkwwTy+H5Z/tzM+
wPStdpwKvREBnlPj3+NuPUODjIjd6sfViYectgb8O2jKI2qNcjk2cqN8XV4yQdR/DqKLkx4/tjTg
hzTzifVej5gqLG0CF+V1qlCEVcThFVe7tLGI4ItVuko5rmjb/uEgJj4V6jvW35Mp0jfKBh8mFW/6
FC89FAcfjwa+w49BF480FCcILFF3He9WkulGvQYmO721I6zzeziLHRiihad4omURDzrVetQHtpk1
HFXz0NtfTCUorQXEUZvQDFebi6r9qoHhdA5E02feCsRHD04XqbrE7Np7cubFg3c52W3hfCtqSgTV
697wOOzKIen3ISpPeWPs+Kwj96f+aAFzq2Ekv6OjH6Vk+NjwGPPEkTmeG/LRaL3Ty2edA42b/dtT
JTSu+G2nhXc4HqZalF+8Siawvt4GQiiEqo9YIW3wGnU2UFNs/wJGgaQdpJRWpmQg69yMDJVGs+1i
aWTyp8jf9gouRyRl81lMNa5VCW5lZktD+0Snp4wDkQqzrIqJ4BXKOrsu32RrjxAK/PcPsiKA3cLa
grDqw37Y2nnt+eUn+1pPUyePrtn/FXZMtP1lle4qFU7dNrUTrOxUeiOj5gWCyek8UAM2TQeIQ+Xy
LmnZnAOx1Ibcq0PvBIjjTP92bdYttxXMqqq1wlZlcd4uLbvgGHxhm8xalah60j4wj1qUyYT4WsjZ
+Bp982tQF6ppRYHw4aOD1F9mvZ0QjC4JHyoe+Oth092bYf6UBnOL4sPtkTBQAxqHnZUEvZZtC1H+
a6hdqr+is9BoiEG91CRibBxCY4ckeiUr2jZHjrGE6l5/rbJIxbdpONotaluVNemLKTWA6O0ejnE9
G6r8PmLYH121xLC2ofJSMmR+ze2zezrby8+3xeyCnSXYmxJ7BOXbeOrOLjbIOPfGOlCZdeRsoTI6
w+wR9NE2KeXb9B/2SzGc2n7iT4Fbk0VsD9QLXfoHwF+vrADZEwXn4RNHXbFT5nvPCHaBqkGQQB+F
XG+A8TT+pLxELZUkJoWPdmUw9pE/aX0na1DFhl5NXd8YcmlxHloPEr7BfZ9omxpfY3b6jZGurIj9
8TNt9Ikj568LV2ppq2L7AF093rGroweBH2Gq/q39h/sgMHDmnl+S0ekCwCzUdIGqH8IXdu360/KN
3eJquQ319AlTOuWAmxizwXRPbir9tQ2TrKcDjOPZXzIniugtEuPvBpJL08J5U/xya59FHCMwFk2j
4bHCoK+iJhmQQ3lxqowPvnSMyj4093OT3dd1GMW7YnAcaX+qQ1oUBO1gYgwuMZeX1h/RMnVEmc+C
YdMpAPTH4pqKzerNQ1j/cnzXbYi5kIvKLe/aNOrh1A3jV0JCtp0FGaYCOD0SkVphsDTUQHDlULWS
sEXXS8AHrW3yHCzMvLKJ2GiiDpAfZuRoDWCFFQoQl88DPd8uWlKASUKGmGeL8jpL9HKflVH9mITv
Wc+ewsdPAV86nBGl/+1TpzSpM4b4ueWIHIj8iA+xbYOnAH2pM1cU13Bsh70nws0K+Tyk0LqooD8e
UV09L/gZKqCQSO6xtfNBH1NA1xkW2ECCzqeOs02SUx2dwIXnWsL8q9zz6YJ8hEV0Cde7aWhf5OIA
9kKksVjNHYqRq6eRgiBSfoHKjF0BcUDHWkKAQtBVN/o/c5iQjbbIv+qn3g3uldpAZuLc7XsjnlSq
Ot84blg2/7XDCKFH9CEMUmZQ1Fi7T58EA0oOvXVDXR5oiOVnN9spemhz0vnSg6WdJikDa8ZXS4tf
B6RwrjKos9MqAuDgT69+XsqOxGLEpPvBTRyoqbEgQJHZqGqkV0A2ue2aC9YafqK8oaQmiiz6v9Hq
yEeuEvBwLuuuUpvEQQeCz+ub1FBqcidDkLcvIBgf0FHjk9OIdOKX/MiNevBs6zQQctRmi0ebh5Ub
FJsHY+7uQs4iwq4NCE4pXUJEByqxj7BY2eqzqwPTlZ4vqWu+brdjdLj+prvuLNJSIFNJKaNQygwq
JnAZRjBQhTiC83+WQdPz6u3LGJUuRsP2RrwCeEUf/9w5CF+HGV6JpLGGwKm59qwWzuWsi2lmwPMF
5aqydUz0Dl9bwD07EXoT3gIrKXBS0y9Y7fU/yhHfQGykHQbuyZloFJuQda/DwU+8kTONRUh2W92g
vpbySGRhqQIuXSbKxPzOXtIYXz4i1e1246zDl/oOBOFI0JOH7A45+KTnjf/n3Q9HNdkesZGoBRPQ
+shlieH7BMOCofd4JSweLndOPEtvQzdlYKocrt9YvVT3dGqVTrJZQvmGdq8G7FyDRTaqWOJ02qgs
Ch9uW4zKUoEg2IP2YRLNl7XfjyJIc3OAvR8ITglY4ZBPnTDDUtKyuGqGH+yPVbqKy3pvdXN7OBmK
I34GB6oOg5tgQZayre1fu05hLMeW8cAX526AOg4isD5JtEdnRgFpJ9EUwSLICwIT2Mqur4lOQCQZ
MmQeMzAeMvglMxHLGG60I58+qIS1BMUWTGzRyt1xhw6oalhvNWxzhaKM3j47bGLT4mvbbNG4svLZ
uryiUITqOHG8k+z6yt7lmg7/XRie3+EoSA78AFahw8mKtXisOQGj4vWlp6mn8xA81pg8u0kztQhl
GU1C7Va36GbpQykAvsbi6e7ntiJCrkzlqLTX/dm5pObgXjF0CcUquixgpKOUcpDoJhl8ppMBO7Sw
Et5xgC5ZjW8GQ6FsHCf0kZ8L91cT+TammgQOAsgf/qecKkQ7gvIKyKJPArifGOpnKE53Q6PMnna6
MJRFTh0coPkfxjrGxGgja0iApI/vmqA63PDJViM+mzTW9V+fgXXVBhbW3nOlkrkt6ZJvQ0xnXc1U
uje5twTH0QaCzDTfJJWa55vxJDE1N2f/bn6SoVb+tpqAsD1dosoyCZtLVdH0I82GkLX2Fg5LGE1K
2N0KmDUAGxTVbITl8WJGIH7RVdYFNn07u1GvMupO76mYjwpsFF6wARNA4+xNcSO6dES9GxrKEw17
CULyuzi+wFraToZaNQuB2U8mqsEcNB5/QgNnrpVYmt0QH+aA10P8DB31zCb9bq2NxESiOlJx+E/I
3yyNYin499eFjOtQU1m+4aeykqgpxAh0cLRuhmHzUb5mU807M7uI4zjy780W7raZUEZkrxb43JTF
/R8pojIgYFA++UdC4N4Kw++2drvkEIP0XfBwsmSc/1RRZZ4wQ5Fduggs8DIlEo9g2Cu/wONRW538
CV61soixMbyvBJCWuzcW8TWs3SgiAowUgLsZu7njOmoUerBwQPxcbZXxk4sJj7czW4Xn9JYFSzN9
+dCIlfJVMsuOp3/KyNRQzeiNozyN1sbZxRIOXzJ9w++DFoCw7ry9U4RGO/hf5z3rWy+Z6R9f4UpW
WffPYZWlD7iUmo3ttZdEF+eYAS9qU6bo8YE3G/XwsqQ8yi4Zf8Dtf6cmYjaLdaUwkz5Kehv81PQV
0FV7FnL8wTCcV4J68nW6+NWjjzXmzo4xbpHMxYpGhhk+JgvrT0lOG092xoUGIYXvKLV/C8EOFRPu
B3HXHiXafewzvox5HpRQBpiCUsTPtLSlLMQjLxzZ25WB61Lqboj2sN4sHMMIbvg/+t8rvcD1TmmJ
JGhX0KGEdodczrSedRbEhC5jIAwpIslW6zKA85l7Q5GkilEri68rTGZGmPMugxsShszaOg993Kb2
YF8+6jYekjJEPnW4SvCaJrQMltQIcrA6j5DJMFU+x6qSiJPwwu1/0GRCWk5rSeeXh3NQ0yqnhh0c
V5aAMBHyK0ZnlDRfL3UzsKwt0TUqcTpS7sgoiZ3bKdajJlJw/Tt9NhvbdjIhIkmVptsr2bMXAT1e
H0WXIsXHUAMGALEDjaMKIEdlhtvdOG6rVZuP9WC4aYhqlkPGuhpXjDQasNiaO9TtoZOzpKF6ZLgD
8BbjrzkpPVoaxVrPdgrbr6dCHsEK5UCRhzvOw5LDjD9S2vd3eFoPS0hV0nxi33Q3y8958fTD6pZQ
fsd2lA8dgcL3jC76KZ+qhVZKI7raRP+stprV4CnpzeZPSKEu9MhAEROoMJ5UFctT706nmGId1DjE
UiYg4ojJr3FPpzzlpY3OELmoyovU0GsXpiF7wDCVB/Na12DSRG5lEJhmfVl4g8ewgav3/+VnrgOr
MvDRva6E2y1+ks1fc530mTGNGKaRbdNU5cMt7fpEYItlky7Ch64z+oxRY43rJzzx4bgyDosTKBnP
2y80y8Vq/BBsDUUdrp7pYhX7F5WSxEvKh5CAl23upXcuxuDKBnDJZpzw3UPxT6V/1DNtg+M3qMXR
RkaAfMS6QSsZ4qZxpnrwCscqgLuQhcP+OjPQXDMSWCIQTg9S28yB2iUsDqNDmfEipqZJfQ3QCfyU
Aa0lXzAoCBMnstmg4f1PT9KQ4Ux6ScdHYPverbrv6GeAGMh+oD6XVdBe6Tg9uRUJNyBgIli3YIJo
rPYnVzYpbaRZhuXRW1x3gsc2g+67LSbqwwkVjMH0mAufEfq0qOW239dZ3DUmEX8VH+zeEHRyM0z6
Xd7UIT68KNGwZ5pO+ibNRTZ6qlVYldBjpxcU55rJkHwH+I6tsH2fegtOwIR4jKDxZjgdAzQXINiH
T32+Ncffrn9pp0pFMkE8c/a8wkxJ98j5MK2xbDgp1BbVPNPDUmCJ+Vd493VdrSMaC3Nuu+7lQXNe
0r5/dsN7ZEXzzWh4i0vq0ohUPWcd6xL9LbLOFIue1q+iRgDRRGyCQZQc2cBa5hKD93jsUxLMLbuH
Jw5gs9NG8Mw//aBNZJPmg4zjUq/eelu/3jrC8I3CTBgqyEpmtYJ/lXRwW1hGqIs/grVwqUVmvz1V
2eF5+czZb+IdySqqdRdljFDEYNDaKWPnCtDKNFFjyuHAem+xgxPOSdZmWTVkzDf2U17INTpbeKuq
Bm0AtrR3qTEpqr8AqG7DLc/nEHDHDXe2wdktVyKPPdOftcg8crFFSyz+NGIfHl8VHjaMBO8j7Xk3
UgiSPHGuOj/vjF59fYZ2X7h3SJqTs+0vVV2PdGdOuC0B+vx4VmTRg0VlF2BzIb6y+mHTH+WotjSi
MCjBjs2Kr2ytzwOf+WT3rtURiPMSTBk+PmOzsPvNdE07b4j1obtDQslwsoPcaboaJiNLpqZQ3D8t
L3dcBvu8Xv/fcWnANx4VgftcrKS2MqDRY751nnjrvqXQgjPLK9qu88As42qp2BNL2nnTpHfcfs8g
8tIPfs1u2dBqARG3GqKxooCdmwwM3hOn64YQAf9JXwh0y8SSH++ZgMQnT2B6KzVTvcYj4lzSS3he
yzfQS+ew5JqWMW2xiSyKmZO5lDRkaoEZw9QM/QlA5HOz1xX9wI5sUVF8JZOqz6om8uYr3B2o8s8K
P/E+Pxw9YDdeEXlvLG4rkTrCdfspUk+QqqqLokFso71P+otq+opDW9bMPmPO/3iflCdM9Hss96aI
4zR/7P1nkWBhr5MNHfpGlHG25ZmqIXKza8q31nL8BM7g4sbeYuS2ReSgPM/pkNswQzs1KtMgp3aK
9JyEBWQ9DM4OltL6L6PYL5MWWNqNnnn+BchTgQ9AaQSuvpvKwF71MkE7XESykXRroXEtCqlJRriz
LlnwAVzs0Sb3IE44UI/GeKUD9ErMNn2klmAzUIk0AcAgAcYe+6BdA7sxdqWCoeNzT8qTV+W6oGcp
cDfgIsM+Ci0wT7ZY6EAXMVMtetVEntWB4Fn7lh5Cep3dpoIi/s9hXfVBa1fPVs93tGhi9ceKjxLm
2PJ9VY8jkGqFTCcdnpQxjWlFVFGHmCJd8jmlqgwmWg4H9EdWtV7oB5zgYhaEaYvoG0/OdxN1INsu
x6rDclDkm/YaT/g09pnMdQGxcZhGwF8oI7utNMMe37SQQb6UMMk4M61HeUF6iew2IS+utEdJfc9f
N3RYKEKejyGmDcBZl+DNQqdKRqBKPlb1q4Pb77Cm7rHsX65Vb/Pqjs3incLWAnX0N8Ve6GxNI+Zc
+6URbvtTdMeWhc9WTr5C0CrCI3KTM3KTWShFcw22BPCmfbqbszmB5bUs/Pp6t4nmGs3wYkcUIrHu
or3/PtDeedD8NksyaU6lzsksP6A8D/uDYZq2+23+K2My4Ruz2JlfPgfoVk9hpP12spc2Y1AnOImn
Ud2CYHOdAEkBNK8pDNHk6yTMmp0qkL4mKmO9hPrKautksO1thfaQfuJ0zsgp2q+RS06fwcV2zGVS
LwtjA1s9WLHA487IrTyRGUlih8tmMmlYWdhTmaTJrQctz1hdCsMzpdEsqHsG8vVFWS6HoAf9k9AR
xnbs6tiHRN8vBuGVVmMXc3RZpI4AMz5axxWQsjfWLjdSL0WF7aRZMlus/OnGz2a9GWRbOfygIVJi
Eqh2VYmWgxMk1JKHjriFuBVyJ3f3Pdrz6FY1u+nFw4faya5t1khiidrPuhZdUKI3o4/weREWlRkt
xgIWtbE0TCMT3P+4CYqrdD/fAASBUIs1vXBdbnO6K0KAU99w2t3Hj/xYN6v6J0hRSygq3pauHaif
WYZH2CfB9OZkuXrqXVcuKgjTHUU91d1/4wJre6JwR2ltaseTX2vrM103UlFDREzXlbqT9Dz9vQ49
zygEfnyeSimhaVkBZUozb7hsoypZYDG27cpBGA1aHLjCBFJzzgh/kn+RfYpTz/ypAvb5im3e1ZNk
CdpTvH18yUVPeVghF8xwTGEI/UBMndoXHB9K04aBxams0snNivVCI4IwWV2NVasIIxNQIO6MAcBI
f8xQI3auQvCtKYhYobBXH78OOhih/8Oc2BIg3syB1FtejZ9RInpsFYfnBmi3CxInPRzq4Aywk+eE
Odl05VJvUqTajlJ2tPvILSntdUAAc8YJbHZgSN4RMg6bd8N6NfeoAtIr84kcvprrafqQasawS70I
DIYOONnh5NQ3gPZnFN0KfvFcLVcpGkjyxo7M+P5cKXupSEP504DJzaO0IVyH5y3mM+XJRwc86Fgx
BP9Om0E6SDQRNobTkB87cJE9VEnnYEck9ARDXlw/SWBTHSqsaC61FoNpJE70kksqVJWJsR4CQk5J
aaGTIAFs6CexFU5ItyxGDD712e5B+2/UQyWDIEusbkD1l4Qhid9suxKXKq324ATvrW5bCyRIH7CB
QrzXzONMBormdds9rgKeb65DqAbt8YQc/TqrYrFQ0eyru8nI5ogCToJNvRWh/DDlahULicWplEoo
Pz/DYQqI1tqX9CVhUlDoiVISoPYGPsRhKH+OVbuCVzCOEjpKVvFAXQJNe+6OeF14NaC5TlbIy5aB
aWLJrAmmID9BuQpruY7FMCLxjSKOMDB95EPzmsJooBgA5/mFI0ESPk0BK99xKwFjke7bvgN8rJi8
NV2dzBroSD8y26pdw2CJcxeMxLbaVYIOGvlCe2GAUGv3iDLsWlMu0x6+J7dJhWKBE6HAqjFucjg1
Zl0b28WfHiwtVZmYYeIs8ukqeFKInTs/IjkSn2isANeNR5JXlWgppoluZlye5VRmDtA3ibPeL1aC
cuLKr4bN5HvCxrvq5xpgFd7SCIgyO80V4269M0J7evMsYDp1TUTa/Q3gCACAlT8G63hGTU7QID1b
gcbXeng1xDIANYuWZRsEfxIv+cPtWYx4T5j+piny3/chwoXVnyMR7+cF/zoHvgICSbRQBu2ZkA5E
pAdgp0oJRGEJXWGOqDUCZzSwAFp1CHZdvHUrNEWqKePfMcoTryNPG3tECyyeyueqF0ettdpLvjuk
33nyaynhu9GK+I3UgUUt099/LrVliuazlg1Di5U6objeJbWlPYGvrWRcw7faO6Updzc+rVuQtq7Q
JGzxyddxrEP570mipHzAXugR/OvIn5aJ3mJh7HCCXNhQ1K79vQvL6O6KseDV6cRLJ5ta22xkV7o0
t7ayQLJ6qioN6HBLDYwgQs5ORG1xKrsB4jXhHKHONfe/JVZ/jXjWd+ZI4luTFXiZCxS5jWs/xEKN
7QEn5DtaJ7mEKQSdQCwmoQFcDt/1ZO75mcgBgiuHXfnYQMlekQ2VIFFPH3NRa5NrbZ1TnFLtirq2
e91kAEPzlxpHqfAgesZTMBzVXeGOe49qGAXlYUf0RHOjM2jQxE2vxtxl31NzMDRWkQ7INa/EOEV6
hf25rEp9UGeie/t1Fhnes6bqIm9DqtmbS9WQZh5NAJv90aCivryF6aw6+e6hinFVIYjlGFhenQVY
m5GR90CK+wZH8WDDphkxSpaFlOiHm+TsDfKM5llwcSXZX5nulEBkcs2J/IG3TeRYZk5UCoslbp8R
qAcO1tuwYvA35YmaY6F0fj4yYiwA1qKxVxqR+/Q+kwaXiYaHHYuRv+N9M+irUSiy7f8xk7WbndvT
Lyj0I8XbtbJvd2VsE1oi4KwEBiEhQJjln7tIV98oRGqHnoX9nMCOEdkcfNWMYoaH2AH56SabotOp
+D0WJSVGksffd8GLK2Vyoc3SCNrg1z5Oo5RuOV4DKInPt6W29F3tGcp1QUd80wh8LSP16CDYOSzk
d8XiU0Y5wu6zxIhY8eXLi6Y47aHIK9x5QbmYaaSLV8tSP/dwTDFKVFH/JZ8HLA43WYO9NMCtX6Ow
QhtLZDkQFgfLHOiHcRfE7Invp/w1r5QJJk7e+P90yMDqkVst4ilSpVMFy39PmGoPksx05ybz62Ob
HmQ2DfoZ20bHNmgUCUc2XoH2HFdI4bRooaVouvuLdMcZKe8tkNhIvW2DeVhk1tnbSYgI9kA9O40g
oh9oFoO5sNxLFC8ijg0mNBD7m8YiBDqXj+2aXKshvuX7paonw8m9P//FyJ2y8uBvezuGo0Wg7YDm
jt4JLQbIR5RSblKF1DIZT3yT3XnWdkaoBEfl1XQtwj/D7j7GwAQ/XkYM7ddBVLwWnaKA94tkvUV5
FK6eQY0nvp6DHAWH5Dks2M2D7EDUyWRbngPAGzgbrsycQv/LJwYiw4VsSptMADEwSh8NDDsfIfTX
0AKnXXQ4JQ+pSCoXw6uhH0xl8gi7OflATBne5EHW/mZR6ZJWnWX3qXGsrgPhgeCWRCpizRXKVZJE
57qjPgfn9dw/JTeYMru7qnr0fWJ5ej5z2LPI3oTAY8it5wggT7YwkT+vtLTlk43f12KgsY0NCJt1
niUd6aFDBFOo1ogIukDFiaWPFZ08+TwrSyIewyK0aEqpr+65keo4qJ0njAq0Bg5ALEjRne2I4qxF
SW6RdlPZ5yvtoOB+WyS5S8oT1Lg+hLQlrQl/OXmTuUXQZWjsYkURN5jQ+V/qoTE24LdbJKd4iqAA
HYPfixQLwBOhekZo6W+QRCwYwIcwRHdYNBS8M+huYI3vgrDAd08Kqa8qGW3ptqOuDuYnq37xySBz
36CI9UF0xGxGsErBGXGGBZ3xedQGmy8hSQKjaC0o1x6NKp2SwAe706AENMA4oNjFODAdeiRTp0cp
r3UiySKU5Apm8lZa/bFVi8vX6SrBjPmsqQWB6ycZXfasT6qLqMwcg7ht/598UT3HlcJuHbhxzoMo
4VB6TMCjTJTL4vdduKQpf55rAo8ZFVD83RAPeMcP0y/ZTB0OT3EhfqvJy098oRRocq1ySyLW/IUR
VwG1JHOHfsXfNm7sNwbiJDmAXhqcuQ7e1W5CzMRhtA1rwiInmCutDU5yhHfqJZHywVVBWWwF+KrO
MjOFbOgMDBOyStpwpk+c1jVJaFqOU95aCrjZrOGFBjVlxn41DaK9reF1XjF63P4Vko+GIB8Bg0Ic
KF0MxISqn+VVmQwHVbKBxtstb3hFVSkQ3T2SFupp7WmBxOOPxNplhPBRsRkKEbiE0eXzHbBxlHNP
6SzP1Vh0m44EtN6ai9UsAFKwYsy0Kdod2z6qylmv7VehSxWuPLsXqyPmrQNlhuK22d9LowzufGCM
/A5bSDif6TyA8VTHDmj1tpktpZNy33TelK1PaKKuSnpvQ4p7DZ/tRGzdZxuOUAMjf5XB0hh/0gVB
6DXlAhqvCdUvNQ1fy1WV3KUczTEVQIA0fwet2s2P8BLckYkMyP/DHErRfQwzcsqhWLzSj0e3RVs0
uMjbJ0mLc2X5a317mMbci4SWaJ5OJPfCNykIAQ93SfOn66qzatn8D7WTlM+iyKqMsPX3cqwgDHA7
YXRFpXO5WMAD6/PFBzGYoPO59X1gsdZzG2Bx4+jPS4W4hgS19HugLapWvMPhvRqcojhiHpQvM5Ph
CrGHCrCjN8rlSK2zn1oCZ3II8isyk1zlM2rAentzIhHvBx9lcuLu8+usoHGvycYUgCgyhO3jampn
G9y7hA4LOFgjz237kv2lOX0wLfHLh8j2cbkg8KW4Z0r4U3Bgi95FV0ZN844lem2cSwAOquFe88pd
5OX/6lvdY1c+8Ft6sQd7ItwqoDg7K49wW/SHbtpSavq1Bbwd273wzl8tkaK/lpJg/PIaf0cVWxUR
Ms6wJNRCkJ4VqPytiImYyFVAbBrn65UZNSkgvPyClrQ+zS5+N0DNmHJ1a4/Hb8FQlkQl/wawk7w7
lq3c9P3a+EDq3GYL3NuaueGqPGDtUOhhRArpu5knOQRl6/hbL5jXNJYHqsMOwh643PTmkvIMpnGy
1WDdJ/EuH0113wRhE/vIkEdUq4LF4ZGTGqVUMLSgxRYaeURJ3enExdNIjfG32sXPPth+HP53xx9U
oB6UUJt04r1XtNjcI6cO9c6qYtTYaxyJPnwWC0HGF1FCKGjRLmTc9TrQSjLPnZVkKQ0LAkK6psHi
IdHzlrc5clWLATMjtyn2ceS08uFwkpveE9e5kpDG+iiZmXHKmzvZcNWy9E+qTUrMGcrUMRmLT3qa
IMJ7DaNqbaHChbl9E9pZR2h1QjXbGpRENUC36KUte5ng5t70Ck4/kdV6SY35zjyLJkggBhQ/0QAR
LXQRNy0q/CDuaTcD6NoU+9yKW1p3kqDFZA4eUhp4rMaw3u7QflrHSBRYx9zYE5F39K1uBcJ98kEQ
+XGPAMDb1Z6gBfPMIeYhUdC5JbwG2zYIaVCiYo2hJFjrW+Ddlj6mC8xjlTWfz4SgoOuWyulMRJ8X
LzcLxFYfStisPBl3TNuOrTs1xN3qpwi6ACBNYCiBGfsoCETSGwR0GaRomZuY6P5xq6HmexI5/m8F
jz/Tb/idNsa3AojTRgXmYLzhDAnTVp97P2Hbrh7fHuXDfwCz+auIPVtwJYUygb/ezQehL/UaqKdD
y74CiSyqnaDs5ohY2hxwFN66q2zHmrEinqVH9g28n83Vycf9Ckq+TvJLBPNb0xL0V7Hv/JPsyvHG
Ri47JtXUAwnaVy8EwnMHb9ZjbAd9k469hYgX1RbYAmzfzi3vxY1AUaIDsYQXzcMU2mETKMZsueOR
FXUzGtkFEnKQIDsTTCOy6zFQv8SyBZmjtr/gh55Oy2mQSIGtx8wHxBsXHMCFQ+EfOa0DLStveXom
MQPAHdIXVg6eJgO1/Lt7P33ZpbxCp8CUpSKFdv0CIvjnKjwNpjfQQniLSRH2XJzYaqhNS920fdRJ
zywUc/WIw+8fYxnIjaJ7wWjnNKNe9bf3BqGIRqe1kHZbWw/S+U1CKABaBZ43xboGhTo2lpMxdCbU
tH+vVtoxfG/8SK+aqxgxdm+WJWPN5s6Hm9kkLjHC1omMLRdX6Yc2vWw70daYyVNXw7Rw3eZ3OXRl
N4gTF8P5p2Wo7vcq8saKoZApPN6PBX1eG4BqxPRSTZjqt12M+ZnMFfT2jupoqanpFTKnLAWY3SXg
jLHBxFF7o/hKxFH6e2wWzfRWlVn/b2Ies/HWrPde/TU5xsqbahkPxb9095IGTE0bdKUbNiZcbeaC
RcfwgQqjU21j+a7+VW+wx2/Y4XNzny5uLQfQeaXFK/t6656F17QolVicSAZ27vjo7RfQAuJoS8ab
eCO5WTDMfvZU6+8PbdIiLGvu1N2ciLDOU4xuciU0pBcv+4KwxlFuVmnU8F2jaQpqHn421bgy+xGu
vJF2kJUQOqRh8ifyr4nM/F13OEwmuRxaEH5hL5qTZIk5Gcbg5uVwPUAj0DsTpqUAHf7f500lCOqq
V8s92YHbpe4bc1E4zQWqM2nqFUhaeijGGKhXuwj5Dmc8SWa/R+AVmxmg2O5faMOTa2lYTHSEnfl8
4+rODSHlIK8Z4FifTqqxlBy2Ud3MUQB+ZblfI91FyKRXma9D5+ifu9POj5B0rJwHLhkxXW4GUnwE
5u9gvXPDJ7HTEPjOHNm/kZFbR9NrPDNXOOG/Nez81xeyu0gTzvRodzT5BUKkZ1z2mLRgsrxLswZw
/2mAlwO4TF6K3RkSbARN424+u6cEmXId/dTYHjE56tcxTxzaTjlzRXYykUyT/SVHgnAlCT404omK
3ik5d7isMdN/WsqvsGTMcAn2OfPXOGyR4VEw40BP/mv0LXdZnq48U/ZuRzV8uEQ/5qYSpPpEP5FB
UEzpn+tHhPHqCUM3wqKpxvvKG3AtkXJs/TeaXUHUL/woVui/VcYrHvguAfq9n8m7/NnXxWeFHZqi
APYxgVci4C81GjAoOfxHm0o4IEs1+IdbB2HNVbZAy0U4Qwxv5ZB5wr7OY/HS0i7cgXm68zzOnmm5
+841NhCOExBViIEoWdZV6mS1dKsYCYV+26BgCQwrut2MnPwJ0ccqzGs7Y3JNJrY3+kX3e3XO4gjW
rCBsVg7C7WOnZVSe1MikblLWG79vmHvSVncByjXo8DVFkwVu6v2Xp/uxFXgh6gi3HEZM5I1y5e/T
OCFar4980uqIU4rSsyt8dGku9foQ5HZ8A0qAwk2+KWh0wDmyVhdFev1sfzBnwGCNxDD8r1G0P/Zh
5Xp4ohV7Pp6g2NoKvVcpO3BfFeCFJuzV3L5iYV650eMafM3lEoFMtJMa57YAcaIRh3YvCvqmvhEW
X92Fe8h/JmbmGbrHuS+2jqwooZpl3osiV8FKdLbCcjQgjy/rHaUJW2AmhYIh9x5zYQSzV+Kmoqeb
O1f2vWn44+o01Vvrz8XEhkwEmwXHSxEE84+xpFsgj3kEmx3M3qs0Cfx2b7Y/+gifK0i6RWnlLNNz
uEmDc/Ay7zb0N9695+uIkuX9+8ZqWRtYnOrVj9YgnACFjCkD1BE8ogvSvsPZq2baT+gSEqJaPzCm
UwpSrMW50ImfOAExq90IFxUquJ11MPTZ52qnqU+sAseRAAXAUzovajr6qr1p+e5Aj6o8P8nOm13k
hPZHQo2vULzvZi72NaUYyW2q8vrTaNeEYJmUapr54cPQV5pMC5TyjEAki4ick0H5szsCuKd689Li
478s4wTYjbpEGu7yntYrHdtKyg/+HFaqqvQDxiAMbdTOo3Jq9kPMV7lRJocLhTsmUVO/2Z0TYBF9
7fsjt9exr0hyRjirKYuqz61uGn4/HJ2XUO/4iXyRpHxEN5hHDJ5PxbRsbbq46zq0p22xY0j9aDRP
lPRPs8oGBm/Uponuhh+6sPVFMicP0pQtyXpRp2T4lNuFXIU9tkUWNlWRnjMBqJ5zfUGX5Bp3qTuh
G0ff5cJCfcvMhYC2hDWYTxEJiQfvt0Q+EnPB9ArK2M1BcpI558geeaQFBiupHhgGvjEQI0uG6IBn
qwtCAdcaXoC2sQyOS0y6wAzdwxm+VQXUNaYlmtBcATSOt+wQ0SwhAUvSgXchf3EviktwM5XSjaN4
0lKNPplJ2Onb/pbWRmi6ULbOINfV57lp6Sm7bvGWj+DRg4YQqIHm7OIuAKmxrGmbO4P9F9S70jH5
ur/iDi1Wg3O0s7Q+G5sdtwFXsQuFeNO7sHkHGcAGuBgXWvrtUsIgwLwv4SJYCqk2KlWVe1g/XADA
8jxl/vkwf8dmxmccWUhvYo5I6xLIyGcjd2HnL8zzHMoy1DTla3EiNCs0bObxsAUBqC4eZCdNHhOx
eIGJoQoeb4ERhvw1arsuF5WTHyiyPn1T2bIPeEEE/oK3PSOPk06TGUjhrNNoqF/+sbFssj23Uphi
4835pjjEcvr/+QNkU1XP00VNDkd8+EBKwmuGc7TTGFgbefF2RNBtVi4mp0Umn+2XwCCriR3ilxMj
M87FgU9r3+9BNQVWg3oemXG+0t9wye+rgdWIV8rozeDRcwThtPVsFAkTvwk13ba1sdsDuuEe8L9P
3hjaoEfFoNRfuy9NogHGbBejAyMdFXfNd22ek3kPtOqGwzhpEIG4hH+SSkZD37P7ucR4IlacYxKP
0EbUTzl0F+5Hxx+LJaR/qxgpjH/YZnn+KwOBu7cOjkfclMCKbMnRmhviEaho51RFTE/Vf1yIenXP
qdCLN3yuCpC2DymO5F53TXkr7FnYfhy3H2rVh6Mh4ruu6CDSHQ6jIaMq+/NKAwkcRzqHHaK9I4Rv
+yvHKNNoGClHixDJ6K52HgX2uJPF23ogVZU5UFdmxb1EAGXfOPbrI1N7NBiadxESJCY+nzPcY7bh
YFoiA71U4BIwtgvnQm89vdLYKema4ntXpcdWkyiw5XWiMnly2BeoLUP058VZEr0FrA+CVFI1Oywq
SwyuzXbF5WA9Vn3vQGb4mBVBHRIaMVdssMHGSC9ia2d/0/T0Jdnkj+31rTPRuNk2KM1NlHDuxBTI
k2MJVC2wqAIgtQHuQE8pCnSzWxFTIwcObyHCWLWAdlpZnlgxjP/EEaav746mzOcAJ8ED/C58ULRZ
Y6Xc2Vo6HrNXQVkftU06wzs4KCqrgsRU9iFRXOlYVoXFPrtrNTtluAapor2GJjwYPvtUcjGWI047
6Jpjruq1YVvBu/jH9jgMriGMG6czp4JVRPAxgyvM1s3h/CocvoRUwVeW5SdCm8UXIw/H4rYGt5vg
JVBLf1FrSvTkv4sfDqTbmaRXi7q/r3H2/POIQ1tNAkja07EFgArhOhKLOztMMmXyD2a3qzBoX4CU
E0PCxDYuCBx7jHMNGfFDpSEgycXWuupgELqbDjA3YPUBfaY6suixHBBz8n3CtsvEETAdOcE0SQ9D
kkUoivGOwoXt9QpnOpL7yCOX4sGHTWH8cWGcsFmPpOB38eBqf8A53iIz0vn7F1Ule+7sY4LpA3ln
p92ALws/vvCw64mad5TM76d9tXSyXf3lxaqvx/E70VF4LuTFLgx4Btp8e92tb8x4JpC70eTZ8jaf
kbMaSXfdpVnIEP4SBZiSUUgZw4T6cKMhnE6KNepz8GJ7dcE9VByCSaEs5KCI/VIhHOZIN5nys1lm
gkkZLdHPgmSdzJ/zMg8bGcvw1qceXs8CfC/JEEGSa50dcd21wh8QrrykP2fMLi29nLqKhZzQlHTj
4j3f9gLGAaxeMfuQb/M5I7HZ9KpcbE2uhyzqWMJI7g8OLlRdw91AqloYBwxcr6yNwsLLdVd/mCqc
pqrJfFkQIoo3pQN+tf2vKosqok/Pr5fLs4YP8gsKiIqikGU4IDEGq/rh+9JtDYVS3nthsVnnDpHu
G3+0wiAb3tyTXDR1KfXETXou8f4JWA31M6VZHpbbOW/XALoD5iBp3u8hpWffJXCE2OCn2ppCMyi9
kAVre2nm9j5hnf/IpMkR3F/Neb1gs7Ki3iofj798VXQFcLq+zIsY1bv7Eq2k5YaZczi5A0fMtuA7
yjzixB5DYH6F8HqYXKFsfjC51BU07fqLygIfzXsdF5ro1K2NNxLuI3yc3+yR9llmxjNkQJdDgjHw
vT6hePq5xfKiGuFS88nNj8AMvQ1VB7cI4zAyIyYtsEDA9JsPZMdmUyh7wmYaB8YolndHoGwDbHzX
wMEA3NbUhKNDOM+J/dNljyqCXVgdeM3pzyVDVeyIckECRdWlLjvlQI8YqeXuDmdRpOUnyLfp07PI
mKAHwaCvZn2xYTgVjKySTuHgFn1Q2VlDxLAKJELdEbZ61a4U4hcQTu9VL6ylW5o4L/fT3I72lf46
dWzCKrr8MEm77OLXsQXSp7YOMyu/P61VYBWTg8yRCCCQLyaRK/FH40pM4ZbJjI+b5OR39Ztlv/9z
Y7GQUq96l7cEJLQw0lPmBoToAGDsWXNN2Dqfifpi1ozyGFkq9kj6VPAxb9LV9vAGGdy54E4ST7mr
q1RaetbnwiLpdJTin9NwzMV0sM+29rgAnN773yt30miu7LKWZPgWJiGgKVlYCwR/ZKEvV2DCWsJj
Wj7Q9VumJW7hvC/YAox6mXv+AZEElYaHoKzXCPrRal4lL08uaWbNqANjVu/x2eLHxS0kNkUPRNK7
tiadLv+sxoOF3zX3+cJXF46HLHAgYI/xHaUY5ONk+UJwQzIxEAS1UST5YZEiv6Nhw8oO9A5BMfDp
VHJmMMatlcd7AJDho38L4X2JzwdEwRH5yxA56xexva4xTHfeExksge+NNXZ1jFvLZ05ofHYrGlLq
IT/UB2iy6gdwciYr08OaavJDRAlEpIHL0b4ksTvhQ88YpdGSFjc91Aw0ePB/qwDoZAzIJNKZdjuH
x00tVwPRZhB7EDqWKLtFHbGZtYbH1E9XArLnFupe7W2suY5KJSYr6Yl5A/ki0rrkyKl2cV8pVONA
5NkDERH+ERw/aTqx1RNf3CMRyAD9JdkCL449/DBiOa6mAi04VTxkkRrIfsTqcRu6Tsgw91/ef2iQ
0XWKWpvkL7USLQaLSxTjRwpE2ec0U09yvEXwK5q/CizRf3QCPkvJKWK3DeSdJH6wII7AyuCFwbae
6MJPjPvy2ufRjs8LQa0xTg77dGTJpo1C7o1Z9SdTTDvmf2kBEaAvsPsILQ2MYqou0s5qzbQgx5IS
+nNgB6iB1U8dHHnMGsYWky/j0x1UJokhFKhIOC70adIHWdWMCmkdwLOuZZkEXt9nUmD96f5sljU8
2M3gpjMTdhjt19ipJSH6B30jF1ISrczN47CRsWXE74z1+9MpVnqqMvpiRp6JjGVtwj6ydXCWpgDV
pTlDhVZpI+wphe0IoT2wkCQ5SCj53rS3ULX5gmcWQRZR1ho5oPmrR7J4oXuUmAlqY6Xgw26tzDlu
PcM7Q/xerB6UjOWbVN9lU1ncMb9/iJtCQSYxe69WiCobaWpEAn5ue7x3IBmuqHfQ663bah8Js8nj
u9I6RL57Zk5tvOgBUzlDP/5wuRGQqpu9yq+qMlc25eONurSTZD/VMDjGkx/kNPKc6fD7J32t2CC1
WgS17lOryAZ3M6HRQUEKHTMdE3oh/n583Q8iFN5I59AvyV6/le0f7vDp6PXrLjYKR7cEIGdkce6V
RhWmmiTDHF0xPVgFASYDoKh/o8qO4nkaYplcPm5t2b9WF/s59287ujaPRK8HI+q7b3hGTVw7SgvC
hFdE47hzw8est4S4ET++JCauU42MDZPokzSwmSMGbe6iA5MEFXJy/4m8ImyMukhjx8Aus26aBu0i
Kh/RK9rg1g7K/Rx+UYUpnxoTTIXTsYkkBeZlPEs5Y/44EwN2t3NUfQIgTc65Qc2s56k91N9/Ftcm
RRnzrA/b1jmVQnCdmrIguF90C2xuLcgbKdzLtUOWvfcKG6D0Mh2fe4ZMxuzVkYCir1DLT6Kko1ab
az1XCi3Qiv5AAKLRhLigrDpjijJFDVxan5QOsZTDKFTP5WCFUzCXkxF4EKSrlcjVu+M53UHJAQAD
kE6/xqT9a2eZfASBC7cgjQCpcZOpEWMmhGqIvlAJA5NKjbSPukqVdBCMh8bFhjIoO5Xe0b6NQRHy
+zfbZd4FUlgl7jqiMPsjHm3GmFemwS+8o6i2BqG12HnAxGbEpTnYWLqxSYjZQ5bkl5y9t4nShsuq
Fb/LLaUbF8yw/k/yv8gvVyMJUwmI90uqsml1yigUUl+R2Nf7x0mKatWNTwhIn73+8Y43UfrOjRoo
u4LZrzCfpJxeBzdE/s3wLlEcfKuHE8gvtWcD2G38iEIGHIcuiHT6AftIMzlSuzSbyieAa34BHOHI
DCTAdVPdgEq0vJSUggYYcqXmhtBz+eqhDUKfS5sh/RCnCn9A3H/r5d5h0ous+XUJLJUgyHG8QEKD
yvnbZV7wMEhurHjLX8NtqVOzageYKYO31hkmVxwe6beu84wP1OKVPLrkAUmJMLTBMwq/IXGwsk7w
MKDYg/yv8pJ9/LwmOc+Vkgf587T7B9tP1l1L13IbsquCTXrBfL64ErL0y7vKQznK1/TDGSdqDL/r
Gp8iUE9jDGltw9Jk0PpVfeygKbSPIR6aZ53QRgYaRT9I6biN8WRpTDA6l5xdihiEOyG2BFnndIpq
H5DQBMMnpEQDVvrO9EFVbXWAMRx6hcVBfeG2V0IFS4alggATXqmTfUAG0rBh2bt/dWjk8vB3wwwM
P4o7W+rIHm6rcKaMYMIkGU1O5vzxpp18R7Aiz6Dr7tvo0F6OorcFE7qcs4p23gVixGhVh7WxUWl2
ll8WM8n4RsUT6OIztk+jUOR9/lqb56fYYGCSVqNhJpFj6VILzbA8JrIgq+xt8WtI3pUc7HM/JbA/
PwNa0ykefUyE4yML/DxTQY+1eGuRVel1jdoy4AsDvUzJzPFiQn1Gem6qHWvgIx/5gFXX7p7bnV6u
IlkTQJBEWmCpXJejWuCHX1hkoNjkHN8LCo/qjfZrRGF2t5mtxqHa2HH3t9vkCgD3hn5tgkxcHFcN
fe6mdYu6trYpG/YbjU4/rA54BYJWutHKnF4oWXFS0PrmtRRDnAQpkLAqdt/ICXAdZmTHE3F2IuRJ
mD5fHS/ssx2rebCUrJndRXzllP5DFqCMIcAz97rkIPLuuy88HS5iJ+NjzVlufo9MWdQY22UR4c6i
/7sSnfmWGACOXGTFAVKIdyRdKDh2mL1pHp9l7DB6mNaA2pqgmGEke7aSTxVM3kZax4jFvwcdv5fh
Gr9Ayf3fPAOpQ2fNUpo4aBM0hkFG84xxztAIuP8PO3WX36K/jV0P891Rdd2cLiMlfpiAiN3jPQwn
laIuzIXUELZajffMLtB7Eljv7oXYzIcqu2CzQRYz99SzY08GSkDSuJ4Pd/8j5zB0hVwcKI62akey
hEHXegvwDVhYbvMCZFcyyHWTirT4YjSde8EGjiVpr1ALKT7DGjhRPnyyJm8PieTIn5bOzOUrQ4Ky
9MIX+0eab4ngSVl9pj39Us+QEiI4QhUtFydTvgom4v1S9DwuIlBZh0xTXUrHo009B0Q6icaOfZfj
aR5GqVMtMmhg60+ATwlhdR4d+KV/RfH5kMVBhI/vBnxurlxjbXqVJDEb01S33p8yLhRsEb1GV7Sf
EQeYtFGActtWzQz8XCxxnaypwU9kaq4cIIjBABQfrGWGRC+yl12Wt8vaDYoClaEHvlZJtTCVqNUy
yXMHNwpfAt4/QXyMq8CEiAiR1br6Zs1Knkc2gD8YhiH8AFHSoB8vyTpQ1jmM++kca9l6zV5g1GIx
XkoQx4AjYpMR3ICaFotAGECw5WGiFxmDWn3ia/XwA3rFxCN0aA9ZgADv0lD2zcFEHCq80GLygd6P
6VB8fdzigPnBm/krsoOl19ebmqZfLraDSVdlTjUWtqc600wODaYG5/38bkFtCRpR4Q/MhnEQw6yS
YUxiTElTfpnDkhMp81peObe5JyaqAzREl4U/VApwRCkigbN3y/bpYMTJyHn5U3dLHAzD/9ynu34L
ulizOCEJ8EBZaE2asXqLxdcZAROdb8UwPBqqUCdAv4C1b/XIPAtnY/s6pOS9/3/4D+XzS0pATTf+
ia1GnoXxyYTvYf386xIuM0VroPEx+0y3pF7FvAj4mqZQ3MovnzZkV3ymnSlBacCyeaxQz27Z8GgJ
KHZrtHY/49Vc5wak1/mZ329/J1JWHstVPIcYknB/heQXKDmZKSosu58A3YbY9+kgXVa9kfu+0R7t
NMVc40bl6N78QZgZKMrXIAnV5vGtd/MJc/fjItgxQnqZ6jvZyFMTXkYSH1hJdrNoTv0tBk7VJJn9
G2hG76nSPFCg18iQCjwncONVwM4vJPCe5t+xhQiTUsDwcJiqnMtPvHdoI43DiS/YbJv7F/u6G9vZ
tvJZKW9WffYURJnNfzLs0HOb5ZZk1sFP7zf+If2yb/eZHd++mk6eNFdSJQ243lBU5qcLiWe0sPl4
KbE7q0OirRAYPoMYeRYs9lDa+qrLzIgC7emweCe3ZbgbmNTlNxURKDKhoEgwWNATWopW+JZBrMPK
hDEG5gFtWD0m0f8ttkSBTQvSEqix05OOJEW1drUPuAPksA49gwAkHN19nC9Q6RBQftqeoAE9qEDM
QIBHlVVrtoVyyXKG9BcuZ1oxvbHGSkYxX9orEkmV+SrCLF1O2/tudt9JnstAnm49ZWEeLXaCQzjZ
puTHou/SH+lmXO6u/0Vjo4gxwmFKg1Zg+0XuljygtnjpE23xuwDQk8OtRyZyYpqedNfYwkQ4AY/b
D7z0BmIfN2Q/xSz5Fn9GLWh7mgsqoG6xT5YXfy41IweauIzj7eFgmvMMKH6zaplmBcRwaRf0hM0U
CVv+5TflCgPTQIhBSv3UwitPrQM3UPfVOznQzqD8EUxj/0Mq8Hb2yJBdu1BHMmpxSMVaNbB+0A/s
7M7m5bI3iysvoFVIi6Gd4MMDe4HVSNehLF/Il55XQpt5n4QaP3FfArw0iagSuc133/WSKpWEjtx0
OWLoGsV0CtRNbShxGXDVTfdo4NimY8aQ00jGPpsnA8cOOBERaTmrC/HSQvHQqOODfdwE9rRhdJYR
XZvo/IUaMoghYp/64A+KJ6mQmpBjG2TZ/pao1sHIeEv0rlmPLkI+M4sxd87bkLJAHaiSPvYJUWPN
aZVT2+IWpYEMGUmhGHZo9kcGUqbsRjFcISlLz7WhDyOppDpKZCHwQko5w8QHZB/yW+SJFRvFuSrN
ZAceQ9uytkqiYkF5g82EsyzFGBovQqGqe7kC5WXyZHimS1giKZI+SP4EIPd/NdVDsAUUUUka4CeY
BKnU9CrEP2s/4BDVoY3j4zHxqlVxi6b+nxUWgr8cCTwwyqEPRJac0sVDc8FKobOPjZQbW6CTZF3K
ahL9UABggIUHP8RJHbviyw2h7FYs4OcxeCoDdog0jqyzzjM/8Qi7ryKJkDRQutBTQHhbB67IolJ1
QnR/Z33VoagKiRYSivKWC7WREWcLWoSkAknKz2uAp93clVYnZaCb8G8ShV+tDp+UfF1MhTf1Gydp
rjWR9P4IM37mhbPIqDjQmipAA/qZFTfosDNEW7G9IiH8pcaudPQgGA8ax9XYR72woBz67nTsKXyV
HeIY+fNdZsf2VoEogW7WUg1IYiJSZB8wuzq29N6H9HkMyeOyvFZqeCGyrjWWyT2M38jQQCb61bc4
Nzw6Dq+QP121i5QBOZchwUGNRo30VgPV1beJGjc4WXgHg0RB3T+UQgZW1Dpeb3BokhAgY3ljhrW2
IfHkVUPk0oZxb8iFg2ylmH2DEVPRrnjSJrV8zCRtTqpVKiqSbdha3Oq+Q7yvqNudRvfYFarwHVf9
if68iIcjTavjol+yahjgHEDzRHUOXl8ux6DcphTx8Ty84KeYK1nVDb3MJk2HVwIjCUdHFdryuHT6
kLhCEPaQe7RprPIRoC1Y79mvYRD7K9F9fq0VNQ0BbfNbE4/p6O3pzJOVisFzJfrLdBRd89bbuCfe
G5aGSKLkyZwpg7Ilot8haJxv+zainCQoYtNIYzDo61BmkJH9HtfJZnifTaM3gLnbfxI4YAEdWd8x
GdSqzkojZKo20e8k+qbgsRP5ww7p+m4SO1V00ntu7wq13R0dMd+2Nr13VKlVYzTkOVo/xX8SqWIH
CvL+DGGPzVrRXIjeNq3S3mOFVOHiYsxbQrbX8jfNX/Ar40W+T8BtHF0M786KnUN7SCdFnRLRhVLw
q4qZLmoXHpl8jgzaFa1s287hKXhq5wDFxHatOYkO1VnqjiXAB0Kpr1VvUxPAmulajxfKDduGWYp1
heCeei3HkZxOB64fsneeR2de7hqY7NufStQ+GXJGgp8uC4Qbdl0SDYJN4CNmn+YkZnADJ1PoCh/E
insbCYPtPHdZTKPVNHBuBNBEaFb5vGJ14a0cyFDrlC0iEiVHrcTVynijRbeVG/egiM/9RNFA4dqc
UY5mByektLky7SXrKYjRYUcjBkv9g3eXK3IjwYjJPqquVFc2cvTxfh7n3WniI4M93epDhx3/JR8q
Adv+qLTPPIsJt2381PqucahitKqCkNn8HOpJ4XNNiHbPcpMpi9kZmoVcJKOwA8iRvM0Qw7jCDXcR
edgR04Sf/TqxsCEl+Hq+anuIPC3bFuiCPbcPXXUp/g1BNnLwMJgk/RBVM8YZqysR9QQLQfGAYNMi
o5CvXFJxojb1SI65ctENUbfYM2tM/uWzKk17160p+1woHMgEtoTlfBfknGOuPjzXxxiblL1oA3le
OsjhDJ07L5k2tHax14vVSoFDWxP5Y6fU9upy2UKtGqWnOML8XN96WXR8zAwl0i6PipFq1AlT/wHx
xyeeV34prwXHs7TsK+mzBiVBxqqo/yGbPbCwFY0qucEiyYALeSte5cIUHKHAcN91BRXyiMh0CbnM
w91r99V0mzPqRA3hiEYBTcNwaDjYOrYrbEH8wh0v7/TcNzA1/917pB2n6vf5WyS/vWI9/pVOwvCn
dpEVcmv0UeEncjEzH5gKYblAlzzxYZWWITwUGar1i9a0WXQLEbV+EVsH7hGa7rT5xzhsQ0u96Muc
8T22itFlzOmOVAmj1A6qQXg53h+/tQ0cBZuMVTsHvdKlDCJr2UyO9zt8zFY4QytKrJF8Sft7reee
S3UKKT3VS0/GQyhTVIsRk90VIYM0Gb1QfBR4tFYKK4dMA4J/clQHKCNr/WL75j3FOFgFTKnuEHpW
7nG/45bGVteCrkF3XifX2CMSR85n62I5FV046JoVn02x/ZLsTkD0gtkKROzkvnVI2RbOJbTuQ8pN
RELBtkP2c+youHnwpotsG/bYYDdOkdQb7eTl+eAbTMQ9nJfEenuK6uxll5rN+GYlE/Jv4+jv43DI
Yoh2xZazGdfS+irOxbGpI8xwMpxjAWHAl6JjnUTo2KTH25M65OR0dA8Tjd71c2tKdem+weMEImzf
/hLT/gOwi+XgiuJQcNyJLV4NVp9E9vgJvRE7pqG5yk1lNMUaOQHGMsJQKjuKjGKF6EBIhvDjzarN
Utg2zjVoafQhbu184C2z0WmHloCE2VHWGFpxSrdAqbKWx6jFFPKqfa+k0ghptsELtDhZsV8Rm0go
Et2RDM+1Bwr2rOYqisoEA3aPQu3MVBU0oBuwDRm5nyVyWv47CfOIhcGHUGbPyDi+PbHRQ8oYKKbm
4R3BsBkTk0k4ofIwvu952qrz5AGVfmPTxnwTg7tUxRifh8ljV1YOMJpQmvNH407jmMRRo8suMu97
q7PJA+XS3NAfm20baP8//7g6r7v4wfEDGdHN87JHwkfMzyL/WMX2Ds+1UoTKpegg0vzhgc8nnVO3
cQKOIjkataDBJ08iX3T+kyV5t7k6OnVPV5jo0hcwvsM2iALB9T27F4kgJyXJ1N/FRmbUaa2OExlK
X3zg0jb9joYtfbUJM2Egmkzr+euLbHRoMRDx5H82AjA7ywmNU1mKCBGPlfC+QBmUN0mzWAVsIeOi
wWrd9pTWnWX71SQJUlTWzNCr/l60gQGl6c5+eYvS56x5hF139lrOh7Acc565q+1sKdVKNhg8Ea0l
VoxApOZwsquTeExUZHh7PhbfGsiDK2YMgea3kkyqfYgHL0+9fasf9rMXM53tLUdzNVyUFt/D8KyL
tG7dsaGkCyLXJ+moDQ6x9sbGCxOOuhxtK9/iFppVvtzvL4TisTYhCS+qz850EzVFLEQMphIDQ9Uc
wHxL4npbL65s10EAdta6+29sSC3J2tWAdqjkGXbpZzxy6r0R5kkOI1Q+SzOmC3mr8vQZmLCEpW5B
w14tge/0F7f0GDnECw4lTwajzWz7ubtDHxyhf6cvHaSkLsxdElCplcCXS1XEba0t35ojI7uqv96z
06CA/E66rWcb9r1xGm+Z0OwSw6SZrGX9XlJR6Ot71lUItUr+gqzYi6ElLTKVfkJKNAks33fGUK0y
UTzAqqWwKB9TvvDDnPqFqn1BXWCzjphUXLeltXOoLof6QJhnQgOGhR08aCf/84B+Cb4mco5A/81W
lLlyfS4NeriDmnDZXXk5OflYw/DiCEi4M3ueq+5XxsrelLqPS473WZrdgyCPKuM9ElGJgk1CW6cZ
/MUv3x7M9n3hiq8FTafiVfvz2n31FB+zZ2EdV5sGz2lrifwOFUaznV7PqmGEgAjCDoQerJeA/0lP
SSTceJ8jaoMP2yBZlgh9LPTSM4jkJk7aXdPI1wvkCq7mrMcSkBU/QvvmLEf6WZU1lXuv7jObvf7h
TKEthVW0cNcBe8IcpiEXdDFIoxV7QLK7ddedsooP50RcAySMB5U/iWspNHC7SrvboX3pqknZ8heR
hAdVkFA6pYvloEHNUg5oKXy3vx3dKbsIGajkgbTVdyMZmaEgeKzecGYEq1H9+/v42QgcbiDiZOsD
K7T5HY7un6gsnWhBFj6gKHNyWiK3cHHS508PRRTRbn590ZvXer90DCqqwmbTi2ua0qJ2smf5umJO
zSsxbGCOUJu6Jg7Tj9vfM2lSDLck/c71IVZwMZtVL0aYNv+TvkKro0PHoMzI7ERGHhYhz9UggrpW
OIpAb9ckjl4qsN0ULj202rcQzol2DBI5GWjjCTFvH8xmB1ftoM3ATVeeDDzcXh3Vb4fgXYO089tY
vuUdnZwnpCc8XURngXcOTamoKoGCDJ5XUhqyYkLtesanIPWPwYy0D2h1qVXYtyqHILhtUoVkGa5L
+bfDpROFRT9vfZBKPlbj0hwGB7H4nf22jkrp1SY8zwH+7IF62jxnDszCzLTOWd1yXZW/TzwsiYW7
22zlmMSUctKp287rTguNT7BcbaOvS6qMnmz9LbwmteWXJ63IlyPw0qcEUeRMhE8pRYnx4hC8Cgfi
a9q7qoPRqEzuxW8RFIAoKK4boi94IAk9VBNWUB2gCtEbjaBALLM+pPF62Y5r4qOisGtaDsl7XKVx
FEEtjMpAWJPAVlqrvzrbqV52dEDve/xo+97wIx1gdJi87r1YeRDacQkIRJl69Bmzx+yB9iWtR3S9
aEp0rEnYzVX8zjiU6t2mG/Oj/ixt5PXzhpo84XlmhrGmC7LnBu0O9EIU29UexIm7mvbyL+Yevs+X
u10rbnkoEIIKOg6/Z63E83Y4nGeQjF0JzazCnRLnoO+eHne05GFNpB1SBzNiyejKTUkhZ5pbF0yt
nP0B6icX2mfjcPBKMP9XN22KcsCWzqjfl139k9QXXpF+IpwysdkDwfhIIWt4TjgbTq6IsnnEaF3a
X9gK8hU6VPddDouVyr2cuVPTnr9V8ZsK0j0wwiz7wqCf4HW8LG5KEGs2W9VCI2w/lc+6VJfX6UQ/
bvSc/a6KcvyFJ8BsLRPaIA6W80ZICPrlNFN2fk8HaGWqTgeQmw4U41aKHx2ox5y0JIMudAro6Knr
Iay3UtcttGNk/rk6W2XLtD5fKZoH/pJ3QYJbeOzCNz44/MUjijcU4KZQNoa7VR2Zol5+/ioPezt2
mvY7/PFVPGziyKG7QpA2lNN71pg+VBmwGpaVm1eUJKRVC5Fbxi+AdIeh87jQ/+jzeKdBC/9vVxxc
PjPIcG+hSIoXj11nQfaGaOAY935CYy7PoJCZHhWuJkyKXvR3sqZMxZvp2+ovzAZdM1bEzOnu/YAR
M6hLlxKDaWNSshBFLPmdnAlokh90wPwhjR5IDoHr8sd76uyIwoPOtzhifHRstesaWq+NLOWhDt7o
1EXYbskUwBDjEtxzPXBJM2fdgCzFfSjasIaarBCaKHxPaOMdrg151BNCt7OfwdpvpLn+HIzfLkYA
W2YMFejAzPK9d84f12cv5qQCnRhwn6lqwSFw1bWh+ACR5FaO3TvCz8Kc314KX1NiCTjSkOtKFnS1
P/BGKU3ub1W66qpYQpGwrBf2RibFn2+rcg6EpcF2BupCaM8YKebjx4VT9Q3qWgMWrRvDXmYjmBtK
dMWEXmU9f/J1xlamHGgjG31LaPYU5YOz5skxdtoeI0aiymT43Gmlhl7lHes9uJ5B1QhCn5bg+faz
494FOmBufAY7I04noEy/odrKsydhrRiKTDAppj+Nf0MQjcNPqI4hfQx+i+pwfndy6ZED9ckNVzKj
pcTZBvWf3oTtS5PlcV1rlXn/nFGs/KHf7CCPuAikewjWkYSkrLr84hNwksc1qFrlb7DwQ3tLMHqV
eAJtFHDiemoEPMso0ZKbsmzKHV6/EaKPFAb1qB/z5KS5uoaO4Ekj6wBl4a4eirByoX2PVi13s6rc
QOk9sRYQ+1upkvnd3OKV3onscOydpeUwcm0j9a2yHrhwLIeCP8E1BPqNWTCjma20YzyBgVkx3Kh4
eD5zIvFuC4YM42orp24ViEkbhkWIU5SeMlFfeKT+eT+g/tJqjgoDh1XvOf+ufMvJCr377qTSBeQC
+fIoYoiNkkAHTjMiZ4tv4QZyWaaIVAmGM8/v9FcQXAL3JqMRnabCqRIlazJrnA/aEF/zBEK19Esm
rPtSSuXAPVS3psXQKoMX2Pkg9vPz8wX4WHk4lKtIvuUGXhOF4X8phLUbK9hOKp2J/Fkk9KFMOtqe
6qmGcHY2C+TEnGKF26VLfRsiG73Phwakgk5J5m1u1Id6ON9qP6EfzRUm4YW6DFTTHq8NhP+9Vb+j
SxAc4YTza58oyTdn9V1GVQgikZiaIYWzoPFdPUkQWcz4uXna1zdgFaeSoBItXGbRFYY648tacAkX
qkm7asxlgf2bA6Pgbj5SxlUNH0H57DduDiRCy5fm1Subazd3DSNs2C0q9uFZXl4/ufLs7QMpIHhP
sQxQXXCV+rSk4CbeYtDPEzVou0RwSIEY6eIDopxk2AKyQEMEZHsrsmAC9rONkgw9yc8qMGf+8lGy
um7+MaWxTtZ/CPzvV31+nBGj9FJWoTzpYb+pbT7l+Q7hDBTn+yoQcUsN2TT/rcuKC2Pxma35kc2J
iUtoReHcZHCG5ROgdHC4svxDGgbS+dljhRouGlUibBsp3SlcQ/ENH414w916lDVSm1b0o2Xv2DfU
SCfgTA3po6FeAbJKDZtfcs5cF55qqdtWGoSuFYYvsXAmXE0zhbglC/fJmZ5AOZ57BF5sY/joy4B4
j6pJg+KXdLRSwznqVQdKo9jGCOhclpioBsl7BStW3inTbhLZaLYHalyvq7nDCraeZ+dyVE3dLJgC
YD8RTG/mRj5X08nfiwKamNfsnx4qphKtXBV9MMd2syWLPNICJe0iCJttHzUCb8yq2hrucGAuGuzP
F17dXzUOVvzAiRnLm2DrxAEDsn4lFCTaRmHsD6X9Z2S6tlTZeIYXOY9kIq9bCsBdDSpAT2GOyLid
xMJwNqpYU/7mUwppmsWlbmJvmBBLYFlKPKkKWj9USA+2YTPFLoAc2MYrbGCH9igAOFf6lh4iIm4C
M95FXBjGg4Osf76Zj1dyuhI6XE1PLzyofLjJ91YWJq1gEUUaP4z6/IyxQqurHYdcdmv7HfACUyWl
cwPRaF7ACSdl8VfVbA8otEIGchYXZmIir4rPGScY3rAh8U8HEDWWv9BrtVuiHBMxWoiSoe6tlXW8
SLXKCFzyajCGQcKdMTiS3TPQYUzWsFPEqR1OpwvJ047LxE0tZkaHrzFGZ2S83umxVya6ZDSBhYTx
eY+abdiZzUiub4hM90YpD3CmMwadCpZaliP6r0h2llnSK12ZtRkIxEh6ozFweGH/rza7+QmAUCvw
oWJUKDRbGWUfhLO6vlfZ66+Oe+G8QgSkHZO+0r6yLmb/xhbkN3jUs+vXFXWMhWR0GWv7P4PGYMg7
U8b47I1EAt55/1Mg4hsucZFb08BRiP2ebMwAQTBz793hMzRpcoxTWieQGM9WSsfjnMu2k8gz94ap
1iSHhuKUsXNYT9WWUgMEYz+DEc944aX+38+mAS1LkosyS5f+irUljwYgmT/LZDAyX/M/g6Sz0mDZ
bxYpKHVHh3Ca41wZNbtedav3aQou9ttCpeqOcOReSpXdd2ylWASWx7sXcLylR7iu0bZBVEMyvBMm
Et0Y7oT/K0jwn7ds0JwEPpQ293aMMdlkiIFfu/YuavndaBUODSEkajyJInCX5/xR573AX9zN7gVu
JeKsrNugls4s8Jg3cHnk4vNFt0Nn/dYdAGPq28hmto2Tqabxtzw+E2hZHD2bdoVprVl6pua1gura
ygFHafkQsJ8n5+ZsinRIckFegQx1Q3gw7SgG8/11Eo7sxgZD0w5Zg25p5YEcyB6XjKHqDocGVBBR
y3s5S0wIdHk2EtdBLYkDD6dZffOqcDkZptjPA5HAbcO4XiodB3HU2QQpN9uHOiRaUc55AiwnZ3Ap
y7MKiQZh+t4jMzhHBHRCGb7O5WgR1hwWgzs6CrNTOyIEWxPKjNzncFgJT3SPxIJM3IAmjaHjV4Qc
TV+GhXCUwU6pSHv5GUY7VZIdbrnXuDK/oQz7F6+7Qsll+rS4Zff2K6lmBw1PcjRk1E3QfU9TautO
8stj0RK2Jut/kCojW4Ra9PlbEGj7B49c4mvB9rsjNBIwmeU3KcyjwXucMMVh0EfwFzsJ459aNxB/
stqwac0gEVLw0Fumii2U4O5cmLqMnniYFiT/1e01mF8sbCtAHMrWb+ciJZLxYeDvpKuXqxqSV0Fd
jnwGDxx2LBkxh0uXZ/DJeKtvoedoFWM3LpVErra1+Qc1/lyynBYMiw2cxrRYVyBY5ffhhSwJYCkX
YfDqWq+83+l39LFImtmZTx3ib2jc13ZyULMOyZbGXlKbAOwFiNiZ5sTr9/WSw83YHNE31Ip9MtXT
14EHYPH0dxa08hdyJtac586v1WW1LKYsPRnyTNVD8+2q42sHZGu04le3s4HkOL1dseGKndKvorC+
c0OY5zkOFAwd2PWK9nxyODsV9PJvl9wbNKTYBTuR/r0Nu7JDHgWysvXPS2tkKL2GnwPvSIIoBRz/
SkGzNJpVKx9qN6PMWEekw3MIkeCxLcqaoDuF1+gZK1NutK5DjSWqVn9ef4iiIP2M3kX85lhtUxu2
rsGQgssXdIubfG+stVB0/CT+OphXskditwvzkEk0G2znP3Jb2c44v0I5B9JN28DA0HaJDmCqZSuH
c8iZfZ3vDZC7VAmS+M2Jvy0EKGSt3P33PgXDaQNO9wwnmxl+MTh0Li0bUrrPQcWCNIgDtjxYd1DE
lqXUzlY9Wx7oQP4KOAHiD6kWGiLsMJdCi9JMf2IaVrFC3zHxqANwmOXyNmkMYbrc8oCtXHo0ABql
CWGPyJWEbVraGNBcImR4I0F+ZJoUdGFwu48SQCM5PN3P/FXGyEejYIfPo5PEG44vrnG1hf+NsNMw
yZ65Cr7LzjCgKeov18EerpefLLDi1olvu4mFWZVqjfSXq3BcZCtwj1+CUVDN8mDkdvTu7ei4oHBw
tpAgAewQDQCM18kg0RZXjDC5SBSIvk0HjUsGG/bqEM+6JNgZX47I7xalU7TECOVh5SgHMfjJIiE0
JW+X49HK95HhfC61isTusEjyh+wZz9nV+ARWniRt/m6ICJOdRXYeDUm8qrvg3duzGtDCF8T8gqZS
p+RJt4sqO9bG+CPtw0tTMTuXqJVlBqGXEAvSGZsW6W7OrLzl6z01HDztxeVx1PNimo9KN0/hx6lD
Q4olxKheVrbUBhdaal8naJMyzrMAscBomUBkc/i2Q8RYCQPIW5CsFyxqPPd5khCd4wGlbL5fv3js
xXnIQAG114ik5DkDOXN6H1hNfddSrM9XcfSlmpyQaSvP7CUK+T48h+wqc92k7cud4OvhfFEOpPnQ
4x7qQUw0W9AcJuocWK5gcv81H7JKJp8pI4+j2MlCMAlM6di5N6CmnUXjiKDtNfNnpHbspaCKypIo
440839hV3wBRmd3HOq6rEYQC/56yK0rcHcZMX5FoLup00PAmBx4g3D/d8IrWm6kwHRPOIbCuUvTf
WwL9G6ngiDcsUMiDxxsNsOQ8H2Zdyr5XICZcXlX3kFwwXmsYwQhE9e1wvkMb9LAby+U4FYvbSB6y
rT1viWefCk3m4KRCIahws+LCCfPMHnlcRSWXOVL/opQyW7CnRXSbroWK31t5FG5o3JC4BmBLQV6s
1/1h9HgKuGz9SMyw0/iFRnPwRZD5AbumyFj0iGYD2SIBFoFTlVsiTCEL/kdCNN+kMMwv5szZyRnr
nbvctt+XqOWhaEoY0erQxV9RkCyp3Nd7WLxL6BYbbfHRWKo0bm0N2KgXRFJd5fOMIlwUBQNe70va
WN4ijYFcnVS4nmt7VEADqmYyoMaQcF0eKBEC/N7n0ooBQyrxBkSwWkyrmlalQiwSGcN/oqrUAeSc
8mKf1mX4qMFeyLpYbgOC4jn9J6HxxacEr6PMEkPb/4XZ2zDxdYX2x2KXfIP3oLrSN9fFtR1gscUq
DlX4HDit92Lc1nF15m3gLWf8zDxVNOwGmlXNxTt/h4KBhpjCa1Pwm0vR29y2jiq7yOj15mtNn1QR
Hk9odgdZ5x6z2MzYx7zpmhAdOSrwWqj3iv7+DTmQxBirXAIqT8hsVbjgAko/MYCh46Mkd2OFB1H+
oa0MrbrF+0oM9kCg7m1ScYA1pP6I9y39vnh0FYgsqCUrfZTb//AofH2SYSpNKr89JAJu4k05G3nm
Qzp4/0GX5BbvuK4bxFp4NxR0dLD1VKwLwa+UPNFH3gqKRLiR1Ibx9Pm7xdUuBBr1RmsHoP1b7nuN
BKNHfYxT7PC2WmxqACITSfRef48+qUdH3lWfycUC2jQNU6I7vkPp+oOL0NUpB1OzShoN4RpO9z33
2mP7auG7iR1IrgOo9xefVaJU8R3Er1Rww7r/Ga7hu374cTMmPoHMlTxz7iWEpiLMRxW1J6tKckBf
6+9rFSgGffNlaX2TCL0CQ7s8CdoDG3rBzrPk3bBS4gNz5h3YsWa6iOnJ6y2PnM/uafRLlbV7L8Mc
wie+vnOqFNCxNy8zlbVJN2pL59CxDz+MGyhB1wyGInzlDK4GoBaiOFUPTzk9jj12yMSlz63lJ2Hz
FM/IPQkAmJiBAe2ad4bbH2OPO4EkRrv2x4JZg2bQUvUjD8ATvtXdhBc1bqJOwIYaagOtVST9Okrw
WJtV6uixh/DG/MGqY0E+BQgtTIMqCayMLkSpVmlc9RVg1yrPPy5QFcJLkCpREGwCbx4Q2LAT6YiS
t1URrpBZrJ9AeZb/EV9UkVHGAvyGKVvg/p5UVIias5LOcCsBxOVjXcAObtUmPPAxZa69qhjJyuBR
EWhfpXLFhxhiOSqfj+jeaWxdUvPs/gJU9zbo7BvjT5i8cp/FCgJeaJ8Rq8on9BjvzYvY4WlqgX+f
htXPf2u/z6UdsdgF01pR14oalKqIRSatzXj/NKSVbDLDlH1hJ+v2nxVvTy+iSbWCHl3Z/5U2ZDOb
h1Ml5VLR+pbOWQ0mo5qCI38caVPa5qnxp03SWj3jg+CGaucCcjBY3RKmqh+KSpJh1u9nIT4DoDA2
5vlWLiI/EJywNCjdgO1pjA1m4HZRSX0YWIud3nIFal5apBSYAsyap3tu/ceLSPizTl4VRBC2hs7B
ogyYze7BNqzE5Km0dkiz8RlflJb82K6SS7DzqryZA9DVZPgcD4DkNp7TIs/6BXW5Tgx5qVAEK7EF
m93mXztXbqu/u+sRPfJf+oKsG9Y+YlBK5Pie23cGjhvfG6pwXdny5Ma2toXFpWWa9zfUoQ2ldsys
KaywCLKm+qctmPLFM4iEN5wQgaQBga52d+yjxv0aPnJ2BwVZbQjn30i7Q3o0eBRyKfF1T+fc2eNO
X8LkQxECyCazmN2OtR84qnReum7HpGYL36uRP+jsglACa1zHw5Iyh9PEcgJfcbJlCY5jFpsk/Wp/
yaC4I0T6RK7aPlT3601tFcI2Zcc+AEXFA/Mplxkj1+jLeYidtQrPDgQ8gCKQWUIg4SKkVmtJXyip
iq54G0iMXaDzj8UZFc7kghfaSXS5P/ezq81XjntYGV8P9wKlIAIoERNGtnGUyN+doPmJkr/yoOqi
fn5YrJP3mPtk1EvV79Rn9ZriEbt7XhR4vVLiHUAE7WMH5YLakfuloB3fIoxrPGkNnwZ0vWyXdxf7
B0VFLoxjvA0vbu+JLt11g3Te39y+Wqtj2uOh/P8MROyzYWlp256WDFaDEkd3/w+QNpX5jmXUhtcv
0O2C1GovzdrKYI08egOe2OjXBi9yU/lMnY5DktZQ8yEfV/3Pa+rWa/3SBWo9/NTxCDp1j4o+VIwR
9gwZaWJTP0mbzNHRnuOjbGOdtwbpWD5bWqcYvdy+qZRFLA6VeiieZyPQpXmT1mgcFIfih/8jmSPO
kaZHkQromx6jhEQWjZsPJ0UrqbM59064SfuLk7Kgy23ECwkUtcUp8Ytx9eeM2sk8XN5B8/21ZF57
gCCZCoieOI50WvkY4pSndn8UVVl6UjV2siL2+mc8qskHscXqoeKNdtq+hXQ1afL81c6FD8xaeTcp
qI9L1csO4HYK7CWIM8DtQ4vSkQVF0/4l7XLgbqIzVWeUELTqtlPKIhHpbUqKTceUj0AhsadqBs2O
OSim1tbOyl2Kao9IM9Q4QeIgdlTuAQuD+mSvij7yzP1ijDTTZrL/TtrXjVHb2znqeu1Kiidsmsk7
gBL0+nW9OSgdwYEAiOaAC9o8YiiyegdVt5DMWRRYK0UCsg9kwJPSGstBJwYMq/eNripVcdICtRft
RtZnSaXwgJxH9+P4XcCDzEZxqEPSOkiVbP6b+DPTwgtOEC+NCqtTSfKnxtVB5mIcsRxXli7as7nw
rRGX4eT+KPPHJGGFhFl3aIW/sWLD6asLdY+fqL/XxNySYbi4BGtKlQRDQ2f+AFabpptMJqX7+qYw
hyyryz49v3yltH7YlmKUhA4V7RRQ2TDtFtYlff+i/agK1tAel41bqcvxuxE8+SNWkk4qpQfCzK4U
86wRQWMl3sVCxQZ2QOqI0cESV89rH6bSWMdS3JDSHVni3GsD/6OhVx72qJASUDh6LRjmG+DoPAcW
Abd8tSD48raxv1Odev7HKnyyZhr9LJOHKG+Jkc2+qB4p0sahxO6MxkQPneyTODHeVcEB/SyzznBp
yfx5j/aur3Ue8YWGnH8dP+EID6RfpN3ZW5K/fddQVRjv496iK415i6reznfQZctUcYD1bpBCSG4p
6+Oc5EXaXSDaAwUojbbALxJfQUwAoEijdilaIGq/BAc8hW0TSpQhWtQ9GeeLrkh6L7bk4SXGGfE2
0RqmV/Ge1IKvFUgSakbntbKEKL5o75ne69hDltzWcXaE1La/RFa4iDJ8sITBCS/K0fnFXvYdhvIs
1GLyFBBR4p58MtA60X4ELbejThE4OqVOsu0UAkGHL48Hua6f1smLdlnUJyd3tnrF5VfV559wwhEY
LNGIi5ZbLUS85nZiA+FWN6IXbJl/JrhIuTs86YgRWU4ac6SzTWFNBeG0WZwlWnwfIjp9xC0SUy3a
ltF4Lv5CASkfW4WyWfVl5UIwNbNiTO0UQOvkD4PH3cuAjIaTGt/eEOnxZr9t4yQpQnzUTnF4XHAI
SNR015sRlA/uBW5qCgSgEgwO21uUbcMnkw3ONv/BOnd5BTj1kQoc2HwIW4H8iv6w06eVHaThJHZQ
yLNpcWdHirs3Cd4unIlUw7yfRo+eioouqfi8eZUoun04rWYV6yzQSUA/D5JNHXCFp8JfGrVLVLqz
V58CTkBJCjkQ3ByY9oTFcGLOS879UGU3A0s1tYTe4iCN+frpnI5a1BuPBn222jpkzcvyyU/KcDXW
0H1yu4tSJ+/4A13dTRzrzfkg/P8WztBPhZXbfaS/vy/RzK9txo2qE7ANz2mG+NrOKbAywd6ZTN4W
dzycpbJqOwHq30PucSYTGQ1dWI8Gw1XhjcNLY4QCrBpNVoq9PpNNC9vdU2xS9FWbW91BxwFsb2/X
8RHwqLptm/05nzzOcw/+vanly/kNHNOIHpK6d1BqTGWfbaLw76QRWmQs2AeYJdb1V1Zu9UFRc8ME
foke9Q9E3VDz2TlULwckp3mR3u4zTgjOwAMz+lur6V1B4eGCH9lffQUUmJkIBKg01Q0zGOjSVP94
JsSLSPdLrT4ga1CM+BPeMcqJhttHzrRDMb9pTS6NOr9xD00l9VlqF3FYni3vs6nYoDI+hmqzg6RB
t8u95fu60kGyzCe3pHbYiCBAMydBLpMJE92drX7K1A2l9l61UN0Y6nSKk0fF338+zCAFJSWFOwJ1
xx0VL1lw8FnoXfk+1sTkYjbH3/LfaLHSCQEWLnxSyuYtlQKtrK7GbLGryaWRNOJOp6x1rym3yZKO
Qvj7VAtD4mn7xCO4fhU7dB4zPQDXsCoX7C+aAX2NodjbIGJ/Edy+Z5ANynKGP+FYYuQFhTbyZgd6
wCqFvJoAP4w44ivfPr7aVnh6+lIJOaxwUU+PaFkcKFFaghaiR1AEN/5dGfq7/ooRuDmDBmhvoPqF
3kj6kuCOOdnbfumsJLkcJhHA78gORHFzcnJcFh52/cWzLSVaVZNcSPXVbeT7wnLPUWO43F0vzFJW
UAAhthtkrxH/U2pdABP+HjcQM78TYC9lQLLtoIONsa+zE4ZpdQB3cwveavzbFB0j3uCcy0IVD2Bp
7VjL4Izp6I+e6XpBbxUWXsdjiicZpFqnz4ojbxN6YAj/tnf9+qBs9V6eggqSAPhXukMTrNqc5KUq
xRku6s0LelQ1TsOHiYRSvfIuQ9f45AumzP4hGWJIMTPkuAjGU9oywaLcwenp2MAeCvSir1/eCb4F
3EyG0cI3jmOlPBTAE2WmqCL2AjRCDBD1v5Wtf4npqtBhn1fz4TKQceqz7dYFZUopjUBNPqg+xj3s
lD6pFdvkBrvsB1EL5ZbHXzS3Jv68H1ZSYX+pfXvL3P8/ZMc6uMSmrdjG/1wnXX/ss7OvRlFZEM+9
5qQGCxQx8gDljSZC21X7Yj+w7/YzI0zk2Msl55zEdgwEEpLTpYbXajWOD7h29X5IjqpAsflHvubJ
w3kUHMTlouPmPa3Mws79miJgsS/eFMCG9D61RbyIdGTB/1C9k3S0AaJaNFmY5fsio5+Eb91AWsAS
5k29SedJ6+G4Jz5WBkqDBhX+mqeeQIF0rEBuVmAG8e3Aih8o5+pQ0hrgy5VCymlWM0iA7K12g//p
kZfoMoAA/ZHzL1k/rrOnJMmRLVMGlJ+RAbwvnJY+RAsHRhLIIPTP94/r4QJw7hPOgNpjsknYGPh+
NNZaNPRuHRYoz2hkEDMjtlCYDXyXym83WUhaVH6YItMhbgDXmK10H1DNLwUX2M7W3SJOI/ifG9J4
QbqUcbGeDzBN0Xdad14tdqV5IEsxToJEuOSuDBSKQIHfzzT9IvbMJefsULBFxDWAuihTqCa9EkZp
nkyO/ozeYyPhL5jVx06ws+3Sv9cKxlGf3zvFs9zJNl6c6slVJDRByMjF9B38NyLSlc8AA6ZTmH8a
k9J9XBUxwIeSprb5xAEPxOzpYrWaK2j3Ka7YiHCwuPyi6ieSqtxnnWDXLpSc9vDL4RTKEG+jcdm+
H1Zs6RFQV3W0l5p3uj2ldk/Pj4Wy9uLtTivCwmkZdRpF9BidL5jw/HZ/cP/GV3Pp3Xh9qaRCsVHb
Hik87FxYFpmLBPFMmJcHBsyvRSC+5FXrko6cLnVvKi1L5+zhCMcfyJocQHc9K/ifjFVFdDINwlk9
Iresf5gerqYuWbZtTY++mnEqgujMY2NEImRQvGIvDt5Ncn7fTyhKV6iq9lnSrcz7JZ6GZcfJu5x1
Ikl2K86vUAsnrHu45rOUDdfjxX7loNLsMSRSrWwSBXW9gf+E9vlS9tTc0kh8zllyz6ygiV0XuUjt
eG14A+/l4t3zRfKInOV6tGlXG57qVwOpY4CAYyM2NhXUj8gghVACgzQ1VQZyF5pu9O5HfbJP/dkz
5K/L1k16FTeEUT+Txu7d0yTvMBOHA1iKtGiVju1hCgDeedRT4AVMnihR643iCLOCu1/r1ooL48Kh
fEcyytUIvNxsQonOWlGKp6+ge6qAynQigqlKCFtBPyuj8L4XUgjWPqwWRtWZBhgB+WSakM+hlfJ7
OdI1p4QX71Uq1ULknqTV5FUbijuJF3KeHPE0ajsHjCjYtXQ/HFhgchweL6/o5y94zUe91sIs9Wbc
JCsPF/hYQ7ex+Xos8xmIRXRv+tGLzqafWpsmzeOyxkZtY8O9FkQqgqCPO3/o5Kr2vpURcYbmUJj0
GAs3K2Ds2kDJeFp3Ku1MIJIIRTSEMmxvwvv1osKD2kT8Xkmr3EFTcnqnLrK8gRPgrF33lHmcExH0
dQKwZBc51O12awS4RXY4BqHzBXw3AcKE/0ezcAvOfrSRQb+khlkhHgLj8X6K/VjvCdqjBVy4zIx9
oXSvM8cN2OA5xE9L58nXjKPyXFaIFj/WZrE4oo+PFvABxy0pZ+wWbI/2Vi3drZWnkaPs1dCNghWD
Nu9lag1+wbKworiID6KHF80mi9O6hMM+c/U+0xQ33H2vCxLvm5MEGdoLmIa+/OuAYhkok0R7eKpx
1r8e/IUc6N4qQpHE+shZ3/RvKafef/jtJvX+br4ufaMe+/8V8lvwPb3TQ4Srb4DR0VLgT4PIxUny
OemdmCshkx1icL+DCeCIheI6gVb/87QSroMrpxHgK7Q6Ba0NFwRzJsJcx9IOiRL3hVgMYGKjQovM
c9fTFYqL5rd750PM6D4qttY6dJ8YEb7vRI/ruxu8X5Xwaix4xlMx2aVEeO7zWb+31A8GNfUT6/lK
+mkjJUCvrT1lQhv3hlIQCGsc6MUywetZwrUTfuaas9hdjtW6VswrQXh3A8OCG6F14Q5s26Em4zQD
akaHT+cen53fh6CMEvaOp/dWD7La9r8UxYxf+X0Dz81OPCQ2k/4fIyL7Y7yr1ZJun4eE71O3RsCE
uQfWabRpXOrkc7PopUaluvELUq0F9vztn5z78kOKwHEktvcrWTxtiax6yRDZmbAWmQfZDuqBexxw
WK0hUHPqOHbGuarbgZpgYVG+0sM72TwDSQ68ST/3Xqp1RXu77Ja7QXbbd0SzTpQFBHOmQiMzt1dg
yex8GkTNVEExrCWL5pHJ1aqrK02XImXlRYna1wfTpaxHHtRkxeFsEmCzMYpQZh22Qm4UALenwYkc
R+Fb6gfFEOQzgv1Seo0w7sWnKF1DT0pSznioiXHqK+aXbGo/+KINZtPGjRCV5qLpzOmk0xRsqW9I
Ozc7LzykMv46CSvi6gLRQ/p8D2ZDCmBdHip7t4/YO0HsYFmchRTd9Tza0vilOJcuINlNU1nd22ms
71DIyOS92pgslEJ9jBWgaqTNDMO96dlDsui3P5Q4nErFPi8/IvgR5OO3O2R0pjZ8GcxBLqCRG/mU
TXsyP/f9GDpN/d/OEoXO2KKwYsHLe/T9KKBelGDjfnn2BtnO0UtkGEUubOwueddehepDbKW5UVzx
mO6Mx+t6CuRDfAtVKzS6fUvK9zSz0UtNjL2jDdygJEkphDdZIN/n8l8C2MS5G0XXhhth0QYZQ+if
deOMEPHcVXNMDb5Y9mo+RmWQz3RuxeuL+0cuJzRuAtlmnRKDIpb4FTBmuhLozhEaEnpgHXtrkLa2
RX/0EtMylRA4bzuBHABzgaE1vZecBWGubV9jSkv52QiTlzzqnQkcoZG5doYql9FxcOEJIlZvM9FK
U1aTV/EiHcqZ2SOdlVo4FBbN0FHZucP8n0hi7OnyPu2IxF6PUv9HrkRSvURrmbUrei1j5rs8I8p1
60QNNeDpnN5o+skMNmsq0RJWiOoFuAnRJM/+TIaax1MR/D9e8wo/rYoodPALCIZUUscvltkKNpGp
dwE7zrXLDbYnvTyKFn6frjSKlzvGlRe2iWbu7ZR4bV/QQDal84jQcXZ09jBIscHZl6hcLk8lDZ4o
mX8VeaFFHT5T7I4N1E0yi6/e/V+mA9W6xWNWtlfxdeBR1tH2owP3+nrNoLABSZ45xDNV474eUFks
AYdrlIaCEgwHn5Ju2XsLOloLyqb06DZqgRvMs4jyVuSmCsHm7R9Oeyn5i2UgCxFmNq6l//heirFU
uT5//TLnaM3p9dFRWue4l+1/HDTY+czoxqDGYc15IGxXwGhLOp0UKgVG3gjkoLYnASQfeubAWCQ4
NZR/1W4Fv6UwD6ZZ+RmARqJPQDT5QgNP8r43aNqEQpR6iNTGPUEurC5sAkczJ3xnj7NNF+mSPM6N
ro0DascSezX12sdbv73yMZJLwmR/Dw/qyjfFDBhD6Gv8X/NpkSr8a6PWLV2c1Gcp6XeBeiJ6h3nS
XoFuM2gZyYWEw+sXBDa6UqByDvr4t3pF+2ZEQ8GmmMSA6JVelcHUeJJG8G8zbBji5Rquwx8TC09x
HewSn7zz3kMmNzNDshZFIsyRZL/yMKtsADcKqs42bCfUBP2wwtpIrLC+1KJMdj+SfQwoWoonpOVb
rRKCRUVMdBKhQwl+z0aVCR5PUm43lLjOfO9a8qi1/aVPi1mLc58jEOYtJFYCcrsBDbk5sPz3kOF3
dfbCt+4Cyuac/YzcBEaNEViN0C4IIddHfcXFCQjYnhDXnsAWrccr0oBBa6bzWpcFPy7S7WagD3xZ
bo27ibw/I77GpuEIG6/+S901K7nMuRauSTcqSpebsZegReo+EcMxa/a4s6sLQc1mcBZZTu0vh9lO
q8jbp9Al01eniOlxQFE41rx/P2FLxbgXAqbtjnj7ZKYeShlVCpBLEdxrOooyRD1vHQESB8k5FJGe
G93tXvwYNo9OC6nrSzlp7BhmboEYMb7Swi+7ZGVpvlnBSSUbBwFNW3kwpaxRgIC7Aovzqtd/pCFS
Ev1qv+QJMu9oG4RGVgMx9GHb4v6yA/EA0qd0Er2Wltw6jdq99Lt0oQGWinJyAtJK60kl5WeZVs7L
EEPw+Xxs4hJyS8ASVDN9Tpbnsja4rPiyiPaPM9cgLtebRt3j7GCPUuQ2pDPO+LcIJUWmcCm6JeIK
Du+UE2agFMdf1LoMCcUMv/SeO/deej46RYijAN6spUPbfvHU+10ZFX4NiYcVjTo7wUJw0WIc+wO2
KCRtNyHqslg2KojMsPsgI5WfWo2i1Rc2zdWj3ByI3Qp7B9arLYgrKNNd8yuaKIkFQdglwPeNuScb
kzDB6mgmju8oCudYbZi16TvyZGk9o6RpwzdSAg0EHBUNGN0WLds8x14EuuIJuBhkGvK+K8aVq5tr
2mK5gNDXKawzOYoD+2i8tKSMhumeUi5nqcTybMQ4Y7MJ4HWSmImS9Cj+a7bhV7Yz6px+VyOjUPn8
BarNSjPcKU6o1EgjQcM7mvNT+5aTsJVoUaN0uFVz6x2X0qYu7dJQlMF2z+2ftHndhPBE7owiuIyu
Icb9whEZVHAWhJHcv5JJ87CzE/X45bkE5m4kU79KyVQjnmSQAFwr/69gtzls9tEJT53h2HdPRKZG
qcHvC3HFiw1mog3fk9qMw70CBTsggm1ki+vaApwE5uzRcNywZ1t+RZ6E1I4/AWRxL51CIfH0ZNzA
nTlHCwN1jdzK8YGKhY7fsuc6C3EGjpMSeMVkAWIzPD108nmiCOHWvZCYT/qyr6GPiOCTxpzQLvxN
leayxA8xdw9hyMTEC53Ju/K9p3DLfYrd+baTcE480V3Rwu1cKDGCYfXXb3r1e0oQWDW/sZ9PVO1s
17UGVP/tUU4SpLhAq0UzOiNDKr1R+RSUDYccNldR5v5Pn3An2zRnG0Q+hQfnKToi3V0qxsrXyN9s
44LLVsey7ataeSGF5ht3kiE4YUfZ/W0IXnZShNEjB+ZBs2IqbMBkoDyCeEYB5fc/ECd8q4MBxrpw
ZwN/MfDe2GrFh/Cbz72w/kc/OcUbU2PHLZzObr1DelMis56+bfOxjqRMahAFo6kKRQ6UwsgDtQ87
btBABkhhyUVBlAL/3yrtY615MMXplIxtfTKee2EVMRSp8rEzaoYRr3TVM2BTGBR4n7VEs0VCc1de
ryWcFo8S8z31+hCJDbEkYkaXflSYKwZY2dV2Ls4dnCgdvHzcnB64aQ771i1hY2GZkfTSasHwma8J
Gl1o/58V+tY1Y6xmzMdiBlSUZ495DxRMn9ji6KSbTfnZ+3Iq480EKRatKiv6xFSSJ0+s/XgCsmkn
r+cFXc9nGUI4j3HaY8zC47+O1RrCRAjANFeo9pWCZrydIJnq+FwDF+QsaWdjO+9S6HmNh2RNcCTA
q9J4auUYZ3VQiE2P45ahZETpLju1ybZM72t0aIgb6ZcrAcv3zCqYghHuJTBxwLd1CHv5JbrfQWW9
GMqULh2kyVEqKsAMQoWbKjfySxhy53BXRkkfs7aRxTEz2DXJrltq5nX8zF50AjVsSISUj4wwO2iG
M0cNJzKIxRxPN7L6KzxwOn0tGF02eDOKe0ICjfaqOT82FK2iu8/QcnEabBggHlRf+4btGuqy89S1
dqg1nKEdbtKp1LSAE+eUB6gThFcL+ih0fc6WjLN4+00iEbinqm9w2X+43/HU/IxNKBzayAVT2iv+
89Ulioojp0eQaAAZqElmtQsw6z7RBKdphCGEIHjeegQ/IKwBBSVUZAuATNSDXGO3B7lGNJYH1+O3
9V/Ui5OzOKKT0YjlA62SvDSpUy//BubD/vfTjjupQNC1aAPik+44YpbzDzsufXe6nWCo5sxMKTWi
sxWxDUhe0lckwMuiGHtPcB/n3sj1ftaOlaFdNS68EiuhJJ8ND74HYSjs1cydiozNdZMDT7dT6myD
NEciSGzkMmG52eS7QaXe9clb62uMDzsgC0qj+4jwqH0A79HjYDmbwaOvLz3l8uh6/8MwC8KFTsnO
kbGopW+T0Z/43zCbB4nOhy8I9fM/JSij1/ClXkgzMo9hlVm+L9gYgLl+25FFb0V49TP3W+CpCst3
2FLCbUDHzOnHLWA10fvzBLxVSMrPyZh5jg/hwI+b5IXV5A9MIWlzsV2i49Jk5sZYU/+lJaINB/0S
fedEZfIRjDhBxLY7t329z8NOm6oHg4qta/V0Uhlj0ONycwy/BO2AZ6xdVM3BeX/ot0huQXh5AsUn
/SbpR0DY4NjzImMRzQXzEmKWo+rn5NOHMq+8OMesHlUOstuLithPH9ykx5DK9CShEULaOdttKScG
oiX+ko1ihjGf8OxwC1r+Uws8qD8vr57rlUmAWinkRWv05SoMcRsJGD1lih6srPlYbYmVKCxNXnib
FE6j8IW75rdkZjofr/ydcllpKhZ4dmKISWyN7eQhzva1tNmUykZC0x+jAQ8iYxQmv+jpkylUIR1g
Q7gFhVq1nlr80w5hi/GP+q707yn8WnFBU3xq+pnDZYepLCH/BhwtpBlxf1q8sZ3ipIEOfHOpzdtQ
1iWzL2/vD6DdGEmxU1eJXJUA4pRaQ0cYpnmj0/Xc24Y0jrEGUxKcglmFgheDAYEtGZOql36G7ij9
vYiyAkjIYy0LVH43sE6W6fvO9CmyYIMo6k10UfBmiq/Jo/vDhfAewS0UVZ5IwZa8R7cd0K2x8SlW
0lqfOtzkRLu0DvhF/G5mjoJ0EfC7WsH0v0TAyDKgkszluz0KrlljZGWP2vzi+yYsL4p7nO0KF2i4
1WfbDSkBpRFWcXGXoWVfFeYdYoY66/HytC1SEQq8DNp/9/Cm667K4LLRI8Ku/k/Q5drIVCyYD7bB
8NtdAsuRATiMRXF0PLerQy6PMoNWAnCA+56lbayYZ1Ve0MX5abIl/khCumfl7lz7PH9rYKQrIO8f
0T2hNR5tmGEGWHvQHXsB+hxq9OAdx1pFCXOgX7WTEaHqjcVft/0DqCEkgTAGPNoOpd4yizdBTD7/
bg92t8kwEi0ykayuxUHqMvniLW3qqQZeN+TGWUY42tp6dRzd61e1AU6LS4DGy/T0g0jy8usi0Occ
msV6TYTLTuV9RR51v4EEz/B/24Nlq0xgEq3gVoyuvLitTaZX+5bobHAYsQ7Nk1tCyo9J3JLRvV8t
h+7BpaM9DGSC1ui7BSmEyA6wy5KyCgIRRBuTSGPn0gXwET5N6CNVepMDT/amgP5NwrVMGjKtPWdz
vWx265TtLInuc2N0CXP1ttDuD3CaWEGSGMBcAbQ4sv54//iwo4ZS9KLUtEAYorgVxbAUF0LLpgzo
tuOgdDZ3r2AVwLGF0WbU+vBJHKG+OMLfgStUOe8GImHGAj5T5ePlmmwHD/2Tz29hV0WbIrWvbFes
CulsLwAM5RSKeq6QLk4ucPTKz4Ma7k2Ff06wRc+EljqVbsrf4YhOab7e8DwY8X3oeie0HWyWaUZs
sYZTktJrZF6y3LdDuv+CuVndQ/LBYbW94GZ1LPGL21d0x6COpKV/i1bKEwdYn7l1ZVxg2zkTZCW0
CVQmMJw4ESEhFoV2+PMa4ylW6UMFJt/P0Eyu68RwlltnQfX75r9WMmu91ci6oRuXt0BGuJQb9XrT
qaA7Nma4ip7sPs/Vk0QfEH4ozZ45iKpKKxtjWYQaprCVkDILGszP7tJTE1jPK4A/lQ3LXk6nB5A/
luSxx7+G524FXYA7LC8903jCdIDNdHyAl9Z1H9Mtz5q2OpoJVn5gJTCbJYAJs0R9nCK98qNgqhan
Qs2Yst/+zh5qDjnUmkaVgbts+48NIH+ps9iJidno62aLuDhzw67xvHzk0mloFjT95J0G7qEHJZMH
DZESK7Bt5zOyFLFnsqEzzsmKQlmg7crbG1TCklR8tHPaxvp3nZRY/u6gSR8HBlQMlCviZfIOgI8H
Ro+yN6RizEfE+8nLd+pwth19OhMBdBqb2k9TEWeOgfwvlJ9b9fq3EZgeElguXbfsKMeMejBBXzDY
4LF7briVdp6O4361P2NnvW3pQWZjmhjvSfLAhwE8RJTUPCpBC94YyFKwt7UAkW6xj20yVrVBe4xP
6z1ZlbkviEGgTOqSknQs7OtJ4CYzuKi/h0Z7bf19amq0nl6DNTsa08yXTHw05Tcv0AZaoDCIrWRt
uBLqLGhPazbHAKrZHUFVck91qRCojTi60iZY+cUg2Que/1sMxMOW1ShlMKDCiTZ0ZevkVia3j775
uPVEE1Oaz08K+ZLAjbyGHmIAX4nMqnKnsD51OtBzgiURhBPZZB7GYBZHidtrXOHNbzGWi3zP7wWb
/l6+3OyJfqx6xjf8F5B/CgNEP/extPlBdyRdqa+OJW5gYh8Lo3khR8c1zhXoB7Y4USduQRzRvCGn
6m1tmREvfYn04ms3BWsfuUEDTm5RJXuzqPOP74GTG4NKqjJoQamCvMjIBzOZc5ZiRbUv9kh8wdfS
s2Rhj75mBISU6//IJWc7JESPobAf+9b7MQxfGptAUHraaJPQaxRttQGvgOIepJ+WSpfbfEUC0Ua2
F/mN/8rPUo6egq7kYMXiRVG+6zHfNW4nUaA1LWpqrqL1gdtVnqExmx0nn4BMbzQ/aqzvIp8GILkt
1oSagzgZbWJZcykD2Mo+f9cCIxnx5MDpvqFxTF+JMxM8DfX/5W+KD2HsGTnqigVT2FzFZUJjMG/J
w05Apl5jrOYggHU8Srt6Oz31f9wUuFxPYa5kq3Y1UanWO7FglaC6/5Ex1zfNtrlYdd4B4lI1pkqw
8B5lHM2U4uTBSiWG0yYU1Zn2RDz6CxKKtuC3gakglFZoFNm//06cQ7rZsEHOdPqELCMM95/r8q7M
I4VV13gOXDSKbcvbkDD3MizOHUJgDWBlkUe2Pl9lvSeOUGGN5GaGJ7KrDA4SRZihYOMWYQyAyDrO
L3Bm+3LhUFXE8AYNB9pSKQKjj1VfKxu4KQQedzlmE3VxV/Wsvrr8WVpIGbspcrgVLXmY5jKpCvws
PPyyA0iuqj6Fr6qF1FAUO1qvYPLZi+YknS/PrZhnXhyi+BPEcf4WKrlyv7Rv8ErLYcbXkL0otCHX
GTgA51acV9GFVwGq3TdpF539ju6R86jONlVr1HpvajdeTXGjrhg9bk3V8Hx/vl4WTrPy771TKwWs
2+ix5pe9xH2tDpTgDg1IygE4sVOnxT0ZbH8VWABUEWebbkaxfhxdpM38XhviWrgsdl8TRkmpofsc
znq5J2VppCJaQ58dyI7oZXFokiQpT0jOgtlQ9ee08X+eV1S0OgWHYJOBf/9Gvx9Bmm03n645jx1z
xLQomaYWJEwxEE+NM4zBoWUH/L4PwTBdm62Y/KfYHRAVB/g+vmSLyBC/SeY6rR1skwOIdb8IH376
HT/FHTaaxcrh5TK02a6SHhoiNOaV+0kaSo1tbkk1Mmcn+U3F3qVz0LiiLkI5FhrNgjsg0PcFZVSB
kVsgITbb5uZbyzU1ulEgRhT2yEYHeHrKhr8KNyrHh3EtuCdFKINWsDwv/4fZ8HfvBQ0GLoZhSQ/I
XFI/3NMdcLJ6rubpC1xJfHKumXpotpkF1MqIU3+jE+4D87wwF1lYokixKZ2ARA6IwEThsER9AUgL
eKK/LUEtLu/xwDOcmWY1iXwc/zht6pYrs7S0X96T67DJ3L0RjeFiFkRcQRwFgkSpzu3bDqEvNBwU
f2As4ioRU0BNkLN6uQWEenRU7GqqIOecZFCP0x5zbxONnoTUezSr46u/ZKgVWXuP0JYEc2+xsNpZ
TIBkgTy49Do87cXV/ZWIpumvHM3vFrQGQKSpnJ4QQVc3Nq1rWnFPgzjMCD9t8Ai5U03IW8mW/DWp
eWtaiMVVqR4K/0Js4VMtlPqKzxvnFvtv6jChMOIyFVDzTN0SWiSd2MkaQshdRU+VJSnmAhrcnFkV
ADezRRU0Gc9aDYoqxD3e7aI74DslZhufSGsTW64nSZ0oyAQ4uUKuglfDsX/Ix2tAUK5ZMrt6TI27
kUXWmLUAMOCkvnfk0HDlhNY6Ln75bjddgykA97SoWUyiO7CgY32X2ScQ5yxlCup/HGedrfOGhpFp
j6f9YBDCOp0XxPSnxGsTjDia0G25i2G2V1AMK5IqGVwB349fZbCLD0kJRjXpo6LGyWEBAFmyt+Ha
oPnfwfgIdXMI6DReAdBMvv7WT6T1WhIp5FEaCUZo+kz3NpQtAGVR2jKbq56Eo1Pc6uRJv9oTPLOe
SJ5MBJPCYVS+dyY8TmheLXCg22asnnRXBkHHqZttjM1kzsa5L/zsEVP1/9F98+k0zr9YgvFhL9pL
sxo19FIi88OkmBY9snA8d6TgwFx/JQffSgb5PznNlMBDoxXvklx3U0BzZQkL//ILCMDggiP9uB7z
IPHX2B3KLxSQ409ulTh5iWjiGcBJ/WJdAlp7dFk7ZJuQaWb8l3WoZBbzNDuCzEq2XU0VR/HPUxNI
t0kdVkd2Y6Y/dv4hYd2pM8FE781T+49jYmIQPaNDC9TaFs5qdzehHKq6PI9f/OyVaAc5jTtVxIiW
NcnWZQOHIg1y+F4mq2RznujMheBTya0HO8yoVMrQMG2uwT+BsylHkqlV07YpDbZR2xwu+bW8eWJj
i1rFhgwAw68Zn60VP2qpEsz/UyVpe0UrFFvavPXz/QIhUE6c8NO0wWBmDuSddKXHNNgiy3FBww3k
NzWEagcTNYiI4ooLz7kTdrWzNcVBDSLKMcMqzLzl3soR9e7JimSt8H8ofojWvVI0GrkcNsIf465n
tPreRmqMoFnAif9QuYbfeX9XCjyldgPpyK0LOWN4g0nGLjyZRh07taEu5WRybPmeMM+JsSxWiq2x
RMeR4SxY1ejc8Gk7Rnz5XZv2yPrv2diZirieiFYfF7g96LXz6sowWPdcxWRmYViKEORr2AYJglrD
JWzt/cJKh5dSO3fAJ2SYgpiLlBk1hJesDUVA3/jTR/NaXhkkG/DozUBsOxO7P7kpNTN/TlJ0TSLC
KkXOSU9Ulqw8TrJU0ASPZx+vGSPOapsKXl6uk1IfrM9+c+R1xZh1Rjzr6FK7lA6dLNMCezMIFkMz
+S4y103p4RJxmYzYsnhmRirkOquROscSHh1rnyqoToYQkdtCALZjUNWOJyOGwtm4K52Ej3K4qNPA
b315y4N9i9AY9PtyUYyicQal0M3/Hv60PHwf5HSzXqa4Pt/5cFK2NeWDbf+j2edJGPpSLyntOkTV
2lCx9gK/xLXEzflrM3o408AKp3In5SyRJ1AaYrzDg+JnfeQVVH0iA+t9UXS3CZBJ9Um+XNwmOdA+
VZj0o+Epha2TqSunSlsc8uDVTci75b/3Zpy3NLSYy/b6afW9CAzY+gjPzMxpKAyqF40/3PJkdUln
FRQ/yqCQKOWojhi9DoaQW79uGvCTpbN8O0dASr/LsfnVjVmLRO9uwg/+nylOwDbCV+DThFfyJHE9
URP84CRWH9yM9ltMgFWvOEMBJlsusDjorDggJmaUd6eiwIqaQDc5S4Uzc2OT2m2F+jw+sMJGi5rU
ZPyfHEnXqm/WtJJa+myU42q05dAdAGZxodKnfevmiCSclXeyTnaoIZ8BHniKZvhxPp0pAHik+kkv
bIiFZy58PwgGz6nOxgYXqWctYfGBpelWTfNfYnVTkogO9c53ELINgBCBbPWJD1d02ZEZo0EZfjnQ
+68oDiLVY4bkbP0hlCk5kPkQudDLxgsW9eulXNszQFM9oGybnvQKTcsWwdlGYs5UZ2bHIo7lKj6T
aPkBo4zWG87Yyr7sfX/rxW2shAx5u+9u30MqEWleq9nnOMEhMZl5FH73tU+fVT6uLqN4WwLWoFhK
y9ad8lzmfiWsgQsYKflOt5GC3uiosAJVObEwiUJISX46bsJ3YXyM2g2qBJ4YGL9gyULpqp8D7Vfi
DsSShHa1p6xpfKt2F8qlBnYuxD4TcepCXZFqWtj/PBw1sPxZl2ByKobiK8haa1t5xl0o51I2qyVg
vGx1w35KQDRunX9kSd1b3c8hSPVmQKhwSX1GBrf0YbAMIg4H41RF0QwR5ekVOvHqGuhG6IYzsTRT
QtYGF/uyQincCoxdXbXq8X43dsFNpnurqzIxuOMJMV9S3edx6i4RjiMz5s9PLNOJNUTiSsfkMqNr
ltEuwO1H8wu7sSyXw5HprIQhowqqVinYaPc3uCaF0kbuv7uLFEBhcV5OoJqkH5DH+8W2RthzR3s7
1LuOW+FmSU/7hqg3qFo4SARy/+Fy/nc7E/gF9zCLbMIrFui6PB0ZJb/Jvvtd9mSuQr+jgUbI4hru
dSOsRcFi33uSRdwUtbJL6ndscTuSaHAAbAlDNjaARYL4OS27govIiP9+HWSSDDZMzCn2PFWEXXZn
hO+Hm3lLzxVOAtAh5J8kf2P29BXAx3mUfRVM00LPR0jILTZrxlV+HQVT71wELtUFCdnU0jrQasmj
KrUHZmuJwCFq0PDxLgvneBQ7BDBKi0wD44RgOCDF+TdXUT6uWJHjPYbMtlYl3mfc9B3gwqpXRB4m
CsQvdJS/4qmOGB6FBvdqIzT72ROebMTF7KL1GypG9tEwPnQrzjLX7oiY+bRiaQc1mi2tR62P2wcN
k8ePH2Zj3nSVScFIo4M05JGgIgLSsdL4sGFe9GCIyHNLpPCeoEoCHQGKCKlNMpMJly883D7uPHue
wkx5SxMu9FXNpUpjtm9Q7dmGwc2o2CsrXePjdNSPlt5qSfH45jzNU/UL+DHi/OD7edZvZ4RJ1jHC
6+qLfUk4Zvd55BGgEBSJLQkub2dwHVcVN1432U/boeqpxyegCkZn98M64tRqVOjcc5J7AtiTgi7+
ehoBZs9D0b/4RvrHNTe9lA0QRjhLtG18Iq/BntwceT7bpfJ82LYXZ+pdqchXOKlIops5hsf38hy0
Z+yj4vsg5HHqE8hOB1wqKS9sNt6Lb9Dai4og3fEZTZ6uFvIvgdfEHPJ9cf4WsyhPS+jJoGMGQq5s
WCeO8JbNLZ/QjqI7jWkSuhs4sJpYeNzQ82fUVLb9kg/TQksy0pH3CID2pY9n9kd40IJZWRE4W/5g
PcNWoNJbI+POwqVFW59FIg/Iscx1ipmkXz1fSK/O/2iwSQUPkuzWS25kMNe/20VMNu9Vneyr+nFv
CRjSPyjMS/SR+H+JTirY9poW3DH8vWBiObor9SEWtCx70oNptllQ1Lq0733aHcSPJk2UHaAgHGoy
1Xe77nqdGiTYRiWuZY806xZicH/mE7+iVEfgbivSdduSxMr2GLGiV/C/T2UfdWU0PnFuHHQRnxOH
hF2DpUpt4NbzbknMdHZyIzIbia1TeRzEoQK/rCX45zf2ukjXA5OUWp1Wjliy7bMfAWfcm2uNFrU+
UgGZAZ5Wj6LdAQXVJdm6Wl1poK7/HISvxJ6v3BzJNRcZJI/jjVwZusOaZZytWqwNPKhkYQadbjx3
/01uoNBxXUPbSeaDG0G+Vj3RH7ncQo/LDVRsFzTBz652XDN+l6JTQu5rnbE+Ue5+s12JIeHCWTKd
0q51db13SwP2lKOiYjo/V7zayfjmuxm5iah/YGYwdr5X08x1xrjlPdYdt6Pjvx4ToUxhEnQYjf6C
HobpYwbF2qUDj/IpPVhQiDsXp8tmk9VCbAI45Z4V/VRJ2UR5L4scs/qm4WLkzeqmBLMAmxh1Qchz
I89Q5SB2BZtNqou28A3AmwTFzBuq+iHpLilgAM20WklnmxEWwql66XgDxm9XnMTwktiNbS1dzr0D
oxc2YvQwz2Q4yV/hhOgU8yRsS+/XTeBP2rbkikwIkPq7AMsT2Lb+2In/9VqKVjj6eZ3vFLHAzYkU
UF6d4Xyhwcfg8nHlhIrPg5FSrsipV6s2z4JhaRAjWNFsOkYuXUi/7KXY1p/SIc+jggzBIrbSLoof
qtSD3szbyWb7jVdwk0dUjf42DltkAEjYqNKsS9RJ7dhkJ/B476xx8DMOFAupmUU7cD6foBH7fjLZ
5s0R0JbhlYclMgbZnqNk91M/+rTek2CqDFpHAzNlRvzyM+aN6q4y++D3IinP13cQQ+y21YPmEh7O
6vmPn4IwNSE4cMDXVRV9eOGHJCriXhog+N2lLgRkWwiMIweZ9c0lZZuzTwstVTSW1+sikOmnKyYD
LY8IaVAVJ2LTumtyxAvdZzFNqh73Kt565lqAvOfR+FufUiD0AYyFPryZAghsVedao+SkCHoNd4dN
XO1jR2O9hcwqaGItV8aq/LtA7Xtj7pA9IEk0OOHpCfLHp5ljbVVjuhBsm16gb1Ds7uRmaV5UQnLD
o0TtEyb89EwqTmjjJ0m95OJVJPqbLnY7v9gGwwv4spjvogrmY5QLbs4SxyOY1tSI6YCkOlpFipcY
9P2TYgMKJa5w7pW4bWwcSbIQH35CTpRbrOifUajJgaIqRD52NvCUV6zbJ9UwqC0soVcTE+10Wmmn
GrWJMOiKZvjjsAhIpudzxK+QliiXpASI3Y1eBlseAX1f/VsLL6KXMhKxeR72UDDDn3jrHWeN0lOM
lYKNyLSbxoAWxmF+xZFRQGtDlabiF09ethCPQAjn5pb0B+IqzNB+es1jDUUPPUFrgjgA0IjRQKpG
wgH6e49b0T238rqJWB+69Fu5SoIbrI5mpQBNuHWd0KUbC+D1yyfOfRSAU6xnJTQNuIfg6HIFKOXN
gurv/W+lYGpQOW0TmmPsA/2DE3qEiihgrGwufVDLIah9qfOOUp2UsF/IdUbyPVjfCVwCCzvHikFS
jGvbPkqUD0VzCtA4f9jd4gz0IPOSjPthTy6rKA5ryaOpM+HAg9p4uFJvZ0ujc+wlAafZr0vTC8Pz
2uCUA825ol01FdjXSFJ5bl20QttCLk+Sgoex3LuDcH8YAB9J4k4c/xcCBUHYf3lEr42k+tiUEpQ2
K14QTbaNq/ofZuj7uMw81aecer+FIijrkGTzKJup7zce/VLFwZKu8SMxPgXV/JOaawTqUPuWqWLs
7bKeFcBaNVRNEXvT30qhNSMLduBLG09MV3apJWThHcr+A9No7LdCFL/JM0vutEMSohC23iEZoJFr
cMinB7VoI7HD3Qx1i1NheNZsZSSR3cR8NUG7TAhqGn3rUd4RhQyOP8mdHeH/kLWTxbVcZeYM0H7G
MPiqvDJtLvpV1TkE0JcJoCotXWp84rljZAmDEngq9Pir8vMEsO1CBRBvx+YcUsgc03NxdMZ0HexO
tqOXf8UEybWo+Q6iKDhQM7an12uXSktAceMYFUhsudMXgy9RCwAFTF+dMvJZ3TKOxa3C5il/jBtU
nsC3D30idxl33wRburUtsSbQOwQb2NfMA+6it8YTp4FXJRLH6PjnmDDmULnTTLx5/784CAPJUFgS
rdiZe0HMzMC5LRQ1g2W0irn3wMt5PBBF5tDPqM5mf8eVTBJL8Rw4z0QQZZZmrdkAYbC/0q14RpWL
zeSDPyTJTVhZY8dFKqLBWEvtdXtS/yuIF/uDq5WEgnEOq5ZvFcRPx7NAJPOVqXjhZWjZl9XKGBNK
owQO6+IljNYswOQhEo6g4RIO4l9R9zXEllaO1ALlLslfv0OEZG7sUSNX1kHmIyfepP/dl/iEgTr6
7qxirTIxOALSJjxxY6/uZMaPt0tP+XL02YkluY6Ea0BK7c6Qcd/2iH2kDZ/3ll3vAhM7ZaxYoA2N
151kPdvqNlnowLcWRyyYOaRNWx/+tzGXECVxICgbL1ytsOMLUEtA/2mrlPufrWbCMtTxe2yL2udf
EAIvrq3mNn6PsgFGWvfk85F24/NsAONGFDhNWFSNuUoIYdzyJ25qFZrNGXG9OmDJcMEEnBe9VaTY
aKR3GblNG1Ub48uq+/m+/+weltKalRi8x3uQAu9QxrvuZdvM7boHOIKDslT9BeupTNT/jZlLps2d
Yik6Pi5NJgiluf+qgMoAdW1NhgdYcfExF9nD1s3ODRCzyqjxLnv08SauAT5LpbIJcoqL5+/wPaxx
LNT/etaY3TdTpDIZc/YSmhMaRIf1eTRlTkBFCz1E43F5RHjCijHeWWD9OCGBbR8ZgEcDjazm4TiO
cyKaK0sM1Lbn3qmTT6xcmKsPRewSeGkIlZFKoC207I6nvMI+cBfKz7LbrLYO4E42fd0pAjf5pm9X
w0x3d7AybbP1R/wTxK+BzYnzBa8mYBmP/dNuZwPAXcnjIQ0s7OnyoYwgb0vncHn96gMLHfGYq0A0
nhalbonY65Pa7rJ1g5vwoSlQwTEnmyX8HCRvkWlm1znaotM+fBRDTarPPWVzFNvuFloskMWBE44D
QDhyhrGlCyEwbw7yitl1Ba8tomplf1N3/ixUcwdScX9A2PqDHVg0Vy32OjieiT6Or+A+CGUsVEl4
AXnhXKfd2dx/LOwGa0vw37rPM+63arEfF4kLJnMfUvlWmhrtQsHEWJirBp5MYIlsYKVSya+HaD32
tvh3l4hxhFiCUvwqddaFIAlmUUsulgDAZru4HKi3mnCtXpQxakH2ZPscfRZwEelrTLs/tzF/Ltsk
/iMQAeFaHwq6PSQTXUpSekIxs8ZZ5XLBrqxhmzaRDHZGN7eOMeLQZvUPuFxja8tjhU9GlzFbf2nu
/IZmww+2uG9Zswsx7UV8HTBIi6K1svaxwvYx45FLSNJPR4bVvsN2KFFUk4AxrW4lpW1erRzSAdq7
+gtsQyvaLlqtOzWmOD7SqN8PQSH9hOvwLMpM/7tb6pBT3aZk0yO+mtFtXW2XiV7OWC73aQzW1rtF
7pwlj7Sra2X0eka3HhzqpVmzCPc0hMAlPNtyIznUGvnxtZZHAhCVZHsKUJ5ijmRvYOafl9HPuSIQ
LfnDWhOE2n1AZ/Vy/oD/mzMm414NagwEjwZDxJkH6DsQ2LvFsURdSaOXSCx5V1AngcRfYpm1i3Lu
nlym9eURn/IsjmQoCzIOrmlN0SFd1WDAET4JX7F0jhCVuKo2v34rfOcmUxMGfh0h/qonLH2cS/Av
rorkfXcs/2rvmG0NnaqtookWkahDVMB4en6XzbLW5fZL41EIZjOW4Wm8hmLvnT86J3LmicPGXbv4
3psn/XdAazumuWlzC3RtcSZLTCNUjrdk+iMgFjTbuVXGAZtlm7E+sdKaTtLCnTmBfBUb3iqy23wP
PZ/FAnm9agBe8j9czgMdXBiwWKg/n8zaxF0Wig2lv6d6vO9UoB6DrQLX9WbEBF+01u/mPSY1jc7M
JLklSxnd091J6tuFqAb7R5TM3AmUKcOyitkoNB+F//eU/m1fILKFJiqEYC4wo+G+07yvHn0kWG/x
0EphkxjAqRdULsjUrHXQjAH0pZJbto7t3vUN7ninGPxx4Tdpdx3QABafWBNog+s8JXlGDGTcBT8Y
xfnHJDBakJaKqSTQlR4ZbVkFqlRnA5kpCbPsbo9L9jxUgTsTTjnZeKWlz2zTqVwNKir6GzgiyTsF
ajd3365jMUyWBeW3YhLyB4k2knydN3o25NXxrigMuO+kKe9T6yTg+v3ByWCamwVXnYUIN6DxoNoD
cDuMDy4VPnYZMoMeHYPlqOFjZOG2s7+StN9QyCRDv2C4eGh5Y0wA3ZGkY1W9rzfcO+nbwNFRb85K
c81AqpTBeBDSwtS8H1/+vVYSgMn99luZUSL2Hs2LLF8aap7OjnJjqAj4F1ShzLzJTs33M7EBXbEm
fHKBw3NafamQFgW3vVALk8Z61vgvVGjIGxYcvM23TEE6r95ZYWi7vabUWoDwSXUX4dLNtb+pjjgA
iDn7OMGjN6+FVr/RHInpaVbUxQ8OXeDDoXcZu6tbmrtXacz1lpOiAm0bZXUKN1+Br/P49bVZ9U5v
d2sFUeK2x6P+Itmc4Qcw/+NLrGejDzBsFASbHGclRNe243A79bi124t4JZy/QohgoRwH3l5/lDQx
c6Bz+CB7+j+GqRsCj7vVzLmmK2bVAb5aUcgo3Y31kRvqAbVdTSKRAF8x/rkMuXNdtlZjz74dexji
pyPzbZacZ9zkOwSh4G+JCiuu3anFUzy3Eq4vZL62c8kLZBC1fQX5aViHxt+QxjypsRe4ezZyltR8
N9U08pX9TjJ71xM+sGGsa6tiOayvPv6gERZqf3TdILMEzwKuSoKxwEFwrHWWeS9Av8+/WLdvw0d2
QFVgsVN2co6Un3tgdnBaagfBdVyFqEQHP1YQWDAJZdXMvqdTxgzhL/fypuowj0BO866VOhVvGBkF
u/ukLGQVyHre0iawquMssGabJ+2Hud7OCdjTvysbY9JUlFJZ55BvHJ6Ze74RNGYRKtv35YXhH/vL
152CnimFpEkUqN1+u4j2DWrX+5nD7XNdPHVphttxglPDDjqXYf1ugFnC8jFGxt0R5Y/KovmmzV5/
bhJ8x15ZuZeCy4xtnsDYV1rlS04agG+LlKyHwo/bqyHgT9ToDPfNuuMjxDLWaHSmv3yWh8X0db/G
WFQQanaDvP2hRI7uiPOC+yiA/1HpSQRbwsE95PSVuOOQw6xQFdcUbGixZu739xsmdREs7RcYyuRt
XC9Sqi5M32rUBkUgO26k6PTdARpJs15kgBEtz45WUIsPJnskH2w9lKVf1Ps8L9M9gbPshj7/BhGi
gEZWMBueAPnqXbhh65Qp7kGVHiczaekES6aNPtFx2CkSJOByrextqwYM1z4E5hydTr3aV79BInVC
IKhoR12+o1kCxV11g7M09b0O6g/GYuAIiOwF4SJ554WR97tOzBPnPB5/TT9LHSXIjGV5+6nMk8ry
KlOn+U4ISXherSSUPGRQ8KpZhwiqk9nIshDj73NGIVU3hBidoKfHOQowuSDT6JIh2ZHZeglId7ib
F/YWT2WHELaEm1hW+x8O9STD8IEBd1pSIOQoH+33WJ2fAxmKHh/o0fhtPlkS9F0ZwEx8+V3Ad0oq
+l3RcGku+hjDgG1/wDbzGp9c+8OUOYoDXT7KagTf54BIBagbM0RhWhCkUuH6d6ZHJ9gxDv9tE0Ll
O0lt0C2nJAWbfv6MaGxKegSfmd+x0zS4P3z65JUfNWz1AbJe6TfcPX3hAlh61YXEWvK5Xcx1rDon
q6HqBGdYML+S85FoyOdSu7thhwpiPxpfKK/4BlrmjTpyNB1zVIRpq4mS5flYWUo8H3BH9MIliHLQ
WCCnYIQrTf0bHjRL5ey/BCMt/hgAKg4hLvGxAnuI2AUdWxFTe802T414eZ8DJlHNFL5Vs373bdNO
hltbUq0N7YLGsDzyRRHIMdJ5fb58nP8PekNSC6yfHIaYhxjsfAglb18xfDEhZM95RCS6q9gUbjvp
jbpDOLCgYI4i8PQQ1EnoeW3c1jRavxnobwbglE2lTOllY6xDXyaWV9So9y3sJvv/+rcbjPtqePgO
gaVHDmkh5P0YMoRgmigX6XQmSw2Ueg27z0KV6ooVm9BZIojA1hZVGtmZh0TgVv+Q4110bufmRuR9
IfMRXPVpsgDnfF/adthobZZs22qdlBXwe//IziaWEiWJiC9LEmMejN2boVyI5hMjuEbLtgWF+5r0
nBRMLJYz3MO2KjjNuXxxyJyuaUjDnTQb6gRPAegfvb1NtOWBqxGf/m9aAJyotNo6Gf2+vM+73zEw
5aMnf3HkS0qIoT16oBpyT3hALksG09zr+jBA5AUw7/wPXPiInFumJ1LCXH9bezgqChgKic0QXT9N
39fXWMNj8YsLKz1gVQVV0nhbPjrULo4A9xvyjwZDmKH+zniRvZJDEnMbLzQFfD1TWwdyuBEbBdm1
kL7n7WpITBnEtJrycZZBNoejPH+IDgqVfWc5FmfcsF0rrn5N+anlXesYQx/jhS6FSfKSSseVQjQz
1bphey/VHy7X767VOUO2uyG489+noixjtT+TPbP2cR8YnVgqFkuJYirkvGkYPxWLfRlPxEWSeAxk
6aD6b7gE3V7/h7z6dtwVaJbpThtIHngEeJvj1HBr2yXqQejJlQjXeRHdh/jmtOL9N5rVkJrY/b32
jRooL6XJzkQ8onaCJHnNNsI5226skdf7FTASq4bqrx3reu2C6pD+28g2OgJEzckiEjJyGm9Gb02Z
nniHwyD4tAnK6xDNE+m603GhfdJC85wZ02wovaswCt/f9dtFofzDNeEl9+YESg671rdLGs+EVugZ
79GGu2Dspj9fh0NTX81rSXAupEu7XTY/K3Ke/Z0ZXwCFmBC/0CjSSuKwo7tIIBb93UWdXWWfvTb1
1FJl424wux12ZBLaL0WjCjUuqiU5IDfCceNbPR7juApf0wkCG7UlGWg7fqyirRuOLANlCA7/MfiL
Tw9YIwppfjVjr/eCgS/w1X+kccLF08k5T3hCMdtEfhoA/k5RJDAcPW2t8X2FVc6BbjyXUdFBAnt6
ZB92xiLFHcELKcrRBChxCEaUdY3/yf1e8Mlk1i3qMm2Yo+aPg16/+wx3aKFXTzb4ZOIf/T9/Buc8
SetvCPve+cokXs1PO1DHwv9s57LpLziwqLVrXD60kG3o0GRB5czgdabs0/CWW+CxnW5h5rHIMbb8
iDdWFxej/xzMsoqlUXtSd8lf6BBnazdkuzTLzV7EJCY2H55890VogP7ZQEKgR+oUqMDmrKcWMPum
wIYkeYmi4VH2nVzvfSlV5MLLQBZJDO3NgBbSB7yyzaek1kgKjOjav/xiAB5hUlAKYxjMDZO/JdSQ
Ezzko04i6/Ujz84/AolGgjxGX7W5Y0Zqv4ULWd0elmkvAUaxk58U+gs1CTy7DHBJxIVO9H4qGOxR
C2At6EkVH+VPGcqQq/U0LoP3SX1SWUqR6QNltrNcQ3SS5Y2hnzdG+rKx+05xXeyfvY+gSer9zJBc
GAk+MW9PIrX+WC8DZAyLfNOUDxpuESmDK60IsPyK5XNxFHZ+eMuKWB3fo926mwvyfDs/uxf/RimP
xPL+lIjD/5FZ2T+8/8VIQLsu5q2cYFrtL2h9t7BfyI4H96RejBOsBJFHu3+XmVXZb7HNLN8c5xjK
5stXW9tMzz01LNYQ4X+I6jThtiEMBPz6dwYBiti7abB59TXe/Rvj/1bhJu3NJYrIeEF802dCHmtw
jv8Pu3P2BKChJdNA5jUPCgrh5sZVP4tn1qO3bCVfCSRymBvO886fUfuX5IeDdHRBGTTUVAeyzhQ9
T3kX7ZGWTxHJQuE63h8LKUjH/YAeQ2fGnMdKkPuzVZrQdJrCU7r/qmPJGPUNDlbVI4yUKQE64XD9
SY9PhmATbV3AtrGy6IP2uSBHASZIUO5BaeENnnPQiJpOE2dquTTtoJ9FxYmVPOkpbrjIlMQYOQMe
HSO6qPCXj7CxQlz+u3Z7wOXlVsF5aM6K4oSqPq2fW/jfVR66IrO4T830PyDnvc+/M7whuKcCFa8g
YfQwNuvxot8aKQ1JTq2EkWv/JE/WbYvdocgIZy90W7qvaIWw9bVt9+RpOF1VBLnHvBx12MRbz6qA
6fq7YlePkKpZl8QzYQFfpgI/Ly42sjEGdLeUflXBQD4qzDM6fV2iAXy1M+Kq+5+iTSIJHXZ3njMS
aSM+pJcBlwT7x0agLtrnbjwT+fCbuED7h9VPPFaE0iB8iSjfQbf+4W+lxDKO2WIU3h+g3YMS/OxA
lDLTFboOI2grMaCf0Wkgnk/Tnqd3S5FqV5kGFG0fv2i7mLrNPQw0F1y55AgBtSZoonulASSf6EnU
0DNePoKj9IVg4dEyynmMNbUQqolALF1MmZz1ecaukjjOyTMjMNWKQR0sNUX79f9UDwFaKSIrY2+d
r/0bGh/u/HnrlsKyg9T+FtSJyp4AU9rtb8KUpfyflFCTNATw7ECW11xCv6JnCUQhJb8MGmrNRaJ7
HY9oDAlMNEL2pqH84t8G2sPFp1ukdZwm3NCtKzXaXx6zWUm9Lg3n2DjsZP17xnutuzWPLzOZdGwL
1aJYnKvTdgRM0vKB74N4iq+LqskfrKS5U2BNHe2074Y0z7XPwPyTtavARDqjiT/K6FgouD/x9VPf
DJW4jAvGeREFJBj4pAyDxlPRdWkCDS5BzdpPZ5Po2yLJTuhGz4nny8BIuztf6U56cQxovgJDAFvt
q+Mp2GoqVHPD6/uc/kIP2zuZZOtXU8PotDYrOg5qJQM0o7xvqMAR62ZLByf2jjNVkBDRL0U3/i9A
mRT3xvwYHh4kIWrsxGbHAgoqljFVjwpJXJywRWM/ExU14dDN8JaD+c0jnvLt580hdAs1dKEf/Wd4
q+bgA1j/67mh7q0sF7hyhp4dT2b+FLqkQTWM0BK74NeGhouUWojVXIFMXoDuWxRAJtRNYG97Ki2f
0L5JkKuKQakd49UeAhGy6RMBbTlePTz1kyo+zvXbm6d1m8RGqX4GRm9KjTcsSbA4giK6YPB3rI90
46bdUGUwZXo43PnpJ2Z8e9gTi3U47UNSY6wD2fSNaQNEMy/104Dqc1tRwTFebwrKBlXLH/qhlVya
LJIbUNGs8l75jQ7jmtR5iqtsgUPbduvvhy4+FVASjOPmhtg0aYyZuV19+WZsD/fz/m1MNEwTcNxV
M+Zfgq8T/MKXARi+CNSqiF7tplo9BSVYmV2+cOo2jtV41XbPSuX0d9zAQI+h5NVZFWRfnJzHfQMz
3SO3rSA7g36lyvL3pYfladJTTQxxFPRi8pZH9PMIbzLPAL3fBP/ayhHgwBbrglhOrHU3RUpe1HSn
jaNv+UpDl/yPC1Fs4/+cj652ts3nafXeRBtHe88qKyGGsoWY/doC/BJfeCiAdnJ/PR4F+HaYCQrg
EleZMIGoHAlHcgRkfC1ZskOaji83fhZRnIL6idBZOgRwSR/KSlzykD7TMIgPtfK6BYSzlwhVsAVs
pu7uZX13DbwT/bRsBrdBRmx20WlHwBQ6htPzYXVOQDK2+1DS4dOATZBQdtxFsczfbDogd6rjFJR9
bM8KxyGmHerpLGyjP+i4pyFs9HMdasd7Q9BQ9ktp00Pp1msWftHPyBGJbPHVVQUbP+Kg9kvugZV+
vrL9sBV5vqCAyU1JfJXg+Ykj+rSbLlOK3eJk1b0Wfg4t+U9YeERl1robCaV8hW3OqEZwe6Q0lJqm
SRthOxa3MP/gHJ4jNf3PXVzDnvPRxUZ6CJ5Xwcn0BrewC+mEujAL90Xgllob+CK0EMEC/2m8dSuy
5V4TQkJR3H5hRwhN0HuES2WJmw/98XxDXo3RduEfT/N4o9NKbs/zrEsFh43rEHprWnK79y6frCRN
G4B2zGEl5Y+tKeCQ8Ln03yxWjFZBaH5Lb/03p2xu2iXJp7fFF+w16Wt/lZ0CNCtE6oxy9v/3Pv8n
UpAinZVdxNQCRBxXKK1H+xHbmj58KQ2BhRzt7mR5bPUnuBt0LvUUJf6BfyhE2WehtDps4aY3338P
vcb4rSu18qM9shT1sHjJ8nhIfkJutwP9jeQdS5FDUqcLJn++miK2SGcd1HdaExXcKw/9sZ6xlFQy
mmgRtfyjjixJKrzlV9gpyYgO6bwR+aBZunRx4lwE+EsZbV+QlJtlJpPgReAtP0jPeK1tOOoqtO0E
Xjo5I4ANrjeDF473H6hfV6/e4ThPVFeCzo85/MBq7puOTq/YQ6TUHs7VolAiQ3/SwdVR6QVpI9/t
Atl88rBNEgPY3VV5/UouDjTLZZvjgBgFThIoFrmvhcmuXxkVevMrhrSQHcHA8VRMQEdD1lXCGwgX
1Bu0QVKvdBS9detF3wq+3524T7mlmRhASRZLbOlnrCR2gbALLabLcAfPtaR/FiiXoAFGouIOfvlU
wMGDejT1AnGxG+WQ3Lxbu4gupXNMIWtpP3oSKs5r55MqRNglkn0sJ16pC/aEjUo3c8QHVuON7Dnq
bEY3Jw29VV0ynBlNtE24nNLH2jSSRh4BxEWr8uDnRT9LXaD0vUVU8y6ehmA8DsSDZ6iCc1iciJ3b
KmZGA0wCYFKApFgKaqeAQC0dU014dZ+uIcFAbZ7G9tDux+Y6swgCsVY9tET9Fx4xpq02BpjOleFX
NkFnnYyM1T7J7JgAOBCYB3+heOih0YjUqaBkWWqkh/etHqiQyjhnZ7rGwfKpbtb1PA7xqQ6/3uqp
le2/jtnNtmc29Ks01vSbJMMl9ewIbZ1mlOnoCC0eBG30Xuz5oFb4EWp5WzbyvFee+mK2Pa6CBUPt
9KggScswdLrSwoanHFXgfp12wsS/NBnXt9vzB5pEl0wM6wGK8LiM8vKfADFdWUX0OC/a+IUWe2Pf
tRdx83e+ouShSHypNQS+12bji6vJVnC/jiahDtaJChcTEC59M+KL3s3RabFkiUSmPr6JRqX1bGCw
tGphI40gT8tZBPyYe+Nxy0SHG9Sa/ZxZjJeqlgkRy1NAtfBhpmBy0fr6VBo28XWtHyUMh7Fv7hMj
s0a7PolITGhrLF5ROpidI2FanVc1OcTzgEts5XvwQ7MDA2s7/diUfUINlcJVdcNwqEAMduh2Z57x
eZ6DRPHoSljKFOwIl58AsdUaA1Ct/kesZaf8JvoUp3uIyzTqsX9BCfCn1NNJm44Bt4rFVuTsnIDo
f2gCtcc0hZSx92YPBzpKXBF3u8t/Lwyq1E/WEwPzvRuQ1ybbop4yL61fWE9eQ4WDLfHFAT+J4GeD
fr28ErjhguxT8AEoMZ1nQRUXkmSfJ4qlz2uMoWsbtDQLazaiG+tjnzshFICtWyt2kIy29gaM7nrQ
TvPO9b3mLrPuOrNTNfVBXAmv+Mla+aDtzpJwdBg/lx1weN+DN1IGcCwQZe6Qg8WcWCM9iow9wjgu
dwAyu1PRogx8IMnkiImhNJiMlbTqu2HgWjDRAFl2RmyOFE1rUzCDGaQfeK97BmvjvsqwalrlVmrB
S+GMnlnDWYXHonR3G/SoQzTGeL2CfoMOjPMF84In6eNRo3f6TyqCgALNLkSJaErqYlk5Jx1tQKtm
deaXysnNqNJvUFfMXpc+a4hfVTLeMw1tmrhs66OCpE1V9ahXXKd03P+p56p0zkrOUQPSoUgBkMZl
Gz+5MqTjQyzAEblNYc0S1QorarXfOUtapqacAqgCrIyUtiGfZWWD3qsI3bLOHL7XNzcuvxeKNKCP
n4li8lID5cgctzdzjYdrZSi4fXhH2WRUBsWM9+2KGmyDvh5A6DAcYAh3gMPmSSkkxOP4Vfm9eYfw
AE5WZd015VtmqIfh15VzKphTLKaNn1S4GhjuahWtH/fxT4bgIm5NvWyzzcm4DdMLE2iytiwWoLva
+UiQAk/dTfPSfPj81fxYTiA4ktwmtIngoCql+gd/uPCZhFJtJ07HS3olDsrlmH6h/7mVE/3UK486
aphnBZrtSzE7BV5tLdhYH/SGDpHsw2pyDDEHczV4pdv1vZ7UBlPanFuBCQF1u71sexEXTD/V5Qcw
/+N53iTbzwSh4cd8NnmQC6D6xTCbzGt+ysZOM0W5uz3ct9tSkIy5lNLM7HqtPf5yme4FAVykqCvb
14S4STbhmgQ9YCHUcKo63b/RmASAo9oKsPP/BnVaQrMW7hl5ba+KaUqjen9OoF+e7i4oL+1L2T9Z
d3Ank6Ry/pD78YOMxXnhuZ9/W8wifiAgC9CRfLROGRRfQNG1tqJvtnvQDpA+31GodsOymApaewM/
VWyNzfmxpJIshTUDk/LN/etYbhItwGGA5t2rSBcATJcw6HXGzBuKjm8sLAtphidLDrPjjLGD1hd4
/SsPAVj+h7bUXbWN9fLOtE+PGLjcQXazaYLF6/gB9GaH123a3AY2miT/j9LLtu+nAN023+WnqdkQ
BjVDy/I7EpOaf3gyi36QRYNM92sqKv2Ef3TjquplkTJrhfu37MLeFVAF2HhNGXiLNqv9olET69eY
TPdeO9wmEn8Jk5QGBB5rEbwgUto3iA3vjhtsdWsISBNBW2cm5TQo6UDmHrZx+cxTIUWPaSw9nEzN
kaUAp9jYcZhkKKY1qwlpX3sxkyOk6/07PrZG4B0xLvhD9JFVxdRRHAAo86cLUksQDjv0RNygb1/f
jSnVzRigl3U7Z2ynMtGEPMuNys1f5Ye8XL4B3Sqe2L1lgZqb8HpV0cs+v1O6ExJPSvkAYltOThNh
5F6vnuCUJUnjAF8YIfo0PpNGByJU9StaUUuoEL6tS15NwEjBsbfsCbguGh83KKNy1nwdL08LflQv
RNLlmJjzadm6eStsFDLfWNsWqdbHeYvjpLjjJwJvJFxWOU22auRQbfEPvBO+sWj+K2G1PYvhu5TT
LRcKupQAp5X27D6J8pkga0DVnGNSAYs8ex2vghepGaTmz4KC+hj69onbwgX18CVR0tsc6bo8bWrA
y6qNFaTpILBcFzk09+z9g1pPYk2hSyhGdSgUdcmw12vWYtxZSJKjHQgsDXfj/xRuzN1XUKOxDWx3
YK5bFvrZLsb4cw4/HUWLYRfeU+dZ1av/ZKiIj1mwE7v2oWGAr+4VCDMfCJUKyjNAjE2xPN4OpBEH
rm8u7V1ci9NAgUyDLKa0M7FMEeq4Xge8BiGiLyUe618YB1RfnaIrhFzlEyR/gUre5p0WFrOyL8nm
FYm3438XiE1VZie32o5b8EHUu4OQFnjFeDmC10ViMM/GXsdtif3yU6LgA39R8QhgrzWmoiQElIFM
9IWgHATcy0PDxngpC7dKuEsk9VoFcGKAeSbpH/9y3UKYNBEFnOnytm+T2ISxU1cQI02ZkXyDTMXe
TGOnbMwYfwKfH44OS58BmrOuN9nYfc6aEN02ckUVlwMywCSrafmwpCQcb5bTfDG5a9Phoz7bFAcO
zpOMO9Bjd+Eaz03TEE79gX0lvaqhh0Hxsil1ZFF5+x5kN6xb6ngnnaaML0zSko+yBg62M/r8DamV
97yi4/pp7g21Or2C3uh29XjZXgsjrg0wVZomqD9kWbdetWpu+xSGZ6Xgq+XRj/CpYgGyvty8t82A
BzihgevzqNkMoEyAgqoKxSjKZr3agFHI2zXTuP/Ru06oxnNI93S013u0ccLIDgahq7hUR7pgLZ6m
4guDkJN1iInSIi1j9fYZed1VNT8uyYIzJy9mPiFrAeDrNpUMFZXaBa0Qkul+eBujw+idKIwFpRZl
8Mt0zBT0fIKIn4p93JPK07H+2VmnlraPxDPnLfbCKDvTAw5ZGVO6/cXtLKvG7ALxb0fc0mFm2LIG
/5RwciI6MFDBeSJHjn/7gfMg5W0ouZNzwXaQDsCKoRflegzkSxVNtiCdzvPyejXLBvQiAEJm5CKi
OZUEESjU3/vAzN73GgzOQFE4VRmI+UVMdroDepJs3bhXsCcZZqYMfNvUTZBlUSk1hVXO8aUvaDfo
1XU3L32iEAQ7YJjp40qwKVqsM6DuAKzbGxKXRN/sP65f6QXDc5GHIt+UfqqxwpKfvkaFxGqzCmng
iZIbZcjpaIEKSB0jNVqv4HGtj6Q6h64Un6KzLPfHJSLFUKYv+v1Vm+uMx+wXoH6WUn6fQsQrWYgU
IaInZu05BXrJHy3cKpAX+lYtIjKOJR0kkNBtRDoxhwNTlDut+jPNj4vYCV/4ugtwPMw0IYoXupZ0
BaTppvdrngRk0uhSPNAUdN5phJW8Uobpox3AVqem5XZmPstUnVUl9x/RvTV5ddkcelftoCipmCuB
GVu4XgozzHPAanLYUOiwcSTGfvMPAXJJ8YWXi+A21ZnU3qz7KK55FGzBi19BOTEVZ6rZ74441DOb
hvimq/Y/7TV+T6MqEiJoykTgEncyLztdbrovLwJCezv9Ew2DmeUdzJM/vro8uYwg9VyzmoSWR0Zq
kzKdFyL6h+1diRiTYIh5hcfwczaGcTpk8LZ0y4wM22vnbfWZB5RMDybFJUk2I8hsLk3VhAE2kcO9
4q0FtKTdNBcZ0P7U5IhT99m6mUrP5JB7mkqc51rJS8cHdGAZH4MNC0jPYUgTnUJ3NDXSV74yd4Ly
fvQby1NJp8RjtyqzVQsQzm56oOA0lvpFo/Vssafg3eR6/XgSaJXo74bd37Zv/+sGVYP9UMcdAm0j
5kY0KjIKe8nzbWGo5lknjH3rSKpTRjArLQK7zZWd9h1sDBkP0vh9n1gLi1qoGHRClz010YShnQPo
lR151Xr3DEspATqH3G3NNqO9oJPFtsK3mxBTIccdg3cOuwxB/UKJuf4MM/dy3R04vgm+ygN56hDg
TTojKN2UaxvgJaJXq3bFxLqkaWnBdaOA/uAZgBPK39h+PzrZvdim9fvnKrOezAIrUDvRYWeBmNNu
ad2k8vNsh/kPOFWU/dEXtnfgc9AV2Ewf6VjyF3SlqUKDnu0ZB+CY5rOZ0dAFV4I/qGMLEoVULbQG
/i+91PPiZ/Fvc9JZ2XfXjKAJEx583VUYvCTVggG3WBezipQZi+i2fgSTliU+5hCkx5hCBFxb6qG7
quc8WYcjdOtRb41cBiP+FV3POnTg0LWJGWC1ttyMaddZLLep5hvUAPlAPJb5m9VsI6WZ0m4P3yY1
SA+VdKgqMWS0OK8bqXLQAZcH/OA0GspS62ndLoSUhxFu1i6RNjLup9BbIfRGv8AHH1zwSYO95OYW
ImVKz7+8aKobqgdPkfXvecef55hmtjmJ6cqIOSntsQXAnPLMJDY3Eb29vB6d8nC0bce6GgD8gtIA
UR294CfjzuLX8Jg4evBkUo3KV83LK7zfR/wGKdz8xQui4hxKcTs0xKSgRm5Bhjtqz+Uq5VTbwaMH
KE8LiUfq+kBotjNN2r1+fTPFcN4q5EdG6MCTI6QOtybAqMsp9hRefqPkEPxo5FdeasGtpdv6V+7s
yxFqitYBWpKigXkaXMIMvJjrYSoHMPkzsBNW52XdxEfLPe7Jgjh9STz9tC/1T+I5jlakoMo/5AVA
NNRr0qjrXVYrld0ir+Gc4VW5K2S5MBLXC2d2mWJJFaFBLsEOnaRErqDH5r5NnzkBHmntBGHHbXwp
CtmNEfnPZIbh1xLVp6wi5avr+vJGlm5KUq/voOC2lRBI/IjsHY2N6cbh287I3Vd1qSW8ogPWtnXI
67N0dHrOKl8ozJvltQOhQraYxvrdb85zv7IwQLdPm1kB5y00BDGRbT0DjafajyZpl33orL8ydBs8
YtkFx+xrJL+r0Sey5z6hnkuOjLBUUlYT9qMCfWQvFMTTmDGxBlMh+/e/OBfFqm9B0sssqCVxEXmg
W9+O1ruzPjZLObLyUJmRetwJLfYxgiJM1L++q6Iaxgx3Ff5ch7G0re1fNu+osAH0h0zoU9bCTHoF
zd2toJjiKhoA95a3b4BL2jh0yXodmm8fhFfCrW2ZlYkpU1BrZYTLfJiEfqa7WCbrU8igmeK4Sqwl
umcKk1fcOSVBv0GmyKi9V3jPouSf9/LNG2/vZaPs0KDbKpflFmGL730KtA/6DAJLKhcxBSvI+2KR
ocWAyQxbwQ2HGJrIo/6WKe/Pw7iFyxVa1SvFHmqA8edhHg9gfAOSY+1Fhyzncb8fO5mDcrJ3XkfR
Wlo7ofrzHWB8Sizqvj9zjj4qAnCzACBFfp/MyOMuDZSA8c3QKLjhVwo1J+zoxDN2lw/ZIsnG9jP0
bppA0NnVrgtPZ3QtT8zYDl7RYsVXwjqlK2KfFK3lX2Bwte8K3GJHrr/IrJYnUXkYHq7tW8Ug+9jZ
vDKiudBjS8s93rB3wRsCq0p8DNwN0yGLnbhNuhzQQuSNxWu7JRxSjyS6OjhDDiyQMrnh9zGumSuN
n0Jv1U1PrI1hz1SLctw/1UrvNsvNgoSKzkBY1l0QrsdDhjx5Mm+llRQ974gx2k6dbfWkqIZzDxM/
0BWAi1xz7DOz0PsmzWki7PDrJlbc3IAZoc1opiT6fEA/xNkjOUtwyHv6hd0A/rKpJu6NmBPJ3Ulb
7oFwko6OQsuiXRz46HI9WQcFsDWVyoc9TqLlQAWhne55jocefHDF9Y7qu2MGvQtJML5Bxn0Qr9uR
mPk/nu/1slant6v19ylMA7Q8m/Ho2hZKCAq8TptQfcrSiMfP/C8Cy3p82L0zCVEk6/BYUnRlmaqq
hjcRfuLaTVgOR/IEUeIEojVTtHt2GZua+qNyvP9C8zIruhiujz+WIPmZrny1GoLlWe6aLh3/LIhl
picpQoXKPjtsDa8fEg6pmhUvpym1gK4W5NiC0cWy9fTlqiCxWkjHted8VHq0vB0onPd7Cx1WtwBy
3XSwYeMTY1sgbiFWgiTB/Hv3AT6c0de7f8G4H/yJfPZiowXMnfUsxMYpXdwuDoHPfmRbLhMl4+54
q2uDMzxTyedNsXximEOCOhf1M9jDlAmesQwzGEtcprbE9psrZ3meRKUzd8PeD/gOpG+ku+tnwjU4
DBHURWqEz/wZRFm53ToZGgXSr0TSUsMhEGmjeLNz7BpshyM4+ySsXM1Yfiu4XidaATtPB3fntJ9Z
bCOA8EiuO1fzP7zdhRc1TYUVhf+mXGRSw+VNLhNjkPm8MqpfArbhmp+5gJ/lCRlMJ1kB0BpYroXK
tPgwGDa3XNJOBpKsNk+QV+MxqYrCwqJsU6duuxJsXC+aiN769r8vQq5wMNoADBXC49SsXjDuura2
iyX8wvnSG7UqZkD94J1VH9SUCntzq8Rgn5KQkY6cPvWu69TGYfgnmiERA6A0b72pKPsKY2PwSERm
Qm7x+eCp7t8Xf8YfvLxmg9DsswKrN1K1spNEgyDDqb49YPYC+TGyAlSKIbdrcfZ++R+wgPAbCSIq
gyBDfZAkn7JUi9yPUbYPY8kOcpJT4bSlBMkWges5Frbptm/iFiRHIuWebg4iUBPcppv/CLYJ/bLC
1QpxhQakILzeqkAYhPF9CoK101sYyYj9Bm7T1D4yYXz9OT5CbSwAfqazPfuuhNNfTgf90oqYpofe
xxBpngbkxTSmpnpUX3XoEVYX/zRIzyN1Up36eiIdY7kgapulI364cZv0XbUtLpxbR1W+8oMUuqj7
E+Fl2/HouyVE3FcrIyHVl6m6EXB5DV5wNmreSiuyqhCwSlaS8M2ve/nzgSqY/PYiw4iYURE/rMQE
sWgxU1ZWWW6P8YV6OV8Wq++AuIHjFuOV6ZWBNn3k28yadGKXWsAAbvJDQCYzBtiy02XvI9l5Mmbv
5+AD3TjRC0dspjO/MJVnal5lnN3emEBsOur93xw8hQmBKG/mG+G0Ngzc7WBFn+EcOvwJ+fuAojtw
vU83sKY+iMh3GLnHq9lkI4oJj2Z6L6RvC8ZlVX9uIXVN7JfI3zlD69rXyY+Cs4iezin6TnMBdd/X
jnhxsOh2ykbuQXY6Aib7FjT56OGaPmVq6klsYUG3yXCb7+ZW4YENJmX3jtrxbEjjkY/q1Dv/bWkc
V8pubIb3hSUw8rJ5eS5R+hP6lS1rTHKz1Fh3z1T30r4sD62U8CegNWq5gwR2kqjaBQ15No7tJSse
pZTdy2EweujSmtgAZfR9CkzATlvMzMtdbcyDsWLIIIFFrt0CHO0HWOjheqPlvHMLRQZ7nA8b/Pqz
hPF84Oxr5/yjYXYMvnZ4Q0Ytt09Y2BbtEkUR2PJKFYZYvShA5RBFCWNQ8qM6Y7kOJS7/+ZVuuBVv
xgwgaZlLk5bGWpXW5cg//IOcFQQpoPEof4hQdiRx93F67tu8ASpAOT4LOu5MRZoIT58himn297HR
ozELQdWXl991Hx7MnpbXe+giDNbEjCkHz4ZdXJONqhZQIq5MqAFTmQti8TN4GX61YPrYaHRciHVh
0z/p+HxeE6EqfD0l7kq0K8VuPmd3ZkKoooCLYxmLBXODIKYECwfdCpW2nXCjK9Bzw5hBUiT6wtO6
LK+vHj8SfVw+oA9d+uydZOy8KvwazTM8SvkYIOojTrrkDatwXizOxy8a7oPC4uE4H4brbHnFN/6k
7+agQn5j7sm31A3sdS+mhoZ4FDBuyAJDYHG/+tx1/nFVcoBoRochmAfCrjBlT+YO9/5Nw7q+I7TO
TgWj8e0hQemS1snRsjYZS4DUN10orq2NdbQXvdJd8l7sc2zhyKoyI531Ka1cDuYGDGaWBzz4en4R
ZEPTwOjwRLFEMj2SQX3vdin1mHfyDFWyktgCeaBfWXsPKSuwhIj+GBvqlTluyo47GtkQOMFQlDiv
lAKRRa8X54s0Pc32BtmGzPN6AuPLzVC2H3AlN8Hio/zm/ctgCPLa33DuY63VvvsU0M5bZxZnML4o
OlvqveMa4KQqwoL3I/0Y3zp7x5sr5RShwAqwchWH5WEK8OMIRwISrgUR1+Rt+EQD81bUwdDIdxKy
fBbYh7ilhD052EMJGGAhrY956jH+VzLNO0idkv2z08BS5lUhfnNvlleF+/KunTXbNrcIineb+Cvp
wsjPlQHiPLLWuiDSdRtvWidNw3ljRATO6JFy65TQZYGGRwCHcQtNxPtiU7iyE/TlKzi7tFlmedER
5s8aC4D0HrNy6HVY4rs/toaB3G8sdV9NE2HDkj6cfaDLyNlnB9Y1Gx8bbSJa8ESwXlQLzJa822vS
orXM4/ANrmBcULDIH03v0oTKHqE0XgYPmGX1QecvtQpgUHsRnYf9TrbytTD4ylnXjDhOO7Lsnt3N
dqtgrZKKNHjprCjLtrkx8glBm/TtCVnSpZGWsB6a/h+jONiAC6Pm1+KFA4YK1rEP/apGKpeubfeW
8c3D/9SnQeBHeKfLXdqlO2OJ+Ys9cKa3xwvZdJIDJLuRDUw2YVh0ru/BHehb9Y9mrK9h7iE2NAPI
hu6rGc/qRdxGLir0eZ+wnxD7FFqIm5VqHYBqZFyKPqDORce1p3Eliz2HQ/QO480O1nNk6jqC9QK/
AMIbK9OxwwRESwKxyb6wjJEAss1djALuzt5mwiPxZa+Iw2OpreIn7qAKB6DJPSl5rC1wZzAimhXS
f7x6cau0erc4fP2iR1NFxYD0831D5fpaTSCRgW4/trC9qVw699GBRAvsv3kJBv0irvZVwU0ewwuE
lqEaS51ot3qDQmlFhxU2SR+CRtdE3cW1v2ojnkZkT/8maT7XWn7ol6H8VHe1p2WM7LjseqWUkpUm
BOUpXaBkpLxPmVkOuiZFWbo5E9dMpCoXjlMq/jtfw7kfaNNISjX7LKU2SQTN0yY/Bga6RQy5pMRw
qJvlhY+Ks6AzMisUbVLPMz/IpUQjT7XXrO86TfCqxvzRRGxnEwRBJd4AbP/8hZKFQGvBWz8e6Y5B
cF8k4H4SS+o7N74vYrmHSxtTgSrYGhFvG4mXISeX9eJ1UXpLWfGdKwU+L2UTopW4X53A1TiEfba6
wd8/GWD6u79aO2Q9wFa5BgiXLRgyxozGfk8RKV7yo0Kof7hQaAeaPbWejS6kW7gnMmUgMHdZss4B
GGMl3E388HM9UYlIGLH6SCN8lNuhF3VlwGnDk6+9AxwIGvhKC1BWXwNZ8cTWuIAZJZuRAYYFcNLF
nD9v/SAFHb09Wui9vyJXGjokh+AGvoIzCdQYdUlRczDtG/+Uul2lV8knAO7hQk6cTsGHl17H2NB8
aNHBSrBZVrP+nsdekgYesz5MhjZDKsTRLc2jc3aPxiOEclKZgBNISNFrL27gtrGEwmgSDDWZ7nOx
wU639y0KaTh7MKPWCA4xhCxjDJRxJuK+r7USTn190C0YTHo1Wyro/4ePMsOyXL7IB6i9R0rLQDvy
We6qsDHvE14gjV+jcGxwC1oNmJwkw7Sskcktg/+aNab44AMoE2tEZfWiBFvkS42USaL4fzesowTR
N7nR5SGUrNDehPNsHoVmUer+UniOPvWXx1dgmCUsWD0L76mzU2qBacUYxySSFV5Y2sc8lQH0sBbc
PSCmrp2YtWccmKqi5iG+AMmpuY/gahJfcJ+L0MnWEYDSryrODQkfqW8jybslcn31CWvjAuK7MwiI
ukQd3YQY/wAa59jJTCSsopVj7Ny/nuMpTBMz2HePD+ZSRLadSPwRRVdtWXmGcQih2naNkL5M/BX3
kLyoOMIH/467YK5Hc9t1HNz9dVGgVTkKju80696vliMh681a0yCO/PlfOtfSKn52zQ+ufShQVqoJ
DVAwL3m/scilwnS0vQbHVkLob5vpE36ZMA/f9obXZXUFs+ie2x6ISAmB6ZNR2H33z9GqN48p7U3N
SATbm2LNa66Tl62J+hC7vJMYLUSHuwmyFPyxYbhEGlptnYCe/4sCH/rgoLwgps628eJc0KZeyqDy
mT/gA7zto5Rh4/+bEdr/fAzw3lLUOxVGAjeGIioAVdlfWiRdYMmS3a07243CaJshU/9jSYoYBax6
joxOSnNAh9kUTQZCjM/mGrotckBrATV/wkFXUkRWRQN0mYWUm9xvHDOfQlIUT/w0MXZDoqNNIjRT
0YvjfjGyJjsTqU3JUYmF3ynJPT54ymXsaAt2ZSX3yBHSdjugCUj0vIR/jY+1cSwlUnF3WjTHbK3h
zr1J/V14ruytLwizJ++AzJHfdOVHv9HCYZXo7DjP5crTXn1Cu6ykl/3xCu2B1ZpcXa+GgYFKjqWu
jw1ZunD7nPVDz2tvu7HbhIFYKix1x9HOep50YHnU8NMbmVmerPV01prsYltXZ0Bu6VNEKyvWfqyL
vu0+cz6AHQkzsflTow5MSoIqAgBp1OpcoI8eDgFlHSXK3vaM6wL6tkLeVWiA8KZrFTXLh3VXnx9Y
kST3cUrR7Kc+1VzrdEbOP5C0F1dUIRGFofmDjijaQCg5EU88mhKudBaKtxU+1/eFK76kjEq2lkfJ
AAqIQYYtVwr390IP5llBLV7bhBpk55RVVSpo/2I1HYG+xx2n6qab8Qg4gtDq2wFJHyNuAgaXDRIK
sLSLWNBVNF7iNvwyRSssA/SlX9eKAWiI+h1CAv6o6Aw+vSsTrGKHrQzpGgNnIPyy5X0lFZflD0Vm
0wrWFEWRz8kBcPpONYr2CZdqvw2J6v+xcHc4YED0MJhFG4x5XHTeL1QDBfsvq/uH9zt6NIuSvaTR
d6IoxwqJpvBcYPN4edi4lMsWzZT1P5F8mA9i89gkKDO/ScimCcxtVEYzHVUTS1CR8G9Ts3zAbGZ+
KBBjVWlf6EVF6l33px48hAXuiARPQ6L4mzaUf89Lo6DdsRQDS/S0GTH0QNaP9cfE2yFjnOL0vMRu
/9RyWn0lc7WXPz1n11soC5jb05pxv1iKMYeShf5wmnOfzExRBo+ltrBUHwyrMQyFaBY12VWqNcXb
/sgFkiUPQu2APKUJc/PrBKd2DcupJBXp3oesWACrUDdoAWEAw6T1EdWSc7qxEyfm07AbYeHUoNiL
Sd963GrsQcpJOFFXic3Vs0Fw6Ry/9V0nG1uhYl8LAzkupabagE22rJuq376Z1KwaH4t5/SnQ8GWM
wslJFLMvIgOuUTwPWYvtCH1aYnAW7+GvoumWSKQuNORpUsRYTbE8BktJmVZubkL6b+L0tNiBQQzW
lLs/seCGiZmmjR3IKb0X2MRIuQWODToaj3H+5ySX1x7rcoRFoebfdW7Gmv556xVVhYaDbGAKTqY1
jTkBU7OXzh7WTyHC+iKurxz6xBjizf40dBVc1hefD179EtdjOPZ35I/v5AcuUvrPyfM6jPQj0GYS
GmFaVUVH/DO1GJL0eduZFnikRRd0au2r8pwa5tHgdjpMiPdI1aE+ywqhH7dtH8T+MXdMS1GfrjuF
e3I9crrCftNTNOqkC7y401BHJX17C1mZLD08pd4P4ljgJc3tP1aWlhnVbhV5dmZWEGu9hb+FtPid
RJmnr7nICmGFwbW13Wbd1FBgndriJkJ3uSoSvwhJem6Z5GoygaHUfR9ac//vAwA8lgeYDZ148cI0
5RJNweDVnNIvyy3afAvABGzsdF0PYg1QfYl9I/UmBueS3B2MTjAjnANQLCe8n7o/YpCgUCO+dk+E
7u7lZvLtuwjCwIFDEuKNSMotUIjTt6Y9LUgPRI1a61Q8nwvEhUNxMCkqwwT71SnOgDN+esQx55nv
odIVthonQgZ/4WM3JNXpKK3Sw42+4nKtFiV4Jw5n5e4u8Mjh21qlDIere5ZS/Cu1TFJB/fp/9qPe
xfZHZEfNJ7F6JydPVrdj6iKt16M+qxo6q0iuLm9gBYfaDHr02Q4udZ+HDYAS1C4ULs1BQUcO13vn
aDTCR3qAb/kjaNATGWtSHYnGL/Iu2sMX3otASfvrammZQdMKuFtSNmgtezbui5zfl1ydx2t1xw6H
XenB7FINymDdt61JSsEGeujjNMrCuQuOmmeK2tc9IM+rtXrc9/t06iFb+le4OCB82+oPRY4tRrID
3uwcXcxb18eE4ral6NcxGyBcb+g/qDsW1OwujY0kZ8JTdrHRbSJUTWTsx+I8e3xu086GeFuv0BTz
0TZuDQndwlA5fTaFCwksh8wWRz5NOC81ftIxIRo6en/0FPlO4Z5gL6wCz8Cpa6E+aK7Ic4H+K0vT
pO5ULP74qxNn5ecdo/pu7WqCglwf/3P2jCAeL0+g5K/2jwetFYVjAQeUiqf5bDaZ/c5XRMewNzXX
UU8BHpeH5mGAYmofO0s8M96jqn4/KFhjGWwf7KIHcH5qQ9HdwkwLLqHsyQ8SD3H14tGgZk26IqWM
sIzUdM8mT/6Ole9lchupH7QKGlmuoKPFU/8rALdejImKSb5gW/Lev3npK6DokM9Vf4+KRft6V5P6
cuop9NqTjsnQm2YGHpr9TS9Grar0lAppHxF22YrM6ptfndisWysK39TyLfV22VRjdpijyea0ybLf
zU5bjSPznhn2fNyrBcpnsnd/X9aLR3sJGtvjQaPz/vPYe2nOqiDiUcMSSBfeM/7RDMWPgCJj9TK0
OuhJU9plGhfLFSRsVjhXatbh5jpiFUfRq9vLBcpasY/l1r+KlkXLfwEXQIxirQNx4g7QAPvPPMWf
Jf1aziCk1CaCEcoe3PCH3mR0RnXNmHRDfk0p9LnBLV27z56teG0YrEdHUNrPRYr62+qWhdRLdjMP
v93essarhRdtQXD3ApysVuzULcov5FE/UDYHuiVXacYh/pOVgrrD9iC+8uV9/fRjTKreU0KA+xSE
6DHClZh0QA6GGkHdqUSTJA3mA4B/YFNeklmfH4plH1fCne0ftywtGc2Ty9p7cZyIMeogfEkKQwbF
T3PXmzYjmywWjS9iOoivNyXx9jxSBQDyCJAewa6XUPLBUnI+6nEKH95NA+Tzzr8ToJVAqAa2/Uff
TOhypHL58g2dnbEhWTiYFoLVTFz/Hs0wUklG4JwkuRPEiDbDXO6nwMj1eLcH5UHfMn4uY7HnM0/G
1jytByT+PrAWDXDsoYL8eoXUgL8/+6KgMP/kGvuD2kz8qG5StYP9uZLHogpWJBmFY6vdWUrb5j+W
sm8gnMf4GCQihzkc+9/fCceLODob96z/hyTVFzCTBoJdL77tPPJFnl3ZEaYnPqdIfmt5KwQ9GRth
hNE9gSK4R23kXX1l9+Sol4PGLs4trRZpp6qmdXTGTjTP2OCqyGmZMnV9E9c2xyPTHB6uwpHdC51y
YaPFbG8e5Kd+/n9/FnGeNlBL2akc7URvpnBOrNbGa+9LkATvIDZhzs09pGo6SzYin5IHZeuQX4yh
TimyUAA6YtSppH40Pknrgp7VEOzfB3ZWBRjwFS1lLppzZA6TOOEPT1qK+eajxmFJwm2MitP1tvQE
CQFES1pc96seEXtPER6XFXyGky6ola2i3PCLacQEuAMjoUufggSg+Lyi3fYUqLBDLZ7PCR8sklRM
H7aUDxzhVrJ+vt6/DMim47aiMy2EICZXk0KYvhUahKeMtHSRkmbNsya6FqW7U6xVqqa64FXfrw0C
qyRnqOIDO4PsFzuPz0sY6Mhby+KZDgAaPwJdDLT1+DB1RBOFcdi2XByTSsrkU43QEssNIjz5zpJC
emXeQpt5SMvo4YH2tj+DvTBnafoHcGd538VrCHLNoQ3z0Cl/rQJJF2IaSMWYxXXsOQ43PubDK2Po
7QPjyxi3tIzhuC9RDSgiAuOzpHfCpq1VklmM1YrrrJFKEdh/aXAQT/6xjW7bnZsewq1O+gkqsLgM
CrL7yc2j0QnZDnlu99iIvDprv/DdX6Q9wNi7Nll8SOLnom0KvJvRm6GVE0RketJYGfn8MFPbF/xB
TjZaR2ASaz8XJqv0X2KzjnlbqH+hSCO/rt+BUj5PzABKQS5OBVV3fXSsTr3C4gd7aVtowLaI0nR0
ZUjaaEChYZ44L9FqHw9CzCveguApXWeqHALdAAxy9L+BseDOo2cYp/xVGqVjsVSzegDCEIFWYEGG
VSTbxYsXH3znCFpt9kdqEJV7b5vbFW+8qaPlny7n+DgdLe0Rk+vcpmCbCMVlhn9pv/8QsYc1Qbq3
N8PKXgHt9Gpp1p6LIoBwRXUw9nowz1tWpq7j2BXf1Ctkeug0KDgi5+WG9o5+m/HStkTaRMRyVWyw
S4Mf+nV4lhm5T6QjOHizqRex9F3KHxVTdNV94nBSfWg8ApozgTfL4gaTozSzOnJqEm4CXRR4iLl4
Ylub6Sg/vdTaGyFdrgYRYmCoZOz9pKLZKDxCJPNNDIrvlquHYDkWK3k4SnVarqR9q2BEDiNTGl/v
Aft2vhJ4BJs2hhtJbUr/DTrATcIOo7gtqf7z7VWJGsL2nsbJUwa13wLADcM7u83J7zX4QaboQlpR
YIjpC/IvX7rPY66x5MvRNx9RAeK7cfpCHrDd8O2SoLmDngmLo0QzBToEmwtTHvqsiZVQFYKPpJFZ
eeSLhHGpRr0ymOsDuT53cH4D1upGKwQKYQZE5qDauWb9XJ8WA73TgFZ3GIFO6QwN2g7JPra8DzDE
1ap1btQmKSPdaFQPvodnjPs/GKQOOZvY9BUq9I7Ut0mbQXWHn3+oQ8xrTL/otShyXxzFCovDBWw5
HCKnfgA6+UigVbYyv6JZKPm18Nj9FgJdyVDwkZINjWnj72FATPLq0dcw0v+VJFq+2DTMtjnrMOVz
3STw1PtNmiIpE+ukNnQOrSCwAPRmjQRtNRX9+e1FWDYp/L3QPbM9AV7m1PBwpcv9vJz3KTIpFBhF
W9+oH1SE6UbqgmrZcHnOMJGhBphWHZ/5ZVjKfljDJUfrUNT3JCrdMeuKQhRRnFxlXfLZJSZ0x0t8
wS4O0Iotb0QX+/905LnVQWNC6ePtSpjbhakxc0qJpIO9iRR8Zyk7yvgKyVmnIg0bMmMnGZq0JwSq
2+XIESojcZV+yzPzEsgC2PoOMGsvDjr124/omQ10Mu/Yr57s/G1ldLXJNYSfyEqeeTyT50F6q8Kh
ciKHfpf68XmFzHcIECr0Tr3LDAzVLOj/p5ghmFQb9Q3evBjvbXbTZBUVtvZ8o+9mckbm6PGF3qyc
+oEBqF7SX1rr4YyLLLtzDwePvQCC28yX09ARiOs9H5bfSb980B3nlUWVFURt97Dm6/k6tj1HX2sS
6uFePI6wvATQ4d+HPnDefkiIJMwGVSOr2gy3YBvw4J5zYtma/xEYUhNzn1guHp/zWJYGHIwyP+b/
pBvKLfendve8056VuCsEnVCDNIzQsU9iBnQubj/m3/Ob26zl2o/SMDg5zWhmhoYRanCIOY4D04Sq
HQJaUZf07xfEjMO/1vIVepLEBA34xS943iQO1XUwcCgz70qvK1xZUQXlkfCiw0fu3LR+WncKz/h2
q9huM1eeVJ/eWeimYtsU8FUn40tLXG3uhGLkfunWIhkZf1YuAvIJbc1zUTxMAvnGyOTiDtNuZg+J
yQkno/pkPqqwMIzRelcxsFKLRP+B/Npvw3+kYXOVDqV0hJTi0wOHu4vrgl8j6kTyEmDvfOfDLFrK
R3lfaVfn4/qy1ESUpSNzbID6GcYJLR26nR0kOBZ8YR6wBR9d0KNlBs6ogvHLxfOreo/apxrHXjeQ
rOX/lJYkVhOMMe6r2Gw7eSJ7G8o04WMy6d0aXXKPwa/Zrba8fqzTvBLwzfsZqEL7uhz97yq/d32n
krZwiU1SZUXe79GNXzNHLWUlrZJ9Cgggb9avbH7p8QpvdJB4HtHZH2tiFTe5+8ns6GTeUq2xZAD7
igPWCJvyVL9y3F5McYR9JuaR1DkrySnmWwC8UkO/rniu2s4XoY7Y4LLCPXKSdGaSVmYoMLOO0EsU
annpbhBlx3bGGP7InIQGZHBPi0ulWUCPG7WCGIWymW+QuguIYX+5O8wSZgK7zi4YOiMTJCqF58xE
cv2q2weEiFk7P2qu5NbLN65SP3pmK5hQOdsM6Le3WZ2wXe8TXDlAX2PkZksem4O+Oo0lca5UYG7q
MgM81+8RziR6XX4+bUkgs4lpEp4UKp/bY1+f6m0GE3qaR0s2yJUU9XiJUXAnhSsASUfFblL0I9B1
LbbVXYrgnmTbsfsLPRyYWquXt7r61Fc4A1Qjmo+nn7Wz1aQAFNU9z4y8q6ot4HcyGvDXnw5G/jpY
5/4zGGAO1xyLbcoIe91Q4KkPtjQKno28+u+JpiIQd6LM22auZNboJdpe8r5X7LCjCNUDWfTs76GR
KBAbzaMJOF9HXy5AgXXz8XAF/xbUP+S1pPAN0k6h6mlATdfDqfAwa+0m0BZzEScwd4rlFv4Zobs1
9wbDUgynggtqvFbPpRIRZn+P3KGsD4lw1Ul7UiKeg24ehxYreIcyNWrRvxcphipDG1En/ne0RSlt
Kqb5GF8R9yo6KnKXs8C+8qFUbJJV7sQpV4NBoowK3dT0AjsCWVFnWFHql0rdJQ33RqioJJcMmZPm
r5pxyunn3Sn/bhw7niCwj6M8mxRZNE5RPCCBxwy3ZlyLzmNprjuanc4Fl5vUR8aMahmBvb4nRxMp
6EEOOcyD1PEX2EHVNKRkz/iX+mSoq7diSzgCxHYd8WD5TbQ38RK0KhjvhjcT8ezZjiH5Sl5hLdgP
tGD5/sy+phNLVAqSbrh+BIADvEmtxfLLFUMEVGvuk7ImH08s/9THedje7E3xznI7Hfo+Ji8H7ibf
xbGzkM2+CodvYzwniM7HtVrFJccud9Je3EHfhmV8R4AoNwJmZoK43oJ9rdmVv0Yi2n7OzX8Qrh9b
yuVbn1b9YsEJ7dZnd2dOApqy7ymhjdlHqnBLZdno7SU2mWqcpVj2LtNnNd4detuBruyj9FTcfry0
nYqjMmwsjpfExyk1u6DL+YEQwKYwg80telvDsaFz6YlTZyGtG64tfZWGAF3G+eKBlKfnAgDBQtAa
pXdUmBENEt4fak3euzIv869xi6k2cKiEPsHFOLcjgOpIDAtODtaMA4qBqQxCrZRMFgYkbyo6lJnz
l03T2CeIouljiB0cfHACRG13Xz1nMtvt1GxuW5qRl297f+HJeUw+Kt4gWCezHiSawwhu4qSzQpS4
GQb5Zd5DiMZo3K4vICn8mnIW004Ss7axz6Q/+BPc7YbZOnARwda1MjfcSzNjK35xDAh2TCMCvbWn
Iv5JclNPNk/G+eLvdX84UhGOvAxCDuCQ+wAvXKNI94T4+unVdMMpjfLNI43FYe+ZfkXt03Jw5ZGl
Gz+Rt5DpTtkFdzWjbyABzDZQ6doOsNWyKDJ37dWCDQJ6/rLRmlY3TaFE6FQu1pq43JhujhcuMGv6
fojFCUHY0p7ETHLbiF72z6W8/h5/GU6y9mHOF+lsNJwfSe+8h23/v1lJM97b74AkuPYJ8Bs1vkX8
dE/4aGPitzsa8qEco2h2oNivuNoDG0AbFmieAIsTjOyNnA4keMjllOUWPb4qnu8rk0aHxKOCytMs
cJ9hUDvCWGGF9GOHr8G9s74IyXNXJmH69J9zfq8pGFHPbGxybyP7Syydkkvdo7HuvNHsP3D02u6c
P77rOqIUAHO+jNf02YaUrUzw5Xgg0zSWP87wTM4T8Pnlsn3XD1VLvbnPLUAziJMPbdU8lj+5vcCp
RMOFY3wAzF1NbVpW4/0UJYJc9vFQ1e48TjHuyK0Ne2U6BDBSokFwgA/jWU+/gt58sZRDgNyY2k+y
gY09rEoj1i8MezYmUMLmLoZOBaDG6fBUvLDIWql+uBd7SyRefUlDHjKyFozr+poW2nNjVDzNNoRl
vOHuiyThDJ2sudbsrTgA+P9D7YNlMaRmcUkXGGyegjDkX++NVP+STi/Y61AaLoMEIfuORzkxR3hz
d3akwdomc+LpEkRqW895PFit6F7JzFLHabs5eqCdxX3n8M7kZcaVTfkuh6Wab1sk7EA011RqYcD/
+CV7fZNR7wlQ/TCPdFsoXcYHqA11e0G1eBgcE8G4pkaCdcKtHij6s8kS84N64Bb4YVDNIwsNtQNm
k8zMRRsvK7f0LKZ7JCbUz8trHVaM+1MQcCBKqYp499EJd43w3nm2f3MGAakgiJPTHTsT+3yVoF6T
5mVUFEws8GGO/rI1cade6l9G97uuGB9e5Ho8axWA4AFK/La6FzP2r3OU+jP581tfR3/P+c1Y9l8L
s37TmtRo7GJUWp06wWWICP4OUy0mywqDQgclKuytRyrOW8MD6jKRlEE3vcu/YfZvA3Ko8xAYfDSL
Di2ZQ4XV4vbhdWjC809G2g3Ppf3N6UZV1bBk1jUf9ubWnX8mBxqTsA3CnQPJsIqkExm0+b2zcxBb
1f5uM0p2uN8tBYUmgUJBnyeQqNsDcFLsM+ruvq9fCpenP+Kh5bw4fbQZ+1YiUPj4UnBVQk7vBhYb
yf51gmemz43miFDFBakvKxc+TYfqpn+ExCRyd/ejjheK+FFlHdKt44zr7CGYZybdlP31EswHIyw9
oULAU44jDWMRZ+qnNvN/tW9dk811EFZTnlRCFQIq1QLCDHHm2siVQA4tmmf5BjtEh3p4yCkOeQBk
cfVgHDA7IujiymgowGaVJ/KcmUargZG8D//MCt4QtBO6NziSPZ2f22wLdO0QTh1udH5kz4AtoVvA
mryE3PbUQd7dG+emb3/4eM6GqNCgEf9stGkAOFcRzGM/CnIwuXtAcILeow7pGQjch4mY8D2fH4PW
/WqKBJo+XVSFYUza5VeVRIGX7y60hCoTh5ED0rCz/o4BbWxZoUWHMYpEAtHgO7R6eBWrxaKM7GJa
/DAjSTR1Wm6F8xaCCG2HKpHQ6DzJmdY6FPM4tfzb/apD9caQs9kt/icGfM39U6Armmf7GhU4hy5k
PNqCHYXqD9AaUGOjep7QwDwvPo2ouVfxFvWMsEnCVOVsvBN6/nki3hfHMSvX3F07QQQFSf1JH023
k3q3DB6TC2zyA8WkBW7K9ioFdRqJ9qH3hlHrGQGeq3JskMcBG6fsFy16PIHNkU7q6WF1pgJ/atBb
tx5MKHQVAa34DlT8+84icY/WJEMIeK878D/JZalmMtLtT1BlQK4VpVpUkh/FKhB4tcmrD/a2Mpnv
DpQ+RSonx1IgXmVY3RHL0T9d+ptsePK3pUaiP3dQQGvosu1RBuaV0Tg1bTwR+s3CxLY5SouCDpff
XIb0tScjNbSYaaz8vJ62awAtomO2miGdQUQ6w0TzYDKEaYlmZgmDtT4sjFHEbveszqJGqj066EOy
3q7FfNyPFE7pEhZD1nNYrTZeaiTg8DkQlxx///cRZDItvRCB80EpiyAXlmk6zmPRpHFM54s4JmBS
GTJ1a6VR52ZOpmgVBz3cSfcUAzeDUK0jt8vX3uCeAwKizcWZaEyVvySVlBxVsLHlRUxlB/2y2ogv
63twfs9OVb5wjNcgPCLv77aFqVHCWhH5xjm7Jcl1WE9Ac8sCXHMsCsb4kRk312oiefi4ASEHj+TO
EkRgkTYT9nUCbFHM+al2LFdtyb7R8WybR4erGWuHjShP3i1plMk4Az13g4BXGWYyW1MSOymMhB7D
FWOVA4PrllgfoUsbATOVhUOJY2futeNPDxXld3T2lexph+M+4TJn3se9BRjEFZ6kg9qMJ7LK7YOl
0gJ0iUp1sitlpyd3w4ei1fomjmdRt7OWzVg/xhfbor1vFJF8cAsIrSFTX6QRxrO1DNnQgJuJXot7
zLi0EQD/gcPcGwLCuccDDEIa5a8ggQMyZeopBa32iGZGL/Wzw/fikFpBry+/ddWfnuvmv0wqGOLY
GZ5Dfgeftwymdi7plRNJyPKshkqA039FtdwHIaOC+4la5VRH7e1QSVdOHLK8dGn76d4UEYn2K/Qs
PIbInsscI1huKwkiD/MQBmuB4Iz3A3ozwa9ZAifwwVmHqochh77fPEZOcb/cFXm6y/VUg9YHv6eE
vf/OYKx2Rfr2xRW9Xpz39wZnWM/zf6ovQwZocTMG+9yfhq36RJxx25FZ988J19jtCOx7wP/N52F2
DSRpzi8srHdF041kbqP50NlwKqKpKBfsT8PlNs2QNT9m6gahmCnep1AjnB3OAwut59mckNETVjty
S+zbbpJFm9BFaOhGsHso1u0p85ega93VvzmNoEGBdaDMKZBFSQrzUr9GME+5u57qIS2n18eoFXhO
/5i0sGpVwN7iIMp08r9bSzzVd9ZE9KusQbVpsRyOzNDByCwen6CauMLEdfp4NKBNACScm7kDnabX
fiagbx8fYYFWq5hES5ZwzB8OeRoLlA6D9a3DUccc8f1MDu/889Na6aAUAmT+CVHeMQ0pzfeD8fKX
eL7uaZAacULfRuP6mV8m2bZ7JYVp46n5DU71Gr/0pLQXP704Oj4QfJh8uTzTQT/0DseL2LA9ykH6
e6C96pVURxgb1z/C1CIOVQTOnPEHrMbHBXpHxh7upD7y5zzF/o1zJghOesBqUUoo7ronmsjn0li9
Lcsovy1nHst8j5mvA1jnVpHH7AxRtGwuqaPuny629sxSjFrVHhvh3L58f+871BGDRlnN0y5UjSS3
Ov3M1oOXOgHB2Zf22Txfu+8E7ottMWg0hJt35o6E24+UlgRG5dJldzN9+EXQVuzG/itcKX1QpVMc
HnXfkT+ACagne3fmRsKYiDptU3E8dC38U/kgCCIHf+RpbEzIuu7Dkd+pJz7ixVSol+ml9X9G1ijB
WcX1MYMzaPqTgRESTC7KFPMB9P8KLbib/KjiR1TJC9e8LfuW6aDHPuPLOL2tgvwX0gfxvNUh4msZ
uL6GCGzyQ5nMf8dw4d2Q2crjG6ZnHJP/3RRaUIqQbFnzCo3/85xfKciSdo1qDpr6MWLT4Y8BZG/G
JTtnTCU/2gF8UJjLwaHHhbl01dCR9cdvA7wt+aTveifOONF7hdQLXNlwBebjygR2Pn31ek+VAbqN
dMtgHl03HS15VXNMXCbqm2qJcBF1q/Pzharhhxftoq8ej+NAJO822QlaPlkdOtjS3RMP/5rDhsks
hR+yS1u+jrParSLjPlBrLcJu29np3NdyZ4Nnfn+ETw/L9QSL/PyEBKI+tZpuhSV1iAcgH0HMglyz
Tj1+bf6ZkUTt/geS2alKcBA09srm4XuM1JoA7vAW2rOwSjps+dkdaA8nWGwFtlcSliiXHzk1N7CI
DgmOlAXoAhpTDs+4XhymO7P9rDOsNl+Uj/SUnAtD0JYEeBaxvS3q8RGqTnQ3DK7Y6YqN1lnbMXJU
FnE8Z0eKZdrfV3u1xDU71g344ihmnoht0FfO65MSAfM2lFJiQ+QI4qEdLlSw5bhDVuVoQ1mMVIjF
UKKquhZ6ggpICEwF6VMf+MqcL7CY+Palst06WbIaNxA9ZbEaloIPKFIKbiuXNIgXbn/pqQqEks4Y
vo/4DBsSRvIG6p9n41bPYN0mdqbTXPB36kbmc0NQLp83vkAyxc74RCeePiOme3aPbLCfZ4mscRuQ
OLS7f56/MepDIG5GeDhFxBcnPg2lotxRRQ6N0E0F6svVqFwOAkxxfxSXCkj2N7yeXJgb05ztK+tN
nc2qf72lZg2oM1G39Kh1Ofzy2qQMNNWzkOBwRQung7PkrFp4khroTQDE0KiGBk1Kqe5OPzaYlLoW
it2iCWW+YyjqQqWED3qdSHncy7XQ7cqRYZwz0CcCOH7QrHUrPqEvapSFUD0t1z/jQ7inItYdbpiB
8MYxmqCCbbgGUotLkj9Dxa15JQC6R9XcDIto1F7LXdRfZxxDEwLsYcabWHJS7jVY2vFMPRbLH2yl
RliBGcbSNWI5gZRY8LiVGChR3NuyGIB94ZblVG+w5L0Nxk6FY2G1N2FmJ4CszPqpzXBlo4tAouBj
hnUO8Dj6daTZGPCgHciK4w+ypbPWb/9c+Z5+lonUlaxnln+xiQCtBnbcYkoC9V+LBgUhCdOEZi6K
uloKlKBNfUVO56dDjP+g5c+HVHVkwZoDhheh+x3h6ltZoxkbrQ74ALtKGmTyXD4cTbDwWXwL7wR0
xzjCzXf2EHD/1z+5bRBJT5BzgKHtrC38b3tGXrPAjEoOMUDWnVWkt99mSdKtJVFwCJovVLRMUV2C
sNBRToO+YVB4MFyDRUuRT/WTzez5y53EMjai2RNxylkaPhnpUqp/Q+Mtv3pHTL8jhuvi6Hr9uJnb
7yVu0pwW041r5dqiiYo2QjRTSq2p33t7jsq6qEKHfWg8EwsUV9ieEp5wfYLgrWTPRczAf+7IlRhX
WLUXMyadJv8+MlTCqPN3WKg5NTeDTKhAztGHyYM+GnHspg9wSVf6goeXrR1Jh8psy/37YhpwiVUd
B5BGDgb4PkPTLGIoYnHZjVykyjJZrdZD/hfYPBwqpmhyEjbiEjsNgwiU9sM84NBUZjASWx1+w4SM
9lXFG2zOsPYZcEkyCUYEOT4++MoYzRVm+sEB9R1+TH7l0YX4tbBbPEwv0XfnFu5hgVLmkKQIJq1/
EcfBmoDA1hN8wk3kB2/gtCqwaJfw1A4SGYokoiNee+lcR9elujMboegOxMADMPheP3gdbwfPGMXg
yxpjE8bDZz6zA08prqd7nPsHuuoWb9VykYV28Whi+1a/RQczRLUeMm/qocF/o2Y05XDlf1LnFrpI
xhiSAU7hVrAjrd4LpYyPF9ZFNYKJ6cQKBnVcHXaI7gHGfop9aT9OdvLHcy7mgAfVLmVZWQfIuxwr
rFx/TaBBT7l86syLbTr6gz/a06Zpole1bQGMAmuFt0DLTGXKoi3Dl+4/k6D33BC+NzgG6Ol/NMi/
wFzUXAp0Sit3Bmc71ldCMf0KrtUkd0QbLs71l6SbGm1eZVHE9VWWOVJElHTR69kdVze6VfVQYnHd
dO7BJNAJ476Gwprw3BI1stbKvJczuxUltDsZOysqif9qrtcrId8mkx9UjGUYuxzW7OnC7/CRR/9d
hSLNF4znBPgg6ZchjwFcdiG4aboLBw47xYr2gPaHLgRnbEQQzN6ZEzRcMHG382lH4GnQ0DTGKZKP
LPJnbL/PI8zMv4pNQyE9mgSZR1l6P+huw5jYIRszxNvbSBaRm0LYUue9cRrTUeAiNYdNagYIXTU5
zP/TvOYETCdzkf1ZwmNvO9YrRZMJXzIiMQz5Mnd7uJsUfJD6+hld8I7ukixkVpXU6KUPXWfuf+1t
0FTcu8KuOR7E7We+PcCs4iAqUNep66haPXH8KvqXtEnAB1pK97QpR6eFu606GDLo6iN1iEz/jU+S
AuuZ6P9FHwGroZNLffte5ZyvfhiWcZ8eXbRTHFh+kwA1beDwXfYRCIIDNLI4y9R4LBzsrojSaNmP
G+hUjZbBZxhSwqjKJAhbNTfu+GWfj49dLXVR9aiT+48X7L8kW3wfTqCPwRs8EMqr4IvXnkipFWPw
SlIIMrZDSBdLK8esttpnLHurv2duMKLegoMtxHajPwVoNbPxnNTa1F1/twX2hnzL2ITmWgQJuhn8
IJDNNB2v3rMrkPhiDSEWfVFGUmFH81mBNtlZd4OE+8RjDlgFVSLquSfDHuCjFdL/TXvz2V9Tfioa
FCQG4ow+IE4v12Ub2kbRSPudNX/hlkusuJGG6aQFBAPMhNlrjvEvsbA8ET5bUYlWfhcrgB1h2CQJ
cYoVA5r7JuvG9FeTMdzQKR1jXT1VglYrYxCrCeb3FxoIemMrwc/Mz1VJ+Htwl/UF549SRzMD3/43
9gMKipRkqcXw189qp+IwuJqAdduGEOaD4KvjEUg1p52RWPnycrL57QPU0bQxoXVHEEGcz7Fy00FZ
QTmFVlOuRYDR3IeOgwERFzWRJQeYeRukQ60gLCLtmHIRxL9xufp6hjzqfuPDXo1sRCMcb54Xuxaf
3+AeX8Ufii9MvqcLleh2uxtafCznqF7ecbcuuXCSstiLzuZvv+ex+7USFR7bivchw8PzPhblF7Nb
nAQy/TQsFN2P1PPEcF/MjrraRdSKbtJnMPe8obf4aKlkwlblGkOCH3gnCOOJrGhbsnNB+g1wfyqD
JLAjinlmVP53gac9I8lMptcXQiRDtBAMWHu6LhYx3wBA38JElNW221N6/6wNXcjJWgIjVdL/6syD
hx3u7CaSSk2DsVJrJ3vttvQlri8BA6JaiM6VZT1RUmSSweg0oGtJm7UfMzMmSYlKVph+WhrdyMXf
f7YKO8UFAnqE8Ji3Hludd1/hnnziwb6oq1Mq+KRG+DYfO3T7n2ZpLMuv+X0Vd/Q2aEnrvwvhLjEm
tvSGji1ZlWMR9d6JBTvZboztp6hvQQXt0rVfsvhykvLQzW2o5+r4Y/K67u5jBrl4GtfCcKWTxlHU
/9xicLKeNHNhcDCKTCCEIx8VbiBl5iDAzGna5jFAtlf6OSigQZ6f2+p/2pFeFrIfcB9E0lGDTbaa
R2k9N9DeBMnBKxymNj/RIGS82ypDprHxT4dZ0X7ygkddpYiu/IDp8nrmB6MkaXHu0kLIhGGYn1bK
z/m+cNxcZguOgDSV+2s2wxOfFuzevWgvdsIJ8ni3H5xqwafhW6Gu6xWt8BLjuy9m3OScwWo1ds2H
ipvIqZTN89guIdNCDL45iDlZOcoWaB0QehBt7+H0A9Gfp51gMpRmTwBhsgDRBsr+daZvoSlpa1Ou
nD9vTSdIrJhvp6GZP4mwvP2g+r9vrg3L3hN8k+4GZb8g9TbxjaMu0UBfrSHhM5G6IZYDs1WfgKu5
lhpPJ8mmHjSQFWYDAdLi7ELI+9MLirpwQN3+smUVvoX+tzRzdM3xtTeALeU2+g+qQ4BzrXiWmTSv
pKWTBu+H5kwPpVDBJSIZxBU8jFVPix89LB8H6D77Crv7qDGnJ8yoQvvpXWStrbmmrSpeH9LuDHns
02E02Q085rQCrdfO2blMfRGbuqLluV0UlrH3W7GcISU25AKPnBkiyOIrch1roGTILHv1MctPmrwy
G053lp3wAy1bn9B+SnE5JlDuWQ3X2AZ4OMjBxpsKZq87E7sL7tNMwg6RyYWKghm1MNo93zXXl7E8
nSKRJb31WMsjNGGA3zwiVHXwRuZrtstPM5hqDPVo1BjMEsbY9Ft6kF3d9DZ0UFT7bmWuoJMHrAhh
l3vETAaiW/K4dbs8VEAuyeTu6Y5eTFJStSA8idvYC/Y1o7Xx4HLylxnnZl8MUoY+OtigfEcaA8i0
rpzhRTjTpicGynvrYFB/Haf2F4EzCQcwM5uG/Or+iTa84RXyLLBoF7EZraPAPNPTg34Upbj9Yz9K
61qgBBQGkcy5pKK/HgtYn1dUazq17G8JPDhH+rILCx62zcAGe1NiKmitTN6ekrdSSoznkhQw+VXM
G55PNJ6ylhbXGG4wByyaJImqr8zY4YvGkFaN+k6TPRvB+lJPiY85+hss8NsKn2UXdOaFgTOnd/kB
WVENt3vqo84v0NtY3YyY9EPbM2CmSPae8R74FsVUJFvqrE68vgvisnUUE/pvNBQLBXIdJP78Oq7y
VXA3ukjU4VBENnJoWqLaHTnqzTDSfR8OQhxYM+zbQJxkaDIYsEje9cREULV/mAGzSnYeNtcdcNlS
+1QkHkYAIYPWK15+qwB/EOC+taVbJbTAf8XFPz3nuatCSjF03P80i+UEE4outIF1V/voOmqNvyCR
8UyS/FYM4avSk8n/pdrWU2MNP8XrkqZrqmVsGDkytQLKtUBsgZ1dUtY3OIDiIi4DjSTdXi5oxMDs
AV517WARwiE9UWbzHlqx10U29HOvMCq0QVN25tf29VF3/pV/RhEljJQd3BOqfE2Mv5GTshW6WML6
tKzKXY4XTkzbAZ84Tc4r1dw1XiLqwdlaME6t5eSZCIjtZ3V/ZjCqtMyTfzt8wFeQME1RWoSI21P8
fIHa19OZsV37xgjUPeOfqA5QxUu6vdeyxNn1Ii1u4rnFdMt3vJGcITnS1BNMFabXowGyY7Y8Qbb+
tXA2uOP4sqOUUqydvffZ/RBAWFtXvN9ToAY4MOQmH2k/8WoiUnQrmgbNiSRU8RcsSHsiMC367sSn
+14haa4BsQBewQzVs7SVfh5EdJ64ozgXtYpfF15zF12jG+xxmkCmo6w1cnN7JJ/9/hXCTKJFEr2d
ZAswotWjn5XAaAd6llbepFAHmEHynPdGp+DZ1+sKSC0qdti0os+MOj7Fa3LOjPSP6V4KnTVE1rLe
vSqMuqa+PxYeEoZPIw8+mt75v3ffxfiamLKI17CI8NUiNpTq6k4ONSNXRihxHVvjqty/Ucp2qF1O
Pg3ssB0gsaH2P83su99xvEavNWE6GhosaBeXXdDWYWAPvoyVexor0lIe1XoicPFiCeZxapfJB+FS
sNNO8SCqLPkKEQ7tqMqcKDmiPTas8fAht2pQK3r3GvVFIteVE4XXtvmSwJafPhkiH2B7L70usvLY
TrxQSVlclsdYlxTwy2qlgmABP59oHBllFrPpOoFeLOF+AIopHQPt2/XkOPS0rCyGEKAiCKelXPPT
MiiDex+42aw2YbGMq5l1Zs6j/l+rtwlqfmYXV7AmWMEekqHsV6yP3Jp0BacBcxGfNS4qOAoOV32K
QTUs1f60VYtAUtvVhwDoG6bQHuPDIs0wsfGU6oBKW5VA7eQyMdTN2VOF1x0wSnMPfT+C6h3c1UkB
7gVMnr4J7tGJIOgfu8VqhZlme1s0aEsiGMqrzCo01nZRfYteY0DAXghVsDDQRg3ySlbhuOIhe47q
tvmLSnRdtlXfopfUq9EnmOrTHh2XIUrF2WC/Ab0BjvAnEDvgoCBHssGsuyrJ6RCNKLxKzIGBJDq7
mbMb7g9uSasj6rnvG1qb6EWKqpFVKeFSz4omp4FG0mADfBEX5Flo2LThYRSbtzmUlc7l+vzhHvDZ
ywdcYbtOm6eRIXgsVF1GAvguomNrCrb2ifvDNI/1FmIsPdh3BrXX2VPXK89GO5lQphCvB+jx3J6E
cKeVUjkh/92ad2hCtbEnIVgDLtQZ3Zf3QcmBbheYR40MT+LDdc5ipfYu06LZqDHXgjpam9OO01Gb
yaIju9+Ec5Y3tl3t1H8lCiQnlx38wjB8PGuw9JNS3WOo+J4AJIjK5seb36icw581nOhVqplMx5tO
QaHP4NeGkA45J2RGmldftgFqVXFJp5EthgTQd3vX2QST2/D7IfcIU3EGvjx+uTHTA0OIlO9nHcbe
k5Ray07fd55Gn4pvC22ok6xlxI7oyyLp4DPGYi71TG276ucj86NheabmLRdZ36bCpVNxPwtfqN6b
5lVwjI1sSKsANs1voeSKRFiamJeiuLTEQDPYcIMPWUUbRtEFG/7qMKJvJZN0Trgk3niFgaAX5BN0
H0gOp2+aaM1SqgDgFOpn9qykAzB1Ito8exQHuT2AtlQd8LlxsSgiQXd+sB7/kCZk5zViZ1cg8CMl
hP6dr8RlzazkdP+QwvWfJi2tyNnZxKxIUbt1C4BgDw1XbEIAnDCDpV/DcoJViE252pzvQlyOTDEW
bG+BZRdSil7EzQQKjeSOEh5iAqu+gXkaAx0nf4yxQ/rBsQwupj4XFaW0vAD62w3h2qTcEwsSJf6E
D6TlW99Rl+UpwaIJz4e1HbKFVliveg+XGgcyW9YJMkh5enAkzEG5yoKn6NDRRn20b4yrdzCNXWmi
xJ8ZglCIgQWaERd4w7u9OMbRB4aF/+3zVbbuGyRWzsYF2yPd8oALZChMLIr2eTuJQWbLbOUYXTQX
RcsYNaie79Uv7MrXAAL2aXSJbFTcklzIunmeR8Gnthi/hWLYJuUtFEsPbfOUKWCnFrEGhhMJKlZQ
W4k1wnpn4fA8Am5qljFSIcV9T2SrmG1R81SZEMjKeps5fc5x046n30hja0Ws2XgesqSj2X6TGb8r
U8XIH6+lknRZKyiyOsPMea/uNb3zdrtIlrfqnsA1sxRuvvcj+J+RvvN6u98S5oQ9rfkNVwnd0ThO
iV5nqxjJD+WRjVWl+beFoopwt64C0740ez4hv9Bc6+up5WWkTa6H5BrVT9AVfbAL0LWh1XHBUbFL
LKITg3b7ADkp+52tekGUmbyLBbAsh+fumzA/yZ12k0Q9lgR/okd+rlhPLAWT2U6G3gtiAI8fNmPx
1xTj6myi4udvicvwZ3iKhJ1w20fm6h2WNjRO0Ccj67BEGPgr3CzpmmppN37KtkPzCQsmbz/CFXOx
db/rW863H55K1rwnqWBO7btlEyuesEHiKOkN1lKv8EDhRKORo8d2DJJnjmbIcm+CAJcsL9jii8QL
CRJe6UrVhdls4EuWV2+BSxnSohCsDoOnUN6ZGE7hryQA4juMKmZpxTcbxiKXMvYmveHDoQsIA+X5
OkPVyIDwBreO7gNVCm5w4C+sfdpIZ5WmU01+vAktFMbjAp3Wk5DOn5RV+P1Ha0dkD/PS8GfYyITu
v8KyzdtuVy18Op4Ld7ATrxw42htPZdI4TGkmvJiSz372FO8OPwrcT62pxuP5Y3oDFr6cfBd2wM+n
eEQKRBsl/mn/OSqmSYcoXcn8alMvrHacMRu6lOK9S3l2Q7jCFv2nImwkS4LJNUPeAfdWbuXJIKP8
Ed62F+GFXOTuTVoOTSIEbK4Ns/Kva8BijhQ87VqMn7k551vdrDugEL4bFIyiHNtc7U7cAIk0aujb
r3YDFa5GtGn2XjTvl/9e8F9Y/VUwBLGExJSk0phcfONQEAhODQ0mm4Uku8Fmz1sKKMsRe/Gh2++6
97zXv2wiM/3eWr9d2LdfsHz7TXktHL9xuKJbCDeqmErGV4INy05E3GjZ00iNUPyfhsF0oBUinsMK
IaIk3h0ZVitP36A8CB/DtqTTPPS1BzPG0L6eY/f7eURbRLrOlpE/CrQwjWTswITYUFdQXRlwIeE6
4KWJHZb3bpf+NTTFyg+HsNB353EyCfjqUBn3x3mT8rok2IgICh/ZhpU8mavRyJ6gGgyMJC3IZMpu
zV6vNG2oWgvFeLBS5gnNQu8Wz4nIRcmSmVY1uqY5Cs+Lz6N4CSrUMnUsaujIhGc4fWpW6kNw1mid
3XlBcj3QwH1lKdoRxp5JNM61Mun5njX8gBcHGEX+pV90hEn1K4Td4Bxny7TGte3RbNJ6K3vgqjdR
AbVrc+fSkIaoe8hhUcMydg28MdpQYe1iYfIlxTlf3yFt7bQ3Xx+IRg6FNhq8XNBsKQhOgxsj8gGE
t0hfuAplz9/mCj5SwLRGa6q8rTJbssLwdv5ycTSxSMSjwvd58R2PbZpRU1mq1gzpZkyZz9Ajp9T9
KpApBYP+BuXicrtQZ0gEzcVvLgI4nD5vYymRz75V3NyngBLGvgNmfuiQ9weO3RHatmz+KWbMKr1O
iWBB4nr1LY5xuaOztsCFpfG0JO1arzJ/8EitfevpyKeEZZ+xMnCr+1gz7cI4Wc+ah2kAzY3Yl4iQ
jFyTebwTH8Jw1lD3VEsMEqyWVL2WkVm2ISWWNCNjOKWQGN+lp501ESxuEz1xW4zV4fofPoLynqLf
0dpZsx0ZbaQcOkRbtcluLnfopP5xxONjFPW3taExPqhNADqNRxJZjvkeOQSB+tgyPU8a7kDBSESR
zNhJOGLCharJe33DKqLPN2+2hJmRthUwybu3dVR7fpuhga4xQM8k1TR05kNx8Ni8lEEVerf2ScDL
XpAV30zCi5XCvV4qr9pYXHS5mDpFk5+L5Y+aS1X2rJn65UtvhTMlK7VbTrUiu44xkeZ85l+CcihY
myNp1j1BYCL1DjCzFx+Rz9m40EaPXMCGWl3widk2gKjmbbzUGjxhu+i4j8RZe1BGeUZd8crlCoAK
GdgUmJRDSY0IfeZjxvTOk1aCuj1gSQxCgK2LcIHL4yxYlQNNEddsY/VnAXTH5SvVv9grdVGrpn8s
U7kAsFjIKgkHRhdWEH2TpYejBcZO4XyVrLEiaPpQlQfghJHfjvdi72PWEopfQ49D5sbLUarfeAZl
yAr4cPFHmoO5cLfrTUXMXQFpeethcsk9T4+wRjTByWON+nfEe7K75VxS6vMwTDN8ft/80OIp7Zb2
y4h9sC3K1mJogC/6HSNXrKnD3X3AXp4dSWnToGxhf0JkqqapyGOay/Xh3pByHnbSjPP2rRkX15+l
HPwwu3fZrxiQMGNT3jMbwHOT4rFkxVxRmmUzM8GigPM4rLVUzowOr+Gu59AVbs80IClLjjcWgB0P
RrPTh3N2YBBbjMW3b/npsNS3wUTNJ+zRZQ3GEah5hvRPtVRfDuIJaO+t29lNjap0TaiobD4fuNMJ
k+x0+YuaRfOebMCJo2tFizyO5iMNoeZ8CUcy1tYiBT2l+fsqZzhOSTGLO31KH8Vr7fCof21j6hKf
JWCino5M3QX2XazkL8YXt+MyGbw+WWdnQemhp/RTnYME5XQsrBqyNAvEg9zi/sxViM74PqIvjmwm
6ygTV2Bk8iClowKPPMlHvsC2gMwEyC4i7Mob+Wv/i7YZIPrbOtEEzfJTVi8IULPbLkjzhyKd41Wd
VtKtuSp2YH+LGuhsGU4N9s80vsbJkE26hE5t/6C0N7HM88UP+3GydxglvD3JKzVzZ8GKhXdyqGNP
adcgHjuKe/gyVvpmsu7uWE7m0TgiAvgRvuj34TIYL6yq1e83PfHSHqFJQigIR8K4LFLIYBa4ydhV
IXN+1ABpYcwR89IApRek7Afv6CT/sHt7RomVSbp8qeabFkvP85m3nPKa5nBIX4Uq59KAgYGarzzI
NyxvDREAtjCdt8bYrT6b+akN8dlgsvknBmsf1RZMNIyXn9y6CtyVOpf/c1cMSDGXoasYU9mXejL/
4I8WGXeK+T1n0UDJ7j5xobS09CCY8W5d6Ml0ub/lzsJ5PXYozVOr30trQJIknVs7rBfl6gQTUqyf
5m2bIzs2eN5YN01QuwlcTrKmWyM/MPbcZXsUX0A+rkQYW4mVuy0pDycAW2vqOOTpl1VVxfYg8YrT
5xoSqqZfMax97zw2o4gsYDlyezi1D6KYoSrV8fYD8VpyR6gUyOe0WF1+e4f3llxqzGNWjKBBgkZR
JbDy/y1bs/qPtQr17jkFLaByK9PpLQth5Lfb/ehxGfYOQJXdYHztp0vFN4wXYcakyq0nCCHJUbBl
R6vhK91ah9XGdJ4PZv6RVmU3+8vBao9Lv6KZLG2xVkkt6Vks7YLuwOXKeLomGQs9piT4kH836IHK
pRpEZRNpYQR20Zft328mbXXwihZXhdVPWCOYLXAPEvuwpN6VNg4ascZKWUxFRFZGStdE0bzcklJi
9gGDViIupRggTZzRMKRo9dijlKo8VD5K0IGOHnEFQ93Vwln782PGWlf8l4FqZf5aphPWqsjxvAO2
JgMLNiJSIq8KDDMvv77gvc4kOX+SdwLuX20qb1y6xT0vn4e+nOswPbHJDeSFgOuWTYoF0CyulZBC
/l1e+fL8d20x4FGasgcbVJxE0MbH55DG1ZpHqj5Sn9GcuFOLtcqsbggGacfaMDmqbioL5N088iAm
8plcWvjB3h1FiLkm47A+Uk958ZtV/rnSs1YCD4RmIqbi5HmBNrGdA+PeqfJfAM0A6nNAc6WMefWn
smsu9vCxc1p0FjRo9sT14b+Ec47tfinzpi6KoI1Vzha/cS9FCDcGP7o0Xtho5Kv/x5RTbAZBGMGi
dA0ggfGYyYxDdrvDdhA+rR0oeZz/SBq7gRQjHeREAsXPZtSjows8NSCRJn57te5WmY5hfF8nZpWV
cXXKMMlly0mHRtZ0ARsekwg7ef160uVjkLsVqjd5J8FSE+P8BZpv5CkXVroHvwwd0yzaBlL12v/S
g4oYUPWuz6fvAq/ef8GFZWee+gQ89jj88oHT3HT8bZ24zchXLX7jOmGu9LKa/tX3ZoIGQK1LCd3f
XBuWVYmL62OUj1L8sId0g5VBAzPX+vuZEgG+Wju4T6eJwBXzkzL55EIJ0kaAyMY0Jj8xKXH31MhS
+yVG/16PrZi4xufkb5jajtxlJLTeASalZIgtKMaO1mG/XkPHf/LSkRN03c5Xkryon+ECeGS6joFX
Nj0VD2hO8NDgtNL+zzTA/Oqmc4ThzwUQViAMdb9CLbGbIDlVMA64EvuFympC/wxiHs5Vuz3Qmg0s
Yb0dnWsat9Xxv49tHiWsiB7bOSmquDGtOWysj1mzvpTgv6DdiVXjGHV0hpDAr+tyjyfbP1X5lAk+
LLgR46c7sLVkwOc+n/hQB8CT+KcKSzjAcaiyiqHmP7QitZridOS0TOenS5RbTosvN5n3UyF7EfY2
Hyoq8FPnQn4oE75RNRkwEzih0cntYI3TGcNHcp0nguVtw6g1tIUJ6jNi8YokzCwoNPyfz2BDE6pW
FYO857NPY9oyuGrwDlpiGBTe0x17kLTocPgqLrOijBj2JClmr/Y+p5FWymF+gtYiNp2OacGHzo4M
lX5aCg6jsXG4gzqSgpZgs1kFl06ewH5DBWZpL/XunUzqHqbQ2zwp5LxDgohxRTXMGor2TOmpsZft
WEKi0az/ILl0jfkKoSMYJNsMZK5qTLVf/2C8jGOuX3JpoPWWkBq1XNAO9Y0OCs/5LEibSY58rdRS
ZUY5rg3Pd/nYhgG35NGH8fTN7ua2QIm66z7Wzpycl4GCEFff8694dQFbYRy/wZb5CQgY9BC0+ydx
WadMU4cHFx9rXEU7mgEuRslTrD2mzIEWcadxSQ144sNBGvRa1nEYxKVAwJgpoB9FGRyRAB1gUpib
HcZGm/A5J0s1PQ82024ufxAONi0GX6DXROZ1JdaYwCQOWklxM1oS6t1M6qB+zTnwhVTG224D0wpi
GAQXTTdG1ncFcW1/1IqNtawBmGdxCkUlEPXn8A6A2ECnn1NKreWUl02x4i7D/UXtTMNuARXhklJU
8iYAIcxwn1LWY17bT3Oy/lQ3gGlRRaZlzqvD9LUSEtsqAUe34A53zw1qXNe+ZHPrOiF5zXjEQ+tC
82JT4TFxjTsQEXsKfKVWaEcD6VwQNAEggKtgpDls9QGpK07GFrqDKIdEdfyqxC4fxWSrIf1Dgmal
t2wSG6e4OUDjQ+fZVqVqPnnuytb0Nb2iY8/yftDe3NkwK5hqYZAI1nlksKZQ6My/z5bDz+mct/On
pakUSdVdsYbC++erS2hVVGbrsudPycF5xLsuFwBtDD2bZqNeRTVpSj7ZJ29kguPGvEcD3K6cQ/Ye
LjesW/YUIVItLYUoOtgzodRKzlxmPATQtJ0wDdPqvA9iJJ8t5ZXimRmkjki6E6GfjthOtQMrCTzv
0FxXkAzEcAgagOHrhmKRVozmuG6lcqiEF7uOjXM3FXTZ4Qz7/d2cUuYwepCjfSHDsK/qo3dfB7Hp
YrQNTSMP0yvQ1b1VPkXm+paiL2B9atzFh69yvHyKyq8b1fEPUlpmmT26oqSd8HkJIF4Vb0eKIa7m
/FfexTnK8uGQmRyv7/BULK+ctyOaijV7DgoZASwC6YJ4uAIWIoEdD64wlmTC9tc0/DMSaRM9IbE1
mHuM0lSLFvDWWZHjbY0rCAFtZGZIhYs3s4SqKePwFlPmbiQYEPCrRQi9XYzsRim8gPAgBzdFvnO+
eUKQhRI4gA0Pxfg4NnrsbZvsDl81DisVPCGNAGiSDwuO3S9cumZLAAElQA5je5pJvLRW/HL0A2EN
r3ks9YG17a/ff/Z1Ey2PZmBhxOIvr8HF4jeBJwlIFdL8Cqvm1wxEpnjvNoM66ImHNT5y8L5PYwrr
rwQb3HMohCjK+GSZcvjHVCbTJtXA6yKt7hur9FPLfOvPzNixpQs/aM00lSalEnLat73oMXCHuVgS
XBO3th8EIgW6x/crw0OTGmJ0mgvZaAZP8QI96C0ICYzTKqknsf5FNB3mkZmHlADE7ydqIldVBNEM
KYqvZHF4qyZd4BNtllfOkOYnwM11pXEEfFqq7G+KGw1XQA9EY/xrgoFzeHWdXU+0pF12bwc9/fcN
JyzYV5kZKcQVWaD3hM5t18srcpANlfJTi9/cQd3Ea/z1FXDoO42qBaOw8V8UWWCXqxOyGx93O5qU
06elP1jReestQQVTl/Uj4bZY4k9m1QuiBxBo+OWrfzZvL3vohc8Rsiv/1qCXSJGM2/EWPtNZ/xO1
U2oLzO4PfLZd28KfO/TYEQ/3Qgab+wERRXTUNs04H4eM2o1TFtmjAXdx3prjrso5mCuW3sJ8BV0W
kMxPxewzrgfSt/BscxtUZYejo+cOwaE8Fa5JS9r3naUusaDz1TyLidPylYHmP4joyWXIyulonZXp
f9Pa/7GuuSmi2VuIPNRic+n0BJXhwsIQpCH5W8RVypZ/T7ht7I9UWqcvoNH3aEkz+edqi1uziPwA
3y2hG/k5lK+9hoP3S0uOBE3vST/5na4awYsMWNRImzLzdgY0vCpccClNcxeolfslLSfrTSYoCTjx
gHXW1da0wkc4aYcuovbpLbtABqM1A3jt0eN3RX/LAOf7jS/ddhRh2oE84yzChBGt/UMD3X+FYAH7
EkaNQIUT5t2mCxiwZ9eL0J53/I2LfccwBr1/R4YPl1haKpT7d9NgjRTsxrWrHN92+/UZWrplcWss
DFUW/euyvQ33S7IN+q6qt6RDpcpm32GjbDDBja7oP+qFP7oB+J32Gus77AsBmBgNhETi/a0YY2vB
C+y1cD92mO9pm571+3vg8PnB/PzllsfAXpkbS9MtfeEr3+PLIhbRWJxRnCbFNZcOE4vtj79QvBS3
qIHmZFP5LRwnbA06hAcB4qtHuUT5SsEH+0+zauhCXeahhl5pOdFfX0UsJ+wjPtWs6r67Y6VIXJwI
OXFBG2GM5Rv5BR1rJ915SifaX/nFfkcX+l/frauVzaza+OVj9JdQagj+THqjhb7ivVFiyJ9aiAXY
qPVppKDGb71EnVmvbedI+W/dnutA8di0jcuQzesdO+yg/jtuECu1NPqy7RD0XNsA/aQZWrIwBjyd
0HJ3O5RmFfAc33rgjHe5Feow5YRDeuBzpHHep+TL48dkdl9ETp8ye7LQz13U2SwiQ8bihn+AC5/M
HVbCdtk15jhISl3oCPd2I/TOb5Xb/Tjv7EngUdXFRU8lTkCnp3TYuBZlIwtMSjdw9JbEX6r6rkLL
Vodoo6Cfury7N4dA6WihoMZgqHyelwSdJNDPSqDaSSu+VRrvDQIFccEGVd8N2a3Wlswgm2jbQGOL
wMgE7cefu02DkGdnc7V1kSnWsEszxIkYHEX4e0XGppp+nrB5hoiiTi2Wa0391DHcfnj2VGFbMmIr
VqMY540iT+CX0vgdK+khZ/f5ZtZizQDzeYPnAv+7KNXPMH9qMkkZfJowci0c8a6/MQaki7oobyvS
EbmbQSZ2ff4zI4/RA+4Kr7k/EC+lkqba8PDUzDYMenIYWaLimu3+RhMRtoNBZpq5BbNL+eITsX4A
RYb0frVnp0Ulc1G/2fWGTXTwz5nGyiUnfwcryhl4lrLJ6LaQ09m5n9ZDwjk+Cz0BjIpWkXPqAPjZ
LiXygX0AZd0fhAabRbGuMNGUV9RfEgzmdWsgNzPdF3Fc9V15NgwSrlfExcMIaUYKwFhBksHf0Pn/
XHL3F3pmi+ucKRsngDhCBaNmMJoFtwpjOxcFdNXTWd+nKgiHhFLOyVUTg+huUWySSU26QJJYeAMm
7CzKBQJIEtFBOWkhwdVvEdjzL6zTx0OkXda3WR29LfGyZrR9fJhTFTZ49xhY+k1MsPPfMWUF5mny
WDbJeclhHBVQaBX22+B/DTWBGwuVTFtJ/1k6gEt/eFZ8IKS+tVuthJo6k4CztUGc0hZbpW6tpSM0
hoDJEjl+1brH4OG1SO0M/h/I7ax576Nh/4qghZHdQYgiaNseYw3pre/WG8YEZVwz4NlNu7Tfj8i4
YfAhzo+y0O34xwEILzEasm155aMyAQdMzRN3EvvoHdT4plZRpqcXo2XE5mPR9qJFY8U2FVJk6EOH
L3vtAnTJzrADCNSe3yQgkjUBMmC4d+PPQmsNt2VlMatVicoeED2Z3XwemVJGTpTUKUzempbnXMIe
LFx8qnoyVus+jM1SHqJW4Qvc7bfR4g7H0p85VPG6kFKDYrVqZsrRDUpRh+79I2qA9CvpT3kxDDfM
LkBuWVFdYZslEjKeMLgLQHb5+gOOxCKNlbMYFAMsrrMMvvs8FY4VAjxykC21VHle42Dap9oJ8XtH
RlcxFxKTIVMrxdrA6ytgSuhR7StLUzIdmxW3R99uil/20J5vzB7YCnSl3dQrZ0yD7WBLqylmvhZh
E2aCBTSgl/BErpLbn9D3yC/QOfrd6CzQbnAaKHuqieOiO0BQ2rOKEFPrmMIOJmtNgLY2eZXy0F88
BG56EgoZLQvqnvDClOfHQXiJiZ9Kl0hgRh0i0/e8aCBCqCaNGm0FVtB+UULuRxEo+/PFbwXUW+9X
EwaRxfVc3Pn2PHWLRbXHctcEeLIWAG1SusOT54qUj3GIxqI1pd9Nc3DWIpYio9EKkSa+RGkQN3K7
baq6lSQzrbFmGC3d77vnFcnBG6EcILR8BGnkDjUygXsuuYeyFkLokF/TIMYIox92nvCyKsXn+JTC
KmS8x34PPXc4s0VtcvXa7MTnxB7xkNb2ZfuUcqfB/QarNOeo+MPifexHs7zyVYKXAqO2p9l8RoOr
FkL1DIKrTl4xLWIygIqiTPfc/psF0pCYrHjIWKagJrxat8LBSMK0TdzzGBiCQUl7Z6tKjfDIwZEN
uqplvcNTACbPKYmAlEL3wqZ/he5H62oIoS3MNgUmvHgK69tQgfNVRqUjkB57FBabfUWPkTuNCyQu
zXZGnw8NKdR9afwTlSOVIhU5nns9lSmOP3WCSAO2v1ojz9EXvLZVZ1ODm5SyB6X+YG/kRcEtvxCc
GES3Wcl5vqoVVn0uldBvVzZgRljVpNiJ+ljofCP86ug0n4FyK0zcKw2ywaJJGgY8brAcDDKgE996
EltDIfW6hmKojJGvp4gqY3jZ2av0aE/Ch17iCKpvISYey0hwYfMloYuAvqRRHqkDysnhSL54KMEF
uD6wobhUao7PtaAv8h8mLN2Q8u/tiVEPWbUXemS3U+MbnbjMFm54i/fCuwQd66DFi5GELdDEUOUe
dI15qRL0EoaXf2AQel++FADjUonjp5cJLvKJ6EIC/bNGMaI9nOpqwd193TZXwEYRx4AuwPWyq0uZ
dtdrTZ/QmEENgi9N683Xzu7bPtXvmodmOefeKtaPOMhA6/jCRdfkcEWE0AgFA+CNCmwP3SV5CFQo
4T0gMh7lMzjeYwr6w75zeHEAlYrH0G1aTIEdSSViJ+l2lI2apYzWcs1pU3DdQ0dQSkoZNLwYOXg1
A5hdjLlDusl0IVV19ffeAIrdTsX98/1BrpaSQTr201z4O3ej4M9yW9LiyuWUUY5U/som4FZTOW2D
6dkauCx6dgDc8OMVhVzqvcN+RJtOw1oLchnb0g4wz2dqJLs0dHsIlTXvOLewU54fdK1biZpHLnN3
AjtoBo1IBoymUk/Yl9XcEH/IFUP23hkaMJ6foyARnUMA4HZIpoanjt+jSPNLSarMNnmrLt5T8cN9
5K9Cy/p+Dl4l+ON0eyWczjgpD52QOFS6wWempbuTYo0yWNKKE7dpp/8JuOhHT05GMNLrL85VSVog
a6TYm5lKDkdZnFYomBB7ZXO3g8X+IpdTFESkjMO8m348MpnL5EL9alSrz6oy4MxHXCJ0HpJHnJ7H
BvQAU0Sh5lyt4GkYVu9GYUTagkSws3ZuIO4ZoaHvw55UFFHziPA2+hKQmxY9+xl5egiFGg+1gNIH
2qRYQU7MnYLRYbzcY8BaKTdZeVMQUWPLRs4xlg9qvxzGCqXXx6xi8EtiicJAEnCZgBznabaIcoCu
11RPwlLBhJHMFRo0Ol1J5fGp0XD7NfLI7wpTm6UPelngLl76VsEpmNXINGrE3IFu4IOVQvNorpvb
Z6iSt841uRLAZYwvSqTUmbM+t+5tgkKGqm/K67IHwISHHe15Zm5+ekuoi1IuAg99FStEoJL69Zvd
psKfuqnbzKZURqdd3cAQXi9pG9s18VYXUAHaHAV81oBA5tkNC1ZJ183R5Agq3v+Cl8adjvmPrRgf
q7tSqWeEXnbJSTciMHypZxIMaWP5hQhIH6ZSpeawc/oi1Hbyn7EfdPz5qSVNgIKXgLcqGV2kG3Ce
R5ZXaBB6TPS1rm60alQXN70f5hBLUI2IjKwv0svOPlKRjRdY6LDapAj4SnRw6gtc2w28WXvJPYn2
Kqf78Q5EZSNsooFwTpiovtb7Sm+ixJcdYVbu8hQXMjdGm4EBUx50cDlkElDFZfvWtfbbRN1cD0M0
OglgbO9YRpTD7eaThDs0qTUbgsdo1TG3MGcZOmhCjM80GT6zLjmxYdCR9FUvBbTWqGBbUMYQ68BL
/zfdKtNTEkNI95+RFx0kVBW18RhZezMjcfHEfFiEulEvtWpwlmtMPaX+ioXWiryngVEFPf5aPm1K
4fTSM6OVVXGGYtszkQNsSJoMRjMvMNHqW+jSKbrFDsvidevhtRwg4b5b6R1nd4APpsZTRZ+GpU62
3D07CeJKKH269I1u306giTvi3m9qvUD1Qy0DwLTGW3IbppnTiKj/ISIZkP4WsFEop+aS/Bxznpb6
ih1J/aGfrasY0WxCXIUkC7dtK5n347Qsr+ciPqj3lr0+I9fkee6Qpu60MwyPtCJmCeTvzbJOfwGv
J4fSKtwrY3Ixt/aJ2Xn33CZRtvS2SsIfDUBWgyYcav+wgBAILmFCZMDKkHNHTcDQaYd87P8w4HIq
1zNKhgm6AtjVnf/ZftbELvXwQVQGvFvm41eJfT/xfSMKJGtvz1Jep3HdiH46T9JEvl8M1S2fbg2B
VdH4kWlWUFhX8kGi2OqE4ni8DAbl2LGMqvVqrBGlx1GT3hSEL2RyyfG6+6huslL8WzNnMl/+tngd
9GuQCpLF3SuTRiRcUi7so4+M7bhMmPiMODjVM5ApoEaSed0EdrmsKJ1Sk+xX++exUiHv4SkkIm+/
lQhJDalMITSMqnMnGBEz1oXPZCtBiz4CYATaBa0YHx3boSCgZlWmp5lpxiZLEjReo+IsvBJvmCZ6
HNUjRbSNiVZtgfgDE6KqpIdZdHmE+ZDP0IGajiqyTwbuUiIQ92hQFB37kZGSkreFq8ZCn2TAyHdX
NDKhQEaVCYQEH+mS+uz50lY8QoFNJWNt3JZfbN8gx7qr3Z2ATcT4AM0UkViGIchZUzm38ThBYVB0
AKJwa8Ud1TOadsR/tw2Y1LvvYBkGlZTKlOtJXZRK2KQnlSICmW7mhFs2MLFU0Taef8EMrGeNor/T
+Om2+28r2UiS/TTSp6qdX0vkhd+3HifKQ+wXlu56iyKwE+L7wIm35Db732g/Cr1xVbuYUhSyTiwY
UG+m8DmIYNDvkaZ4EM0MlfxY2gayLuX2GZASAjL+L7fsDn86dPjtpEnnP6XNbeywa8ZNNRCUv1B3
dvTGm8F331h9zvFXnU8OG9bJjmmYuI96y/PhfCFfbTbj2Wban71JT/JfU0rEP1fgRzgy6ihGxxu+
9FFxNeiVzu4+QIp8+z5cyFn/gO9AjUys0hpEq1Khwe1syxduMaZpOWQ4VAJTuCInIr/IGqVNOhxR
j1pbOJH++HjP1dpPPXsYI9GMLpAhnlfngFPhfGqRjcoqpI2c7JZVXuXJpKr7ZS4bgtDLToIdKAaS
bog8gKHpSp4P0BgpvzUCgokKZPU2UhOFmYMMhbHXKJje5PpziCOLQCdpxH9yVo0ECM3iKw6jaBEY
2ZrYZ3WcNHIfizWZteLbx6Q65beniDN/zoV5S4XupptQG0EUTv1w12E6w+AbdUbwKoBfW2wzcwsC
JYet2sA1dxMLcTl+NQlSziWW+BFVOdNKclTbzziCvO5IZOPOihNyPM2jS680Z0bt+8TWUzirCMyM
D0u3eP4ONHge+EDiVPJOX8fqBOgV1FQp1x9dXQBCh8LJLVH3GlwUoYHzCPOTra0ilLq7GH7vBUkq
0nNean1Xo03RuA0szxIxgzDy4MKsn/BUGxI8up3YuEz+/B+WR37ghTyjxSGuiWkaGgdN2aPVOgZc
eBp/MFdE4f8Y1nxtWodWW3hZCMnIsiE8tulQbID40vLSslgS/Yna+qJpcH9kF6RcsXe+jJKc23PP
d/aFuWCxEoc1iHccMYpVCtD8ddRjv4b+SjRNK32fB5iYw3tW3EaLVT2k4w3L+acSdR03tjr5hYJ3
jO6idJrM8ti3S60iWNd7ePDTHXk1e9bta6g7zwgImkclf/E09+xU+pX3qm7mJxH12+LkSOw9mEqU
eu0akESoADtUeoUlb5jgTWNSHDcAFw0BXWQnDU5Oeep3cC7QdrudDrFXHszf32vyd3AUtnCPxjT3
+Kktt979pmmIbCqeAoE+SqfVE3HqhQ7t+C/7JXuxN9sBX87pgRbHi9FtIcPV46d07D7+Oje6tYWb
63c5G2KjszcfzM36LDY8PKEuQspKRetXd3JS8TIgj4uOSSvq4xYahUUyO5Z25JSjmE3/AMz81xSI
tRINytX5h+3oxan3eqHGjt8ChmgBfyttUiZbayTUPAFsRvmrYU9giSiU1OPd+MmIOGDGCJ8dYe+a
SYkJeNs8ssvUebLohtX4dLo7ga4s27NaF+2PvCA1Bq97ImBmEyI+20UtDnXMjvEkp5m1jvx+OwZS
ae1Nr/X/61BLc26YmmquAOOr25xa8cQQX8qstp44mItcMsKR+F/m48AHh5WkX8RmW+U0M2MV1Vk7
IeQNZIDRhOFSerN0qVc82KWy6x7smgKhP1utAJ7e3Yw5MGibBMf9APUG2l6/owjcJMBSYaPI7FVm
sLo7AfPEG/CsYY4+dRPQ/OaovKWJoI7ytQ2cS0TxyZNF7cZas7YdjN8WLIvY3M+8zcj0GfZW4D8k
ac17v485k5kFNxKaPmd8O8EL7KeWiPRYYMNUb4SA59Kqf8GeS76FsQj86emJ5ltLIksEI0NogK+R
5yzV2UFTOXNGTdZXzd4Btn7eciVsQYa3taM/nHlj7oerqmn96BCwrKrLsS7ieufdzJYMSV5dImxO
AnrAtGPj4tpT6IorjLQdODoyf2w8ensKgVv5HveF3E/YpY5PJtpQ642z68JXxsobcLLifGS5tSYV
zzNsGho7Xd0lUY9hcu11XBmKEQqXXRxLh+BCBp6M11JMsdUGyJcjb1bJbUqVY+QkRoAsmfUfCuaT
+H2AUGaLVrWS63a1MO19guXMoIb9uqfZCbLeX+KWi5NTfC7m/DH6v5hcwMfXuSp3vG0uLP0S7OnX
GUss9CwuIX0fFB8LJIu3gzuyw/UD61QuZE6UmUHZsQQg/CrGXylXQo+lsGaZ0p/2PL7PNR5ve0+t
rxNcQsK3LA3AnuP0JppCHYdHpZIDACyg4gWltBJFua/6RuPh/fp8T/EseRHsAy6iZQ2j8hGYSmvr
cDcGwje2Pf1S0SUkAhmpuV3SuWS60VhEwRXoZ+z1bVe4WAitIrtTdlicbRn7O9jF8ii8fsIbVFkd
tgZuvHgioOlrcmso7fs221XR+zS1UutFfEWI8ghuvnoYAPFD+61pP2dKal5R6iam38VNFR3+d7sW
1e1/jrj1BjDnHGnSIrDCZcFe+kGmMY2oW06297a1fKvxGdhL9Sd35PL0SbtSkNV6JTAN5quXWOAi
RZ0V8QncVEmbRY2FsNcE3B9ybyYCOMowNFLB0MEbBAF+dEzQE5y6rpOCIKl62W3+koP+FmcyRV/N
AFAwc+9UgrqZQdBjrzw51rMb/B84Yo8N8nz82zu+ahF3dwDAKNm1Tl7P5S7R7V5M3iqdrLUNtEd4
0hy1DJYUP3lP3eMsdRPIMldaWYHeAZhX8heAPk8IaWjuyGgZBR51epLaHDgOqj/JN2VD/OnVUDsA
+cnAA39uRsvWFqzKVYJWiol+K4RLd0L2l5wPEGbUTM7213hIpZiul3muohG9qT66NVKY2Qr2l0mS
lK2xLDI+bf0bz3gOU1aIzhxsPgy7kT64BqgzOk9QYOO4SkGH6m9AwTG0JU9Ph8Qoo/e7891YxfJW
9XykUBQt8uuvARReYKaEBmo9/Gzyzb34716efABHyFsZiD2TueqtFN5RRGUwmjZdpy9XgHcr7zm4
L/YG97l80TFJ2hZG2EzudzNPA1XSkPGK0SIwwgIG7mIIDwfKZuVq6oCMrfkkGhbiiE+uHHLkZ9bL
3aDXOeMirJpMy7PfAIIrLFtPF6v1AinILXviJVopydnV5OE1qLWYfUpQH8tT5Rd4pSiXrc8xuTKh
dCkiCHRk4qiYCT4ai1YVQlHV9tOe7rBzxvW1aw9Fc81SXMFuHV+PIR9nNLQ25HVNzpqKV4XMJ0NG
YTYFvZnI0w3wk/VLs9A/b0ak6E0reJK47i/h6U3NN8yyuxcCHkrsIxgfBh9mBfmHyQCp0xOTFiTw
XZGMSfiJ0BBZ3lnx7uft2UZfsF7Jvi4hkCKlzdqKRmngodRR/ErIpRbVekHt2xnAcXQvScTuy4wG
EBH97kpO71PXkMiI5AI6zw2de3NRRsXJTa0QxTokKsfL3K6iOfQBMSRuhjgcGOds1XeOVxzfAwgU
EVDHMhhExaDLdKt3BNXQI6sVnqTc1xTuntKEZMHECwfLffC8qWWZcGrQ7c5N5EA+0rukfJu/JWu6
2GYPhsHsezXUU9PfFVnCJGOQxFBkBMJULfQPc9QoA/QvSd1TUfF54lHVhZ6wwX2DLbrJEhilBHBg
bXnKd8LgLaK7O8Ae0UShD8jDnP+wu3jGlFuv3pX9UopxP8BkcwU+vKWBkPCaWmT4CqtxaNK0RTYy
V/IXOwYBHCISa/78dqvMDpjHFwF1KA/JnpuGUpLbatfuumryuazCmhtCpcCBCu4/T1Ezh2U7xmfP
+ubzwOSx+aPELYnNDUNOgmL/F76ckwdhvgaOlF1JTpscIy0Ld9UEMR5QJrsFWXhLaRl7ip+vN3U5
WH1+m5a5xm1X9MkA38v0nSaN5YnIQQBOKRxHXhP7df5UdjKEuzaqlu8qACCbgijD9wCO7k9TPOHE
FwQ32si5W70Yug0BpCgn9Dp8aoc4y+XHUJWR4oPv6Eo7R1E/648sp0UrMJrUJe63CJ0y4IYLjwiE
RVFDS2isEOQ68BerMzZRpClFBsMHW9VATIR8ENEsJJvxHVJPCjfHrTjTMStrIS4fcN00XLjaYbsT
dLc0B2Nldb7Z4g0wX7sAZ9Cg/brtlbu/pnvY3/+28QQOs1eqDjoRRcwDOaXAs43Xm+A0vAlUK6dN
DqrEZbP8/UzxSIZMEQrIX1dB8lU4v/lELk+1bUov06IAg4BPKHG6uB99fTcIQBW8sz+OTq6NmZZ8
mec+OBiwNkb/X+MYh7erQXlmEfRCdqJgeKzazAa/Gf+eG+cj/Rp4plBkKH+QSebjQZT/ynedMsOc
dL6kLKc/HS/WkcoT2/Bh/QCYsFSAUmB7PpwoHJrFj6ry9MmBhkBopzfUIYXfLLk9cjoghgmb+le6
YjMaVUNw3A0KA3xYpu+RyZn57JMDmKuMdWYDQiHJbagcJ+rt8eg37exXE79BfGYlkn9zFN+awjhG
ZF4FTg1nUBtNj9Kof+cnrbCjy8DossxP7uE+pUKwDlZeAPHHfxGLBkJKTzBZBb/+1HtAHkSbgUCl
Kx7w6jG5m8CKHDjSQJOJJAtiP9yenWbLHQNOCliv1JViO9HFCvG3r4nkj8t4lDy/WB3BQpRDZVfe
pFzRs6laY7LbrZEZZX927phPdrJIb15CWlYxy028avwA9uQLaYYkM1UaOMTa7ljxnOKRQ4Sh/hfi
5LYfZ+G9OFySpiDL4hn6umTNhFnd8tY1zjCKiwdUojX/EGLY7mI06iILa1eYZYSPPbrYIv+e8ZYF
u1MLi3f2InudmFyfPZDvSH/rKHTl3jAl64CDqsZos+m2ktALLc27Q0EeD97lR+GO9uHFbUaf8l2C
jSxRfu/dtxl1xzgf3Uo5KngiWZbL2w9UfRHCQIijV6STLlrksMd6Pqwp2oqzTkq8FptbQzlCdQn9
HK1TqHIcki44zRgsj2W3Z5kl9axENVYdWraVGCi1ni8dkoOPbV/b9Og40osEbVIU3+q1LQFzRMX0
7NIvD5SzZNTwbprxhhSZWw1qgzz9xBTVloN9sRttnf1BxlskFa1GmTNmbuDUTzlHN38ji8qcOST0
2vJXyxkQ6Qeenl7VYcXtSaxqqvv/boT2LWrE2edXfPqjimj+Xf/uIwhFyvpWtSi5aOa1RhsMYjKd
LfOk0VMzGAVnnz7iA99s2ZdxmVHRwx5iW9ury7+elWQDR6LCctUn9zmq8s97vIfyyS3zfSeuVWIe
K3lUSQJmlVOvD2ePPJvJdqLV07XPd22N5Q1GeYrYkpdlTjKYo3opLsCBODF4LxOUMYQ9poat1wiz
1t6tx4hlnESay37H+r2v/FuAO0D6AhQgNyzw8IOcUC7TAT0etbcQdeUewXJ4AmUIdlHDebe1itYG
6iXexNMexvh8j/Y56LE0bCKyV7Wt6ZWKBGO7OLpQIsGyBhgbt2V1BWXPM1f+gDHiz0CCmmxmYCxC
XkYpD9431ocDCFhJDIx9rXsNDPFYz3lBr3c6Yu9up89l6QDlOuLL2s8HGeqE2OUJY2F0snmhWheX
moGX/KPLDwaaC7L0gGjuq+gQRd5KQWF9evHvQfFcu4lymb6aFkXn5bKC0humQ4dEEMkr+fye2p7N
ZvoHuoab+DGkSGOXg7L4SpN1RzOfl4c30WG+FNh/7skSeOpvOEtJjC3Upq0ISKDCqEDuCbfTfGDH
n2iokBGMx4EcWJGWeaHWXAW7p0sWUcn/GrFA9oreyut80LqF60AJaXOc1l9aEbpXwFq0pAJ24HTj
PxX8rZ0o1UMm5QiCSDGitZlEEPK2ZrBSYUX2atQ1uvDXjJEZiHiNcbRJ71zJ+rj0nqn4H8QCEEsb
ej3c0C980kTe41W2N+D5FDGLorFhHCKQUJZoQm9G6Bhh2toBY/mw7kCDoIutNkbhzw+5jXoQz8ej
NRSXzyvoRAsy/JJUqwoa6QKQEZq1ddTVu5sylq+hN3ozzkYCx2KWiOEoq5GUrKI5yovm64xGLf+1
+T6GRPhUUJRojY8DFjSZ/Nsc3QCO5Pnu/VKlIdh2a561ZZRDCdkVS4kYaA8UcoId73o+cU8lLbD7
2ssay9YIC5Qe5hxg3e0azwxRrbZSsl9ZcHuT0jXVHyM1/yNjYqJHm5tbFalGlD2IJWa7NW0YSr1J
3Zud+SlJ2G+hGY8lsLO7gyxpyTz41HEKUhmhBqz7TrbaCh5yrm2EFw4m8z1xHjdDhb7y38nhtSl8
ekX9SpYVNf5ICCOidgMJTTQd2+PPedGtUDkwH68sIEZ1bOYSlN/O0tqLuWbwGIAlqwZJGIaqwes7
i8hL61uXBYd5yxPB1h9nnoO+HjUIl4+ubC9hUfsrCM+SKitvjgG5tkEEDHXH9UtzEldIDhq5880w
BlMe6EcO35F2QTYdWJQzW8Dp2ywd1zzEqOibDOzYpVMDpQ/Sf/oQ/HCX1dKeUPSxadCgG0Gs24OL
yX7gRwE0Nxe/TRwqV3/WaTPy/ew5ctPlCqIKC20LCGQDc72js9hiG1h3k3KcBnxM9DVQ52luNLZu
Rx85zDzzma1QITL77SKjM1gEs6JDdTome9po9SXoyV+LR7gbWZeR9iZcB5qGg1EVePztWxpmxdUB
bHnhnX8CXA+RaSva11jNTqHJPs5rSz97jslnoAKocrnxV6kBeG27MTt8EXfmjIJXZliFXa8anEE6
SGMvl5bMOAFRagzRGfyvtEzfqHVw/hEF74mGJ68oxIJKB1JMFq0NCUx9D8yLu8NdHkBw6NDVdEAL
zSvzmVwn2RoDpBZorOiDJ4x10CcVmCuqKlbaNUUbUtw/W9RLVwofETOsyFHBpKsleArElcwwft4b
XV38707e8ZraWeLSE2Of4SoLbuhJEe6u7tnOqzSbF4cTi7nYGJ6tvMnZWA5x8grktPgnNeSKCGyj
G41APabxIRe5dQKjc+BgTWsfFkDNbXkRXUs/HsfL46424aVtW4A/vuE7KUeIycR4LKcoSl3OwPET
/K2/hPw+PSEWBy/Yi44v/Scmb4RQgGWN16QfUjCPmNTuTEpZHxFdhdPI8q1y+8SlxRCAuVOuamFA
ASA5qHHJVq9PK9NjLf3FyCEvdxtS40wzpMHr/9B0iMXn3lQZLeYHZf8VaRHGuDcMoiF9CZBu8gZ6
B/acWV9JVzJn8KOe/Uz3Akv6csrIqmSuD3RqsMVJ3GwHMcIEDI8o1Z0btNwwj8smUWVz9VsOT3Ho
QAYlimaPq7iHLYfoB/O4iM7mf/8gaIzvGqtmeNWDRbG/t0WVE6HRNsMiECqUOpIc+GF/ZbczsBtt
ijOOX9VySaS4ysNLFO4iED3r9BO7nfolZN3kOyjDkUGqKTkQyPV/mJr1eEMXHf8HO59zyt3jKGoH
JuN0F+vO+znTHIUN25PQWqFw67pj/5jcISB3aglG4lyWLem4bpc7PmYMiaXn5AuOmUDy8b1A66rM
1ZlKyv40lOLHjEg4R6W88tt5ILQNJlfPcHO9gAwA7uzWWn7aU4TRTTl65h+TG6ic75DbbYSnSGW6
YLRrfK76DEbLvcssqcWdf4eKRduFAMS7xnreSmYDPltFJUe9rJgtPe4/GcFTKF5ntM2A0FhH8d3P
nEviYLzDMb8HWTVAHNgj5Tj/j2b11RGSy0MhBLHpk16lRGy3lkbIk30fz3ZOLMrZzAxq2i8kjs+6
WPwZL/9TJZDoOFv/xzJxlPeQq0ARFs1x7HDxpu3KGl3sKYLi+0iUlBr2+St+1PdQ8v+KJnc/+p1a
Jl/0C6HjsYcvrQMNhqmZwv8Z10ILF9l3xQTBnvyH/66FCOpGRaLifQsit+bldp12/GS9SvO9t+/S
iTk+wPPB27JllqHNp/AnGT6uAShQgDLJJ/6cYqzM/3eECEZ+uk/iOLYxbgyKoEwp6PCZKdjfC8Ki
vBpIfcN/ukBWhjUroZ4+jiKMd0pW7p79YzngAq5nAARzZlTIYC4oLaeeg+Q00L3+lYTXcwNtiLRU
JKidAO9qWIzKXqgdN72Yl6FBcV+QPfwL6JKc7ylR4xazm6ExHLUaVlQhnGVUiQyBDgbywd+Yz/hr
GM/n7tm7z8jvBPpaXk6PcdcML7PN1pE8MY3riyNwlupovVj8GFeMvwJPNtk87HDumvXOijBpL2LF
/Vejo80UKYkC/jCPCtEDVCwd27VhDU/+/rGuBzB5WrycGryJlMoNk6Nl7OlX7CoubZAGmbgy/BpD
DT9AYrobqXS/HymF9jFlIoB6/ijdzwaQv8ITNUT6Ssz5vlHBD5OOj01rHzjhCtndxQfx31HtWSCn
VZWBn6RT+hM4bb8O2q9cxxcHD2ZjYV+6fITqGmiCczsgrtEyaasySBqUFcUtSf5bzQjEIT+R2fSb
Xeng6hlzk4xNYoyjiBsj//yJh0bJMLJM8p5rnBMJLE6oS5tuGjubPKfmezA1u3TeIynJ81UPeYoL
vFc46YnCXvS1kJo+bC1pp09zdCvM2ZV/3gmwdUfeWNFF8y8e7ViGGnN3qs1WOAdQaCCBDjbb5XCw
TED5VVlAfGJ7lXGYhnw6oUWBQ5PNe5rvFC8pMzK8akCSBSk+cPQrTEZ+IURhPVbwJgLTB3XJX66K
kFtooCRGHQT9HTWICZqRq3D2QVz7g5Gik/hEVjvQujJZ7xU2bADuOI3nvgJkmPGmpZKtr7/ICsUo
e6ALV8tsiXug7fDmib05xk92obuzKP4ca2hGV1bl/q8jO+MnzZLG43vk4P6GJ/Qmuj+Tv+T0Q5AL
A21fbVRcNem+Y61rj8/28tFS+10KYYNOtqWY71PZHA3nE8zYmejCSNpE+GCnZzZeNjSzviop7Ft9
PXc075XHbF4VL1mgJ4Kz7OYLcPVFglN0fX3P1htDehABXmxHwxHiqq6/rAMiRdgppqAU00d4YE4E
sHQanBNIZ/2GuBDQ3uhEIlkbETcUqCGdUE9U/bbM5mypf16xl3a/3A0IhJqU2Nd4EN33I+EwGoj+
HqbzMzmTj89zJ1d3oXleRcAHkHKd5MHfjyyl5fgdX637m7u6iMcYij+z+yk1WCQ1/GHIdK21jdmA
+z4kWvXynv1FvowCx/Ub8rKAX5KIXqdks7Aj/8VZiRQi4+sqOScvNg/6ouXvYBErbuYfhkkLT0SN
ix9ICaE2vdMnwTObs5aBPpLTEMwhJ8yCC6POIHIuC+eS4uxmZkqOcDUuHTmTIXs1seOni/Cyjxu9
tSoCqkmGHNJjta83nNRGUgfvnyHlxAhnxLl1CWF5IY6whNI+aW3QxSZNnGbWA5CGvhnMG2BUmpMb
0DNI1VJ27KPpyN2U1j2VMih38XPEDbissEUl79E80c/iddPXMmqVAmV/uKEnwuzzWI1WVddzVx3Z
uypunc4SWoJ97sJiKZMbD9s0Qwb9nfn7+BOJPTaM1VkkyUq1/yAmOpRW8qTxVRNpTAqhe3Sk12hr
4GwsDURqj3u6KfG5TSBYCWGInxX86gE3G+GiTs0fngan/IScHYDpqJOPG23GUkc+FUGHF4O7f/t2
ocGvTlSb1TVWSM6+H8KllPabOdtswn+bbTy/KzDlL4OPl8MaPqzz8VUAVYTfI+xUBLaddCl6VfqO
mUqSR748EUwXm0EpME3W0a9LeCMUhBbCybZLTeZz1289Csnl+m0/41a9DdytmxYjU6rEDy0X5NEw
UrfsZxNYW/0mX7cbyMQzh/K93APQtLia70ZMkd3vXN39g75WrWSQw2zzC6qXCOTERa8xnU/lqM3k
DXjsnXU2/I44qf4Jzm35oZRjSIVtEIz9ORDkKCKs5K8zlEBJDAxVTvBYo7+nMnm8BVWydVbhkWKw
6Ai9aVTL+HU/QbBur9iVBMtV9esQg+Lu5ftLZdPY7VdAhhkALRpuGzoa4WENtK6Bs2MX8yKjYUcb
WHsYwAyrVWfjIIgyZVvyzV3O7qWz9Uk9jXXX+DyqT6X3pPBfeXuHK7s4nM1HSYJqtYAb9ykucQlc
9qp/4XpAknJutYcXnw9k8KHUXF0FwII0V/jhsBuGYzd/tD+9TqdO9h5cSkOk715SQgMgFXZU4UWl
pj6omqcB+AUiWYI/zGFnemY0NG2rJ3dDm77eQKw3GdXwpiahNK+4f6wBJAmy7kTHL1alp4ON9aEa
VHhbRhXRtJ2BxLmoonun+ZP/oatzb0ktgYmKerD1wR6VWHmfeLX1+hatd2JDDazUs3ZaJiG5n5o8
ZtmZmfgA9gd6HIYWnKrtP+cjiF7rjxB1EYjTUoeseROqM61tm21La0P+cb4CPGpL8jhL+PwjI0N9
mkm5WtXf5EIYOeaGJOsoexdJ7u2ZDJ0wWvF1ByX1JZfa/KmKVu8Af02lsXSCwHLqTmvn9e7KHKkx
5sMZA5dPmCUP4Jj/xMdJn3DQBGHcK6ntlPsG4aK0aBDxWqtPPtimnkAS5n2jpCnujyTC7KBVEKhb
7fn3hGYnUqywR8P3heWL4/QjKAO/TXqUcOrB182vdeTRptAdJEpJSQ1+VTvzG3/h77wIwrmJTDoK
ndhuN2GOL2UEc/9HUbxSyGbxU8tcEhqoWc0J2n3JO5xAbKkMmtd4T95wPl+VRP00ePtmDnmzhGBP
w0w1LmIw+rHRci2VkQtzxmLW73WdSwgKxzNIMsa23Bo0fAvT/nCdWGUucCuE8qiCPlD2kQpKfpv/
HA5R1dC0139fg6dF4+O0cfv99DW8egwLb0378PsALNALcDEsEEbgFB9s2GqtHJixEVME2ZpZ/ffm
ECXc4x5JYZaMFQeqEWDOO44FbxNEGMHBLPGI+ARgwKf3BAxoJ3/GqQ+x5x0cfQscQK8pOy+Gt9R2
E5HaIV/idTvmZxFsBZObXoB6LTwcE4lHl/rA1lDgn0jY5eubKpnkGS4Lq41IA9CnVaKI1H85MNhm
99P1JHzFjBaguTp5ZE0T5DZs22byMzYh4gFl2yYiUGtRjvdL9DQBOZ9j3/b5aOQXsrSiJlsaIzbh
J1TX5qFPgFH5w5GRiIVm6jTrAzh0LyT71ZDOobFpJ9SzlMz+S36EAEA8hHZ7kkLyrG5++3EdgHtf
GJDfvovWNf7Hj2ubKB/leFAl50GrjvHuiyJjXEgF3AHrtQbYS7vl27UF3Bp4PWHU+fKSmaUnxNCk
nmT4A/d159Aab3Nw9lCKXJm5QxZ4sZIVOHZZhwqo5PWz+YEF4NqHsF0rPWSJiizVo8i26yT2P5ql
Vzg/tV3i9ul2dGubFysqLK332Z1PNM4IhBTjISbmacgiHfwIWtSzJatr4r8kI4wEiOoOOaFTjEod
qKVBhFdwSDDnv4Ix0yluBJ3tsU8391RyBTKvmavCViNI/lMkhgz2nkhRwHJp/c93ziKLAhY0LEIp
mT/654khbxKSshCmBnf4/95vpIz6uEGd3d2ZbQDNr2IU+50EWP2kNYLGasly3Cl3Hv2mtycIdyPt
rSO/m5gv42HPRh+/PH50Ln+faKNqfStdsCKbzseM+cz+/iybh88mprkmMBBmgEZivikCJnzRNMr8
z0E3CTFpSipu1ssBhlW+q4702m2uGEtyFNGa2kGXMnEwF4g5BDB/7GheQ8CUH8+HgP9crq59eNWk
XIChCk6/71fF8LZqQThtixathR1B+e0qMyf+t7T3T3qJ0bKxmA42LwNZEm6o3PUO96MEenErDWhA
TIlvk+2yJE5UXi9+PVixqkY8spsAaUXuJ873Dyc7TO0NynrFMlSWJiz1R9lCqQcT3gGBhruoAhkp
0LVHMGCCAtpqekJaE1q4h73iTbZRqFcPaymJRCEoRI2Ghqv4J0qoy2Evbpb8HWQUxvwtQUG+5zFj
OrmXSH17g0qLT84boK0yCvN1ziqanLWxaogJxJZt8PecL2Ta0FXrqlT6Tzs2Tt2OHUFj2g2kImhU
Ze8kcgfIXxTkTtH0tDMYu5B71gs4km6ruO+0m7bjg170M59JXwf+uYCA1S0Adl0BVloJWIunKH05
KVAzIt/JdVSdjIuZJePeL16XfQPsyWSvSI2mJ6TA0+sIZrDrs6ZLnH8FfR3IMU8N2PPCEvBmWXWU
RZV8USuq7QQlQhbHZ7WIFtzmH0E4q5YoEXg9kcjdLSLBfIhS4Bo+J3Cy6ct5CzXGQ4Lb00ROYPaI
E/mNyX6faVs9EktLtL4LoB4FOzA9byRNgy+H7si7STzAlMLWbHD+a4dOW3WdBzWC+toZDQQvINBD
dzZZ+YDJoMCgOogi3TxSFD6pGmoHHCr8P/+Kyj65o+3czZHpiQ7SzR3E0Xn12WouP//fuGGKm6o8
7VVYfUMu+VZSgu4ZblyfH3E4ikviRHyzpt+72djCcYPGXprAiMgA+IhM2LfAfAnrKC1iAv/LjRg6
yMFC5mjqOnCqEn07LQ2sX+i4UErHVGHy7uUnn6ASj1jtojH3of0jmDLBQBXV1ibGsF+m6LZNAO/3
DqjyGR2goml/EaVvyZfhxbIc9vBnHfgNOwGV+34Qfv5patlZW4PqCY0ReAb4qx57kgy9fTjbwN68
ZZpDGsW3hT6RGc+xU6sCQXe1kfJHThpxM3OwRDwzzmsZ6hFJtJ7ptS0bS6cPqCRQo46hqQZaq+lh
75emIKeAmkH1VySyzdukwdtgCyJKHDGqm3yVwflo9wt1zSDhC58jScqfVsfei1WtJWm1hUoNN3w8
0P99xurrj73VMHxdMJIN25d8LCCcc4zR8ZairgA8y2wXtQwdpR6cU9L5cI4Jk0gf8LEnYuohyu3o
RRljQSzduR/Goc8wuNhhWOy3vnEFlABkeRAc1kPvNM7FVNk3yBKRv+nrSuPvDWZ/udryblNWZV2q
fBwLtC8AsvgAXxqEhM57cpDYRqnfkV4mt9QF6HwxmLg2fHD5aKAE0/hBuzPuqUvYFGo/S17pm9Oo
jvgJrlLVoKYgjVYXO1SqwyS80Al7xGRnhUYYmuMTQpvupfq9AYk6yh6JJGDupbIp90iS7/eWhpzm
wVWIep5lOEQv/C2avbSj8mlMk69+pTIu1LxaW/MyF35XYsoH9vIIxN17XSHcMd4SgxKGl7GWRnsC
8qhzBvIoBLBnSG2zoo7XNUVAVjwcPEpQBV/7SWle9G0cNTO3S3XYrSm4KJ3Vv6IROheEw2ERmNIv
WHJO1GChWqyPcNOIKte686nsw+BVd8GpO9aaZqoCLE1KsA9kBz7tHD8tS2pIbUI/Dr4Ot1xsBfnP
AZy9boJvcoxhzx8JVMofFp6zwGGUYZz5/YDM9u9YK6H7fe+f+wssOnIRWO2FvS70fr9+ueb5EWqL
sRlwvMe4XAeaweLTJj4VdNUUeJA9c7GThfTZamElIp7ffC37ofdk4a7SkoTVQ1GkJmMl9GSviYBR
X5WFKyPaVPDlfcIFXo0xiOp3wPU5AN+seLUNk+p4zFRZ4gbBQPnjLJHDxDGV/was3Yd3WySCLU6e
yEZmtkQQ560dfUNI6Rn3mEai1DdBsmpkqq+jGI3IZSeanJ0ISp1tFRU2mo09P+xgk4PkfL6J8abr
6k/dnspup+wcQNdt1UgGPPy4HqTE4wNpeZg7cpOlauvDExtMqAH+fGxbiwo/JQK+5w/Slta0ph0o
P0bC6DZj4Soc1vlhta136ufbNGgx75GTrAZD6UIv3kOEEurulDZ0o0q9b76SLSA1QpmV5+2m+jm3
qPkvZWHC50yrlVYih/2dj39jeS7Zyn8Pfl/DDYTIn5rMi8ZnyOmQMgvDfpqCunvaPGjT9f51+jpf
zIDTwnznaZnnfNLuEjSU6eHIuPIXXhjpYknQH2l9djK+6YKNMcB6k/AMMixP/xjvRxNqFkiMb+Wj
bWad/7ULzxLuFTJrOp9D5MqHqlepiD+DByJh62xvSxJ0lMBXvWQ/7S6IqhWyFJFPWX6oJ//abMym
CblfvT0UccBHcDHZN16gbi+nQ03fJA773F2Qca3u0TSYPY6zPvptrHQUm3HpxkH3cvji/QJnclsd
QX8pTtKRtLegCXdg0USX2J1CN3UlZV4gxVHkKyp+K4xgDY8YW/XgM7lRHOIH0vaeRPAt6Zrc7ZKf
/VLoSXRcVXPxBfciASwCNlDSRKPbJukvVUxAyAQnLlTMZW6F/0te7c639C13NgBCSMMZ3+d0Y6m4
rnV5JpydJoBpyPGMibaFb+OTGRl4/DT3M0/uSdNlgfwSPy37D7SxFfNGylKOUO0AVeu+Lqp0gs4U
X5VWHIwhpHcNz4xF4lljdc4gMWecjjrDsC6d6q0jO6eGWTpOrBeA7xYYwALwQ4q12jByqOdWk5jK
7n9ZzjgLNLGe154gEd8N55MfiPPNAFhRGtMBjxWDHhQIex71q9zGv7IlYBOF/d9YI1tFp3fg7U8E
cKDHpGs6I/jP1RHfst4XLIUzIxYTJM4moECur/8DIKnAAIqJE0fnGzz2ITeottoSlO7q2QYP0adx
kXaI0QSsKAmfOG39HCQW8w11AgU7X6ED5ZvgLi29d0MVGe54Fsdksju1R40M8SL8abN95VBd0d75
AakbQx89VOMl9KLIYmpmhPT/6+Km4DzulJZ6HViYlPPrpJWMDKSp+iVqJS45C9sUj9NxmuICkQu7
s2NI/AA+i3wwVTvjxF80M1S1Yc6al5ECiAr0JD8I8BpFbNa5z74n8FBO4+gdJqw6S7FUcGZVf6iF
pp0i6mkiBYl/DE4m23bIb8v7O4tLENt+9fFvQpiaA72iEImhmtE01QWi5kAkHSq+S6igekYrfm81
uuaStoq1uOtqPQI2FcXD+1fN92G7ygJ/Dul1Ico+deR2njWDN4/7rctWRW8Xm1pA0hVleBIudeWI
WcmMpV3WgbWivrhVgjan5/hf8eakgqd+q2l5fGnWttR8h2b0UtPHk9U/VGbRAqpkPBBdWq1cJxk+
pVXwzGEPcebUPCj770q6s8Gw85a9oaG8MwtjAuNASz6XCoqcCyvl62rECsswK/xF6WlDQ8Hy8+VS
Oy/Jn1gLwzeL+oUiCzIlAPpWRcO6U6Pw8PD2OmBtBUQu7LXvVwdHx6VgNsGIaPgQ88KcPHG0sFAD
dikP+jVC0nxS/Pqa0uFUGNhWhWPxfP+/TioOBsjwtgzWWQ4oD8eN049kwZ1NVVcSGUWSkO7cYFyN
uB3mdUl2o53i/73D6C25UQmdsEn6q267MWsQ6AVU2dN+3GHHQ45wV/kU+C9thxNYLNMpCCQiPfKz
bWvwkgSU5Kemos2kxVk1+syBL68AX7GIGu4NG49XD5Dzb3mrlK4wECCm5X9u9Ab0lPXcOwYV8ycr
caRJHxWwT8OMrVF0DXjRWAptJMluA4QCPDz0V1SStUzKahwYOyQEmvkC19iu5CUI7pG0q2wR2rqo
XeTQ2OgVrxtFdxVu09MPuQAxZ2zYGIsjrqBmeKoitMINchoQRdcV69bKz/JC8GsLEVLle/Tw3IqP
hw/qjadB8HTFC+ZnJ5gytigAT75v3DARfv4gdOn39+a23iD9dcFiQUEV4BWhGbStyeycsttFauYv
9/TUAXrTzuRW3RgIcvISq3YqKUANoJWHu5m2Ui/ukuV3rV4ubMjugAbMLfSoxRVq0mvemP0sgrQZ
lJKOc8UkQfFn13/wDLyXvBThcRUI8Cmdz+fXcC/Hx1KsslC6knntOGfc0k7+i5SBkLP+uIPHcJun
bjiRJppSbScCYAWhB7gXk113qvMXcDiXFvSo+AuUINGy/k4lMUmXdiGtKIt4fDsnPmu5mAu8C+5e
crQcYqUyYqCzEumvfc32x+kbIe2ivktuwBu10DAqyP6kaApOv5+cu4iFRtazZCKcgzfNSf9vCIrw
nHaHrGoUOpbxOBBRr/X6F3M4RIiNwWaJtlQETAX7KX+Dz6JnayBFqU0ClfIrQbUxN1axlmhs6b4W
zoAmekj59ZYG40Uk0Mc3rDUloAS2e1XKw8jxjKGOGtRSFKSLkGy5M737PPDIHNEm2GWvoZxTcTe1
ZFUkm7NPd7KfI7t6PoL2XzbNvs6qWMVxiFYKUKvDf3qhZRSACnEi79DE/jZ+qCJ2rVjEPkoE6htW
gV1kE1NuJJjYDwoGhWJYj8h4tPGxj2cKVbRmgXhla6shmeH0lwB5s7ETRuVB6x55ZVxFA56IjT7V
fh8xDZ+W609MvwmgBQa92B9XfWEbytN/dhWikKaSm9Pfg1XA/ajbbz4DwX/Aaz0nviX7wsmn8w2X
ibee0f+redJtsThuWD9EQn+Ck5bBH+zLtP2TU2Rka2hEgZjIdm/adngJHpzElhsWdMlhWvJCGIbi
SJtPiLndheRhp6alDbN0KikJkRphJkdHg+gQnR8gN4RoNmEF7EAQZsbErhIjPwawcgCKACLSWK9J
jF8kwxm2yeMu0ZEBc0A4FC06L95kzsuquH2Ov+DOhxzSjqw46erz9lJz2kFbZHixP0lAkb0jiWrU
3BDchOmcfL+QXQSKkWqJSW+CPwqd5ePMLmwHDL8hE7yZ7MorEivr7ypq8PegaAdVekzGTg1/7edw
Ut0VJvJvDsIp4vREOHCmK7mes2H8LciTQn+p2z5jeuTW4bkdWTFyeaVLsLObA2YVIfWDN9XdicsF
2Q/nqvNU6EZP6FwEUBc1k+7CTVjD+ThBFIB4mWQlBq6M+UDxwaRLuFVaOvdm2Y8aPDPdB6jdP3o+
pbdhncsqqbEwkPzEB2+gcfFC2l8k1iegTr92y3HaQw/mp0CcM4WKBbQ5NEqbmS6ZgxOxE+GWbseR
5UzN+POYD64euh6lqyKWjBvD6pQk9l4bjYF2hP7X+7CZno5n+YPtoN2uIcN9VxZKPH/gihkLHTC8
u8vb4b/L1574YWGMhekbv0iCkkUCtqFPk2NpERocGoY6XC/adWLyIsXCNW+ZSsTjl4PvogHeZFUq
32cK+aBCT5rhQSr582PSgOs1cxHePTGbCEYjEQ7ktSWe3QhgBsk4J0YqoJqma98MXZm1a7oZBYwS
g/Zztc5m0Nxv2QLdUpb738IM8ho29WALle3f/c1DSONFPSyj8SjaazChbh6frXWR+v3jMUuhc3Sc
h6TvCwE/XhCNgTvXp5SlgFMUpGVCbTBomAe6jJD3Y57aEb2ezW4t1TEhTb4LzFRU/Ac04D0hgw1H
zVkaIswsKSbi1Vr4Zw6u3cCc2h14KqcZX2vE21ZrkJsUryPEXUPGtK0K2d49Kj4J/2PS7cP4nuDM
zfi+bLkesBAA6OnxOGrH5cxiaVgxQ1qZgCsVI8OvQsNwAzncAjSYPN98sp1KnfgSBr2caE8B0f6C
s52IOEH43nxykg5q3rA7jUCNDNRG6MnCYNtrND/h72OXJkc9g9m7NxuPiidUHjkiLbt55/mHC6Fr
tZz2kQd9znUOI4jWCDx8hO+c0nPHm8fyDrltRQUoWK5Oo9mAzqPbnaYxlkrc7vuqFXip07YWABmH
dEjTQJAoE49lO+WcGku/eM89E/ycgwNDsklRlm/fQj8e/ofZs31X+t8FJmmpdT2yAIwFrN8ye2aN
M3nMivkvxn+yPOAhCzKgMnzw4JNDRnl6FfNk+iBNsNmkf/4FWLK7iBxoLL3UjdAexR/IYP2v41Yc
WTxYXDsOg2Zt08LlMw4bX+249OmZNWmCcvsssIfFI2J0yfYs33/SsbHUWuxjAK9Fd0/4xoTWOZUN
2TE6hsQ9CjqB98ADNvsc5hs3JW7siSHxYSJRW6u14PutB8n3FKrCfF+1gVcMZ0XuSc9nOU+mCJN7
yeJeYPdHIZU4BFHjgLgeHm9UJnFd8863SlhNSN22R0onMA8KI+k/8j2dZvLFIgI8kNBRgHQXHNMM
fhc+q7KSkbF2ViUTmQy7Y+sLKfd7USqSZPWGWV5jvOSjOOmkOJQdXTIooX+cdJZOj+tRNtznmTTA
2gNzyOWSZ4ljj8oDht0N+qD1HHidgvdHGMoHyA5gr3LCsbgdU96OvJm3s4tMlVsI3OHL9oS0H8ni
2Agmoj1eI+weONBOrz/n/vVFDAfVW+LVz5uAzOhIL9PeQ6ioOxXpKYDcJOyR3wea3VL2OS/+Sq5+
Vac2rgaj7oQmK1KkGOrOuvyk9hcRHsQF8DASN11Z/xyenQZhbl6bgQIuyGv5k5WX+IxoAeaxnXf+
SIb2feNmScVvqV7enUkViYmx95Z8GlRpcCWWHvciz9+11SaNSxvwVQXECoOe+is4wXNoHcsnyus2
Hdal17oyTK9c0hfQJKivZTr8VqAGkN2GE/NJlsu7atLKP1fl9X9uLOaZFqQbs0W4Jft7wIJdisL4
jFcX8qu1u1vWgZnFr1muXMoysprlH4133NBB28Mkbps9y5ALhuMOxG0n3POKPVczirywRNzytqHH
Y5H+nJl0XiIzHiFL97XRF0JIHXM60195gTCo/j8/pws2M0jwy9isuKQEV1CLf3CRss95XNTbSLwY
EpqYjGwP8CZGzdVgiNVSrgFCvI0jrulVkx0qakgzjlGWPq6OBpRPCG9GKwn3RavWCQnvvUjs6o1v
PfwUkpHVJBx/EsNy2BBNFcOdp81GFCWY4K+xOxLJ3CnwrayZE//zpWSROcL601I3I3/6gceClJAt
H8VJLMKJWjPtylbL2dZyD0e1cFkR7a5PyUNOStZnh1zbZE56G0/f9ytA686oZv8X63kYSwXPy6hH
F6xqS49JojgNjH7Gt9Q7kLSaxd48vL9fuBg7B0UXMSRWsdGEUxRhUDWYziK2jt3RRIE02mxqwcyO
FFgWbk3cXuWQkAsWgZ2/lkbWemrfTdhJExt1QCmOm8yBQMg08v8K+29whxFPxJd4VzsyFe1KrZ+m
JTS/sTgDWGszyqK21N9Rz9m20uoj0qciHHGUWnN/sCLG7v0UzRRM+GMVXkQl385z/bYgA239UGyw
TmB+n2L72t1yr1BfQM9CC7WdZRcAnqoE+VuRzGY/4Lp0LFVO3m6By73e1H0AtZZNjWnXcfb0e9Qu
PqaImScrg6Gw4nLXB6K9dcqPEEmgxZtIFiTAlhpgcI1n9NMd53/HoncKcwMBL5srQRf6lw1PjC+S
5b39OP0v7B68Wel6aHFRZ4kijKsY0zF0JafOysMEYXddvqKOrlG0elLG0vT1WSYz+dluXo/H6wk9
ncUi5GMDkdwzrPQBYK5cRJta8Ns1v2RJhG+HffZ/m5XdS6WElvJnObpxfdBT7og91VquCfIgSEbN
dfS2KQ1dOBI+13Tkfi8QhQvvRa76+xTWxa78tQnQmT/0ETZqDrRjg+V0gN1hTHm7akaMNUjoMRR5
Zaxc0VmGtYsr8w8xMNQgNX3S/tXisxJn0JgIT0OIKhzHmUb1RFdwGdgpfv2bAmhNcznYwfJMALom
92wEx0mSxNmRQ5l2+onvQLiK6x0EaUJh57fi5H8c80prJhLoKX9SP8GvwWgm+3+jSC2e5fqNSPVK
e6OEn+EY9I1AhQgbUF6LukvQTWbEUrGpi4iCRFlchGR5aZ8OjYqzify6MpyCYKxOYGiEbB4JVder
rthctA5ymEPz/2mHCyd/fbfnVKCrqKBVIijuT4vbaHff6sJKOdA03VPZIOYOHpnHZY5ouOln93pf
U7bkjh2IYKZsaXX6qmeluwM92a8UciPe/uHqBB1vvLhEhHQYGofJzykvzFx9Y/zBwmplzAth16AH
mN7sw9yRJkq/5VG2RbZtRbAZxLMa4UqI6v7X8OZsGbwSICxpbpzacsJspVCRAT+MKK7g6nh5x3LA
H5EvKojrB+YrCyx3/xv7QLOrpLFFqEktKl1ptb0ms5Lst2S266tLSDYQWQMxRbR29jXdIFkl3caL
yuFT5p6A63+K+8RQqeAJvUeF5HB55vfrUCJQ6RqjJx6W1nzctKHqHIMtK+NH8yzmEVyuEUzQf1Uu
amb+62uJ2cwuJnkqK6YhSBK6CQyUMnn1Cpc28baQ0z3YnYlD2Zij18xy+u3EsAvKxvP0Kr6WpZDV
K/ZYqgKnjgkHuEqZa4wqZOYj+HQDuWgLAK7i2bQkcmp3mM9+yYZxvzmXqaK69OmpKeSW1ftPGOe4
D2iEcihH9Qyjl283jZikqSNcrH3ptAHxdDHys8cpweFhtBAznHlRqhOW+j5QPnbCDJzDRvj9SOBh
bEbxjaMgelbRaI9+mJLWKVrD6JH8l05VuJGaLxwP8HUk77/BvcKfVw5Oee4hLosNqCZ0gGME6GUh
fxSvhK4x+JUXoxDljAomg4XlwIyI/e4/9P0GzCoLFw08Hsg7+W/iV4WBcM2d2wjjMq7T6xn1rnWM
EgDb9O2knNKFpb6j6+in2koHADwSUZuESDpX3rUobs1vZJAhl9HNRYNCZveMBtf93OLuJz6FJBSW
5qQfRkgX8v+PhyU0oKtfGCF9chxOI8B8+CrZwj/mI/9ryRGPWsz+gBP2EhBuNjKm/KnC3+XUOBKs
MZYxRAVXsJa4Uhp5ZQnF9vuHKsMUdKVi82YzVUMkGNymrXLZQhwhxtXcoEeQFrxgTeklv1NARtAg
sxsTVwoB0y/o8HsOhKiyycQnRTFmpC2IWAwWvlcGmR5+aTTMP2CWCqyzjNCRqSDJaKrRFFZbO4wz
r0LBGr+zw+m+m3XAQVEpkxbBKpmZM0VzCfMDkji4mWMygao5qCctO+DJBw7PbblvZgp7p5JOKumw
uSsBcVsRZiSs48IbMw1UsKHubxdhssZmBi5YaLbzPQ/Y+yaCB0wmBJ76IFUG262dtwb2+2wGlxzx
357EXuCmedR3H+yNvgcC1PKYO+fSyMGACkb11244L8fg0m/wMc5YnwbzO7yIv3rBYGdrSJkAtnb0
O0WeqWgSC2Mfhpex4jhg0mTDDtnRKgW0Eihhbuv0/TtsYDBVhKTt2Fydsap/dGqkeDkxMvC4YABy
y55EsznF+RuoxTelQvrqi7lXcPmaWA60SVw9ZBJTefsrlqLdhA7qg/m3FmJFYKIbePsKixcV00wt
48eYcIsdPcGnK/tYUTwks4qz3uAFd5bz/rLwOYnLM9WCGmx8U1YzZ97iDrzMmpIwufGfxH8nnq5e
x19I2/EO5YEI1Znz8g60dsw8qNbrg3k/P5r6BgmKr8bL+tkJUD/S4BpHHQRq6gc1DvZL7JINGlo1
hOEDxGkjXtwybr1Nk3O0sr8rbsOgRrWlsRvonVw6lw8QUxdPFWhw49/n18QxLk/UReHjiBUVv7E/
mS9U+1gmXq6QjNY56iSAXGtQhzNEY6WAJUU4lsZ0Ba7XBcM3lPCmhwKRejQlkMkpaH6GwdQa11Uv
M28OCD7DxARI+2LOvSkU3E00nc8Kt9y9JtDTpKUm2QHet+5KPmYoJbPZVAm8OTZWxXrdwaEvGF5Q
cXDUdwdMqpsduLB46PPuMRhH9XVSxZNS8fk46myiy+XV5U4khCZ3xMD3dwGNGvcNy6dG0BEk8pA+
MTvVBnLRl+Tvbuh+E5uC2cDVeRsD1ToqcBqPJ9F11zke/nW0WgbXg1BZRD91cgEGyG9SU3EXJGKK
EyazD2GHPxjG/S6dqA7B2WEsd++mGn/NkYKcr6k+jBTLRTE8LUCyiJe7YIl36tLnPOQwPsNqHUQA
FKfYM7jctx/nf/cLhdIeqHz9I61ELidOgKp7xHB7xOSaWlmcmSO1Amjm2XwQZS0fFDo5Iur9+2pK
uStNjYSGD5A9RfqrM1OlOWIBERgLve5kvIGdARjvXKLRekgIX9c9RHVzTgjxw3+67/6yIm7v53XK
I94vFbc669xUDoVkpoKHNN6Pz9GgFWUO4jYYo25/hPAT1tfXwJpBFQ8Sp1OWFXyOrA0G41K1p0D9
XjasuGxX+WuembW3RXt74gGD/1Ap8lpRBukZVsLcFIhP+FDdHOjYc3Iqyif2zwkF+2oymUvXoIYF
T3E1IsMWoq16ExGaOGE3U2RRYunV/2Rkxm7ZgptxC8v1HSmMpGLiTbBrs0tYyejyMN72m34bHar4
3Ll/xgrg7Hvk2ObPkUD+ZM7AT/dtbTAljoEsmdHbOhPrWyQ7wKyTE/wr6NBB+zI1E/czdd2IJeg7
k7WF0C+Z3lAN3XbrBOUmVHOiBB+8AE5wdunjUu5AhBQ0gcB0Hfs/9D8Frkek/v2v4rDd9NOQVVdf
zdFZkXZ59vsOdEgARuEKoCOidKQuBxygEBtymNfL0THaKZbToStRyEcFo7ZpxQBZ7rDwYWjJvYbL
iw4soBWYMZ74DR4wFjEVif3MjW26/Gt9ilpBxt5vFoZgl31a041VX0YpqQ7mE3vpnD7uBoD5QCzG
J1JsOhK/uHkivtzgep7EXKAIAOjyV9jMpM2ADqZ7ylyxp4rsaNWjtnen9enYt3GQSuUXzwVcqtmt
V8MU6SCJcrBpeBEqEuychKdURH82RYO7ZzVMsNxzHQNzza3dVkeiHfTF05zxX40p3CvDbZwadX7R
iW1aDjkB4NfWUUHP6AbOBrO53ZwyQKKrLlldkei8Oi3Mqd3J8ysMbtc5lZHZU4aEGDLT1cF9upZ2
OwIF/AC7ugrkrLAgRLJ9xzJ97nPsR+mrnyNUOg48/gGu/NqcgXQF/B9v2NaKeAFGQR7SJYiZd6PA
VBypWU7v1149W5fd6GHkmRx4IiMZJQVMHOlzTmmzRMGOfb4nkUukL3nvo+AW1Bqvf78R3wlO8VaX
smmwJYCIVVNDTjhm8hpJ+NqS5r2/fN8dSjyor8rvt441X5KohKzdKAgY+R2Jop5ADK2cZxpqx163
KvMQ03loBmUWc5knuMRABBFeP/ZfTMU1xPiFtjeOMhsWlnBr5+ypCje2QWaYdm/diRAe6RDCGhsS
GRmz9pwC2Xj+rDZLpKWibQjVxorwxvMQm+3lIxw+mSoaXWe4lil38yFEqX8zIz7pLb81odwG6Pi6
oWQD/AL8hZ8LY0uVlcxBANog5Nr1ZVxpYnwaR3HRwFoX/BfkxeJ2ImxakB55kReZyc81FQl/dRpo
bPrqpyW5kZljqEZn3Sev3nnYxkfHxCDUdnAv3p3CdQwryQbTD2W8L4VvoVCeUELW7L/UYX9Ha3L0
oXy7q1h5yDbtE2jpAY9X3iXRse87CvT1fPlmJY2a5piCN7Tgo7gJPYJLkt//SgPiIEnK7sBl5QRI
klrVyIkES5wcxqy14yOTpRcVnGm0wmCMQkjwcXlgfqCDady5DnJm7yFLMLhcYDPHJ2pcYIOav1jn
FPXRt3Iw4xAtBGQVgYdFjx2mCTq8Xktupunb8wBl4tEi/bc527+gtJ0QlE2osxpol1uMsFUd2bbh
YuNhI59Zk3QyYG85p4CupMBr+SxuE39CNNb0gLVZXh6v1fcmpkfdwebDD2eG5opf6vWwsdF8VacD
0MDcNG2/iV+AenHA+q+1Gpu8QZeGmAG/oCRhyxQEwxKjcapuLgp/2aaSYzfmWCGq9h51E0DX7i25
2sopfj2VZiw+eiKluA/lKhpLrS74Ba45aReWn9reYs6EzfWVTrBNCB06k7Jv6vUxoY1nX+L6l7wX
UOMMjNt9xweQYKuwTFUVBqLR2cNYNEV2HQupM56RgmM/oQ7Nr19UHZ3r6D8QlQO4i1G18CcqM0i5
ByM1uiGDoD9as6X6IEwQ2mcaRwDvuzpbswbYasI3K869GsVUT/Wf60N8c8/9/bh161VFuBfi/CqG
6cjH/LTuhKG/3PQkuwAim2BaWqifc/yHcCj1X6QXimFiJZxR6Nznb3hnSLuev5yqbZzUjIGUNTj6
9r8G9pUDQiPkuMu6yjmKLO2WB5qy1Aj945yiRed1lof1GEEJzfIv2kpXmzKGczfc3KRjkZoYKqfc
MdYyqivRrv5NznO4VDv5F/Rr1XIwJz4Q2VwjmleaAREkpyUFMulgcsDWQcCF66Jk/tBM0KfOT+Yx
Ejf3t4aCECWvsskTVW5QrnKdEQap6o7FECp9YtTxU9Ue5ltV61whv5b82VCj8F0WdhLGJzgV3aB+
tYK+4iY7zodpKrnYhI8YWZf04Asng8JT/XeTM3HJhIx9x7lHA8Ryp/SA/Lk3v09q23ZNuUAULhhu
o+UQKM20jYiGPx+7qVBpZULu81EiWBbe5S/xio1/IoTwQe7idNJE02nF1sF9GjoeOVczt5JNFPCF
5q1fvBxB8EtKnB0nKLoLhVeQVZiDBWCD+SC37yxNyO5mHO+O/GFkevmibOtj2z8Mm4JPger0BUqD
4jIJgajiXNBMekrhEY5KTEszuV8OQnpw3XdapL/DiHZ4ys73AGS8WOved3fDOYEC0Ricwv65FOqy
MWaB+xwAewJI+utmt1iJUhof7cAEHiY7PIBwl4n44cndGZFuVSYWKH8qn1rdj6eJgBp4+rkYGGL5
JLcOfCGuzgu49DQ9eQ/ZG6XdfqYrtnoJQqc1L1BtrkyeQzG0BbTGKG3otqF1f5R9Dz1sTNJ6++jl
8b9ih/zAqBLzB7lElrKRhSOCdgU9UBPqmc18HSzwhcXreouaFHLfDYN4wO3kKYj9BXaIGboQoVXr
pjlE0mVt/7bAyWlz3cQ38La1JJbCpyLJ0c8p3il6gq3fRYscNOSmrOoQcLqBSglrG7E4OWA3JbJh
LAJ3XP0AZ0Gn8AyA6EhY3OrhISenoxl2bUz6rOsPkAhnPCkgeB9Oc2gF7cakLPkV0ivD7VLrb1OQ
v2OO4JiVRHwROF0xgAtQdDMPGd8BZ+oPHGA5hdsQJ9ilzXEQ5Bl9lfnNBPsi/kdGesYIN3Rwl1Ic
c+an8wwlCCP6InMI/UAabWGn3s2vq+tuv47hgoDRei/G++TAr6pZEZtZibU9zpiAlDY3UYUzUu3+
u0mniwPve3TJNaIzruy/hkpIVshKU0gD1f7agV+pS1R0zMiUWR0YrTnz0J/7Rt8g8gXLr6QZM0Pd
V6x5vpAPdqU3xiED/EZ9EBHDyN8GLes9NfiPWDAPDGUsJA2QjJ4PDgN+Wphzy0fMJUzzW/XKWL8/
3gmPIBjzoDyveXUF2+T35MRmSw4y+7YTbyLJhlHMRYg+6DJzVd15jAPxNPG7KcSWtqdpfNxbbMUf
fD5OoaI6XarXIMyhXxZppnGSV9+s3HD1ZghIyxmSN0X4hTF8EnFd5W/ZnGS8+8CVJAMVOgZuidoc
PuqAk+moRcqQ7Poj2K2qTh9o8EmToRlVYc4XWV7uETIEpDndxBtxYeIK1iyFCWO3b+DUfrMTfHLK
qIQe+498JezHIi+hwiOQNhyf1qp5x80ztDJsoG2Sf2/XkKjJ/xbEXgLDJF5VGS94aZapBQ7ElpmO
woS62vPYihj6UInl/muTHXUHvUNgc/FzWv/BkgZwSXyI84wZLV2Ux+xbfK3I73vz1t2XeAbCKdYU
lF1djcgr17RswtJ9YuUfoEhSzfvyoDEK3vLP1bxs38Kd3gvdsY+tpYQFO7Y2vMZs0UZBKTSAr+e8
4QsC4Y+vDNeVzJsqNK/hPiXyHmVgmhIy6mCXYOuDrOaOEDJZsaCVn2zCMRY3Lo9MVcvr1jmBEuzT
J9puNDiJLsp5Ey7tKtswa1c37jvAfO1h3wyUNcVDCRm/PDJ+s2l4zKV+8z7bSQRL4P7C+mLng3S6
CTaiGJ3wCk6I/3KFp2BFTj2fq7B3z9rKvVXrchstK9HZbHVK5BTNzkmf3BLjba7HsksUPkXJnyPV
1bdgRPt/T8QW9AOiPU8nFS6TSADbeLFG8+HPyocIYpTT092IZTnWjsBIXZU9b+sPH0DdmIAPft2f
3K7OQSAe61IJK+y+Y+E41PxUlEqFb3Do60U5Yw46kXTQFvQq7mgrgOOPQYnsl5ykxuGPSpKl5etd
CCs2O/WX4Cne0r8s6/H3SjqI6xuoCNRxgp8jum+EhTYawgwIZ3G1ur9XNQNuNf4I78vX+vfhjyVE
TjAfUvL3yxJxDBQE9zgg0JU1lgD7GFftaeFLZpWhDMqLSGcwW5RYF2i4oKy0wg17lCWKSI44ZbCt
juQjVICMR7nOriryCbVciSpHHW6P1pEUOhD3ji9p9l97dQtwc4BFkET82BqDPynTr1w9c5H3i7d5
Bi6VHtaWWrcFkv0uHPxOVj7A2bOHzFl99H+ERMMeVSgT62jIgQjJWICjy7MgiofDwhUlN2Ohnynp
qIbXrxu088CfAHflT+0xJActe6/Lo17V2Hzc6+aetpnAK4XsrgO2PUOm9Oitrq6UlT+ni+rjS4LW
bbQ+LPG/lr43H2ObUFZy5lE1gMGH/dhoC4Urg9yHBWWZ09pAqMAE8vfq3lj6WEErS12wBi83ZIip
HVRRxbz4nsoOC2DF8KFQ72g/gP2zlonzCxUOvNu82LSmEvgPr7R0DgOm0cHYfxIEmDW/ZuV8SWQ/
oRdQUADe/sihFyR8/0GRHxfID7fZwnuZ+ABdLiHDoMcLB6bgdC/8r/n6niBmQMzJ9AV3hYLs2b5v
N0FHYj71exazH3shSLc2KqWImoFe9Kl3Po74LlyXwHC31JxTy8KVEVAHzGiPex+O/RspZQAdhgDu
nL/IprZBPXcIxDQafMiTdW9OvFWYcXtPqn81GohOmmWYdQ2JznK/tTP4PzNqCAQsX3o7Hlv1euRS
mcPvmyqRiOcyqmV7dKEticTdA0T8FM261QpLo0nhggYLlawJF8HJ0D8JS3QEEVmipao2TN98jfUn
SQLTtLUu61kSKcfLW66U8+uSsylN0jrEMb3lKbjWWyj/MrVEe4xW5pMRsyjVgKYvHxOnQxSothbS
8XcKRqfxo1AzQTpWnJft/gbBkIzXa5jgcMwpofIE+5M6VAUE0LWR/y+soYVUFAuT1zM3iqC2BrBb
IxdSpv25HOfYAQLIFCyg+tvXQYvqZuFuQ9QdGqIYmAE1hwcXMEL30Kp5+PodHPmt5N2H92fZ/lsv
k0EGoEQ8CzPisIydT/WPXw8MochXYf9fCUDAj9qN56V2D4SfkMgGHFKv15HGZCHncZwO7B+LcgNb
hpx7hAaLnPW5hyVjmMzrgLWeOoBD5oPQ5vAIkd4Aoj970rxUhaNsivobils4H3REDnsDxkZDyUTg
RoI1uKZu2NjK1hACYpngINb26iItFAZ1De/4AWYUZ5Sp3oQAPE9sEQRCJczO/csuRFEevuouew/e
2bevqrD67YKRAWNs2jDtfbGYCyuTHZdfQDXneh5qPx4E6sb6ZhqZmc9LcjPD0bceFpPYHRc+OYG8
Ee10yELPxCVzjGa3sBJVzAfPBMUCWGYBvSG7OMlngyzorj4KSMJy4NqRvwjyoxL6Wo12WHj7NpwJ
C79e93h7ncJyX1WYsRlUbSIwmzZprdv9svgiPFKBLO365MTUs+Jol2qLEvr7lO/vwLIYLMSS/CIq
35wvw78UwVVzlZlCplkl+JXmE6i7AMdCmLE1M93DBmC57ndhJ21LdqyOrZwgGzzjNeRoXObXeJ49
rnhxxBBBnEW9qbL+2YhQtJZuuqR3auPsge+ekRgX3c/andDbsGSjb7J+Bz+c9AQZbiBSGSy1TIDD
sEDWFDk6Y6koK+OlBiBv/ZQVkQ8X1iF7L62T6Qbz9qUVsL0qN5Xgg934+xFov+FqOaB9pQaIgn1G
GDP6qhSBDwbZktCyfLFxh1u5pG9SYpXw1nR8b969W0uack1rS9EqzLwyTK/hGqrTCba0QOS3IHka
QU3Wbh4cMDcrdaQF1XWwx6y8N6hpjQPNRcSQBaR63fQXcCnw4cyv6RZOZv7UOB48GZqfcyCaBHMU
EnhCgjCTGdKDfOkbpakisEekgEtEiqofUXgEVV5fhRVKSpqUVXWn4b/OD/jGDHCussMiELZ0IlyO
9R/AAfwWHbo8PmAwKI7efRMxqDOnEU6UU7zPFSLn+XLMuYYPLXEcevf6pHaNEN/N2QCBFORixOBD
HWO73SDe5Kp4NZMQV4URZErpqDLbCIo1TaADoKphgzsOUnKEjoFoBzbc8xj8t3AgJjPmN/WvAdDg
jRYiREF5OUXlewHqMKj8wMoxiJ+NPrIJb1vAxzuDPgDuUrDQ/J610rHBgPiquH4Oqwn0Gep9Ai+Q
Rm4BrIzISdiQZ5AvtD2RNQUzxSn0rHNhD07A6wLmLWK58JzNldGt/WPEkkQYkBtuQdnvkE9Vhaaj
UKjJeQ3kw8A/tD8qTUGqHqvdRRndBInv6mZW1+EDjrVH3dGRzzU+TEB8QXdEy+KV5fyZJIqCcdAL
jDLYTkJD/M1MxfdDxkTnENRbm7zBP/znyzYFnOMYTTUeZT67gN5uHed6qx+liyNVhOGXU80NPJDN
LTL3dJxzP7WuP0f1ob6nZvyA3rgte9XvUsiNzE1yMMG0C65kEe22xeUZsxrrH6IIN2aUPUMzNvmh
22wu8iyy5+Fi1HO4Yi+5Rs4HWJzfHScPZziQ2pQS6SYrYwUghX/3WavSJtoiXGppjKBJYo9k7y3q
C9BOKOy/1skObUF4KxvWf+9dXS4ol9CNMcrm+V7YCq8PbA8kPBEGq378Zg/aI2VicmVu2oR/V3C+
vVH9bxrfTKTaUoJAhPuIOjl3VWJIMUzxqqpWr77JLPsPcFtV7hwKd1vsgz4qEWQMl/Xvst/sLygR
wT8mHXdQI6eNh4fe2YSIpogLTStXJlmwARCHrRm3P5hMbbxa1yHXLfN7C4BjtnLcmm3MNlYfN9Rq
GEgBL18hOCrwW3bZnN89qRVQrw70DyJgvp7r9R3dDz0QQzTY55HO24IVI839GEYstaez0gTF+hqH
ZxDL0MRp4kZu51ee+dqpbaz3RZG9tgZJRYhXh1FGa7FqMTjGEaskXfEuwwWwAG/o+SkI4hKLDb2o
mWdLJ5RS2bqbJo+OpQ0JUS9m3lSe7P+ari3ZIE43RYuyLvRBp6tJLsfDysL0ypxWMsvqHv7dhRQ6
US/fp0Ptn8HNKg9Ub3HDgBAtNgdI/3Cym2RJtRrCsNL2S9Y8vgc4i6bmpvv8uXmiS4+jCuFkDgQ6
1Td6qMi5v4RM3nEuCKxWle1UKCeLZ0g4V06zl4RL65BoGBBwPV+/GPbDfodojDjc0FsLmRCbccV0
/Tcv4gKpjM4CorN3/dEOBf3xHQ+pPuP7HpVOkwtIO5SaNupeQozusGEQ0NWBdQRkccX+LuHbypVF
a3HJTGZ6kExMLlHMe3BlErWc+9xq6Hg6oVYlLmePiaKbhhmxlE3EkeJfATOp0eNxBtCzk05xeP2P
3b9qJJvKZaO4NoWmhNx+1yTPc+vvocSgroMjkgf8JKWuVMYm6Md/q9jl8ndLkRvA8wDUjmHpYhro
XKg81dNMbQsv7uIZX9O2GUgNCzDkIQloSBxNgmYEbttPNOFBm3TQk5Jr7H14zqS82qnwlCNrpYRl
qNCsOQ3LfUDs94SB8uimIGDFc+4MNXnhmfxi+XJETy1Fra+TeHs76foy+cJDWta26CX8G2pVtcL5
/AYeF5HMyGf6jm9nxdFdE3oHLDj3teU0Mt5cMTgmJnI95yor5kDUt9daPD1iZd1xG0G3MbCyWav/
YD5ugHT4hd9asxMJQKX11jUmpPWi5Uw8NV0ZHOOvliS76U8QhPwUROROdHZnlyExtLitOKe/FRze
4yPfUvw1iL/ridSmnfePjYBXOpp8pj47gxtEN7tohDI5Hm/x3HiLV1yr1abZFIqlKcNGxUMPeVW6
2VXPVh5UCaI8fTn+5Md/LZZ5ET9A+UbQRPFehFsEE/0ZAqzjpzooa/rvGJ1K6ARLfmCrX+tMnZbm
ootpsHgHvvUOHZ4IwSiO7BAtGzXEWmzGqReTNFqL2XB2IwGWmeizbs1mGVEBJTaBR7TNljSIAo7C
KThtw8ZVpYUBHcLOJepEhWonJW/XqUN89gyOpefCoAASxmJ+6kuyxIUFV9XixynFfatRdfU+UvuJ
Z9cAAWudakgWP9AtN1BgkCBE+fEgeFdPb1uNEh2fT3lDT/aun2Q5hM9znASQeACqo/9MjZkdQRA9
1R+QVtOSaGFNJjmJdJ710gFCryZBTMVEJlpJ/r0N1GWwBHqL/B4Q9NI3pYgzmGqhRlQzRtyWdcwB
NOrTcHOCQcnKVMwuQh55YufiwVyU2qBFJ/yyQZWyRRD6Dclbw+GNQ3t8Qsc+DzAA2hCAgeHsfyG4
CzzLZnP07/Mr5NwXmjwhXfXUmC4YC1WFDRsjPPLl5zEMWde0fpa3sid2E3Kms+pDbYc7NqerYUsL
8shrt5YQV8g3B9hg3NefpuVnIk36Nrswd0o1Hqqnch8jdHBOlAvluLZE8sOMusSNEl6yz38Qjumf
Tfbjb+00lOVVDtjgsf2iMomxRbuEYrtCqfLsnxg3rJiDjf0oEkiQs+bjY5hrW17b2Ub5nZqAslj+
E9tPXKDGrkjpD3RiFImAdGaZ6qlY0SmSX7VXQ0Ac691ymQ6r6kPB3YyrJZkZhgl4oirn+b6LzGHo
1RohHPEKl0owlFEDOEsYcBsLe3p0YZnmMSYBJgrhGMIgTVmN/PmFzcK6iN+I4Jv8BTlHofpyQ5WN
5ANf5eV3H1rpdeFWXgxjv9IekaWYEALM86SV8NHhSC+VGue4XEsZKjAIk5dJj7b/PfmwBxSZQakO
YDVM2huIebsJCLeAbWo1jEqEa/WTE3kOSt7qCLc5lUjdRMntOe9K2k55l5P1pF6oQIsXZWTwWmEK
PNwW+XO6e7funPv835SfDxldyrvdP3qeqOPRrs0htBb7GRP8WaU0fBL3nSNu+DG7YBpyZjaXrll8
aCsn1c4FKsu41kuJh6rl6ijwuM6Sd8q3pq9guE+Ncxmo9jwBSgNMSdXRsirZBjwDdjupVVCeZ1dT
T5L2myaySlTKPL4oow4qwTt2nCXvnn/x4yw4JzIN/DYyRHePGX1XzMi3/EJHDbHCPXAZcVEfFqP9
MVXve2efQioIYRylu4OYwEyqT+Bk3mjgOkCbwAcx+OqT1XcyNeB1v/ddc1KRmrh1re7+T6vum3m0
ozS061pZjev7y+Pk+n7XZmSv1gQUDzZ8d2LhEXT5Fe0qi8rk4s3BF9P4hggUWreowoL/h5olXnuJ
55LLKXx70w5Iny6ky74KzVdMJVI4NjdR6dZdoolvbg1uHD74uvZ0SRvLrF7r5Ktuo2z1TbRPewIm
GGdk5xRTK2bmFPk0fic9mlWhqsGL8/NouZihH6S5J2RQhZhjB3OdiM76n/GjkA8STUht7n+rG/MM
dDdD8gnUJJclvR10q2CILTMnhoPxBvHxSzGWMzwfdHfRw0VH7uk3XgkINFNzyCrLQb2PmmxLSQhW
o9ZMmpPoW70YBRm8v4TwxF3CR9LDSHzKfQ2e/TGWSdsgOoKVhnA5MGXX9bkOaarSfyPG6n962SkB
QM2xq630xqGEzmFmpCZDOMt5yS5DzDnOIcR8whTujo/xDLRP0JEIz6HTsebBaWN1DQNLruWm8fqs
Hm+8EJj2CvfMdFMXz4a44WKnKD+oqWKoaMWT1mzSmqpD3eOxBywmZNcpCBqCrGyM+SDCSd6Gd+YD
z18nqHrWyxbsdVl/52Q1FYWO9TgB1rE11sMXPmCUagwhfDIBPppdreA1lJXstIj23hldj0inmJ3T
hzp1Utx/QDAf5xncdF/3PIHgoBu4hm42tDwUyZENKiTb9NIvcsuELiPULmDttYHZKmJ5cwzzXokh
4XF/Jf+IUf9KIul6toWCYf+ESOKALdJkDCtXoXw7wMMqM2enoQAanDnRdEIo8GPchMQ8qndKk/RV
aFwIxzY4jmPXVJhwk9vSnet4pxxDy874wGl7XsYKYKd+7aGrVOWG4Ts64dgCeDc2HWUUadJnvOxc
pVduFFt+IGEFBWJts0XE0sDP0J2WyLqbSAkBxu9Hper2lxHQ69c53yFvxtFEA1x2K8oKmbCWGSMa
V5BfKKPkWimZ9qFVlBTalZp/f/BDysW6ihcxPY1hHfJtJu/7kLVM06J1rrNwiISqtY27jZ+m0QG8
d0sFqSmYyRwAlp47ZCjLFoEwaSUh0muArTSj2ArGaxY1eU8zkJIpzmHuwdSmiM2KsKGxfayokVtP
rg3QIXuwVBKE5tNpI7YadAIgfQRv3Zu2tMuaGNeRqkSqE1jhE4Gpp1UTQqHtb7+4anBml4/ByWrj
2YHC0LXZjvcjmgtLkEmw8QySyTp6L23MtHbG6rwfFQx4I4ELBXW0mjSWXQCf/9mdoHmfpIDBGrK2
+W1sq/nAL33GgWqWAmmdMgZSXiz1nv6n620RyzM+ejZm9psJ6QUYqyJ9koPC4GD5n7WK+73NqkcQ
qdqI5YHH1sR+era+F6YFVyBEX8wlmWhUB6gXrY5GC37GF1Hgx5IQ/sxn0p4011JOOCO1U8NHbgY9
LSueq0PCvwQBkxv+VjNo2uZvJL0A7pwR6mnOa61c1FEUperI8OsrQiZPqwUF2p5/QPO495U+j2GO
JIIrBU9cXKr04DF8es1KVWdCPjL+KwduDelyMYxijGpYjHqqaps5HHhH+laW54Ee5JD7O3e0AHtR
39sGeo97xhAUGC+h7EOSM8lEVRrFsQ2WQsrnXL3GphDeZ83pQ94Bxr5QIHFsvV9+2EXnKBwXLbJT
LFzDGwYG9fXb2CxuCLWT66TxJyZh2gieNFq89gM3RLxBRAW17+BG+kWoVpZNO+dVl8ReKWzICSfh
rGpaDZp8eViToiDhwjKSDANlQYkQM63KDUZNvnJKNi+GPjhx+M++mr06Woyu2o4NsGBvgNi8EE78
qYtMFlg5w1aRVunOn15LMlp7CflWUqEWp4JNdSHeA/3l7gH8QTOTmWttnbGfpuHSEW6xR2SGL41L
HOF0YCEGdEEsTgGHmgB60SZRLGR4CP6+kJZ8d4iKzddlcOCeor0OWW6+ZJgUTrVt+V71MBHmgBQ/
DknM+3XNo1Zif6wBx1VLQpD2LkQMefFuKTc6f14bzQ+Xziak+Nw0I5+s4+bBhkGRk6mjyDDaIIJL
eowF9eA/LjThqSedcJ7pUNmjbW3UNxO9g2hXRM/4nAhWwrYPnFlXZWx5tZw20kBxRujAiI52TtlB
A+trns8WdMuy7Tflv0/+qZ9IV02dHbt+x8nXjo/0kjftvT84532qr02BzJPlT3bwegjdd/6tsJ8n
p5CkSfqUTDgvQbt+zxOH+Cvw3X94dmOfarauOvRTMCOf3PHIt53+PovFjBuUI6xvO6MDpZQbqey0
UIznNxv6kE2dCxTCiu7/NVIvKGbElzSBY6CefjRJP0xIEL2rOOFKmj5VTgbsbV4WQcXwhqLDVeau
ELCQnTcluMhEt2mImrbOVTCEkWPSh8cJh75nWGBsdAUJpoKyzW5hSJtZPJSE8qKYBITe4kBYtIo6
ELSjvGvJi3ZySad1qWkqkQPJBf0YWrKe1WUrzUN3z7QA8pbnwnUy+S4pXbQP5WqJ52BF5nUbK4PD
diAY1Wvb+Bb//EsfWVxUdDDPyZ2eF+PisnwTIELQn8QEn7hwhazGQmpt2nNw0saYnFU8i48axQER
Bj3/ta057I1enLdAOLIXIOEypzfpKpnXY6/wF6HIux4LbOWJxNGMjMYRbes+m9INv5TOkkWnOTU/
+9/WFshcJFhaUBhzgUVD2iF1s9j36jUXJt5ehGbQQV/DK0MDpDn1kjaFLBNdO40pKlwjZTn65v6E
RrcD1C8mDIjxM+XLuaJSbhjnQBb9mBzR0L4HuaJgvGiiWvozbVmWj7tA7aMlsVlaswQeAU99psse
w2N1nVgml5Dhz37jjtpvwEsx2RXzrSwA2/emrdaG0u6STfNxL1AKls6LW0O9zgkDh8GuiXjzK8Am
0bV7eNG2ZKF9dGmCxp+6H3BuIZTKzCYAd07GUQYtJoGVX3EExpwxEcKfRBjWa9l4hnLrbRVX/ySt
KfWnXDZ5ZqJ7wUmSXkpjjV5IfHV3gg/SNnihSpCza+i0La6Wad0A+mSxLkx3rbVpHlTJQ4MroMNw
JgRsGFbO7FfX1Jy5UoNYgt8wHGNFP4zCspVb4Th2kr45vEbUhiJ3JN28LWyHXkB+5jlLlEVjUVNw
CZWjrzOoSRqvu5b4Usjq0rgcfe4q1rPdj5v5IHyxDA1wpthd0P6C9BsblL33tJyLqtUJD7oKFx7s
ubyJBwPesJoIQHa5ycW0wQcTOCJwUhCnYY0+HOqWFS/UbOj5VKbO/AAOQ1guml+12R1pF8FwFPVO
FfL7o7D2I44qLIDwLkZs1Yq+KVZnlivusxvIGh/bwENSthixlNgp3w/8ym0vb/fM3oX7KWDYxy9J
PCTpvHDkajUfc/2XJCCBwyNQ5/X6waWc32Ga4OkZyfnaZz9oYC4PBx41S+5ZLS6ms3InQwLgQN7Q
qwFBPYRuZKgQ15u4vq2NlfgpxlSvWShIz+hoAPrCeEQ03k8Ks/oJfXcbcH2ShX3PFvzBYI7Lxd0u
F4qQ0FKjQGDb1aga3SuTIOLlV1I/WetdSl8c1pKb2E+o5x+kVklY4egzyMsgfIrpaNoEiJrXkyJU
u9nVvECxWh04KQbM5qAqx/0p1JWe1UOmGUGy2nPtkOGDqU3pH0FqtuPFNFpueocblO3y7OBqkf8i
pZ/pgcmnwQxz21QeoLrL5FMkvAtyeWqOe1k49jHad64EB33ejpBuXP3e1q9mb5wymIOzAlHSGcnM
KUj6m+9DS0+4t5GfV//pd4CzLczZx6nDEPpj+n2xjviRBvo2n/DfaM2QwK+/b5w6G4IVaOVn//H6
DlIWNf99VvlH2rELn08lIDREnQMSDB91xKWkWreOkHC1c4KFs9IjoyecW5yf0Zpvzvtw98G+S3pZ
I+ht/dqmXZElVKpFZqEEfhv8MUwgiKLsrW+mni3+zE4+qSQD5YipRNdEYSkXYbNEJxM4LxntYVIF
NbaQS6OQnesoBEC6sadRkWJXlbuPFO1NRHF8+dKaXCHcCjA1P/q3wOFIc1gpqRv6x8lVigG+UaIm
EIkW1ybKm1cax/jYxQMHzMT/vGL7aaIy5g9wYBR+KPcEAJfdof/vlk9emAQiVQWvF8ByWNHcIXyh
cGcGeX22xLhw9vQPNBih1MA8rFZtFwDKybmci0HczdwalR+2k3T6/QQVogcETzaage7OMYZxbGcj
VUbDXEJ/Y2ICOw/N4PUQUWO2RwpHyft0lZZtJq3uvRuoN2dXbeN/p2Z85/buiIzghgpPwzPFCDYh
z9TvXcnXY5HZsH6leP7Rj8x7NThVciyAHtCck7hSyrITDZrefKLgyFCAXoJf9/kjp9bNSHWPKLcU
SbnKkFqyg+lNsGsxa5DoAJ7pN9hNgEKBAl87uA/CD1lLlsIN2asS+/kw7aE7im/HeTkJtrhklVj0
woH13t4vcP4WDDFZ7jt4HXZE9pUQzNwP2kO76AZjdhaDTkqBPyr24vz3lubyO7KlnOHwA8TJ9G1u
Dr5Z25Zeaq33ORg0Zyky+RWNsmZ2jRX2KWLMa6L0ZvYKOOyiqxPncVOeFAtMegcK9NpA/pXEwiQG
tHZVLFI2Zx7a1BNTpO0rwGOaqzisrZ3pFNEHrJ5ViuWn8c8KBg0XEsR49hkxSrjghHXpYV8tr8T4
XIV0QT5ozjWXZ3zZC7UjaxQ98BvKCqQkpDCiWrASx9pzjWe5KAg4pGR7mEdA/+HV+USwauroz4/S
d4tpAkeI+zizFsE3ELhzDqpUyifezMmlX0A8MJytqg3SwDaJH5YoaVGgX4tQAr9/Ge8kTGc5aAID
kalLt1WvPFLX452cp3gUF9JBqCAL4Z6LDw0Zgu1LHORo0+Bp5VdZGMNZNFliutswa+8vMjflExBv
bZ0OmcAN0yjgKAGGnKNXuiDm+K7N2xq+aDvmjDkH/pUT3eWQquQUCakJstneYvA2pRbdyMJm5iIe
srB9SkbLmMpv30+MefDxAIkMQzriAfHgY0LS/xhzKwRrHZynLRmxv+k6R6XoHs7vePqcmARIPGp7
AG+785cr2/uIgzPh8a/XqQv6v16pnUSrCV9oR0cMjSXEIpZTFbtrOxl+v0X68MEyvGI4E9U5pk6h
CgNPPwhsd7aBNUAQ+HEgQ3AHAH3PuDNbQ6eX/1PUftzYNblJhiMCNAduTajrT/asfRZ6NO+k/ZcQ
hf2mbfX/PNddXrlgqeEsDlgP6fRjPtXoHmwriecEs/4/xvpaYUe34S4K/wWdSY4v4Dn+V+FJHm2t
zudMqo2ydjR8duhgkuDWsfNmEwKPfNgsD1ecoFVMj/pbRNTK8LGUI0BDkR27kbf7aOneSaRKqQ7f
sxlyN0bkfbeHP7hpKy9jNI52QWfBKIDpx7pYHj8gwUxdKCsmwWvM7uwCJxG8dcFkon+SAwp9Gxc7
kWcfXjfLBJ5M7JxPp2G3vKxFH6Q0C+ofDBJsEd+u8Yz4tmIpixLm3Lzil8dcKk0VwRz8BAtXNNhm
QMEqnMjbwY/wzvdeXERm/kcybwnQYNDEg5C68fP6ztWfG2HtXg5CXXHVG07YhF5rAlIO0kkz7Xi9
2KAVI7fQpQhUNVeYnVOlEk3epmUNvNWRao/3V5Fr+1kaFZtBrQAVbLlMBnt0U1fQKCbXBPKHfub1
tliS+uxmXKJCy3PiLz/U96aD81hypvgXPhPVEELMjSQbHsj2XcaVhu7OJUCHHFmXU6T8J1zuhM2Q
msG57zJMO7vxjihKsTshVsuj+0cFTyhO8Xl0ujqtrvssGtIjuvbknL6TWv1kCU4vAUN1AwclfzA3
vBAncuFF/RCMvoKEAem1Zs8WkqzAEeG8H9VDYdSNk8Hho3UcnNb0MVMi27DD9A3yQRZlso8JYoDh
dV6XFQL2n0t41SbqSyoQDtX+2aaTBy2JK4Vl3nPhFhuFgBU/tOztlCRXp2yCRBPp1JcW94qTALQ9
FdIMahaRl0DuLhDbgBEfdyAxREbEF3eFkvxtEIxR002ZxYL+XmQwUoO7QNA7Jn4jE3eMkZilHCyw
qS8milOy8z5vOlgvjk0oq3+yXlQGM/mpkBJoRt0Y2XPNbPkDsI49GivRw4htYr3lOYO7GTXSLTEf
sqzsy6aRtVySwV7sa8FOPrujP9vMryNeOrcPpWC5cjgIA8gOBl7fIET2Wo8Gm1VyvOe7KeZ1CwIr
4tuuDoxtV05PKhofRYD8cKoTG4XVhMlgcgk0LVAR0UZbY4Rs1NRVZDMVSKpBC4Er0RGmkpo07f3J
9IdOPuN07cyc0/vGrmH7BO4J2yuFMniuYd0JCpEfla9W34EcuRiOGZD+SHyTaOGhPUV+RcKjtls5
Sd45Y+PftqFBe1ZPD29ssXAnQMKh+HpizTfqgi0VRVnUEmrRxgYLaPkqWdMrJ2By5JyMT1sEBUz3
PXYZcBolpTdBHaZhA+gnn672Vk52SEiWxge+pvcVZQKGipHYYYM13HdqBtocDGJUuGRkoFNxrSP4
vBYeglB88KsEHtNG+0pRR+6WuqYcUclngV1OF2hc4xvyyOzOyCpjwQLemOzO5dmRhVFJ/ShGQtP8
rYavIvXQi8FbPJghDQ675612g+cjIwc/XBx1JWH8NUDBj963GcbFGdYxYDigBft2WbGgg3Vozf0F
GTx6M22+Z1urz2EG68De4Rdn+AxGjO/7ncMh/vtlvESs/89eGoCfW638Otp+Jn36lSGpWciiU6p1
+neTCNbGsTK76MqtnxYF+bjOtRzqcwU7h+jeLI+QX9Hb3R7i7WV3ASCEsB3wrSjZgJfcsTFWaKMa
feLP2+OmZn/BwOsGJa33aClAFau1/u1dwHIQ55W54GKPqbRHDFPTmRHH8TCror1C/BA59SVrvjp9
btpTDL8FWDTgqo6BF8P1l+/QY5J7Sba/nRarzVLjwDIYRrTYUJik13FlMwXTAxA6ZAA11fmjArR5
u7AlQTvSEMaijE1SRLqrVhKFR3xxIUTD5aTHT9m3yZB9bDJXmm4iIfVAt+izt0vrGY7pMUvNgudi
4foX/yGrrIH2mTbEG3x+5wennHneEpmqKHBMpRJFWjPHhH11u88/OQEnPOa6rZNg+I4+U71reqxN
lxxl6BUl/owKRcWjs4jPvKjAG/7RwF9E2ioKOYrkYj5cTfmwkwUJJcziaMxgV9UAkDkabIChTcfw
fZPU7S1j/YmXGz2LUjdAeVg/96eYvSC4qzwFIMhOccDKCFLl7ozczYXNknqZih0//fwRhf3K5gKl
98cUcvDFS18hSlKrJHZ3GBNVXxk3mEvfrU/odnKF/fIj8hYyIB+ZwLn17kadXdsJRfxakKXEZA3Y
/FETbKFW5Mt83o2yynnUJluOg8kmIGQURKS2JiXYrq5bW0esPsjhmB5146zh26viFEYmYIeHpbkS
cBHGHf9UU+c6HaXovmXhggSeOxixJyOn9dJot8m5IbPzmpDh/0jKXrfHc8DEx62CHDCmlkmOvrUL
MPfJJyzEQLvdE9uPyyG78jR4wNhliS80Rom4NDhHbsXGEl8WNB9hIULD0OIFXFcpgBRqLk0CPcLL
TuUNBJfEA7L7JgVOuwY9VyMNWvnlPcicF9qsGIatCN9271TwOOZsl3k3b/xaQUgarhe7V4oBV4t9
0U89lTlWhTq3JFcSc8KP2x3HKgxgzVC1prwC5YxIzTe7X1HoNF+SbzjyD4W3E/myRovYKSEC0+EO
syn/Ci0WW6adC7cayn6oKKcJnQv2Kg3n6gKjvEq4OGxyZs6/arCnmdJRoY/g9zA4X4L4LhNM5hW4
uJx8eJjYpjZbR8r4lo0vueAoL/sgZjixm4vgFPKVUpIrg4FOJNkG3N8ELmjBQlPRc6fyfQzK5aLa
bUBIW2nV1ZTZfIgS3JD5IctFNcgipx5Git8qL2KYLR6Oa7SLJX2Gh1FOTAnAZjEFPP/4gh984Z2f
q4WJiUtkkVra0iQe6eW38f5FDGK5gMflw0pd8X/RzWY7L4zOLMJKLZUaU3ecFnkRJuKWbL1CSEE0
UpNgEKn2SB4YigUSfDRWsxoZeTsGjlTaiuIOW5teK2ZcI+zwxpdkVvcgOLXIct1AQKEmF02ai9Yi
Ua3d/ON63Kt5GaSZQ/CmierYs7EvMadQ6HC9TiAyPhmloOMy1DYbTj38lAeVvsvelkZRyx4nT+e0
Dg8N87hWn/9xzjHLamQNrp/QtVigDrOdwYEZ9BeR92YgKiiHkKzIInVmWCzeT60Jjhf7Amrrnyu4
Fqczw3n7HGTi2BtlH8wd+GsXlFB5rD0Tvdtn5dkZfPsqTluCeTPUY/IGWzu3cGtZHMpPyQoEySs/
drffhHC4wfBNKnRaLJkqPw3Eg/xEG78mp8aQMqcP5yV78YUEFSt0qHK7aYQY/clI8n+MhMPBU7Sw
ryGW8Hc8seW9SlEQ6CLPe3L8ccfhwkblAJcg32LnRwU1ooAmNEXDtBLPYPfbapZk3VGG0fN9TzzE
WomxKPbGQ4DKIqWawZRAjOztsJZH9lm0lgZECIdbqfROWlJ3Xob60OCg0RXp0N50T/2tHznX4vZo
LgG+qqg9WOHc6lCo6E3/lbjWgQfwAepixxN0LO1uW49VEHgz/hJv0RWoJhxDJh5+e9i46nNUzpnW
dKMjqmmkgwb4eOBjUfsQIMUxWsJI3AqPdK9vaZl0umFci/sonrpLmHEttNlDXiT2TSz0mz2mFK37
hZ6JMx/ao+4nLg83bIPt0aRVwUj4XPj8XGQbqOGTTWL8FZ5OHCGpavkCn/vvzrcrW4ZP2QslgfCb
RLhyNXxhRB/D/gwNOWi7rGIG/mG8gvEjPK3Pt31w+2A5zKIR2k6cmSmLPpaVclLu16l7FKr7mlfo
G1tSOr16Xo8wk8leTmeDYrtHjF21VBcieJPe33xmHgFlihEB3BDO/vtGKf/gb/Z2bL0x5wsMoAr+
h7cF36U6aovrWeD3itGSAIK2wGsJBmIJkCr5LzcCmw0Lwb5dv2BLcwpizY1rn01hYc23ZT8N5WcL
DjoJorLNKOU1HMoGTajML0Enrwje20MurZuY3LEOytcH85qnSKyLKE+cCDPjVBHFwH8Ij4Z9UoVq
VLzeW/RjiJAtoTxuoWvE+tfEVRmbCHMSDunjQB3QSS+JCFDSnKDEbBKzed/6pQlrhctbv5Y3Ry/3
n47JzcLfEicX7FHKBuo9DyxIbpsRJiWuDOC81boILFLmk/8yd/luo3bnGdSPAqaAHY2SRITQq2gB
ZoCvW20qGF6lxvffqqspmANtp79UV5rLXCnhmRX+KijINTc4hfhZLEuhlb3nAV8Acvfy/ILw2+yp
spq92p3Csn45s7cSdIuhVMkuleag+Y4Km/+SagIhE9ikaed3LBi8d+LzjKpRRRP1pRBdTu/REJqA
Uq4VCR96fuKsDtHVL0ANOvZ+PBdisyskGTJcGpCIDU9Z2A/DW6EkkA6NwEjFaTAyIgMifqFxpgIx
TxRHJax1inDmPZ5wFqmgbYIRF8cuxuD+KxvhbMfGaFG8SElyRkXrxHE0XEwlCMcHe7nGCCqE5FAU
U3eduMvms+asJGOz9gMaJPPpTsUdpOMdURMXlyTDZkn9P440CvKeGaDPoBUo7TTdeBRBoHYJO/SP
PSrCHJJpcyPNvEOxsy1okJWzziD/SyOc8JVPwoEm6NzcbHjm7CCUzJyF1ONdLzgNs6A0VIMuaHxC
kHu+WG22Ur1a0olnltSLszuKAcvl76SDUECNd0icNS/35zPa5TgbyP8KJbQFbNtMt8vuFuePsADf
KikuCiznhdi6SXat0tk0hSw1TSgGY1UiyxwnxzP5/ayXX3tOzHXLWrOuZt5aKg5vX/IlmcgWEKMN
Xju2Vz6rZmTu7sG9WhvTb56CG9xP3wKuTsU6Ot55UWGNtwa3Zk0maarDigqbZWFhBgdQ+jPl0QXL
ZAIuYYXHyJ2NThoupn97o/a0qnAdZ1ocC+iuUsRGgP0gS6OTI0rmIYGptdbgqa+yCp5wHaBt1myT
TngoN4iMUBV0Hr8bqkQt9sKSW3RSxs6pXACH50v49CBmO5FNijqY3mEVg1KxJc/oog10l3V1hP9V
81CQO7tbZ4Z3N03lG/ntem/4ZM37e/vtilCOW6oj84rRLLhg6E3hzmrLK629b33PtHLv9mB2fou3
iNd+oCsmCS7XC4FEIJr3qRCB+IK39WoYrUDD1N6ajAgp/LUMs12iSbIAh5YdPrd8cP5AR6/bNvIa
KEGdzYCm6Qxsho7DSgNymENQDvIBlmDgAb3WDko8G4cSax/9RdqcWRGJklU2tLbqfYLXzf0A4b6U
fuYez6lmwogRLir9gSp+TlUsuKKtluHyqXQa9M+Q16p24aNEGgOaW/JCKE7pZj0VoNhd12yrc+4y
5lDFQa5zp+CwIWeD7vjkfJJro6orsSI8SyScJg6+i22sNJDD8JQK+KVs7c0LowPOFl0XN18WI4S3
tmnLiscHaAlkrZMrLSkBMMaNRgXjUeQoWNQrM+CXtZ+SjsEgoHZOeeDQABGrChBX9qzEDaO7DI/W
yGs4unw18YQsJX/cPK7lsXjgNXCAgJo0SUsSwrnYr9TPKdgOxE0VOg+iVuDtjAdV6HzRo03OTeEE
BFs5MBo/BX0fp8JZ9L5Ku7iiPKFsOvTAWSxrD8g1GvNybOd8wtl/DYzj4EKsKLAcx9Wu6b9yAfAt
Y3CFLs1qaSugCvmKueCsNcnSnv6mB9S4DAVjiSXWbz4TGxgUQOuNc8qZQ0D0nTGu6ZSTo+q26OHW
d9qWleoFnMIHYLfxJXBEPISTLs1ZpUdeHOfotvp8K/sqgmjh0GBDQac7+IdWM1vBMY0bI22JTUth
2/XPmV/jnHt7cOi1IWSh8YHcpuJEZW1oW7YBMbPkJUAmnbzfjP5iLbJ1nkgrZkKCL2zHLFiOreeq
wj75A0ZpCaCvVt7Ikhy1aIe0o4OHOowy71w9Fzizxt2tqFe055VPlQUNsK73uv7Xla7aTKqltF+k
cTvFHe8VqZ+qY56L4zFyAk5qYDJeYTW7dZhCcaHHyFzzKNUycPpTM3dKt1hpQFk2QY1zvE6r1z4B
WlarHUsJcnJp0fMsWspHRtZE7YEtqSLj7KsivFWYCF4k332MfbPvENHLX9N/upo0sz5IYgTcm+as
ds9XQ4YGJoFn2YzMU4JH4xf6lbFIE5OCGMZukO2n6C3xlhItEXkX6YhXVVdzVg3TtTm5LuCki13d
7BndudMle1xGMczO/ehm3qbKwvWGlp+aqDi0DKu+MrX8XiLw3x3ru92Md6PaEPqruxPXQyQu/Anb
F7MG5LM8Fldut5uEtlDkwhshiI6nM8R0Yo9h91u7ty+zAU11q33FQOg167G2eV2ey+yo+htQdyTX
HGhsq/D4gWXXszl4wjuy/QWo6QKB7aWxr5YI8xpA/jqfKa1W8Pbxw1uw5JF/9SBqSa4nZHOLsgdi
cE4DtQNqHoMC3CPJm6yy7AP0jTHvFw1/jlfxnqWAwPcyxzeaxBQ9LnGrbY7fzkQUiRZG7YocUF8H
C1KTnv9ZZdhCEI/HRZ7oU3RtBBs8SnqkXnQ2FYaPU7ukYgSEvQLDU9xGoEskjZZ/prsvvTkZkIBI
lF9v1SdISDG22ibGu77zvDpFFk/Q8UD8UkN5ACl4gkQLLv5GAnC29lh+WtAXS4KOdcBAKXIYQe6b
lVBny5otDpbP8POlcDHfCHsMGTenEgT4cre0OoeTpIn64MK+RhQXmT1ySzK1HXrOIjAy1XGA0C39
yTIYE3MnoCuf8J/VJ57x0SFKODt9l8Lq4lX9EvaH4Z1tMSreA2QF0iUanI/AG7+UpuXtMEt9sl/Z
TZFWYwj95bZyQK8tStVOTgnTKI3upTKqQb8OdQGzsKGDcAr2NDqy3l4K0Qk52pcuUWddm9wsZVvg
o9a+yWP1W3jW8AsYbMxbfSG0qlLSwD+f5HVvRcs9rNx4+ZDNWv8RwFa3vRpDSCKq2tyWBdEzzFGP
rwp29zNKL0GOF1/T2x3ODc/NH2yGl8d65U3/ccOnsPt/7krMoz6istKzRejzIWHPpDoxT84sJchn
MBZFy4LKgMuG0pcorSk/3dU/jGWsjMt8YtyuoZL/SQYoKdT8cVEnh0zBRBNK5YwIikPuoJGhdDC4
YPc2GR1v9FHeks/J41joALaOh3mXMjgxPyV5foWeE5FtrWchbTcQEHcoTpVqAyzLJsOMn6gV67nQ
2HhsOJOj1lbKt6VuM6S3ZjBLPUOO5LMkO+ajgm83R6SQvhESy7owexMMrHUuteq32scQG/35Fm1w
9lgq7uuyS+0qTz4aSekeLcLsvDX7dm8H+nuKWFMpfWts1b0Ti5JOBQxleqhKYSvTCwMXu9/gntf0
AU/GkhdT+XBA7RMpJYOWlOsxbhPzrjfWJSDR5BHZO65G9M4y+oCB0mvTopEpljmsNqpbDMQm6KFB
AYu0YnU5kPjogQyX0pKwFU6v1rAMUO7PM/P91yMeufN79ComQh/xkgJtPyRJvUG92ynaKaNgPDII
pG2llUuleXJw8LvksDeuvGboxdP0Z1jUULc6qQn1a9Lv/ahPAwZgCBKiGY8m+wktVeQjNeuL95vy
4pwZXRJVroQBhCakU7qfspXIJLGNXshTpBqdUHNGjyYbK34xqJBzjdbtuTCmNZ/LzfvuC7UlwFRl
1EM45zPOpMJhDtPXEUTulzbfMUbvvA8abWIa0yfOTS55lb9c0ZPKOd9zk4IXit8b0p9ViMDqTjh7
fEN9SiQD6y9sxoSAMczhsK6AlIKmiigqg1hQn/9chbYTdvOICRkom/TQ3sbDyt+056+xOO4/VATL
/+vRGA+A/PsAvqx19sPD09/1axbhjuwJ3ZvCbRK2qKL6QuvaxyEqfcmiNZZ5KzDBGwN4+W6y0iz4
Ip3jqJ4qt8lGh5N1arwhJLIx7c9Bpb7y+iAUJj+dzhOtmG9ym6CCDmjPpCOEqEX7N0aaeCzG2ikx
nEw/KVtnNmLgBZZR17ZkIP/JxWp3gbzr4ZpvzymN0szhJ35S3kJmwWfMNS+7m/cLpixuK7we56lx
VwyggJaJ1SPhOeSMPNxGycE4G6H0ex2RSU9JLs1ojq0TzKsRbbVx3DlypyiVW+6PPHbG1htsUBry
gjYdw9L9erLUr4JUsvAcpT0uhcm9BsUpqQcr7u8llXdmvRPbkqjAYwP6HO0xRUmoK5+s4HDrOi/k
sMrGyctJiPXQgtA/VX1Siz2mXY2ipBJB/1ALgpndFnhXSH8SVmZFo9n2TpnAsl6DCcovTporYpfX
Zm+fFozUapdWGfkUQ9lwWJYfpLxMwy7vNN9JN4XIEBpJAWriVuDNevNHMTobtQFOzA3ABouPCJqd
5Vz95C9SRSGF7QJiQPdUB66tLt2M07NmMUWNFXz1JtX9IqBOwshBCvdra+IZV+hIWdOyyAf4uQkM
1nBIKZ+Ms5qAv5CeD7F1Sj7fSMMGXJ27nqH5Pts4QChyQrepzYPFxDzWP7GBWBOp0Jgvf0fai4Xq
KQ8D8bluBP6Ukri7kMHN8cvuzHoe6SgIWl8pODBGdWs1Pf80rhXFesi5+vc5qP1SLjDIuWmuwU1Z
6eAOgmxzTaHnBWv6jrhUJSe8ZkaGkW6kxHM5vtpK5qcuUi3gf3rksvFtkRrv7YrqQOkCXZChkjWg
7Ck+wvFhUzGQyRpitbJzNOLNynvEPM1uWBQS1W5V7kNpgECgTxoDkftb/629hElQat9H4Q8FjrBi
4HePMt7MT22++IwNPqmYyQ9lQGK25ZwI/u7dh23D4GIhpJl5szCgXt2we8Dea6/ukIWU+VuzTji4
xnLQvdRJ+uvobUBumrcIoC+pDiZrlPbWUzgLQy/tlhOWD5NXo6jSBRfLYKWyqlv37VobFHFRouXz
nipMRitdeTPUj6BkFFOw/UYkzCL2Tl/Mk9u4hPEuygj8h17a1zNRyXIBa3+QcmLAWHLAuGQ2hGV8
leLlrs/UjBy9gVpGfvMKw9dlVSQdtltlIb93skbaU3RlcS8CxvQKZ1Yige+/Kty0p7b5cyoEODOo
RGc/NHnmBg9I3ux+0y+grpVrQgwV1C7wZtvyuIWbnbWOIBv1ibyVi2Tyq3AvEC+FCX0BE584LkYd
mKdXQxcioNj4rw9cZV5oag9/OJe5byMNI+7vMoQdR2Q1GOuA+e6HALZZ/hFnJFpXzOueq0Qw1iue
P47BhIZrCzlgbVOJ4Sv3Q2oUSLcb513785+g+6L1OfzhQ9yq9d680DaOPPFjKNYxXDX3LI7rQNuh
cc5RmE/zuPyatt57+NSA9lRmO7iJGACzIj+q1x2V4cEB9dQA6KNOO3Ujbb0EiUAO8njzDaaucyY1
wmju/BovddQbVAoTIpLUw1Z5yyyG9nhZ1Ec6/FJWWcVp0WtFIbUypgRpXNnTz/k0xr+MQaar+4FT
RXvO0IwmvHw/frMyEf0aaTZk2BJ/j3P5AV3DkgxwO4IDDx0VcgdlVVcxe+/WsB3XnRNM7JeP5Mqb
Ban5IXl/rp3A4U1TzBYmlTrJ7Rv6Cuz0EqZRdch2YdzqXhBFs4GpzrQRytPWF7dtBQwfbL9dF8yl
iAn2xRQsXjlqy4Hhuatp1I1yZzvGgubMqxOq6icI2+SPWukk2pIwo3q6dVqd0w8MtfCAFJ9k3nzg
AAKNzPQ33wF7KFAVXQXD2SUAdm9m2BvyGja97QMeLKSIsEOsfF2LmER0eElLtcWRnQUHWiUzT/jx
jK0NUz3h3/451JQtkHlXrButgFRusAnm/x38Z57vQNIRTNikqXeyluPiC4hGhqQ6wGxKWSiHY9Oz
nDLTkNKkDFGA1G648lcKKnHldokcVfaoRjb0zsmCTZvpvxrGFJvAZws5lTcJxBPCBrhMuqmP6xjq
u9uw3KOpWO/ZJlonsIqs/gK83p9Blvfsld6hKg2U441Jqa2H2ngyoLlS4gmZ+rDBDftSeMNdilcV
8zC6jGlDtTHvYc3CdSRTLFVqne/ffQTT4wOH/yJ/vMi9bRvTPYFlod9e3p++ZqvlNGYWKtI2sY5Q
sxynrTk/OEdj04q4s6+rLxfVCcMI656YOOcG5Dq+IVvry9U6g7cTiNlDuNvjhQdwo+LYulGTn0HO
1mWM4iXiJ757U2MJlmwc8oDGhbPRAQRaaC8oMeNHQeN1usJmw6fnY+BRGEz5j5SGcOU8VLaoks0D
DUGR6uRe5oD6AlsHW4Jl0MRdI9ZMAkrv3Aa7vvTnizJxROIT2EdRD4Isot9QlgTbTO3+EG1CCdqI
4iprBg7aPRd5+flEpYF+5HYcovXGNZtMu7SAyD+MhoTvtKGKj4X96+Kz2h4Tm0wQCP9i7KfRozvH
oFK0DGHKTBZuIHlRaYP0ZE1UgNsMV9dPAKfG6do3EWdWf+x/83qkNB0hfInyRHJQkun7fyoUIVtm
dCwrsCKUz49WgeAleYtLTxLIxbmi23oAFHRTRXdmBKBw7pkTpykLXZM44SX6iBbwQRpJYvYUdo4r
u3e84Ub4s3bqWNEUhYcQdKfY9j0lXWDFCzJldy06NLBM02vSlSg6pyZfBsGW3Lyx19HHy5AImiPz
PQMdDjjJu/QXaD7ytxvGS0+TfoPTTEX/+Iof0zvjHnH3bF4SYz6PExUxBjPU/P7EEDVki8XRE0+3
cGYoPgdrgegyBhQQ7EkMf+1fGXRIw49NXY72fEFIc6gis06WenuE8IkRwEa1Jb1cm+vIlppmK5Cx
wo/rS0rmpBKJGOXu/gMVxuvOzgn6rlNDNBC+xFtQy8iglclQAG8iukCBDmRDSGUFUmnVlHXMMv/t
XkqsJIH2WZivI/SqUqnYbYVrNyJooEabPRRrbXDUohZzKkjNNa3944NvFuGBqvDM5hZ4RLLYiqqO
bns/t6mjU6jB32po7Hr1xKRDOzT64xf/qSOCz/lUbDCNmCkD+7W3l+WDd41/jbDQDhSuG8CC6zp+
lJ0af6zfZY4+uJ2n/Xi6Tw754GORAaJ3H5dyxGrxbuQR7Af+JuKddJPiOEqD3lijrBAHI2w3
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  port (
    gtxe2_i : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_0 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_7 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_8 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_cpllreset_t : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  signal cpll_pd0_i : STD_LOGIC;
  signal cpllreset_in : STD_LOGIC;
begin
cpll_railing0_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing
     port map (
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gtrefclk_bufg => gtrefclk_bufg
    );
gt0_GTWIZARD_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => SR(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i_0 => gtxe2_i,
      gtxe2_i_1 => gtxe2_i_0,
      gtxe2_i_10(1 downto 0) => gtxe2_i_9(1 downto 0),
      gtxe2_i_11(1 downto 0) => gtxe2_i_10(1 downto 0),
      gtxe2_i_2 => gtxe2_i_1,
      gtxe2_i_3(15 downto 0) => gtxe2_i_2(15 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_4(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_8 => gtxe2_i_7,
      gtxe2_i_9(1 downto 0) => gtxe2_i_8(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  port (
    data_in : out STD_LOGIC;
    gt0_rxuserrdy_t : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg6 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  signal \FSM_sequential_rx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_9_n_0\ : STD_LOGIC;
  signal GTRXRESET : STD_LOGIC;
  signal RXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_reg_n_0 : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out_0 : STD_LOGIC;
  signal \^gt0_rxuserrdy_t\ : STD_LOGIC;
  signal gtrxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3__0_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \init_wait_done_i_1__0_n_0\ : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3__0_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal \mmcm_lock_reclocked_i_2__0_n_0\ : STD_LOGIC;
  signal \p_0_in__2\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__3\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal reset_time_out_i_3_n_0 : STD_LOGIC;
  signal reset_time_out_i_4_n_0 : STD_LOGIC;
  signal reset_time_out_reg_n_0 : STD_LOGIC;
  signal \run_phase_alignment_int_i_1__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3_reg_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_5_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_6_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal rx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \rx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rxresetdone_s2 : STD_LOGIC;
  signal rxresetdone_s3 : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_data_valid_n_0 : STD_LOGIC;
  signal sync_data_valid_n_1 : STD_LOGIC;
  signal sync_data_valid_n_5 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_100us_i_1_n_0 : STD_LOGIC;
  signal time_out_100us_i_2_n_0 : STD_LOGIC;
  signal time_out_100us_i_3_n_0 : STD_LOGIC;
  signal time_out_100us_reg_n_0 : STD_LOGIC;
  signal time_out_1us_i_1_n_0 : STD_LOGIC;
  signal time_out_1us_i_2_n_0 : STD_LOGIC;
  signal time_out_1us_i_3_n_0 : STD_LOGIC;
  signal time_out_1us_reg_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal time_out_2ms_i_2_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_3__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_4_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal \time_out_wait_bypass_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max : STD_LOGIC;
  signal time_tlock_max1 : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_1\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_2\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_3\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_n_3\ : STD_LOGIC;
  signal time_tlock_max1_carry_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_5_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_6_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_7_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_8_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_1 : STD_LOGIC;
  signal time_tlock_max1_carry_n_2 : STD_LOGIC;
  signal time_tlock_max1_carry_n_3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_time_cnt0__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \wait_time_cnt[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_time_tlock_max1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[2]_i_2\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_10\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_9\ : label is "soft_lutpair43";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[0]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[1]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[2]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[3]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of check_tlock_max_i_1 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of reset_time_out_i_3 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of reset_time_out_i_4 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_5 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_6 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of time_out_1us_i_2 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of time_out_2ms_i_1 : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \time_out_2ms_i_3__0\ : label is "soft_lutpair51";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of time_tlock_max1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_1 : label is "soft_lutpair52";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1__0\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1__0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1__0\ : label is "soft_lutpair45";
begin
  data_in <= \^data_in\;
  gt0_rxuserrdy_t <= \^gt0_rxuserrdy_t\;
\FSM_sequential_rx_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222AAAA00000C00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(3),
      I3 => time_tlock_max,
      I4 => reset_time_out_reg_n_0,
      I5 => rx_state(1),
      O => \FSM_sequential_rx_state[0]_i_2_n_0\
    );
\FSM_sequential_rx_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AABF000F0000"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      I2 => rx_state(2),
      I3 => rx_state(3),
      I4 => rx_state(1),
      I5 => rx_state(0),
      O => \FSM_sequential_rx_state[1]_i_3_n_0\
    );
\FSM_sequential_rx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050FF2200"
    )
        port map (
      I0 => rx_state(1),
      I1 => time_out_2ms_reg_n_0,
      I2 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I3 => rx_state(0),
      I4 => rx_state(2),
      I5 => rx_state(3),
      O => \rx_state__0\(2)
    );
\FSM_sequential_rx_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      O => \FSM_sequential_rx_state[2]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_out_2ms_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_10_n_0\
    );
\FSM_sequential_rx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050005300"
    )
        port map (
      I0 => \FSM_sequential_rx_state[3]_i_10_n_0\,
      I1 => \wait_time_cnt[6]_i_4__0_n_0\,
      I2 => rx_state(0),
      I3 => rx_state(1),
      I4 => wait_time_cnt_reg(6),
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_3_n_0\
    );
\FSM_sequential_rx_state[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000023002F00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(1),
      I3 => rx_state(0),
      I4 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_7_n_0\
    );
\FSM_sequential_rx_state[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80800080"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => time_out_2ms_reg_n_0,
      I4 => reset_time_out_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_9_n_0\
    );
\FSM_sequential_rx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(0),
      Q => rx_state(0),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(1),
      Q => rx_state(1),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(2),
      Q => rx_state(2),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(3),
      Q => rx_state(3),
      R => \out\(0)
    );
RXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB4000"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(2),
      I3 => rx_state(1),
      I4 => \^gt0_rxuserrdy_t\,
      O => RXUSERRDY_i_1_n_0
    );
RXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => RXUSERRDY_i_1_n_0,
      Q => \^gt0_rxuserrdy_t\,
      R => \out\(0)
    );
check_tlock_max_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => rx_state(3),
      I4 => check_tlock_max_reg_n_0,
      O => check_tlock_max_i_1_n_0
    );
check_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => check_tlock_max_i_1_n_0,
      Q => check_tlock_max_reg_n_0,
      R => \out\(0)
    );
gtrxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => rx_state(0),
      I4 => GTRXRESET,
      O => gtrxreset_i_i_1_n_0
    );
gtrxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gtrxreset_i_i_1_n_0,
      Q => GTRXRESET,
      R => \out\(0)
    );
gtxe2_i_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTRXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => SR(0)
    );
\init_wait_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1__0_n_0\
    );
\init_wait_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__2\(1)
    );
\init_wait_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__2\(2)
    );
\init_wait_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__2\(3)
    );
\init_wait_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__2\(4)
    );
\init_wait_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__2\(5)
    );
\init_wait_count[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1__0_n_0\
    );
\init_wait_count[6]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__2\(6)
    );
\init_wait_count[6]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3__0_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1__0_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(6),
      Q => init_wait_count_reg(6)
    );
\init_wait_done_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => \init_wait_done_i_1__0_n_0\
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => \init_wait_done_i_1__0_n_0\,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__3\(0)
    );
\mmcm_lock_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__3\(1)
    );
\mmcm_lock_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1__0_n_0\
    );
\mmcm_lock_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1__0_n_0\
    );
\mmcm_lock_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1__0_n_0\
    );
\mmcm_lock_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1__0_n_0\
    );
\mmcm_lock_count[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1__0_n_0\
    );
\mmcm_lock_count[7]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2__0_n_0\
    );
\mmcm_lock_count[7]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3__0_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[2]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[3]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[4]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[5]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[6]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[7]_i_3__0_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
\mmcm_lock_reclocked_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_reclocked_i_2__0_n_0\
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
reset_time_out_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      O => reset_time_out_i_3_n_0
    );
reset_time_out_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"34347674"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      I2 => rx_state(0),
      I3 => \FSM_sequential_rx_state_reg[0]_0\,
      I4 => rx_state(1),
      O => reset_time_out_i_4_n_0
    );
reset_time_out_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_0,
      Q => reset_time_out_reg_n_0,
      S => \out\(0)
    );
\run_phase_alignment_int_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFF0010"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(1),
      I2 => rx_state(3),
      I3 => rx_state(0),
      I4 => run_phase_alignment_int_reg_n_0,
      O => \run_phase_alignment_int_i_1__0_n_0\
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => \run_phase_alignment_int_i_1__0_n_0\,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => data_out_0,
      Q => run_phase_alignment_int_s3_reg_n_0,
      R => '0'
    );
rx_fsm_reset_done_int_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => rx_state(1),
      I1 => rx_state(0),
      O => rx_fsm_reset_done_int_i_5_n_0
    );
rx_fsm_reset_done_int_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(2),
      O => rx_fsm_reset_done_int_i_6_n_0
    );
rx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_1,
      Q => \^data_in\,
      R => \out\(0)
    );
rx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => rx_fsm_reset_done_int_s2,
      Q => rx_fsm_reset_done_int_s3,
      R => '0'
    );
rxresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => rxresetdone_s2,
      Q => rxresetdone_s3,
      R => '0'
    );
sync_RXRESETDONE: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10
     port map (
      data_out => rxresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11
     port map (
      \FSM_sequential_rx_state_reg[1]\ => sync_cplllock_n_0,
      Q(2 downto 0) => rx_state(3 downto 1),
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg,
      rxresetdone_s3 => rxresetdone_s3
    );
sync_data_valid: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12
     port map (
      D(2) => \rx_state__0\(3),
      D(1 downto 0) => \rx_state__0\(1 downto 0),
      E(0) => sync_data_valid_n_5,
      \FSM_sequential_rx_state_reg[0]\ => \FSM_sequential_rx_state[3]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[0]_0\ => \FSM_sequential_rx_state[3]_i_7_n_0\,
      \FSM_sequential_rx_state_reg[0]_1\ => \FSM_sequential_rx_state_reg[0]_0\,
      \FSM_sequential_rx_state_reg[0]_2\ => \FSM_sequential_rx_state[0]_i_2_n_0\,
      \FSM_sequential_rx_state_reg[0]_3\ => init_wait_done_reg_n_0,
      \FSM_sequential_rx_state_reg[1]\ => sync_data_valid_n_0,
      \FSM_sequential_rx_state_reg[1]_0\ => \FSM_sequential_rx_state[1]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[3]\ => \FSM_sequential_rx_state[3]_i_9_n_0\,
      Q(3 downto 0) => rx_state(3 downto 0),
      data_in => \^data_in\,
      data_out => data_out,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => reset_time_out_i_3_n_0,
      reset_time_out_reg_1 => reset_time_out_i_4_n_0,
      reset_time_out_reg_2 => reset_time_out_reg_n_0,
      rx_fsm_reset_done_int_reg => sync_data_valid_n_1,
      rx_fsm_reset_done_int_reg_0 => rx_fsm_reset_done_int_i_5_n_0,
      rx_fsm_reset_done_int_reg_1 => time_out_100us_reg_n_0,
      rx_fsm_reset_done_int_reg_2 => time_out_1us_reg_n_0,
      rx_fsm_reset_done_int_reg_3 => rx_fsm_reset_done_int_i_6_n_0,
      time_out_wait_bypass_s3 => time_out_wait_bypass_s3
    );
sync_mmcm_lock_reclocked: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out_0,
      data_sync_reg1_0 => data_sync_reg6
    );
sync_time_out_wait_bypass: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16
     port map (
      data_in => \^data_in\,
      data_out => rx_fsm_reset_done_int_s2,
      data_sync_reg6_0 => data_sync_reg6
    );
time_out_100us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => time_out_2ms_i_4_n_0,
      I1 => time_out_counter_reg(17),
      I2 => time_out_counter_reg(16),
      I3 => time_out_100us_i_2_n_0,
      I4 => time_out_100us_i_3_n_0,
      I5 => time_out_100us_reg_n_0,
      O => time_out_100us_i_1_n_0
    );
time_out_100us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(14),
      O => time_out_100us_i_2_n_0
    );
time_out_100us_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(0),
      I2 => time_out_counter_reg(1),
      I3 => time_out_counter_reg(15),
      I4 => time_out_counter_reg(13),
      O => time_out_100us_i_3_n_0
    );
time_out_100us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_100us_i_1_n_0,
      Q => time_out_100us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_1us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00100000"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => time_out_1us_i_2_n_0,
      I2 => time_out_counter_reg(3),
      I3 => time_out_counter_reg(2),
      I4 => time_out_1us_i_3_n_0,
      I5 => time_out_1us_reg_n_0,
      O => time_out_1us_i_1_n_0
    );
time_out_1us_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => time_out_1us_i_2_n_0
    );
time_out_1us_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(11),
      I2 => time_out_counter_reg(6),
      I3 => time_out_counter_reg(8),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(12),
      O => time_out_1us_i_3_n_0
    );
time_out_1us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_1us_i_1_n_0,
      Q => time_out_1us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF01"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => \time_out_2ms_i_3__0_n_0\,
      I2 => time_out_2ms_i_4_n_0,
      I3 => time_out_2ms_reg_n_0,
      O => time_out_2ms_i_1_n_0
    );
time_out_2ms_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(14),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_100us_i_3_n_0,
      O => time_out_2ms_i_2_n_0
    );
\time_out_2ms_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(18),
      I3 => time_out_counter_reg(17),
      O => \time_out_2ms_i_3__0_n_0\
    );
time_out_2ms_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFFFFFF"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(9),
      I4 => time_out_counter_reg(11),
      I5 => time_out_counter_reg(6),
      O => time_out_2ms_i_4_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => reset_time_out_reg_n_0
    );
\time_out_counter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(16),
      I3 => time_out_counter_reg(12),
      I4 => time_out_2ms_i_2_n_0,
      I5 => time_out_2ms_i_4_n_0,
      O => time_out_counter
    );
\time_out_counter[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2__0_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2__0_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2__0_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2__0_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2__0_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1__0_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out_reg_n_0
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => rx_fsm_reset_done_int_s3,
      I2 => \time_out_wait_bypass_i_2__0_n_0\,
      I3 => run_phase_alignment_int_s3_reg_n_0,
      O => time_out_wait_bypass_i_1_n_0
    );
\time_out_wait_bypass_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \time_out_wait_bypass_i_3__0_n_0\,
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(11),
      I3 => wait_bypass_count_reg(0),
      I4 => \time_out_wait_bypass_i_4__0_n_0\,
      O => \time_out_wait_bypass_i_2__0_n_0\
    );
\time_out_wait_bypass_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      I1 => wait_bypass_count_reg(4),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(2),
      O => \time_out_wait_bypass_i_3__0_n_0\
    );
\time_out_wait_bypass_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      I1 => wait_bypass_count_reg(7),
      I2 => wait_bypass_count_reg(3),
      I3 => wait_bypass_count_reg(6),
      I4 => wait_bypass_count_reg(10),
      I5 => wait_bypass_count_reg(8),
      O => \time_out_wait_bypass_i_4__0_n_0\
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => time_tlock_max1_carry_n_0,
      CO(2) => time_tlock_max1_carry_n_1,
      CO(1) => time_tlock_max1_carry_n_2,
      CO(0) => time_tlock_max1_carry_n_3,
      CYINIT => '0',
      DI(3) => time_tlock_max1_carry_i_1_n_0,
      DI(2) => time_tlock_max1_carry_i_2_n_0,
      DI(1) => time_tlock_max1_carry_i_3_n_0,
      DI(0) => time_tlock_max1_carry_i_4_n_0,
      O(3 downto 0) => NLW_time_tlock_max1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => time_tlock_max1_carry_i_5_n_0,
      S(2) => time_tlock_max1_carry_i_6_n_0,
      S(1) => time_tlock_max1_carry_i_7_n_0,
      S(0) => time_tlock_max1_carry_i_8_n_0
    );
\time_tlock_max1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => time_tlock_max1_carry_n_0,
      CO(3) => \time_tlock_max1_carry__0_n_0\,
      CO(2) => \time_tlock_max1_carry__0_n_1\,
      CO(1) => \time_tlock_max1_carry__0_n_2\,
      CO(0) => \time_tlock_max1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => time_out_counter_reg(15),
      DI(2) => \time_tlock_max1_carry__0_i_1_n_0\,
      DI(1) => '0',
      DI(0) => \time_tlock_max1_carry__0_i_2_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \time_tlock_max1_carry__0_i_3_n_0\,
      S(2) => \time_tlock_max1_carry__0_i_4_n_0\,
      S(1) => \time_tlock_max1_carry__0_i_5_n_0\,
      S(0) => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(13),
      O => \time_tlock_max1_carry__0_i_1_n_0\
    );
\time_tlock_max1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(8),
      I1 => time_out_counter_reg(9),
      O => \time_tlock_max1_carry__0_i_2_n_0\
    );
\time_tlock_max1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      O => \time_tlock_max1_carry__0_i_3_n_0\
    );
\time_tlock_max1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(13),
      I1 => time_out_counter_reg(12),
      O => \time_tlock_max1_carry__0_i_4_n_0\
    );
\time_tlock_max1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(11),
      O => \time_tlock_max1_carry__0_i_5_n_0\
    );
\time_tlock_max1_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(8),
      O => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_tlock_max1_carry__0_n_0\,
      CO(3 downto 2) => \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => time_tlock_max1,
      CO(0) => \time_tlock_max1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => time_out_counter_reg(18),
      DI(0) => \time_tlock_max1_carry__1_i_1_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \time_tlock_max1_carry__1_i_2_n_0\,
      S(0) => \time_tlock_max1_carry__1_i_3_n_0\
    );
\time_tlock_max1_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => \time_tlock_max1_carry__1_i_1_n_0\
    );
\time_tlock_max1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(18),
      O => \time_tlock_max1_carry__1_i_2_n_0\
    );
\time_tlock_max1_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(16),
      O => \time_tlock_max1_carry__1_i_3_n_0\
    );
time_tlock_max1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(7),
      O => time_tlock_max1_carry_i_1_n_0
    );
time_tlock_max1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(5),
      O => time_tlock_max1_carry_i_2_n_0
    );
time_tlock_max1_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      O => time_tlock_max1_carry_i_3_n_0
    );
time_tlock_max1_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(1),
      O => time_tlock_max1_carry_i_4_n_0
    );
time_tlock_max1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(6),
      O => time_tlock_max1_carry_i_5_n_0
    );
time_tlock_max1_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(5),
      I1 => time_out_counter_reg(4),
      O => time_tlock_max1_carry_i_6_n_0
    );
time_tlock_max1_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(3),
      I1 => time_out_counter_reg(2),
      O => time_tlock_max1_carry_i_7_n_0
    );
time_tlock_max1_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(1),
      I1 => time_out_counter_reg(0),
      O => time_tlock_max1_carry_i_8_n_0
    );
time_tlock_max_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => check_tlock_max_reg_n_0,
      I1 => time_tlock_max1,
      I2 => time_tlock_max,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max,
      R => reset_time_out_reg_n_0
    );
\wait_bypass_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3_reg_n_0,
      O => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count[0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \time_out_wait_bypass_i_2__0_n_0\,
      I1 => rx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2__0_n_0\
    );
\wait_bypass_count[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      Q => wait_bypass_count_reg(0),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[0]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3__0_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3__0_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(10),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(11),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(12),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(12)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      Q => wait_bypass_count_reg(1),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      Q => wait_bypass_count_reg(2),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      Q => wait_bypass_count_reg(3),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(4),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(5),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(6),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(7),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(8),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(9),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_time_cnt[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => \wait_time_cnt0__0\(0)
    );
\wait_time_cnt[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1__0_n_0\
    );
\wait_time_cnt[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1__0_n_0\
    );
\wait_time_cnt[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1__0_n_0\
    );
\wait_time_cnt[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1__0_n_0\
    );
\wait_time_cnt[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1__0_n_0\
    );
\wait_time_cnt[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(3),
      O => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt[6]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_2__0_n_0\
    );
\wait_time_cnt[6]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3__0_n_0\
    );
\wait_time_cnt[6]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4__0_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt0__0\(0),
      Q => wait_time_cnt_reg(0),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[1]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(1),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[2]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(2),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[3]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(3),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[4]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(4),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[5]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(5),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[6]_i_3__0_n_0\,
      Q => wait_time_cnt_reg(6),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  port (
    mmcm_reset : out STD_LOGIC;
    gt0_cpllreset_t : out STD_LOGIC;
    data_in : out STD_LOGIC;
    gt0_txuserrdy_t : out STD_LOGIC;
    gt0_gttxreset_in0_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    gt0_cpllrefclklost_i : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC;
    data_sync_reg1_2 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  signal CPLL_RESET_i_1_n_0 : STD_LOGIC;
  signal CPLL_RESET_i_2_n_0 : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal GTTXRESET : STD_LOGIC;
  signal MMCM_RESET_i_1_n_0 : STD_LOGIC;
  signal TXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out : STD_LOGIC;
  signal \^gt0_cpllreset_t\ : STD_LOGIC;
  signal \^gt0_txuserrdy_t\ : STD_LOGIC;
  signal gttxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal init_wait_done_i_1_n_0 : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal mmcm_lock_reclocked_i_2_n_0 : STD_LOGIC;
  signal \^mmcm_reset\ : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pll_reset_asserted_i_1_n_0 : STD_LOGIC;
  signal pll_reset_asserted_i_2_n_0 : STD_LOGIC;
  signal pll_reset_asserted_reg_n_0 : STD_LOGIC;
  signal refclk_stable_count : STD_LOGIC;
  signal \refclk_stable_count[0]_i_3_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_7_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_8_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_9_n_0\ : STD_LOGIC;
  signal refclk_stable_count_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \refclk_stable_count_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal refclk_stable_i_1_n_0 : STD_LOGIC;
  signal refclk_stable_i_2_n_0 : STD_LOGIC;
  signal refclk_stable_i_3_n_0 : STD_LOGIC;
  signal refclk_stable_i_4_n_0 : STD_LOGIC;
  signal refclk_stable_i_5_n_0 : STD_LOGIC;
  signal refclk_stable_i_6_n_0 : STD_LOGIC;
  signal refclk_stable_reg_n_0 : STD_LOGIC;
  signal reset_time_out : STD_LOGIC;
  signal \reset_time_out_i_2__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_i_1_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3 : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_cplllock_n_1 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_2__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_3_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_5_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_500us_i_1_n_0 : STD_LOGIC;
  signal time_out_500us_i_2_n_0 : STD_LOGIC;
  signal time_out_500us_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_2_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_3_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_4_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_5_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max_reg_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_i_1_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal tx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal txresetdone_s2 : STD_LOGIC;
  signal txresetdone_s3 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal wait_time_cnt0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal wait_time_cnt0_0 : STD_LOGIC;
  signal \wait_time_cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of CPLL_RESET_i_2 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_3\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[1]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_7\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_8\ : label is "soft_lutpair59";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[0]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[1]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[2]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[3]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of MMCM_RESET_i_1 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of TXUSERRDY_i_1 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of gttxreset_i_i_1 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of pll_reset_asserted_i_2 : label is "soft_lutpair60";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[20]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[24]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[28]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of run_phase_alignment_int_i_1 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \time_out_2ms_i_4__0\ : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_4 : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1\ : label is "soft_lutpair63";
begin
  data_in <= \^data_in\;
  gt0_cpllreset_t <= \^gt0_cpllreset_t\;
  gt0_txuserrdy_t <= \^gt0_txuserrdy_t\;
  mmcm_reset <= \^mmcm_reset\;
CPLL_RESET_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF1F0000001F"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => gt0_cpllrefclklost_i,
      I2 => refclk_stable_reg_n_0,
      I3 => CPLL_RESET_i_2_n_0,
      I4 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I5 => \^gt0_cpllreset_t\,
      O => CPLL_RESET_i_1_n_0
    );
CPLL_RESET_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      O => CPLL_RESET_i_2_n_0
    );
CPLL_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => CPLL_RESET_i_1_n_0,
      Q => \^gt0_cpllreset_t\,
      R => \out\(0)
    );
\FSM_sequential_tx_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F3FFF3F0F5F0F5F0"
    )
        port map (
      I0 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I1 => \FSM_sequential_tx_state[0]_i_2_n_0\,
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => tx_state(2),
      I4 => time_out_2ms_reg_n_0,
      I5 => tx_state(1),
      O => \tx_state__0\(0)
    );
\FSM_sequential_tx_state[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out,
      I1 => time_out_500us_reg_n_0,
      O => \FSM_sequential_tx_state[0]_i_2_n_0\
    );
\FSM_sequential_tx_state[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      O => \FSM_sequential_tx_state[0]_i_3_n_0\
    );
\FSM_sequential_tx_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"005A001A"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      O => \tx_state__0\(1)
    );
\FSM_sequential_tx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04000C0C06020C0C"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I4 => tx_state(0),
      I5 => time_out_2ms_reg_n_0,
      O => \tx_state__0\(2)
    );
\FSM_sequential_tx_state[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => reset_time_out,
      I2 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[2]_i_2_n_0\
    );
\FSM_sequential_tx_state[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4FF4444"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => tx_state(3),
      I2 => reset_time_out,
      I3 => time_out_500us_reg_n_0,
      I4 => \FSM_sequential_tx_state[3]_i_8_n_0\,
      O => \tx_state__0\(3)
    );
\FSM_sequential_tx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00BA000000000000"
    )
        port map (
      I0 => txresetdone_s3,
      I1 => reset_time_out,
      I2 => time_out_500us_reg_n_0,
      I3 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I4 => tx_state(2),
      I5 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_3_n_0\
    );
\FSM_sequential_tx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000300FF00AA"
    )
        port map (
      I0 => init_wait_done_reg_n_0,
      I1 => \wait_time_cnt[6]_i_4_n_0\,
      I2 => wait_time_cnt_reg(6),
      I3 => tx_state(0),
      I4 => tx_state(3),
      I5 => CPLL_RESET_i_2_n_0,
      O => \FSM_sequential_tx_state[3]_i_4_n_0\
    );
\FSM_sequential_tx_state[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040400040000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => reset_time_out,
      I4 => time_tlock_max_reg_n_0,
      I5 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[3]_i_6_n_0\
    );
\FSM_sequential_tx_state[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(3),
      I2 => tx_state(0),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_7_n_0\
    );
\FSM_sequential_tx_state[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_8_n_0\
    );
\FSM_sequential_tx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(0),
      Q => tx_state(0),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(1),
      Q => tx_state(1),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(2),
      Q => tx_state(2),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(3),
      Q => tx_state(3),
      R => \out\(0)
    );
MMCM_RESET_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70004"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(0),
      I2 => tx_state(3),
      I3 => tx_state(1),
      I4 => \^mmcm_reset\,
      O => MMCM_RESET_i_1_n_0
    );
MMCM_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => MMCM_RESET_i_1_n_0,
      Q => \^mmcm_reset\,
      R => \out\(0)
    );
TXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFD2000"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => \^gt0_txuserrdy_t\,
      O => TXUSERRDY_i_1_n_0
    );
TXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => TXUSERRDY_i_1_n_0,
      Q => \^gt0_txuserrdy_t\,
      R => \out\(0)
    );
gttxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(1),
      I2 => tx_state(2),
      I3 => tx_state(0),
      I4 => GTTXRESET,
      O => gttxreset_i_i_1_n_0
    );
gttxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gttxreset_i_i_1_n_0,
      Q => GTTXRESET,
      R => \out\(0)
    );
gtxe2_i_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTTXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => gt0_gttxreset_in0_out
    );
\init_wait_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1_n_0\
    );
\init_wait_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__0\(1)
    );
\init_wait_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__0\(2)
    );
\init_wait_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__0\(3)
    );
\init_wait_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__0\(4)
    );
\init_wait_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__0\(5)
    );
\init_wait_count[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1_n_0\
    );
\init_wait_count[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__0\(6)
    );
\init_wait_count[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(6),
      Q => init_wait_count_reg(6)
    );
init_wait_done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => init_wait_done_i_1_n_0
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => init_wait_done_i_1_n_0,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__1\(0)
    );
\mmcm_lock_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__1\(1)
    );
\mmcm_lock_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1_n_0\
    );
\mmcm_lock_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1_n_0\
    );
\mmcm_lock_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1_n_0\
    );
\mmcm_lock_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1_n_0\
    );
\mmcm_lock_count[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1_n_0\
    );
\mmcm_lock_count[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2_n_0\
    );
\mmcm_lock_count[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => mmcm_lock_reclocked_i_2_n_0,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[2]_i_1_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[3]_i_1_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[4]_i_1_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[5]_i_1_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[6]_i_1_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[7]_i_3_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => mmcm_lock_reclocked_i_2_n_0,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
mmcm_lock_reclocked_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => mmcm_lock_reclocked_i_2_n_0
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
pll_reset_asserted_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000CD55CCCCCCCC"
    )
        port map (
      I0 => tx_state(3),
      I1 => pll_reset_asserted_reg_n_0,
      I2 => gt0_cpllrefclklost_i,
      I3 => refclk_stable_reg_n_0,
      I4 => tx_state(1),
      I5 => pll_reset_asserted_i_2_n_0,
      O => pll_reset_asserted_i_1_n_0
    );
pll_reset_asserted_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      O => pll_reset_asserted_i_2_n_0
    );
pll_reset_asserted_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pll_reset_asserted_i_1_n_0,
      Q => pll_reset_asserted_reg_n_0,
      R => \out\(0)
    );
\refclk_stable_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_3_n_0\,
      I1 => \refclk_stable_count[0]_i_4_n_0\,
      I2 => \refclk_stable_count[0]_i_5_n_0\,
      I3 => \refclk_stable_count[0]_i_6_n_0\,
      I4 => \refclk_stable_count[0]_i_7_n_0\,
      I5 => \refclk_stable_count[0]_i_8_n_0\,
      O => refclk_stable_count
    );
\refclk_stable_count[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => refclk_stable_count_reg(13),
      I1 => refclk_stable_count_reg(12),
      I2 => refclk_stable_count_reg(10),
      I3 => refclk_stable_count_reg(11),
      I4 => refclk_stable_count_reg(9),
      I5 => refclk_stable_count_reg(8),
      O => \refclk_stable_count[0]_i_3_n_0\
    );
\refclk_stable_count[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFDF"
    )
        port map (
      I0 => refclk_stable_count_reg(19),
      I1 => refclk_stable_count_reg(18),
      I2 => refclk_stable_count_reg(16),
      I3 => refclk_stable_count_reg(17),
      I4 => refclk_stable_count_reg(15),
      I5 => refclk_stable_count_reg(14),
      O => \refclk_stable_count[0]_i_4_n_0\
    );
\refclk_stable_count[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(30),
      I1 => refclk_stable_count_reg(31),
      I2 => refclk_stable_count_reg(28),
      I3 => refclk_stable_count_reg(29),
      I4 => refclk_stable_count_reg(27),
      I5 => refclk_stable_count_reg(26),
      O => \refclk_stable_count[0]_i_5_n_0\
    );
\refclk_stable_count[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(24),
      I1 => refclk_stable_count_reg(25),
      I2 => refclk_stable_count_reg(22),
      I3 => refclk_stable_count_reg(23),
      I4 => refclk_stable_count_reg(21),
      I5 => refclk_stable_count_reg(20),
      O => \refclk_stable_count[0]_i_6_n_0\
    );
\refclk_stable_count[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      I1 => refclk_stable_count_reg(1),
      O => \refclk_stable_count[0]_i_7_n_0\
    );
\refclk_stable_count[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF7"
    )
        port map (
      I0 => refclk_stable_count_reg(6),
      I1 => refclk_stable_count_reg(7),
      I2 => refclk_stable_count_reg(4),
      I3 => refclk_stable_count_reg(5),
      I4 => refclk_stable_count_reg(3),
      I5 => refclk_stable_count_reg(2),
      O => \refclk_stable_count[0]_i_8_n_0\
    );
\refclk_stable_count[0]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      O => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_7\,
      Q => refclk_stable_count_reg(0),
      R => '0'
    );
\refclk_stable_count_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(2) => \refclk_stable_count_reg[0]_i_2_n_1\,
      CO(1) => \refclk_stable_count_reg[0]_i_2_n_2\,
      CO(0) => \refclk_stable_count_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \refclk_stable_count_reg[0]_i_2_n_4\,
      O(2) => \refclk_stable_count_reg[0]_i_2_n_5\,
      O(1) => \refclk_stable_count_reg[0]_i_2_n_6\,
      O(0) => \refclk_stable_count_reg[0]_i_2_n_7\,
      S(3 downto 1) => refclk_stable_count_reg(3 downto 1),
      S(0) => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_5\,
      Q => refclk_stable_count_reg(10),
      R => '0'
    );
\refclk_stable_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_4\,
      Q => refclk_stable_count_reg(11),
      R => '0'
    );
\refclk_stable_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_7\,
      Q => refclk_stable_count_reg(12),
      R => '0'
    );
\refclk_stable_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[12]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[12]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[12]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[12]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[12]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(15 downto 12)
    );
\refclk_stable_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_6\,
      Q => refclk_stable_count_reg(13),
      R => '0'
    );
\refclk_stable_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_5\,
      Q => refclk_stable_count_reg(14),
      R => '0'
    );
\refclk_stable_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_4\,
      Q => refclk_stable_count_reg(15),
      R => '0'
    );
\refclk_stable_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_7\,
      Q => refclk_stable_count_reg(16),
      R => '0'
    );
\refclk_stable_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[16]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[16]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[16]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[16]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[16]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[16]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(19 downto 16)
    );
\refclk_stable_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_6\,
      Q => refclk_stable_count_reg(17),
      R => '0'
    );
\refclk_stable_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_5\,
      Q => refclk_stable_count_reg(18),
      R => '0'
    );
\refclk_stable_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_4\,
      Q => refclk_stable_count_reg(19),
      R => '0'
    );
\refclk_stable_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_6\,
      Q => refclk_stable_count_reg(1),
      R => '0'
    );
\refclk_stable_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_7\,
      Q => refclk_stable_count_reg(20),
      R => '0'
    );
\refclk_stable_count_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[20]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[20]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[20]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[20]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[20]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[20]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(23 downto 20)
    );
\refclk_stable_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_6\,
      Q => refclk_stable_count_reg(21),
      R => '0'
    );
\refclk_stable_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_5\,
      Q => refclk_stable_count_reg(22),
      R => '0'
    );
\refclk_stable_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_4\,
      Q => refclk_stable_count_reg(23),
      R => '0'
    );
\refclk_stable_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_7\,
      Q => refclk_stable_count_reg(24),
      R => '0'
    );
\refclk_stable_count_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[24]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[24]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[24]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[24]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[24]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[24]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(27 downto 24)
    );
\refclk_stable_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_6\,
      Q => refclk_stable_count_reg(25),
      R => '0'
    );
\refclk_stable_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_5\,
      Q => refclk_stable_count_reg(26),
      R => '0'
    );
\refclk_stable_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_4\,
      Q => refclk_stable_count_reg(27),
      R => '0'
    );
\refclk_stable_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_7\,
      Q => refclk_stable_count_reg(28),
      R => '0'
    );
\refclk_stable_count_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(3) => \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \refclk_stable_count_reg[28]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[28]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[28]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[28]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[28]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[28]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(31 downto 28)
    );
\refclk_stable_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_6\,
      Q => refclk_stable_count_reg(29),
      R => '0'
    );
\refclk_stable_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_5\,
      Q => refclk_stable_count_reg(2),
      R => '0'
    );
\refclk_stable_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_5\,
      Q => refclk_stable_count_reg(30),
      R => '0'
    );
\refclk_stable_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_4\,
      Q => refclk_stable_count_reg(31),
      R => '0'
    );
\refclk_stable_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_4\,
      Q => refclk_stable_count_reg(3),
      R => '0'
    );
\refclk_stable_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_7\,
      Q => refclk_stable_count_reg(4),
      R => '0'
    );
\refclk_stable_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(3) => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[4]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[4]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[4]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[4]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[4]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(7 downto 4)
    );
\refclk_stable_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_6\,
      Q => refclk_stable_count_reg(5),
      R => '0'
    );
\refclk_stable_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_5\,
      Q => refclk_stable_count_reg(6),
      R => '0'
    );
\refclk_stable_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_4\,
      Q => refclk_stable_count_reg(7),
      R => '0'
    );
\refclk_stable_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_7\,
      Q => refclk_stable_count_reg(8),
      R => '0'
    );
\refclk_stable_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[8]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[8]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[8]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[8]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[8]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(11 downto 8)
    );
\refclk_stable_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_6\,
      Q => refclk_stable_count_reg(9),
      R => '0'
    );
refclk_stable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_7_n_0\,
      I1 => refclk_stable_i_2_n_0,
      I2 => refclk_stable_i_3_n_0,
      I3 => refclk_stable_i_4_n_0,
      I4 => refclk_stable_i_5_n_0,
      I5 => refclk_stable_i_6_n_0,
      O => refclk_stable_i_1_n_0
    );
refclk_stable_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(4),
      I1 => refclk_stable_count_reg(5),
      I2 => refclk_stable_count_reg(2),
      I3 => refclk_stable_count_reg(3),
      I4 => refclk_stable_count_reg(7),
      I5 => refclk_stable_count_reg(6),
      O => refclk_stable_i_2_n_0
    );
refclk_stable_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(10),
      I1 => refclk_stable_count_reg(11),
      I2 => refclk_stable_count_reg(8),
      I3 => refclk_stable_count_reg(9),
      I4 => refclk_stable_count_reg(12),
      I5 => refclk_stable_count_reg(13),
      O => refclk_stable_i_3_n_0
    );
refclk_stable_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => refclk_stable_count_reg(16),
      I1 => refclk_stable_count_reg(17),
      I2 => refclk_stable_count_reg(14),
      I3 => refclk_stable_count_reg(15),
      I4 => refclk_stable_count_reg(18),
      I5 => refclk_stable_count_reg(19),
      O => refclk_stable_i_4_n_0
    );
refclk_stable_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(22),
      I1 => refclk_stable_count_reg(23),
      I2 => refclk_stable_count_reg(20),
      I3 => refclk_stable_count_reg(21),
      I4 => refclk_stable_count_reg(25),
      I5 => refclk_stable_count_reg(24),
      O => refclk_stable_i_5_n_0
    );
refclk_stable_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(28),
      I1 => refclk_stable_count_reg(29),
      I2 => refclk_stable_count_reg(26),
      I3 => refclk_stable_count_reg(27),
      I4 => refclk_stable_count_reg(31),
      I5 => refclk_stable_count_reg(30),
      O => refclk_stable_i_6_n_0
    );
refclk_stable_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => refclk_stable_i_1_n_0,
      Q => refclk_stable_reg_n_0,
      R => '0'
    );
\reset_time_out_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"440000FF50505050"
    )
        port map (
      I0 => tx_state(3),
      I1 => txresetdone_s3,
      I2 => init_wait_done_reg_n_0,
      I3 => tx_state(1),
      I4 => tx_state(2),
      I5 => tx_state(0),
      O => \reset_time_out_i_2__0_n_0\
    );
reset_time_out_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_cplllock_n_0,
      Q => reset_time_out,
      R => \out\(0)
    );
run_phase_alignment_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0002"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => run_phase_alignment_int_reg_n_0,
      O => run_phase_alignment_int_i_1_n_0
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => run_phase_alignment_int_i_1_n_0,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => data_out,
      Q => run_phase_alignment_int_s3,
      R => '0'
    );
sync_TXRESETDONE: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4
     port map (
      data_out => txresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5
     port map (
      E(0) => sync_cplllock_n_1,
      \FSM_sequential_tx_state_reg[0]\ => \FSM_sequential_tx_state[3]_i_3_n_0\,
      \FSM_sequential_tx_state_reg[0]_0\ => \FSM_sequential_tx_state[3]_i_4_n_0\,
      \FSM_sequential_tx_state_reg[0]_1\ => \FSM_sequential_tx_state[3]_i_6_n_0\,
      \FSM_sequential_tx_state_reg[0]_2\ => time_out_2ms_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_3\ => \FSM_sequential_tx_state[3]_i_7_n_0\,
      \FSM_sequential_tx_state_reg[0]_4\ => pll_reset_asserted_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_5\ => refclk_stable_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_6\ => \FSM_sequential_tx_state[0]_i_3_n_0\,
      Q(3 downto 0) => tx_state(3 downto 0),
      data_sync_reg1_0 => data_sync_reg1_2,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out => reset_time_out,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => \reset_time_out_i_2__0_n_0\,
      reset_time_out_reg_1 => init_wait_done_reg_n_0
    );
sync_mmcm_lock_reclocked: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out,
      data_sync_reg6_0 => data_sync_reg1
    );
sync_time_out_wait_bypass: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9
     port map (
      data_in => \^data_in\,
      data_out => tx_fsm_reset_done_int_s2,
      data_sync_reg1_0 => data_sync_reg1
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AE"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => \time_out_2ms_i_2__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => reset_time_out,
      O => time_out_2ms_i_1_n_0
    );
\time_out_2ms_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(5),
      I5 => time_tlock_max_i_3_n_0,
      O => \time_out_2ms_i_2__0_n_0\
    );
time_out_2ms_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(14),
      I2 => \time_out_2ms_i_4__0_n_0\,
      I3 => time_out_2ms_i_5_n_0,
      O => time_out_2ms_i_3_n_0
    );
\time_out_2ms_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(6),
      O => \time_out_2ms_i_4__0_n_0\
    );
time_out_2ms_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(13),
      I2 => time_out_counter_reg(9),
      I3 => time_out_counter_reg(2),
      I4 => time_out_counter_reg(1),
      O => time_out_2ms_i_5_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => '0'
    );
time_out_500us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAEAAA"
    )
        port map (
      I0 => time_out_500us_reg_n_0,
      I1 => time_out_500us_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(10),
      I4 => time_out_2ms_i_3_n_0,
      I5 => reset_time_out,
      O => time_out_500us_i_1_n_0
    );
time_out_500us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => time_out_counter_reg(15),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(11),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_out_500us_i_2_n_0
    );
time_out_500us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_500us_i_1_n_0,
      Q => time_out_500us_reg_n_0,
      R => '0'
    );
\time_out_counter[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBFFFF"
    )
        port map (
      I0 => time_tlock_max_i_3_n_0,
      I1 => \time_out_counter[0]_i_3__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => time_out_counter_reg(10),
      I4 => time_out_counter_reg(12),
      I5 => time_out_counter_reg(5),
      O => time_out_counter
    );
\time_out_counter[0]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      O => \time_out_counter[0]_i_3__0_n_0\
    );
\time_out_counter[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out
    );
\time_out_counter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out
    );
\time_out_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out
    );
\time_out_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out
    );
\time_out_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out
    );
\time_out_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      I2 => time_out_wait_bypass_i_2_n_0,
      I3 => run_phase_alignment_int_s3,
      O => time_out_wait_bypass_i_1_n_0
    );
time_out_wait_bypass_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFFF"
    )
        port map (
      I0 => time_out_wait_bypass_i_3_n_0,
      I1 => time_out_wait_bypass_i_4_n_0,
      I2 => wait_bypass_count_reg(5),
      I3 => wait_bypass_count_reg(13),
      I4 => wait_bypass_count_reg(11),
      I5 => time_out_wait_bypass_i_5_n_0,
      O => time_out_wait_bypass_i_2_n_0
    );
time_out_wait_bypass_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => wait_bypass_count_reg(16),
      I1 => wait_bypass_count_reg(9),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(10),
      O => time_out_wait_bypass_i_3_n_0
    );
time_out_wait_bypass_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(4),
      I1 => wait_bypass_count_reg(15),
      I2 => wait_bypass_count_reg(6),
      I3 => wait_bypass_count_reg(0),
      O => time_out_wait_bypass_i_4_n_0
    );
time_out_wait_bypass_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => wait_bypass_count_reg(8),
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(7),
      I3 => wait_bypass_count_reg(14),
      I4 => wait_bypass_count_reg(2),
      I5 => wait_bypass_count_reg(3),
      O => time_out_wait_bypass_i_5_n_0
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAAAEA"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => time_tlock_max_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_tlock_max_i_3_n_0,
      I4 => time_tlock_max_i_4_n_0,
      I5 => reset_time_out,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_tlock_max_i_2_n_0
    );
time_tlock_max_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(15),
      I2 => time_out_counter_reg(11),
      O => time_tlock_max_i_3_n_0
    );
time_tlock_max_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_2ms_i_5_n_0,
      I1 => time_out_counter_reg(6),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(3),
      I4 => time_out_counter_reg(4),
      O => time_tlock_max_i_4_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max_reg_n_0,
      R => '0'
    );
tx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF1000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \^data_in\,
      O => tx_fsm_reset_done_int_i_1_n_0
    );
tx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => tx_fsm_reset_done_int_i_1_n_0,
      Q => \^data_in\,
      R => \out\(0)
    );
tx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => tx_fsm_reset_done_int_s2,
      Q => tx_fsm_reset_done_int_s3,
      R => '0'
    );
txresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => txresetdone_s2,
      Q => txresetdone_s3,
      R => '0'
    );
\wait_bypass_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3,
      O => clear
    );
\wait_bypass_count[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_wait_bypass_i_2_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2_n_0\
    );
\wait_bypass_count[0]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_7\,
      Q => wait_bypass_count_reg(0),
      R => clear
    );
\wait_bypass_count_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_5\,
      Q => wait_bypass_count_reg(10),
      R => clear
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_4\,
      Q => wait_bypass_count_reg(11),
      R => clear
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_7\,
      Q => wait_bypass_count_reg(12),
      R => clear
    );
\wait_bypass_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[12]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[12]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[12]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[12]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[12]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(15 downto 12)
    );
\wait_bypass_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_6\,
      Q => wait_bypass_count_reg(13),
      R => clear
    );
\wait_bypass_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_5\,
      Q => wait_bypass_count_reg(14),
      R => clear
    );
\wait_bypass_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_4\,
      Q => wait_bypass_count_reg(15),
      R => clear
    );
\wait_bypass_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[16]_i_1_n_7\,
      Q => wait_bypass_count_reg(16),
      R => clear
    );
\wait_bypass_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(16)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_6\,
      Q => wait_bypass_count_reg(1),
      R => clear
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_5\,
      Q => wait_bypass_count_reg(2),
      R => clear
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_4\,
      Q => wait_bypass_count_reg(3),
      R => clear
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_7\,
      Q => wait_bypass_count_reg(4),
      R => clear
    );
\wait_bypass_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_6\,
      Q => wait_bypass_count_reg(5),
      R => clear
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_5\,
      Q => wait_bypass_count_reg(6),
      R => clear
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_4\,
      Q => wait_bypass_count_reg(7),
      R => clear
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_7\,
      Q => wait_bypass_count_reg(8),
      R => clear
    );
\wait_bypass_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_6\,
      Q => wait_bypass_count_reg(9),
      R => clear
    );
\wait_time_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => wait_time_cnt0(0)
    );
\wait_time_cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1_n_0\
    );
\wait_time_cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1_n_0\
    );
\wait_time_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1_n_0\
    );
\wait_time_cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1_n_0\
    );
\wait_time_cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1_n_0\
    );
\wait_time_cnt[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0700"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => tx_state(0),
      O => wait_time_cnt0_0
    );
\wait_time_cnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => sel
    );
\wait_time_cnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3_n_0\
    );
\wait_time_cnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => wait_time_cnt0(0),
      Q => wait_time_cnt_reg(0),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[1]_i_1_n_0\,
      Q => wait_time_cnt_reg(1),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[2]_i_1_n_0\,
      Q => wait_time_cnt_reg(2),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[3]_i_1_n_0\,
      Q => wait_time_cnt_reg(3),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[4]_i_1_n_0\,
      Q => wait_time_cnt_reg(4),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[5]_i_1_n_0\,
      Q => wait_time_cnt_reg(5),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[6]_i_3_n_0\,
      Q => wait_time_cnt_reg(6),
      S => wait_time_cnt0_0
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
afmoZr9skqIZsiBY7liE8HhN5PT9cSTlE7b7FkfYEnmAz8/sYewHAx0YfvnPtVZDeaUQ1+SA/YSK
81BVyxiW6Q1+w7v5E1h+YJTA02rq79x+8OQbPa0mLIjdXTtNmx+BnF2Qrq3QGTPNoSFginwNNM6R
DHZuBkJnITzBkDYC/to=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
oFhR7iEHB5cYBaPdDcFYa+fK3Gj/00Z1GiQ4ZVkb1MGu+Q6dRi7Lgvhbwd39WT10uvi5m95gOjlX
gGa0fqW2XuRf5YGzXsxqVh0xVmshpf7o2GLRr2CmqpGBtEUVv4LbXJ4WiaEo9wVMVr2rBg6MtuPB
CQBdJcTka6nWZivdNblYGd53J9DObofIqJ9W/NUzFP/DmzVmGbctoa4amp6KUecoKxnO4tESoQzm
lz7s+Zslv3tCv5wVOwV3SAR+Y5ul9hQX8OljyF82Z31ipd+bzojhPg9RhmrvIMbFqzM9eU82pEIT
IZzSVKYMd1P6t1qORfwdgodKT0ZApoyFVxfVeQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
FJ5kCtzgKdagetGF6bdHAPKIu2cq7BcnQFRTWMN8j6rUq4ZaiydIPtRo6ybDhCzgm0ZRPth8Lgd5
+HXk3g/SdsTLZoOUuXnEz+TkfRJdu8kdJ+0y8sJMNK21IEW3pSWsj/n4toksSjLJ1wENuoq4XqDK
9VQo0Gobmx7OOqOYFuY=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
d/VWmC3TdLGZzbtN4vTxv6kYGkcDx9TNbvTz1ALcTDtGFLEUmm0Z0/puE1NDYgBTJquGDZ+j8d2H
/il9HfJ/2n/hTxPKUXm83KvLOZ3eaRdjaEk1wHy+UsjHJ1VztyOuT/nrMaT3sUq1kpP7wkC5I2EK
lQZbp4ngkuiSjWCad10Yoj0TX6DfE58qXjaybvWsINo7H6gMzi4JvYHWbBnCYQS/RkvaQvSsne0i
YvfJuYy63HVFs+Afh6hOO34nbqzXvlsXI7SIicsCUu21AgLUNio4IsL+Uj92NEPjaJnw2wTXrMuH
A1a3i03K7vrZ94AIVwtSXRwfN4q33BWP9bjYdQ==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3xYA7hcw+q7P0QSxtus9heaAij7kiNzMgPuCXFQTIhWPWN0atyWMkDqoxytTvKyCd0IM0n0DheIN
XngnbZmTQpitsZh5f/e8iD/+iWxl+7S7I6wyIivHhUA/yP87+KG/vFGPXu7WexVyPmZuOgSS7sS/
Fnk+QS1l3qltq84DH59g9EBTV7YkcgLEdICkCR0tgP3K8aSVjCptgXzA0onYGrkQESEdw8nPfEvR
g/5lKhHOmmE6kTh/XfO6eQpuQBKOlD7RaVWrPxnTkM9lxrjRZZb3OSfaBLMKhMGrOdEY4RIqfHMG
MBOH8gAUoIRrgyLztGTWc+RsxUUYHq9Hwixqkg==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RyOZOz+qJ5WH/47JnOQuMFPCUAyXetmhoeJiNflULv2xv5JJeXWSOURz6kOeJ5/Jbz2KA/psbOVw
rwZKAlTJoEVMq47lcrgzkIZPdllrnwbNl8hiziEb/YEq2vvpC9EJzY7LlRbQINiQY3hAzP5B/s+N
lYRpAUjOhEst2xZEiFz6cr7zAMz2jn7lHWYuvu75NGUv8JJ7PZ6vUISUeHOPW9ljH0TH4CnOaTpM
Mey1hosrMsp78mwEmy3l4n56Khrq2FOUzGZbi5lhqEebDEU/90LVUpUSzOkhJ03OfvAgBuj9ejqz
WgZRD6DpCk8BVUByKAGJqQZsJ0NcEnE8/btwhQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Q98gl70eQ6J1AYIClxRNKl48Tz7BoxVypGMCmvUy8HqLl3jjFsIYy9amcj+/0Qts3PcCHOutO4e+
ptR4cjDneD+41DiKHcGc2sgfdKq4hG60mda13C9qtjKCuBhg+Zg1jHi3TOh4bVtsLuydPMwaILNj
7ORZo/jzBQ4OUPNFy2tkOMJArdeTOIuydxhUFtpy6FAABlc1SE1Mer24GTLYw0pJtOOJOX61uJuc
ghuBuHaXV8kExCP3IU1qx/oyfIpY9TQiqLKEf0YUrmEzBzV+JXQqn3Jom9PXTay5YJ+PqraK9iFt
ai1YIDay5F0ncLDXiT7OMRFjFG2mL+C96DFEmg==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
DgXXbyNB3XMOH/gmmWb6uBYjn7XQuHgaTP3eav9Y+qTkta/ItNcuqONFgEZl6duKUkQI/YVDFLf5
JwZHmI7U9IACSpdRAsITpVXnabTJhDmcKIxi5anNhIeSW9RerBcxlMYtuoI5FgWJKz6h+0yZmes6
xYMyXTT3gJS5ttPc1n6kBHNZpxPgl/Yv979A0/ZxYkRTfuWrCyaS5T08AajGwOyC+2sH4FjL2xLX
uR9a04wQespOimJ+InC0FAfYhqUcjs5b18x0hSfPhe4G0xKNvmpukNOhIflD4bBgKXl0kqXnpatt
7ejKBpSWKRjk/M7rYOxErj8EahuRFH07pBR9b3XyXF3MfQBwAzV2GIz+A/XzIUDQOcBbojfGwrVA
2HhHfn8sKZB5ktpwF8MBjdlwrtn+kdQ8ux0ZNrgndrvw4nS4TWxZtQeRc+wfb7Q/lIzw6Pc+Ifnn
b7UJlJvNr5ZF2XNxuHGz5L19UYgB0gpH8xWvYpn0Od797z3FfYIiGMNP

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
YW19UgddTwwnJVcciGeEKvH9ZuBWuoo4LgtOhQvaoLIGVBAQAVfk5VY0pzvluZUn2+81fwbTPSLA
O+/RYs39A761jlViEBPYfmojlgjyiOTK9aHXNLsD3WBxfTXGOS3fHhhwgCmiZ0OncriPRHcNAK7N
4cyj3anKb5y4g9gzt9YeJ7T/zkLQuzT4zqnb9ijTisbH6HxpAVXI1ydKtGzVETw+s55Jb0gVXyGi
Q1hVe4oSDBTETiD85J2/A1KF+2DHKkzOJhr53qxvjND2l/LqsA0G/4J35IdLcmuYWAoCJS7cHkTk
wnGOjEnNbP+gboLu5UrXAFwXCo+jsZLVqIzGqg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
58apVjNnALRGpDI2bjJhSWJPQlj57Kat42daGg5JiM8yXDkjxm/BmzBRp9oBmOurwCnZOZcKlj3m
HlexbUqO32l40m0mMLkLzlGYPsduAUijMIb7DMl5HUf9XunE4fC69CyB0KyecsJZI9Ru4XxZrniR
CYqqjLtNjfFEppiRGRN6bXqv2T5Vgbh9qrMDIrkZnrIUB7RBd6xTPZ0xU1V6/f7Oftp0kyRv3C0z
HF7RFTnZ4y90YOsvuCM5CoAyyE/0gTXvBlB4icz61XmHsIlwfUu/xC6yAitX3dikrJ3DWeMC5Soe
vAifs/1pf/61AlzL055eu8EpDBQRH32ikamDmA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GkhzmJz9o93IJx+1EnrQfOwa/OOrgHQT2+XhwLsfJNRk9bxhLz472g/aTSOruRXBYVSa7twJJ6+r
lf0nvm4VQfQ9bG+fbYjOsryrSSKlth3/Bq54UcBz0wLrrf59Jt2KtKCOUQojHAIinG8CLxMBgQJR
wqwzAYtmffGsCYbobk0Y4IPkjQhtQamQd1BTI8ZwvF5zPN8kBYuoFLMFqwet6s+kcH/tQSrB0o/K
lHILn0b8uovehAHNWllP4phB4MeAOgMZbbsniJgqPQEhSd4nOUZs4WnWtYOsf+rCLliHTc9c6HIS
0u5f7mdvO+uC8Pi/nwNFr026nnYUX3YcG4zdBQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 59392)
`protect data_block
GBOmcsox0I5FPXJEerPEQrbT+oSPJp3R+j+IyDP4b1bfnvwnLbx0cXUVmFwl4wnPJ8ANkNxGb7RS
qOAbsaSIDtKUWBD7lS/tdywNTZqZvZbe/hCpvNvxV7F6B7Ge8uZJF3VBS1aI3UKzNiuESekvsOox
cLW5217OtIl7j6gkEKESYelT6Xiv22I3cje3QRcRhweywsW+EfOZPoG35c1z0MNjeFLZ7n4Qo0ep
G7H4Pv0eswkqMFpuuTc2ULRyBotWpMUS/NnZyy4Ix9h5mY1gMuqNwKfMMFc1C5SnBIVcEL6qUnOg
Ne+cksofHohRv8yPDu3ng/z32g11iR8oNU42sFOMQQzezvJSvGNmrQsHZVX2ZXqZbe8PbnWgK4kv
KYbrigZRxfyIeLZljpukW912C4pySTsjBgAMRiL4IQ1+DGl1gueOKxyGKKSg0hQBOkMd9//J/uk1
21ENetghWvpuLKsygx90VRRsFLg/9qpNV54ySrZNUy6oAjowvMtYxEXQtWmaRADdGfKeh3Ssoswz
dR+Ln9ntNItT3cq0MJDYY00xfJYoudc0uJwChdxOexbS0hakjTgnqP0YOjTy8DPtIKC3mHVNr3kK
LoOK21K3IQLYpytqBNFH2a8R8L0ZQYEggf/FK4OzTfpWbUvLv54Q9UbVdcfxdnkkscFk2h82mMXQ
xwfoRo8LlDJ2ag6IbGom3zQeAfGjmnHi/nMmCY4kVA+wls+mZEY6I55PvIb1d/W2s/xzBvQTURBa
AA0jg9ilva0yArd8vAQ1GFOjQzDgsQjWKJjyuYP6ToG8Yr6GN/Tx11/Wg5reeesXxSuG/cub1J7J
LYytKK/Lqm4rq6vo7WVeGBbQXI9daE6PWGob9592PlKoJTRVbsapWiHjXgfRotNEydoInp20YZbP
hThbq/PZ004EMPV1feCKBqeo9chTGvuEVDcx6ruhzFqidENRDYHXI8Fy8XPuzfp9VIo7IHeX9E6V
7MlRG+3/bnFacUwAtbW8kGAMnv8/k9PC1i/GWChyBpe4/RbJQpx7hzbwTJ8PqsB/ptbYIq3zLyC/
KFQMS4AMqTlEOW749N0EHr1oOJ9j3WJQvAgdLmwtsAURxrQ4g0N2jMrJYHtUFwRPi610EnOZgAfX
A0837G9RlQBaGlW9dP4QCD2iqtC908yCe9DuN8RYHhmTRp4F6tA+yNsC5nkrcZ/7nBPEWZRIUTpb
o4EsZDb6us02/pEMkq6MStVdgd4Lj1UMJaQMcBfxWSg0X7qTnwXt5xzTM3SQaCA1ONIZH78qXJWX
2IWNVdZAek669nrmIg4eC8eq3W28cmKzTdRKfsaNhRl5+9yWLRD+l8R5sKyoVhxusHcxoAltvu6F
hX0p3n02UD/8PvbHYHGoIaUMY+eyXsgBRYUc2BYN9eDDZ2cV0GtID09MPBP9YrZg8/BbE9Gqq4JU
oGDxwmNm0eGo/HVyIq0oTB5Exfhc7WeutbiVxLiWLyBqo/Rtnpbc9WGIiZmK83Q77RUiU5JuFdPI
+e5BeNpxC0cenxdhX9eK1Jk2XSB0np/apfY2ldDajGC0Cy3XC71j9L9d57Unv5lgKm6jBfe+xcHy
Q9mYksSymYX+5Cm6CfA+C+R4SzDc+0kID+po6uNwxSPM0yueCQuh2j1XvHUDRUBJdPsr1TSATtFe
fK0nZUo/bVbZlwJ5vi6MGsU/Gs19TG4I8RqBcTCWndA6TQ+IhfXDMfKNOhEFONrZUAuz3Kxs6UO8
pIT3G+CUSe/7KcANvJ3nR+uv9FwNqhU++orkoIwxq/Gb5o1Ys2dkQApNSiGRN7DEXs8vCxivGISi
jfqdH2xO1JSbGjIIm/BCR+qN/Z7O8UBmu86x5F9620b6wnGHIjb9nzMga2ibx1EW2sdgOj/5MfVf
49510GejlSMiVFv85K1vCdivq0vV5X8Spx8K2Ji0RQJ+ChnrHtjonYfX7iw6rM6V/8A1vGF0b3X7
LByf9y44W9bJ7aIGabHlj0zg5bFFiaeVdIa4JVkdep7WHViJKyRH+/XTmWpO42+7uR9o1drVVKRV
x+4Jggewyb30mwckLKU7Z1tImawBNAcyQqaNnubl0zS17oSB8jRb/yzp+pQssoIxSZa512X82c+c
lTTc++zC1sUkwOOWrZ/TwcY5AfdMq1DH83ObJ0qRZyXUm/uzDWxonmiawOpLQfQlrHZ63lLhPMHj
xe/VLwMQime1+6yRxDMxBL79IGh8y3rJ4dYjcanNhOoB+k2XslaEh7tRBuSYvJOkpShLE82m2Ut/
2T85p6YHZrq9urqYjhGILLeQcgBZwRUGpNm2rjdMS4GEPRLoU1J4CMi+f1wWVReYC/e0iXtI2OmE
BNIUFviof2xlu4KPbdbZtJT4+dYxne4kZkdUqeDVSN+0B3dlLK5pOXyDcrlSvn4sbsy1YwglwP4H
TERFGlFziHU/ibeQEux+vsqL/H7ggKG39xkPW7ICi0Hjm0MZX+oNFDBCfMDvsyL9+9OrMqIFGaXz
gEjy4yUqo5ModeSjRndbamKyh/rzDlA/DzUxBOhV2QGNihcWAJ8P1EcMszxFGjdMRs+D43gCbdbG
eivpPmlmvhuPQ4jM285r9fKVndUpcRnbxE9SuyksXZ+XwwVPb08UCuLpGJ8FTYc3dk5RIzyudXUS
6ebVXfPSW7dSB1GPWkzCOmkyiFBcZmj9tYcs8a6xUfxl7iCGTnzjtyVw38LuufcOQnPC+zD+4kmX
SQnt2iPpX5yi/yP8PeAMZDfFBhYZ6C51bMjOhZ1BfEI5DOovG1drOxvwtIKQxMfGbo0v/jlvN95s
7bUsjG5IwdhzmcC5Co9UoAqXlIfFPq5lEgrBzXjKYWf6n09VYWZ0Ce5WmZ91F7XVZkmDcXfTMgYr
RbeoWvHEzBhjeBCGz0K5bEqWVJfxvnAjLf2l+oeMuFeuZvlMe5rTh9YZOp1nsw4qg4qz9pMI33bp
sf8/9+RylaCDc2xqLvlIUoWEhnF4IqBEL8hX0i1tz2VPSG4dtsMKf0C/Tj4zmaQMOy602Ql47Mcf
92pzkZGiO4zjKS/p+Xg4JAOKZz3R+qWCMdm9mPyWv9e+knd7CugPJrNPfxxTBAQhs4Q9T3glci/r
AaOKpiHDeDxs3b7VPOGy0ccOHZQ/6fhZ76xq/hZD48+9l9l+Iblso2oaLk5Ml1gE+QrMfDBfgDCr
wMm8Ocqms9mbmzQkrYwZa6vZWG3WbEIHuyXsbzWYFKVkZjFYEbVwF8+HgVeOjwdm9tzRsT0cLI2k
jk6SEJIFvrx4YVEMKmx0tQhRbTzdBxzElFP8J/NLPhsBZ3HsA53KrRmUMQrm2UTEOyuWf8Q3OAUO
skDyERQx5DmHFPlMvUYXkMvYkZuEpLH1THt1joi64FiL4L4XGiOiO1OYOqcyox9T/kwxtpPUPnDa
uQwdh5RXX/rE86K2wmVfgd/neHUHVq1HhdBSUs+bYzAk3Q3vqv/oxcdtpGrj0EGUIQc1fG42qB+J
GXqZkjc+PAtLGx145M5q+a61iLWBy/v7/SFCix3TOvcBE5jN3gDNm4Q+q4d3UeL55cL+P6AJ3nZ8
+Pn1YyhU/FM9GUg+FnRYPpi2+TKbsxb7Yukux5rUV+Qt6aBnmX6eR8feansyDhagdE9ikKlmU+jJ
+QZtkFQcQHeLtfj/tNTPJgQvSfBOpq7EhLPuto92Y5jo9kaQxMRaDjPlrH7DRpWS4tvxH4PxTeZ/
FnxNm9OC0cJsrtsWP7f8jVvpfALmE76bEXAlNOoq6sYvgUTyMGbIyGdDfrzECxDBqvcYnIhfdqTp
VqG1I/lzsWwr7WHbmz24dMsisJjt4+dGqb4G4tjJJMncuJJ6vNpxvvYkcpfOewDQPEIJF4ekt+q1
lVpkc6ARTzjBYYOwrifJBeajYtILZKeA8re4YmGPKyypHdgZIDz+xEWhHBNnHOtUdCgLVAhiM5K4
S2Lawl+P1SgmLhX27QONEKD0+wzMHjkG1WInrf0oNAg76b8r0N3sRryAY5draP0w4v7pp/3Jxdy4
jHLPCJKUfxrZdJvUSLe0f1ixRhK1HvOPsdkHzmVcNZvSLzFEdEaPiL1pK7MHRjURB4hhxJeyrMgG
F1qoGX9+0uwzS3ui0F9kYYjDhynRwcr7g+1Y5hNwOMr3/KY5iYrcoV69VBUoHFDGUTf5dTzigDug
26wRzhr6Sw6oIEX1ZgIDPlTmnqrsV5NfvZlWwc9FzQtfJdYns9G+HKwjswybgbkfJVahK2IoVLOu
p5oeVkgy5qiPY27C6cHO4W2P6B2flYq0v6bQNFCFdgms5E+Mv4ESdtn/u9QyVnyHdt0kJ66DEK1C
WN0e/yCSL0r62vrLcaZX1unhmXQVjmwbJhteZXRbIQgUdpHQ/o1GLSxG02VfTTj7JMqCgJgHWbm1
fD9RA9DD+49EC6t163TrVld16l5Jx3lpU3tpCWyCFs8KnL1f04szTNfpCxCOijWRfMYYi8RcOmnT
SJheibKfNsK/oAW4s78DPlxLirOK2Zrz8cKZL+0jkZVT2ZuWbXweugpEhEUqex062oycVUtlDe4D
HBp5490rRKuin8aUQ5BDHoml2jekRa4w8GM6VK4tA4IqArkHA7M9CMJy0DBKn1G0TVDkab91qk/L
/Y3mjG84rpNbPpVJO8/jk5Jk46BH/5R2p1r584J2DQsKABMQO75oTWj9xQOAOY70lVCb3Pyx5eVv
SNyvCpsTBoC5Xmo5ozPERdwWdWS+zhINjVOP5T4T5k/PubHQLQltTQt0vMmAVnM5gDX8heo6e10G
Nh6nh7gshttStswVKJ8Qfa9DalaQTp2ny80k6jdDzzYQ4+43sQpTor2Q5jVJDisT1BXuiwIIr75b
bZiu6EN7wtMMmgz8GxXUT9CiZkXgz7ocmcQK2mPKEsgnOBfC+tprvvmoK1Q0wORXgKHVYvhRL66b
nCv6ulOXoxhVulfInvp3ayPPKmbiT7FRZKm4brI+QdkJOfaEDUTUmzx24wEGjAUO9ew2O2ioN61K
dURM18SeZkUSw7eID1tBQI89wuq2mbEeM6q/JVvk9XfyDzu0zlunIYPYC4JlENGFBd7pFBcB/+aA
8aH+7LbBl1memxHcogbC9X9d2BbaUa5rqPiYzRB62Lb1XYodqmI/BV/Y27z78kme0bYId40h1dHw
k2tQEXNar3KYRRGYiCn5jIMn5QOk1z/n48u87zefg+F4nMG3SFShhJrRptK/Q2jNlkt+HZS32s0S
dOaw/C0sfktDad0XEQVd9bEZ7WnEOs9w4gFoJFqU9rJwTZsXv4H29PAAKPjvoCR4kLEuiyNbJysD
PSrRShrQSZZsqGTXRZqI9/pRDwfTQAvnn7TtB2FwjOfsnA92Ib0PzJ2HhGYaHnjJSFy3lVl43eEq
CK8VwR5YGCib+URd+nyQveDOQQYsJ78tHKO7GpADSMjTA5IiPl/4QADPaEpjKE6L7GVIn25dsS6R
wmmwZqQkuLO8J5t12f3nuso35+xFEb7eX8/V2QAP/Ng4A9v6lP0M0uwWKe0nFfL2QbFYbFYMzaNZ
s4/vx90y2FX9rbeujpHv3yCREQV5TapJ5xQ5fYlIHBeswOR03ldhGWtxSXhlShLvZg4G1Wp0S0e/
Dyfp9xOe90N3HAcYK11Qvotdg5xdUN5atrBzuqSXw0o45J3mdNhmzDMEPJ3qNRqyBGMMav2LJ/7j
TDmdhHf8W16N3yx/scEHV1u6UJTpAsjyV20rfwws17vXgkrxjwBdYZkAuiLsPIHmJ0e+zXKPl2R9
m/COWddnWyS4gzhuqG3lswqFr9KzJiunj8H2WOTRedUL4dZfQiDHvzi4PXEJp1DRxPNaQjc7Jfjb
PGC0JvDKGUpUpEvvjlE0Hq3FWdBABt2N5O4whV5576cJx74EiZVojtzYx479O89POotd0LT01jmi
AnWIIMXwMIHcOsEnz2OuasDSkmIpnk71ZAnI+UnKSDuysKMitgRedlV3yHKOHAs0XCOGqsqgjieE
k/6YR0oseQwQSgeY0j5sdemwh62UxQ7vdFaEgLcYFk+ucNStYeF3RTfspPVXpiqFjEzbzNyWBBdX
6avJB+/oXkcOYVSw8m9zvJCETl0P2vOABG58fJbd+hLhPr1TVCe7q8F45PSnrKjIWVV0KonughhN
81rp4TT6e7IHTdTEFKwW3g0uvwyviNbLzPF3Qpt3wI6vxE0EV2a15v5Z972SFhcKsUYh8+8jwQu9
rwNI+trALIGGbK9uDA+V7Jac94L5KmQmJCfDvBS/4dTBAsDl+Q+tzzWD4TdbsiPofkot72Pv/QDu
C6jNosd8LveT9AB0M6N6HsjkxhGhFpQufLsDwwA2RQxc53xW8NJIvK3mmhd25RooRvUgPFcNa26W
AyyTPdXvJbaoQoL/tOJkICdgE6k44EMwBUJIAzfiZYOnb7wuJwm6JNo4PCjD6W/M/E79dNTk4kNz
Pj4F81JLRi3j5v+B+eVqwX9KTt83fVGRwSCsp4A3LhrxM6zNn7vYFSopG4NaA7dACjGO+RMDcoz2
wVUesUCtrH28qGpO+jAwOiCNrM7I5NOArWCZC0L2F1Q5HlPKogiZpGkS85XkO+pdjvEVlKf39QRp
z80dF9opGh8p0Pk5JCA/IxvTr4BCKaVEyNPw7NGnxBGS8VqexOwSdku2aYY6z4JH9UcAqTN3f21g
1/b0/ZGGnDPSbJ7MOMOS8Ob/nWhPHYAL9LyiR4GNk313qqg2DEXnVJO6JAHaBX1Rw8UVFsiaV5SV
rF2ud7+6m1kFX6IahaG2hJM9da/oSTeqcd5iLAc+7siURMQ67GIU1/4lp4LktJhVmS3OSp63A9HG
cTxoNfUbG2ZQ8StfG9t/kUt6/Aiw0tRO1nhWyNVkvOrgpnzNKEv1ubRCBoWcJ5bqxKcQSYE2+Y7h
NbXbvtA7ZaMH6/Zq2BclPF+nwYWL1L4yxZUoppxFgIzU3LGnOsMsOiR44gNQgi7PcYywnRA/Xdj+
64kddbEO2/X+bV2Zh218X+RMzi7amJiQ0rf8reNbmpxM4NMOItpsFZ+pclz5c/xUAGxFxFHbPRDm
FHFKeFSgJWBQ1LpLHwXDDUiFHgWqdjYKzGT3vKYblQE/E8WhXvqzlc1WRjFaNcoDh6j+NYFbRY3a
ui1t3qVWVOjx1b1BAp7mafifntlKCd5DJ2mullVU6L4dHjbqqq4jWtc69rPm3KuAixtYDDNQ0PPp
MHSHb9xvHDPrc4Z02Hq5xeGt64nISi87PFR2I7+FFTIfsnR/iyRZtZVsdXy0zv000DwaZHgQUNVi
R1BM4F2/G0++JK3XnS3M0ThxVR4ARcBXeCLBdkMIMlTZSsqzD7hcLPfJIX0Sh6trkOAP4KVWcS8Y
2ALiXNXN2wjzKdwzl9e2SlnJaQTj2eA2g0f9AO35RI+b3JeTLUThnYSGaIOkOMT1ToCe244orxnH
iXUdZolxbJgyQ4jtZ5Iqg4Y/pNwp9GTzlzRXZHeyR71PCW0zwKqQpanov3dtWTkq5oh5GSW3Gv/R
5mr8yMBmjN06ZcY7p8mfOiT+lz+Vu75ZBvw42uL729YNNh0jRehsdOyoVDN7qzx/Ihdhx03gRdLo
mrbM8GdQO880MBLWNBoKUhn47ukiQiOsqshAAdt8q7wM5pI76cs6dz6KZyoNV3ytrLXyWEt4v+D7
b0rr2CkHW1T9ZFMlBzGpACBgiG3DCkQEkClP3Gs9YiJt8U4YjxAEspOOWTwIVROW9oIPFmkUVMQ9
1wwNBL9qdLuO1YFID8ZXUIXVqOBS9ouM5BZNtVQNNWP7VmkXG+bcK28ypw8RMdiyecIDu2n3n6CS
QoW7VTkvl6sAeOKjGoO4YgseA1u/F+DfMhe2T7FuJIYcnLb1qLBqNuj4nVjBApucUexhZVMJfAnn
FN0Z6/B77pzTzhmy220NnF3VACOQk/CiH8HvrxcH8uZGqDJac7Y6ab34h1WixTQBTLoW8Niu4CGG
KJFxXS73Vw7tBw1aUg7LpILJbA7da+7NeDstTYeeNXk/bLheFACw3GXdOeoIJPYsycrPWNpd9SCC
7rcxS7aoUkmGkmfv+gjkhIkCVXugUAT3wWcmDAHxkIH/kkaTFleNepxdcdlA/WXmb3SU2H1J963W
qbtLebuVdc2L7Eux6yR9oMae82m26GrlFYjDPNxAE+3cOXU/YEDiozpS6+duZg5qBeb9oPv3ibVy
CiVdTdUukvXXUTwHcp78XCoLxAgAauFrKKXUe8Dhv+TxqtFdc819lMeNj5k/befLVVbV6AsRVRfy
Z2BnEy8vOPk7wdjWWftVFTiC/Nk3a2uK257TZJ+qnHpZ0P7EYwUY9k/L3YgGAGEqEstvOwbX4RWH
G27qVM9EDKMFVNefyHhsAAnSI3MriLQM3hyVsci5CSd2qxrSzZWuHNarj6il0P6Zl6mJA6uK2tmD
igS8teH6lul5VBHi2DbPGd9xr1trCMxK3IP2jWJw0ZxIS9xBGxbhDzZJq9GzPKI3YjBvPfx5VBQe
fkzsNq5jb5mhWYJHHliyVQqF5nFlVpbYaiOBO6fonJTglQvxCN9tcNpUP7aWQj0jPqxOAfCc/M/b
zUVzHOvY+ZxQMRRQjmWaz5gPOd069Di9IpS0sSnBCb6nAF+Kozty59MkimULSSYokJevPhN09zuf
ORxsDJx3yiWKR6DzO2xu/81j272xoWrA7NStTiA9SGq3Dumy9+Y89W/nPSqg7JXVV80BY5pcf/Ea
+AoFIhL6sDrsD9GccU0o+v1Qt16frZtHvr5sFOA31+PNsuTBg6rnHAuHAEpIYLx9cIe6n3KlSe4Q
T3IduYcWKTyKYD/rW/pzOlZy4ZJzV4ruxhWxDrdUFEQagLI0MX5P3CEwWwjQgjPK+Q0WWYKEtWGI
IV8yx1JzY2jJGXVjgentwvzztXoTJEDBtqUTqhxs+PV9mKYjKo1/MHYkWF17lKuP4cjUrgT42li6
PiAa7j9dwgD1wiBZQX9lFSCWjAjXar2dYAQ0FEkOBAiJSr/5dDmZ0DblmQHvf+/Khceoq5dyzLUy
3UHhd3TAMPE7NiTRcfFTA3pLxnqNfspazHACU3tssZ5MnTZrsmLrAWNEMMHWLRL2tv6lrXMT6pVm
C+U3bGczlv+lj6QqFnDAyv5EgG7/z2DRXizlfOmiH4shEiZXGokiQxpNhiHPGflefM8HkyP0xXhB
FvgINZx1FF4G4VLF8dlHZX89TxfyQWXG0UQpz5vyFZuMNJropKQeso/ReXS6iQRNIXJMgJ8fJVUj
qiaKEMtJAnXZox5xobrS7MqUARvKzblipE1oyXlWVlU1hgWVmbcbs+ydAWi+FMMZrGEVbuaBoIty
K4LHr3PRpqXTZe3S55cAHWg+PPx5SPnY60EG9U2Z7Fh8469sgNZrtuVOQQltUTslji5YUeCu7CFA
rw98ZcEwLxRtojvlY+1SzzuTB4u/XbszJ5RCljItnvwB+ZdidujSHpTasy6e9cMQnawTtfM/pPc4
QuwVULb9Aew9Tir0fBcGiXmqzeABpP6jhYRtYmrPO+ppT8Ls8AabsVL5ezzfd5U18I7+Vv1eci4z
rVvG0TgrsvmknRqKnmqBte6KdGv/04fWcFExdnVqgA+7cJ/g/KfmWUZoCJ6f/bL8JOU/FrDtPN0o
0GWb0NenBtol3v87VM1+y9Bzppv69wxaCJVtt7hirfL9SJ2V6xzV9GD2eFZTvP3098NFpQx++Ygz
biLXe5PXS8Nxom0qD1FVyLzRdzz7rFroDO92iLWTnLPS/hYUCvvWfTkiXUE2ONeCPc6bC1oEXk6Z
smLpkn3swJJX1RTWgsSm+J9bwqzA5raebshcfmx+4PaApEBIOmsCb6VkeChK55cSH+UM5hNIrE3R
AdEhn+qhYC3IV6EqmfTfQEmx9wH3+GRYHsH34CXzKjSdeDMHSpoAUNxXVgNVX+9kGFXCkqNxr//A
YDSekMj3f/PR1mUIfzgjCIf/Vun56BLcUC50AmFE1ZjV0I12oWW6VR4oeznztWr7QESOkqv1lWwW
SDJ3z5myELjhNDNeiTrqsS0Ubo1ch1i5B9KfgNvVYb5llw4ILn/r/tEmNNA5lAISd5m09r/SravQ
XvXkupDxI4FmdlU31RV2/zyQnyxbFwKipDuIfsp2woCbhBdPfvBIOKeA2JUoDNYkWiP1NRcZOykH
rQ6COxJDuH6VobFatuYYVQ9C4qieVnXeJXfC/kGCjN3l/WUq6xF8qrpGnNKXEEC73JFKruiBA5aW
15s60I185pJNI4msfcPPxQ8dcf1gUjUCzRGdC2dp+isOvHL+y76QvxDTi6fsH2HZzsHtiiUlY/pU
UYC1LrTRH2iQrYrXRWu14B6fd12hm2ux99CHMddfAra9UPqUXpwnUAiMiQ0vglfroC1EuDENHJ9+
W8hY9HsRJtMBjnU7z0S9rP4j8/IlKwLAkpbc8V8TguUOOrbaKsY1ygNea79brgxrUtVkC7BFtgJy
HJb5CQzjj0cZp8JX0L0uRn+sDNmRks91GIw4Qa5SUADWGTjmrHPQxhw7Ua2/kLKz996DN+h/5PnT
3H+rdvinXiI6B549sc4etWyklFVHToinz5Q0/WBA0tlDCrs0zs/8dZZfjXazi6GTnd+7SQSC3GYv
UeXZ+gfjhIgWXfRFRbXvK1y1zfrGlzHCpNmNgn0i89+zfY3dAt7gGJqCCZQhc5PPh4RbKKcg7+eA
vcwEksXkkmfhiJFw21KEn2dEIURK9wh5L//YEslcTG++fCZWqRXoBmp/Y7fDPIFoKbiGrDJKKBDX
IBWWJeeJNLEarIh84DepiKQuW4LT65kzfzcdmVzgi/q3tO1gMOQMIVG23Ay21/wW9TnglCGR6568
i3jNuaQsMIFA3GuUZfoSGWzLO0AKv7d5sYaRyQz7cuI+rPHroxUqc44YniLIJ/rLUv9FY6Nrpiyp
XlSbIameifrMvPu1Czykiq35fmf+gGZKCGLnI5UqQK9MhAYSB19C0N0cAhdabwojFeozwQlFq1vC
AEcX2DnJq9xG1VXF2m7TlWlpmiRvdkQ2JLGj+5XnUXuJC0+3Sd0qfub6WvqN6vyoFvhxlF1VLOTC
UYT8WmoTJr9y+AkqfDUCWKjSh1fZ0BWFJ/7gypJVKEY1fW2Bf1xo7oKZqE1PbqEgB4yHSzXtGSSz
He0jTd9+CtQG1HSRkPoOpwalZQRbQ0IRTxIpRThXn9W6nJvg1U0IoN7up30cxt42D6x5cGNkOE/J
NQ5R7u57tXSbb9zb9flLQsraYU/XjGk0bVFeE32YRTupP0K9eIPWSgLbXldpz+UmaaKgFEv432ju
YjH5cHwa9SYk66Y+8xg/1MDkuyW6/7YtuNuu7dLBbAREB3GW9N8qb7Kqj48Skzvq0qKlvW2D4XZG
lwOMpQKVEwLqlYVhE9yUa/lUrRX1pIdaj+h0eX4QS2Jh7s4fqoxZApvOGBWYIjiUREOm3+qOnaTT
sJo3lHC3mtOAFCjmbRGQuGk86/ulTRs7avzo6hNTRDTztFGZbUtEySuIry1wXxOl1t8f7/CF3S/Y
Ybfo8UMlMlgKhwl/u0JT66s8d8y9DVajYzaYnw/897G4KtQ4nOOeNcp4zmmWDuy9p+9zIPCgbxX6
fnbbGrnN5dh5hwobbVyEVmapuJpbrfmlIEAsHLiMsx05IOSQWyQEDxQcLQ/rfhmvGz8TJ/x0AbfL
wxHnX7Uf89hKLvqJagvkAHxTa6fWc9KFt+1M/Dctdm2mppQkg6g600TKHU4x8hvlKGptgn8Rovvc
eqEbfld8DdAnpYQ0uBAJbYX0sYzpRaQc0lUjqOqOj9FC7gXRc+lmBo+vI5jnfw+534jGTkbKZXyX
Sd4ycclMRdaS81JQvNIOk8rzue949zM4bwbst95TcrsbkXE9PACnyren1AOwmwIRCcx0bcZ4OyAh
uH+AkUGRqhrrGbNP0czSvxrGUt6r6YFxt9TwmvwBoci1uFSbRop2T9K5CUzfb+QaYgs+CFiQqxty
NstxRtNdQIscZ7p/ynmEGzBt1zUoz6jv5zA7qll2ueAq5Fzp6d1mqqPwNsAyNAEESqLXlhptg4Dd
SsUZvKDrh8bEZbwY03m0heAXTM1qVQeFZxv3fhArWResb4A5ONEiI6rdlA8x8VO3dA8N95NHdwvv
PUJY/B2ao6jlF1W2JsXBROl9jitbRgFDsZzRBDM+aQMRNeQbuF/Aqkcft3lEYkpRApr9nbBfv7jl
BKMScHtfewtxxWnce+WN684QMXlH/jI6JVmbiXlsh7AxgKdvOQMKservJFbR2JG/49mUA494KlX/
RS13Rlq1rJRWRzkSp80XISchqqzuhktQrScJ7RKKcpaGVMdXgz1NI8ACu2wAOspsU2BYtpBi8Gkm
x2d+hf3E5P/MVh0Pb5nY+wmL58BPS5WwLQ9YZpOaArgVEQ157qXPlo/agp+PRnXmWkc/K3TOqxvI
kXNrFXYP9a7S+2ESERbHhABcY/upPHJWXbEAAzXFsvfTxwsR13cqEF+Nv19Ro62Jta8czbd/33ny
IxaYzZnqHGGtgclSiDrSD7c/IlQ06Ipv6z9flePnsOJFi2hnTJGsvmwWvFQeaec1Gyg/Leanhwml
G1Oo+SYEtb0o6VZJLvEFycUqusxzyMe2AROErBZgfqrIKgb0rUD826SdrS+ecR+jbrzqJTj5BM1a
KLUjeXRUW9JBr5/HNxqLCOZocJJ5lGonYgzcVzZLPxhUrecBYhda3xcFZ++4+EuxJGweYYdwKgep
kPx4sSpxrwJZ21/EWRD027h1eZ0+Mz4Y4H+nE7NwCjiUmY5AVM0WIFs09PMJa6n7/nZtkphdMuB5
1uD8JrAjeZKG1eS8LnX3mNNb0S+jOfe+7+g6RXwcdOeHZVB4+UCY/qmYq6fJkAShb1R8/EK2cZft
83O6BcrQaxBJkOYQzxzTxLMvj6aS1/E9h2YJsRNJ10fcQCF8i2AdkIC6r+D3VRCCAxdlKWn0maE4
1Qh8ZyY5ynrO5rH13qvPsYVLYk292pANmIdvm2Uimu6/wcKOi2VSiB2pkrtVVC79K2Ky16k/IiTO
CNYwTIUW1ca5L9uvAllgqDscmYz+7xB7ea1fbdAze4RHl+HQS23Psvi1OgZV7ymxik+JgMD7Gslx
m5zBBtRTPG+JMqU4eILM5AvK+RGodZVQ2urNtLbGN3hQ7AqnIVrOniprgWsISfN4fWwXLfTxNnlj
Xn7UcQ2MJ2aYYqrCy7emLAT0JFQWOpFgSDTtasAg/YkVs1Qor++D3OmWjMnMtc0o4HEhdoZfdZBB
Lfr5T/yPXPiPnep8pTder+c0g1si7FTbqtYb6iJcwcu4PEf0uazCKaiNK752hQPhMvk2r7212r96
kM16AJokqnxJYTPaHwpTvfptJ6f+B3mKWDkU1qs2X7juAq/0LVZ3q6vnEOJO6k6cFLk+gmoFfpPn
aL9l9lbWpHkl1IYjqF7BfurwGPdWt3aQa7ZEpgJI0STESaLbjmIWxtAg5aUgyIP+G+GcOYJSyJn/
T3OfcYg/2CtEozuzqPCeeUAOsiqJqacuePLc0lLMYi83vcPyKHdzMZdw4pc8rZPfvz4DfeVGkxnU
apvbfaGPnfzgNTSIEDGRJ9wto9AsJKNNsy+0NSIml3JQ+TBgfVx3te51iaHvS2wg5o0943nH7Hhw
fQG2yf1WKcv6rN1DfmOoO5VfTAcuGfVKUhYK5SiYHC0dDob1L5Qutele3p4M9XJ3cD4VlK9IWMkt
Iauf5e3Zx3l5N0oiLS2SKAiifbYABunka2egiEJcMmXlTstHqIFXAvTtx42na7XNj7fGeNJNorV/
4oCsENxOMURVFuM6U8XDlps/b7beTp2tZUKYf6z209gZjfjo3MjFSGYTOJioV1CgVErYuSAdM/Q5
l77kvT4a+HBjJMyJEKDZ2Z9KVo1lc0BWFxaksssNfzrnIGO2iW+2uJP9hefeDbYMz5sLAJg+B84d
U9lJm36oDEPf/SczFubAsENtk0a/tpwERWYgYUKOmyfgX7uwF5XmFdBPUBWFrxeZ1zN45SVTOom5
9lI9IJ+QoPxRMMIuPyhFy65K1fAwGzIt3PC9brTTPXER8Vba8hewiV7RdllH48FspfHoxUWxChx0
clxdPig+Wmi/h7krixVI5qvc8b3gzbWxS1TmglYvkZ3ZTk9Axg5P4W7zUZnUcFUBH+syMEW3nq9g
ELMEr2tkZbZrRCNyPo58NZAjkWCtZT7VhYrisNyHa2XuIL4V2VKKyk8/xxTak9BxFt/QH48NNaU0
X7b++nsE1TA6CqG9isc2PWSikMeV88RyLkCD/0sd0cP52zIvTCk8kxj0j49zvWUbjTdKSUKZodMM
eO+J9XC9kpHaCJTfdvg40prdOiDfg7ywJgsrcJM66c3DRKOIeIG7RDguiRNQm9soFkuIwRd3qnzF
3cWPzVvJX0MegNVJ5HX02RXd+pfYICXFfN1uqEiPEvDVVJk8UXbZuuCcvGtvwFShD8kpi901NLRe
wh+SXRj8LW/cd4XuJYNq+jmrXxE0D9GGHAypYHSJUeDFMKHampHqVvly6qFAZbksxIjznvN8kozX
atA7YHyikuhE54orin5TANs5K5Y341fornyltieSLNBrPYJ4egYofcVy569HmEITkJwW2W8K9wxl
QqzHJw9NnYEe7aZInmJgG8F5Bl6DPXwvm0k6QJbHlXKsPA8FevSwmTPXGBIEThA5jxJwc9z9/Jhd
0wzJpyI4Crw8mB/rdQlMfoLQpnWsbmTS9+RqDsk84tCHGVAAHcftr6SOS8pIPcGeEgMWkUsrDIlm
FzXEymqbBbemGCZCjYrb7xe47HMQAkUxdBvS8hjHtvKWL3PyzH1pR1BmueAlIy49k/b01GXQmwOU
GSTXx5aC77yZUOtPTizqNlwqd1Rjd6rDF/Mahg8s7vMH68CccueTTT8grEBkBzLdhFjysz+Oov9A
RV+/6+/61PctmW2bF0E3IhTrEBgBiZ/AAbir8ScJsVKrtfTIXTzA27URfbKPddWiJ5N60A52AzY7
aY2xU28oQ0LMFgKN9bAXa1WgydU/QAcFFgyoGYmQnnOsGVNuvVbYzYsy9NHSteGCRvPbmyNP346f
xJT6oXwo8MKGvObJivXZjpX4GqktZvMkm+c/ZZREzRNcq2lrcVjulwg4avPqddSq9PXdk15mEO3v
aK29Pu2yULvxlFnjeaL8tu8pl+Mh7YRVBv3fjaX1CRS3tTOvxRc9uB33RwyWqXd37Dj8NDaN3Xmm
5LRs2hFlPMKOP9tNH/+j1qKobVJFg2DQlCTDz7GsddAknIOHqBT88ogNs4GN0/7RxTkNZZbzQ141
USgTMkSH/WQCPO7JV/HKBpykQuyjkLrcBj4j36vCYRgl2VlZYSH0N6jySp2lBsJG54IvDEtqYFlL
00TkgYcULR5nYimgeOmCo5kuz49Xkw7LyhvStP6HizFK9JDb7h7GQiA1L/602Phbpd+tS7LwMVJt
dMZ1JgPebK8kSrAzab7xGfqMyjnrnKUP2qKOwt9lkTX00oDrXy2v6vnWt0gsRc6KtfM/VHhUL5c5
CN1+dpQbAwOD1Q9jE7NnHtiLlcyfb/TAZGQfyqxuHrE3JVP0Hd8wwj84ppomv+dl+0eMb5bTzJzM
NVRUhB8huPR8YqyBK9skeGprmlCPVlWBMxHel3440TGoRJlvSJ993V33r//rNduhDz6zRW4A/pWk
Av+Wk5TVEtCQjcNKrJ8bjKPuXVgl+QTDJLLo+L7M8IxdeXe4/IxpEy9c91+mAugf1o6goQjuR4Zo
ekb51LrNLqNiX0K7XAwHbSHk/AnLp22P1SApy3Uz4OiIKvRtPc6oUp9c6752CJsvtJ3+RYP8L1U2
0mlZAjLpqC9v5CnlyrWrHGob/q1STQrpKtryWz0peF8QuYyuDzYdsmDnpOioe0He8MShKKMw49tB
oZlI4pwvqRi8qzytBr211szXFypWvtw65PlZ2yY+xCU2hKHlEB0i6W0LA5X4lIzUuQlaGM8Eu/HC
dEgZjgqVqPwg3c+hlx3agtiuL5v7L544UHImATOvcazAMNC4hVKLkyKeeAUQ6QR53r3W1SPlp++J
ZnwdQt2jBTTKm5kALCQr1FnItl9pTKKgVvDayr6+O4URM/XI/E/c5NBJYG6OdWOoR2Urb9kcFXKd
oveW9nM2VSZJ4AIEcg0sgPcf2NXTorCg9kijjNFg3eu2Rb2RVoC4nJ6xoVMcAFd/F8FshniWm0Df
1ZodQMm7AwKPtSCWac0C6NaXfNKhwJrwfnJ5OftjK0qjTY+57qsJoJtXmb2p//LSTUgSaxqD50Nu
eMuSTdSqlOIw8pj/zHwFcTGqQUK16kBAi/GhJpEvBNCIHZxl5aRQHH2rmmy7l4vVzyFrXUKEescA
dB4m+vJ//n0F/WcmgyBdeRoLqHLIjq8pbgKRMhD+xBUJ1MDynzmuOfRmpoboVZmjL6rJaLd3PoYk
rmbZRsOUYeztFJ53rtyc3PzEwesE8W5TSsWhDRbIkTUu0kMydK0GP6ODZBrdYQ1ZW1ht5usS+s4g
svgCOCsA2xIPRNfrRZs9BUC9KBfQrxPoIQiFt7BdAWAnCItRXtfVrg/sTuviPXihLeGOod1sIveT
tE1XAomU8RHFYsu5TyndaXKnBh1iZiWzbF4/wwvX+zvXApolR0vQKMhALedRBfO21fym5Pw1cvmJ
pcUfIhUn8P1TkJm5Q3mAT+dD7Lc/Db3ra3D9yOUKCCWIbNi1NCjlJURNPeDbQxzFMBnn2lxWEOVz
9NF5l6YgREX2kMlx3e8Sa6lX2TVXpPPy98smcMOBJEHTIuQmwxExeTfiSJ0161rOGmnhIEOylntR
kcy09WfgU6rcxQjyT0D3hif6KdPYcnAtQyWu6iMdcnwQINjOrvXapLZExfpVkVjLPXFLfoqfy51/
MbpVLo3k4gIY8hywKEhZyGhsafg8+jqjQkdHz97t3wjZ2YyfuzgD7VBDS2qZpt2eao9L5c0ovBbg
SAEBS3hsoccPo3KX22lBFPMMLWaxiCKfuLVNs68U+UlyYrjEch2eyqFaioYQABXZJWTJWlyYtiLD
z98nBHYvn6PA9VpcgyWcPVROpwUAxIhKP3tAZrXFLyNMYl0HHZmsPbFTFLNIgk8zdqYujCCvUzy/
82gYMKceGAL/jzN9PuKqswWHg6H5klOgbB5K7S5I8UgKKXisNVIw0SSrSk0twK7fq7OOr5thu2yT
NlFAX+p30wHTCYF43g+GsmRY0xEZil5bOFPtnjj3FHlkFBw4JNovqX69dlcXUxWJhx261vlAeNt2
mM/Z7khzKToJ3yX6FeR2mmq9Nusigd/DPnevmQK7yFW15HciaD/rxex3qCwa3FBlqdk79PupHxwC
uFY9Fmos0WjnJB1UjAApipuCHt5H4KFJhK5r8KZqqsuxlMPaRJNa2B6jav0qQGqmu8Jc2bOxlzoX
f6Nt53ma3D3FUSaz87dcjEz9munhQP7CrwbAZunRMr0Bw8tZkVQqOpFfL1lCyJhssbJLtRAgVflY
FHBSZJrK8H7baFj4urZuiQcVoxaI6nKALzfZDiwfpDSgHmbYlMPJV9f4H5xPYIpgEBhbZmF8WT2d
DZMCJys/X6NA9yQ857M3rpgHEyk6y0dQ6lotFaFrcSwwInL+6gCHHnSyGaFluLZz+3wUlyZz/MM6
KWWPp1R2LQlHFPPb8g81jJrcG6FnRBBsO3SHdqV3SfD8AoZoQSwn4QZIZcw7U4Q8+g2ah7HCGc9A
LG4bDymH3qWm252a4aUpLQZSpGErffmmOzINY8Af3Nvn7pfFoofOv4rgBm1llWzG0fuUWAtiOH2y
pLMSVCPh1ArkjJHccA1EiBXRKw0kb2Aw0TAPZHC28XSXqEGXm/cVlFmihX/OwkJnwqOgPH6lRqwM
jbQ4lwTE9v0BVsJJt0fKg7/GOD4P3ALV5nVgItPW1JfSEUouUcL4kcPKEZBwxRE8OVS/IVTwS56l
NnDs04x0QedUAXlnPJm4rKp38TfcCKkDyCpUcJqIhZN8Nu+SISAbz9VSrJcQkCwXyQEqAaKZeteD
THE9J2fknwpBFeofKMybmzHMYPCL33k97ISCxPDoWW+wUB0a6EBCYxEupxRVorkP5vxBYKlknN5K
8AoVOkBp8gAMfB59nbtuv8jxiDFWv7gzadW/3t2+jxQiggCZmNgKrZByc1Cqc+fLXo+BqO/L3Gs1
6T0TdQgUt9If03O8OBtoXV65jWu72G21XJRqNbCcXYH7gft+0bszrJO7ejyhPL8uizU1K3CKqq4B
ExNzy2fiWRPbFdEN9VY+jv2bAabSmQic84GmPC9MRaZNZu6/2Uo6enuvIvgpUSkzRUpwp/wW4/7Z
4r3rleiWW6DcTPOyqcnLYWjYFBwbGiUrMiWWnBXftwvKtdbYEevTa+APRlhPJlc1EZsICinUJZ6w
0jNhdppgE4Wvw/5Pm+77YZdlTBeQbEQ7IKzeECgDHqkEwxYvvMC4G/0eZkxk18+eXP++EaB235qf
4RwoVTS/YUBtnfNLe97efsADn7gC5NNdvovqVcR8BMV0PVHDDMdSORQHvMcH0mm6iB1baWMDtlqI
y/UCdccZdXQwQNi5gA7Bsl0mq4zAsC0T5bM8i91ygXkRZ9DkI1pA+G7M1S3I20//p+2VHN4IfUDx
7FucACh05tLV3F6auKm6lg+i6havnXTQcgc8uii9mWagXm+ya45b6IRZvwwlaTn2k2m2rOcSMpIO
xQINhdCMRPzme/I5LzGA2hSobGfEB6sOSy/gcrO4tdFLwvGArbA8ukH6lgHHD0KPJTn3tvhLEMzq
x19GfNsdUpyeEwJKCOrkU1IhUpd+sgN7gQVcdVFXqG5k6meQKU0AVb0pPCPJ8VQTyHb76UnfU6P3
JVKaMublah+9mdS1sjwNesq+Jcmkoup6SBOajlbTnLhpoVP9oZ5XXUlC/+RUE04XwM5D9ogYu18c
rV1aAJtbhvdsQUSVELTvd9a7Yi1vQqIsgVebRSzah8TweKn3vXudZfbG/Uyv7mwhLCmMp5uc9MUv
RFnK+ncFB7PiXfTLgS6aKVz7oemzp6Y6OEbm3uXgFXeyWbYOTVj3zh3lumV6CWdMEpIFs1tURUf4
1FyKgwNECisgGKIkliha0aZXTYVkjbppCucwkV4LigWQJV6FJvuCDXwtIcLIYGVRzjDdTC+TSg0I
+MqLwmGqPaPZp0GMv2pGYflJOFfmVyZhKk1AUaC9JOBUuClaLa+fMNzjios7rorwvcWOlrqsrAoX
n50l5CKYZ/uK7geGvuZTshlnb+SdpSEHGgCaR9KHucYMhmQ8MtWnaaDyq2HQNNQieQAkBYJS9yk5
haxSM+fvjMOL/g8xgmY0kUEcoTdpb8vnG3kFiuZG2KlQZFZF8R2Pq0PJbcpGKDBa/gy1YKCK4pK1
128ZhgXo4vwBkzzpBTb/rxxdLt59xemp86SEQX5nllqUwonX28mmyx3QxmnzaZt+42ykXYWWipit
zW2MufuGUDEnAF5l1m/jCdCGB/tcHpfmYDEjKN9JvpeXwkzb0o/U3peSKng3H9vk0CJqhD0Kwie9
/H5nt8P4LGBSZc9Nk8MYaJZTAZWwBYTIa9WBFffYSMNfTPKUK/25UtHjtxfoHSnOfgGU/aR0uVUp
vncXzsTUFWMOlhInWAaMe1ngKSIpe50OxSMZIDIMdiQyol4lCm1lmbUxCQRfnVWAOXI7dqjeUwxT
9IeOnLlERs6scB0TUDJe75lBfRQiMbW2trgbjoyqhQynjIWJtM6Ok/rqjPDs/gOZaahgtXmBM+dv
orwX4qfh0Q1vE15a2AJB/5ULQZUnQCoh8hd0dnFRw8fBLDlmjwtIf0kaid4y0axEN+eDkT/POowo
Ms/rDRjypeWT45V8w7cMLttm3/D7eSO0Wbyy9nOH3U5P1rpkmCSFZgVKsCBHfHyxpj8vd1XsUCsz
x8JtRPzD/JnWanA6lAv8xW/8i7oH0GP0gAneiDXZPwZabv7CMLBlH3vnc7jQyAQbvi4Ideql4JrY
V2Ck4TiKRkQi/Xm8cfXjRMu11l17yd/Xiughjhfv/MH4JmZsYkcKptKc1R7gMaLbjsSHApDArgmx
oEOwa7ABdGrAtlYjK7bkwWEJsSKrekH2XbYaWWPksJUZ8dkL+Bl+3qvz9vcUiS0UJYv7V4i90xH2
HwpiYu7kDgcJpKyUo/MahuTjj7laWVc7ohUTsOiBtA3UTrUwyuM0bQUvdiCAXA70qSitaz5ZCODs
SnusQ1MlU8KseyXb8d/ZwKGmQK9Q0H93yoN7kssxgvv/Upx0qKDhCC4wa6hABzG4RNJrauox/Ra8
Ol2SBRE+OfRQr9SbFKUsKFhZ7gyjFuz9ugb/xGxh/M3ldCuMNeAL+qyqfg4er23REAFabRQe1YT7
3zQoKNdDPCPp5Lj3gqqE9k/pjE+NaPKVFRAle3VlOEEra+3h/QXnK0PwyG0guBw20akdPbPEtQdX
jjPGNCwbLXX7i56+qo/IlaIxuC6xX3yE2iUxI7KWlQgiKyriDlxv+/BQUCInxxfCFFGkR3U3x+gP
MM1Lii4PJ/16h85xRE0fUnXMvgweVa+oSxU0vu8LIDAETFF1Pcw+SOFDue04AEyyPfNwQIwyHRrQ
6MWq+pbsyqkrrhunZziHeU1obMqeL/k4Fo/QShB0KPfuLp9D5E9is+Bt2uBsPYoN4LcCccc0u56V
9reZOvT5erPlpbZxkDMy59ZEsCpMELN5d2zQp1UulDn3YTacZDppQXlnV7Rj+bkIRaMO1us7laht
Ij0tCC+dYHmVb6OQAcHqr3Sk8modFxY0DlcLO4TB6w8MbwqYnDXlrwvwbdRRhJBOd70jtDDbVqHf
bxQKpgUs6zZ8gR/Vgl7qrtsvBioXuegsMJsCFT8G6Vy+4RUAzjPst2odYYzzyVGDD1EiwMMHzN+R
T4pWoHCtGk/GyAEp0B2rx8shg719MXXR0khkJxYwcSqDvtJcQm9rNv6sktCLJHhYx9DePAFH7NYR
3Z7h1E/WCcRsCydRhJlHYekPu7RithxcvzD2BWcHBYwFY1ULP8NmDTEibWNboaPMq1oJkgumhdjS
mGMasBi4CFLkmzgVv2yJWoeHADc6eJ9YNxBeomwgR9uGHBhyoDu22g4UjbFEgM5zQjq6vFK+OLMM
EuwcomKHS41bq5+Ai0CYxxavAU/OgTx/ttN3JdhD/v5clWLa6kH8Lqv1gkiUZd8b3CnV951xOU7t
YHiugYJ+I/QvRM/jOEEdMp6iyZ90kMwQstH4QXfR7AXpsQcfzTzGEH6AET6VNRVg+0gIzTfgNoAH
PNUz3ackarzNjdKsZ/yPwT3dW5FdaL1M8h6njnGccaeBKfqXS3yYHMiSqfvMBh92OuFEboPV/Dag
ZdoNseGMVv40M4HuTASCwYhb5+gpU9reUzlF5DuOzE8P7OCyqStvgXxYc3HS/5d+z2nLkljdOazu
A/JdaIcl3S1FUkheW8jE7XbVkaamCkrmWh1H95JLnpNXHfX+I1TzYCvO2Qgc1/2FZ6uUGygmW+w/
bthZ8BBD/PdBbpJ60OW0bs5oY5s5ifMf89mWJuhFKOv1sWtUjTIg/q3m5AfqPATY9FO33U/Qfs7r
iWbcXgztwDLGpzfZQB+BDGtQx6VYr4VASsv8TR7ZHjLuLZbKZkJfLI8omK3VRFtQ5/0iKErD7403
3LtnvRd/e8gEjL9Waz78QqzsHmmgtrK8ogrsQo8JokylBPFKnqKj2H01rt2z6Zih/ywE9SvnyXAQ
RB5sWCO92MvLPUPvO+8RVsZujo0S1MQb8//PHvlIM9r1JnHZGCLYwBM1/Ba5WBAA+2YkCYVP3wnl
tYZGXqzdjHIZRpcPMbKd5YsKUQsz/9mZWLpa0bRj+YMynzGMb2baV8pc+oJjjqqsi587AEe//rmf
qCW54n82rShEMEXek+N4VrR4I0G3bzQ5UQPAs0LqNd7ejInMbh7/U2NuykwRgcVuFYQTLb+ItOgE
jcLAUjsXi/hDGRCqiWPvQi3J4/olFq9uK60T+E3bem5oqQWlPj7FYzA+gSp0tyk+Y+eeOPnAqYaz
bdgx6eTvn/6akdK9tsyr3r7+extZB7TmzXTegGs4VNq5t6wYTiKGrk+cWHgZjaPu/DLsrnXeYc3L
tavA7rVJP9LisIEuvEw+2fVgkxZ64AA7OJv9VHre9IvQDiWLpRwEz9jJqVo5qV2cAKuZeFYoNWMT
3k6cds6CRTwIZoSenWokdkmvMOPBJZSTZJvcL79olPy1mi10Wfx0boiLg0dwJIlkWBOFNac7+JTe
DQwucuYsnQSXwBXuNPYED8PZcnMZzGNzIxzDUnqfILoCeJcTgMv/K7OsATHlqyvY/mvJvzN7x0Rl
EDrUkC58xdglRO+DdddZEE4RuNzkCNaD5tsVeUgiCYm92CHP+OhHg2sMUtrCLSX+x3FJmqOepv74
+OkQ4uWJY/GvNI8E4/IkAy/EzH7WMeRverRL8Pq8yBGdORFTgAG7mH5wQIgE8OF8WUJTi/NSLJbO
mBZajCzqcY0h3igOesoRY3DVr/u9DNukI9MAQDhHunqdLyIZfCKMLaA8jyw2INvlpBmowgtMIMOc
vbLle65EHEQ9tVJo89nTvRZNgkfI4kjvwNvcwftx7aJ/SJDcTSeDyXDbMHkwWnxIXcjAUa+b20F3
F0aUz4Ujq/ahONhi+qr3jElPfRCG3YcHPnr7rD+TxNMq5n6WrwRIPDdqL0W0X+6AnmxWFRwJNzB+
vcYqwYK0tS1bJOlFp8jhAcQs3KplzAMOpUiuNrrmqP9Zqh29Kp9PA9CbJIt+wfekTYAvdmDoxbR3
eeoS3a4XVhfQZSDPjfN/lOio9zqeFlX2ZiiNQceQj1A8lk8sK8wlRSvfzXV87OSW+c78EvbhHfMf
41EdoLjiCPWN8Oanp9O1w740IhnyOQbCNDhETukjrNg/73jQHA+LeQyBRRBRNP8sXfDl63JDlSkS
TlMa9UcrT2DyMOzWNNH/KY8Y4Syv+8y6zR5mWVezEQNxDGwvYDN1nZ3pczNCtHPFpaULl+sHvZri
cWhELnrXCU24YmAPmCpVFabR6RAlyDr+hDieVKVL2BSyvK5oQ18n7i03Z8ekRwUkxy0bJbuT/x4T
cC3GaXFVdEOiCpVgTOY/wxa+ldmSwGUn8Uoz9/T5g3NsVFXXtUnhuru6EvBZLFfsLtm3QDtp3QnX
gqOlYbVwmkdid/KOU6cFL+RSPmfI0NSxF1yJmsjlmHWZUXP/116dUlgLSuTEjWFqdi9hVib4ZIMd
7Rq64JuwQJGQ84pyEnYfBSsTVesnps9ZrAo/W3l4H5lJMzRgQcUjnzTljiOde1P0l6bS//luY6sZ
mh7S11sHdwVaLSwyx7DuiPsnJpwjLXBqu6NZgSNY7M1dutYzKk9n/7lwgBqDkHpmmLR+nnkg/U5B
Vzfn3jXcdKRHcrgpAe4HAwC4oPV3o027A8+JTaIYYpWCcf4VnzhDsv0sBI7sp2lD1jW8lZVyy3nJ
1RgybvS2BKcIMi9rbZX22Wvz0QlYLE+LbCqoGL3fxjhzY5tD0yiL3U/qjW9cPmkknxu4e2hJ172k
1SAlujbAlovXJrf0MbiHeAtNBV/AiulGQY7v9t8i1mNX7nMyHU764XSveQYt30a8R2AWpAaF/jVb
gWn7+yIioNMFE8SwH6mN+zrRBZBSFOEVXuCTS6f3cJrj9wYKPhqM1bIuBBo2Ahp4WYaeHgMn93kp
0rSw7aotzSQDPEToOoCIsGfFNtklXSEjbOutdGXHdhZggVMygVLbtpFsZcLM5Qh4qjVrEdcxg4GX
QI/x6UfX3Wu/3aExBgBJ5h6kO0hzNbLwKlAS+b/Nu1KYdWFF16MlSekoTTpkwg3k6c/FcXQ9RoAn
U9suEPzACDs2UyD+k0a+ToqKqS91PTsH/OCFLw4Z7ZZcvJFJ8Hkm9lqaLBWBrDOK59L9wfKeN4hR
T+yJYtGulJ0IQzNgmKrgS2kpICO2Q3ChZzadrxztPZCXpBlGdZH4t9vTP6TmSp0kMlUztb0bMA87
kgegzFPWD6oMZQv8CoZf9oO2B7dMSK4f6Lw+QjrDI//T54/T59WYLJHXTyWsKnj3USHXbYXCU6Qs
GA5JnH5gsR6RWjKd7o5f/223lLgVttLWXZvUezfrQV0KGyfmwl7ZX25JPa7MMDDeFpsSKyUpbtki
gVhKIsmaTAC/8OACBqZLhrd0lZzlNoeYT7hK+0gtdd6gQ71H9D9kiB6joVB9kuod0YWQEywDFWT4
THHU5dEHNGy5E+j8H9UwuKX6r3+O/cRV+nkGg+wSsWjSJ5FbcEaTjeTI/FDymIiVlXysH/T47Nct
U0rL2XeZ0/6H4/Axw2RZ44UUc086s+YMKfoZjr2t5TEt0jpBqlhqfOF8haEi5JMYS0u4+NyEs0DK
2QgSOGZ/d0cwvonpO/rWBUcAP3OGTav8+DSAlj5IhToKWF0cr9K1gthipjxzTG2gYfe7A6nEeb3N
yJnb6pvd6kzWzHkn6514n3J9CTLSidWLMSICXjogaL0GohchXjU4me8OwPN8oMcWImvA71mf6x9o
Oih9xDD+Hi0mB8H8w/gg5MEIHfwmqrtCKi0Q3YKW1HpR2+Eu90025owpoRufLiHdEpmSW7gAZh/I
5+wnhR9qSAuulxO118hATTy3KodCQ2/vcJs6cJ82ZW7QvKO4umZEBMsbHgEClM3YWYa2XfPLiDP8
sBqs2Yk1e4XSKc+1u+cC16aB/TTvfdBw0mTRpvswAPv3Iyb0Cv4gxejt6WM7nj7P6drIkfK3sxCR
x1Ee44gvIKIH7N5urpZ72BoCBdDw2eUWeIuZZULimoeKeAo5p+Y7l5pFKzW4JvhUms5vKNtqk+39
gw8adjDNtbGc5WY90DU8DzblBOlzcNfywLwIwrXUuLJwiezrMRW6jlpznyZTRdJ7OL6wgy+DiQig
AVoYU3kMD1ZdPcYSnEDLt3MfNZD6Z1bhWVOLYqbaoQzHGCjCctL2ZgR2Nfg7HGXNI3CL503rQyga
VGBWxy+Jf2EOfsW/31kCbSbW9+mM0MAP6zhQE5iZM+bjK5+cWFLWM3eWaon/osr7su9DXD5q5CO2
ULxewk2RFf6SLwOqLJQy6YsToLyPYajOau6wFFIExvNvAG77V1aI3VXO7OyqBCRJjWE46o426leo
M5AH5eeoErT3i082YHNYTGto7VqPJlPR/xXN3PwvCjbgL+xXKLR1NE8ZoEk91ncMLSTOtbLzNBlk
X64PTniznUOewz7McMXs9gei4eAzZfW4idDc/VqsRy/I9+7DGZTxEVv/tRLNp1AMWNZZ+y4JeGD+
BkEUM/Jypaam1QNIX8WnyZMARDI+BZFmDX3kqq0iZ3MwmyDrLrdm7WWUg6zn6F7R9LnbR7xG7C8a
PHGWZR+viabNAkbZEHT7V9S4VHXE/wXAC9gK7zvbs4K5Zb2ZDz0rvMlcPEs2/yjAIoaYnisHrPb3
FTOkNB5xZR041+4rZ+7KLqtL1gD0HMPFZR6WHRZhO8jQzlzBhpFPpUxfC+pYZNbGstU3L+2zAbyF
64oA9fsv5QPghmOyNX3gQwDECxi2QGnrHdRtVvYQW87JAgK3NqR6tq0Bx8ERfVR0DMBWxnUpI7Uz
C0aLHPgmEPlFIIDhCCbZ7mFVdRyErnn9LIIqbuoN8ZuHtZLitO+6MZbOo9rF0WZwVQsXlPp5CcNV
/s/Hf5Jq+pr7ALCPNFH1IHp8DWqP4Lk4RoxSnI90FoBt0/fiNds9FPy7J0FvAIxTd+sMNHlY3XHG
jrg8T7ifqmhMnc5ZJLxu1To7JmH3BrzpywwzVqOctmAWiZ9DA9RJG2713MTm3zWNBXnlKab/H8yh
etNbTRtS3kfvxbkCofgjCXI2q1HgGb1x3ffiwh+ncIgXMbYb+ZUQGeI6ymNJe9+TXqj+miFfPMwO
ej8GecdQURJkGZ8ewJOKvV31BxXLcEIEc7gNFyMmCo60T/Ev9Lk+23RMzAO/SKZQRrCPxFXtaCth
k3w+v7UkIAMQBskZsjyv7JEz1DOp6bQWraGUC+ngW/bs2ki8vNeeVGBxv/ELjmCgoqdoyoity8bs
AdOmgyfhr6M+D8NoZQ72u28M51/CPfrt7lzFoIcHuK0eV6T7sjMZO5gR/e6MoYhOjaA+Sl2GswWS
iN+I26tV4K+xKs1NpjvO1vzsEKt6Gm6rf1ax9E+Mu9oDlBGjAUj10Y6d+0l7OsMnX/9stT0RtMNs
E9hdkjI4C0+nRawoZOFDZb3XKVT8a1QOiO2kfIPPGj1egArqtAeM0CACXKjPqnMfkRU34+rdZMjX
1kLdTJqvr6E98KWAYji+TMK6fEiNo4FusHZ9LKFcc4O0dhQkWhDDDjr5GLprZDH94B8f24Jb+yr+
Eq07LYvlrODUchHibBgkLDP/fSpGs5HbHaUQBCB1GJnPSB9TWnJ1oVqAMea83LxnLY6wl3rpoTtY
O/F1rnhRGy+gIS5GgcFHWG8vuVqJQDKfSu9pHbpRJTQloepNyuofYhTKSUT8/H7EMZFMdoSF8H/u
EXISSqdYQ6o7xAg1uezpItqPl073AyBm28TViQ/JxiYb4D2l+uRI9p4W8X34v1I0z/xS89WkvR3/
IhHZSr/KNUrpYObXHi4sVdRsTdQmvOv1Nmp5SwwEFofXbukzZ29GJz1TO2F0Lp5RSFJeYv1DpF/i
BOQshdgYVd2Rhr0UM00PUa2CmOBDRTNLQroEL5/QOSJ5Eqdq2SOM0FpdxLgE5dCIqWmdEeaHwpps
Yol01gFj7/8zeV/cANAGG7zVf/ZaKly5Kim4zXdmjU/9sEvLfHto+w9RlBMpbwB0VH0LUVxKtgut
QoaqMyZRr9S5bD5fFyYlkSu14lqbFGl2vWv/u2Vxu29RofG1rwWhEb6cbBRBb+O+M1tYgZPkP5SH
5W2QEAhrwaqtEAw8RifZQ+jXh424kfcPLgX/rksmK+eS0yX7fzjxzM+U3LS6L5p9xp8XswBUTMma
zxequsHmYCbDvPS9HxUaBdlHiriC7Oa68t0HD2g+jMZlzH1/pu3CCCXml27/8CRFTIJuOS+x8m7B
nstNc7lkwJE9lelfvdVoZvZVX3k607KtgoUO5rpeuyloOBsOadvkMQIW+YYPFKnjkxhvf9uiTSt7
lufc0cMFSkIf7hbewiL983zlL+ZI7mISmP4cR18lGme28183xGButFfcEgKtec4IW4fjlU4TcZLw
cBc2raq5iQUpzyeuwn/kieN2rguT2g1/Aqcpz43rmAYEXX7x9ZzDovfQnQyleAncdvkTrg4smuOm
lf19A+KGS+2VCa0anPQRe485vz6pmNMwzOFj9jxojgvmLl6iM9AAjuD8b2XuJdJS6+IoY5sRGDbM
ZPVbeMhTZy6Jy9FurEiSiTjiVGY9VnBVi8tIfU4tC9XV7CgYmg9ssqb6hYbtqSF+9xXjBai8WtbU
ri0T05hbF2kpbPnqSckgZdOF6gEtA70v1fYr1A/KNkivIMC4Dqqaha87+CeGz28JAtbjCYDyE5ix
1qWHi2EKApRWm2sPbp+yQBwHqM6BTya71/RrbubAxDvbbrg0rF1RI8ONWXD0qMDSIubzo8Kjujng
+kDKPSQvBdwq7VGNkQJw68FFk0RtPECUmwarLFwBK84L2QSp2dKdXZYKC3B7F3zT7QTVLEtAs2YG
kqPv9Xjk1Em8xGZqu1eFfKa1O9Udct+/nRs0ji3Fr149kD/kwoUx+HBP/kzAu3AuQASSvpnGzmv0
4Vo8Jizrsyu1lPi0S6mutzCALFDdeUCqrc35vobJ6XIluOb5VcyNroAw/m6t1qvuq8PGTWY454F8
M4JEImzAfYMjX8TA81DihNQeiOFHszXeeQr277zkzs5+GAHB6Wch+YKeCBLl3iXt4BeLQUkUQgR5
pm3vpJRTu6dDh0bKD6FwvqXr7cy0xwZJKwn00C45kM/e20VGrzbAJJZcFiHWROpBl+/UVm+YK93b
irSZEYOL6JGcXBy0+qIOhWfhvM1qnV8iVEDJTK1y7lT8zfNvsKlx60TChN9dbSCDlSeIx8isoCb9
fASyKddYXdwS7lIgNpaqoiHluFdXRbGwhYcaVN9jhwB7TPq89hRau9dSFWiA5qJioxAl/0CNZake
2uszE8UROWjtjas/KZ9sypmQcyocVdZmKVNYXe/jCcNkyyQ9HTbjH2NUWb7ehoFtuhqHf0puEYll
eYjXTnvO6xJcUv6mRSPmlBtkeAPsO4199TxRPeeiEGBDA9DnssifVBaaF7W+C2ih98rHdp2HR9xi
ZCAjjvLJjvyZlV06bIW0OnvUTHo7On+7A9vz9G/swyfx+3euxaEKnzzv6n+MlTcpy7QIh23PyZdn
/88H5v2M03BBOKpyxc7mBQRxCbO9azbBxgfVwH8sEM67iXEirkgloNyht2TDQRbOBmeNLs3qkG0g
oRHX0BXNvWOK51eHqPMnghu8e/XeCrsH3YEwJnrTgx9OzUZ7sBaooxC/a+i0tvDt3eh5p5PEbaSz
Gdh3a6xnEpVRhzoxYPldq9242T0N83zgLvA/JeFPNv4vVBJC/Y467iYUCPZ9J7ZxWy7bZyUKHKsY
mKz/O13C8NEfTsakMWwqyTipnHIJ5rj9yZ4uTlbtKQMbvURhhC47FtaQvYxsEBaKfDCHyvjWB5Ug
EJst7vrzOQtLn+Vyze9M88ILsgqdOtdMGvcnfjADvmyCcEe3rSLVNA4pLOS9HS7a0D3zgAFnaWu3
4q3tNP6jRhpLppu716QYRXXlNZGgWWGzaW0cI/qH6EGMDSPKcZ6/xWWtAWxUF2petwM3eyMTRZ0H
7tfknVT9d+vdbHj7LV7/UXj72vi/hP938tQ0oJnhx37/j7zWHU8rcin3R4Jsf17H4B4j6KxnPpBD
36X/uNIegMH3cztP9HWy2e7dPkZfutjQWmugM283UHGvTLQLjBkn+u+Oho6bNaWo9oPBT0ErUmd8
XPrVU5WgSd80unZ+7KEnVWf00vLzucf3tqI6P6hQFJOH/CiPZ8k06DiJ9xrXPqkSyUH6JuiyjDdM
PIQk3Q0n876wHgJqIBEcmHhUlfzQfgM9HsGezx9894xipX4P6ciJ06UXRMdoR8PR7yTeaq8fQt2m
23x5PnoWsPDfyTMv2H2F/Y8uiJ2x5qcneJn3TZ4Tp0TcFow5kjA5AE4bydUJG7W1qcQjn585Drvb
8Geq9a/g/+N9OzoSACElRQSCZRr/RRfxMyfum5rEDzIcm989KsM6OwXiGwQ+o4LjcjKhlENOH8D1
fAdi9lkTvuzRUsoYCojwqosZ8dAIUwIVRLwwMvNwGFlvnQpMH3Wkv2apLonOXvJ4ey0qfkB2H/g7
CxP/F34M8QZCRmPBqhrYBdbMY7JWbGbcIT9CLYOAubYksIPh2LxLCP57OiYYq1Pxbw+S1HD1wLU5
s2/ANPAd664rQO7Mho4ZV+Pf5bTvOdOUTldw/hNuU0fUwDdtaSf7Xz5A+B8/oqU+umD6pERD+07v
p1WFzaQSunATbVie5eM+V6zofP+WEM6OxXaApETCJM1TznoBgVixGNTKy4/DXoKEb/2jQDQaHxJw
V7ZeSP1tPP1IKwX4zbfdc0Su8aCqU2HMZhmflpL6H6P4RS1RFibyL+iRssJdSW2NGxYNgYz1WVww
BwAUZ31+zGEumu524u89NJRmLmJc7Li+b+8SdjDO8kk3p/wxeYXSscxA82lybecc5OrF6HSY0Wcx
73D5RcuL+G65ONQlttN5k7pGyJmNERleCkKs5KguvUV5Sy2TFhNU/yx5uZ1XgV7VrUax3VE0dBbM
rWahPKomDjymdY5yxqaGZr1LCmEFi/0xzb39fgoiqDOU+4gjDFxhOA5syyYmeBffg/4Y13dGRhmd
cdVU5jQZGJKiTT0JZAE7o/4/y9VGHUlkNJgdSbkpSaRtFNZQN8bHXTEtchadg1MKHGlhw6epQqCe
3gpMiMdYebZubl/a6w6266AfcC3wMp3CpehsKzoDKq2m7fhVAdtQhf/3TsmG3D5ZuJnt6bbectad
IOeIVHwJK9FLYfsSoDVOJkiE25wzjspBA2V38ZcqyD8AxtCoMuSiJGJ/ThXYc7iPYYzQgzuJCnwT
6grDPhLABsl1wqmQY6aZUtPwyKCiO/16/cbERB4pXAMSJC9wMedS3vUGqBH2UFmcCafVDU829pe1
pXqKvxxDFCbgFHALlh44gLzYdcs21wMefdp+2H1vhtU6n/5Jndx3GzBkZxTNWFQL7lCKBjCra5K5
2SJnvJZaT2quSjFayeopQrrclq2vrA0pyNYtEPs3iQWNkSf89z0RkjYTfibTc2E6saC0VBjIWPbq
IcVg2G4MzubdPZb67kWXtA8voadkokSdfiEVVp/f7/9y1K/TS2sGn/a8keCwWVLi8p1C+6noQO1X
edIA3cv2/guN527VU7VXpQ4mkGVk8LMEKNJvq3jlkXz4LsirsQhXSApvvdciQyq6vt5NT+wxHauZ
dClGf3rQwQ/Bdocz7610nWlZjtKal90Q5nIPnQHiO6P2CVRMHMKWg9P7N/YFs7Gnmc6t2VmaUHz0
OBSjHhc5QRUxPw3xnsOnMedd+X9HKlE8qtCV6wiwkXAdWyzPXjW9c69OOzJUznggT6ZJNHnLllF8
xc/uYUV29SmZTZIbJbj5ZHC/SOlfCtslw+2zLPQkrIRR1r7JpRB9T3Q8l8Bj5NBy8j5t2Y8owuCQ
uhNreDwlJ7RPr0LUVx7WWq0h+W3aNgdZD3TgA22srgby157ok1NI58OY05g7GENbL6gGKDo4tHyU
r3BLEwRH/O8Mbb+D4c92mAapzjmAvrleVhldEKBqkR2jLMU7YChfYyBKrOcSxSKuYjVayKGNJ9qr
S6Q2V5BE6YBDetb1OEM8uiu5mejiTtKmrWHSJcoIlEegNrZs+ZcV+kjrQB/AZdtDe+t9KMjEGH8z
Lgd36n7qzfxWqXGIx72Wo+cpoUEJqEYOntLsMSCxumF0v0ITVSn49EdqCqJ7yNBYVm4fb5h4Cmz6
eGZ6m4fyDVRvIRkr5TmpFSKQWKgX+xrOnxMYrrgBc70ZODh+uGOcRLoc/HYE4j75u81S+mmRrbBH
u7v6CG58PGo02hzthmkz42Dd7xKQAsBmDf23ubVCPWMz/oThYqxlqb3x5116n2nLxCby22DT3zWj
6nlgZ2lhj4EgkVixe6rluN+mPpJ6usl3OY5ZupJUlS8zMLbZM9a4zAjtyGdXYUX7mMkgEqGPGu7V
l+/vHLVmKsLWuBM7VcL2uKPwdoYxRIp2Clyn38Tbqg900dvHwt9PwF0RcTEqpfb4ziQUQ4KJHEiT
ik8wsTEob6vvHE+9OV4+CpJDVK01fDHnh+D+XxIruXIHw2r1tkx68DLBhLS9mYAB9uP6lsJhi9cr
5pCAFIHinWbSZ6gf8vFO1C5XTwcftrMJQfUR/f4TVEZk3fgQA0D7pWtBL3NPP5HeNMmfZHJoVne0
s9WwOGhqD/L300Bbcjj8UOwYSgrLEE/B9J7yAiO/2QXMD4wpEF5rOnzpG/MHxUekn3kFjzf6dZT8
jkE88cg2MM3jTr/Qc4lkIHsRXJ3G48l/kl/y7H1i3eGAPPtPRSWIqUkxAA80N6cze4PCES6QL/eT
u0EH18NorkIYHrvMnoqoGrLLZISDLRqu34KdHTTo8vrzTPmv0n9SAZrmU97lg17hmGmzjrqMv727
Q8C8OS1LDgftM3maCr0coRyjZGKa1J1D4CgUM1p1auEWhy9WPxOyr/egT69XvH+qkBxMfsLPj8Ir
0FUyyrIFRs5KwogwunKva3M2EBWzF2+9JJJlboJJj8czfIwdOfnRK6RjQTM8qOz4comCZJzk9DEZ
/kJYBhH7vlwFoNi001Jt78JMkN2mUKZ+m1VKnxA1lk8nvWL/B8zK4ttMgwSaOZtxphKzmKbX0ZMH
nwrgj8IbZSpvmiGILoL0+PcS08YrLGlG7+eRysYaWpeUM8XIrWBq+0sn+KFwqDq9LMz2dlr2mDiP
fYBJAvSJNb357gb8cMr7L3IKSjQRnRW9RpagXV8WWKcCKBHZXWcyTU3YPboh1bg+T1J7qvj5Mc1q
D/YcUpBf71Dxm9fXyHVOPIjnmc6gFTcANfnp54ppQk3trXwazYzUQWFxpSbQw7FzE23iiPh+S04R
rY67SvaMHehMa1f9fgndVP7kVWG7qgBPrfAlLj0CUh0G4q1mRdMQc8sSLP4EaoSJNd36DEhZP2Dm
dJAvHe73trIdt8xrLpSlMg8AXbSX9QlmxjjrOBlQxj21eVmoULmMDcugpYh53x5ID586kq0ZGNmW
09pqFOu/oEjMhaTuqtN3WhKVWzT1M+RIP9ZDBR1l37I0wXUZcyYaJYW/AESHpM1v6MmGGbni67gA
j8cbUx0TtH7cYnAs/mAL1v5ftv+wGdZpvuyIKScXnfPYT3tkPeod6xOlYUNWHW+4tENC0bJSNclc
4oyyURjpELOCMR1TbeE886XkRc8udLOYPa2KH128SSN+Uix8kMY6bVq9cykrfojiYyrN/093LEkj
W80bX62EQ6byhVkoXujokGg2BiNtvTkI6owIKbSeBw+uig35mOl4M6VfOfITAoMAhhCyZnruhrcL
ejm8g39fYSvF/8byMoex8DjQ7Q0A0FTwlEEYhkYUClGhMxNbvWBzH6U4FZfxJKqBSvcz8oF5C61o
CUhg6pIwhkc72GRUI5WbaRYJ2RGB+Wm3dk9TfzPznPF4aNaQQPL6nnhF4QiGrY02j9mv7oNyDufb
mS6npPXjbJXCR5QhRxYnAYWX6moqgTQ3biP7CghZOcnfhZRHtlOwfKtcMwuWvCLSOXUkaLHtoG4R
FR/fB2EUhOLkJY/AuICs+/JMA4/cnu6f2KQjbdPDJ6QU86kmi/tOLLe5yRNmiOHnBZ9uXbGnfvst
0liFS41n8Car9AahF+Sgqb7c6c8WtjYM8d/xfZTm46nn9bXfFQwgQLvKd0dLrX+eiOjBXlgefeuz
iJoIEFeVy+y1pt+xifr8uCKVGf5AsxOBJH0ApQ0zuzXn82REFPgHM8ypiX44bMRINyM291fMoGVZ
nwor7GzyeQCcJTj6fwNSQkOIFPLMcRmkzYg8Xi6GX1iT5byrMg5tTyj+EhKYaaEGWNgQFXG1PQeN
TNdzBIsLGwl5k4p/7UnTth5lefQKkNOfaNdFlmugYB86tDwW6hQYzSnkkjk+6Tf8nPf7QvVa60aj
vOQrlR1PZIlYuOxRwsYG1WL8knChk7RXWEfkA7IKKtWpC9g2w9ivjiqobppUYJEPs6v6A7+i+Pl9
1odk1JifjqdaOwYdcufBDtJtUFncMZsGVzZ8Gf3/SFdbv6gvx/mzXpem49rhzasedL3bGl+/wjJB
7ud9m+2v50h4PIayUT7IlRc/+KGTOCMj3EL+a8/QvyQ16u3+fRAwyIg3r1UhwvkibprPb1ojpyEH
Be8QgbcbwjM+BD7nhGtb7vPtiCQFTHQE8OAsoK9X3w5JwLPvPiImpZo8IEOyUhBiO9WL3Y11B2LK
2T4OoEIobe8aXXgALrDgjxIFW9L3TsAOuKJ3TE8T46OF7w8fcyNPuPpd5GYX5a8XUF37HOGtB4JR
mQrT9FPKheDHpzoL4j3seGoSujgjP8zEjjuSyjaaCaCNjpkl62qQIxNUh0IiJbf7zi6a6nex+wj7
cfWWbxPS4qzEw/l3hcboqhti5QwcKBGR/ccUPCjwSpAYpFpLDwx0IlcXgKaYPmDV4gTmjI5VHXXG
HtY5CtvghqMatF2tbwuNfRGB4rmKujVVMyJFxMCmsb+2nAyer/reTDR/ldR8tRhNb7PZhti/ZDVY
0qknci0fR8GaBdM2YLsAC+lMhNRw/dXS9kJhRo00LopElsI3MpjNMe8xoPwRXvsl5n9bjZE2rTrr
UTejX0qbIwajBfy5sF87jkGfW3SRRxStxDH4u3pgiSul8D83GXPOOSxIsOyjsOnCAvtyU0jVOMEU
R6vhXLHDE+AkaUwsxNrIEqpn7lxfYtdQ+miVe0sIJuRNU+FetHDoFGnDgQTLWmpm/MRNaWI0OL3L
xhlD7VROp1cxao0Qk3tezvgx2+oouixLMkcqw0lr5lAvwIu56x5ATP/GxowX4ovKafkMMtQSltZx
HQ1eN9P9pC/Kzz7ff9mQm+jpMgwXnMSRhzgli2N3FaWzug5xbhFTiqEzsE+xxAnb/gEc8qalfvcM
L0EYGK95rRJuTMYkb20LBU500L1eN8OFWoc1rxx0ays5x2NB5/sakmd3yzQ8BMk6eHBvUZuE5XpV
GZLSRmTnD7o5bxWdCWRz1D80lfosMU6GRLd0FxmL6TVOwxfoQO4Q6XCd9WH3Ha1YooJiD+645kk+
x1pDuE+4DjLocM6SNeKGrx9LCVtS6tutJOFBG3aErdo+XN65pAo4yaqL82ab/OWT8toIqjDV8lSb
xum36TSfRDoMNt77d54ZetlntzHI9oMUZGcvLaLo1dhkehmNG34dS1n8btG9XX4QFRIZOOUStrJd
IXbEoSwX7sSUD3YfkgM0OJ8gIeGIRQvJJlR5o2ajuUnbttl0a8WJvtgztP7NQndE/BNT0RYydhZE
O/zRqzjIfkQ6gsSo9Db6lp23gg5PYBH91t9xL/UMn7kZYq3PVfEq7cW/2/DC0yGKFYJa7OphOaKM
0bLGVefn8KZRs7EkYPbiRzEsGaCMyMJij90E6gmv1/sl45oGnKjyplPYN5hPKMKLTdW/bXMrH/gz
gpaDM3IzfabDGKhr9G56PgH8hqvsGU1ccYVVvzajHz+1brQTe0jqtU37WyLnao/Xp+X53Adk3rwY
geo0enKSJpa79YvoVZvXPBIin/sTZAfP1gEqY32Y7yJY7eE/X9Vk8zO1I4LqPkxIhQ6Iq+NBIWKv
p+qaFfM7cNBxQRH5Pywp5oVdBiGPLGa75sv/GRnKFXUz9Ecz4GHU2Az+iTiuDeFt4Zcj7obRIUHm
jCDWVJumg0qzytZ/gh1w5LP6JAeCjT8wp01YI04IVrwGFw781lOzJBGiB5w7nSl4vx2WTNrPXc6J
j4cAIrgCKU5r74kCBXi2o/1t/uusBRqcu+FeAVV7FjpzwhcT6hlEyan6AkmtlaGJSiu3xF14Z/XG
W3CbhHEhc0F6miaI4Zh/IFHtpAArCab5b+GyADiQawd19tG4/pdqdRi6rZuLmcGhjiQ/R4XwMFf6
FEXj0aNFTypamLW2KkB8JJf5J6a84f7UX9IcBPALhN5EDjkfxUFlD5B8wjJtMJncA1jF9yuHyxCj
1C/1AsmFfZX0DNp9BdY2UfwrPx6BMZQRS67Ik9rE1qXlf9Qq895BV2k91xlAwtJKk0evxT1BJ302
nxxsadvfYlMzh3hGSMrzQJPreNEg2VpSGz1l4xbb17Zr2ts74GHEj6ZPGADyayKxitSLzPmisvJg
iIWLZqnttyqVH+/+EpflSoxbqmUl6KqcIe7XAgXJ0RWNboz+uwdebnuXV8bhI23biIhFBKQmIOXu
4xCF5O39Tnqr9aeH1lkoXWqnCV7SAuIw/3nVWkSsA01guKsGPW6VFpCVdiMHlhgraDVn6XhG1Q1n
xRw5GlUcqKlGarcM7j/yCI6iaxNmsmAOqS6y1hE/03uIJthIBWNJiMn1kqRO1EOZVe2WLUCTfRh4
8CiaN7ohtaTarO1eYhWBUJnwQgrz4iNVMyDmBNzUCaSyNja0qWfIVMFG/kXsqRQWWW8DOxZs/uEp
S+aXyjEVOnCQB+2kND5Fz3QiBkw/+nxYNUx3GzpVlva2rqys0KZmWU+vO10YIo0OMdRWnCSSzVC7
GcCpj54ZrjaJeg8cMc1hQU/jgXtpr+mPSB5DXyCx/YL6VRhUuL92eV2CUzBkBtHBMvCw8vHNBasQ
hHBFQelwnvNFwsP0UFmej7ysQ+4APl6+6tKXELUtF29khLb3GirlW6uD0xa8ZpF/RtILARHO+Lgm
Apc08kKFjY8kX2MEPy2eDhaX+oQleag0/UtqbUOeHQWgwBW9if/+7xDg+BNQ/+A1HJDRbo05L3Tl
6N3yDhECEZa1/cAtnKPvrvm4lmTgNXdcYawH9uvdCIDOkiNH2GVoHuwWDxaIjDHfqt1c3ZGFqoJS
KZWhPADlxTgTEkuEkbVCF9MhB9yDL3WNy8yFptSDM3hkrhuDGUXGHWq7O54TFTliQWZOoqpmOE0d
lxNpjsZ2SN4JToop5VFLDfw2hVHstIP5cEMlDfhQSxHmfcZVkdUNjA9zRHfPtvPXBUz3oD2FIosb
e+nEXGBzFxw7dSGOWI8FytaGgIgWp2ia/nsGSXmL8D5n766jajnBFpo0oqERMTI8BdHHlBspDgMg
o3pVLtqA1D2JLC1/EGTu69wlbXq7q+zuLD967rJvk85L4qPfAXaMcGDSSKOGk5vOVwazN6yuzDSc
jwfYyg4LE1mnyuTXI668P0kxLC3ZwqVrIapxEDChwkI20keBmp0CmQmtaMzWWZ1W4MsnaeGpJ9jr
SZQ1zMxCTV2yyA43w2/G1kowQ2upuAShq1Nd/WwRHrAJnhFyBT0tjtJY/QIjIfz592V2fz00qd21
goq2C10GlVn0IFwtVeWOBLxYKDaZh89UpHJs5z6MdLH+iISupdvAtmOC05VzcQ2q4R3H9qVqgneC
/PCFBoTWsg+gsH54sdKHWzgGYkY1hC6SBJXw4Zf30efkMmFbHdBimD6U/UyzXD+W+d/5Q2h6Y1dG
3kMcpzFihP2c4snO4yP+YLO14+/5aRTPvjeM7e+eFGYE8hp/YvZG1Ld5KJS7C7xXjX/cfQzjSCll
NsNx6qZERXqDM+y4g81TcIMpZr3db7kqV2DkcPlO4qwVksPs/CMnkaOUJs7mRg/F5Bj5llVxHsWM
HjcNi8G/Nh8QkOUOLRHxBc0BuY49uC88lKa9NJEvhmKt3aQYYA7T66wIQQt5btwWBEqNjLOzbQGE
zFaA3stBtVLV4fzJqOMaySneP/mMCvCs3jCEEa7vjbDgeOfPSeIsgS3zwzF8ICBDjVehLyrd65Yr
Y9S/GlMZUEWjUXYHPUYLAP/u2BXj7JEY4KaqjInoPvPVjKUYLZ8MYfEQHdPjv808b4UyQRU7XwFZ
KqU/j2bu8ZEhBCw/rE7P5dZz41dsVVnBSeYRsEucY1i0tzoUcXWp+BpMHnxja8eBHFs4/WPtX+UG
SMHjXBBuvHcSyyrE3ocK0Z/f+9fsOABu17IEqtgPpS/Yhd2z1afb6cWANJlkpW6s607HjCK6qij7
DMPTz/rfa907km57zQgjMJQ32oAYbVhk3ySKCaXPJ7ebu5qE6eggcORnFvkQtllV/efM/4fhtpRn
k5LKHH1ipjE4iWl18/kluLSuH85340MwMriTL7zKZJDofn5EXNXONPJtJJxgaox3RLfW22MM3RAR
tL8Mg67BjEXqiGEAqdfaNyEYprHUUcUdp8yVoJHnTItsKFS6fNSPPXMv9THiGg31wfT+9sZ/dWQQ
tRSm83h8YSjjr9U8Iu5AHj6v7/NRhvkImVW0fH25P7HP7Cz665Y9H4MTR/NaiPhjv3viCaqiw/LQ
KgG/SK+wXopT5ka0DOarbqrI3AE8H7HWxzcTfqZc3wZvnh56PQBOQgyJffpf+DHESyGsIGC5FwQQ
IAvx8CsEipzKnWxCxRkCBE4aaXYgdK5kM7dZ7PnUTg2QjsPrIKkETS2jrL1B4eIvalJDqRdql7Zg
9DWxUOBpZKhIBrKsiNraKT169yPhnq+5HQfnXP04aKHmpHbPpPtWG7/bEN+n+TAvKxfApFx8Tqen
zl72Zl0xR50k8syKikbkVzYuNHhMfnQ+uqHPR++Cys60Nxy0fn2NgjlluXHWij/d9zg1jATP4x6+
fj44Xz6j92A/KWgmRyfnFWfhlp/xITziVUKyXGiLSxXAo+gi8tq1zobTxkfBO15zQMwOYpR5X0qV
NTEpHmK3imhoIdEpGrYDSNUOppwFmc53dzT2Z5/8oX6g0YnshzNt52+54IG6wIzbK02Xp/G+4Uai
bexVDESaQAtghEgMDmPsawrmfM5ISCO2XQzW/0pYpFlwCEcdGjDwC2iZp6gCbU/Cy4joHJb1sw1X
lzda91S7YgsWSP2djMweYzIR+lbqFcdOOH88rPS9rNkE9LKiUKcs/cAbDBwtQmc9L3oXKISC8Pox
yBFSPuTYGsFrXTlqJRi+9tMmUTNzvUsE4PEg+/i3VpN9LnE7I6Fh4g3jXU+w86bT4SX2tlefyNAH
NieIziUDyxKQu44ZglIxUSIOuTgbW07Mbjax/y2rXtGmRYsbNXLII0aC4ACjvlU4tcvQbtTVceg+
m6YzCaoSUqmxznWJVZyjkF75u+8nQbWPMrSe3cbOwMXYmOUaLeF6y7Fot+P6xfdILEbzRz7jFtzI
B4SMhkZWhy9YKOIqvWoWameBRv6vi0W6PPjJxV0zP1dJkr7lgKvweqAPwGgsCTjj7EUs/R4njfLk
8Y695Nn1M6/aWcWeXg1Fnuay1ZzElIjYuzabersgfCYcEA8aBfpVJVJzQWaGzZb/ufbAZSPpbA0j
wdjgFWSV+RCUwIJ8YwqQA7ri7nHMiOQDo/YFoUzprd4LAoitlbMIo36OleoVpcfy84MzoEUiOHew
TTW9r6LGC2qT6YWaLJEjrSKJL+kVsqcVuxPcH2oPdGcn85Gf/vs7XS2b4zblBCW3XGSt7ULcQBqs
4YoEQL2nsdRtPcsVNZwB/FmH028gkTnkSuYTCkvja6h3ltw2042drEbKFyNGZUepOjBCQnzdroIS
TbUAdWDn+p/Z9QeqXTj28P+QQBBhNTOCWITZ4/y+dA04m3KIRNy91D7wgKwP/Ouj6CU2t5WdVv/j
lUSd4lhpSXCONGe0JxEVY5kEdIqixOjw5kU6DMMpgrJSXBkllKSKFslarurXQSZWLCvxUGQ5zNM8
DmRXY/8NrxfYHJenXC4GVxWM8ZEFRsEsXQ+RgxPkVGKDwI9zOWu6ImHo4D17z5vqxngaVwGthVOd
QQm1MvsjmT5JJJv8NdMGqDD8pHApNNYNI/BV68UC/RuzSf6S8J2YZA60wUMn6GZRUAdhAVFNHRjO
F4rRaIYLaR0jglx83s4t6XCyA6MkHLL7BBRgiu03IB4mkEVcCvUYFryj6EXceyHhlgh6WNf2EK2G
1mtaNpG3lksSnQFfHygIoSgCiApY/o299Q9oOM6KzTobREqbebNBk2HZVUN2J8Zhkz4k5ECGo1cv
fc7PpFx2p5g/Bij/5b8GFauSL/jSNbi6QeXaEC5/2PQMgPk8FoNIiQPepkv+qOtcLVIUr97Xfh+e
qbVsndlu6CAJ+UqLI2ssYoxwpg6Ip1K7zdos6RNyvyJeKqTXP6iYvWLRDAKKfhn58fysIRVn3Nw1
0mNfbWPe4VrKRDcjtJA6E3ADB75B7Fe6rH/0waijS/dM5p/CCxadtLWYEQ0JAdG3lEJmtTsRlj1U
DK3wL4gz72dTguCreCRzhd2BbtnzLxD62nTTDLhTCZi5yz0SXjh3nVGkkyH4G84SgjPkML/l1t7D
nQ63ZzSQvxs+JJjdKiTtkAmNSfPucNMnZ+TXCNYphZ+UefcNq2hYjhX0ebZvDIBYot0H0qwQpoK9
ui2ZY8zMDQdr9OCcX03YYTrdVZM1e+8cB6MhoqGqzfxwSCmECfNUIkGNIGuymSoY62f+qAF0G77O
LkA6m4H5Qh0NpS193FZpBjJm3A9VP7F94JLkySq0Sg0rbaB9dZrYoGZTczIV2rkvFXkRr+bL+5UT
dhnZmeRb518ckXuodjpljnw+gMP7l4kkFb8XPHcfIjEe0Xl2mVl8eEwn0jsYfwtX0O7ckhC0VF63
BDjAm2meODPURTPJ688riFAkTq6KKyBhKGpRcJnliQ0olvusMGbxK8SruNMbRduWfysclbFILAkf
mdX27CmRwGhBtifMI0/1oADGrGoQ1Rfy80gQ2tWIomKboPufCWhvdv/wNDgJW2+PzCrGA4vi+yRk
Etd/64TddbzzZ6l9ptycNpCNOJP3HGyePg8QclKj1KvsgNVe65/ksf/h0swFqvWaYoFjyj9jphiv
Vx2B5Vase6/rIjL0a8md/cGElrsrtbJfqek+E+uNBeA7C41Y8Lebkh0mV63xTo4B1C3BoqD5SD7Z
pgOp8eSUdKRpG3c93U5/R533z4jIx0SSfBq9YOEsYcSYuZessLwKo5nF9YGwuQ/289lxk02GOCPO
qgVlnyFtJA4Du43CIMG2Ubnu/KyNZqyVwAqwSuJRqyWz54v7iXGPAHtPG/+IMXcCy8o3GP+IOLz1
O+XLJ759NeSvlIy/QjKTmxVGP5sc1lRz3Wjg0lWrcpTENRxoWAR0RhWG577986W8tmp/pwC2zX52
dLa0MYXgJ5pakO7Lj6Ofl6Uwpeqoq8/Kfi6CPfdHLBI2/MkZi7RCUle+Zg6cyHE/OKCsNMCpEOTP
UXJRDJjZ5kqmhKrPDKxLIGSU6mGZF2OWOXTOX+2kWxZpLLz9pgXq78P8vnNfd4vHLF2ESfS60bOg
Y35fS4U3Y5Zl+7jJgGpc6NJmziw/COzSh/iM7gDy7Y2RXXfqk4NVLA8BAuhbDBvyP1bR1yYZtNag
OvbE9NoDBxBaFKT8dcfjcNxkqZpc1P470XB3WX+kssVRoNu5r+G8wQVr74PYaIWCuKX/WtDxEdR2
uM9R03jE7fninvLvm/QtXYv4DHAhgyyiEoJKfu7ttn+Nn0AZr89c44aBQl18xCGel3hOSYjaO3bT
Nys0hsw+QJqWIxnP5PJB6YG6aqy8szJaA8SS4RKSfIDafUOq/C6MxmFR1okZy0fSKeQVBFLkKcPb
8Id7T+jG6UYjGON/dNl2TKAoGSFFhQZrqlyf7oXQo1XQJTUgMTYkleod3k0EUFfb+faXHlVW0s+w
hOYBAdrM6uGoB0tGf8N6mAZY0ZajjanjVi0eW2CfumIFDKiPIiG+qBWSabVWt37FuHZZ8bKlImVK
1NrHuGukg7ZrnKjEr5Po4d2KCR+U0OmktikqMvo0bCmsC7f0dUf8Wh5fnb+cH7YoKVoI+BovYV7n
SUjKO40RISWcTlsEPPvHnFdRbITFrlofhhcmLps6IpGY9/IpXIYNWs3hHi8XWpcMUIunXhdDCszr
z5Wq/Tp2tYs/CcQ8u/bM9jZe2fgl45cvtVzHaEQiiPqZMjjha/iZymUErCiV7rHabbn7oTyGXwtl
HJ+VUrZSHxvg3r6vvjiJYX4sGcYiWYWxhXkUFTIFOS7riMzaU5i6z79dtGZrDn0AXOyRkxmV5xHd
+0Mqun0BVYRd9gF6uHuqdC49r+UtLD1TLjT1n/bymn3xpIz+32oMxWHVfly4YKHY0qvA5+tdWbfF
WFeQiXb8C+tJblhfmcnf1B0R0usA3I4nvUX6ruFZkY4MQ2Ge3eJa+nc5vELsYiQSewIG0eBLoFJS
BR2npIdd2NBzB+vI2vH93B10WXAYhomy+GeKhwGhSwdpJS1ApmjBCfN6wrYMZ2nXDzk4C5b4TWHu
e8GaX/3FDCTq5sCXdaC4kZ+c4mPNXPyn538Tq3dm7pW2kU2dgw3bR4lLwPtesg9sa2YJimZfe5dk
a2KK3LgYKMLydL3SkIwGuFkMnTNVL2rLlItYcRH1gTKeNBjxhEYBWOhSgSKSWuR08i1jPiAZVFG5
uM5CWm2SWpdEAaeDT9qrvRtAjUP8NoW5sorRcHn3Zlr/BucwUZdIk8hUJ7I5z+wtH4w5hXGZ9Rj9
59ZR4zJw6x70C8gQy8I24jBwAaQacb1c3dVwP672jNDSMTNiS7meflzzNvdC9lEPNeKHxNNlzukh
b+8q0+fOS2vK4a4o1rCfY0DSD4poZnFeN9ewHWh6dcOy4pukfjUaCApDsOsLvWZYI7R2zve1G1th
AdRpBcAziuysVlGtFVZUp3G0WFCf88Wa4JdhFCD61Lmsv58RDi4Rj2zuw8NojyK0Xq7gWIna8arj
0pviRcuK53PvDeupXvBrdXXFqp6uKtaJMtWUfmyNwQXVqFXz3DnEb0mG+8/LsDMWU2ZwfmCQUwh0
rnXqmEL5dBen2xQnuyuZ+FShdY4OZxD/t8ZpiSmb2QuUioVzqIb/cuu1RRsnnv9N66ychVWFsM9v
WmNKqAcw4LP8hr76Rg0L/9+6smiJmCSkJiBUFdzO2DOXtR+A6Jm0dif2iXNF2+fRIYQYun2rhs5S
S4yN2/IcDB4N2/7zhRJVJGdfHKyzMSEKEKv2iZTZ09wKtrSMvYrfCZnPrE1yGhmXmOxG/BMjvlD9
vsq39EIPV7DEqsZU3GY5WQJ/FPXDtXfM8aVKvyiXUXN92QCimfpEHUIDsN9//IuLINZ90++cte49
NplMYlQUhywJRc9yI5UfRDLTBqaPMdD39SflZQIZJ3Lb7you5tIi8Db84l4NNKOzQoD0vsdqBqfa
MXXcTNlX2VC/BWEwle0F98fwjekzv4giiCmUNqpVKLfEs3eggC8HsPMmO6MMMHjpw1wn1VCAZbH9
iTl2vfNbH8CIjtbbei23lb+MUpn9dxih/2eJOIAW5wqjoP9Q55VKYIMZKTydEpgy+8K3wpo7mf/w
0mJuqX3/QaVJzaZyJvmtUajG3k8eE79ow48wXrE3Ld/2J9cDnzIEST1C2gKEhhon+FgRF/rTdm1f
hSpVe42pEfi6YGb4fczHF/TYESDNKchpDweLq82209S48cAO09civPmfPOZMHo0CnEQwSHshtea6
iQdr9zH3kzZ6n4jFVU1KilwRa6d5Fgu5tJingn9eOH8RkRe1m4xGAqAkaXaezcI1CkV2ZaKoffO7
e/s4+adMumxzA0V6coZZu24N2u5uh2z2pwFuaj4297tV7/rB8nh7EXPwtboea9HhSzKVXi7r5osa
20ljW84VTcb/hfEoUlBUxYvED3zSiRS/38ODQZiYYuJBJ8OsJ5+osuMi4PaJth8nZqNHiENOH3eL
joTWC4Or6NgGV8HrmNegHl38G39Z5wKD19kb9G8+1p2l+d0+jCAgIug/HofskEpOaF0JnmI1Mri8
VaHMFGgc4Zoqyuzb4DkZQAnPk/258m99oUeApCNkzta97r9Oi/HzQnZ4s+mQXgJJifRnfOrwDXOF
MuhavnWxfydeRT64pRjMRzYTd6IYgTf0TLapvrisxp7SKtbb9S+1zgFkGNy/pVoxITWUKpJcpU1Y
9K8OJ7UsLK5/55sronI+7/mDQNDv8Ix6p/2RMcFEez3Zzv0HviibNytrr5DxTv/PKJtQShJvY6gV
lqs1ibwAgIv/49N5V1K/I+/UmCtgHujeriykJcBwb+3P1Kwq9HbMXddXb6Ol4AblKf7zuTqPV9W4
PdTdlv4WW+YM72tKY1pLaLjAZIjHsyMmM0QvgCD912oxmHgjvXLDs2J5pZORBDC5lU+assB2YVce
yqJjWlJmr6D4MYVok8+Q2NxxCjlsZReoaAfkw3CMrZXDlWDn9j2NLnegtb1dy5v+o1ODTFF40WbA
DCGujyYqzbe3O0ITwrIii454sgdu4yyGNSJAplKYVKUzusHPOrZmYMlG/ibhqNvhjfxuTcWzgoB+
LY4dUuDTdiWhSjyxLzRK5urvkF1eMQSAoHaW50dNV61pTxE6YjwYxLi7ltO+heIAyLDF7r+b/7Mp
Um6x9c3TFjmJ3vMA5FY48n/UgZtF1MXfmrjYiPMOheMyWSb7OOVlISL7GXe3endSZFkuwOVUCRE1
Q9rSGZ4vCR4+AOuqxuabx819IsEFGptbcUGEhfhBkM28XcIgj5jwjCaD7ueCaUVoauKk5fjv82lp
JZw52zIKnimjPlpuA1tombZqOFcB7CEiKI+NvhdmEsAn5m3IIvCCgWJudeDQfDR896vLOO+Gnv56
NsK8JbO0OfPEb6zZoXuZR5wZG8/RUpn0xdH1NN5k98C278heSbHPYJXzgQ7LIphZt8+eGhNijnX7
pjP17UOmdrrqVd0djHo86lPU1RveTyIrFT3mSN3uVPsYAZkM7xvGoDSpZw5JfZQBocD/gQy64s6c
w2ZKy8asbcoijC5yGugpWD9TIZE5qN5Nlpn49xMYD1NRvml8LwFcnbpzBfC+Q822JoFTHoG9YwBj
DcvrrIRiZCRl+RL43LV4/3zJ8+jaX4P3v96wWq/7+pBaMsSJV66e/duTuhxLKytaG1tJZZmEBaos
fZy3k2Nf5LwbJtseUlb/sCwWIkPxOa5EPty9U2ArA7BaGjibJCT4SF1lsNaA1HHnSGA4t53+UaWF
nkmiqG2017Vtq5LWHAq7meYdiEl5vjejBTRlTdhN73AKjhfvFzYwrbt5TqQVmdDC1wUAtnu8eVzn
+oKgCbFfCWKtW8vCWYRuBCBShGnVpWiEKt0drwDadYJnU/6a00kq0paT54YByK2+Pxd1BoPa3J9H
79Y7m6r/mQWEudi4/WW+H9YfM5hsLRrB1JoDFfNdF79L2A7ZsN3ikzz088eeOoMw+StGhl0nWZo+
5lq/mRxduQy+gAjSNXCOheU5MlQEI5m71M4oCkDqqdSDeD6GnJR250pZe+PwhnwsiShd5BMUOKW5
w9bg3rVa1T64HvE/q51JIFp1fM9QyZLiP1vSE2LkkSeUbXoMHxkoZbwqS7siejThrmwqxfYj0h19
Fd7wdMQxGyDEkAPpqOm1q4kl7LUciZg8+XNgiq/rCq0rF3ZzYeJ1tn/L1fiIB/kBXGso8mscO8F+
apqUXgtQWc1xysPZ32j8StpvtBw+4w0YcZTeYl7CGCJwnQC7i6GauqTCxJy0lqFmLLty2ZNKye0Z
e1a+7iBTgTOQvaZXu5ZgvX8lpZiWbLoeVplV2ueCO0szx7GSPDhktgn9clgL2mWnfEC6twKXtwBq
6mnM7GKn0HB0PTVvDpWIW8C2WWyN6NZlMWOmE0jZJsxi46iM8QBC/K07QUidI5LiFTDJybPGS2a6
M5aR1MuH4n4FF2ONxxIZLL4x9XgrA6zKHZzQUwvteAus4GYN6vj5bxF34NyTo9f1TrDlB5MZD4Wu
qbi+Ttcq/pdVw5taRbir0nUQzFs+yVwcFgFXNQ/nnT9jhMtL/SQXHSwE2SkRVCwxgNV1z6uBrgKk
RndOTiQvpaymrW1HoM0PAJgtsdHhCZD8XWk7L0203iCspBTaB8n8in4/bD/KhU6oc3SW4J8dCyHV
JWzQrvXkn1h6xFIAPHB3MHPb4ZgBkd5bx0pYwn1y/7lPVPW7ick911XTcVicMYZHIgBlg+AHg0gU
Qrqezmm1Bq8PxpoGJwByQf2DNXQBzD7JCuEI8vi8DMOwPlPXmmyqoLiUf+uKFgQwK0jjYG4hMxS6
+xERhMj8bSXcdPnTfkLG4J7nmIwF1tF0/qDX3VrtkEHG5g7O6LyVo4eMTykrC4IqpY2EErzSZqQ/
TzZiBpVpKpft81AaqZxcF8FGO7cwIywPza8KPm3GQrV2hKDlH3fXKCDGM+r827/xFetUgr3t7Pvp
4Otn1wA3vtlavr4PHwJd3c03oM2boG81vTDbObCUMU+edcT6hXDhXigqea0e6Vy/QY7aSbocbOJy
tJ6Nn7uJrkvq+YRqUGv67w5B0simLLzLl22vYU3u8F6yAb8yIUZsD/w0Wd/FYpfvf0nhcKDSnbbt
HRbslr0a3Hv93QyiBnMqdlNfGGYiwvRgq0VDno7nSLGyIVxG6NPXs9JHzeZh+JpwyfF+4cAaPrcR
1JxVfVqK7OJDFIllXTGD6W8g4p4ZmM61/Cmq9vLApHimtboDNxnsUqqjrWbUr+3ylD0WcBT2gfUb
HTwl6faoVbVY5jYNV+pkyfimdJaIyPtjP30HPJh2kOk2W9qsFOr3apLSfd1uRnyhzsNz/VPw7YrZ
t8SOofUWY1H+zuQdPAF7DpokdOokTcU4UNTzldiSmAK4fNdyOEKYo7bPCTRq5xAfbNlZyFfuHc6N
0sDryVoNh0OzEmEl2MSG2hfAOrrM23HXG1g0IA12iioP/7Rbp/IjKCBJO+nxpcaV4mxJ6u9VDe0N
NDomWUuSmGlpVVq5fdkRLQDqCs9vwtWMWXfYo2roGJHFHIT+GHJ4qzwSQ34bt1lPZWDu/OfLo6ve
opwAxnkrCT0kmwRRKt5moAp4cBiJA7snmUazvJ3v2Pd/nz8fV9WhMrM31vDzjeGqlXZtbG5hovgg
Fb2msiZeWbu39f6gNEjTTEZ/+1fxs/JWPYaJRLdIYsnDTjzGNzdO3jwlGf4rSOcFoJO43+iHW02X
c6p5AiSssFbnOK6nZ0D6IKwVLBefptKylkqKWL0CpXHsXg4p6yxofTf4XHyRvEcSQ3m6dO11sCPQ
Rz5dOGpL+Z4v2EStWxY535eHc6Dk5ohhT+WaaGx3AZTgl1hvKL8Guq4YS2K6Dgykqz7V2Rzcwcd7
p0KETISBERK1z5ikuvYCsxqL6bc0QDyZaycDUj+D30V7VwvJUK1rkPzLQoQSJaaM0i+2FzBnmK3i
IDnT6jQsGQcz9NeXSJErWI+WeMTsGcJ+ABpJHNa7YQJ7vC97RD2f/tNpZe0O6liwGQ6h1JzSlqvt
2DFtVmXSKoiQuyBiMxLP3SVqrGzz1ZqcGmswSnKw0V41Q969ZHb2AG70GDuhLHvbJDXqRb2HdV3s
Ildm9ZURNOmM89MYjlO8c4Hmc6rEiwoVRLHL38CyN3GAK+af3jGN+3ttZU1I/G8TtCQPtnI0UEg7
SJ4wiBeHP6siHOz6SUg/Amv5nTxSM3BZXP+osT6puXp2PpsDe0Iz8xT7LIXSLGflvzM8SDiylI+G
MdkJMAB6SYLWOQYReGljxM8bDsEVXXEKlJySljQVqxXK7kLxm8zHDyiOrvcDiEJSwBzkUvD19poc
iskM+YbMiQKDf7BWNJd//EHLwJ2fusYFHHjQu4pOaEHysj0SKB5uBWQfrsljGv4H4qNS2fGUAemu
otSHwtkCZaQz1p5rOFyJJUYya4QVqQcV4y0vAdP4DgBRmh2TADPgtAM+qdK6dAh+Sro/vWP5PTHO
ADlJMluohxz69CIllkC8Sde226+u/u9SWvn2l7jDBuu6b5XEAst9cDB2i5I+axfyxzpUtWyfPJfb
iJreJTA3oTgPbyDhVkNeV+v0pC0CPd8K37IM1KLEaxyS0nxaIje4S+yDrHj3tTnnG0BUCXYGr6mw
Q2okz8oPSXt3hMmze3uc75li9ML4QTlFPDkQVOvNk36p9BJIbukKnZDhNmxqMRBSSflHIy8gPf4F
N/et1hbsu/6cJloSidOBHGo1qphjwaBdUtjRRkghacxJakCGzXaWVavgJInoAZoF7DDMBd8KiGXi
lb9H9NcscGABtYULO1w8DK6/0pVQ1cGkQ3OdxoADAZJL36sigZGOhJqHOL9A1dlnDoNYzsLrMpSf
BVxLwPAlnXJKEPCYUAqSZ18J3D8DASeAv954GSVdzQFeHdGPyZaFhyLevq4B/zJxk6KYP3+EzPRT
RgT9x1myYBMsBnLK5UZoI3rt62BWkrYIL9eV3yHfpanw5Kck+GvPZYHfn5hQu5/yJycWECpno+MH
KrbMPB0YfICYtYMbSgXkDdQE7vma1/BPn4/3d+2hrXvTjCyl1lbPlgHY8gSF9YHmpOXSx0P/jPsp
A/OTGlbFjQLR8lGdRFHZCj3Gun+/52hDZXxUdrfzrJgJszFTbePM3SkYhGwI84/K5JoxQkvY8f44
Wd4u4IJ/eUwVJQv5l+N2McXp8TSxSjmKxjfkf/thUMA95bIWVnb9p4XDkUZQ8U0h/RylaWRZ2vw/
q1VdXBjaQMu8xYXUirudmRN5Ib8BpP5sEu6rpTRxFuJL/BEUEA/Gz8N+ev4bKZfzMaV83D0oPx0G
lLr/BKlkdtp+PhVNgPjU2Xyb8qakzV2Y+Jiv8s3g94aF2r/ip0YOrtP8H6wVR+dJ/Qs+jBE3AfAI
dIiluKc5GmSbUThT79SJAi1gTbo3MeTzu52ebVSB2Os8b7jZhv/XwjZ5ePMmxDTrIQx4FYjVWBFH
kqmgZmQV4vIE3E1cyl7Y13las+f1PWgFwzTU/FORmOz3WBSNovy5Y07vh1cASWvH2WtZ8CTJY1Si
9rzAa1hNOf2TbU9CGbfUS3+uAXWw/Vw6w6I2LQCSdmnc01RP8AqueRlfzLQUSZDsu5nk2lEWfCXe
+4ouNauonUReURwpzmFZiZlwT4RDvD0DtRp5khaTbqfCDch33IxnOLyM09K8/fXtROFwVs3O+76G
Ux5MENfKAnPopYrS3CPHNlRfOla/F/O5V4tB86pJfq31yalJprsYgJayCAnyRDzoFm/oSrETO6gX
OLR9h0wXDhpDuBu9EWZVg5mIcOwjTIGzJrlBs/99Im7Ax7nQ6s8ldEH+ZqKUMezcAWXbBZ6fUwkW
uCtrvcvNQ2F3XmCoOofBmTX/aUIA25elB4xKV1R1Lp6FkTaiPrClUPwhvKtjfR+DkSu8ebAtr6x9
Wh4qM3NIHXW8WixlXv6pPnxAcMeXCR/bKqO5MRrBhuzAjcQz0QZ5qjotVNg0TniORAX3RA1egjRQ
KUptI2UkSeUDCaPB/IAgwzMt54EGL9piPGBKjhahdxWaQzHJAIQrjhhRBwFpn9/SAVTjbqTYTLf/
noH/7B2tI+BguStDZAm72iQmtjfQSpVIMaMVTmf4RDSFvgeF6hPP5dHYm2gE4NTPM4lvl7T/f7jy
lxLtn3ueMymozky+FrmPVqaiyTdRlNU3KoWeqtEMNFXuUyN4Iqk/9drC1q9MpvAoKcFZhKrPbaKW
Oyar+lRRu0fMKUdFuoW3S7I/OkdqOJwOMT9BsJ12hHTzFN3stSQ3M5v3wBRMCrwjpPZON80MpjZb
ZnBv2aoT7XS6FqIvXLN4DFuL3E9ZwDzB82cqwfI6pw49xXD3rCAFe1kcAziJ0Yp1kynXZOkVYChH
vk9C4ZYNB45kcU2bxRLmFWuftpfTpMOi2W3ioifZpj4oNrGEhaIH8a7W5XfRFWclGiWcyuB+3XJe
LAsDZPuWW6ZGDr0sSFMdlmdiHpLxuGxBxNUhP8LNY635JiU5g7xq+4EBR0jCyPn3YtCroWILeQC0
FRxXESWIdF/vGj4ygPugFg8JOcxkvyFkSeMAT1qtGT24hg0c+ScbK88Gj10XNkBPJ1MvPuZzCvE2
JxEoRS7ArxKY1e8EB8z4Ztq8Izw0Pr2V9Q9UJTYUhgNfdovJ/f9F7g1EOhEeFFrTaMVXAmnO7Xt9
U3HpFHKCfzV+Jk34DG/doeYEfi3ifG/KDGk5F200d58ZtGVn/mUl87YuwZVB8dKPlNfKGIQRWRZD
lxOaqEoosLijxdRgIxQNlEZvBddz+sveI88kIcYrpXLaH5LfBXUmwqvNnu0p3E6gI+b7ZLf97AY8
mPfffKTW0po/koQvile2F7BoTvwG2nDbxrUMPGD+VKbRK4KN8vkGWCUy/HgGF/vj3smSnA15a4wc
GO4INgXmKOZBuuEk16URzUnXTPvt1stGfT4y4zgiuelLcjzaJJ1apNaMSw28LNhd0VYhWDJxGb7Z
C0ot46je/NRQrPidjNJMqpP9UCcCa8IKG9Cz2qOWPNoQNZNxvBu51evIyHAkht0wYiFIBRehMF8Y
BPgisTkzCyR209m9HsbeO6oEENcStIHdyBqqYMrQmdMHoI0K/o5Pf1EerdZsDCWE042miC91Z/bz
gL1Py8FG/MVOw/OVUX2ZDFQuekhzanRkdeGfUg2ee566ATDJDORSCsL5yx7Asr9qJc6EHY6tcusd
5Q4VTcu0ecWYkPibqmpx6b72zZnN4d7FA1VCYe72sUZRfAcmIPvzMdKjPAhvUSwOBA2OOR9w/TmA
VlNM1fQMjlp2XqYPiApO3ZKt5vf9WokYHSvPtBf6Q3vmiboyZRFDJVKxXnzBhle1cqINqOvID1lm
kLh0xalVUelA5mx7XC1quTOWePSoMnQI8JENBkm1SIGXkmOlljSmyFCTWn4zfA7reZbexxycWciL
6O2rlOItg8SaZsIOV4rJzrpSpcZ2aUfsUwUl3HW6kh2RWEsYzkimFCTTnIoXuQnjVmVHMON1XLcC
DrhLw5Is8mrDrfHLnfC6PW1odYpCArfCDvfFqcNBSNiNSjA6Mg5+CihRh9sRMuH8IGSTXixksioa
1zPC+64kf9Di4hglxWMN8bCFDDY/XhWcBExO8m6l52pPBwZf4itoiQ03iIS7BeL1Yu7MMuuGvkgh
GaR0gNLIXpSmptMw+Jx/vF192Tis86vKSgCYD+giIRLu+Jspps9pzHnbPUGYQExcl0zom7BAcd08
sP87ofrbJeWHrYktIVB+nYlAGWHjMClGaMrIo2UZzeJypRFemvQ0/n912auN6PQxoPZgQuh4aqsK
g+WpgL1rJuHjQlFtVhabJ/B0+SasKu4oW8vwO3L78beOuY2OcBPi2cixPN3F89LqO/JMtyG/jqvy
qMKgkQmmKQExGsC564zn0bX0QQTSqhA1cIa609XibogQ/jgVwd5evMCUYhBsHERLcrzwKv6OuDgO
rmAqyTu8uE0cz6DWjf6uPCVttcJi9LuFnarLtS9SjRd2ohRiOwPlPf70zt+R2laqU2/7Cm+7O8Eh
iU0QmfN9/raxEMSGw33YVNIe/NdM5bunDjgIpZ02qYD+xpiJGqrMEH6h+HH7UbQdpD4TlGv31gtp
SUxpXMfR3vV7UoAEnMG5NSa041usRX2k0yAwvV1QWp0EVS9Qd1EpoLvxa2dEuNMCLvA+ji9J/7BC
86ZN60GEBJkjtEJhWjtYmYwBXxXdbMRGlbmsyW5ew+xNfJk6RYV0YJNw5LOXakkH6lJFiwJ2yuPt
JsZwmdzAKzarDeWwkvwVP1S3g9/Zr8fI6JmHGMMLovq+RZFJf5heD0LZxHr/UBRX5s8rpbKht5Sw
r2lzHP5cg7BtHP/8/OQbCZ0Hgx8pfwvPhJHbVBkayJHQVGEV8U9REHLykAvrlj8ieAMtTBOoT7B8
+4r94iIojsGRw74dmQi7WOgXdxDRTYrcls2Kp+oV4XPLnXRf7qDwTJFQE2BO3uNJIDAtRE1AI3A2
hA1qmnN1cTTxXudFtdRUIPSkacsid46NnvxiYioFkbx7sXlDrrr+jef2ZtTVoebH5MFJXWytZWTe
5my5Lt05vgYjduzXuWZkPML1+JTZ906nHzAVyr0WQk1r9s6Nnc9RW3wpVCAr1cya+DR7yBx0mcCE
PH/O7uCG8bvnbbFik0tLJ071AijO2sV9HDhZjVMZtlpvt9/0acDfVydSs7KdL9GWRCck+j436hMS
GM4k09jNFbRu/zgE6SbykISKKJ3r1BwoU4I2g3lZ1h9TGt+yaRjFZC4lCs2O4AHG6x5HxUSyocsV
xwSnrdVMm3vIyipbhZbchksMXYxImcrS+pncB5pblrB9G7aiNVT4kzXRx8uijzLcR7c3MPVhSuHw
dqF+rrhS4xY12sBNIsTE0TJtA/7rB3OXVfP5mgJUMHfHBv2Y/fOEWxvVW62R0yNul7OO1rGUsK42
b3uIz2wbKvUCv/jUpAXmPczLvHuB4LDn0P6r4pHynuUlNaXZsxy/v7IDMbdq83jluAZU4BB44miB
to4OJdpqv64WkYGCyNJqht4mgUZXSMpsZvsV3xCDFqOHFjncnmf9T15OoO47fBQQAA6jFggsghTd
tzoqmXrRIJ7bp5PqUAIF8Go4UPm6vYnIw5+DSNJk4gKY6krmNtXO4eq29LkEIAqj/K8jzZha+gwf
cEMv8OoXi7ZE87JU81uppNBQjX9SQoi3HvVa0SsuF73/Dlvc94142p2R2bnQDvD0aZvsDg5R9H6D
gUf2wLsn3rr5zjCVYYOF+foofgWNs1wcIkpDUgVizYPPHMJka7u2H+K82kW5vWHv7E9EH1jHpo4c
bVa9cewBcLyWHbKIOeRfuZ0CgSfLo77X+i9GNAQGJNULXFCl1YBBW7ICJhuQyazr5e1aJGYsRYow
BeL//QNSiwdNrRRRPkz47PmaB1TNaEVtaBw7WqtA1EjpPDHmrZfdCBCRXYb3CB69ZV+YCk9lt9vK
0mTFsjUZ2imu3Kes8looutpm0M2xGEZPI8QFPXO4hlhg7xeOx2l5g00lP+J7XhioXf98qfK2Y9/q
IF6kXwBDQ9jtVcWoVa3/IbVHuCUTUb8eaKVWOTmMJmHYSgDzU5AxUsqshtpYZ072OJTsChHPH5Cw
zlqSIdXrbphjt8t5JB7Mnd4UASxpprpwnF8hNkqBHX3WjyMmdhtIGhuVV9YMkvTWsWsEQn0EubOV
dVS3AzwHZhxh44n7XCYk8YZjZsQi32T5Yqk9wCJqRB3I8TquDGVfmq1o2mEFnRXSTcksAeyf/3Si
rBOVcdWWCCJ0walnphCkTAwQg+ZzYaka0RhoXHF6tHI0Ea1Yy6tNaHAasxe3CYM5kXPj8vSBUNKU
ybSXWeGJFz/i++7zIBIOMeE8ikVtE3WZsIX06iHZvS1UER49fkdhDN1gdKxiQbew/NzuPEB0zgcu
a9eSPLUW1jLOhXWEC4wJa5fwmChO8RnSpsco/EZyG488TP0DALUaGipxzNeI3GIXWXVeyxKNoSP+
qYTUb3PAOpK3DjUM4MB4KufqHPuGer9g5jcBRU4OoQ/ocCsc+BDmmcHCBwnKrCPeI5WZdABL8H34
g4ERz4GJFpDjsdLQcrU65SX6L15UgDgg7nevRCsctUDl0obHGEnjuohkumWMsaSLQivDuTgEPKya
PNlNizmnJBIb0/nVk/40X1OihDx7YPQfKsJl39t3AdrIT3xh3nThXQbqAkj+AnYneDV1OTGk+QJ9
Qc6mXG5mIDhZHeEH9Vu2WqvflCz1VySyGnrENWH/eyhS16IE+o+c6asX6JFj8E+ZjR1B/F5wEZji
VLCouN9pm2Llye/Q9WrHEaA8H9lbxa2pqJO20Aqk6QFZIfg+GVvtWasSDEk1GgEHz/iBmM7bul0S
zNEzcFtwmPxoboXtJziNer/FNWuMokqkoZn+EIsZEOl2DFnr+4oqDJKE8XB+rcO4hX9RMJK/1Ale
zWjtt3uLYvy+bc4bxtiz7MP35EL585do4bZutiSzWnPgXRiABqP4NU7ecH2XoCWSP4PicaU0Teyx
GT3Eyz2hZU4q+IT6OAWcW4rWtM1hY7/wCvK/HwLCkE8TaINwEYfYM1HhQjbKeqZ85LyVV//Pv9ZN
4neDqljWV7+6Q8DfSF2pE39DINaFbI1yO7tF6PZ9xKcCGa9NaJG8MvfHBrjpC26Zd0TIiqUh80bw
xEtBrTyU1BKPjv3yfgKhqJUQY7LhwOiTT7cN1b0jAmDHQxbBKef8iXFixxZgq5YZIfITvklQ+2yp
GwlTSZh8MCo0Sid3aiGkRG6dJ76TrmuiLghJo11Li7dYyaFibeds/fWBLjKxDi9IWFhoMESKlNv9
S/NAYH4cKHiQYgIyCOpeToF7WrpWzHYmPiAXmhX6f6o6PyaOHuwn/YeiEUdEyckjFGqNMo0tN2OY
vf2IRi77EvMx3GsfukQx0a5R0jC5q37z4vDURSd3BAEWosKQ8eTnSRo4pPZaUBL3QiAgPRRdJNJ6
D9L+FA+xl2Vwsim8cd7bSxIP0hJLvKtc73/XINkn/8B2t/4YDlzk9zFQcMIkQHT8HwDsw+VF4LKx
KAhERSVoFgC7jkrUzy5T+X3vQymo8vlehgVF3JTMq9whbNQGSfwhOPCWo3u7aclId+l+LQn9r7i8
YCIHoFpCSYX4DZOBmyFIY7xRW4bxB++6UVABBP4GfE0MdJz9zE0DEiT/brNLeFp19wmnVy542EAV
grUsrODpVIchDk+Skm/6mFsY1ejNKr6+7pmjkzVFiWDrrC8GHiHSjfFV3be6BDTCgNQMb2DLiawy
29y63AxOgNOY3eed3Tiaheu9T8/rDK4+5XpBP1e2h2vJJ9+d5MZKyHPwLQHk3Q8DhBEhym05HK6Z
CZnUYDsFqaGIE2G0VIKsGbJTu0X8Yu8mXQDQKavfg1u65bbADpY4TQxl4zh4LPO34HiF3A/cMde5
XaKhCIfUdws9s5E/TAlrdB9vH7QQLBCMBTWfL66tS8iXzeJ+zklIdTiTAisLouTUEK5+ZWTy5ViC
SDB44GTgGhKroynOtWpU6ddNBUsohqvUkW5V53NRd9nWzlsyjC6fyZ3eRPxWw5vljXfzmyMhyLCX
P0EclbJfCihNQiFkTr57qCdDPGRh9OAQiInnsUDsWDHZoCbIVCJf4rzBF/3oLwoozgLLixgzz4GE
4MZM5Kq7HF4v7Tu5iT2xwo9OG5YF7rr3wZOzgUswF5A5bifDg6VFhXz/EuG/nE9k8ssgyPhlsRu8
EkBJC17j/bnYtoOx6g1HNY01urSAC/8WCW1NJegvXO1Cl0RmkUTdX2S0qyL/NKZOUjygyj8L42EX
qZeBMon74gY+klT8K6/bub04OggWNEeyRK5lezlw6Iea/A+GdYJVDWQfsxRhzGeKXm80TNvV6mLp
J9seIf2AZ6ya1pDsOCG91wAuloAblX2c3pXLUbAUuVmGTIFY6esu4P2pmHfNwoK1bhhjhkYNMNRz
iv3uXe968ReQSOzIWgRT3RQPCfWAVsN4cGbQL5VbCgqbtAF3WaaHcs9RT9zJRfdsz5HvunRoQcL+
NcgA2ZqZIZRjamVo+2WdAVNSDrTqxXSXNi4Pto/9OEmCX0IRXynu6yoI1VskY5EhOk3UeyvWFb8U
jRBs140q5y/cWtUwOirWXU/RDIjLirG7Z/2Fcum/eJi/Wprb9+AhMUeafixYmI4C2qvjNUT6JmMf
h8Rzqkkegci19kPUkvmXkcy0lHXz5rPSLoH/ePKWtR8kJCaNH4zrGLAEWsNTndXWAULT4UgcCj0u
pOuVjzet5s6jrx6UnGJHvnnoWxZkZgEQMrzSPKs5sbDrInP9FPr6wGFB6N8r/qu3opvfP+HJEOMW
TIPLp14JD8ADlGkM5WVRbfTTM63bhkEQpvImCI1oD8XgzZcVKU0/Mh27Ler08CWdRCQA/b1hDayS
Zd464/8+VnBqlg+KtrqNazRG8ETIVzeaKQV6WUAnW21K35lusi+7Vma7X6BILQScp3PqhLPdcOYv
nnw5IbWr5SRfcLIasygnr0ujg49f1N7SqYlH+2kzHyKAkE31rLcf+4Fqvm+OrXC4JzIS5LsaJ0Fi
NGTGBQzJAi0/jc6wJJUtgZ0SW2q4OJOH8T8wMHTm4K3/gLlyI52y0od8ZbJzcDRJ0agXRrFUDr1+
DgfrgsleIKL3QSQUxP0QJZOwLUOveyjUnTsS8t4IRZpsNXxoOcbG+maFqy51AWSzwrTghiDKsgY2
zMpvkhdxWmJBJaPgrI8/Ihv4khtY/2IzMBUvCNawnuqKPkI5Gb6ECFNFyf4aj/t2ibHJqM8rG3HM
wpUn00nT044X0WeFwiH2o6pLAz/Y52AvbQgfPWIinE9Ga6GM7aNZC8578/Nl0t73vPF3ZxQmRtRn
HeKta6AQrf4ufF2fWB01CmBkqSRwvHpqFv3zB/ldF6e6cmik2boj7pBB/Qp8aHa05vOa+md9pi0Z
fnq9JFJpxjRkyuihpm/JS7NQ9iFzhFncHCWxKym5gp4Kgrdmwm+6S8WnGEYkKDcqBa9hqaxRJkak
tdGcqc6ZPTKwp9KyJcnK0U4JV5z5EoorIo1GMSYEi1efAZRjLK6vZ3a7KvzjrcOciwqlGWk/WJzj
thfHuZMJEt7T/H9gSJqHXgAuY1u5imdel8buM4qJ5LyV6ldWUtR2f5QJAYNyVMsra9t/qyL/Td/j
rHV+vFhQ93i3jKmokiwnKIHoxWalejPtAu857DVE4mC71y83HocKUxddjMvLvoPdiBlyHBs6eR0K
LrqxuEH5YZt9Paq0V94V8FoCA1cJGZPoU5oFrc8zwOSfLl5BIBt7fExeEsl+7FCsIGPqyeBRPAp8
DrlrpeIPO2NlWe2PUFtLL0QtHgv4yvFWq68Jl/+SzHtsQBvK9rfCLJriNvUsQLvSGg0iiL5/KpvV
hb63u7Y4s2Fxj6T6xNwNo+qbGOk+5pRBN7bdEIJmremxhX35sohcT+MoBRSqLxnn+3cSBbfDbeKd
/l46Wmt435I74uHA5+QCRd0OTaVwF+FYZIpI95P1y7xGVir3VBhfikujS0MAbsbjWY+6gRwOFsAb
+AXnQKXj2I72//03WdVQesTuvJtDDMCveWH+4eJ5uQSSW+PH8wOjDJrdcSqGPVcTqn4O9m4Bo9nf
ZDfvykILvUMA30YU9Df0KtKTOmugWOx04fsBslxc/BB2X9yo9UVHy5ErMEheXa0O1YvjCnUDHKMg
9e8slvjbotv9MbDtSXAIFk/aY9kBw0W8kKd1h1WLKWtMRR13/wX8KwTNEozUtTU9QasfiLgnxuo5
CyhONegsSLIwqdSZI/iqQdKX5n1CR4XEmS8DUMA1dkwqcvnrz82bsqlSbkNPBbAyaphWPGpBwLm2
GtTtoYlbliCE8lOt1cEcfkKvz5KxxjTPQevBlZmIYd/KAnvl//wExdhqsc794jcWLKpYL0hqX1w6
bE/KDhWCI9N/0iPi0gA+BfHMqacT8LHTonhEUlTDHWd2XrPAZhuwGxele146GLOrhYuLavQU0nce
5LZjEqxB/OBrlNYcfYaJRskEjVIZEXBxx2Pz6hgFqV64GU7gEkZhj2gd2SBWJejYeWAODfUbfG7h
iI+0IhsK6i+AM/bj1+IHqpUOPy5af9OTXWMYd9w6pLS/MwOj98pp7BEZQWCj9uu701839a/7qz2p
7fPLt8tLeDCgEj5rcnTaN+2Dq7PS7Yg1NY/oblZylMpPpvpjIjK3Is0p3JPHvxNuJYw3vnVN7phL
dmMV1OzSgT6FHk5FYgOhxAY3UrKFXVKtLH45S9a1L1wXBIjcSlExFmas4yn8xr8egCt05by507n9
Kxxp5VEULWx42a6N6h9bqfxI7mU2tYiecRKROakO7qHoN2EkIrpbAyWvcojySt/IeiFujW83c3CQ
ciY+UMJpz3ObO8ob1WN8/1ccVftlG8fevuRLeYHE68TazYQ7mJHXytin6j/D4XVcR7VZTQGxRlTn
uVzs2wqYZ4uRjhSTaTTflROYk5ag2E4fN34pM69wz6RDCW3vdk1uPYn3hQp4jiT/sD/TQJz8PKBi
kGhKn/iuXZflRw5twmtC/sqtUl+bNTWCZnriMddztDJ8g3ylNj5In7Is4SVDyfelOUVTkTqUBaPS
KQ5HUiNbQNaoVa8YUQSwr/vAT+YD8KFFFGRVTozuNYgQlrHFB/rkJ6ZRpNbltD1raxHbXCMP0vxh
yREzJpNd5pBRXPQDy6h9oIFvnUbGbea3YyQkz68jScaYkrAXokD5VBw/4jIpqfS2bhSdT8UZsNP0
5iz50J3TcDui80RHqKyK/jdnvFluvuHfSClCa3aX6ojT7sZpygkAv7yCdga9hzBY1YGEseGwcbY/
xtET3/F2qAGxvPFmGOwJRhmg91v2IvkPXdOQqrO8UX8++lLBudOtksgvPlN1+TNX3JndeiWZsUoc
9+teyzTQqQBr3YSxYTPzRhbdl7AymMS88g6CfyDuI197p7mZOtXxwrqa9hjP27Zd8EBrOT2/BrKs
aGA4DMgScRi9oUAIybD8HFqwQ9MT7RmQNOf+ir00e/Gbocbwo9HABPvT5kgUvCzAfklV8fuMaVLB
U2dbqDDlVuYPMTWjyaSP+9mrIglP+ACPNYf9hBbw4ZaYAjp98M8iEt7wCe9OEk3hDYAmhLca64cc
sztoNdKxAZ12ukUedJzhuGRFiJ3VGw1JvpvFRw/enepnF9xrbqXf7pXR1becdWqQFF57FQfFFNb/
aa73IjPKT9FcMTbIFpTa4HPk1pqIK4ZSibVIcPlUUGoaBQOXG3bxJ0CUYFuinmKfPDs65uwojFB7
mBL5P5AcC+9jl+Sn54Sw/+WdjEHeCaxZto8VxHxYcFlbjISKF+HpRrz+THLZIsrzw4TSPWOiPC70
zlo6KUZerP+g3t52I7U9DUeKSRZVdUCKVYuZMqwVfe0w+8LBWZrQvQNPWGM5IH0iUpaUMlZNz/rs
fjvtH6I6NeTjFH5oQxjQk2+ECsgaTSgdjtiz0vou1BFZ59wUZv1XNA4wmuWIYPvAxr8ZEwQp74iy
ou5T7KyHd5nrjFnXBGoUIHJudfjHAStsgZOp9qBk4WpFLMUOHyvT/eWm9UpKnS8Asgwmm7gmovEX
8Gzc5UC+nG242M/6dcQkOB1GzmfrakAXSfNxRyI2hnfE5ytBfqRX4n1I8FoXrRb2EAOqBgsOftQy
wsDhMrsgkvT6b5Dv3R3s3msqLausfzYNb0gxMnfAIyC34oDH71MurBqoza4hoblAFjMLkDBrX1k7
5gLR/pjhkm5diR31gp1vkCw+RtkBTp6LJbyH3hyMRlMqerfi6hnKkUwVtpIUOqC7zj3Q0WfcIcs1
pOQO2aoaRSumFw8ihjW0op9QO6Efay6RyvB6gN8x9cbecWzcU9oFbn+CmQ0y5TfjidR2vskp+R4R
YZlEi5lBvB2xA2tJBFU3qNEe2N9MCLO+VfoevPac310l9efDFFHcPjTsKmugUQnv1MQh+WaRfb3j
aO/kdn0wf1HQvDu4Pjp/YNfDvR/vdP6lffxQS895mWUdHm0HhsMG+Q8Wd6Lq//8HIh8Lmvcnb2tA
nU8sJ185QZA2v1AdBPtOM1/kjW0s1+dks0m9Bo7gyqrgWdJX7BVNBAJWcmGQFclCLsvu0PzMwTut
SZR/13wyr7V8VIs7HjioUMHBbPOZ/ToVwDnSVJNQwt/8YtPHP5Nqv5BMM0dZ7M9YQDmjucJSJt3F
OKgKizOT65nAWIL75QK3duPmsR/RNRE48nE+cRxYiPnVXw57iJPRO94Zo/Cn7quGtHvseUROa97X
H8ZoV+yU5TDvONqYIPgDQEH8eujkH/p0Bgm0Oc0DYdhBq+L5rrL72p9j5/TALifpNeQdOygYjiey
Xvekba4RKRiEbjvfejaSOJVf8Hid7DNxANa1T7JLJdr66w8wT5lkD/jW2PYsWbsQeOAOAXXM9i/a
JNmURPOm5L/uzH4uFHl7rsSjwo2Ku/zvAP1eBf47BZ5G5iS9iMU94W2IpXR06kIRsB2QCqmBWKdD
vxyLLDBHPTWjQlF31SQ1m+tiMYjrDEPSZJ9ShIuTNjE6kAOgj8rBZbl7bOt6Exs+e22o+GnbL+7W
3407weXxGo+g920CmAwP77hQ6MBdTVZYKV1r0MS56g/6qxoejZRNlWT5SEVnVjjHE3Ik0AcwQiSB
Kg71/WIcIoLF5dY/45P2xfbArFydSKQtKfTITb3IgyteUZX5qkqtNNOi0vPd8T57s0Ppe+kKtSiR
E9fnYz78ot9jrOGGxsV9A/D25ntfO8qMkQESvaH+uKSanBnqje9QRfHTKbRxrMeUYi9ZSPA0tKD2
hEsgDovpodVH5j2eWQzGoCdgzXz2xn3OYxyIY8rtA4fvvhvvnQrUaDY8eCR7Na0iAO1AXfArF7aQ
3x//nqR07Vpmr2x8nVOCH0epRrFVlZbUy6A6Z5GEsg2m3tnT6LLkDgJ4OCoHCai/37s7RQHtcVLq
hq8+lHiffE7+4WKCYxAdxbGyHhFiHl3J3u4Eoo4SOhtMXxaeYSoe9kHKS6pMoFbvWABEMRN6tGrD
YnHmaza74Chk3lS5nfzkqpdE6dDfVoNEe5oYuI7b3Ds1juclemDkU6N7kGaETfSdduU2KcpCEr6r
PGQ+iFB3f+LgSWyOfd8n87v+USueCdDE13nzadah5sCNWJF8i3KZptKuqHYdIsfqTVWxUl7dWapC
g9rhzsELKqYdoALr5QVIfml0TD6OheZvnWG/Pdc5rm6aMAaKn26dHsMDcWWmJpd5NGbWdUF6Euzd
SI3BXS8Apt36vr1HVJD1t6/zoGuOej7/9dq+kroWxFFHpibNV1bN7X9rWywdxdvtg0IkFp6ILanM
Sc5lyZIfuTTgG5q9m3csRGoQQ/WnSL652IYMDT/qVjePM8/sNH8XsUesJppdhSL0M3bUEjMHF2EW
jS0jTxW2qZWjWWBxSe7bU8eN7+4DWIbhPMqBzjmZI/6CJ9nJauhqW2a8EVS2jfexFcpqX3d99yd4
8ATUAYzUYErZPeZVxtcOpm3wVIaLgXmhS8oCbHWbVK94/C8wEVLZ45UbMoFYvjjCgsKWwlU+35Pg
EVDnIqlENlcjTfTol3HG488vu5DoB8vGWbU1g2JKI8XUVa8xyHy/X+s0AWtJ5Po27B5W6Q9bFpuh
tdQX7pJzBZS49GdYgZzXbaJbkpaIbgHV7O2Jagidsdx3SmvKmZwGLW38uaAZ6LhZPQJNItN+Bnnv
HTJkSSCmfyio/0co8oxiucQLI+bpJKRx5R4Ik3Pi9NI73nZ5QdKLAIrTFo3cLFs6cI+3zxn+UTYk
HOTPJqJ7jwmaUFIOZlcpG5rhw5soko8DJcmytMUNuZ3XYEOhoQFEWFzl+Pt+GoH5hMqzIeiv/M0W
PjgZ0eDxRgcaGNZSULLgdjh2k13H09Kou+HerWYU38raGbWDUXfHSxXWVd3Sr2s8V/U1iVWUkPEW
7PBEWUAa2+4+sj92DowJdgyQZGwGFtUyccdHA4HgbobEbbLPJPtDCVv/qIn9/H+89aH8He4JV0ix
iDy5POEIT7hZT5XIpKLEjL9QRcCJA8C9qd73PLPWzb311HNdaC5eBivVkMDqu75hCKDlYDriKs1a
30m6rq1kKZlL2snbAirl6Xu8RutKAJ9978rtbFJM6HdotXwwRnaWHso0EsIYEMW3ZjTwp3QTj7ti
mwbUux3hED0JOtTZh9jw17AhGMWGO97LjabjoDIaBlguPWKBJ1tSVStTDKF8+OZPGzfIMmlVZTyF
mVh3Z7eXNJAfz27ESxdeeq889fFBsCGJrkm4lKiXrkIpaKgprY0bKNOVBwfntqZPW2JZZXlp5ozq
jaLtSsgEIolzp8XIPx1lqfJ9KezMAZowEQwi7NohuL/ImfufjwBiubJh+SnUhgR6wr+DnsDWhoJ+
GsyMwO2FasCmf8sRt30DSGe7JH/qXwFLWcD8bvQK8rBiBqgBlupfZaKVTiW7ZnE2/rSQKOoqLejE
uBuO8xBLMtpiEu67sx4OQBzaoGaLdtGlL58bGChfWf+ZDBC5pEBL1XXGjiFOjNGl1OxFg7L3hrqx
QoMdpUGNcem7SL+WAGsv1t5bfaWhgER+GKQp8+nCzu9LbfSkLzyWpbO0NKJN8MkJdTcC65gO5Y5o
ZLfwItD4xn9dQi3q4HU0UEOBplwfACKXWy5dJjaGn3vzW93S9wTqYj4TeT4tPnVrL7yWHTwEpJtO
JU2G6JzlKcO1/Ef7AL9wtdbaNzrLZBLye3Jz1IU9oom1Fqjv+e65nfr5mNQ2QC1E7vZ7SW56DXm0
SJb46u312489gdgnu9JlPKRM+N9f7O8yw2lRPgrfksFdzwQf5nNBqAoWinp/iUdKJGpGyfevug2u
1h82AiJ1pX6X0oAEjP/sXvNHdujb+J0NaJSCotl89EQe8P38ujYvP25wFULbiyqjE8iSp2KExmec
Pq7RgCpnMqEo+5IFFwRHQDCOgkuHiex+1DmLMhZPeVevtCfDtTxUJSciZe6uga92ZJvUTCRdQRLk
PKP1pNpym3CAD8VHzyk7Sgap0lyu+qgXQBo1v+ZcbaWOZgc6heVxxvQ4bZHYERBonVpTOheHQr8g
qLfRB8uayTRwA8G9lq5vgpTiU35bwJQdQB2JtFR4Pjm98F/0kkbquwbEy0+5Ppo5Ki0kGtVSPNd2
yZfMBSbaW5YARnzW3FLN7XCGep3EuLtTC/zzVjqUQXlBtGPXhz91DbUygpupeUwUNZUu3vVspBth
JnfStYacNRyhH/9z/jl5AcoFBWPZQfOSjyGOqzl5xcsSkfayAffUwkRt6YDrt12DLbStTaRqozg4
QZtsvYuu46XNxTF6A7nDnj7QQEM5S9S0iOHIWDEwG1yZWkhyp/hLfCXQU8yb/ufmokrl6RyT7ltR
JLAEBTuqihrtjDe+FRmEJxHzIJM36O1d/ns0bAbbktFLelurkEho7mXKVRLMuYgp0BuVgU1bvE0/
Yzy5PxwCIfjmhCAfDXCQhF19OrlmQGWjjIzLHu+HuItk5XTpzKKNLNPVwVE5KpGs8VzW/v3m7nrE
r3WkZiq9rwDK+oCv2UdV2ghjaD2fV/FB3xCARRcNNybowbA2jC4GWrEyI7grhc0hgV6EwvYn6D1k
zbNQ0Mq+DOAWfFnpLdkIMBjiSFHKCgFgT+F2IPLfSKQTYJks2m+mjRdFOMuS8lv0mMLuKmysO3uk
z+ZT9FEX6B1MO9cB/YMtT/wc894KfBDg5r4GVs25PKbfKYaeRyyxkEueRARnGaxpCGLt047rYDiU
VSIjHkGtTt6biWkVghC/TB6ULhYc7KN+6HB7xWK0E9xG8FRedbvRc/Y3E3vS5AQDMgcPGuV8FCzU
4/tB/iwtdUhfqWih3Wemxpf+YOAkYLjHxRvl1UgIDCvTo2fgKem3DoE3DscIoA+0SUqteEAGbcL5
KsJyE28CPeYZvqmBWmwLKzBnX0MXDPhcNcmqNzcKFyJQ+0hSkOgXUOC1qv9bg076DTDqirEztac+
LVaE9pyPcZMtRpbEcXPMjhzgUeyjFrF4RHS9GyRo0tN3k/tbxu6AKUpN6tDoiofWinLUwNcdPYhA
OtEFZ+eZ4UUQoKYjyoBfy2+bsw+1jE8vfvazUsvDmy7XZ28XkwFzRC6SdiaElzBgdfYO7dSsb0pB
SpXFM73kqqOihBISH1bgls9goWWM4iflmf9H4gZCKIl93DMlvOTal7DWlAAubHm4S5j4H7twf9Vj
F3XEfkCwLFY/5ViyBVIUL91DhOyKDMZEQ7adjKW7YXAtv2w2cQEbuux7net1Wb/KCP7Ig03/4FEn
5xF0gtCRcpj93+spCZCtVAftoB6lP7D566UbEKzyoHFecbFI0YoFEt2iNmmBxLCkb5U+3HTkybpt
CffAjGrjyBgGFD4bZfeZ6f6PexOo97H5z/eMJC/3y4B43wvO9WmiLp49835x/72Wl8ipW321NzUa
vT4g5bejuOJhAW+hOT88KaVotHCSy+B576lRFmzZhei6+APrEeh2Wt69Xsj9N4pX7LLse9sg0+ak
1CIjPHj4NQpogPxpYWJiQfx6L7EA0fFU/fx3UsnNiXjwt0fHMVP3DBPEsPdkW4y+CMVUehlsHq7Q
Qvhct+ZbAk+lcKgzezTQGey5Y2AkMdg5QIKWjEobum4lwo9Ama+81NSaO+6Q5l45qBbPeC6STA6R
xuqTlAYGsHvinqgP460iAjUOndgA8I8T0ElXvuUwZ6e3Qv7DyLgPpb23oxQ37Rdb0c6i8V9ib0lE
BAWCGM7kfYSd/F4ZE6HsQlLwGaZguMbqeh02SbhzlbtgSA23J6Vim1AVVBUUUxkyWI0e+5pkeKmF
DUZsfbjqaFfc0YSyb6GnNpbkWudaU+eK7aC6KF6Y0wEwe3/GI/5E1k1vyrglI5OGmnjUlDG9LGsn
nImDOvmORG/YvIGnBvh4midl/Kgfn6SOo/sWxBPqtfInFLkUVMsn2f3mT2xIsaijJI4D/8fTwF6C
netO5HN+1cbLfCVjqA9os1/Ok6tkpofo7397OFsep8ZDtXB1XfYvHZ1Q1iJ23teDpBP39wrscVEA
0c+cu9DZ3UZk6Wl/EexoRp1Qf4E+SJ5e1NGsf4RLpDPHx6yaXp4bx2a3kJgMsfPqIAUyVzqNJuZi
9+l7HplCO+J2Gw3AA1PmYs4UQ6JTPWYWjb2+JbEtHKu5OUBh/Xy5XxrpgMg/kK2Y0VVUJoKxu1q3
HFouY/93NuXbpndT3k48LyUvzFPiUAWDE0N8hmDknrjUINnP+Nv8EDs3RJRHa46cCmO9M6/h9H+5
VXDE1yR3y6PT3f/tUO6E6bFmjaEu4z4IeWLN8Jp+Ug20e1UcoyRCRCpJxnG8SG1fZGHjiieB7tM2
RxCQXyvXhwNizL+NbE/6MR8oEU7SL3+Hbgusg2xwbwrUG+LQKKUAED9Af7+sj+TUSW0XdSTTQocZ
B3PjISwKh18w6Kixzad5B3Edp+KzJE4VC+TDwDf2tzdSyjlB2XkaUQsdkJxXhVCn3v4UO2PrCCiL
JxCNd5mG4qODh6itphiz49MvMcP4iZpQqiETmHEzBRCoX1KVM05hjg4Qya4/3XKUJCw0ZsbbV3Cm
bKfLZQVmIo9t9HOa5a5Z2b9GFDt7WD8t4JHXYSGQPe8rbhIaIfu8zURoGTPWMjLxOwnNPnAP1Ryv
adCXFnHC8/eMOxSjhOycNzXLWrElcc9tEbcwueDNEx47casEnkwx01X1JRrUNopkiSikxZh+gRzp
+hd7ACRkmW44/6nfQQSa1UnpVUhMXLQKwbFOrdH23lWKTjsa30U/VHuErXi2aVHKb9TO64yqDttK
aVdt/MuR6VfsROlgssMW0bgg7BFMOd4gQvEbSYX27vzAKGGrqXR9N4rcmbIVigkr43IajIILYFHp
jWpe7La5w+heibxyJLuceRs16WPHaG3gSXB/q7WMTrKKufBbXFhRPjJxXHFG2UXNbd5XrBDpAEW7
0XMB6mDE7iCJ/C3/+PF3KXV16a59WI39A4pVniHhfyVdIHtI28wLfcxc7+5rJEbBtcLmRqmh9fe/
u1bGIYFKLE1DqcPxnxjxGLT51nhTIQDIBvSBiW4vGZoVyt6DvaNbMzM73ftk+/Fe5Nn/2maGFAAV
Awsb6VPSY1p1Orh78doDziRsbMXVw1w4DyWVe45kxb/baK7pSDfISZWEeUwVPHC29+IcV4B0CCqM
3W0dEtcMGw+vTry6dsDn6JM2oufz8/zXBduDk32R+q5sMEisK3RhVHhcF7pfCgpHTvO+7+UrWm4k
lvfXKjMDRmxyCtd+NeszdoenzSVZ3TAX2mMx6ZNU3q3JXMUXTR9F0IYR7E7CRODilGXjFCynTYeg
nqPXdD8ltegh7OKWGn65g0ZuvC7hrKwFfWwU0lu2vswwfZ8mLvUvg/egETtaGWZ9PYz+f8Hz8j3b
Wz0iz9aynS2GmDNJoYXURBVutXmTj1zwaz4oBVVkweHVO9CQQJoJzs2hmAt+OsG3vI6ZcOhJW+vk
uaTdhoJ6XSFCZg1LNpKnXbbayyNfs0uDem1xbu8w+kMNK58IY6i4CUdR2ydVuwfEffWBmynPuksU
JzN6cS22xuazFzF4rQBTPqNFVbq5c5LhRDu2GMZ7susy8iJ4brCX71MJ+ioy+GmRlyftd+nCIpc8
29RDbA+zgyQLycDFYizFYFNiPUyBqXgd/X+s88ETVpK+ZbGqYJpFYOiwcca4EGhYnT1ClPfqKNMG
ZYEEnVT4EmX6imSoFvBxgY1fWSRxmnkOCSg9f/4sN7pZ+eP/cFsLlEiRThVIboBWepfSrZ2eBBO8
vBhEzbkYsaFJork/wai545Gb2Vi3py5UD47B+aIv7DwhS/xJcx1HlvQNkdVaAEHATH0NAMvrRcl/
RWGxuyuaVeYkjET05rWVftsfZKPvYfcJNZGbT6rWHbUTtNgPd1yjzqyU+n8K5cIHFPY4ql1m+nHd
XABziQbPQJ1qAID7XELLOOGNSwFX21ysm1V//l5Rn231YAKP1ZngcVREEfo/NnFOZ5WCgtoVsk0K
b0nEulmSpMbE4M2VE0LuRLk1Qb9NzHHnBCGdx24icDSC4Fy6E0x6kEmY06KxAxBTJ4f899kBp8yL
k35W3X5bzQDCIvEsGc10lKuWW+31XDE/POzUrPQbMrkLkGLVGnV9JMXd8sV+dQ/yZ2Wv3xNBgmSQ
T5CuCbQKeEHmS8LgikPjSDxJX+Ds3TrI0ZKrHZ68OkRVhiMzfzAQig3bDLGtDOS5ID5jFY8Qdcc/
qZhZvRLPCdZKOZ1825WJceTKxnBOrWz8FnVHjGO+gWIL5Zi38o4ZwawJAxkpr07g/sHZ16oNi94v
d3qdKz2lj/UIClEjp/wX8nfA/wMhcC59h9r41RxRFIBFMX6SkbS5nw2FUzTQMUSD8c2ZoyIoqqrB
lnaDUKTNKa66EU+H0KwgKq3G5TL37MB0TCS7I8UzDUaHV3DcfpWwUK9YF10KDk/3K7Lb71n9DhTQ
QfZoI7xAEgAyvqaI9AFmYQbQ0l1iH5JI+pHqOoawXHvUGuJnvk7mRouSReBL2BQD+y9rCwQs9lvQ
X9Y3fqMveQbZoXKCIkNsGpKbzurmzqMSRkA5DmlgvfGmeRX/jSZCjeg3o3wR1uslRwqtAR8r8OxI
kGhjl+gWvH27ibWNc3fJmMchYvltHYT7NGyOJdVjSy4kUoiQ6qs/xkQBHgqd/88H1/jDoH3YLEdn
z38CHCep0CujN44TT4a+1refeqt9lteiXHt3G+b7MCX6MmgZ1RLmVB6DbIGHaR5+ZvZh+I2Cr4BE
QIYBDKG695ltfYo2J0ejgDsDwTiDDTs+nu5Q5R7sABnNdIJe92SlNvwdXBlM6YgRxgdpjXEJHzxg
VBKj21Ty99pFUyONIATZ3VZrfdDPeSolihSSZb0EdzZo9y7FXWWH28BK6BbFyvID2PwMAXCNPRCi
g0NQi53BGiAYMZ++GB8AiuiQPZHMTunEApaOaRQ6b2QbkisZTogi28und4PnxjyX1Yx4InTYil1d
HrjIaLmbkQ6NjH/rY35TXIWEEI8j1Spr8fSC8hQk4cprSDeJDgua6bGSCzm1GWOQO574AIEzdwXc
PG5ZQIW8zjiS9DAhLNNKpajkC2G10LRzmTKPvnat/SydHXs0JMzF3HeojVpYO5LOGdKShxJbe+Se
YVn5a8zocXMgb7KSXU67QoNt/eDTzygDPOGlicx1KLhcoXpBQfPAL7QKVR5ZFTylQuRtaYgt/Lei
p9plv7oolWr0MMKJjju1zGddMA6dSBrGXxyg2aengtm712hYjCpipZgIWSkcnVHRIdEw8/d9dtz0
JCUlbx/NOLo6Y9HJXmOlWgnkRDCGW2Zx+C1w9i/TPPm2PH1kwNbYI9kNTL+VjlTZrp8vs1XtTjaF
0U4+hXQHIRDSzrr+8+k3z6LOHDyF6yFdLhwkvHHbGk0cu0Ivyvvu7+OJkUFbIXUlsgBqTY9JkiB5
r5NzNSK/Sf3bAuXM6wJa/30WvaXhzW6gFPbRqYr3n33bNTMHf3zrqSlTByyBPPMhAs6oYmCiv8si
/s9YIbS1EVHvMscHoLgviqwXgxtfOa+V8u/ykyr2uZlqOrh0ba451EkqsB0cr/ZkjC4SAe7UmILk
9DDHnCsUmz9XRlaVOSnN8fJ0Q00hSI1wOcnXVaQXr6mn0UEGWNIqgt1HP4xJ/JTNsYpNxkEwqJhX
Ha9tvnH8THD+2+GM+2nCYN1OVwE58TI/X3trm9Y6ngMwE6OpLkrw6qzrfYC9yLSWnxQ61dqJvhaY
i3P7vb+J9w/Mo0dJkc5zwrpjy21DeaK19mpozOts6WXEFpWOLSbEIGa751f75/8sKIHqoo9nMnb/
PzELZjfOCeJZ6tuH8FjrNG9Ci2spGcDakMbc4qtDwFLgbbWuaZ8vncMbZQELE6n2ucZpN71lQEyD
xE+QTMWRQ2gX8BdS40J/ONZwOEKZ65x8QrH1kCrxir4pJzFjJm/O+GCrMMKHqG9xPBpDHpyWEYyN
SbGvvG6eWIiqkA5RcA5SxXzn35HBCAMHzFoMkZuOxKfKODUwAVU9AaF6QQ6apyBfNFC74rsli4RJ
ZzGO5bXEi4peqpfnPKVHiI+xVw51wy6T0yVw6SNh3aKRB4HXiQHcrqcgiH8eP6v+cBT0bRsbUMQo
FgUYW6JV1aE9q3uEz+V2kv3dU6XJSxBYn1FR9PdHPAHJpJPKb9XSuHRJyqIJm5qxb7SvavFz6I09
ZRTYn4R3FCSaZIZCzA2SET0jwnLlmYxn87U1Bmh8UvyLwsDbH3LBGoLv0e5cAtTyydsnUc3BlypJ
VdZSkk/rDvSGCjwS8DYFR+PMcfSz8rf+36WJe6iSXetLwfxgDVn2IRWwcqi2OnyEuEKU4WfkJT7H
YZHY2DU+hn+xFYyR2Q64mugH7XIYkNAYOnqCo2jqsKwQ7W8Ob+m4K4v4Dayq8yjVETG8U2n7NDUJ
lCCipsMKtptxjSwgyfKZqlLJ+tNQy6qARR5+EpsS0pcY4zHBBS+TAeoATSQ9/RQ5QGz8GLU4FIGQ
93PMexHssTiuiayR1AvnoBnINTi5bAtsmp2zW05VRzl+2+qbrwfZjdDYRGf3Y178q28tunT/F5V9
dF4N2tiMmgO88SyvXzou5s8rMHbPpkq3R9y8icc1xsbz4FnuQunUfUiHYE/r7wPChXFN6XEmDgXo
tg4oOt9jC04nwoyO9DRU5af1XCa+QWWA7h7SJZ+RQRaW4xT36rSUbOBq7aJ0YftAtjBJbdsq9e9E
RuzAoGEPJqBC07LEWQWc217zws0N+fCpLfqfRoTo1o3CeS3kT/7w5ToXEPmzNCJH4wFZN8CEBqiy
QaeTnEhm4Wn0wJZltgtFKGgL0HEPX8SozRn8+6u2EDnt5niJ//nLvcl7OY8Ab/PiNuNeIc5Hgizo
TpTX4mamLUEGnbvpMfW2Xd53hUsNUw57Ge6Mh2MRJ/J+wAQisrHACwQ4WI4Diz8obJgsUStn1kyh
8khUVt9lZYCOOjf58Jaj1mxY5b8s/0eIviNwdwcnSzr7OjF0nI5AP6dJP8GqrYjagjBVBf1NfIoM
Vh2Qs4RGlfmm/ptrvvItvGcPPNm+1uxVSchCvRFsJnzQkTNad6SyV2+01+geHQWiPG6pIWZICYmG
esu4rhRahsVBy5oq7reGWPjwvdNN3slisQ6GtfCx0m1yYCfSenp6yaueRkMTClhPrLRtfhRuVITo
Th+4MRKxg3gve/+5zaTCZ3cTm3OMnaAVVPxjdpktlW9i4AnFg0I3mBpDjpisum5GvJO9JYqsPKP5
UzM/qW4OEoKoJjhn3vQj0k/97oewUVK4YO7CpoXAgus5iiz1vMol5t0h+PxDx0wjU2q42QPlznHb
PFlxy3DdZqkDpibTAcyLe7OJcEXGS/+lGS6jn6oph2v1zYmNXR9gRKjvzszRiOdyAuerPneGFAAk
MbnbUKLQxzx1s1TlpDlj1NarTlLaOlux8ZtQQ6/7dpv34mVnurnJS64SqF8hyMl9O6Cx8wcCtzgv
t93EwzS017VKAwDmFipzu47b3xBs5GYdmsd0YjwzqBamYG1ontnP+caOJhyCd9KVjrbeeJeAVfc2
d+euIbyMqBreQZVQZlqh6ltozb8Fp9CBCDpX7y9e7O9yMVCL82+EU/jwTp+k5Ml1WFKfOegZlnYd
Ks64ae+zWyZmw8JfY+j8o+gsyAvvGZbbuD6CyNhnO825XPHnSVhbqhzyy6SlbLvLDF2fkW9qniNd
GdXysRnc6be74naQJmLc1CK18ldaD/nBWLZ188E+BN/CeCxFptaLbLtg4eDfYmO/LjbIUYWol/c3
xE4UVOiFRooLHCQz6o6CrGgPcs8Wah336C1CF3jOKZEZu4RM9/DVtiznSn+pFhO6Ejy8Dpu5d2e7
SFYaHfpA1nrbY+tgfjAt4/c7278jWLHB10EJLC7SKOCjWLSM0bnqKrApOlQin2P12Rmot9m27T3g
1bIoXMMwQDZd2da17N5YS8B0IMiXWnKWSdiWWq5QDMAttzFDjcEkctzFDEt84J8EOrmdnpYmCGZS
18nevc8EiDlwh5p8qWwnhLRbMOFHk/lVnVXRhAlVTiqm0DxBSYLXHgO4PrbC8d0fcYIivAeDg3VH
zODScs9o8Ig/6ek5EnPpbeMPUK5O2uGVbUb5K/w6HX9n+/fzYDhOQpueRna8oWM+t/tk7UP7ETr0
a4SXHEtxxJ1ZFEKN+ZNoTXuXeCb2IhFB7cOUQCnDsv7vOGKTKyofPTS+U/hCZj6XVBYfT741RZBc
iyE6EJLoxHbct+qOv+D6GB0inEg9OCgZVhdDYIUHVgtvrPuqnAEOPMpfroZDCaHP4kHtmlAW4D95
iaCG8JDZB1czj/haP0YGNQFcAvR+0qIZau8GQTRNyE0K4d1nhi5JXxiF0vSKc0vmC2psPCr887xe
T8Yah2eZy11vtqbLW6eXCqhipbGqzq6AsdWh2BcF8IYqNm1aR52blvLt4ny9MNyWojsxs/J61xFd
VIOPMwLTUpVRVVk/hwCZ0WzI2/oQcr1e0mdPVl0TGsQdld93qSjCO5b5INRnuWOPrzEVLNh3U8Nv
3vTCTmbYFg7WswMgLVTAZFeEapkmLBjMj0l7L/1MRSfAdwwc0kOkJZk5oE79FC9ZmYqwMWlCkmiG
YZjGWK3RaVJvBUTs/eSj8YN+zASnJTjr7Xsc7fAGnoHPeCZP8R0Eeo+jI5BdmlPQNeL5ktXD5w7h
onYo4l6bdkgtcGF7JHgelSPGma12s48FrJAWtQAD2iQb7aBNR5TMtmZDCsgqZKyCpgPpNJh06mkB
a9LGvHg57y81N5CggUeKMycF2dfwcP7X0GjrYkZkIeOE/Ysidt1NjKh79B9mq/WiwKunB0k+9RJr
nrLGCcagc0ej1TcZB5+GbcgfbTzxuYWlYCV8yNg0D5kKoNAuGSejorBCe84rEA2vowWAKlT4lGw+
7CNRxogQtx51OgXJdOs0rY4TF1VDpUDIzfK2XnpPzw3RsLdRCnzw+0yaE5tD3CHYaBrH9mgO5AUi
0QcBXp3qwHPbEDwk2ixxYO1VfOzBq8RVjbiTkNtj0E/DEJJw+HiquzIOb7G+vZJsBde0DPwzcxh5
exmmWg+ms7N/wwo6QsZe4MHJPMj/QAyizJ97DOVqjkvPyzaHnpvhmpiY3a/ZCeXWDAltXjf27o4P
tKFoSLeSo0Jf/uuRiOwMmHYu9GxDdalGxJxRzE3rHk1lPLj/QR6Xztymv7qWajKpTFly0DJEmJtO
xQ9aFgm46ROCqrJ58PWSLwaJGgUs85kO5z12Bo+sNWkfj4fLM/ceEfT7NsFrVWSu1gDFXAY5JOcg
v115GF9sCknFwMFc8UXdeB2Fg4/3jKm5sLFj1PVThlXjmaT6TWO/YX1NZHKFFeyGmx9d3Oge6MEf
fPUppjLxpLZN1Br14oC2vch0I7j3MUtQz+C62mDcuCByd5u9WZDFX9/P8jxytEVqYg9ZD7RQXMSg
pJER7DeyNNixyaZbMAZttTtFhZRGBjVm8GX47q44mXlk0pMPWQLgCHJubx3mqGcRGPsXBgKSzisI
rE4Rh34LBZEzt4isRNYJL6HKDlLPaj/QlCbSKtyKz1IK3QSKJpc4GlkJ3X35uP3iwlCqBy9e2V9h
wgaa672SCH1iDy6IartBhEHJLA63J+WlAg3hH8LcoCDao7uj9cvpQnFQ10Y/WaqcsZqmvTigpEgu
nL5YNstv1qvFOMfXZE+H3g8ba6vFSQnfqSUCUGzKb+ivwlT1weWJ4zsxdj31IplF8kFdpKADd1ud
LgBpzavlFYWPR+l4eKRO/dkCGMSWfzMLulbauuaMDezwCJ7xfsexBRhv72d49lzux0sddoz+guQ3
7TZ8/wI5zMrIFepw/gGMIC7hlAhUsvOPZ2QI7E0+H7WC8587FTc2rtJG0eOuywCuTG8h+/rGvy3C
y6he5kpUFRW9rNVItWeNMj8j3L+jIK876g52hoh0sCHHukc76cmEg217CjrcH3c9owTCoSN1uEzg
RdEVPNJDgFLYMa0kxMLZdIO5OaE6mfvTJuu6CjqWcFtsYd7fSkQa7fQPy7XZvJKlftbGqu9hRQJa
K8usNbygn5FGc61EUj+GTqSN3/ihrLvZwQaPTDgkj8vuXEWB0McfeuhUeD5D6pfdozgbwnJlMcZ5
Exdcl9tu6cz3Wn118vadRuNNyinbxrNZ4JF2NO60a1uZ9UGTpn0hufdao12uXEthvtyM2msi/dlO
o71ewBSs37TGJZFiphZO0hbkbWKv+MvEJE28DYyMqGi//1SpBZ5VnyvDjMZbAiuWpiB52kzhpLkv
FRYaV/KU6KLv9ryckwLeBRMSdkrCQKTOso22DajDTET8iKltW7FDf6cZ3OL2nhDaVc/Xr/EWM1ht
nxy0j+SDs85pL1WLuyGYHFSvpuvnRp8RR9lBWOuBWIGOpxredFzLcfHQZ8pdPGDq8jeF5Guc74Tm
1/RxjTN+fEFh4o4NVgw+AlJl1HLwsRHZG3qX65z9JEXuBhSmbL9u+yXvWnbjr8j773i77IkGtbMQ
J3e62bKMU7Gml/YFAFZqnbFCP+WRXQ9VBIQ/IVDluY7/tUhogM1NsjigJmYe/CgZm5EuH2+Ou+YQ
X5jYITb/80AGUUgaXWQ9thsAH3S+7+dfh/nSS1FMIvCl910Bn0pOniRZHBl1rKldhbgQqt7YrLfi
bre2yr2/RFxRYnocc+J8v+HyO39c44eWrJ/GoVYudyt/iM2ybHBMMCc8cpz4F67QcTa6Z4pJLnC2
rgsUhAH/+ykQAK/NOLzlmeCEbqsxHyN/dIf4aYo6ObUyCEMPc7lOYzqxGYotvPMqiHelDbAkqgEa
a885krPyVRKdaTL50LIuLlIScQXMs0h++OpcAPgsRZ1xVYXrBk6pWhpV3bhvLyal0lgdE5t1Fizh
iSzGSZ8OnPtd+Rz8fdnky6T4ebvf4pA5ZSVNr3e7fZPMN2kW56R2fRNYbJOXAuAg2xftqmTq7SZa
qBMOsCo2h+UEXEh/qmfrOJQMh5n67atpBQcwiS2xJ69lfFhzXC77/q4DJP4YvzEvAAlDLioxu3Pu
JZkBqD8ldA2ZyOYgtNC18UUC76i1tO66XjpbEkL/4wIeVo2ov3P1cD+hCp37SC4p3Ow+1PgFqnaJ
XhnRBbr/Lgaw489Kqi35ktxJCXx2laQA+00zMIiuwQB14vRQ+wy74JUv40A+ej2M134E5OB5V1e/
DD+jqZWTutWXyDSzb9QLw+UXBMTNqBE7ccGRbs1CbRdTS7ltIFeFOU9B9gHjOb1DS/UnXC3z+rQ0
ormVPyo2OHPNjiQq2/i0aB3oAatdzv2b9htz1pHEye0i2zbLSX7TrBLuu4Ek9YCjFGuCQO0lgNn0
3W23tfdfMIqx/ruFoOh29JlP1rCn0JTKHHr+na10OlTG2Hd1M8o6jVQHb+3N30Lurb0xL3JmaSQG
ZE6OD/37jJaRMozNl2P33iPLNGroKGHLXihGbC1fXNFyuzPXDjT3mmSJcg9STKN0KQOCTEgMLxBJ
Q7RjXzjszmSeFlijSO/R+zj3AF0AOyi8DfuvV1Ym30F802W3bKoUOqJR+TkEh5i3vnCMA5i3JmI5
fOKcoZyIMBTpKPpyz5FUNbjpBCAnJw1TzRFrZXU/h1Rh22PVP1jONy1kbmr27a0lp2NIO2VtJKuu
FsDYYDm0iiGfQAXOFfm0y2bqIw78DennN3zjfLH06DI2Rq6F40DO7Cxn6/J7ebLRcOcrqSQI5SNC
i6pVhmRHKDUvO++avZBNfIuFUlATAKuhHr3y2a9BSd3M1eoe9yXhxHHJUPR40OvOwMW5uEKtrMbW
SBnHzSXaTGRGowEk6FSxGCgIR6WZIQks8fJIpxTxpVhhByA+jwEPUc0YVJbkuZ5wCeGfoVlsyoXL
WwtoAiu+jqJVuFfRV1oItzn5niKl3f99xSCIjv1Dsj4u9X0OdEtcxJo3WvuWcizOs+vYOmW6LCeP
BiFfbVjVXwQMSMNXWm2a/dg1Vw/F2t31E7pr/y0tPuNAbc5NBqX0koCYk+S544amEROlXtmgc3wB
LFBpJ9yJa3/VAu8qUQ+0O2xfHKSW4RjaIp9blcg3oeXBTFxcPazfv6uvc/odRhuRxjBYC3t2WD6d
cjuVuf7AWgJt5N0j/GXFr6jpCO7wwne3+RyvBdj/ZqQF0gq3xrSdz0GK9nKqYwC0V+kYRojPCl9P
1/PThyQccHG2vsWPT7Z+iITWGzQAO5TbMNMD/C5TMCkdl8e91vA57zXtObWS9W61CKIO6z6DXqYo
cfoIkyhR4XNdXwti9FrowBiSLaxH7vtbOF3UxGyn6uedwUWD5ANNOnp8DSO9pfTotQI8kuBkn72f
OFIxFxi34PrLCFeLieA0q8hatNaWC43bUv5SMp/nf0JZ9dPwnZiFx/Re3Xpe0XQ+jnEI4a6MzgO2
3vjuZQyViYsnLk6Tz/RFsyArofL5dGPGUri1kOzfy85FKx/V90IIkFUQacK4D65kVUO+xm83kVXm
C6NXcuznoMD0ckYS1X+3dyou9t0XkTibAKt77Vp21ML751QiL2umClbwuMrahfJo9j0wR5A4b4nz
5LXd2J72XL2Uz6bkt3tGCGXK/abscl7UWtggb3jhkoV9m8pEPC3q5P9od7s6NSZn5gNfXxm3Fa5V
zq5eR4YtDLlIdBpUVX6b0TGHk/zPwiKS7uQFcCwJL8HnRngf2MY8l1mvIUnduKVVOu9sMj7McGfZ
oEmYn9yIKGiPhqFXQSFh+iYpzeYamYkZl5AjLLYmwfivFxd5iKrfdzmr9ygSMpeTh2dn2a7Vqvvt
dsnCije18igPepZu1P9yp1RXITCd2dcFduRzLOgep6ImLAFZN0CkRM4CgRZklem7UX+/FLGmfRWG
oAgzjWiCeS1ilMgij2Z6QOiX79opkGxJd4x53yyt5Hf5tLCyUzYB2KNhOkd3Q5zFIsA3c3kpIPno
BJAZ1/rg3lpbEq0jwOnkZOgQYdcpOWCSr2u6UvW9VEgmcc8AHKMfysGu8NEtilLrpnJu6WnaUMgk
LDDmjfQOapjKuLqpUn30kEWeAp5TltIV+ZiLJz32y7H7cwmzXdh2c8mxYho+XwmjNREoma9GsbCz
ERwrmiEARgnLIjx4/RTuz+Z8evC7zjhntHCH/gFCtEoY46PlF+abKFXVnoXgzo3YCO7WYs7ZPJp2
K/x5GXqYDocbdPY8092quBc5orLGqkNy8qPBFCR157xhfEaX1l7RmeDMaDvK2I8boTafnQxMinBt
jpvjXzZ1jhlfQZZDlthEfY8WKM3TmWIV3LUrkcir4dzTt6gQA5RWX6Ub72tG9jpNnttxkE02ueN4
BX7gWP89eaa+dZHC8XXdiSY2UZQTWz4AfbojJx/rWmeo+WWfYgvcRbsmj/unsoKpAch6uvrVF+sD
V9TBbFq0xLkGqDXd/G/TF6ImORUnVla51AScdCY5d705H+krLPPp/Uk/2SsQq1xy0U37WLeqQfEk
THmE8NtuDgsigoS4I77Lefdge8FaHp01r+PSq8Yz+dJ1pMmAobiLW9skaIgn+1T01IoQfNvLvddx
ZATu791mJ83EqR6pSN5VCaKFqKQZMh/gj9iY0esYlNO7IfT4K6O82vZydUdrwyN/aOIgt4Cd4PUy
lRgr4vTCx5+CYA6YynGCWn9KxnGnhWRdRA8QibuwhPJZuJ6oyauVeVd4JSmQ0qaqnptzvxs7/KVy
Rjkcg7ZwFdKhLllJfh0xsrXuw056aBk6WEL+RcO6WFzvuMooQ8iS7cieix3DVQFExR89kewB+xs4
j6DfEFpxIQFR+BzqaQtzugQqfeo0VrLaiAyAsL8KsxCwK7X+Q6TLqw0oihA5de9DPM/1CZ9y8fD4
DT/62k7R2nUY5Mpd2qcvwu9JwDOI41sNHWs+nwlPWyIhrBbA8hY0Kk7uUeTgMxQBjlLqhyxAQlTw
aNq/58+18B5c0PK3TS3LvOlZvaGmA2mjF0rdfzpb7ArcbONAtMrjGeyrgSOeDOHNO2QOKuSwLa9f
097FTZDO8x66ZqWDG11qhIBlaPb+ZjXEQf0Gt7zm1oMjO2LvLdzKe8bKW3Msk6xyOcOhkKyO3Ocr
S8vPyu81LXvFYsmo8P6ZpjSprUt7porg148DVGclklHo0H1UbWLavVmrKeUaLdVVu2MQnXkQxZUV
XN2/+F/6lqu7tS89U1su+qce3ptu/HejZmRF4u+zQrcKOcNRNE7KLE1NVlCzbQShwx01fI/FO84b
mV6gcL8FN+xv6OOiNdedr0n4Py81H66ODq8nINt0rjA9XYwxMNk88WoYgZMXdU0k6ianNUJPz4WC
jgZSjfxOq0OpzbdgQXyFuGsA2r8laZjlcIwuTq3Vc96JC+w/qdOMk6fqMzhHJ7dDx1mIWDEKMzJL
nufpHXsO1RM0tDx1+4Dx1Pm5iGaH2Tu5Tpw+KAf3kmX8I7K6wIydAS1kpjcs8F38UPk+JUlVqjTo
5mD6w+CiCG5K4RK3U6XC44ceTBsmPCMRgn8BdRtWBQOfWy7fiANvLLwr6J/I8HaIMDgfJmxmOW1s
E3tcWxdcK64JnlAtvgTrSOhQYS6pnPG2Gi1HV56egHy3gfgbGnEictiXKNneVJqWvOvlpvpIfBSA
sBiE5s0fjXj9jqikKW+IGGssAJuHzs5mofoaAwXo8Ge8L9yk5ksZFNfMeoYAo74imSnM0LPhQ1kQ
rIh0W8gTCTAGJ6nx9YL/6QKpogeL7IvVb+LfzaC8t3U5pDf0F+OBEgmA00FnApq0NRioC/AbY/ll
2PpF+oBG3j5MWoIPibplkDt9b/c9dIGDx4qFy/qAFDw+Bb3fLh+Exd+SVZfWqyuwPmXjr9ku4/fW
yQo1MCa4K2zmyGd0v63mk8sB/qpwivVqICuGUiA7Wtx0NkA+TpfavwvE7H3VRFpqOa0imP1OJ+tr
mptFnWfLCIIbR6sujB2PBc0B4OjWQzTFrk1W1cBsHN0yeRCnoAuTX2jKJxHwUk6ZfXSndqtQ3zfU
h6QeDWqXduRRLRG26IFImKeZIWaHhBfWBbwqgAodo1cWobu5BQ4zV9BuyRfewo88JeQM7TMjWTBU
n82K0apMMAqasrZ+mHuuo1seYl9s2yGkTn/OxksBH4Ai5f91NdCwW81EfHcTgQjUrU/UPVIxZn4m
JM0oF1nFlg7ZB7cKAcIqBjYFFN5bK+g4/W+EXxJMISYl9PdhzUnjg98QeQnuSv9chFG9MWWcThQJ
yfeaye5os4jthqSMnL+4bhguBUraVFWxMlxxze3JnU20L6WsJnG7nyDTHyDPM5GhH8ldNXXpXs4s
/MF89nWESuSIbL9PmwRFRygTzLBNyrrmBLJLBo4K72Swyr4+WI99zwY9oY6xMlL696uurb4QY7dq
17w3NCDSXQu446sgmm+i+MiGrJt2zYOQ6nisQn+4doM5wClle0KTKDXIK6usn3ec0iuEmXFhWrrH
FczDU3mLoJLcS8wmGfESBQGBjSIUUMwvxdbHNYOGESRCIekFeox+335o/ax6gBZ3DLBRWSMzQ7ya
MYuVZ0L7saVxywbq62qK3S2500m+stLCgT0mn7CyGWOo1vVKA06tpzlXJR0vFKPrTLS6b4KNUZOX
DjyfoeTVCQz5TvzakTgQAbk/dxNgEM+gVIj58dGTC+AA8ABQKkMuT5U5rq+63F7fB3ME1Pb636+F
z9kptcGkcRd3CgmHh4S2U5D9xfZ2bEVafy4qd+nPnVJohDUY47bMdwoIks8Iak1ZipCt2zrOsuPL
UDNmSVnyssetu3kuI6tVsd5/Ti3n/KZL26YKGyf4xHHx5DkGdGm+WbwBlu3GjZPdIGaBSf33yS41
PWPtryGIWfRj9wpeGr+BoGuFRaB6bVearIONNtQyfn29KI6vBJwYlrhRuE27GoBhdD2COceTzRBY
nNWzyB5edpnMoF9lMJCuaNhlfHr0qcQm8nHBM8UhglbCRxYHYkgo4XjwQEDSh7X3dsnhD0wqYGIb
+248LDVZcjg8L54nPgDpQrSlmEHxOBVv330zY5LRYZcraz5w2q19+Lxc7TecQ7qrd5c4zCxfaGQl
ruI9Rz2yX6t+mJcTc2AvWSgT6M25V4oj+4cpg84V3kxY5VDLPoZUKOUC2+/LmtNn6mbzwU4ycJzt
JivtteD5EiS/uT+cTuSwapcExbbntMn881sq8MKiDJaaNiHX3VP9ASP7DbKQJlx3sRF3k1nShQuX
J2rxiku3c28vZTSnUQWcLHjLdssTdAU/8mx71F+FWCfrBeusTOHmpeYyssiOW22lqyNloKKuyjmm
4HTADRg6NM+TM3kAfcvgPiYuDiFJ4i92ZGWm4tnK4/+AQX1NBsVI/6pVhkIhdUsHM7XgOO24/cQZ
7Ru89N2cWCZEKg2k/9tCBUv5CbNEm1fsmk3GuPMZKb7k0MsauTYl1E7svEtGRwTOVL9i8gWuVU+7
9ZxywkK3bBR4sIT165kYLg5VqQELAZQxpMKDq6f9PolA1gq9lz6I5UxLCLVTyFQKF0b7ugpEHHyX
iliJ/4Cwp4QpVX5cWQ4oZalN5vTo02Qy84NmsueU89hKEqjtRfK9TRo1YoSrjG8br8vv4a91BTJ3
t6K94cXFuYJD3xbR+xepGuFtB0f7fpRWWbIrCqoiO0zGFChEq6Co8DdNuNuyQxlXLGyJiCFgJvT6
tnIFb5+FbP3DrZYzyrbLbpZkcF6qEoMeApskvMNTfv1+VN5srg7p3xf7UhBYXJtB7f5UKzp2qsRO
d0u4cVF1thUFnsDZc/nxuvu1v1XmbfUX0VN4HbBvsg5RmkxR8OqGhNCNuY/Hq5mnjApWsKG9aswQ
3jYJkfpsMDDuux9W73zt448V9KhEMhMrD56MmJjWJbEqhjTFyo3DY5MjxGLgRy3jE+J6NsMFB23g
EUbk3R+Qxedw6+gt/9FRiHbdLkO31tYbLNq8a0BSNa0iFmnmG/zlhIjLooqBchsjdwkY5BIq03NA
G8d/wxHwJXiCauaUk4daFblcqpy7+RtTHsRx+sMQNU7kla0KRUbkwSMXdP/cqy7Hro8h+BUXs9mM
bUOkt1xEWSTJzcp1nUEQEvPnWboXLDS6gEAj8o9Jtd497zFAhpRwfLvnnU+Rm2gw0WWT/ynAI+55
4s3hUTq9pTT2b4XE4i1C7j3XVMJcVmZB2JjoPeeWsraXT/anwcY7G7l7/Y2Kpre9esoxIWVhsIkN
s9mkVpr2I+sFgZGsIA2+SqDp4R5elrExexvNFGFli5kIFdWsXySlAlLYCVihYWlshqmuxVHEDgbg
2dAJtFEOlZJPcL6aMmA3BW+Za6+GaFIyLp5r02dGbwVjcQTqB1K0aIP+tU4n5Wf1T9iB0za7br2M
KpCDN/kGV/ILedbVx6UpX0Wbt4Jr4BdSvCT50Dj81lAh5oV4keJXNJ0vRnL5qvRKf9p+bcOKG78u
53v0aywY7F7JnWdmCBjEAJl/oxFnhtk2FkZYtBN5Abgf9dyA3Rd52epBEyv5OBycrp5xf7oQeg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init is
  signal data0 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal gt0_cpllrefclklost_i : STD_LOGIC;
  signal gt0_cpllreset_t : STD_LOGIC;
  signal gt0_gtrxreset_in1_out : STD_LOGIC;
  signal gt0_gttxreset_in0_out : STD_LOGIC;
  signal gt0_rx_cdrlock_counter : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \gt0_rx_cdrlock_counter0_carry__0_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_3\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_3\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_1 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_2 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_3 : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_3_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_4_n_0\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter_0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal gt0_rx_cdrlocked_i_1_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_reg_n_0 : STD_LOGIC;
  signal gt0_rxuserrdy_t : STD_LOGIC;
  signal gt0_txuserrdy_t : STD_LOGIC;
  signal gtwizard_i_n_0 : STD_LOGIC;
  signal gtwizard_i_n_5 : STD_LOGIC;
  signal gtwizard_i_n_7 : STD_LOGIC;
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of gt0_rx_cdrlock_counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__2\ : label is 35;
begin
gt0_rx_cdrlock_counter0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(2) => gt0_rx_cdrlock_counter0_carry_n_1,
      CO(1) => gt0_rx_cdrlock_counter0_carry_n_2,
      CO(0) => gt0_rx_cdrlock_counter0_carry_n_3,
      CYINIT => gt0_rx_cdrlock_counter(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => gt0_rx_cdrlock_counter(4 downto 1)
    );
\gt0_rx_cdrlock_counter0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__0_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__0_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => gt0_rx_cdrlock_counter(8 downto 5)
    );
\gt0_rx_cdrlock_counter0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__1_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__1_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => gt0_rx_cdrlock_counter(12 downto 9)
    );
\gt0_rx_cdrlock_counter0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(3 downto 0) => \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => data0(13),
      S(3 downto 1) => B"000",
      S(0) => gt0_rx_cdrlock_counter(13)
    );
\gt0_rx_cdrlock_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[0]_i_2_n_0\,
      I1 => gt0_rx_cdrlock_counter(0),
      O => gt0_rx_cdrlock_counter_0(0)
    );
\gt0_rx_cdrlock_counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I1 => gt0_rx_cdrlock_counter(4),
      I2 => gt0_rx_cdrlock_counter(5),
      I3 => gt0_rx_cdrlock_counter(7),
      I4 => gt0_rx_cdrlock_counter(6),
      I5 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      O => \gt0_rx_cdrlock_counter[0]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(10),
      O => gt0_rx_cdrlock_counter_0(10)
    );
\gt0_rx_cdrlock_counter[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(11),
      O => gt0_rx_cdrlock_counter_0(11)
    );
\gt0_rx_cdrlock_counter[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(12),
      O => gt0_rx_cdrlock_counter_0(12)
    );
\gt0_rx_cdrlock_counter[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(13),
      O => gt0_rx_cdrlock_counter_0(13)
    );
\gt0_rx_cdrlock_counter[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(1),
      I1 => gt0_rx_cdrlock_counter(12),
      I2 => gt0_rx_cdrlock_counter(13),
      I3 => gt0_rx_cdrlock_counter(3),
      I4 => gt0_rx_cdrlock_counter(2),
      O => \gt0_rx_cdrlock_counter[13]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(4),
      I1 => gt0_rx_cdrlock_counter(5),
      I2 => gt0_rx_cdrlock_counter(7),
      I3 => gt0_rx_cdrlock_counter(6),
      O => \gt0_rx_cdrlock_counter[13]_i_3_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(9),
      I1 => gt0_rx_cdrlock_counter(8),
      I2 => gt0_rx_cdrlock_counter(10),
      I3 => gt0_rx_cdrlock_counter(11),
      O => \gt0_rx_cdrlock_counter[13]_i_4_n_0\
    );
\gt0_rx_cdrlock_counter[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(1),
      O => gt0_rx_cdrlock_counter_0(1)
    );
\gt0_rx_cdrlock_counter[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(2),
      O => gt0_rx_cdrlock_counter_0(2)
    );
\gt0_rx_cdrlock_counter[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(3),
      O => gt0_rx_cdrlock_counter_0(3)
    );
\gt0_rx_cdrlock_counter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(4),
      O => gt0_rx_cdrlock_counter_0(4)
    );
\gt0_rx_cdrlock_counter[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(5),
      O => gt0_rx_cdrlock_counter_0(5)
    );
\gt0_rx_cdrlock_counter[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(6),
      O => gt0_rx_cdrlock_counter_0(6)
    );
\gt0_rx_cdrlock_counter[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(7),
      O => gt0_rx_cdrlock_counter_0(7)
    );
\gt0_rx_cdrlock_counter[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(8),
      O => gt0_rx_cdrlock_counter_0(8)
    );
\gt0_rx_cdrlock_counter[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(9),
      O => gt0_rx_cdrlock_counter_0(9)
    );
\gt0_rx_cdrlock_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(0),
      Q => gt0_rx_cdrlock_counter(0),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(10),
      Q => gt0_rx_cdrlock_counter(10),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(11),
      Q => gt0_rx_cdrlock_counter(11),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(12),
      Q => gt0_rx_cdrlock_counter(12),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(13),
      Q => gt0_rx_cdrlock_counter(13),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(1),
      Q => gt0_rx_cdrlock_counter(1),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(2),
      Q => gt0_rx_cdrlock_counter(2),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(3),
      Q => gt0_rx_cdrlock_counter(3),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(4),
      Q => gt0_rx_cdrlock_counter(4),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(5),
      Q => gt0_rx_cdrlock_counter(5),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(6),
      Q => gt0_rx_cdrlock_counter(6),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(7),
      Q => gt0_rx_cdrlock_counter(7),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(8),
      Q => gt0_rx_cdrlock_counter(8),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(9),
      Q => gt0_rx_cdrlock_counter(9),
      R => gt0_gtrxreset_in1_out
    );
gt0_rx_cdrlocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => gt0_rx_cdrlocked_reg_n_0,
      O => gt0_rx_cdrlocked_i_1_n_0
    );
gt0_rx_cdrlocked_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlocked_i_1_n_0,
      Q => gt0_rx_cdrlocked_reg_n_0,
      R => gt0_gtrxreset_in1_out
    );
gt0_rxresetfsm_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
     port map (
      \FSM_sequential_rx_state_reg[0]_0\ => gt0_rx_cdrlocked_reg_n_0,
      SR(0) => gt0_gtrxreset_in1_out,
      data_in => rx_fsm_reset_done_int_reg,
      data_out => data_out,
      data_sync_reg1 => gtwizard_i_n_5,
      data_sync_reg1_0 => data_sync_reg1,
      data_sync_reg1_1 => gtwizard_i_n_0,
      data_sync_reg6 => gtxe2_i_4,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gtxe2_i => gtxe2_i_8,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \out\(0)
    );
gt0_txresetfsm_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
     port map (
      data_in => data_in,
      data_sync_reg1 => gtxe2_i_4,
      data_sync_reg1_0 => gtwizard_i_n_7,
      data_sync_reg1_1 => data_sync_reg1,
      data_sync_reg1_2 => gtwizard_i_n_0,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtxe2_i => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0)
    );
gtwizard_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => gt0_gtrxreset_in1_out,
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtwizard_i_n_0,
      gtxe2_i_0 => gtwizard_i_n_5,
      gtxe2_i_1 => gtwizard_i_n_7,
      gtxe2_i_10(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_2(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_7 => gtxe2_i_4,
      gtxe2_i_8(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_9(1 downto 0) => gtxe2_i_6(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
afmoZr9skqIZsiBY7liE8HhN5PT9cSTlE7b7FkfYEnmAz8/sYewHAx0YfvnPtVZDeaUQ1+SA/YSK
81BVyxiW6Q1+w7v5E1h+YJTA02rq79x+8OQbPa0mLIjdXTtNmx+BnF2Qrq3QGTPNoSFginwNNM6R
DHZuBkJnITzBkDYC/to=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
oFhR7iEHB5cYBaPdDcFYa+fK3Gj/00Z1GiQ4ZVkb1MGu+Q6dRi7Lgvhbwd39WT10uvi5m95gOjlX
gGa0fqW2XuRf5YGzXsxqVh0xVmshpf7o2GLRr2CmqpGBtEUVv4LbXJ4WiaEo9wVMVr2rBg6MtuPB
CQBdJcTka6nWZivdNblYGd53J9DObofIqJ9W/NUzFP/DmzVmGbctoa4amp6KUecoKxnO4tESoQzm
lz7s+Zslv3tCv5wVOwV3SAR+Y5ul9hQX8OljyF82Z31ipd+bzojhPg9RhmrvIMbFqzM9eU82pEIT
IZzSVKYMd1P6t1qORfwdgodKT0ZApoyFVxfVeQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
FJ5kCtzgKdagetGF6bdHAPKIu2cq7BcnQFRTWMN8j6rUq4ZaiydIPtRo6ybDhCzgm0ZRPth8Lgd5
+HXk3g/SdsTLZoOUuXnEz+TkfRJdu8kdJ+0y8sJMNK21IEW3pSWsj/n4toksSjLJ1wENuoq4XqDK
9VQo0Gobmx7OOqOYFuY=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
d/VWmC3TdLGZzbtN4vTxv6kYGkcDx9TNbvTz1ALcTDtGFLEUmm0Z0/puE1NDYgBTJquGDZ+j8d2H
/il9HfJ/2n/hTxPKUXm83KvLOZ3eaRdjaEk1wHy+UsjHJ1VztyOuT/nrMaT3sUq1kpP7wkC5I2EK
lQZbp4ngkuiSjWCad10Yoj0TX6DfE58qXjaybvWsINo7H6gMzi4JvYHWbBnCYQS/RkvaQvSsne0i
YvfJuYy63HVFs+Afh6hOO34nbqzXvlsXI7SIicsCUu21AgLUNio4IsL+Uj92NEPjaJnw2wTXrMuH
A1a3i03K7vrZ94AIVwtSXRwfN4q33BWP9bjYdQ==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3xYA7hcw+q7P0QSxtus9heaAij7kiNzMgPuCXFQTIhWPWN0atyWMkDqoxytTvKyCd0IM0n0DheIN
XngnbZmTQpitsZh5f/e8iD/+iWxl+7S7I6wyIivHhUA/yP87+KG/vFGPXu7WexVyPmZuOgSS7sS/
Fnk+QS1l3qltq84DH59g9EBTV7YkcgLEdICkCR0tgP3K8aSVjCptgXzA0onYGrkQESEdw8nPfEvR
g/5lKhHOmmE6kTh/XfO6eQpuQBKOlD7RaVWrPxnTkM9lxrjRZZb3OSfaBLMKhMGrOdEY4RIqfHMG
MBOH8gAUoIRrgyLztGTWc+RsxUUYHq9Hwixqkg==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RyOZOz+qJ5WH/47JnOQuMFPCUAyXetmhoeJiNflULv2xv5JJeXWSOURz6kOeJ5/Jbz2KA/psbOVw
rwZKAlTJoEVMq47lcrgzkIZPdllrnwbNl8hiziEb/YEq2vvpC9EJzY7LlRbQINiQY3hAzP5B/s+N
lYRpAUjOhEst2xZEiFz6cr7zAMz2jn7lHWYuvu75NGUv8JJ7PZ6vUISUeHOPW9ljH0TH4CnOaTpM
Mey1hosrMsp78mwEmy3l4n56Khrq2FOUzGZbi5lhqEebDEU/90LVUpUSzOkhJ03OfvAgBuj9ejqz
WgZRD6DpCk8BVUByKAGJqQZsJ0NcEnE8/btwhQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Q98gl70eQ6J1AYIClxRNKl48Tz7BoxVypGMCmvUy8HqLl3jjFsIYy9amcj+/0Qts3PcCHOutO4e+
ptR4cjDneD+41DiKHcGc2sgfdKq4hG60mda13C9qtjKCuBhg+Zg1jHi3TOh4bVtsLuydPMwaILNj
7ORZo/jzBQ4OUPNFy2tkOMJArdeTOIuydxhUFtpy6FAABlc1SE1Mer24GTLYw0pJtOOJOX61uJuc
ghuBuHaXV8kExCP3IU1qx/oyfIpY9TQiqLKEf0YUrmEzBzV+JXQqn3Jom9PXTay5YJ+PqraK9iFt
ai1YIDay5F0ncLDXiT7OMRFjFG2mL+C96DFEmg==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
DgXXbyNB3XMOH/gmmWb6uBYjn7XQuHgaTP3eav9Y+qTkta/ItNcuqONFgEZl6duKUkQI/YVDFLf5
JwZHmI7U9IACSpdRAsITpVXnabTJhDmcKIxi5anNhIeSW9RerBcxlMYtuoI5FgWJKz6h+0yZmes6
xYMyXTT3gJS5ttPc1n6kBHNZpxPgl/Yv979A0/ZxYkRTfuWrCyaS5T08AajGwOyC+2sH4FjL2xLX
uR9a04wQespOimJ+InC0FAfYhqUcjs5b18x0hSfPhe4G0xKNvmpukNOhIflD4bBgKXl0kqXnpatt
7ejKBpSWKRjk/M7rYOxErj8EahuRFH07pBR9b3XyXF3MfQBwAzV2GIz+A/XzIUDQOcBbojfGwrVA
2HhHfn8sKZB5ktpwF8MBjdlwrtn+kdQ8ux0ZNrgndrvw4nS4TWxZtQeRc+wfb7Q/lIzw6Pc+Ifnn
b7UJlJvNr5ZF2XNxuHGz5L19UYgB0gpH8xWvYpn0Od797z3FfYIiGMNP

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
YW19UgddTwwnJVcciGeEKvH9ZuBWuoo4LgtOhQvaoLIGVBAQAVfk5VY0pzvluZUn2+81fwbTPSLA
O+/RYs39A761jlViEBPYfmojlgjyiOTK9aHXNLsD3WBxfTXGOS3fHhhwgCmiZ0OncriPRHcNAK7N
4cyj3anKb5y4g9gzt9YeJ7T/zkLQuzT4zqnb9ijTisbH6HxpAVXI1ydKtGzVETw+s55Jb0gVXyGi
Q1hVe4oSDBTETiD85J2/A1KF+2DHKkzOJhr53qxvjND2l/LqsA0G/4J35IdLcmuYWAoCJS7cHkTk
wnGOjEnNbP+gboLu5UrXAFwXCo+jsZLVqIzGqg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
58apVjNnALRGpDI2bjJhSWJPQlj57Kat42daGg5JiM8yXDkjxm/BmzBRp9oBmOurwCnZOZcKlj3m
HlexbUqO32l40m0mMLkLzlGYPsduAUijMIb7DMl5HUf9XunE4fC69CyB0KyecsJZI9Ru4XxZrniR
CYqqjLtNjfFEppiRGRN6bXqv2T5Vgbh9qrMDIrkZnrIUB7RBd6xTPZ0xU1V6/f7Oftp0kyRv3C0z
HF7RFTnZ4y90YOsvuCM5CoAyyE/0gTXvBlB4icz61XmHsIlwfUu/xC6yAitX3dikrJ3DWeMC5Soe
vAifs/1pf/61AlzL055eu8EpDBQRH32ikamDmA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GkhzmJz9o93IJx+1EnrQfOwa/OOrgHQT2+XhwLsfJNRk9bxhLz472g/aTSOruRXBYVSa7twJJ6+r
lf0nvm4VQfQ9bG+fbYjOsryrSSKlth3/Bq54UcBz0wLrrf59Jt2KtKCOUQojHAIinG8CLxMBgQJR
wqwzAYtmffGsCYbobk0Y4IPkjQhtQamQd1BTI8ZwvF5zPN8kBYuoFLMFqwet6s+kcH/tQSrB0o/K
lHILn0b8uovehAHNWllP4phB4MeAOgMZbbsniJgqPQEhSd4nOUZs4WnWtYOsf+rCLliHTc9c6HIS
0u5f7mdvO+uC8Pi/nwNFr026nnYUX3YcG4zdBQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18112)
`protect data_block
GBOmcsox0I5FPXJEerPEQrbT+oSPJp3R+j+IyDP4b1bfnvwnLbx0cXUVmFwl4wnPJ8ANkNxGb7RS
qOAbsaSIDtKUWBD7lS/tdywNTZqZvZbe/hCpvNvxV7F6B7Ge8uZJF3VBS1aI3UKzNiuESekvsOox
cLW5217OtIl7j6gkEKESYelT6Xiv22I3cje3QRcRO0/o0ycgvNPPbeB9Dy+c+gCFgXgrJ3z+cqOM
Z3uoc+DGaIL2l9f7Ed0DGoOAmKcR/bhUkDc041dtjNKzr52r6/2whWenjeMKg4jybt+5PKy6OEFq
UhmJd93grZZ8MRsCnth0W/XzwivyfMJw5gJHY3lekBz0Z2rOnbYgbMUDvN/8kCnG0Vs31n6N23wh
Bpn4zouVeAtyjmssQhoqUrPYiUFv1abKAyZRoeKsZuyhJ5YR+m21Fj8Pxe33Ow/cxgpz1f4HmQeH
OUId6I0IKJAE7ErZgvlc+qqydfoe8GXVhW9LZn6n0elIV/VzOrQYV0g9kFMXDvCtm5sn0ftI1RRT
jfu7B2PybunvidnRwMOQS6eyrCSWZip6jtX7ltJzjkARyXN8iPHAlrPBSLQ1Alb+BJBWeThaok3a
AQX2l64TFJ6J525TFfMcV0eWuclRn5mIKz6CnnKNgpijPyuQfzWnPfSWIcAAiSYeSbhNRSThOQcL
jlwKaNHFlUdhpnJtf5JilMfNjjOs+DFDKD7tqOeWDO0xgUZJRZA4BmtExtTxlPR3akpc4SThrYpw
ubx9fN41/GNRcZt+fBPP/N6x6jJ+vnS1Qnq1aHfOcGr9uhGNmISXdooiYXNMgiq/MFYOJpdpZUif
LyPxXWh1RyooY7mMAA3VsttSOr8yjCK3TLPm9XQ76TLTV01PzrlTrhXvcNsftSMhVY4Akq0Ffl/u
9rnKLf4XFkFfcn1tmOxIRca3ipCWILzofHdTPSxa0fZZ5z05bh9WYybJ55VKg6YElWbH0EKOtFvI
PQHq68H7MiRyIbW7X8HHuMHNNlYTSNqb3md6+QhlxtxDeYZc70kfB+ssoYfg46xmbI1Hz1+ZKggp
DAPquOzaxYqRg+JH7hciXczheQyaoWL5NJE77vvPVQpCQhNzbPieB+JynboWHSZcvR7432DungQ6
PY6WVv+NqEiXqpNFS7UxztbMbguSs779PEjArGhBaftGO3UPPPVeV66xrZFT0ID6cIEOLujzpYzb
frdhJUE4pBkmYZOidyMU+Bz6rYHOW+2f1FMvxkNbtQybubun8GLM0F8d0FYviCk2o/FXL5P4/p5D
vU9iM+cL3ZjrrGsWgHFWfqzb0BPNwHZ8joJv3gRaYbHNtJLLdFrhnhJjYsLy73OW0lxhQNnQ3F1/
bfdL8GJexHM86WfGtA4LzZhgLYM3i2bLNe87IyeOZgN6+dTcIWt2BD7Px6hKcXvcSeSavmsBpfY9
5ectb8v3HFJZ1aQ+G/6DKw1/wzb2ODO3Af4kUkbm2/N5I5pvVH+Dp6DenDV7q6Niclo/oVJyXU+n
VPMVRkfZtbDT5NirHDeOyNI8uwlfgO28lHXccyqfqP7+GJ4IpKvAKaQ1G0T38oC48zW8EJE696MQ
JrOtK7SC6T/8tQXu2dCED+51BU9bP8LlxPnCp/lN2cnm2WKm+0sDBok2T5dfcZ4MMDfJ3AAuqY7h
0bWvwgsEXpLmSJhI1k07nK2JCr5GcAttMNBtZyLpmQ3gadmEHoJncgbCZ542otrjQGrGrsVs4G5T
p7sCwxTT6Jhse1X2FmutEVEuVjKF4uv3zl/z1AqNYImeqOXcuQ37bYpCG4EH+1Ig4zlHCRnmlPVO
cvo984WvbyqKV6rJVfjg1kb4Z4kyLck0LSIewl2S63q65hSDq7Epkk8x+Le1J/+Xgb0AivF6XR/V
Yd4LqwvXwbu1bMK3f8WWdxX3RQMAKnH25NPneaziJthmWXWa9F8fl96Ojof+YlqyytZiY6NOAQv3
BjUVQyifkL3D+GsmzsJiPcKN8s6k3Sf4ByvUkE15nHD3CSFWY5u7up3cmRPSWec3+brXW+bIZv8r
vGTw2UCa1CKTpjJgxIMKu6Rvah7lkBLTvsA3QKG28ws77s9RsFiCikCpAcQceqoxiEHqdZakw/XG
bVBqBl06yICd/7jPJpV0jO1iHbxygyhS39LEUAKuYmB2gq8j89Ec1AAHjuLYo7RjPtGAkcKU8bsD
QRWAYNw5LZbsZcLxg/oAmmZ4qEMyCphT2Vcl5/2GA448504U4w+JfDOo9dv6b4/DDkKTJtkYtd1b
6RKA/DuWjg/D5HDl6xUmvLMMXrWp1O6AIiw/8jLtQCaHa8nx/Gni0ptKYlLg9NsKzetaiYQ+Hjry
pduCNuZU7XoXx+Uy/Yg+NX7KGaZFOMceqpR82pxlXKRd6Y65gefzAMlmOS+sgly72WO9XFji80QB
G+hPob+tD1ct8t/9YP14BRtSeEuDAUnlpAKvTP9Ha07Li7nS6s7pmLTewfXDSaRO10m7DizxKpro
Fp/uPoWgXM2sfHiSmVhvOcceo8Ud/7Nm7c7I2c1qmdWwSicyh6fNojco1oSeWPvBOIjQjbFItK9M
dfXOYVDDm0X2M2BGf/Nmmwux5rAa2wxghES2ptVKl96qMnDJRZpcIEGi6bEcB5izDph4V4wtQIrh
i80XFKwZ0M8P1pJ7AMup8kyXjLM+xl0+MRHWqMKcou+wnsnwiQZN4YTAGUNzzJ0z3fmfJqHizd2O
vF86bctX0J30q3996t4pJRM0s0NC36mNoBxZHbPC9ArgZjp0fHiiuxL1jQC6iCUjACTLtw+HIf8l
s0cJ9EP8jDPZ+sidaTaFVSs2CVOcziZNH1teavqDq6xrQgugtpx3t5OYvUoWEL0NgzE2woYg/08T
4H1cyoOaHdKuVCUgWUUZWAew6oRokRf2Nt3WJFpAUis0RXXfk9aax/JlO7VGgIjIU6tkdkRykF2Y
qbbp01emWR9twQmtBLVEqMu/Bml5viE74vyjknZlX4GqssCTCgW8xwncKkpEAJ1Q1WrcUeeVQ+Of
tQKBAPa89gCS+FIyO2rOCm45ppKDbz0X8KhpfXCLhURDfDMKNjAmtIst1UBkJMWvld2ULdGCzZ+k
dyTwDWa568JwSHdk7u2ZjTqtHTqxT32dlLNl/kOpakdSLl36PqRS4MrS0xUpkgDmlfZdAW9d+x19
6LfYzM2cbh3ZeFkIUi+pl9dxEcbdlOT4CwH/OfQuvEvymBjUe2sYKtMrqorwpFhmoZL/vOratsr5
l0A9WBROErfPBu+Lmd7qY67QcheUxGkvubp641zWbLDeCl1NKNM6HLqgN+v8ZYjFFI7sa3vypCp8
Iod8Jj2TPVRrzU9SYBueuzc1wWAR0UXwTGPb0BJ6JR6Ei05bnWM9X7zqXFvmT46jmLAODROSEmQk
ZK/uY1RnETK0/GZSQDXS19z9zS9P9McnK1Z2tpVmyBdgmEp2p26C1iNjifTwdvUeZZk3/IgvUAd4
Gw32KkNhAe33XbCUW0zn2PFihYsJIBuot+MNxKbCcdFLNd34aWj8AmDaxvw1dJ2PsvDahWyPaLtI
SToHQ9OiHyHuG2bPC+1wTugXbKbhvh+jgk5T7fi4p70gz1Ays+S9ST9wD2a4zz8TS1ppqy08FUi9
2aFs0HvbNG90hn/U0GNnghVCHO9m57Eh+61cxDqE4VW9ypK0jaFMin5Tyyzf0m9kkAM+NU/aMSLf
bA8NxRQZ9kmCmCTCZS9nm9c4S3wBJSC9F99v6LBx6m5P5cTjk55KCi5HOe/j242t5ndLU/eN6ePZ
yhq0CMMCvrXNgZ00IM7dHwIGen89L1qCDA2fOu+ag93qFySS5Y2bkCThz+OCBu983CgpPzXsSXe8
er+pjU2jjQ3Upk8enfO9bN7TLrrgefHm6WGMDH6+iK2DkGjTRAGh/osrrUb8hv38/b262FGUl8J1
cjD9hutV71iyZ8dtyeN0vlC0Y6g7B6P0vMDlC3QVu/VBV6BFsM7iRmLXT5im37mw3k8Duxu3daNy
mgZwSrRKbabUy9mUtkVv7q8t/kwzHgIQ+ShoywiYib0OGfTt/zf7GAY7SPAsG0cGzP3bIuvx6vkP
gowcqGEm8R1d8VQ6w80I7/26JYKlxZh4ka1rymA275bCrxexd6gpXsY+U5ozymVQ1Ykbf38AHvTc
5E68/oV+srJidnEPs2Uw71bs7AxHyEcVI7Sb9Nr1J2EgtMvH3bbumfn8M7Q5/Y8GeXCQg906yN77
bLRbB+YCDWp9mLrQcUnVCxmKq31+xwawqtixNLrvt6zYmtal+LuqW+4jTcBXXt+TUgR0/iyMrNk1
MTKzSw35U9NwBn2S+xkwAurM7iv4BDdPS+w051zkwPV7Oo+rEMLKP4HJumIriSTd7Z/QWHEl9dhz
P1heEyhAVjqxdDsqklu5nyi/XgJS5nMm3Eo/+eBzqNP8WtO4BDcqFNwgDK74Weyzf/D4mXebnRWM
aXwzb2uxbfKDhGiVbw4vln2kHP2rLF4mSXgIDnWcTC1/fx1ZryKLiFNbUgQXqwkoBIZKGsMkLzcR
ZnWM6yuv0POJZE5W+J4PkbI1kxe9PPDxRuqQV/Z/0VTflTaKYvekAWY0S9YS3Rp9oymi77tbkKjV
RCVSFKAxezRCdqb/3aOsGm2tZW64K2HboIZlwq2wI5ji3UVfPwGgiOfJIyHioiPo+KWUOk188nWG
jztgi4gmCzUcDGSVQuV7pEizny78tRRS5065l+CgEme3sXTihXB54XHSPvrI0EIl4ezGOCfULAnk
xz0KcSIDbaZclQu2ryWFJDuXp52F7csKGRbd+RqE8iw2tYWm+3AKVUaeOcuxJ+x5XconZEUlRZYi
dqpTiOMrvIS4cpXBLU0K8RMNHvWb+fLu4tuTUIromU++iawhHq7H5knHHcM4TPBE+vT4CfoJ38wA
8OQhCIL1v7/ta8kTDY7Y2NBjFw/4KDJJPAq5tNwspMfH7kyTXlzR9fGepCAY/+Ow8q/geguq2prr
96wTlUA0vmB8lbh8o0qppYf6MwRv+2NeQKBn8JU78s2LUxiBRQEcj2sv6g7kO0bLIZHWOnEgYfG3
QRQSNfHpx5MkB7ZT1B0iHomquotAvqEnWI16jGfMWXFcSo7ognvSxcC9qhzKW7W6ZGFODQnhi/sR
sAGyjG5aaLJCfQNyrIsV25MJc7JtEyDV/aoUdVz+1lni0pl8T0u662IlSGI2FcO0HW3dEmodgOqt
pqN+d7Rm+cflYi2VHWj/CB8cMXKEb3CqzEcftb4kjrtDJ40fxOFoC+vnlKuJiBygLdZ/h3QuZxbn
vlU9Nt7INvxDT/3dY5gmVNSuHV3ebkvntU/yNNaqxPAC99u6rMG55Nkhus9v/LfEYn9C3kADaoou
L7rFeEIybKjGwqOicQqClCW8o2v66dWume3bxP/rvmzp+ucJigULpYReE1rrMUapyomkkMSdxYTK
IUaLPycFjL3J8iSI1QHneAeHoSFR5qx/NdLQ1Q6it+ekJ4oX08TDFAzDIRBhbAexneEZZNJQhbPG
qY7JvaNAIshgNLy83FzGoBArTI/rSxUq5MOeeQb76xyODIR8EG4HiGB4gvAkrDgyy600ddObVHVE
1SHjIAHM3gbog7tCSXgyA5s3a3G+B0KDaJasmUnwBZfb4SX+9ZScM7xVTrf9332dGNIXWf+k1feh
AdgvHs2x80/vBRndVCE+EEQyXf+WAvt7X1QG/hx29WtJTFcq++xoUbs5NK5gA1PgixtpoXHVDrze
EcaxMkkvdEl7t/MLEdZk5qgDGwTnjOAx8v09mRnfGAFMnTG3LATYFtqW+NyBB8idZl+aWrSQ+EFl
PzRXDgRfQEZWfy6Y9oJwh+NTS1LhRG4NqhDXT73KSy9malnpz45CgZfS6QxbdCJIyBKEJTDIiTAq
5Bqyr69M1acXyiQgw5lRqGXAW1nPl7VrNKlzWXNg06XaBE/sWidd9GJcaaTAIS8nZUl5OlodYZBn
RKPsJmZctJsNULPo/RdEMo+jHk58U7Tp9069gslBXjAQAH+izEzJm36dKqIR//YqLSO4crBg9gCh
BAWgBtnrtdY0vNOkIQE+uWwEjd9Igz9i3oZIfTqiZTD6fme/8ftbgZwQoXFGDjPYnX6d7/Ra1LCR
/xEZjbI7zTpIOqXO0tFIcpEygZy7kgtjrmXZ2HHa3VA1VtSE7o48M40TazZaCq5bQvPK64i3x5P7
n3om8m0ifkRFQfb+8vqnQ3G9up1/V7hLPK4EBLCU14qt5t05k3FiJMnboab3CumWZhOnLDD8g22r
bkyxRUfNHGVAXalkAKu0frgNPjW8OABdb3LNxiyknTIsaGD+tIyai0iQVbmI8EygKohn+PTXSj+n
tDQL2/r3xqMfNXfsNE4Xnur1+yXsnAMSuOD/b96RiVFY/kK2hZJZcgp/YJevv+Vbepeld3vcVxnf
ZC7IISUKdN6+/MKyL3HJaHUaqdbwUsxImHsf5CW0O+rIbJeEsNT0BC85m/RZHUZznHCdNUMUNySU
TiSpKsdZE3mwSmDSUjJVgLmLDFTakhRSMnWjtQibj2EPmbZWvmQHzGv4VN4UZ71WmlY3SQCWmDCJ
C7LtnPrNaoszH1NWh5I0Ipj52bdKnBwxyL+H20qtgMgyRjbRLrpuhiGqeRCifW5C0HXSs6fHE2Md
K4xumPBxCaDaO58xWbDdnzKnS53HOmrM3lBvqDAsbQX5gGraY6emPLBsODARBAv9zs5mXoHoEfEJ
nAe0x/057qa2BJY+P+4plahZBQCNLqSgN/yfvfe8FPLthnaTl/YAPuPaB2zUF/ix+KfyYPowYCNU
C86xELcnDgYkay0UdDGO+LIlTfr1ycJaaRPj0+dHdFGQXJWRcoARx5hgPkZ46KQEixLfnKVCEMsl
kqthRotes/zomYCtyngrIRd8uChvfq4wfv5nPkaFuRCgf8+eWPvkRcpIRenD2OBLSh2O4mMqEU9O
mXV0kvpG53bajjV8bsdufRAELzKKGZSEJTULYBPY4YWydb/nkYkJwQw7RZ+1daEbYPpkILtzW3hc
CD1bKOb0CYbrBDAx9jZrkVe7gzPEH2riIymYrVqmv43dv1SilWJYaR0/4kyii8j/z5IMMpyK+ZzK
HOdh1vTMEoVxkICuQWG/WNbDRCo8FQn/AIdOVYKY7TX8h/RWDXn6fp1iw57MlTjsgZrIMcY1QPuJ
BECee64WPBq0xInaSbMp5t0wJmfYC7x1g6rqAthW0ZFy43+2n9lLGFRhTv7JXJ1jTIK7oArJQKWi
Azhb5yskwu6cITqgSoZDUAxNpaw9sjuEU9o0MxXLZSf2qpGKpS07sgGc05CF+K0qY1qEK2BwH/mI
ASzgzUmrGiaRLvioxLMZQLQtHU5mrgiZH36Kx7GbAgoXvM1+nHSPDXj5SLMw4Bjb7JvePKtGnoth
plZV4MikqpqByXDmQn77scrIcQov0xk5U2Qfu2zPIaUBPRkSkxa0FK9OiFVNeIT3yO0xRHSVsfS7
1ageJw9rKe+bmkbFN90kFpwEESVfhkaBDQ1+FyA3/KMm0qPqZXJYOipRXVgFeUiSKVLC5+LUToAN
IL4Tkr24cu2Dkn80nZJCYJDP7ZaXpEuXEJAAp5UxOU+4zzBtCsx1+AjqFhjblY9dmu9ZASmG8DCO
v1Juy+LJqdVeXCI3UTznx+VkllUYv2P4mMrYp+cEbG7KxRZSprMuXK58/4+4Zp+RvxqhKbZBJztn
HzgB+kBKot3Zx55vuwTPJwhbFPYBBS9n5Atm9djfRwak7rA88d1/crhVFUNwdAhCJ0f95n9nPNhu
+n5k6gJ6NHNVR6EYT6ViFe6LRraSG7xZ0Zf+ANwz2m/H7EaxxGgdnWbpJDETrApsxJl1rRzc0elT
35XcKdeBwteZ+TCUjCOu3w4IyuleTxjOkSi1RoaOANkIkybV6YCWskJ+sIk93oFiGdho3GWpXCmK
QcLqh+pcS8Vvpv/18lk1CVfkUDEtCgCoyVW3U4EL81SFqRFk2AYJ0dohQDCLgxVJFQ8H1md7Tca/
JvPcKk1KwWGSeeZPhrRcfemKjWWNVMs6nyD2pprf7S89Fq+frvxdbhsVooPItlcYUhqUo/al1rFS
Y+WYSjez0YEwu4U7y19w7VcJn9nWCbYa1Clkj4h82r/MIXsNyN+7RiRx5EcbyY5kssFORZsiIMlP
J9jO/05jkNH6mbBjJy8s3afUcSa8jtQsn9C9/O6txQDhozSSKNBF4Cx7BzWyKsKYTSajRKIeE0rc
bnwrBA3M0QscJhXJeHwYYoHjh41qD+1bgPZA/mVyLwqPBtC1v1+TGwnWifeFozoyKwK7ZktJLXBa
Nj97kJMQrvm4ISIv4I9FX4AGq4QXAmblalLNjbWxf1BQJdgbm5YnYdFP3Ts7eJsPyRXDT3ZNgyJ3
HpefEvpSwew9LZtPNb1xx09CFEbSpaYzPc0Kp9hGziI0WdCqorBpD4aNrMx6AH+v8TifHANHA9P2
T29zUarVHyOvxPCSSRNUUnKtLdTxfTDv38IqOchljSXn4dyKdKJYn6oFnBwsX8QR9XAsHTfogh6h
k6UBbeQHJzAHANxJlINO5tkBhfmKXRppeH34YdBXmqc4PfivoiQ0tKmAha6/mVj1owfTUqDvdGkj
Pxod7YrLu1B/EjVfICRdgFZpPvV7vlXhn7+1t6pryxa3MLeg0jrnjPP4CRtD7o8ih7Ntz5ZrPFhH
2bZEDto42sg5fQY9flPiiBT+zia412o1SgFm9/iDFVp/ZXdTK+wdwh3AKqTW12Kzd+RfiqqJdA94
jSqyDIQ+rnocpxWE9Go7PtbiraAOk+E+vAKIDu8fG+mA+pPZAM2HhD7yQdZvtw3Zc3ymV5JAH7XU
jkOElmtniJZoCb8oLZR53Q6HUgysChu5Smz4adpgiLR+Ba6vGXyGUVWgfux9E05xHfLDocQMwhlG
DKoSMHRPri1Zc8Ip+nZaoBg/me5s2yjolYnk+KQnqJWcGrdmGJqTwWP4R2Rjdf1LpoONeX42dlU8
adaw4N92QiwKoX+gNbsFEsaTbJx+3edWCb4iQZafw168Uz9HLpOtdwQqA79gkK7I4Mz1Qus/SaqR
tx8FKvuh3leVoAF5uQkQ0U6qvOGfFsQr4atImLZAZ6d8w30KUp2Amru7xJ/vdxZsQPSqEGdiPI0t
21bZb2wCMiKrKQOCVmDxawtzU0JZnnGwbaom6+SUv1c3Gc3y6EiZ0TQrQq7USHzHmVKqASI7uZN1
SOmPeaEm7/SSYykd1H5BF19l2ffwzG/2s8T6plgjJkUD361afpOzodfEIJl/rBRkqfHzdnNY4zoI
DtM4lxDdsLW5Ym4cyZzA4lxOiyOrFoFr8nx2qErr5FZ6MGN7v4uL+KZP5HUJIEhtOaPAccE2g9ys
5lBoDoZFkFZF35NUp0IPQMoPE3U+MDdyaVFig6Ry42TgwLyVuRyV2vfxYzsmWTsor/lqemCrIPnZ
F/fFpCXNalUDELxSAd7JcO4//awh8VN5fTRHzSTuhMz3A+doKiSZUSXARTIs6mss1T8/LjzJMmrS
6SUPJzjd16uENafPkoCyc2mVNspzeuoe28x2nFlrWv92cfGH+nh1GEaqUVUA+Wi4ReFikFK+hyN4
bah2JdBbHT0NRIvF38wVZ3ol/9TcbDjPNisnXQwK+sn7wmbqS/grXdhXFTixImKf8/iTR8qaOr9D
8RLRNGittPvRbu6ZzxyyieHvqRto8KDja8caEzJ8Em+UNfYg5S9/yDojnE018XeP4ni8TmA4qb+C
Tf7x+Myf0LdrLHU91x2oZaqomevevOcuwze01mXYiWM9m7e04PZGc3msWpY8ccMGUgRSkpN/SXwO
xVj2HX4drZmOCH5OOjmnffioqQquqzdWOwk3dFFSZw31ohhhvdYdZl5jk1i9yTOZKq/A3PQsKUni
f7pZruudtX4xJRaotrhpk5Iu2qhEFmCs1LEkcc9hiHBiZuSkue+IExPOJKAypnVqSPEuPm1c/13o
P7uRUfivm8EQ10bKtZbPmGp27w+dCxrdZkuhll2x6mxiSplM4oFseXTHjlowCHRim4Nph/dzWfU8
qJEvsN0USarb958CtjYVBQxD/QtabErFXR3QTSunwSNpguPuEfIhJhx33a3jJrX8M1lWRci1j5ux
i1R63ousP1bvazl228m+gujGNY5jLmhaWlYYczYwBz+O/KgAFN9DGdOvhxHkRXPZ0olzh+ouTrRn
p1MuQGcy50TjmpLt0fg4R45oUwlzVMHCJoul9/EEIeYsrsUCD4e6BVb/3AcDFdd3zdeAiMOGlRSh
VGCIPbgBaPaLD3HFhLHIGUrP0v3WkKaFj+J/0D+/pJ/2vqRXO2QxbrTVDTwtlc/pHtZu4C9cpLS1
QUlxx5L3ydtF/q1nrc0OYdGc2Y/OyBuv8Id0CrBVQwFDLDEvk2sKvejBARmNK/QrY4/Vh4Y79nql
Q73RBEIgwUZreyR/FoJohg1NIfXWQWHS5Tr/k47P/D3/+avmgyPYOmMxJ13KzQCG7CdferV67GGW
NQCnN6hLCpSJ3XMp961bccJGGivVJ5Ddlf4Pqu6HPGnNZIHg4AXshxoQ7Wigmau75qNlYOJMmZSJ
9GtEVZxumRwe/8eHjHnfz05df/ih6GA2fPnmEjaCN4/b0/3p7dn8uo83njBENwrOiszJRvXXz0Zl
0gdpZz33ih/iuzMF7NxbKCeUjsaMF5ezFKzFrFoECJHJHWPJpdtorlX5Oww9/iB/59nnU9nzkLTf
Y6fjpQjSgL49D7EtqO/PltUSQUXnY8cTTgl4VupVJjJRy88kK8gSCDBhezYeo2Tho5YQlQJPzVBu
lcJSBFFm3bsA9UlXgPIVLYbm3o57wCIqvlXsAzdLrVVXxMz61+Z3Z2frXeTdGwAPcwTv8OHS9Vjw
7ChGY3zZgAI3sQDXdf4erqPjHnwRrqQ7yCDd0cXlcvFIpra2N+jLwtRoNuxHDVZREowqTa4U864f
LHrYT3j5+rzNcxrmBAXFbe1pJv41AVYImKKa1dDlkcUAKxUe43hs+Td9NFDjGTIGxQbyJphPVXNf
4m3+JoDvQoDjsB/T45JlN23ut2QdNlDHWKh7mFGKWP+KatBChMQdvz56ZTapLomkMopalNKiSInh
s/hGGJID67X7n18sW71l4j84X2nNl/OOfMoDKczjnNA4x4Wmr70vBx0VLUT22FHeJdLGUPJcRFsL
62Z+cMNO0s29AptyTsRL/H/tiW4zgohr0W31s+haNOoEwjXhcQoegsVTf+BQHr9arbxlAjCo3bo2
sZ1yZu1xyeGtwkpRx768Aud3eYjIuJAJ9bcf/+kOqbkGaPGoxXX3RveAHd75I7jdRYyYSATjqZ/o
8/04VjUt/hiq9Se8IwRvX8AZpnzjR0FXP4edmcQlD+x+xe3cypdLunMD4suYGbMjUH+sWE3PBnBd
fLzPJiSV5dbnEAEyCgfW+RK0w+r/TkPnNr5iY06l7sgSSt+M4oR/v1KXglriLGNC0eefLB49MNDJ
P9u+A+py2Qpb1AZbgrzC1aN9uQ8fym3a2lOuYHJ3CFa4KLzyLgl7WaDT5fU43z0VUvG0VMBgdD18
HnK2KrhQxL1OMwCxoGOxyzPFny8qihZMhq1y46P0WVNkJ9MPJ/lly8UL+APxXngRopvaCLlIDDah
lF2joIxug93FHggQvRivcbbPrPJpcOeT87I6QKXH0+p6AZND3/ojKRn7IZlbo/Avas0son9/jshz
ssljIfesHzN5/D3CaPktyA6MeUHh3VMlL34J+T8St1ASl8jiRp8ndWkS6XSN6RadtkEkvXR9o5k4
RPkKej4Bm8k4v5OdpOb0Unf3lNC9ksQT4zR7OjSxrNxijhtkuSJRAQsJfM4vvBebvovTpq7750jT
/NvZqV2eldB1yGlPgTh4uFFohIIrUpmuN5JUEUv5VGHl+Xs0zQNjmeBoE54/ooIDzRJjQqsap2O0
nVcS1yHGjw9cmT4DXFhHt8dPWEiCCM5lP0N9/G+5QAXgXzlu+vAWSiOvaEWbtyjnF1yn9JlN85Zj
vH6K2LxaGZuP2GP774OZVUtnuPCsv6EIFtnDsBaN3KspqFGp5vIH3l2VEgLGEwhYyqdr9mhcCEnP
h52+FGWYTJEX8ZFJqgzOs9qgaazu7ZQs/6O+B2bdOmuEH3n/JLjoc39YJnK9zr/2Irije2rusCbS
KCrJsu99kLIjSFTWoKqjok1Ct1qcfhtu8kVZ9Mw1eoOtbiI2PGllghv/nIHNmDQuN9vBEGmLUi2R
wAgyyiFkU7B3e8dfi51FCUwDH9WGWAQVDZoVJmUpbq1TqW3SYAmXnvZCW9I33jNQYCncejy2sExY
7Vf6FBLWjel4fy8dyp1Bo2zmjTMLrHKOXsQHKthMionnqaeRCAA5shybasX4r1yXUuiDpefhuubs
7BRV1sb2xoTQ3De28KXq/LHOksInbchBnfmVLOHtQuybemT9Ezr4GZNJ2pVh5+DTBiNmG+Tzei9T
dxHRhEcoeW8Pxafv04nTXltfjAxPl0/3INVo7fOgk5n3ztrTawqvmfBFcLBaD6yMbWreJdmqGzL7
0HclSUZWr26bmTsr8JIxJ8HWilTChSBOACLyRHwtOwCJIOmbq+lwUSrJaYbCw4Mij15ZGVGinh+u
65V11KeNN7E77KWCNh0CMmnEjZn+Lia+UAGB3j+CoDRDB3Dt/KWq8IrZj+tDaIgBmaWuBZ6rv9nZ
qHa4FikUiGYf/yMwmMut5dvdQNu1Z7+TUMnvC6eD6uXFuMs2XwxhLGXaadZMGDAweIYoBGu6rCVd
EBQARrhsdjQkZumsWeFZxvbaAYqNSWTDNpK1ch15OYhjGbQvmPh95VtjwxoB2CumxqVCQQSbcaIn
w/dAVdGdVsaltkXku2qO9nMya/4nzCpf4MQ6WfxT7Wwb+RbLb5Q6LrKcrn2GU7uUnyLucbjWtzQ6
3X3mgnKWTjftuzFjAi2DqU1N5Plso/vJqwsI2eVyL7Aux4NoPyole2FPa42DA64jWMHKEhHO2byA
/AWOudFDZwD/ReR/0Clm6FTcQBYoiCalVPXi5HKN/h16Xr2guopM4usY9Vfb+NjVA6/CbR74z3JD
l91ZvoIYSYd4VN3fGuta1NAZVrRNPY438BABFuMZkZM+BWj/rsAU8bbc4zzAa39FuJuBLXqEZBCc
iR5PBgeY3V0+MMk8fRTnH8GIgKVKh+aNiogkfSSBNcS+YDq4JOePz8NxKrBdZ86TrrKZ8lwhx6wx
bdYPqcdHM172yO4dS1Rqg7WR8kySL/fqDD87Lx6kmqJAG2I63meTEdeoB+Y+hN/ulZdxB50R/hH2
7g+cwwDAdoWNO7jXAb+0s6RHFek3BhOfqy9nv/tzKrrfnpCFadA4iXs8bNiQBSBDUSk+MiYiVSC3
XBjinVU4MlPizfxWTOYiPx5YOQsJK0GlbWVAzoaoTZ7of4dGWlBmH7n/G1+heD0qS+xFQAMCYrVC
k2yMbPcM0NipPIIpQ9T1uO5pWzrZlfBb9lxI4BJp1178ZwVFD82w6sme3YG/Fro/CTNJoj9O74Zg
Dn+UKJ+rsGBcIEIzVjTxeMjqOO9Wo5KzIpqfsqHHpjOiFcy2we4hsrjURT9akOdmGOKoIZFSVSub
hPoRnaihJ8bWT77/7bzt/OaZIZC9IosLVpMEdHCBEi2l7r1R+zG+M13Ft6w5PisPRhlEX36/h2AH
vxliEjCawhx05LmEvHTfojLVt2PdkEMZedtt24fczOkcVyEMAQFi4mVhJsp+vrhHFWUSQHUV5jbp
9lI8fAGhdk9dnJZOQyxehDB+D3PfFW8G7FHBt2nTRZjyw8vR/OynXzYZnSzIyEV4YN/AIL+8fcVf
gmXbQadxzCUQDAoMxgoAyW3lMtmor3b5bYy6MV7LHgd4luPaK7BuvYAogt97fNDSSJeRWqIIlmWZ
gR5T5T+poTMBYXdSPbfKJwKpylPC991eQNtoM5/CcaVfMvM/NwmINGS8Hf8dyN/nPSlO5cEuQl+5
5ZtdvNl9zDuanN7b4Hvwt4fZeWCc3DpmCVV2jwfN4HqDJBNOYIlJb0Mapi7XRpkTwdm2i2FcVN3K
PrQDSn//lrjdiyFCRO4qW7aVUEgIBXs0KNcGthc0h1KcoPVAHXad4B8K+83sYZ6NkIK0KqB2sBME
uHZZnErmnKEiM49/55Yrm/95dJ8EgOKnqrfsJCJdrpXjQ/Es+Pu5Cp1lAW0XsMhXWIFJcnTgYbnh
OKLxsLAPn3sIbNzQytrYHHNr33TqUtmYhT66hCpOBkMytpgdyUx15Ux64u40Y3YNC2Ryl8FF5YcZ
QG0r2W4VOjvWMfb8xywtZDBAEXzGjUboZ+D/DS3M7AP86eBR864D4X820oPQDMk+ttpHl4Y4p/69
L0DcDfAmEsCUiacEI1lt4dpPZr77ZsaPPbKsVAsBS2qPsJOmSXtT9oCQLzvsYMm183C3LT3mbdHN
8wLm/9zYzjcA4/AB9LTRm8vmMmxkhzKlcV1fc80YitlwVhv+U3yO7Safa9i935lWFqLXiZGQN/Do
FKg4k+CZQBil4PeOu8k3WdvuyJ6fSp0QM6H5FqyA07TwvZ8sX2dtysmZ2eGJEcoYhP9Bt91qOEgS
P8btth5GIagdYzlR1JFNsx0PzHSr/TNK/jkTvOXqumGUVtO59Fm6fBHP1tHBKO8t5PY5AdlDc4ni
b19eZJKrHlYKoKs9SvVSyaI6WieTqW+znxAlc4HpHWLABUqdnCeavIrueT+fIhOfEqPBmn2TJ25U
DyFgV5JyAXJlsbC/aGUG9Wp5HlYc2d7yk2BS1FmD94GXETqoz7YDpDHATEotsF4d/tUO7s03lhBv
/QMzDa1pA8Tmx/7GsD9N87ty7T9hwf6sJhekJcltMbH4L2mnkN7OnSuKjYzKcihP1lMhBUyDtotF
Z5QxfHzpBLHAcvLK1tNX5sIyKxm/ZvTjCuBioYeebwG88NAOlaEWcaSF7rTO/k8T4+/6t03X2Pdg
A40pQRAC1kW8jTditmNxgBxj1zuil7XEcfBl9Bi6EiNcOhUVvZgF1t36mnyysyNaN8JBhV1TyIg5
3bs15io36+xLhcp8I/Tq1FYQLjVNLStgaiDrvThdDba7j55tz13LhBCNJ/KuCouXgUU6ti0TkSGK
BM0LxxM/fWU2OFft3mCUW4o1kZ5YXC15mNcTLtR4PZLtmEk8ot7HXEu+uOM8gEJZPjJ38+RpZ7Vt
Rrd3EIQnecGxYD73iIO/J3mu3CST7LHC60xSzWo73RcKNHzhTWn/3SvAFzYzrc1N6gEP1glRT2g/
AZOyrGgK4qQBzx3urUVgsKfboefPQXb65czwSm2hbSby+cSALIVkiF3bAct18hi1C/aISFdnHcCd
nLCBfijBBSsXZx2EC5wLrawtFjZPPGccOplIsILsevLYkgFkqlRfTwH1L+dnBqDfEF3kMSAwiGHH
TnQ5QKy+bZYUGOYJZUGG8SUslgCDIZ7EHpZaIXgnbqJsUkjdMeCVaxZU4kY803yiVMTpgxFPYBLV
adgDtaEol/jZ2KnlqOcvMR3rLXTw69+cYCZT6WQaZyGMPn0iEI+OtIonYtKo04hLifigHjJxGhwl
rX8+RXFCzc6QYHALyPOCgm2JBgyR94SqyzlDkRTf/lli0Bk7tMHR/zF1vMGEIPQxnzUiqtWUuhl/
2S5VuK+2E1ahe7IyswmuGfK60qWAifs4RT/Xxluf1pNBk5i6z9IOUUYCVgNv1ktsphMaA/dYmild
JtOx96YQr2nj2afjveVED/uPcy6pPXnXUsZlS61MlQsq44N+jl+/fgQ7odBUI4UI98U39M3z0dhL
IsdV0JOKNHZY4yR5dojLFRNLaoyguFAMIjebyY89u+LVeA7KaCR1P9+wmCsbONkyAVYmv4urjIRj
s0NC54gEjDX1IIDu4dVd/CqHPRt6mAEahkk241nnwJs+je3q/Y1BBdKNzqOQdIXl1TIHSdkdshq9
Q5Cp4yIDD+m6/nz9/fvSNFTkd+vy+saiF05KCBafjbEjieUNQSdqf1CnRehOlZBWOQK7tm8N5kJQ
BUnY+07g1iUiqbl6wUK2Tvy5iKSkEdeWga6HFTCCEXNmegnTamvFodnOpuhNpkcCqVG2kOWSAInb
XrrIHlLjKZNC6K4QMo4d1f1gGUCE3MaB1/5U48YPOwfoyOAb0QhkgtDtDe6DN6MqqPPaUgK1K8iH
XF666TNS73JHpH49+iEZuMy4DdGjVkGdqoNTd0w+Ki3QL1cGdzYMskoY7CacIEYP0sGad5w9UnlF
yVXmvskDkoPJTQHOliXifnHmywLekHla1rDA21mO8ELMEysNl4KmULuZDs8aZ543aNhOb/9bkWzO
GWOa7VVeZa4joEqlxdFxSZtd21043zs/68WnSyMNbPEhmrCHd1mS9yv1x11Jlhj4dARzPX38AfdF
vTMMO3E0vjPwxjB/R0KRTOooMKzEZ6UwtbXDePVaxP0w+oqLvlC5V9sl8dPgLmJRpgk6PsXSfRt/
OlLElSWmWZKjXw2sgIPtCmk4c+rXpBAH6PvImqYO2UcYbJ9V1t9Pd8TrE+hJ4jYFs8qp0ec73ODM
6bvFssTkRV74cxx2OVsUWwYq33H7X3EyD1ZIDinBh1qvzAm1cPVVdYXw/r+Iz1UpIIvT8WXkqcsW
hbhTYe8nJ2xlD7EmcvIv4/AIewTN6pCCXoSsE1r+7rykQjIwJvHEGLq1xTWsUyalCzx1WBMonqsw
4lNGkSRoqh54w/fqXzTtraN9jZjvpCPV21sgcUpblnt8xHTdCCpm1I/q0aQEeuEYTOHW00WJwzv2
W1tCl1BQzqk9TpMiO/xRXTuvrYnNtJ3pChJGugyba7Ccw9jHkMT9b8wyQx+Mo/sNPKnMsQeXVl++
EfPDBGbeXyFN1SeACMsRH9IsbV0a1nqC5KF/Re9acUm1/6iq2oMnrXPq494sErBuiShjlg/452nU
DedPrlmnU7hw/qxkgZjHut2qxll7UvdA12uIZavW9Z9XYrrmNquSx8/qHUfFGCVw7o7Lo5vq8nLZ
qmVFYei4wbOtYZ92KJdzy/54BFWDqOB+Ysnxx0d0hz0r52tWzrz/f2JbgrPC06J8sZORzArJ2ZyB
CDo3Wwvfp3OWUHv0qjd1R051sRvtSV20K7/rKE2tMlocqefvb+97rwcVndhtZRdaGzDF9aoQ2I/v
ESj3yJMN7vKfWMnMAYhTvTENfwnSriPZRqOkGcD1xllS+qPKQfD9Y7F3FTYsI6C+WP+xhq2O1mjX
HTazPox5iW9cj+KEYC3sReQDeXfMzoFroEDOm//TPDe55ArQ0v4VmlNtahUCrjGBttsq6ZvaEiYH
zKXDeY6eAJvRdF+XOhVxKenL2VFjH5JtZzG+MYZ7H4JATY0x66IeE3oAGOiG9CNNJYk722EZPiXO
zjgBR4xAqaDnufMhLJhU0KXLZPkvEIta43AAKqXWYrli1KSgEb6sLg05Krzy9Tx1p1r6R45ue/WJ
D5t8h1gpjNiTYGi/Up5BCpQMJeEO2AXZqEL958swSkVsTV+O662oCLfqRLFrtV/QTdG4Q74DSu/n
har6g+koaO4eFCxgtcfDkH5kCIb4a26WhbE8zr2scF6R9cZFQ2G3yC4mojfZ1oKPd2qKDj8ilcXt
JU1fxWp6yzYbI3yq1TQOdM2zA9/ZiPcpNTyYia3myCanPp2vLn73RLlEdyMm0AyQZAYBPh9smMFj
kJ9QMCIehhSQmTwyJJ7ZWayWISCPyTffOPhZ1D7xSCvu/5ugjdvwGfC6/CooyIWdkE3/GT3JrdaZ
pqPmr7NaSRzsUhibH7Zkuc5JkCHttS7Mw5qJndgLWQWLJxl9K0WJ0MmiNY7G62SWgjhQjXNPiU1H
uH4cHCI6I90nnIzxPuUFSx/JcwjAXQLm3pzLk9SMHis/689k4AW6J5zZQLJPY5S7CH/NjYErtGY+
FHyth/q2lcW3dzoauXelQpmQujMeC0s30xfh/dr0yBwm61iHrJvTnTGH8AeVLXtHQUwpsWbK8YQg
DcJMnFAKs9ygjFvMVWagESNi4JE1vbvJW4Q4rWrF+INGEqehM89UHyDJxqsokQDefDb+CEAfCGMo
2Ns35vaX5V8SbQJWMtIqP9Jm6lBJHhHmm7vQmRdf6Ehc28KBMP09OfDK4vwGRiPSHzu9H26NyLU8
Zc62Br7Le5EuMbCkbEoOnNteanetzCvJPyStribNRwZaHgFCjN2vq3qeyr63+4Y/IaHTXoA1WKNT
ajZPodQRVyKoXqJ7LYUT5DM8iJPGGR+iEUeUQ3nlFQDlJDRUP/eKUa8CZcU5CDZtinIJv92lmK5m
L+xt/BLNyAgf9bIdtgURBHrzf07ii8SOiXgIltsoetnH8te6OfAian8O1EIjkUaFqig04AWkrs6G
XZ9FC2UE431Vb6NwBBDzWI0sW5ZCqKzpm5gwEJmgYgbOmFVej4IMhUbKtb+JIP26YAzhNXse5Zzw
a3ydfodbL85ndZvlM2G5zuI2o0BC9sMeBdynfT3d7c80mie14Iso439Y3f4JECBtim2Jn+Z/AuZu
3uoWQ5lQKHxCEi9EK7FEZ75bTia7tiKczKDH5IIJzX/gy+Qa/1BQwa2K/zq8q1juOytYgrJOXn1n
w01niGp/Gr/bsDueNxqQYbrF+DMqGGu/030ZBSrORlpb7IESCM6hSI9KEvPvqSFdzmqfV0RzcZMa
U+LNWsc1iXCskS8Bv3pKcmyY/eEIxZoag38c6ovl4eZ9OLyXDEJgqIOTaHC6A1aXjWew3G/PJlIs
yLmbwtILCW03F4agBNuItqXaDLU/pnLXS8vCHKaNurwjuGWP6zXeh+q3BtNypy/md/U9a7fpFun9
FommGI/At0COjXsuNyZo+DV6HMY88Sd6SItXO62QVWl7lUKGPCt35BLPpg36cADaBbs6O0DgYOsl
pjwG9ahNfOnTDSuTVrFWpzvIgmfG3OwYX1gwrEchjwXYDJmYSE108zUYbAvxEiDrMcLbJ74V4Ia9
wigzLG5AigzkLXskBoWdD3F7t3s+Ws9dfw0dYWv9dzwILiNlkHc9ZZTTbCzaiFYahIKHLAC0Vq2O
8aUOFI0Ib8+zqGuh8oqehfyAB46O7MM/LFkR6CEXRFtKNLtJ5mnICi687AhR4W8uYpGKcBfnPtB3
Ksm+uyrsfQtzEw5BBVV3vA4XRSIamDNuKTKn72WBjBte/28UbEW/g7DtqYJqD5kPIhmk2hGzb9N3
AHxIOrF56sU8G8x4LmDe19DihpVi96L9sIp5Cxs2KankkwMSOKrBpjj0FWeX0jBIjeI0zCI3dwwf
845ww3SjdUBZgPJ+8fQTAlf1GMWks3eVEZ+9sHfSWzz3IgOY5SwRQMbYgdj84IJ5R5bAmr3llWAl
dowBafmMpV0mjojIWxftGY8AYjAYIY7EuxoUYmt3FL6dvg8xIcVezD1WRWsMKPx4JeixvGqNtQVj
IO9MtNqEknlBOBjJNvaHx1JuUtF8zE2XIFIGYPLbsw1Pr38WL4MfdNjOhvCsBdUbjuVMBMKDbrV4
52RONtWHuDjNWYIqLFtdoPxI+1H9k8WekzKJNr/ESTRUPGiKV89D1/kUZ6VnAwj4saUxOm5HNGOr
Vwui+noXFVvVnZPT01cGcIZ3MgW5YMUX6fYFGNMDxhzR0fsEr8e7BydexPnTtPwtlYeacRTTqb9q
9TkRhdIRREqBVAXShTiJsqcaEa7OmqRW592Lp79DnlOB+JyoYQC0SHi+hQ5bkV1smdkqpG/an1pN
5QxzYAp18Fwhmv50tNIg6EXne6otfGgpdGtnIl4r1ETUd2ljLsctFh86D73NhV0E4Yp0d+fJUP5y
Iy4UU4FXim5T4MoDO+7WhxErV7D2yfPqXEuCnIn2AM6G+hqpPgjrLB9KeDY8urbYpB2Tl1qxut3z
PSmEI/bDNKUwGQ5rc/GFN0WqasMI5iHN7HMcZG2uT5inMrs1qyCKyP0MsRli/P7A6hA7mGWVmjO9
5xAfeJW/sE54yzyN9IvkhIHU0zQiVzLx4ebaZueWfNZKon2of9hOgrnxogkVd7giaERkADoa3S/I
FpYNr9kpJHgSv8HAZ4g/DzykwmX4D7Gv5A0j7qf0dDtE0QamntiT/7XvXy7oDY73KhjqSviSqdKT
ClzmOwKQkH91E5ihRjfWFOvlt8ju+cWzXon6tYd4UfZSa4CuSHhPfvPERCSCCDJMfqK2K+rv3zOn
1nU3Ira2BDbV5RJozfWoVy20PSCd/TozHoz7iym69HG1HdyZJP8ZzgmdbKS09hr4HZT0eDUz7ABU
uSstDEAF5AEGzcoTMDaWSiXMUA3fW1TXUA+MKLR5bIglXQJ+WvvAes00ns/5TZViv3zIq4u4R36A
nMyjK4qlv6SDyBBNY/VbvnIkzdigeSebesdZ0+1HS5WlAEzQGv47Brr5McHbNfTIB1EQPD5HJ8yf
09CewolbVLu6j5RGIVCvuFyrReNhzemS9M6EtWAZOOwoUMvu5nCmVfGdExGEBr71oR3VYF9LJuyh
ifw3nBi1iNR3bI6YhftF6gGId0iEfCTTd+JnVcMuhaTrPEDVCcqMKSVM6Esw6YtuHCBfMTNKqaYZ
y8n13WFger6fFMth4tr7etOReKHQcO5i0/7E8Uqn0d1C7ROWSR6tmD5d0LMQZWoB7izT0i+TCTTq
Sb0Jr6JxNeVX6GSOo/vEP3E8wLMavRqJzryKMjpHhe3MZ2WzYRGDOal5Rz997JJ1KUNsFCVEhB3X
u2VicwXhUgenyhN5LDZpSBKjg2RdsRqPLn5IY56mItEAhHonx2lRG2YZedZGiHhajcDr9LXHaqiq
xsnEiOivEj4VRlwk2wdm4c7MdeWbn0dkxZ/8eWTetzaIt0oT5eAS6AsXSW8cKgeuNaWDPeiPVghY
7zTx3UgIEZmB6NLG3K3+6w3U83Z0klh56W00CQ4Mmlm7HJUX1IvM81kGQUCBgWGtKFXFAKxN/Qti
Y8MEnJYsJ03izJ/GBrhNPXvXFDhHpQvIHFeDjyjt9zDGbp/OokiElgRpwdlKOzKuLlG+Wn698FrY
Uk1pD05rHl7/+RiGHhWUdiPuERD2yeqCa7miwsozq/2JpJR3FOz27iTsg28o+MxxQBGsNmvXgDb4
lxwFFRd6eFpzKnDdpIYn4xLhQgY9XY/HQhz63R8GmpWpm2hTIzVifigtYdiVAfKUtg9wS+aCyx/W
Jq8PKgKdgGLqN7pZrnR7xW5+1Bs9a7uMxy/C/qn3sJQbzC1Wzxxi1Z1AmyOCEzTh+Rf6ZjYxm1QY
6tsOuU0PZdxBAy6OpjFiymgu1S+Lj4REIxFA0bMICgKRF3a41RsNCfMv95ksOdCNcJPziXQnOjGe
GmXNCCFZyN9xyF37rYbBL1Ic/dvjfg2GhexF5T7UMQc/VfhoMJWOLMUZVnUctlfOR4RkrHje3pfm
XInD8XT88UKkMCpp4fgCiz7JNx0Y9VvkywiG+ZdtLhiv1hMS7lZLajvvvDt9ScGxLArFT2C/RPIC
XikpBYxXE6vaJ2Ki2DDs5UxjHZffKR3t+vp9/V12yCuQgre/8iAfRKZ8BDDiuA3vKIpxxJQ2/Ksi
5Boqmj3TciJeT3BmYNHuanAY+2Gq0z9mg8eOMzKBvwGqCIJL6p2dlwt3jyTWHzaYHDArEAE68bJ4
jE8Qz83UaH6ipA9reMbQTKsxkNH63rnnrBr8Ji1fIM/qQnGyhXPnyBU6YNGAf+j4Fu1Vr/QnfqCQ
PMChDmrl8fOe3rLYccdsDZKimIGxUCZIquLW66zKfXnDW9t4F7+IWnAvtk6VJTb8Aivmp5Y1QbIB
0jcfcdER+zIJsezfqDt2UUYLb4dhG73j6p932rltr3tMR4PZw266VxfOrENL+ScxENdzURr52YN9
29vEnFT090+vtjf+qzHo9dzeu4QhPVoqT0CQ742RidF3zMn5SQu1V4jofUWCcMpFrMzNhLMpLqQq
1yZZOdH6wsMFqUG78AZw3n+BbZrLI0E0M0VSQT1Q/h/e3o6Zv74n0aVdONsbBjMQlahjCq8Bm0mM
UNbbymE3zLi04eWpJeoIrTouh6Xaum2Up8Lc6bdR20TVlJ73FwgQ8uVHrV1ou3VkrOCht14IBECT
DPqV3BqPmyDcY/N/84/5YKQgr+uQhdM2vz7veGdn1DA5wUGYLfm84UrXwQrp3yUcnt7SROna8Wqk
wVtl2+GfVeCcORoZFJAtK9YhnSj5yhaq2zRrWf7J0iJMGBirIf89JDXSC0s36/4NCsPIhhFOW2Oz
z63nn6WW/IQ2gsWkN0+FoK1iVcrCInzOrE4NxhG1zShP77Sui4ukZjoVJVBDKjKJ9gFnZpRT0hBO
FjEZ7pqFZtO0KepEodSu26rkynMoLor0uLChqV3r+/WCIcRvaREWsavI3u0PvaIL7uACDivBYCk/
ReSl3jH9apTsBWLSFh0f8ZbOw+1cMnQu4dxQWhuGUDrxHYYcb41Dnh2JF+o9Dzizjjkw4IJTFMEL
Ids4erFmqlnYKLWJI2YZ+JXV5sE/a0K4IB/h5bQdlUWPjh3s1/igUaTmQwQTClVQcTw28sMUy/vF
AlMfzu5txPeV04OqO3Udbc/z5oDwfzvhZkC7cvgnF5SBvowkxZxxZnkaX9mn1c4uxJs5qLPW4mUd
cmblzQ9Pqo5opaUx38cdWycWc98N0WmY2csPqzXYpcOGulzU4LFjIA7anB4AOyVEeejiIqka+gbZ
hbLnZBKo4cHP0TiHkHiCB9xXuxtvfxC8OizbB7BySvJFzWZ8oUrhjhGz/bmTjg4zZcksHECcSUiw
ELvjBTYO3h2zaIvY7JnpJ2RZGMPEfpQVhJngBCl+VFDVTOS7OlYKW6jvXh7vr/Rieb+bSzkMnstE
tkduRm0pWHgJBditAChXZS2hcgcnGEZ4G60rd3mEg6FiZP/VnXrIz3HJaAis8JMBZrF6UD9v9YaL
kayF6WD1Ug8M8AN5BrQw8M3TZsh6rS0eJ+A7t0zVSnHR5Aw/TPCAkkXcII5rwV8x/mruD21XSCob
n6tvkBDaa6Ag7x4LoKuabu7lSX8T6/mt+zl9feqS3GMdkPx6bAJxTfcYnEb0lUlWPrzG2WweFc0P
GvXLfxVK3+3vCm+ipM23oZ8+mQOEo3MW0XoLBTmo05s/4ObvDwWxwYyyyIsNNouO7GVkjWSfmJt4
zBotNGHL6wvCoif1JXAtuV5JhdYe5GvsfsK4apfoFaAjESE+XHxgWcvMkD0yj5tHzPyLc264Usx1
lg3CJNWc4Yay2mURLbrBhJEf9AyDdqYK3K3bsucDxr+7yn7Jn7cXvl+6egyZiSQ7weaWhF+2pyi2
g/5A8SZ0Y4IxZ8+IDMyG82Q9Q1vSapAblSfr0aX9dPAw3hVqxriFXe938jrzKDxHrZmffYCCPau/
M3tcdmyR8BYRsySSzjZP+9XFS3zm+kA1bUYC+9mAlJt8xZR2T3ADLr4qz9Wweb3b0wMzkEdHVOEk
npHfxS1M7ZgJfLRyh+GHFb+ikt/QLoSIAs00U2c7KbwHX/RoDf+OAUreraz9L+H7GG+qyXJuvpjG
nIw03/kJN6KmI3C4wLRzI5Dl9K75OyUhZud1dFRtLZgTshBwqsDwbhlejmqos44UyBKZSw5G16UE
SyWYJ8laqWDmCco6B6YykTiWBzgTy16OknUDqgGzAfCn/uZzmnXHjA7ZJLj0VhUM+WYRbCdotl2d
GGCBRs+oCICmMP78BeVmuOfCTzLr3KtyAEluotldIHzRSpyfz1iaDsoX7YDxz2WoG20qpYPxfbtb
PPmS5/ARwUgoJtEHW0xTNjuILZKmiR9hP0PCQPUpEu4m187nPSqlL0z3LM+sMy/qQ2WqxQU7VmUj
1WPEWiqsPVOu0BAastyNZiicrKuyr2USjkQQY2Oglpc+J2xAAqRbhxAh1LcpnLra6QeLJPF23oi/
q3fj67i5ioRX04+PNaGqmNx7owCcrDZ1m2gQe04PiChEg/dSsg2dAtsVnjGGDibhwp6FeJ9VVdh4
xO7zDb6aN4GBrwCgnUXU9DdWyTA9R4odDdiVJrcYQ5LinDcOSKZTjtW1Mw==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD is
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      data_in => data_in,
      data_out => data_out,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_0(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_1(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_2(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_4 => gtxe2_i_4,
      gtxe2_i_5(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_8 => gtxe2_i_8,
      gtxe2_i_9 => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => reset_out,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rxchariscomma : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxcharisk : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxdisperr : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxnotintable : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxbufstatus : out STD_LOGIC_VECTOR ( 0 to 0 );
    txbuferr : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \rxdata_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    powerdown : in STD_LOGIC;
    reset_sync5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    txchardispval_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    txcharisk_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    enablealign : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \txdata_reg_reg[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver is
  signal data_valid_reg2 : STD_LOGIC;
  signal encommaalign_int : STD_LOGIC;
  signal gtwizard_inst_n_6 : STD_LOGIC;
  signal gtwizard_inst_n_7 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal rxchariscomma_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxchariscomma_i_1_n_0 : STD_LOGIC;
  signal rxchariscomma_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxchariscomma_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_i_1_n_0 : STD_LOGIC;
  signal rxcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxcharisk_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[2]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[3]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[4]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[5]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[6]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[7]_i_1_n_0\ : STD_LOGIC;
  signal rxdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdisperr_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdisperr_i_1_n_0 : STD_LOGIC;
  signal rxdisperr_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdisperr_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_i_1_n_0 : STD_LOGIC;
  signal rxnotintable_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxnotintable_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxpowerdown : STD_LOGIC;
  signal rxpowerdown_double : STD_LOGIC;
  signal \rxpowerdown_reg__0\ : STD_LOGIC;
  signal rxreset_int : STD_LOGIC;
  signal toggle : STD_LOGIC;
  signal toggle_i_1_n_0 : STD_LOGIC;
  signal toggle_rx : STD_LOGIC;
  signal toggle_rx_i_1_n_0 : STD_LOGIC;
  signal txbufstatus_reg : STD_LOGIC_VECTOR ( 1 to 1 );
  signal txchardispmode_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_reg : STD_LOGIC;
  signal txchardispval_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_reg : STD_LOGIC;
  signal txcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_reg : STD_LOGIC;
  signal txdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal txpowerdown : STD_LOGIC;
  signal txpowerdown_double : STD_LOGIC;
  signal \txpowerdown_reg__0\ : STD_LOGIC;
  signal txreset_int : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of rxchariscomma_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of rxcharisk_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \rxdata[0]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[1]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[2]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[3]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[4]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[5]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[6]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \rxdata[7]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of rxdisperr_i_1 : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of rxnotintable_i_1 : label is "soft_lutpair79";
begin
gtwizard_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD
     port map (
      D(1 downto 0) => rxclkcorcnt_int(1 downto 0),
      Q(15 downto 0) => txdata_int(15 downto 0),
      RXBUFSTATUS(0) => gtwizard_inst_n_7,
      RXPD(0) => rxpowerdown,
      TXBUFSTATUS(0) => gtwizard_inst_n_6,
      TXPD(0) => txpowerdown,
      data_in => data_in,
      data_out => data_valid_reg2,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => rxdata_int(15 downto 0),
      gtxe2_i_0(1 downto 0) => rxchariscomma_int(1 downto 0),
      gtxe2_i_1(1 downto 0) => rxcharisk_int(1 downto 0),
      gtxe2_i_2(1 downto 0) => rxdisperr_int(1 downto 0),
      gtxe2_i_3(1 downto 0) => rxnotintable_int(1 downto 0),
      gtxe2_i_4 => gtxe2_i,
      gtxe2_i_5(1 downto 0) => txchardispmode_int(1 downto 0),
      gtxe2_i_6(1 downto 0) => txchardispval_int(1 downto 0),
      gtxe2_i_7(1 downto 0) => txcharisk_int(1 downto 0),
      gtxe2_i_8 => rxreset_int,
      gtxe2_i_9 => txreset_int,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => encommaalign_int,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
reclock_encommaalign: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync
     port map (
      CLK => CLK,
      enablealign => enablealign,
      reset_out => encommaalign_int
    );
reclock_rxreset: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1
     port map (
      SR(0) => SR(0),
      independent_clock_bufg => independent_clock_bufg,
      reset_out => rxreset_int
    );
reclock_txreset: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2
     port map (
      independent_clock_bufg => independent_clock_bufg,
      reset_out => txreset_int,
      reset_sync5_0(0) => reset_sync5(0)
    );
reset_wtd_timer: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      reset => reset
    );
rxbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => p_0_in,
      Q => rxbufstatus(0),
      R => '0'
    );
\rxbufstatus_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_7,
      Q => p_0_in,
      R => '0'
    );
\rxchariscomma_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(0),
      Q => rxchariscomma_double(0),
      R => SR(0)
    );
\rxchariscomma_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(1),
      Q => rxchariscomma_double(1),
      R => SR(0)
    );
rxchariscomma_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxchariscomma_double(1),
      I1 => toggle_rx,
      I2 => rxchariscomma_double(0),
      O => rxchariscomma_i_1_n_0
    );
rxchariscomma_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxchariscomma_i_1_n_0,
      Q => rxchariscomma(0),
      R => SR(0)
    );
\rxchariscomma_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(0),
      Q => \rxchariscomma_reg__0\(0),
      R => '0'
    );
\rxchariscomma_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(1),
      Q => \rxchariscomma_reg__0\(1),
      R => '0'
    );
\rxcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(0),
      Q => rxcharisk_double(0),
      R => SR(0)
    );
\rxcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(1),
      Q => rxcharisk_double(1),
      R => SR(0)
    );
rxcharisk_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxcharisk_double(1),
      I1 => toggle_rx,
      I2 => rxcharisk_double(0),
      O => rxcharisk_i_1_n_0
    );
rxcharisk_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxcharisk_i_1_n_0,
      Q => rxcharisk(0),
      R => SR(0)
    );
\rxcharisk_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(0),
      Q => \rxcharisk_reg__0\(0),
      R => '0'
    );
\rxcharisk_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(1),
      Q => \rxcharisk_reg__0\(1),
      R => '0'
    );
\rxclkcorcnt_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(0),
      Q => rxclkcorcnt_double(0),
      R => SR(0)
    );
\rxclkcorcnt_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(1),
      Q => rxclkcorcnt_double(1),
      R => SR(0)
    );
\rxclkcorcnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(0),
      Q => Q(0),
      R => SR(0)
    );
\rxclkcorcnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(1),
      Q => Q(1),
      R => SR(0)
    );
\rxclkcorcnt_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(0),
      Q => rxclkcorcnt_reg(0),
      R => '0'
    );
\rxclkcorcnt_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(1),
      Q => rxclkcorcnt_reg(1),
      R => '0'
    );
\rxdata[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(8),
      I1 => toggle_rx,
      I2 => rxdata_double(0),
      O => \rxdata[0]_i_1_n_0\
    );
\rxdata[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(9),
      I1 => toggle_rx,
      I2 => rxdata_double(1),
      O => \rxdata[1]_i_1_n_0\
    );
\rxdata[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(10),
      I1 => toggle_rx,
      I2 => rxdata_double(2),
      O => \rxdata[2]_i_1_n_0\
    );
\rxdata[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(11),
      I1 => toggle_rx,
      I2 => rxdata_double(3),
      O => \rxdata[3]_i_1_n_0\
    );
\rxdata[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(12),
      I1 => toggle_rx,
      I2 => rxdata_double(4),
      O => \rxdata[4]_i_1_n_0\
    );
\rxdata[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(13),
      I1 => toggle_rx,
      I2 => rxdata_double(5),
      O => \rxdata[5]_i_1_n_0\
    );
\rxdata[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(14),
      I1 => toggle_rx,
      I2 => rxdata_double(6),
      O => \rxdata[6]_i_1_n_0\
    );
\rxdata[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(15),
      I1 => toggle_rx,
      I2 => rxdata_double(7),
      O => \rxdata[7]_i_1_n_0\
    );
\rxdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(0),
      Q => rxdata_double(0),
      R => SR(0)
    );
\rxdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(10),
      Q => rxdata_double(10),
      R => SR(0)
    );
\rxdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(11),
      Q => rxdata_double(11),
      R => SR(0)
    );
\rxdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(12),
      Q => rxdata_double(12),
      R => SR(0)
    );
\rxdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(13),
      Q => rxdata_double(13),
      R => SR(0)
    );
\rxdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(14),
      Q => rxdata_double(14),
      R => SR(0)
    );
\rxdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(15),
      Q => rxdata_double(15),
      R => SR(0)
    );
\rxdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(1),
      Q => rxdata_double(1),
      R => SR(0)
    );
\rxdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(2),
      Q => rxdata_double(2),
      R => SR(0)
    );
\rxdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(3),
      Q => rxdata_double(3),
      R => SR(0)
    );
\rxdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(4),
      Q => rxdata_double(4),
      R => SR(0)
    );
\rxdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(5),
      Q => rxdata_double(5),
      R => SR(0)
    );
\rxdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(6),
      Q => rxdata_double(6),
      R => SR(0)
    );
\rxdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(7),
      Q => rxdata_double(7),
      R => SR(0)
    );
\rxdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(8),
      Q => rxdata_double(8),
      R => SR(0)
    );
\rxdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(9),
      Q => rxdata_double(9),
      R => SR(0)
    );
\rxdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[0]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(0),
      R => SR(0)
    );
\rxdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[1]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(1),
      R => SR(0)
    );
\rxdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[2]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(2),
      R => SR(0)
    );
\rxdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[3]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(3),
      R => SR(0)
    );
\rxdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[4]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(4),
      R => SR(0)
    );
\rxdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[5]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(5),
      R => SR(0)
    );
\rxdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[6]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(6),
      R => SR(0)
    );
\rxdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[7]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(7),
      R => SR(0)
    );
\rxdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(0),
      Q => rxdata_reg(0),
      R => '0'
    );
\rxdata_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(10),
      Q => rxdata_reg(10),
      R => '0'
    );
\rxdata_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(11),
      Q => rxdata_reg(11),
      R => '0'
    );
\rxdata_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(12),
      Q => rxdata_reg(12),
      R => '0'
    );
\rxdata_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(13),
      Q => rxdata_reg(13),
      R => '0'
    );
\rxdata_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(14),
      Q => rxdata_reg(14),
      R => '0'
    );
\rxdata_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(15),
      Q => rxdata_reg(15),
      R => '0'
    );
\rxdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(1),
      Q => rxdata_reg(1),
      R => '0'
    );
\rxdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(2),
      Q => rxdata_reg(2),
      R => '0'
    );
\rxdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(3),
      Q => rxdata_reg(3),
      R => '0'
    );
\rxdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(4),
      Q => rxdata_reg(4),
      R => '0'
    );
\rxdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(5),
      Q => rxdata_reg(5),
      R => '0'
    );
\rxdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(6),
      Q => rxdata_reg(6),
      R => '0'
    );
\rxdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(7),
      Q => rxdata_reg(7),
      R => '0'
    );
\rxdata_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(8),
      Q => rxdata_reg(8),
      R => '0'
    );
\rxdata_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(9),
      Q => rxdata_reg(9),
      R => '0'
    );
\rxdisperr_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(0),
      Q => rxdisperr_double(0),
      R => SR(0)
    );
\rxdisperr_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(1),
      Q => rxdisperr_double(1),
      R => SR(0)
    );
rxdisperr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdisperr_double(1),
      I1 => toggle_rx,
      I2 => rxdisperr_double(0),
      O => rxdisperr_i_1_n_0
    );
rxdisperr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxdisperr_i_1_n_0,
      Q => rxdisperr(0),
      R => SR(0)
    );
\rxdisperr_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(0),
      Q => \rxdisperr_reg__0\(0),
      R => '0'
    );
\rxdisperr_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(1),
      Q => \rxdisperr_reg__0\(1),
      R => '0'
    );
\rxnotintable_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(0),
      Q => rxnotintable_double(0),
      R => SR(0)
    );
\rxnotintable_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(1),
      Q => rxnotintable_double(1),
      R => SR(0)
    );
rxnotintable_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxnotintable_double(1),
      I1 => toggle_rx,
      I2 => rxnotintable_double(0),
      O => rxnotintable_i_1_n_0
    );
rxnotintable_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxnotintable_i_1_n_0,
      Q => rxnotintable(0),
      R => SR(0)
    );
\rxnotintable_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(0),
      Q => \rxnotintable_reg__0\(0),
      R => '0'
    );
\rxnotintable_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(1),
      Q => \rxnotintable_reg__0\(1),
      R => '0'
    );
rxpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxpowerdown_reg__0\,
      Q => rxpowerdown_double,
      R => SR(0)
    );
rxpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => rxpowerdown_double,
      Q => rxpowerdown,
      R => '0'
    );
rxpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \rxpowerdown_reg__0\,
      R => SR(0)
    );
sync_block_data_valid: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      status_vector(0) => status_vector(0)
    );
toggle_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle,
      O => toggle_i_1_n_0
    );
toggle_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_i_1_n_0,
      Q => toggle,
      R => reset_sync5(0)
    );
toggle_rx_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle_rx,
      O => toggle_rx_i_1_n_0
    );
toggle_rx_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_rx_i_1_n_0,
      Q => toggle_rx,
      R => SR(0)
    );
txbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txbufstatus_reg(1),
      Q => txbuferr,
      R => '0'
    );
\txbufstatus_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_6,
      Q => txbufstatus_reg(1),
      R => '0'
    );
\txchardispmode_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispmode_reg,
      Q => txchardispmode_double(0),
      R => reset_sync5(0)
    );
\txchardispmode_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => D(0),
      Q => txchardispmode_double(1),
      R => reset_sync5(0)
    );
\txchardispmode_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(0),
      Q => txchardispmode_int(0),
      R => '0'
    );
\txchardispmode_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(1),
      Q => txchardispmode_int(1),
      R => '0'
    );
txchardispmode_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => txchardispmode_reg,
      R => reset_sync5(0)
    );
\txchardispval_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg,
      Q => txchardispval_double(0),
      R => reset_sync5(0)
    );
\txchardispval_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_double(1),
      R => reset_sync5(0)
    );
\txchardispval_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(0),
      Q => txchardispval_int(0),
      R => '0'
    );
\txchardispval_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(1),
      Q => txchardispval_int(1),
      R => '0'
    );
txchardispval_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_reg,
      R => reset_sync5(0)
    );
\txcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg,
      Q => txcharisk_double(0),
      R => reset_sync5(0)
    );
\txcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_double(1),
      R => reset_sync5(0)
    );
\txcharisk_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(0),
      Q => txcharisk_int(0),
      R => '0'
    );
\txcharisk_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(1),
      Q => txcharisk_int(1),
      R => '0'
    );
txcharisk_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_reg,
      R => reset_sync5(0)
    );
\txdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(0),
      Q => txdata_double(0),
      R => reset_sync5(0)
    );
\txdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_double(10),
      R => reset_sync5(0)
    );
\txdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_double(11),
      R => reset_sync5(0)
    );
\txdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_double(12),
      R => reset_sync5(0)
    );
\txdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_double(13),
      R => reset_sync5(0)
    );
\txdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_double(14),
      R => reset_sync5(0)
    );
\txdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_double(15),
      R => reset_sync5(0)
    );
\txdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(1),
      Q => txdata_double(1),
      R => reset_sync5(0)
    );
\txdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(2),
      Q => txdata_double(2),
      R => reset_sync5(0)
    );
\txdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(3),
      Q => txdata_double(3),
      R => reset_sync5(0)
    );
\txdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(4),
      Q => txdata_double(4),
      R => reset_sync5(0)
    );
\txdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(5),
      Q => txdata_double(5),
      R => reset_sync5(0)
    );
\txdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(6),
      Q => txdata_double(6),
      R => reset_sync5(0)
    );
\txdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(7),
      Q => txdata_double(7),
      R => reset_sync5(0)
    );
\txdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_double(8),
      R => reset_sync5(0)
    );
\txdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_double(9),
      R => reset_sync5(0)
    );
\txdata_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(0),
      Q => txdata_int(0),
      R => '0'
    );
\txdata_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(10),
      Q => txdata_int(10),
      R => '0'
    );
\txdata_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(11),
      Q => txdata_int(11),
      R => '0'
    );
\txdata_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(12),
      Q => txdata_int(12),
      R => '0'
    );
\txdata_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(13),
      Q => txdata_int(13),
      R => '0'
    );
\txdata_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(14),
      Q => txdata_int(14),
      R => '0'
    );
\txdata_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(15),
      Q => txdata_int(15),
      R => '0'
    );
\txdata_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(1),
      Q => txdata_int(1),
      R => '0'
    );
\txdata_int_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(2),
      Q => txdata_int(2),
      R => '0'
    );
\txdata_int_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(3),
      Q => txdata_int(3),
      R => '0'
    );
\txdata_int_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(4),
      Q => txdata_int(4),
      R => '0'
    );
\txdata_int_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(5),
      Q => txdata_int(5),
      R => '0'
    );
\txdata_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(6),
      Q => txdata_int(6),
      R => '0'
    );
\txdata_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(7),
      Q => txdata_int(7),
      R => '0'
    );
\txdata_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(8),
      Q => txdata_int(8),
      R => '0'
    );
\txdata_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(9),
      Q => txdata_int(9),
      R => '0'
    );
\txdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_reg(0),
      R => reset_sync5(0)
    );
\txdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_reg(1),
      R => reset_sync5(0)
    );
\txdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_reg(2),
      R => reset_sync5(0)
    );
\txdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_reg(3),
      R => reset_sync5(0)
    );
\txdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_reg(4),
      R => reset_sync5(0)
    );
\txdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_reg(5),
      R => reset_sync5(0)
    );
\txdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_reg(6),
      R => reset_sync5(0)
    );
\txdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_reg(7),
      R => reset_sync5(0)
    );
txpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \txpowerdown_reg__0\,
      Q => txpowerdown_double,
      R => reset_sync5(0)
    );
txpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => txpowerdown_double,
      Q => txpowerdown,
      R => '0'
    );
txpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \txpowerdown_reg__0\,
      R => reset_sync5(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block is
  port (
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    status_vector : out STD_LOGIC_VECTOR ( 6 downto 0 );
    resetdone : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signal_detect : in STD_LOGIC;
    CLK : in STD_LOGIC;
    data_in : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 2 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block is
  signal enablealign : STD_LOGIC;
  signal mgt_rx_reset : STD_LOGIC;
  signal mgt_tx_reset : STD_LOGIC;
  signal powerdown : STD_LOGIC;
  signal \^resetdone\ : STD_LOGIC;
  signal rx_reset_done_i : STD_LOGIC;
  signal rxbuferr : STD_LOGIC;
  signal rxchariscomma : STD_LOGIC;
  signal rxcharisk : STD_LOGIC;
  signal rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rxdisperr : STD_LOGIC;
  signal rxnotintable : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal transceiver_inst_n_11 : STD_LOGIC;
  signal transceiver_inst_n_12 : STD_LOGIC;
  signal txbuferr : STD_LOGIC;
  signal txchardispmode : STD_LOGIC;
  signal txchardispval : STD_LOGIC;
  signal txcharisk : STD_LOGIC;
  signal txdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  signal NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute B_SHIFTER_ADDR : string;
  attribute B_SHIFTER_ADDR of gig_ethernet_pcs_pma_0_core : label is "10'b0101001110";
  attribute C_1588 : integer;
  attribute C_1588 of gig_ethernet_pcs_pma_0_core : label is 0;
  attribute C_2_5G : string;
  attribute C_2_5G of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_COMPONENT_NAME : string;
  attribute C_COMPONENT_NAME of gig_ethernet_pcs_pma_0_core : label is "gig_ethernet_pcs_pma_0";
  attribute C_DYNAMIC_SWITCHING : string;
  attribute C_DYNAMIC_SWITCHING of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_ELABORATION_TRANSIENT_DIR : string;
  attribute C_ELABORATION_TRANSIENT_DIR of gig_ethernet_pcs_pma_0_core : label is "BlankString";
  attribute C_FAMILY : string;
  attribute C_FAMILY of gig_ethernet_pcs_pma_0_core : label is "kintex7";
  attribute C_HAS_AN : string;
  attribute C_HAS_AN of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_AXIL : string;
  attribute C_HAS_AXIL of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_MDIO : string;
  attribute C_HAS_MDIO of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_TEMAC : string;
  attribute C_HAS_TEMAC of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_IS_SGMII : string;
  attribute C_IS_SGMII of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_RX_GMII_CLK : string;
  attribute C_RX_GMII_CLK of gig_ethernet_pcs_pma_0_core : label is "TXOUTCLK";
  attribute C_SGMII_FABRIC_BUFFER : string;
  attribute C_SGMII_FABRIC_BUFFER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_SGMII_PHY_MODE : string;
  attribute C_SGMII_PHY_MODE of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_LVDS : string;
  attribute C_USE_LVDS of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TBI : string;
  attribute C_USE_TBI of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TRANSCEIVER : string;
  attribute C_USE_TRANSCEIVER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute GT_RX_BYTE_WIDTH : integer;
  attribute GT_RX_BYTE_WIDTH of gig_ethernet_pcs_pma_0_core : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of gig_ethernet_pcs_pma_0_core : label is "soft";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_core : label is "yes";
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of gig_ethernet_pcs_pma_0_core : label is "true";
begin
  resetdone <= \^resetdone\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
gig_ethernet_pcs_pma_0_core: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_v16_2_18
     port map (
      an_adv_config_val => '0',
      an_adv_config_vector(15 downto 0) => B"0000000000000000",
      an_enable => NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED,
      an_interrupt => NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED,
      an_restart_config => '0',
      basex_or_sgmii => '0',
      configuration_valid => '0',
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(2 downto 0),
      configuration_vector(0) => '0',
      correction_timer(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      dcm_locked => data_in,
      drp_daddr(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED(9 downto 0),
      drp_dclk => '0',
      drp_den => NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED,
      drp_di(15 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED(15 downto 0),
      drp_do(15 downto 0) => B"0000000000000000",
      drp_drdy => '0',
      drp_dwe => NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED,
      drp_gnt => '0',
      drp_req => NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED,
      en_cdet => NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED,
      enablealign => enablealign,
      ewrap => NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gtx_clk => '0',
      link_timer_basex(9 downto 0) => B"0000000000",
      link_timer_sgmii(9 downto 0) => B"0000000000",
      link_timer_value(9 downto 0) => B"0000000000",
      loc_ref => NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED,
      mdc => '0',
      mdio_in => '0',
      mdio_out => NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED,
      mdio_tri => NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED,
      mgt_rx_reset => mgt_rx_reset,
      mgt_tx_reset => mgt_tx_reset,
      phyad(4 downto 0) => B"00000",
      pma_rx_clk0 => '0',
      pma_rx_clk1 => '0',
      powerdown => powerdown,
      reset => \out\(0),
      reset_done => \^resetdone\,
      rx_code_group0(9 downto 0) => B"0000000000",
      rx_code_group1(9 downto 0) => B"0000000000",
      rx_gt_nominal_latency(15 downto 0) => B"0000000011111000",
      rxbufstatus(1) => rxbuferr,
      rxbufstatus(0) => '0',
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      rxclkcorcnt(2) => '0',
      rxclkcorcnt(1 downto 0) => rxclkcorcnt(1 downto 0),
      rxdata(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxnotintable(0) => rxnotintable,
      rxphy_correction_timer(63 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED(63 downto 0),
      rxphy_ns_field(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED(31 downto 0),
      rxphy_s_field(47 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED(47 downto 0),
      rxrecclk => '0',
      rxrundisp(0) => '0',
      s_axi_aclk => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_resetn => '0',
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED,
      s_axi_wvalid => '0',
      signal_detect => signal_detect,
      speed_is_100 => '0',
      speed_is_10_100 => '0',
      speed_selection(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED(1 downto 0),
      status_vector(15 downto 7) => NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      systemtimer_ns_field(31 downto 0) => B"00000000000000000000000000000000",
      systemtimer_s_field(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      tx_code_group(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED(9 downto 0),
      txbuferr => txbuferr,
      txchardispmode => txchardispmode,
      txchardispval => txchardispval,
      txcharisk => txcharisk,
      txdata(7 downto 0) => txdata(7 downto 0),
      userclk => '0',
      userclk2 => CLK
    );
sync_block_rx_reset_done: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_12,
      data_out => rx_reset_done_i
    );
sync_block_tx_reset_done: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_11,
      resetdone => \^resetdone\,
      resetdone_0 => rx_reset_done_i
    );
transceiver_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver
     port map (
      CLK => CLK,
      D(0) => txchardispmode,
      Q(1 downto 0) => rxclkcorcnt(1 downto 0),
      SR(0) => mgt_rx_reset,
      data_in => transceiver_inst_n_11,
      data_sync_reg1 => data_in,
      enablealign => enablealign,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtxe2_i,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      powerdown => powerdown,
      reset_sync5(0) => mgt_tx_reset,
      rx_fsm_reset_done_int_reg => transceiver_inst_n_12,
      rxbufstatus(0) => rxbuferr,
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      \rxdata_reg[7]_0\(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxn => rxn,
      rxnotintable(0) => rxnotintable,
      rxoutclk => rxoutclk,
      rxp => rxp,
      status_vector(0) => \^status_vector\(1),
      txbuferr => txbuferr,
      txchardispval_reg_reg_0(0) => txchardispval,
      txcharisk_reg_reg_0(0) => txcharisk,
      \txdata_reg_reg[7]_0\(7 downto 0) => txdata(7 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    resetdone : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support is
  signal \<const0>\ : STD_LOGIC;
  signal \^gt0_qplloutclk_out\ : STD_LOGIC;
  signal \^gt0_qplloutrefclk_out\ : STD_LOGIC;
  signal \^gtrefclk_bufg_out\ : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal \^mmcm_locked_out\ : STD_LOGIC;
  signal mmcm_reset : STD_LOGIC;
  signal \^pma_reset_out\ : STD_LOGIC;
  signal rxoutclk : STD_LOGIC;
  signal \^rxuserclk2_out\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal txoutclk : STD_LOGIC;
  signal \^userclk2_out\ : STD_LOGIC;
  signal \^userclk_out\ : STD_LOGIC;
begin
  gt0_qplloutclk_out <= \^gt0_qplloutclk_out\;
  gt0_qplloutrefclk_out <= \^gt0_qplloutrefclk_out\;
  gtrefclk_bufg_out <= \^gtrefclk_bufg_out\;
  gtrefclk_out <= \^gtrefclk_out\;
  mmcm_locked_out <= \^mmcm_locked_out\;
  pma_reset_out <= \^pma_reset_out\;
  rxuserclk2_out <= \^rxuserclk2_out\;
  rxuserclk_out <= \^rxuserclk2_out\;
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
  userclk2_out <= \^userclk2_out\;
  userclk_out <= \^userclk_out\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
core_clocking_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking
     port map (
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => \^gtrefclk_out\,
      gtrefclk_p => gtrefclk_p,
      mmcm_locked => \^mmcm_locked_out\,
      mmcm_reset => mmcm_reset,
      rxoutclk => rxoutclk,
      rxuserclk2_out => \^rxuserclk2_out\,
      txoutclk => txoutclk,
      userclk => \^userclk_out\,
      userclk2 => \^userclk2_out\
    );
core_gt_common_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common
     port map (
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_out => \^gtrefclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\
    );
core_resets_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets
     port map (
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\,
      reset => reset
    );
pcs_pma_block_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block
     port map (
      CLK => \^userclk2_out\,
      configuration_vector(2 downto 0) => configuration_vector(3 downto 1),
      data_in => \^mmcm_locked_out\,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_out => \^gtrefclk_out\,
      gtxe2_i => \^userclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \^pma_reset_out\,
      resetdone => resetdone,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      signal_detect => signal_detect,
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    resetdone : out STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "gig_ethernet_pcs_pma_v16_2_18,Vivado 2024.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal NLW_U0_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  attribute EXAMPLE_SIMULATION of U0 : label is 0;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support
     port map (
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(3 downto 1),
      configuration_vector(0) => '0',
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg_out => gtrefclk_bufg_out,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => gtrefclk_out,
      gtrefclk_p => gtrefclk_p,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_locked_out => mmcm_locked_out,
      pma_reset_out => pma_reset_out,
      reset => reset,
      resetdone => resetdone,
      rxn => rxn,
      rxp => rxp,
      rxuserclk2_out => rxuserclk2_out,
      rxuserclk_out => rxuserclk_out,
      signal_detect => signal_detect,
      status_vector(15 downto 7) => NLW_U0_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txp => txp,
      userclk2_out => userclk2_out,
      userclk_out => userclk_out
    );
end STRUCTURE;
