// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2024.1 (lin64) Build 5076996 Wed May 22 18:36:09 MDT 2024
// Date        : Fri Jul 12 09:13:06 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ gig_ethernet_pcs_pma_0_sim_netlist.v
// Design      : gig_ethernet_pcs_pma_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_18,Vivado 2024.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg_out(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(mmcm_locked_out),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init U0
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .data_in(data_in),
        .data_out(data_out),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .gtxe2_i_0(gtxe2_i_0),
        .gtxe2_i_1(gtxe2_i_1),
        .gtxe2_i_2(gtxe2_i_2),
        .gtxe2_i_3(gtxe2_i_3),
        .gtxe2_i_4(gtxe2_i_4),
        .gtxe2_i_5(gtxe2_i_5),
        .gtxe2_i_6(gtxe2_i_6),
        .gtxe2_i_7(gtxe2_i_7),
        .gtxe2_i_8(gtxe2_i_8),
        .gtxe2_i_9(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(reset_out),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT
   (gtxe2_i_0,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_1,
    txoutclk,
    gtxe2_i_2,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    independent_clock_bufg,
    cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_8,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_9,
    gtxe2_i_10,
    gtxe2_i_11);
  output gtxe2_i_0;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_1;
  output txoutclk;
  output gtxe2_i_2;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  output [1:0]gtxe2_i_7;
  input independent_clock_bufg;
  input cpll_pd0_i;
  input cpllreset_in;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_8;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input [1:0]gtxe2_i_11;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [1:0]gtxe2_i_11;
  wire gtxe2_i_2;
  wire [15:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire gtxe2_i_n_0;
  wire gtxe2_i_n_10;
  wire gtxe2_i_n_16;
  wire gtxe2_i_n_170;
  wire gtxe2_i_n_171;
  wire gtxe2_i_n_172;
  wire gtxe2_i_n_173;
  wire gtxe2_i_n_174;
  wire gtxe2_i_n_175;
  wire gtxe2_i_n_176;
  wire gtxe2_i_n_177;
  wire gtxe2_i_n_178;
  wire gtxe2_i_n_179;
  wire gtxe2_i_n_180;
  wire gtxe2_i_n_181;
  wire gtxe2_i_n_182;
  wire gtxe2_i_n_183;
  wire gtxe2_i_n_184;
  wire gtxe2_i_n_27;
  wire gtxe2_i_n_3;
  wire gtxe2_i_n_38;
  wire gtxe2_i_n_39;
  wire gtxe2_i_n_4;
  wire gtxe2_i_n_46;
  wire gtxe2_i_n_47;
  wire gtxe2_i_n_48;
  wire gtxe2_i_n_49;
  wire gtxe2_i_n_50;
  wire gtxe2_i_n_51;
  wire gtxe2_i_n_52;
  wire gtxe2_i_n_53;
  wire gtxe2_i_n_54;
  wire gtxe2_i_n_55;
  wire gtxe2_i_n_56;
  wire gtxe2_i_n_57;
  wire gtxe2_i_n_58;
  wire gtxe2_i_n_59;
  wire gtxe2_i_n_60;
  wire gtxe2_i_n_61;
  wire gtxe2_i_n_81;
  wire gtxe2_i_n_83;
  wire gtxe2_i_n_84;
  wire gtxe2_i_n_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gtxe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXDATAVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_RXRATEDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_TXRATEDONE_UNCONNECTED;
  wire [15:0]NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISK_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXCHBONDO_UNCONNECTED;
  wire [63:16]NLW_gtxe2_i_RXDATA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXHEADER_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXSTATUS_UNCONNECTED;
  wire [9:0]NLW_gtxe2_i_TSTOUT_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_CHANNEL #(
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b0001111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(36),
    .CLK_COR_MIN_LAT(33),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG(24'hBC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'h00018480),
    .PMA_RSV2(16'h2050),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(32'h00000000),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(8),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(72'h03000023FF10100020),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00000011110000),
    .RXLPM_LF_CFG(14'b00000011110000),
    .RXOOB_CFG(7'b0000110),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'h000000),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RX_BIAS_CFG(12'b000000000100),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(5),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(3'b010),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(12'b000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFE_GAIN_CFG(23'h020FEA),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011110000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_KL_CFG(13'b0000011111110),
    .RX_DFE_KL_CFG2(32'h301148AC),
    .RX_DFE_LPM_CFG(16'h0904),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_UT_CFG(17'b10001111000000000),
    .RX_DFE_VP_CFG(17'b00011111100000011),
    .RX_DFE_XYD_CFG(13'b0000000000000),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(0),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("4.0"),
    .TERM_RCAL_CFG(5'b10000),
    .TERM_RCAL_OVRD(1'b0),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPMARESET_TIME(5'b00001),
    .TX_CLK25_DIV(5),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DEEMPH0(5'b00000),
    .TX_DEEMPH1(5'b00000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXOUT"),
    .UCODEER_CLR(1'b0)) 
    gtxe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD({1'b0,1'b0,1'b0,1'b0}),
        .CPLLFBCLKLOST(gtxe2_i_n_0),
        .CPLLLOCK(gtxe2_i_0),
        .CPLLLOCKDETCLK(independent_clock_bufg),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(cpll_pd0_i),
        .CPLLREFCLKLOST(gt0_cpllrefclklost_i),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(cpllreset_in),
        .DMONITOROUT({gtxe2_i_n_177,gtxe2_i_n_178,gtxe2_i_n_179,gtxe2_i_n_180,gtxe2_i_n_181,gtxe2_i_n_182,gtxe2_i_n_183,gtxe2_i_n_184}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(gtrefclk_bufg),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({gtxe2_i_n_46,gtxe2_i_n_47,gtxe2_i_n_48,gtxe2_i_n_49,gtxe2_i_n_50,gtxe2_i_n_51,gtxe2_i_n_52,gtxe2_i_n_53,gtxe2_i_n_54,gtxe2_i_n_55,gtxe2_i_n_56,gtxe2_i_n_57,gtxe2_i_n_58,gtxe2_i_n_59,gtxe2_i_n_60,gtxe2_i_n_61}),
        .DRPEN(1'b0),
        .DRPRDY(gtxe2_i_n_3),
        .DRPWE(1'b0),
        .EYESCANDATAERROR(gtxe2_i_n_4),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(SR),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(gt0_gttxreset_in0_out),
        .GTXRXN(rxn),
        .GTXRXP(rxp),
        .GTXTXN(txn),
        .GTXTXP(txp),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtxe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(gt0_qplloutclk_out),
        .QPLLREFCLK(gt0_qplloutrefclk_out),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({RXBUFSTATUS,gtxe2_i_n_83,gtxe2_i_n_84}),
        .RXBYTEISALIGNED(gtxe2_i_n_9),
        .RXBYTEREALIGN(gtxe2_i_n_10),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED),
        .RXCHANISALIGNED(NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED),
        .RXCHANREALIGN(NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED),
        .RXCHARISCOMMA({NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED[7:2],gtxe2_i_4}),
        .RXCHARISK({NLW_gtxe2_i_RXCHARISK_UNCONNECTED[7:2],gtxe2_i_5}),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO(NLW_gtxe2_i_RXCHBONDO_UNCONNECTED[4:0]),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(D),
        .RXCOMINITDET(NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gtxe2_i_n_16),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gtxe2_i_RXDATA_UNCONNECTED[63:16],gtxe2_i_3}),
        .RXDATAVALID(NLW_gtxe2_i_RXDATAVALID_UNCONNECTED),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDFEXYDHOLD(1'b0),
        .RXDFEXYDOVRDEN(1'b0),
        .RXDISPERR({NLW_gtxe2_i_RXDISPERR_UNCONNECTED[7:2],gtxe2_i_6}),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gtxe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtxe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMEN(1'b1),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(reset_out),
        .RXMONITOROUT({gtxe2_i_n_170,gtxe2_i_n_171,gtxe2_i_n_172,gtxe2_i_n_173,gtxe2_i_n_174,gtxe2_i_n_175,gtxe2_i_n_176}),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXNOTINTABLE({NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED[7:2],gtxe2_i_7}),
        .RXOOBRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk),
        .RXOUTCLKFABRIC(NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(reset_out),
        .RXPCSRESET(reset),
        .RXPD({RXPD,RXPD}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(1'b0),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(gtxe2_i_n_27),
        .RXPRBSSEL({1'b0,1'b0,1'b0}),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gtxe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gtxe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(NLW_gtxe2_i_RXRATEDONE_UNCONNECTED),
        .RXRESETDONE(gtxe2_i_1),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED),
        .RXSTATUS(NLW_gtxe2_i_RXSTATUS_UNCONNECTED[2:0]),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(gt0_rxuserrdy_t),
        .RXUSRCLK(gtxe2_i_8),
        .RXUSRCLK2(gtxe2_i_8),
        .RXVALID(NLW_gtxe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TSTOUT(NLW_gtxe2_i_TSTOUT_UNCONNECTED[9:0]),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS({TXBUFSTATUS,gtxe2_i_n_81}),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_9}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_10}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_11}),
        .TXCOMFINISH(NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(TXPD),
        .TXGEARBOXREADY(NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk),
        .TXOUTCLKFABRIC(gtxe2_i_n_38),
        .TXOUTCLKPCS(gtxe2_i_n_39),
        .TXOUTCLKSEL({1'b1,1'b0,1'b0}),
        .TXPCSRESET(1'b0),
        .TXPD({TXPD,TXPD}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED),
        .TXPHOVRDEN(1'b0),
        .TXPISOPD(1'b0),
        .TXPMARESET(1'b0),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gtxe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gtxe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtxe2_i_TXRATEDONE_UNCONNECTED),
        .TXRESETDONE(gtxe2_i_2),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(gt0_txuserrdy_t),
        .TXUSRCLK(gtxe2_i_8),
        .TXUSRCLK2(gtxe2_i_8));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_init
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire [13:1]data0;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gtrxreset_in1_out;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire [13:0]gt0_rx_cdrlock_counter;
  wire gt0_rx_cdrlock_counter0_carry__0_n_0;
  wire gt0_rx_cdrlock_counter0_carry__0_n_1;
  wire gt0_rx_cdrlock_counter0_carry__0_n_2;
  wire gt0_rx_cdrlock_counter0_carry__0_n_3;
  wire gt0_rx_cdrlock_counter0_carry__1_n_0;
  wire gt0_rx_cdrlock_counter0_carry__1_n_1;
  wire gt0_rx_cdrlock_counter0_carry__1_n_2;
  wire gt0_rx_cdrlock_counter0_carry__1_n_3;
  wire gt0_rx_cdrlock_counter0_carry_n_0;
  wire gt0_rx_cdrlock_counter0_carry_n_1;
  wire gt0_rx_cdrlock_counter0_carry_n_2;
  wire gt0_rx_cdrlock_counter0_carry_n_3;
  wire \gt0_rx_cdrlock_counter[0]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_3_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_4_n_0 ;
  wire [13:0]gt0_rx_cdrlock_counter_0;
  wire gt0_rx_cdrlocked_i_1_n_0;
  wire gt0_rx_cdrlocked_reg_n_0;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_i_n_0;
  wire gtwizard_i_n_5;
  wire gtwizard_i_n_7;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire [3:0]NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED;
  wire [3:1]NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry
       (.CI(1'b0),
        .CO({gt0_rx_cdrlock_counter0_carry_n_0,gt0_rx_cdrlock_counter0_carry_n_1,gt0_rx_cdrlock_counter0_carry_n_2,gt0_rx_cdrlock_counter0_carry_n_3}),
        .CYINIT(gt0_rx_cdrlock_counter[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(gt0_rx_cdrlock_counter[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__0
       (.CI(gt0_rx_cdrlock_counter0_carry_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__0_n_0,gt0_rx_cdrlock_counter0_carry__0_n_1,gt0_rx_cdrlock_counter0_carry__0_n_2,gt0_rx_cdrlock_counter0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(gt0_rx_cdrlock_counter[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__1
       (.CI(gt0_rx_cdrlock_counter0_carry__0_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__1_n_0,gt0_rx_cdrlock_counter0_carry__1_n_1,gt0_rx_cdrlock_counter0_carry__1_n_2,gt0_rx_cdrlock_counter0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(gt0_rx_cdrlock_counter[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__2
       (.CI(gt0_rx_cdrlock_counter0_carry__1_n_0),
        .CO(NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED[3:1],data0[13]}),
        .S({1'b0,1'b0,1'b0,gt0_rx_cdrlock_counter[13]}));
  LUT2 #(
    .INIT(4'h2)) 
    \gt0_rx_cdrlock_counter[0]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ),
        .I1(gt0_rx_cdrlock_counter[0]),
        .O(gt0_rx_cdrlock_counter_0[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \gt0_rx_cdrlock_counter[0]_i_2 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I1(gt0_rx_cdrlock_counter[4]),
        .I2(gt0_rx_cdrlock_counter[5]),
        .I3(gt0_rx_cdrlock_counter[7]),
        .I4(gt0_rx_cdrlock_counter[6]),
        .I5(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .O(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[10]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[10]),
        .O(gt0_rx_cdrlock_counter_0[10]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[11]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[11]),
        .O(gt0_rx_cdrlock_counter_0[11]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[12]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[12]),
        .O(gt0_rx_cdrlock_counter_0[12]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[13]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[13]),
        .O(gt0_rx_cdrlock_counter_0[13]));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \gt0_rx_cdrlock_counter[13]_i_2 
       (.I0(gt0_rx_cdrlock_counter[1]),
        .I1(gt0_rx_cdrlock_counter[12]),
        .I2(gt0_rx_cdrlock_counter[13]),
        .I3(gt0_rx_cdrlock_counter[3]),
        .I4(gt0_rx_cdrlock_counter[2]),
        .O(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \gt0_rx_cdrlock_counter[13]_i_3 
       (.I0(gt0_rx_cdrlock_counter[4]),
        .I1(gt0_rx_cdrlock_counter[5]),
        .I2(gt0_rx_cdrlock_counter[7]),
        .I3(gt0_rx_cdrlock_counter[6]),
        .O(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \gt0_rx_cdrlock_counter[13]_i_4 
       (.I0(gt0_rx_cdrlock_counter[9]),
        .I1(gt0_rx_cdrlock_counter[8]),
        .I2(gt0_rx_cdrlock_counter[10]),
        .I3(gt0_rx_cdrlock_counter[11]),
        .O(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[1]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[1]),
        .O(gt0_rx_cdrlock_counter_0[1]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[2]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[2]),
        .O(gt0_rx_cdrlock_counter_0[2]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[3]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[3]),
        .O(gt0_rx_cdrlock_counter_0[3]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[4]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[4]),
        .O(gt0_rx_cdrlock_counter_0[4]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[5]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[5]),
        .O(gt0_rx_cdrlock_counter_0[5]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[6]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[6]),
        .O(gt0_rx_cdrlock_counter_0[6]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[7]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[7]),
        .O(gt0_rx_cdrlock_counter_0[7]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[8]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[8]),
        .O(gt0_rx_cdrlock_counter_0[8]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[9]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[9]),
        .O(gt0_rx_cdrlock_counter_0[9]));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[0]),
        .Q(gt0_rx_cdrlock_counter[0]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[10]),
        .Q(gt0_rx_cdrlock_counter[10]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[11]),
        .Q(gt0_rx_cdrlock_counter[11]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[12]),
        .Q(gt0_rx_cdrlock_counter[12]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[13]),
        .Q(gt0_rx_cdrlock_counter[13]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[1]),
        .Q(gt0_rx_cdrlock_counter[1]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[2]),
        .Q(gt0_rx_cdrlock_counter[2]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[3]),
        .Q(gt0_rx_cdrlock_counter[3]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[4]),
        .Q(gt0_rx_cdrlock_counter[4]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[5]),
        .Q(gt0_rx_cdrlock_counter[5]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[6]),
        .Q(gt0_rx_cdrlock_counter[6]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[7]),
        .Q(gt0_rx_cdrlock_counter[7]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[8]),
        .Q(gt0_rx_cdrlock_counter[8]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[9]),
        .Q(gt0_rx_cdrlock_counter[9]),
        .R(gt0_gtrxreset_in1_out));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    gt0_rx_cdrlocked_i_1
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(gt0_rx_cdrlocked_reg_n_0),
        .O(gt0_rx_cdrlocked_i_1_n_0));
  FDRE gt0_rx_cdrlocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlocked_i_1_n_0),
        .Q(gt0_rx_cdrlocked_reg_n_0),
        .R(gt0_gtrxreset_in1_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM gt0_rxresetfsm_i
       (.\FSM_sequential_rx_state_reg[0]_0 (gt0_rx_cdrlocked_reg_n_0),
        .SR(gt0_gtrxreset_in1_out),
        .data_in(rx_fsm_reset_done_int_reg),
        .data_out(data_out),
        .data_sync_reg1(gtwizard_i_n_5),
        .data_sync_reg1_0(data_sync_reg1),
        .data_sync_reg1_1(gtwizard_i_n_0),
        .data_sync_reg6(gtxe2_i_4),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gtxe2_i(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .out(out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM gt0_txresetfsm_i
       (.data_in(data_in),
        .data_sync_reg1(gtxe2_i_4),
        .data_sync_reg1_0(gtwizard_i_n_7),
        .data_sync_reg1_1(data_sync_reg1),
        .data_sync_reg1_2(gtwizard_i_n_0),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtxe2_i(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt gtwizard_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(gt0_gtrxreset_in1_out),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtwizard_i_n_0),
        .gtxe2_i_0(gtwizard_i_n_5),
        .gtxe2_i_1(gtwizard_i_n_7),
        .gtxe2_i_10(gtxe2_i_7),
        .gtxe2_i_2(gtxe2_i),
        .gtxe2_i_3(gtxe2_i_0),
        .gtxe2_i_4(gtxe2_i_1),
        .gtxe2_i_5(gtxe2_i_2),
        .gtxe2_i_6(gtxe2_i_3),
        .gtxe2_i_7(gtxe2_i_4),
        .gtxe2_i_8(gtxe2_i_5),
        .gtxe2_i_9(gtxe2_i_6),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
   (gtxe2_i,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_0,
    txoutclk,
    gtxe2_i_1,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_2,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_7,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_8,
    gtxe2_i_9,
    gtxe2_i_10,
    gt0_cpllreset_t);
  output gtxe2_i;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_0;
  output txoutclk;
  output gtxe2_i_1;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_7;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_8;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input gt0_cpllreset_t;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [15:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire gtxe2_i_7;
  wire [1:0]gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing cpll_railing0_i
       (.cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gtrefclk_bufg(gtrefclk_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD_GT gt0_GTWIZARD_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(SR),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i_0(gtxe2_i),
        .gtxe2_i_1(gtxe2_i_0),
        .gtxe2_i_10(gtxe2_i_9),
        .gtxe2_i_11(gtxe2_i_10),
        .gtxe2_i_2(gtxe2_i_1),
        .gtxe2_i_3(gtxe2_i_2),
        .gtxe2_i_4(gtxe2_i_3),
        .gtxe2_i_5(gtxe2_i_4),
        .gtxe2_i_6(gtxe2_i_5),
        .gtxe2_i_7(gtxe2_i_6),
        .gtxe2_i_8(gtxe2_i_7),
        .gtxe2_i_9(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
   (data_in,
    gt0_rxuserrdy_t,
    SR,
    independent_clock_bufg,
    data_sync_reg6,
    out,
    gtxe2_i,
    \FSM_sequential_rx_state_reg[0]_0 ,
    data_sync_reg1,
    data_sync_reg1_0,
    data_out,
    data_sync_reg1_1);
  output data_in;
  output gt0_rxuserrdy_t;
  output [0:0]SR;
  input independent_clock_bufg;
  input data_sync_reg6;
  input [0:0]out;
  input gtxe2_i;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input data_sync_reg1;
  input data_sync_reg1_0;
  input data_out;
  input data_sync_reg1_1;

  wire \FSM_sequential_rx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_10_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_9_n_0 ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire GTRXRESET;
  wire RXUSERRDY_i_1_n_0;
  wire [0:0]SR;
  wire check_tlock_max_i_1_n_0;
  wire check_tlock_max_reg_n_0;
  wire data_in;
  wire data_out;
  wire data_out_0;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg6;
  wire gt0_rxuserrdy_t;
  wire gtrxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_3__0_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1__0_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1__0_n_0 ;
  wire \mmcm_lock_count[3]_i_1__0_n_0 ;
  wire \mmcm_lock_count[4]_i_1__0_n_0 ;
  wire \mmcm_lock_count[5]_i_1__0_n_0 ;
  wire \mmcm_lock_count[6]_i_1__0_n_0 ;
  wire \mmcm_lock_count[7]_i_2__0_n_0 ;
  wire \mmcm_lock_count[7]_i_3__0_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2__0_n_0;
  wire [0:0]out;
  wire [6:1]p_0_in__2;
  wire [1:0]p_0_in__3;
  wire reset_time_out_i_3_n_0;
  wire reset_time_out_i_4_n_0;
  wire reset_time_out_reg_n_0;
  wire run_phase_alignment_int_i_1__0_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3_reg_n_0;
  wire rx_fsm_reset_done_int_i_5_n_0;
  wire rx_fsm_reset_done_int_i_6_n_0;
  wire rx_fsm_reset_done_int_s2;
  wire rx_fsm_reset_done_int_s3;
  wire [3:0]rx_state;
  wire [3:0]rx_state__0;
  wire rxresetdone_s2;
  wire rxresetdone_s3;
  wire sync_cplllock_n_0;
  wire sync_data_valid_n_0;
  wire sync_data_valid_n_1;
  wire sync_data_valid_n_5;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_100us_i_1_n_0;
  wire time_out_100us_i_2_n_0;
  wire time_out_100us_i_3_n_0;
  wire time_out_100us_reg_n_0;
  wire time_out_1us_i_1_n_0;
  wire time_out_1us_i_2_n_0;
  wire time_out_1us_i_3_n_0;
  wire time_out_1us_reg_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2_n_0;
  wire time_out_2ms_i_3__0_n_0;
  wire time_out_2ms_i_4_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2__0_n_0 ;
  wire \time_out_counter_reg[0]_i_2__0_n_1 ;
  wire \time_out_counter_reg[0]_i_2__0_n_2 ;
  wire \time_out_counter_reg[0]_i_2__0_n_3 ;
  wire \time_out_counter_reg[0]_i_2__0_n_4 ;
  wire \time_out_counter_reg[0]_i_2__0_n_5 ;
  wire \time_out_counter_reg[0]_i_2__0_n_6 ;
  wire \time_out_counter_reg[0]_i_2__0_n_7 ;
  wire \time_out_counter_reg[12]_i_1__0_n_0 ;
  wire \time_out_counter_reg[12]_i_1__0_n_1 ;
  wire \time_out_counter_reg[12]_i_1__0_n_2 ;
  wire \time_out_counter_reg[12]_i_1__0_n_3 ;
  wire \time_out_counter_reg[12]_i_1__0_n_4 ;
  wire \time_out_counter_reg[12]_i_1__0_n_5 ;
  wire \time_out_counter_reg[12]_i_1__0_n_6 ;
  wire \time_out_counter_reg[12]_i_1__0_n_7 ;
  wire \time_out_counter_reg[16]_i_1__0_n_2 ;
  wire \time_out_counter_reg[16]_i_1__0_n_3 ;
  wire \time_out_counter_reg[16]_i_1__0_n_5 ;
  wire \time_out_counter_reg[16]_i_1__0_n_6 ;
  wire \time_out_counter_reg[16]_i_1__0_n_7 ;
  wire \time_out_counter_reg[4]_i_1__0_n_0 ;
  wire \time_out_counter_reg[4]_i_1__0_n_1 ;
  wire \time_out_counter_reg[4]_i_1__0_n_2 ;
  wire \time_out_counter_reg[4]_i_1__0_n_3 ;
  wire \time_out_counter_reg[4]_i_1__0_n_4 ;
  wire \time_out_counter_reg[4]_i_1__0_n_5 ;
  wire \time_out_counter_reg[4]_i_1__0_n_6 ;
  wire \time_out_counter_reg[4]_i_1__0_n_7 ;
  wire \time_out_counter_reg[8]_i_1__0_n_0 ;
  wire \time_out_counter_reg[8]_i_1__0_n_1 ;
  wire \time_out_counter_reg[8]_i_1__0_n_2 ;
  wire \time_out_counter_reg[8]_i_1__0_n_3 ;
  wire \time_out_counter_reg[8]_i_1__0_n_4 ;
  wire \time_out_counter_reg[8]_i_1__0_n_5 ;
  wire \time_out_counter_reg[8]_i_1__0_n_6 ;
  wire \time_out_counter_reg[8]_i_1__0_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2__0_n_0;
  wire time_out_wait_bypass_i_3__0_n_0;
  wire time_out_wait_bypass_i_4__0_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max;
  wire time_tlock_max1;
  wire time_tlock_max1_carry__0_i_1_n_0;
  wire time_tlock_max1_carry__0_i_2_n_0;
  wire time_tlock_max1_carry__0_i_3_n_0;
  wire time_tlock_max1_carry__0_i_4_n_0;
  wire time_tlock_max1_carry__0_i_5_n_0;
  wire time_tlock_max1_carry__0_i_6_n_0;
  wire time_tlock_max1_carry__0_n_0;
  wire time_tlock_max1_carry__0_n_1;
  wire time_tlock_max1_carry__0_n_2;
  wire time_tlock_max1_carry__0_n_3;
  wire time_tlock_max1_carry__1_i_1_n_0;
  wire time_tlock_max1_carry__1_i_2_n_0;
  wire time_tlock_max1_carry__1_i_3_n_0;
  wire time_tlock_max1_carry__1_n_3;
  wire time_tlock_max1_carry_i_1_n_0;
  wire time_tlock_max1_carry_i_2_n_0;
  wire time_tlock_max1_carry_i_3_n_0;
  wire time_tlock_max1_carry_i_4_n_0;
  wire time_tlock_max1_carry_i_5_n_0;
  wire time_tlock_max1_carry_i_6_n_0;
  wire time_tlock_max1_carry_i_7_n_0;
  wire time_tlock_max1_carry_i_8_n_0;
  wire time_tlock_max1_carry_n_0;
  wire time_tlock_max1_carry_n_1;
  wire time_tlock_max1_carry_n_2;
  wire time_tlock_max1_carry_n_3;
  wire time_tlock_max_i_1_n_0;
  wire \wait_bypass_count[0]_i_1__0_n_0 ;
  wire \wait_bypass_count[0]_i_2__0_n_0 ;
  wire \wait_bypass_count[0]_i_4_n_0 ;
  wire [12:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3__0_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_7 ;
  wire [0:0]wait_time_cnt0__0;
  wire \wait_time_cnt[1]_i_1__0_n_0 ;
  wire \wait_time_cnt[2]_i_1__0_n_0 ;
  wire \wait_time_cnt[3]_i_1__0_n_0 ;
  wire \wait_time_cnt[4]_i_1__0_n_0 ;
  wire \wait_time_cnt[5]_i_1__0_n_0 ;
  wire \wait_time_cnt[6]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_2__0_n_0 ;
  wire \wait_time_cnt[6]_i_3__0_n_0 ;
  wire \wait_time_cnt[6]_i_4__0_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED ;
  wire [3:0]NLW_time_tlock_max1_carry_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_time_tlock_max1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h2222AAAA00000C00)) 
    \FSM_sequential_rx_state[0]_i_2 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[3]),
        .I3(time_tlock_max),
        .I4(reset_time_out_reg_n_0),
        .I5(rx_state[1]),
        .O(\FSM_sequential_rx_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000AABF000F0000)) 
    \FSM_sequential_rx_state[1]_i_3 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .I2(rx_state[2]),
        .I3(rx_state[3]),
        .I4(rx_state[1]),
        .I5(rx_state[0]),
        .O(\FSM_sequential_rx_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050FF2200)) 
    \FSM_sequential_rx_state[2]_i_1 
       (.I0(rx_state[1]),
        .I1(time_out_2ms_reg_n_0),
        .I2(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I3(rx_state[0]),
        .I4(rx_state[2]),
        .I5(rx_state[3]),
        .O(rx_state__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[2]_i_2 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .O(\FSM_sequential_rx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[3]_i_10 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_out_2ms_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050005300)) 
    \FSM_sequential_rx_state[3]_i_3 
       (.I0(\FSM_sequential_rx_state[3]_i_10_n_0 ),
        .I1(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I2(rx_state[0]),
        .I3(rx_state[1]),
        .I4(wait_time_cnt_reg[6]),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000023002F00)) 
    \FSM_sequential_rx_state[3]_i_7 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[1]),
        .I3(rx_state[0]),
        .I4(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h80800080)) 
    \FSM_sequential_rx_state[3]_i_9 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(time_out_2ms_reg_n_0),
        .I4(reset_time_out_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[0]),
        .Q(rx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[1]),
        .Q(rx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[2]),
        .Q(rx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[3]),
        .Q(rx_state[3]),
        .R(out));
  LUT5 #(
    .INIT(32'hFFFB4000)) 
    RXUSERRDY_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(gt0_rxuserrdy_t),
        .O(RXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    RXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(RXUSERRDY_i_1_n_0),
        .Q(gt0_rxuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    check_tlock_max_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(check_tlock_max_reg_n_0),
        .O(check_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    check_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(check_tlock_max_i_1_n_0),
        .Q(check_tlock_max_reg_n_0),
        .R(out));
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gtrxreset_i_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(rx_state[0]),
        .I4(GTRXRESET),
        .O(gtrxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtrxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gtrxreset_i_i_1_n_0),
        .Q(GTRXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_2
       (.I0(GTRXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1__0 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__2[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3__0 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1__0_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1__0
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1__0_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1__0 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1__0 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1__0 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1__0 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3__0 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2__0_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[2]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[3]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[4]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[5]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[6]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[7]_i_3__0_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2__0_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2__0
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'hE)) 
    reset_time_out_i_3
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .O(reset_time_out_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h34347674)) 
    reset_time_out_i_4
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .I2(rx_state[0]),
        .I3(\FSM_sequential_rx_state_reg[0]_0 ),
        .I4(rx_state[1]),
        .O(reset_time_out_i_4_n_0));
  FDSE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_0),
        .Q(reset_time_out_reg_n_0),
        .S(out));
  LUT5 #(
    .INIT(32'hFEFF0010)) 
    run_phase_alignment_int_i_1__0
       (.I0(rx_state[2]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .I3(rx_state[0]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1__0_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(data_out_0),
        .Q(run_phase_alignment_int_s3_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rx_fsm_reset_done_int_i_5
       (.I0(rx_state[1]),
        .I1(rx_state[0]),
        .O(rx_fsm_reset_done_int_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    rx_fsm_reset_done_int_i_6
       (.I0(rx_state[3]),
        .I1(rx_state[2]),
        .O(rx_fsm_reset_done_int_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_1),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(rx_fsm_reset_done_int_s2),
        .Q(rx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(rxresetdone_s2),
        .Q(rxresetdone_s3),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10 sync_RXRESETDONE
       (.data_out(rxresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11 sync_cplllock
       (.\FSM_sequential_rx_state_reg[1] (sync_cplllock_n_0),
        .Q(rx_state[3:1]),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg),
        .rxresetdone_s3(rxresetdone_s3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12 sync_data_valid
       (.D({rx_state__0[3],rx_state__0[1:0]}),
        .E(sync_data_valid_n_5),
        .\FSM_sequential_rx_state_reg[0] (\FSM_sequential_rx_state[3]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_0 (\FSM_sequential_rx_state[3]_i_7_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_1 (\FSM_sequential_rx_state_reg[0]_0 ),
        .\FSM_sequential_rx_state_reg[0]_2 (\FSM_sequential_rx_state[0]_i_2_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_3 (init_wait_done_reg_n_0),
        .\FSM_sequential_rx_state_reg[1] (sync_data_valid_n_0),
        .\FSM_sequential_rx_state_reg[1]_0 (\FSM_sequential_rx_state[1]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[3] (\FSM_sequential_rx_state[3]_i_9_n_0 ),
        .Q(rx_state),
        .data_in(data_in),
        .data_out(data_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_3_n_0),
        .reset_time_out_reg_1(reset_time_out_i_4_n_0),
        .reset_time_out_reg_2(reset_time_out_reg_n_0),
        .rx_fsm_reset_done_int_reg(sync_data_valid_n_1),
        .rx_fsm_reset_done_int_reg_0(rx_fsm_reset_done_int_i_5_n_0),
        .rx_fsm_reset_done_int_reg_1(time_out_100us_reg_n_0),
        .rx_fsm_reset_done_int_reg_2(time_out_1us_reg_n_0),
        .rx_fsm_reset_done_int_reg_3(rx_fsm_reset_done_int_i_6_n_0),
        .time_out_wait_bypass_s3(time_out_wait_bypass_s3));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out_0),
        .data_sync_reg1_0(data_sync_reg6));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(rx_fsm_reset_done_int_s2),
        .data_sync_reg6_0(data_sync_reg6));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000100)) 
    time_out_100us_i_1
       (.I0(time_out_2ms_i_4_n_0),
        .I1(time_out_counter_reg[17]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_100us_i_2_n_0),
        .I4(time_out_100us_i_3_n_0),
        .I5(time_out_100us_reg_n_0),
        .O(time_out_100us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    time_out_100us_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[14]),
        .O(time_out_100us_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_out_100us_i_3
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[0]),
        .I2(time_out_counter_reg[1]),
        .I3(time_out_counter_reg[15]),
        .I4(time_out_counter_reg[13]),
        .O(time_out_100us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_100us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_100us_i_1_n_0),
        .Q(time_out_100us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00100000)) 
    time_out_1us_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_1us_i_2_n_0),
        .I2(time_out_counter_reg[3]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_1us_i_3_n_0),
        .I5(time_out_1us_reg_n_0),
        .O(time_out_1us_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'hE)) 
    time_out_1us_i_2
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_out_1us_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    time_out_1us_i_3
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[11]),
        .I2(time_out_counter_reg[6]),
        .I3(time_out_counter_reg[8]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[12]),
        .O(time_out_1us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_1us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_1us_i_1_n_0),
        .Q(time_out_1us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'hFF01)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_2ms_i_3__0_n_0),
        .I2(time_out_2ms_i_4_n_0),
        .I3(time_out_2ms_reg_n_0),
        .O(time_out_2ms_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    time_out_2ms_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_100us_i_3_n_0),
        .O(time_out_2ms_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_2ms_i_3__0
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[18]),
        .I3(time_out_counter_reg[17]),
        .O(time_out_2ms_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFFFF)) 
    time_out_2ms_i_4
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[9]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF7FF)) 
    \time_out_counter[0]_i_1 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_2ms_i_2_n_0),
        .I5(time_out_2ms_i_4_n_0),
        .O(time_out_counter));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_3 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2__0_n_0 ,\time_out_counter_reg[0]_i_2__0_n_1 ,\time_out_counter_reg[0]_i_2__0_n_2 ,\time_out_counter_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2__0_n_4 ,\time_out_counter_reg[0]_i_2__0_n_5 ,\time_out_counter_reg[0]_i_2__0_n_6 ,\time_out_counter_reg[0]_i_2__0_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1__0 
       (.CI(\time_out_counter_reg[8]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1__0_n_0 ,\time_out_counter_reg[12]_i_1__0_n_1 ,\time_out_counter_reg[12]_i_1__0_n_2 ,\time_out_counter_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1__0_n_4 ,\time_out_counter_reg[12]_i_1__0_n_5 ,\time_out_counter_reg[12]_i_1__0_n_6 ,\time_out_counter_reg[12]_i_1__0_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1__0 
       (.CI(\time_out_counter_reg[12]_i_1__0_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1__0_n_2 ,\time_out_counter_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1__0_n_5 ,\time_out_counter_reg[16]_i_1__0_n_6 ,\time_out_counter_reg[16]_i_1__0_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1__0 
       (.CI(\time_out_counter_reg[0]_i_2__0_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1__0_n_0 ,\time_out_counter_reg[4]_i_1__0_n_1 ,\time_out_counter_reg[4]_i_1__0_n_2 ,\time_out_counter_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1__0_n_4 ,\time_out_counter_reg[4]_i_1__0_n_5 ,\time_out_counter_reg[4]_i_1__0_n_6 ,\time_out_counter_reg[4]_i_1__0_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1__0 
       (.CI(\time_out_counter_reg[4]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1__0_n_0 ,\time_out_counter_reg[8]_i_1__0_n_1 ,\time_out_counter_reg[8]_i_1__0_n_2 ,\time_out_counter_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1__0_n_4 ,\time_out_counter_reg[8]_i_1__0_n_5 ,\time_out_counter_reg[8]_i_1__0_n_6 ,\time_out_counter_reg[8]_i_1__0_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out_reg_n_0));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2__0_n_0),
        .I3(run_phase_alignment_int_s3_reg_n_0),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    time_out_wait_bypass_i_2__0
       (.I0(time_out_wait_bypass_i_3__0_n_0),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[11]),
        .I3(wait_bypass_count_reg[0]),
        .I4(time_out_wait_bypass_i_4__0_n_0),
        .O(time_out_wait_bypass_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_3__0
       (.I0(wait_bypass_count_reg[9]),
        .I1(wait_bypass_count_reg[4]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[2]),
        .O(time_out_wait_bypass_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    time_out_wait_bypass_i_4__0
       (.I0(wait_bypass_count_reg[5]),
        .I1(wait_bypass_count_reg[7]),
        .I2(wait_bypass_count_reg[3]),
        .I3(wait_bypass_count_reg[6]),
        .I4(wait_bypass_count_reg[10]),
        .I5(wait_bypass_count_reg[8]),
        .O(time_out_wait_bypass_i_4__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry
       (.CI(1'b0),
        .CO({time_tlock_max1_carry_n_0,time_tlock_max1_carry_n_1,time_tlock_max1_carry_n_2,time_tlock_max1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({time_tlock_max1_carry_i_1_n_0,time_tlock_max1_carry_i_2_n_0,time_tlock_max1_carry_i_3_n_0,time_tlock_max1_carry_i_4_n_0}),
        .O(NLW_time_tlock_max1_carry_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry_i_5_n_0,time_tlock_max1_carry_i_6_n_0,time_tlock_max1_carry_i_7_n_0,time_tlock_max1_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__0
       (.CI(time_tlock_max1_carry_n_0),
        .CO({time_tlock_max1_carry__0_n_0,time_tlock_max1_carry__0_n_1,time_tlock_max1_carry__0_n_2,time_tlock_max1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({time_out_counter_reg[15],time_tlock_max1_carry__0_i_1_n_0,1'b0,time_tlock_max1_carry__0_i_2_n_0}),
        .O(NLW_time_tlock_max1_carry__0_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry__0_i_3_n_0,time_tlock_max1_carry__0_i_4_n_0,time_tlock_max1_carry__0_i_5_n_0,time_tlock_max1_carry__0_i_6_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__0_i_1
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[13]),
        .O(time_tlock_max1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_2
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(time_tlock_max1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_3
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .O(time_tlock_max1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__0_i_4
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[12]),
        .O(time_tlock_max1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_5
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[11]),
        .O(time_tlock_max1_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_6
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[8]),
        .O(time_tlock_max1_carry__0_i_6_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__1
       (.CI(time_tlock_max1_carry__0_n_0),
        .CO({NLW_time_tlock_max1_carry__1_CO_UNCONNECTED[3:2],time_tlock_max1,time_tlock_max1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,time_out_counter_reg[18],time_tlock_max1_carry__1_i_1_n_0}),
        .O(NLW_time_tlock_max1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,time_tlock_max1_carry__1_i_2_n_0,time_tlock_max1_carry__1_i_3_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__1_i_1
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_tlock_max1_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    time_tlock_max1_carry__1_i_2
       (.I0(time_out_counter_reg[18]),
        .O(time_tlock_max1_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__1_i_3
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[16]),
        .O(time_tlock_max1_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_1
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(time_tlock_max1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry_i_2
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(time_tlock_max1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_3
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(time_tlock_max1_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_4
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[1]),
        .O(time_tlock_max1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_5
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[6]),
        .O(time_tlock_max1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry_i_6
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[4]),
        .O(time_tlock_max1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_7
       (.I0(time_out_counter_reg[3]),
        .I1(time_out_counter_reg[2]),
        .O(time_tlock_max1_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_8
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(time_tlock_max1_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    time_tlock_max_i_1
       (.I0(check_tlock_max_reg_n_0),
        .I1(time_tlock_max1),
        .I2(time_tlock_max),
        .O(time_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max),
        .R(reset_time_out_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1__0 
       (.I0(run_phase_alignment_int_s3_reg_n_0),
        .O(\wait_bypass_count[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2__0 
       (.I0(time_out_wait_bypass_i_2__0_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3__0_n_0 ,\wait_bypass_count_reg[0]_i_3__0_n_1 ,\wait_bypass_count_reg[0]_i_3__0_n_2 ,\wait_bypass_count_reg[0]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3__0_n_4 ,\wait_bypass_count_reg[0]_i_3__0_n_5 ,\wait_bypass_count_reg[0]_i_3__0_n_6 ,\wait_bypass_count_reg[0]_i_3__0_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1__0 
       (.CI(\wait_bypass_count_reg[8]_i_1__0_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED [3:1],\wait_bypass_count_reg[12]_i_1__0_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[12]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1__0 
       (.CI(\wait_bypass_count_reg[0]_i_3__0_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1__0_n_0 ,\wait_bypass_count_reg[4]_i_1__0_n_1 ,\wait_bypass_count_reg[4]_i_1__0_n_2 ,\wait_bypass_count_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1__0_n_4 ,\wait_bypass_count_reg[4]_i_1__0_n_5 ,\wait_bypass_count_reg[4]_i_1__0_n_6 ,\wait_bypass_count_reg[4]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1__0 
       (.CI(\wait_bypass_count_reg[4]_i_1__0_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1__0_n_0 ,\wait_bypass_count_reg[8]_i_1__0_n_1 ,\wait_bypass_count_reg[8]_i_1__0_n_2 ,\wait_bypass_count_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1__0_n_4 ,\wait_bypass_count_reg[8]_i_1__0_n_5 ,\wait_bypass_count_reg[8]_i_1__0_n_6 ,\wait_bypass_count_reg[8]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0__0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1__0 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1__0 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1__0 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \wait_time_cnt[6]_i_1 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .O(\wait_time_cnt[6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0__0),
        .Q(wait_time_cnt_reg[0]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[1]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[2]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[3]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[4]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[5]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[6]_i_3__0_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
   (mmcm_reset,
    gt0_cpllreset_t,
    data_in,
    gt0_txuserrdy_t,
    gt0_gttxreset_in0_out,
    independent_clock_bufg,
    data_sync_reg1,
    out,
    gtxe2_i,
    gt0_cpllrefclklost_i,
    data_sync_reg1_0,
    data_sync_reg1_1,
    data_sync_reg1_2);
  output mmcm_reset;
  output gt0_cpllreset_t;
  output data_in;
  output gt0_txuserrdy_t;
  output gt0_gttxreset_in0_out;
  input independent_clock_bufg;
  input data_sync_reg1;
  input [0:0]out;
  input gtxe2_i;
  input gt0_cpllrefclklost_i;
  input data_sync_reg1_0;
  input data_sync_reg1_1;
  input data_sync_reg1_2;

  wire CPLL_RESET_i_1_n_0;
  wire CPLL_RESET_i_2_n_0;
  wire \FSM_sequential_tx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_8_n_0 ;
  wire GTTXRESET;
  wire MMCM_RESET_i_1_n_0;
  wire TXUSERRDY_i_1_n_0;
  wire clear;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg1_2;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_txuserrdy_t;
  wire gttxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1_n_0 ;
  wire \init_wait_count[6]_i_1_n_0 ;
  wire \init_wait_count[6]_i_3_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1_n_0 ;
  wire \mmcm_lock_count[3]_i_1_n_0 ;
  wire \mmcm_lock_count[4]_i_1_n_0 ;
  wire \mmcm_lock_count[5]_i_1_n_0 ;
  wire \mmcm_lock_count[6]_i_1_n_0 ;
  wire \mmcm_lock_count[7]_i_2_n_0 ;
  wire \mmcm_lock_count[7]_i_3_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2_n_0;
  wire mmcm_reset;
  wire [0:0]out;
  wire [6:1]p_0_in__0;
  wire [1:0]p_0_in__1;
  wire pll_reset_asserted_i_1_n_0;
  wire pll_reset_asserted_i_2_n_0;
  wire pll_reset_asserted_reg_n_0;
  wire refclk_stable_count;
  wire \refclk_stable_count[0]_i_3_n_0 ;
  wire \refclk_stable_count[0]_i_4_n_0 ;
  wire \refclk_stable_count[0]_i_5_n_0 ;
  wire \refclk_stable_count[0]_i_6_n_0 ;
  wire \refclk_stable_count[0]_i_7_n_0 ;
  wire \refclk_stable_count[0]_i_8_n_0 ;
  wire \refclk_stable_count[0]_i_9_n_0 ;
  wire [31:0]refclk_stable_count_reg;
  wire \refclk_stable_count_reg[0]_i_2_n_0 ;
  wire \refclk_stable_count_reg[0]_i_2_n_1 ;
  wire \refclk_stable_count_reg[0]_i_2_n_2 ;
  wire \refclk_stable_count_reg[0]_i_2_n_3 ;
  wire \refclk_stable_count_reg[0]_i_2_n_4 ;
  wire \refclk_stable_count_reg[0]_i_2_n_5 ;
  wire \refclk_stable_count_reg[0]_i_2_n_6 ;
  wire \refclk_stable_count_reg[0]_i_2_n_7 ;
  wire \refclk_stable_count_reg[12]_i_1_n_0 ;
  wire \refclk_stable_count_reg[12]_i_1_n_1 ;
  wire \refclk_stable_count_reg[12]_i_1_n_2 ;
  wire \refclk_stable_count_reg[12]_i_1_n_3 ;
  wire \refclk_stable_count_reg[12]_i_1_n_4 ;
  wire \refclk_stable_count_reg[12]_i_1_n_5 ;
  wire \refclk_stable_count_reg[12]_i_1_n_6 ;
  wire \refclk_stable_count_reg[12]_i_1_n_7 ;
  wire \refclk_stable_count_reg[16]_i_1_n_0 ;
  wire \refclk_stable_count_reg[16]_i_1_n_1 ;
  wire \refclk_stable_count_reg[16]_i_1_n_2 ;
  wire \refclk_stable_count_reg[16]_i_1_n_3 ;
  wire \refclk_stable_count_reg[16]_i_1_n_4 ;
  wire \refclk_stable_count_reg[16]_i_1_n_5 ;
  wire \refclk_stable_count_reg[16]_i_1_n_6 ;
  wire \refclk_stable_count_reg[16]_i_1_n_7 ;
  wire \refclk_stable_count_reg[20]_i_1_n_0 ;
  wire \refclk_stable_count_reg[20]_i_1_n_1 ;
  wire \refclk_stable_count_reg[20]_i_1_n_2 ;
  wire \refclk_stable_count_reg[20]_i_1_n_3 ;
  wire \refclk_stable_count_reg[20]_i_1_n_4 ;
  wire \refclk_stable_count_reg[20]_i_1_n_5 ;
  wire \refclk_stable_count_reg[20]_i_1_n_6 ;
  wire \refclk_stable_count_reg[20]_i_1_n_7 ;
  wire \refclk_stable_count_reg[24]_i_1_n_0 ;
  wire \refclk_stable_count_reg[24]_i_1_n_1 ;
  wire \refclk_stable_count_reg[24]_i_1_n_2 ;
  wire \refclk_stable_count_reg[24]_i_1_n_3 ;
  wire \refclk_stable_count_reg[24]_i_1_n_4 ;
  wire \refclk_stable_count_reg[24]_i_1_n_5 ;
  wire \refclk_stable_count_reg[24]_i_1_n_6 ;
  wire \refclk_stable_count_reg[24]_i_1_n_7 ;
  wire \refclk_stable_count_reg[28]_i_1_n_1 ;
  wire \refclk_stable_count_reg[28]_i_1_n_2 ;
  wire \refclk_stable_count_reg[28]_i_1_n_3 ;
  wire \refclk_stable_count_reg[28]_i_1_n_4 ;
  wire \refclk_stable_count_reg[28]_i_1_n_5 ;
  wire \refclk_stable_count_reg[28]_i_1_n_6 ;
  wire \refclk_stable_count_reg[28]_i_1_n_7 ;
  wire \refclk_stable_count_reg[4]_i_1_n_0 ;
  wire \refclk_stable_count_reg[4]_i_1_n_1 ;
  wire \refclk_stable_count_reg[4]_i_1_n_2 ;
  wire \refclk_stable_count_reg[4]_i_1_n_3 ;
  wire \refclk_stable_count_reg[4]_i_1_n_4 ;
  wire \refclk_stable_count_reg[4]_i_1_n_5 ;
  wire \refclk_stable_count_reg[4]_i_1_n_6 ;
  wire \refclk_stable_count_reg[4]_i_1_n_7 ;
  wire \refclk_stable_count_reg[8]_i_1_n_0 ;
  wire \refclk_stable_count_reg[8]_i_1_n_1 ;
  wire \refclk_stable_count_reg[8]_i_1_n_2 ;
  wire \refclk_stable_count_reg[8]_i_1_n_3 ;
  wire \refclk_stable_count_reg[8]_i_1_n_4 ;
  wire \refclk_stable_count_reg[8]_i_1_n_5 ;
  wire \refclk_stable_count_reg[8]_i_1_n_6 ;
  wire \refclk_stable_count_reg[8]_i_1_n_7 ;
  wire refclk_stable_i_1_n_0;
  wire refclk_stable_i_2_n_0;
  wire refclk_stable_i_3_n_0;
  wire refclk_stable_i_4_n_0;
  wire refclk_stable_i_5_n_0;
  wire refclk_stable_i_6_n_0;
  wire refclk_stable_reg_n_0;
  wire reset_time_out;
  wire reset_time_out_i_2__0_n_0;
  wire run_phase_alignment_int_i_1_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3;
  wire sel;
  wire sync_cplllock_n_0;
  wire sync_cplllock_n_1;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2__0_n_0;
  wire time_out_2ms_i_3_n_0;
  wire time_out_2ms_i_4__0_n_0;
  wire time_out_2ms_i_5_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_500us_i_1_n_0;
  wire time_out_500us_i_2_n_0;
  wire time_out_500us_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3__0_n_0 ;
  wire \time_out_counter[0]_i_4_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2_n_0 ;
  wire \time_out_counter_reg[0]_i_2_n_1 ;
  wire \time_out_counter_reg[0]_i_2_n_2 ;
  wire \time_out_counter_reg[0]_i_2_n_3 ;
  wire \time_out_counter_reg[0]_i_2_n_4 ;
  wire \time_out_counter_reg[0]_i_2_n_5 ;
  wire \time_out_counter_reg[0]_i_2_n_6 ;
  wire \time_out_counter_reg[0]_i_2_n_7 ;
  wire \time_out_counter_reg[12]_i_1_n_0 ;
  wire \time_out_counter_reg[12]_i_1_n_1 ;
  wire \time_out_counter_reg[12]_i_1_n_2 ;
  wire \time_out_counter_reg[12]_i_1_n_3 ;
  wire \time_out_counter_reg[12]_i_1_n_4 ;
  wire \time_out_counter_reg[12]_i_1_n_5 ;
  wire \time_out_counter_reg[12]_i_1_n_6 ;
  wire \time_out_counter_reg[12]_i_1_n_7 ;
  wire \time_out_counter_reg[16]_i_1_n_2 ;
  wire \time_out_counter_reg[16]_i_1_n_3 ;
  wire \time_out_counter_reg[16]_i_1_n_5 ;
  wire \time_out_counter_reg[16]_i_1_n_6 ;
  wire \time_out_counter_reg[16]_i_1_n_7 ;
  wire \time_out_counter_reg[4]_i_1_n_0 ;
  wire \time_out_counter_reg[4]_i_1_n_1 ;
  wire \time_out_counter_reg[4]_i_1_n_2 ;
  wire \time_out_counter_reg[4]_i_1_n_3 ;
  wire \time_out_counter_reg[4]_i_1_n_4 ;
  wire \time_out_counter_reg[4]_i_1_n_5 ;
  wire \time_out_counter_reg[4]_i_1_n_6 ;
  wire \time_out_counter_reg[4]_i_1_n_7 ;
  wire \time_out_counter_reg[8]_i_1_n_0 ;
  wire \time_out_counter_reg[8]_i_1_n_1 ;
  wire \time_out_counter_reg[8]_i_1_n_2 ;
  wire \time_out_counter_reg[8]_i_1_n_3 ;
  wire \time_out_counter_reg[8]_i_1_n_4 ;
  wire \time_out_counter_reg[8]_i_1_n_5 ;
  wire \time_out_counter_reg[8]_i_1_n_6 ;
  wire \time_out_counter_reg[8]_i_1_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2_n_0;
  wire time_out_wait_bypass_i_3_n_0;
  wire time_out_wait_bypass_i_4_n_0;
  wire time_out_wait_bypass_i_5_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max_i_1_n_0;
  wire time_tlock_max_i_2_n_0;
  wire time_tlock_max_i_3_n_0;
  wire time_tlock_max_i_4_n_0;
  wire time_tlock_max_reg_n_0;
  wire tx_fsm_reset_done_int_i_1_n_0;
  wire tx_fsm_reset_done_int_s2;
  wire tx_fsm_reset_done_int_s3;
  wire [3:0]tx_state;
  wire [3:0]tx_state__0;
  wire txresetdone_s2;
  wire txresetdone_s3;
  wire \wait_bypass_count[0]_i_2_n_0 ;
  wire \wait_bypass_count[0]_i_4__0_n_0 ;
  wire [16:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1_n_0 ;
  wire \wait_bypass_count_reg[12]_i_1_n_1 ;
  wire \wait_bypass_count_reg[12]_i_1_n_2 ;
  wire \wait_bypass_count_reg[12]_i_1_n_3 ;
  wire \wait_bypass_count_reg[12]_i_1_n_4 ;
  wire \wait_bypass_count_reg[12]_i_1_n_5 ;
  wire \wait_bypass_count_reg[12]_i_1_n_6 ;
  wire \wait_bypass_count_reg[12]_i_1_n_7 ;
  wire \wait_bypass_count_reg[16]_i_1_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1_n_7 ;
  wire [0:0]wait_time_cnt0;
  wire wait_time_cnt0_0;
  wire \wait_time_cnt[1]_i_1_n_0 ;
  wire \wait_time_cnt[2]_i_1_n_0 ;
  wire \wait_time_cnt[3]_i_1_n_0 ;
  wire \wait_time_cnt[4]_i_1_n_0 ;
  wire \wait_time_cnt[5]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_3_n_0 ;
  wire \wait_time_cnt[6]_i_4_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:3]\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFF1F0000001F)) 
    CPLL_RESET_i_1
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(gt0_cpllrefclklost_i),
        .I2(refclk_stable_reg_n_0),
        .I3(CPLL_RESET_i_2_n_0),
        .I4(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I5(gt0_cpllreset_t),
        .O(CPLL_RESET_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'hE)) 
    CPLL_RESET_i_2
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .O(CPLL_RESET_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    CPLL_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(CPLL_RESET_i_1_n_0),
        .Q(gt0_cpllreset_t),
        .R(out));
  LUT6 #(
    .INIT(64'hF3FFF3F0F5F0F5F0)) 
    \FSM_sequential_tx_state[0]_i_1 
       (.I0(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I1(\FSM_sequential_tx_state[0]_i_2_n_0 ),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(tx_state[2]),
        .I4(time_out_2ms_reg_n_0),
        .I5(tx_state[1]),
        .O(tx_state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_2 
       (.I0(reset_time_out),
        .I1(time_out_500us_reg_n_0),
        .O(\FSM_sequential_tx_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_3 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .O(\FSM_sequential_tx_state[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h005A001A)) 
    \FSM_sequential_tx_state[1]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .O(tx_state__0[1]));
  LUT6 #(
    .INIT(64'h04000C0C06020C0C)) 
    \FSM_sequential_tx_state[2]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I4(tx_state[0]),
        .I5(time_out_2ms_reg_n_0),
        .O(tx_state__0[2]));
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_tx_state[2]_i_2 
       (.I0(time_tlock_max_reg_n_0),
        .I1(reset_time_out),
        .I2(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hF4FF4444)) 
    \FSM_sequential_tx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(tx_state[3]),
        .I2(reset_time_out),
        .I3(time_out_500us_reg_n_0),
        .I4(\FSM_sequential_tx_state[3]_i_8_n_0 ),
        .O(tx_state__0[3]));
  LUT6 #(
    .INIT(64'h00BA000000000000)) 
    \FSM_sequential_tx_state[3]_i_3 
       (.I0(txresetdone_s3),
        .I1(reset_time_out),
        .I2(time_out_500us_reg_n_0),
        .I3(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I4(tx_state[2]),
        .I5(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000300FF00AA)) 
    \FSM_sequential_tx_state[3]_i_4 
       (.I0(init_wait_done_reg_n_0),
        .I1(\wait_time_cnt[6]_i_4_n_0 ),
        .I2(wait_time_cnt_reg[6]),
        .I3(tx_state[0]),
        .I4(tx_state[3]),
        .I5(CPLL_RESET_i_2_n_0),
        .O(\FSM_sequential_tx_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0404040400040000)) 
    \FSM_sequential_tx_state[3]_i_6 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(reset_time_out),
        .I4(time_tlock_max_reg_n_0),
        .I5(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \FSM_sequential_tx_state[3]_i_7 
       (.I0(tx_state[2]),
        .I1(tx_state[3]),
        .I2(tx_state[0]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_tx_state[3]_i_8 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_8_n_0 ));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[0]),
        .Q(tx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[1]),
        .Q(tx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[2]),
        .Q(tx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[3]),
        .Q(tx_state[3]),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hFFF70004)) 
    MMCM_RESET_i_1
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[3]),
        .I3(tx_state[1]),
        .I4(mmcm_reset),
        .O(MMCM_RESET_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    MMCM_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(MMCM_RESET_i_1_n_0),
        .Q(mmcm_reset),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hFFFD2000)) 
    TXUSERRDY_i_1
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(gt0_txuserrdy_t),
        .O(TXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    TXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(TXUSERRDY_i_1_n_0),
        .Q(gt0_txuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gttxreset_i_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[1]),
        .I2(tx_state[2]),
        .I3(tx_state[0]),
        .I4(GTTXRESET),
        .O(gttxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gttxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gttxreset_i_i_1_n_0),
        .Q(GTTXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_3
       (.I0(GTTXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(gt0_gttxreset_in0_out));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__0[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[2]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[3]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[4]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[5]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[6]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[7]_i_3_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000CD55CCCCCCCC)) 
    pll_reset_asserted_i_1
       (.I0(tx_state[3]),
        .I1(pll_reset_asserted_reg_n_0),
        .I2(gt0_cpllrefclklost_i),
        .I3(refclk_stable_reg_n_0),
        .I4(tx_state[1]),
        .I5(pll_reset_asserted_i_2_n_0),
        .O(pll_reset_asserted_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h02)) 
    pll_reset_asserted_i_2
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .O(pll_reset_asserted_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_reset_asserted_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pll_reset_asserted_i_1_n_0),
        .Q(pll_reset_asserted_reg_n_0),
        .R(out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \refclk_stable_count[0]_i_1 
       (.I0(\refclk_stable_count[0]_i_3_n_0 ),
        .I1(\refclk_stable_count[0]_i_4_n_0 ),
        .I2(\refclk_stable_count[0]_i_5_n_0 ),
        .I3(\refclk_stable_count[0]_i_6_n_0 ),
        .I4(\refclk_stable_count[0]_i_7_n_0 ),
        .I5(\refclk_stable_count[0]_i_8_n_0 ),
        .O(refclk_stable_count));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    \refclk_stable_count[0]_i_3 
       (.I0(refclk_stable_count_reg[13]),
        .I1(refclk_stable_count_reg[12]),
        .I2(refclk_stable_count_reg[10]),
        .I3(refclk_stable_count_reg[11]),
        .I4(refclk_stable_count_reg[9]),
        .I5(refclk_stable_count_reg[8]),
        .O(\refclk_stable_count[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \refclk_stable_count[0]_i_4 
       (.I0(refclk_stable_count_reg[19]),
        .I1(refclk_stable_count_reg[18]),
        .I2(refclk_stable_count_reg[16]),
        .I3(refclk_stable_count_reg[17]),
        .I4(refclk_stable_count_reg[15]),
        .I5(refclk_stable_count_reg[14]),
        .O(\refclk_stable_count[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_5 
       (.I0(refclk_stable_count_reg[30]),
        .I1(refclk_stable_count_reg[31]),
        .I2(refclk_stable_count_reg[28]),
        .I3(refclk_stable_count_reg[29]),
        .I4(refclk_stable_count_reg[27]),
        .I5(refclk_stable_count_reg[26]),
        .O(\refclk_stable_count[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_6 
       (.I0(refclk_stable_count_reg[24]),
        .I1(refclk_stable_count_reg[25]),
        .I2(refclk_stable_count_reg[22]),
        .I3(refclk_stable_count_reg[23]),
        .I4(refclk_stable_count_reg[21]),
        .I5(refclk_stable_count_reg[20]),
        .O(\refclk_stable_count[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \refclk_stable_count[0]_i_7 
       (.I0(refclk_stable_count_reg[0]),
        .I1(refclk_stable_count_reg[1]),
        .O(\refclk_stable_count[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \refclk_stable_count[0]_i_8 
       (.I0(refclk_stable_count_reg[6]),
        .I1(refclk_stable_count_reg[7]),
        .I2(refclk_stable_count_reg[4]),
        .I3(refclk_stable_count_reg[5]),
        .I4(refclk_stable_count_reg[3]),
        .I5(refclk_stable_count_reg[2]),
        .O(\refclk_stable_count[0]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \refclk_stable_count[0]_i_9 
       (.I0(refclk_stable_count_reg[0]),
        .O(\refclk_stable_count[0]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_7 ),
        .Q(refclk_stable_count_reg[0]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\refclk_stable_count_reg[0]_i_2_n_0 ,\refclk_stable_count_reg[0]_i_2_n_1 ,\refclk_stable_count_reg[0]_i_2_n_2 ,\refclk_stable_count_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\refclk_stable_count_reg[0]_i_2_n_4 ,\refclk_stable_count_reg[0]_i_2_n_5 ,\refclk_stable_count_reg[0]_i_2_n_6 ,\refclk_stable_count_reg[0]_i_2_n_7 }),
        .S({refclk_stable_count_reg[3:1],\refclk_stable_count[0]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[10] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[11] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[12] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[12]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[12]_i_1 
       (.CI(\refclk_stable_count_reg[8]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[12]_i_1_n_0 ,\refclk_stable_count_reg[12]_i_1_n_1 ,\refclk_stable_count_reg[12]_i_1_n_2 ,\refclk_stable_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[12]_i_1_n_4 ,\refclk_stable_count_reg[12]_i_1_n_5 ,\refclk_stable_count_reg[12]_i_1_n_6 ,\refclk_stable_count_reg[12]_i_1_n_7 }),
        .S(refclk_stable_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[13] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[14] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[15] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[16] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[16]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[16]_i_1 
       (.CI(\refclk_stable_count_reg[12]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[16]_i_1_n_0 ,\refclk_stable_count_reg[16]_i_1_n_1 ,\refclk_stable_count_reg[16]_i_1_n_2 ,\refclk_stable_count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[16]_i_1_n_4 ,\refclk_stable_count_reg[16]_i_1_n_5 ,\refclk_stable_count_reg[16]_i_1_n_6 ,\refclk_stable_count_reg[16]_i_1_n_7 }),
        .S(refclk_stable_count_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[17] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[18] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[19] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_6 ),
        .Q(refclk_stable_count_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[20] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[20]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[20]_i_1 
       (.CI(\refclk_stable_count_reg[16]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[20]_i_1_n_0 ,\refclk_stable_count_reg[20]_i_1_n_1 ,\refclk_stable_count_reg[20]_i_1_n_2 ,\refclk_stable_count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[20]_i_1_n_4 ,\refclk_stable_count_reg[20]_i_1_n_5 ,\refclk_stable_count_reg[20]_i_1_n_6 ,\refclk_stable_count_reg[20]_i_1_n_7 }),
        .S(refclk_stable_count_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[21] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[22] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[23] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[24] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[24]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[24]_i_1 
       (.CI(\refclk_stable_count_reg[20]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[24]_i_1_n_0 ,\refclk_stable_count_reg[24]_i_1_n_1 ,\refclk_stable_count_reg[24]_i_1_n_2 ,\refclk_stable_count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[24]_i_1_n_4 ,\refclk_stable_count_reg[24]_i_1_n_5 ,\refclk_stable_count_reg[24]_i_1_n_6 ,\refclk_stable_count_reg[24]_i_1_n_7 }),
        .S(refclk_stable_count_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[25] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[26] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[27] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[28] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[28]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[28]_i_1 
       (.CI(\refclk_stable_count_reg[24]_i_1_n_0 ),
        .CO({\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED [3],\refclk_stable_count_reg[28]_i_1_n_1 ,\refclk_stable_count_reg[28]_i_1_n_2 ,\refclk_stable_count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[28]_i_1_n_4 ,\refclk_stable_count_reg[28]_i_1_n_5 ,\refclk_stable_count_reg[28]_i_1_n_6 ,\refclk_stable_count_reg[28]_i_1_n_7 }),
        .S(refclk_stable_count_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[29] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_5 ),
        .Q(refclk_stable_count_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[30] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[31] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_4 ),
        .Q(refclk_stable_count_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[4]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[4]_i_1 
       (.CI(\refclk_stable_count_reg[0]_i_2_n_0 ),
        .CO({\refclk_stable_count_reg[4]_i_1_n_0 ,\refclk_stable_count_reg[4]_i_1_n_1 ,\refclk_stable_count_reg[4]_i_1_n_2 ,\refclk_stable_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[4]_i_1_n_4 ,\refclk_stable_count_reg[4]_i_1_n_5 ,\refclk_stable_count_reg[4]_i_1_n_6 ,\refclk_stable_count_reg[4]_i_1_n_7 }),
        .S(refclk_stable_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[8] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[8]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[8]_i_1 
       (.CI(\refclk_stable_count_reg[4]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[8]_i_1_n_0 ,\refclk_stable_count_reg[8]_i_1_n_1 ,\refclk_stable_count_reg[8]_i_1_n_2 ,\refclk_stable_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[8]_i_1_n_4 ,\refclk_stable_count_reg[8]_i_1_n_5 ,\refclk_stable_count_reg[8]_i_1_n_6 ,\refclk_stable_count_reg[8]_i_1_n_7 }),
        .S(refclk_stable_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[9] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    refclk_stable_i_1
       (.I0(\refclk_stable_count[0]_i_7_n_0 ),
        .I1(refclk_stable_i_2_n_0),
        .I2(refclk_stable_i_3_n_0),
        .I3(refclk_stable_i_4_n_0),
        .I4(refclk_stable_i_5_n_0),
        .I5(refclk_stable_i_6_n_0),
        .O(refclk_stable_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    refclk_stable_i_2
       (.I0(refclk_stable_count_reg[4]),
        .I1(refclk_stable_count_reg[5]),
        .I2(refclk_stable_count_reg[2]),
        .I3(refclk_stable_count_reg[3]),
        .I4(refclk_stable_count_reg[7]),
        .I5(refclk_stable_count_reg[6]),
        .O(refclk_stable_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    refclk_stable_i_3
       (.I0(refclk_stable_count_reg[10]),
        .I1(refclk_stable_count_reg[11]),
        .I2(refclk_stable_count_reg[8]),
        .I3(refclk_stable_count_reg[9]),
        .I4(refclk_stable_count_reg[12]),
        .I5(refclk_stable_count_reg[13]),
        .O(refclk_stable_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    refclk_stable_i_4
       (.I0(refclk_stable_count_reg[16]),
        .I1(refclk_stable_count_reg[17]),
        .I2(refclk_stable_count_reg[14]),
        .I3(refclk_stable_count_reg[15]),
        .I4(refclk_stable_count_reg[18]),
        .I5(refclk_stable_count_reg[19]),
        .O(refclk_stable_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_5
       (.I0(refclk_stable_count_reg[22]),
        .I1(refclk_stable_count_reg[23]),
        .I2(refclk_stable_count_reg[20]),
        .I3(refclk_stable_count_reg[21]),
        .I4(refclk_stable_count_reg[25]),
        .I5(refclk_stable_count_reg[24]),
        .O(refclk_stable_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_6
       (.I0(refclk_stable_count_reg[28]),
        .I1(refclk_stable_count_reg[29]),
        .I2(refclk_stable_count_reg[26]),
        .I3(refclk_stable_count_reg[27]),
        .I4(refclk_stable_count_reg[31]),
        .I5(refclk_stable_count_reg[30]),
        .O(refclk_stable_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    refclk_stable_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(refclk_stable_i_1_n_0),
        .Q(refclk_stable_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h440000FF50505050)) 
    reset_time_out_i_2__0
       (.I0(tx_state[3]),
        .I1(txresetdone_s3),
        .I2(init_wait_done_reg_n_0),
        .I3(tx_state[1]),
        .I4(tx_state[2]),
        .I5(tx_state[0]),
        .O(reset_time_out_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_cplllock_n_0),
        .Q(reset_time_out),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hFFFB0002)) 
    run_phase_alignment_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(data_out),
        .Q(run_phase_alignment_int_s3),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4 sync_TXRESETDONE
       (.data_out(txresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5 sync_cplllock
       (.E(sync_cplllock_n_1),
        .\FSM_sequential_tx_state_reg[0] (\FSM_sequential_tx_state[3]_i_3_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_0 (\FSM_sequential_tx_state[3]_i_4_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_1 (\FSM_sequential_tx_state[3]_i_6_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_2 (time_out_2ms_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_3 (\FSM_sequential_tx_state[3]_i_7_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_4 (pll_reset_asserted_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_5 (refclk_stable_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_6 (\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .Q(tx_state),
        .data_sync_reg1_0(data_sync_reg1_2),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out(reset_time_out),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_2__0_n_0),
        .reset_time_out_reg_1(init_wait_done_reg_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out),
        .data_sync_reg6_0(data_sync_reg1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(tx_fsm_reset_done_int_s2),
        .data_sync_reg1_0(data_sync_reg1));
  LUT4 #(
    .INIT(16'h00AE)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_reg_n_0),
        .I1(time_out_2ms_i_2__0_n_0),
        .I2(time_out_2ms_i_3_n_0),
        .I3(reset_time_out),
        .O(time_out_2ms_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    time_out_2ms_i_2__0
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[5]),
        .I5(time_tlock_max_i_3_n_0),
        .O(time_out_2ms_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    time_out_2ms_i_3
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_2ms_i_4__0_n_0),
        .I3(time_out_2ms_i_5_n_0),
        .O(time_out_2ms_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    time_out_2ms_i_4__0
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    time_out_2ms_i_5
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_counter_reg[1]),
        .O(time_out_2ms_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAEAAA)) 
    time_out_500us_i_1
       (.I0(time_out_500us_reg_n_0),
        .I1(time_out_500us_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_2ms_i_3_n_0),
        .I5(reset_time_out),
        .O(time_out_500us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    time_out_500us_i_2
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[11]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_out_500us_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_500us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_500us_i_1_n_0),
        .Q(time_out_500us_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \time_out_counter[0]_i_1__0 
       (.I0(time_tlock_max_i_3_n_0),
        .I1(\time_out_counter[0]_i_3__0_n_0 ),
        .I2(time_out_2ms_i_3_n_0),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[12]),
        .I5(time_out_counter_reg[5]),
        .O(time_out_counter));
  LUT2 #(
    .INIT(4'h8)) 
    \time_out_counter[0]_i_3__0 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .O(\time_out_counter[0]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_4 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2_n_0 ,\time_out_counter_reg[0]_i_2_n_1 ,\time_out_counter_reg[0]_i_2_n_2 ,\time_out_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2_n_4 ,\time_out_counter_reg[0]_i_2_n_5 ,\time_out_counter_reg[0]_i_2_n_6 ,\time_out_counter_reg[0]_i_2_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1 
       (.CI(\time_out_counter_reg[8]_i_1_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1_n_0 ,\time_out_counter_reg[12]_i_1_n_1 ,\time_out_counter_reg[12]_i_1_n_2 ,\time_out_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1_n_4 ,\time_out_counter_reg[12]_i_1_n_5 ,\time_out_counter_reg[12]_i_1_n_6 ,\time_out_counter_reg[12]_i_1_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1 
       (.CI(\time_out_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1_n_2 ,\time_out_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1_n_5 ,\time_out_counter_reg[16]_i_1_n_6 ,\time_out_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1 
       (.CI(\time_out_counter_reg[0]_i_2_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1_n_0 ,\time_out_counter_reg[4]_i_1_n_1 ,\time_out_counter_reg[4]_i_1_n_2 ,\time_out_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1_n_4 ,\time_out_counter_reg[4]_i_1_n_5 ,\time_out_counter_reg[4]_i_1_n_6 ,\time_out_counter_reg[4]_i_1_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1 
       (.CI(\time_out_counter_reg[4]_i_1_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1_n_0 ,\time_out_counter_reg[8]_i_1_n_1 ,\time_out_counter_reg[8]_i_1_n_2 ,\time_out_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1_n_4 ,\time_out_counter_reg[8]_i_1_n_5 ,\time_out_counter_reg[8]_i_1_n_6 ,\time_out_counter_reg[8]_i_1_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2_n_0),
        .I3(run_phase_alignment_int_s3),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFFF)) 
    time_out_wait_bypass_i_2
       (.I0(time_out_wait_bypass_i_3_n_0),
        .I1(time_out_wait_bypass_i_4_n_0),
        .I2(wait_bypass_count_reg[5]),
        .I3(wait_bypass_count_reg[13]),
        .I4(wait_bypass_count_reg[11]),
        .I5(time_out_wait_bypass_i_5_n_0),
        .O(time_out_wait_bypass_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    time_out_wait_bypass_i_3
       (.I0(wait_bypass_count_reg[16]),
        .I1(wait_bypass_count_reg[9]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[10]),
        .O(time_out_wait_bypass_i_3_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_4
       (.I0(wait_bypass_count_reg[4]),
        .I1(wait_bypass_count_reg[15]),
        .I2(wait_bypass_count_reg[6]),
        .I3(wait_bypass_count_reg[0]),
        .O(time_out_wait_bypass_i_4_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    time_out_wait_bypass_i_5
       (.I0(wait_bypass_count_reg[8]),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[7]),
        .I3(wait_bypass_count_reg[14]),
        .I4(wait_bypass_count_reg[2]),
        .I5(wait_bypass_count_reg[3]),
        .O(time_out_wait_bypass_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAEA)) 
    time_tlock_max_i_1
       (.I0(time_tlock_max_reg_n_0),
        .I1(time_tlock_max_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_tlock_max_i_3_n_0),
        .I4(time_tlock_max_i_4_n_0),
        .I5(reset_time_out),
        .O(time_tlock_max_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    time_tlock_max_i_2
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_tlock_max_i_2_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    time_tlock_max_i_3
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[15]),
        .I2(time_out_counter_reg[11]),
        .O(time_tlock_max_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_tlock_max_i_4
       (.I0(time_out_2ms_i_5_n_0),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[3]),
        .I4(time_out_counter_reg[4]),
        .O(time_tlock_max_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFF1000)) 
    tx_fsm_reset_done_int_i_1
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(data_in),
        .O(tx_fsm_reset_done_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_i_1_n_0),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_s2),
        .Q(tx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(txresetdone_s2),
        .Q(txresetdone_s3),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1 
       (.I0(run_phase_alignment_int_s3),
        .O(clear));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2 
       (.I0(time_out_wait_bypass_i_2_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4__0 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3_n_0 ,\wait_bypass_count_reg[0]_i_3_n_1 ,\wait_bypass_count_reg[0]_i_3_n_2 ,\wait_bypass_count_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3_n_4 ,\wait_bypass_count_reg[0]_i_3_n_5 ,\wait_bypass_count_reg[0]_i_3_n_6 ,\wait_bypass_count_reg[0]_i_3_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1 
       (.CI(\wait_bypass_count_reg[8]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[12]_i_1_n_0 ,\wait_bypass_count_reg[12]_i_1_n_1 ,\wait_bypass_count_reg[12]_i_1_n_2 ,\wait_bypass_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[12]_i_1_n_4 ,\wait_bypass_count_reg[12]_i_1_n_5 ,\wait_bypass_count_reg[12]_i_1_n_6 ,\wait_bypass_count_reg[12]_i_1_n_7 }),
        .S(wait_bypass_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[13] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[14] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[15] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[16] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[16]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[16]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[16]_i_1 
       (.CI(\wait_bypass_count_reg[12]_i_1_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED [3:1],\wait_bypass_count_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[16]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1 
       (.CI(\wait_bypass_count_reg[0]_i_3_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1_n_0 ,\wait_bypass_count_reg[4]_i_1_n_1 ,\wait_bypass_count_reg[4]_i_1_n_2 ,\wait_bypass_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1_n_4 ,\wait_bypass_count_reg[4]_i_1_n_5 ,\wait_bypass_count_reg[4]_i_1_n_6 ,\wait_bypass_count_reg[4]_i_1_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1 
       (.CI(\wait_bypass_count_reg[4]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1_n_0 ,\wait_bypass_count_reg[8]_i_1_n_1 ,\wait_bypass_count_reg[8]_i_1_n_2 ,\wait_bypass_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1_n_4 ,\wait_bypass_count_reg[8]_i_1_n_5 ,\wait_bypass_count_reg[8]_i_1_n_6 ,\wait_bypass_count_reg[8]_i_1_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0700)) 
    \wait_time_cnt[6]_i_1__0 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(tx_state[0]),
        .O(wait_time_cnt0_0));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(sel));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(wait_time_cnt0),
        .Q(wait_time_cnt_reg[0]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[1]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[2]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[3]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[4]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[5]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[6]_i_3_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(wait_time_cnt0_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    rxoutclk,
    txoutclk,
    mmcm_reset,
    out,
    signal_detect,
    CLK,
    data_in,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output mmcm_reset;
  input [0:0]out;
  input signal_detect;
  input CLK;
  input data_in;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;

  wire CLK;
  wire [2:0]configuration_vector;
  wire data_in;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire mmcm_reset;
  wire [0:0]out;
  wire powerdown;
  wire resetdone;
  wire rx_reset_done_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire rxoutclk;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire transceiver_inst_n_11;
  wire transceiver_inst_n_12;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101001110" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "gig_ethernet_pcs_pma_0" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintex7" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_v16_2_18 gig_ethernet_pcs_pma_0_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(data_in),
        .drp_daddr(NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED),
        .drp_di(NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(CLK));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block sync_block_rx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_12),
        .data_out(rx_reset_done_i));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0 sync_block_tx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_11),
        .resetdone(resetdone),
        .resetdone_0(rx_reset_done_i));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispmode),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(transceiver_inst_n_11),
        .data_sync_reg1(data_in),
        .enablealign(enablealign),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .powerdown(powerdown),
        .reset_sync5(mgt_tx_reset),
        .rx_fsm_reset_done_int_reg(transceiver_inst_n_12),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .status_vector(status_vector[1]),
        .txbuferr(txbuferr),
        .txchardispval_reg_reg_0(txchardispval),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking
   (gtrefclk_out,
    gtrefclk_bufg,
    mmcm_locked,
    userclk,
    userclk2,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    mmcm_reset,
    rxoutclk);
  output gtrefclk_out;
  output gtrefclk_bufg;
  output mmcm_locked;
  output userclk;
  output userclk2;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input mmcm_reset;
  input rxoutclk;

  wire clkfbout;
  wire clkout0;
  wire clkout1;
  wire gtrefclk_bufg;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire mmcm_locked;
  wire mmcm_reset;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire txoutclk_bufg;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_gtrefclk
       (.I(gtrefclk_out),
        .O(gtrefclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_txoutclk
       (.I(txoutclk),
        .O(txoutclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk
       (.I(clkout1),
        .O(userclk));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk2
       (.I(clkout0),
        .O(userclk2));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE2 #(
    .CLKCM_CFG("TRUE"),
    .CLKRCV_TRST("TRUE"),
    .CLKSWING_CFG(2'b11)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(16.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(16.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(8.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(16),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.000000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout),
        .CLKFBOUT(clkfbout),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(txoutclk_bufg),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clkout0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clkout1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(mmcm_locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(mmcm_reset));
  (* box_type = "PRIMITIVE" *) 
  BUFG rxrecclkbufg
       (.I(rxoutclk),
        .O(rxuserclk2_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_cpll_railing
   (cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gt0_cpllreset_t);
  output cpll_pd0_i;
  output cpllreset_in;
  input gtrefclk_bufg;
  input gt0_cpllreset_t;

  wire cpll_pd0_i;
  wire cpll_reset_out;
  wire \cpllpd_wait_reg[31]_srl32_n_1 ;
  wire \cpllpd_wait_reg[63]_srl32_n_1 ;
  wire \cpllpd_wait_reg[94]_srl31_n_0 ;
  wire cpllreset_in;
  wire \cpllreset_wait_reg[126]_srl31_n_0 ;
  wire \cpllreset_wait_reg[31]_srl32_n_1 ;
  wire \cpllreset_wait_reg[63]_srl32_n_1 ;
  wire \cpllreset_wait_reg[95]_srl32_n_1 ;
  wire gt0_cpllreset_t;
  wire gtrefclk_bufg;
  wire \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ;

  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h7FFFFFFF)) 
    \cpllpd_wait_reg[94]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[63]_srl32_n_1 ),
        .Q(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q31(\NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \cpllpd_wait_reg[95] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q(cpll_pd0_i),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[126]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[95]_srl32_n_1 ),
        .Q(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q31(\NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \cpllreset_wait_reg[127] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q(cpll_reset_out),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h000000FF)) 
    \cpllreset_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[95]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[63]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[95]_srl32_n_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gtxe2_i_i_1
       (.I0(cpll_reset_out),
        .I1(gt0_cpllreset_t),
        .O(cpllreset_in));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common
   (gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtrefclk_out,
    independent_clock_bufg,
    out);
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;
  input gtrefclk_out;
  input independent_clock_bufg;
  input [0:0]out;

  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_out;
  wire gtxe2_common_i_n_2;
  wire gtxe2_common_i_n_5;
  wire independent_clock_bufg;
  wire [0:0]out;
  wire NLW_gtxe2_common_i_DRPRDY_UNCONNECTED;
  wire NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED;
  wire [15:0]NLW_gtxe2_common_i_DRPDO_UNCONNECTED;
  wire [7:0]NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_COMMON #(
    .BIAS_CFG(64'h0000040000001000),
    .COMMON_CFG(32'h00000000),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_QPLLLOCKDETCLK_INVERTED(1'b0),
    .QPLL_CFG(27'h06801C1),
    .QPLL_CLKOUT_CFG(4'b0000),
    .QPLL_COARSE_FREQ_OVRD(6'b010000),
    .QPLL_COARSE_FREQ_OVRD_EN(1'b0),
    .QPLL_CP(10'b0000011111),
    .QPLL_CP_MONITOR_EN(1'b0),
    .QPLL_DMONITOR_SEL(1'b0),
    .QPLL_FBDIV(10'b0000100000),
    .QPLL_FBDIV_MONITOR_EN(1'b0),
    .QPLL_FBDIV_RATIO(1'b1),
    .QPLL_INIT_CFG(24'h000006),
    .QPLL_LOCK_CFG(16'h21E8),
    .QPLL_LPF(4'b1111),
    .QPLL_REFCLK_DIV(1),
    .SIM_QPLLREFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("FALSE"),
    .SIM_VERSION("4.0")) 
    gtxe2_common_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO(NLW_gtxe2_common_i_DRPDO_UNCONNECTED[15:0]),
        .DRPEN(1'b0),
        .DRPRDY(NLW_gtxe2_common_i_DRPRDY_UNCONNECTED),
        .DRPWE(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLDMONITOR(NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED[7:0]),
        .QPLLFBCLKLOST(NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED),
        .QPLLLOCK(gtxe2_common_i_n_2),
        .QPLLLOCKDETCLK(independent_clock_bufg),
        .QPLLLOCKEN(1'b1),
        .QPLLOUTCLK(gt0_qplloutclk_out),
        .QPLLOUTREFCLK(gt0_qplloutrefclk_out),
        .QPLLOUTRESET(1'b0),
        .QPLLPD(1'b1),
        .QPLLREFCLKLOST(gtxe2_common_i_n_5),
        .QPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .QPLLRESET(out),
        .QPLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLRSVD2({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR(NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync
   (reset_out,
    CLK,
    enablealign);
  output reset_out;
  input CLK;
  input enablealign;

  wire CLK;
  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1
   (reset_out,
    independent_clock_bufg,
    SR);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]SR;

  wire [0:0]SR;
  wire independent_clock_bufg;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(SR),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(SR),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(SR),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(SR),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(SR),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2
   (reset_out,
    independent_clock_bufg,
    reset_sync5_0);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]reset_sync5_0;

  wire independent_clock_bufg;
  wire reset_out;
  wire [0:0]reset_sync5_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer
   (reset,
    independent_clock_bufg,
    data_out);
  output reset;
  input independent_clock_bufg;
  input data_out;

  wire \counter_stg1[5]_i_1_n_0 ;
  wire \counter_stg1[5]_i_3_n_0 ;
  wire [5:5]counter_stg1_reg;
  wire [4:0]counter_stg1_reg__0;
  wire \counter_stg2[0]_i_3_n_0 ;
  wire [11:0]counter_stg2_reg;
  wire \counter_stg2_reg[0]_i_2_n_0 ;
  wire \counter_stg2_reg[0]_i_2_n_1 ;
  wire \counter_stg2_reg[0]_i_2_n_2 ;
  wire \counter_stg2_reg[0]_i_2_n_3 ;
  wire \counter_stg2_reg[0]_i_2_n_4 ;
  wire \counter_stg2_reg[0]_i_2_n_5 ;
  wire \counter_stg2_reg[0]_i_2_n_6 ;
  wire \counter_stg2_reg[0]_i_2_n_7 ;
  wire \counter_stg2_reg[4]_i_1_n_0 ;
  wire \counter_stg2_reg[4]_i_1_n_1 ;
  wire \counter_stg2_reg[4]_i_1_n_2 ;
  wire \counter_stg2_reg[4]_i_1_n_3 ;
  wire \counter_stg2_reg[4]_i_1_n_4 ;
  wire \counter_stg2_reg[4]_i_1_n_5 ;
  wire \counter_stg2_reg[4]_i_1_n_6 ;
  wire \counter_stg2_reg[4]_i_1_n_7 ;
  wire \counter_stg2_reg[8]_i_1_n_1 ;
  wire \counter_stg2_reg[8]_i_1_n_2 ;
  wire \counter_stg2_reg[8]_i_1_n_3 ;
  wire \counter_stg2_reg[8]_i_1_n_4 ;
  wire \counter_stg2_reg[8]_i_1_n_5 ;
  wire \counter_stg2_reg[8]_i_1_n_6 ;
  wire \counter_stg2_reg[8]_i_1_n_7 ;
  wire counter_stg30;
  wire \counter_stg3[0]_i_3_n_0 ;
  wire \counter_stg3[0]_i_4_n_0 ;
  wire \counter_stg3[0]_i_5_n_0 ;
  wire [11:0]counter_stg3_reg;
  wire \counter_stg3_reg[0]_i_2_n_0 ;
  wire \counter_stg3_reg[0]_i_2_n_1 ;
  wire \counter_stg3_reg[0]_i_2_n_2 ;
  wire \counter_stg3_reg[0]_i_2_n_3 ;
  wire \counter_stg3_reg[0]_i_2_n_4 ;
  wire \counter_stg3_reg[0]_i_2_n_5 ;
  wire \counter_stg3_reg[0]_i_2_n_6 ;
  wire \counter_stg3_reg[0]_i_2_n_7 ;
  wire \counter_stg3_reg[4]_i_1_n_0 ;
  wire \counter_stg3_reg[4]_i_1_n_1 ;
  wire \counter_stg3_reg[4]_i_1_n_2 ;
  wire \counter_stg3_reg[4]_i_1_n_3 ;
  wire \counter_stg3_reg[4]_i_1_n_4 ;
  wire \counter_stg3_reg[4]_i_1_n_5 ;
  wire \counter_stg3_reg[4]_i_1_n_6 ;
  wire \counter_stg3_reg[4]_i_1_n_7 ;
  wire \counter_stg3_reg[8]_i_1_n_1 ;
  wire \counter_stg3_reg[8]_i_1_n_2 ;
  wire \counter_stg3_reg[8]_i_1_n_3 ;
  wire \counter_stg3_reg[8]_i_1_n_4 ;
  wire \counter_stg3_reg[8]_i_1_n_5 ;
  wire \counter_stg3_reg[8]_i_1_n_6 ;
  wire \counter_stg3_reg[8]_i_1_n_7 ;
  wire data_out;
  wire eqOp;
  wire independent_clock_bufg;
  wire [5:0]plusOp;
  wire reset;
  wire reset0;
  wire reset_i_2_n_0;
  wire reset_i_3_n_0;
  wire reset_i_4_n_0;
  wire reset_i_5_n_0;
  wire reset_i_6_n_0;
  wire [3:3]\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg1[0]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counter_stg1[1]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .I1(counter_stg1_reg__0[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter_stg1[2]_i_1 
       (.I0(counter_stg1_reg__0[1]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter_stg1[3]_i_1 
       (.I0(counter_stg1_reg__0[2]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[1]),
        .I3(counter_stg1_reg__0[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter_stg1[4]_i_1 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(plusOp[4]));
  LUT5 #(
    .INIT(32'hFFFF2000)) 
    \counter_stg1[5]_i_1 
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .I4(data_out),
        .O(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter_stg1[5]_i_2 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \counter_stg1[5]_i_3 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(\counter_stg1[5]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(counter_stg1_reg__0[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(counter_stg1_reg__0[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(counter_stg1_reg__0[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(counter_stg1_reg__0[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(counter_stg1_reg__0[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[5]),
        .Q(counter_stg1_reg),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg2[0]_i_1 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(eqOp));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg2[0]_i_3 
       (.I0(counter_stg2_reg[0]),
        .O(\counter_stg2[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[0] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_7 ),
        .Q(counter_stg2_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg2_reg[0]_i_2_n_0 ,\counter_stg2_reg[0]_i_2_n_1 ,\counter_stg2_reg[0]_i_2_n_2 ,\counter_stg2_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg2_reg[0]_i_2_n_4 ,\counter_stg2_reg[0]_i_2_n_5 ,\counter_stg2_reg[0]_i_2_n_6 ,\counter_stg2_reg[0]_i_2_n_7 }),
        .S({counter_stg2_reg[3:1],\counter_stg2[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[10] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_5 ),
        .Q(counter_stg2_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[11] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_4 ),
        .Q(counter_stg2_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[1] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_6 ),
        .Q(counter_stg2_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[2] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_5 ),
        .Q(counter_stg2_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[3] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_4 ),
        .Q(counter_stg2_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[4] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_7 ),
        .Q(counter_stg2_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[4]_i_1 
       (.CI(\counter_stg2_reg[0]_i_2_n_0 ),
        .CO({\counter_stg2_reg[4]_i_1_n_0 ,\counter_stg2_reg[4]_i_1_n_1 ,\counter_stg2_reg[4]_i_1_n_2 ,\counter_stg2_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[4]_i_1_n_4 ,\counter_stg2_reg[4]_i_1_n_5 ,\counter_stg2_reg[4]_i_1_n_6 ,\counter_stg2_reg[4]_i_1_n_7 }),
        .S(counter_stg2_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[5] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_6 ),
        .Q(counter_stg2_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[6] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_5 ),
        .Q(counter_stg2_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[7] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_4 ),
        .Q(counter_stg2_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[8] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_7 ),
        .Q(counter_stg2_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[8]_i_1 
       (.CI(\counter_stg2_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg2_reg[8]_i_1_n_1 ,\counter_stg2_reg[8]_i_1_n_2 ,\counter_stg2_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[8]_i_1_n_4 ,\counter_stg2_reg[8]_i_1_n_5 ,\counter_stg2_reg[8]_i_1_n_6 ,\counter_stg2_reg[8]_i_1_n_7 }),
        .S(counter_stg2_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[9] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_6 ),
        .Q(counter_stg2_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \counter_stg3[0]_i_1 
       (.I0(\counter_stg3[0]_i_3_n_0 ),
        .I1(\counter_stg3[0]_i_4_n_0 ),
        .I2(counter_stg2_reg[0]),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .O(counter_stg30));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_3 
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(\counter_stg3[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_4 
       (.I0(counter_stg2_reg[9]),
        .I1(counter_stg2_reg[10]),
        .I2(counter_stg2_reg[7]),
        .I3(counter_stg2_reg[8]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(\counter_stg3[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg3[0]_i_5 
       (.I0(counter_stg3_reg[0]),
        .O(\counter_stg3[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[0] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_7 ),
        .Q(counter_stg3_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg3_reg[0]_i_2_n_0 ,\counter_stg3_reg[0]_i_2_n_1 ,\counter_stg3_reg[0]_i_2_n_2 ,\counter_stg3_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg3_reg[0]_i_2_n_4 ,\counter_stg3_reg[0]_i_2_n_5 ,\counter_stg3_reg[0]_i_2_n_6 ,\counter_stg3_reg[0]_i_2_n_7 }),
        .S({counter_stg3_reg[3:1],\counter_stg3[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[10] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_5 ),
        .Q(counter_stg3_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[11] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_4 ),
        .Q(counter_stg3_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[1] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_6 ),
        .Q(counter_stg3_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[2] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_5 ),
        .Q(counter_stg3_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[3] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_4 ),
        .Q(counter_stg3_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[4] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_7 ),
        .Q(counter_stg3_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[4]_i_1 
       (.CI(\counter_stg3_reg[0]_i_2_n_0 ),
        .CO({\counter_stg3_reg[4]_i_1_n_0 ,\counter_stg3_reg[4]_i_1_n_1 ,\counter_stg3_reg[4]_i_1_n_2 ,\counter_stg3_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[4]_i_1_n_4 ,\counter_stg3_reg[4]_i_1_n_5 ,\counter_stg3_reg[4]_i_1_n_6 ,\counter_stg3_reg[4]_i_1_n_7 }),
        .S(counter_stg3_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[5] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_6 ),
        .Q(counter_stg3_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[6] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_5 ),
        .Q(counter_stg3_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[7] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_4 ),
        .Q(counter_stg3_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[8] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_7 ),
        .Q(counter_stg3_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[8]_i_1 
       (.CI(\counter_stg3_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg3_reg[8]_i_1_n_1 ,\counter_stg3_reg[8]_i_1_n_2 ,\counter_stg3_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[8]_i_1_n_4 ,\counter_stg3_reg[8]_i_1_n_5 ,\counter_stg3_reg[8]_i_1_n_6 ,\counter_stg3_reg[8]_i_1_n_7 }),
        .S(counter_stg3_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[9] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_6 ),
        .Q(counter_stg3_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    reset_i_1
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .O(reset0));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    reset_i_2
       (.I0(counter_stg3_reg[9]),
        .I1(counter_stg3_reg[10]),
        .I2(counter_stg3_reg[7]),
        .I3(counter_stg3_reg[8]),
        .I4(counter_stg2_reg[0]),
        .I5(counter_stg3_reg[11]),
        .O(reset_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    reset_i_3
       (.I0(reset_i_4_n_0),
        .I1(reset_i_5_n_0),
        .I2(reset_i_6_n_0),
        .O(reset_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    reset_i_4
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(reset_i_4_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    reset_i_5
       (.I0(counter_stg2_reg[10]),
        .I1(counter_stg2_reg[9]),
        .I2(counter_stg2_reg[8]),
        .I3(counter_stg2_reg[7]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(reset_i_5_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    reset_i_6
       (.I0(counter_stg3_reg[4]),
        .I1(counter_stg3_reg[3]),
        .I2(counter_stg3_reg[1]),
        .I3(counter_stg3_reg[2]),
        .I4(counter_stg3_reg[6]),
        .I5(counter_stg3_reg[5]),
        .O(reset_i_6_n_0));
  FDRE reset_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset0),
        .Q(reset),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets
   (out,
    independent_clock_bufg,
    reset);
  output [0:0]out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign out[0] = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire mmcm_reset;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_clocking core_clocking_i
       (.gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .mmcm_locked(mmcm_locked_out),
        .mmcm_reset(mmcm_reset),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_gt_common core_gt_common_i
       (.gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out),
        .reset(reset));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_block pcs_pma_block_i
       (.CLK(userclk2_out),
        .configuration_vector(configuration_vector[3:1]),
        .data_in(mmcm_locked_out),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(userclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block
   (data_out,
    data_in,
    CLK);
  output data_out;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_0
   (resetdone,
    resetdone_0,
    data_in,
    CLK);
  output resetdone;
  input resetdone_0;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire resetdone_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    resetdone_INST_0
       (.I0(data_out),
        .I1(resetdone_0),
        .O(resetdone));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_10
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_11
   (\FSM_sequential_rx_state_reg[1] ,
    Q,
    rxresetdone_s3,
    data_sync_reg1_0,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  input [2:0]Q;
  input rxresetdone_s3;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire \FSM_sequential_rx_state_reg[1] ;
  wire [2:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire rxresetdone_s3;

  LUT5 #(
    .INIT(32'h008F0080)) 
    \FSM_sequential_rx_state[3]_i_5 
       (.I0(Q[0]),
        .I1(rxresetdone_s3),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(cplllock_sync),
        .O(\FSM_sequential_rx_state_reg[1] ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_12
   (\FSM_sequential_rx_state_reg[1] ,
    rx_fsm_reset_done_int_reg,
    D,
    E,
    reset_time_out_reg,
    reset_time_out_reg_0,
    Q,
    reset_time_out_reg_1,
    reset_time_out_reg_2,
    data_in,
    \FSM_sequential_rx_state_reg[1]_0 ,
    rx_fsm_reset_done_int_reg_0,
    rx_fsm_reset_done_int_reg_1,
    \FSM_sequential_rx_state_reg[0] ,
    \FSM_sequential_rx_state_reg[0]_0 ,
    \FSM_sequential_rx_state_reg[0]_1 ,
    mmcm_lock_reclocked,
    \FSM_sequential_rx_state_reg[0]_2 ,
    time_out_wait_bypass_s3,
    \FSM_sequential_rx_state_reg[3] ,
    \FSM_sequential_rx_state_reg[0]_3 ,
    rx_fsm_reset_done_int_reg_2,
    rx_fsm_reset_done_int_reg_3,
    data_out,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  output rx_fsm_reset_done_int_reg;
  output [2:0]D;
  output [0:0]E;
  input reset_time_out_reg;
  input reset_time_out_reg_0;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input reset_time_out_reg_2;
  input data_in;
  input \FSM_sequential_rx_state_reg[1]_0 ;
  input rx_fsm_reset_done_int_reg_0;
  input rx_fsm_reset_done_int_reg_1;
  input \FSM_sequential_rx_state_reg[0] ;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input \FSM_sequential_rx_state_reg[0]_1 ;
  input mmcm_lock_reclocked;
  input \FSM_sequential_rx_state_reg[0]_2 ;
  input time_out_wait_bypass_s3;
  input \FSM_sequential_rx_state_reg[3] ;
  input \FSM_sequential_rx_state_reg[0]_3 ;
  input rx_fsm_reset_done_int_reg_2;
  input rx_fsm_reset_done_int_reg_3;
  input data_out;
  input independent_clock_bufg;

  wire [2:0]D;
  wire [0:0]E;
  wire \FSM_sequential_rx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_8_n_0 ;
  wire \FSM_sequential_rx_state_reg[0] ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire \FSM_sequential_rx_state_reg[0]_1 ;
  wire \FSM_sequential_rx_state_reg[0]_2 ;
  wire \FSM_sequential_rx_state_reg[0]_3 ;
  wire \FSM_sequential_rx_state_reg[1] ;
  wire \FSM_sequential_rx_state_reg[1]_0 ;
  wire \FSM_sequential_rx_state_reg[3] ;
  wire [3:0]Q;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_valid_sync;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out_i_2_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;
  wire reset_time_out_reg_2;
  wire rx_fsm_reset_done_int;
  wire rx_fsm_reset_done_int_i_3_n_0;
  wire rx_fsm_reset_done_int_i_4_n_0;
  wire rx_fsm_reset_done_int_reg;
  wire rx_fsm_reset_done_int_reg_0;
  wire rx_fsm_reset_done_int_reg_1;
  wire rx_fsm_reset_done_int_reg_2;
  wire rx_fsm_reset_done_int_reg_3;
  wire time_out_wait_bypass_s3;

  LUT5 #(
    .INIT(32'hFFEFEFEF)) 
    \FSM_sequential_rx_state[0]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0]_2 ),
        .I1(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    \FSM_sequential_rx_state[0]_i_3 
       (.I0(Q[3]),
        .I1(reset_time_out_reg_2),
        .I2(data_valid_sync),
        .I3(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF24200400)) 
    \FSM_sequential_rx_state[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\FSM_sequential_rx_state[1]_i_2_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[1]_0 ),
        .O(D[1]));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[1]_i_2 
       (.I0(data_valid_sync),
        .I1(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \FSM_sequential_rx_state[3]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0] ),
        .I1(\FSM_sequential_rx_state[3]_i_4_n_0 ),
        .I2(Q[0]),
        .I3(reset_time_out_reg),
        .I4(\FSM_sequential_rx_state[3]_i_6_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[0]_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFFFFFFCCC0C4C4)) 
    \FSM_sequential_rx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\FSM_sequential_rx_state[3]_i_8_n_0 ),
        .I4(Q[0]),
        .I5(\FSM_sequential_rx_state_reg[3] ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAEFEA)) 
    \FSM_sequential_rx_state[3]_i_4 
       (.I0(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I1(\FSM_sequential_rx_state_reg[0]_1 ),
        .I2(Q[2]),
        .I3(\FSM_sequential_rx_state_reg[0]_3 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\FSM_sequential_rx_state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h0CE20CCC)) 
    \FSM_sequential_rx_state[3]_i_6 
       (.I0(mmcm_lock_reclocked),
        .I1(Q[3]),
        .I2(data_valid_sync),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\FSM_sequential_rx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_rx_state[3]_i_8 
       (.I0(rx_fsm_reset_done_int_reg_1),
        .I1(data_valid_sync),
        .I2(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state[3]_i_8_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_valid_sync),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEEEFFFFFEEEF0000)) 
    reset_time_out_i_1__0
       (.I0(reset_time_out_i_2_n_0),
        .I1(reset_time_out_reg),
        .I2(reset_time_out_reg_0),
        .I3(Q[1]),
        .I4(reset_time_out_reg_1),
        .I5(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state_reg[1] ));
  LUT6 #(
    .INIT(64'h0FF30E0E0FF30202)) 
    reset_time_out_i_2
       (.I0(\FSM_sequential_rx_state_reg[0]_1 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(data_valid_sync),
        .I4(Q[3]),
        .I5(mmcm_lock_reclocked),
        .O(reset_time_out_i_2_n_0));
  LUT4 #(
    .INIT(16'hABA8)) 
    rx_fsm_reset_done_int_i_1
       (.I0(rx_fsm_reset_done_int),
        .I1(rx_fsm_reset_done_int_i_3_n_0),
        .I2(rx_fsm_reset_done_int_i_4_n_0),
        .I3(data_in),
        .O(rx_fsm_reset_done_int_reg));
  LUT5 #(
    .INIT(32'h00040000)) 
    rx_fsm_reset_done_int_i_2
       (.I0(Q[0]),
        .I1(data_valid_sync),
        .I2(Q[2]),
        .I3(reset_time_out_reg_2),
        .I4(rx_fsm_reset_done_int_reg_2),
        .O(rx_fsm_reset_done_int));
  LUT6 #(
    .INIT(64'h0400040004040400)) 
    rx_fsm_reset_done_int_i_3
       (.I0(rx_fsm_reset_done_int_reg_0),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_1),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_3_n_0));
  LUT6 #(
    .INIT(64'h0008000808080008)) 
    rx_fsm_reset_done_int_i_4
       (.I0(rx_fsm_reset_done_int_reg_3),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_2),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_13
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1__0 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_14
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_15
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_16
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3
   (data_out,
    status_vector,
    independent_clock_bufg);
  output data_out;
  input [0:0]status_vector;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;
  wire [0:0]status_vector;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(status_vector),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_4
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_5
   (reset_time_out_reg,
    E,
    reset_time_out_reg_0,
    reset_time_out,
    \FSM_sequential_tx_state_reg[0] ,
    \FSM_sequential_tx_state_reg[0]_0 ,
    \FSM_sequential_tx_state_reg[0]_1 ,
    \FSM_sequential_tx_state_reg[0]_2 ,
    \FSM_sequential_tx_state_reg[0]_3 ,
    Q,
    reset_time_out_reg_1,
    mmcm_lock_reclocked,
    \FSM_sequential_tx_state_reg[0]_4 ,
    \FSM_sequential_tx_state_reg[0]_5 ,
    \FSM_sequential_tx_state_reg[0]_6 ,
    data_sync_reg1_0,
    independent_clock_bufg);
  output reset_time_out_reg;
  output [0:0]E;
  input reset_time_out_reg_0;
  input reset_time_out;
  input \FSM_sequential_tx_state_reg[0] ;
  input \FSM_sequential_tx_state_reg[0]_0 ;
  input \FSM_sequential_tx_state_reg[0]_1 ;
  input \FSM_sequential_tx_state_reg[0]_2 ;
  input \FSM_sequential_tx_state_reg[0]_3 ;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input mmcm_lock_reclocked;
  input \FSM_sequential_tx_state_reg[0]_4 ;
  input \FSM_sequential_tx_state_reg[0]_5 ;
  input \FSM_sequential_tx_state_reg[0]_6 ;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]E;
  wire \FSM_sequential_tx_state[3]_i_5_n_0 ;
  wire \FSM_sequential_tx_state_reg[0] ;
  wire \FSM_sequential_tx_state_reg[0]_0 ;
  wire \FSM_sequential_tx_state_reg[0]_1 ;
  wire \FSM_sequential_tx_state_reg[0]_2 ;
  wire \FSM_sequential_tx_state_reg[0]_3 ;
  wire \FSM_sequential_tx_state_reg[0]_4 ;
  wire \FSM_sequential_tx_state_reg[0]_5 ;
  wire \FSM_sequential_tx_state_reg[0]_6 ;
  wire [3:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out;
  wire reset_time_out_i_3__0_n_0;
  wire reset_time_out_i_4__0_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;

  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    \FSM_sequential_tx_state[3]_i_1 
       (.I0(\FSM_sequential_tx_state_reg[0] ),
        .I1(\FSM_sequential_tx_state_reg[0]_0 ),
        .I2(\FSM_sequential_tx_state[3]_i_5_n_0 ),
        .I3(\FSM_sequential_tx_state_reg[0]_1 ),
        .I4(\FSM_sequential_tx_state_reg[0]_2 ),
        .I5(\FSM_sequential_tx_state_reg[0]_3 ),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000F00008)) 
    \FSM_sequential_tx_state[3]_i_5 
       (.I0(\FSM_sequential_tx_state_reg[0]_4 ),
        .I1(\FSM_sequential_tx_state_reg[0]_5 ),
        .I2(cplllock_sync),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_tx_state_reg[0]_6 ),
        .O(\FSM_sequential_tx_state[3]_i_5_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hEFE0)) 
    reset_time_out_i_1
       (.I0(reset_time_out_reg_0),
        .I1(reset_time_out_i_3__0_n_0),
        .I2(reset_time_out_i_4__0_n_0),
        .I3(reset_time_out),
        .O(reset_time_out_reg));
  LUT6 #(
    .INIT(64'h020002000F000200)) 
    reset_time_out_i_3__0
       (.I0(cplllock_sync),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(mmcm_lock_reclocked),
        .I5(Q[1]),
        .O(reset_time_out_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0505FF040505F504)) 
    reset_time_out_i_4__0
       (.I0(Q[1]),
        .I1(reset_time_out_reg_1),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(cplllock_sync),
        .O(reset_time_out_i_4__0_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_6
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_7
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_8
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_9
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_transceiver
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    Q,
    \rxdata_reg[7]_0 ,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i,
    SR,
    CLK,
    powerdown,
    reset_sync5,
    D,
    txchardispval_reg_reg_0,
    txcharisk_reg_reg_0,
    out,
    status_vector,
    enablealign,
    data_sync_reg1,
    \txdata_reg_reg[7]_0 );
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;
  input [0:0]SR;
  input CLK;
  input powerdown;
  input [0:0]reset_sync5;
  input [0:0]D;
  input [0:0]txchardispval_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input [0:0]out;
  input [0:0]status_vector;
  input enablealign;
  input data_sync_reg1;
  input [7:0]\txdata_reg_reg[7]_0 ;

  wire CLK;
  wire [0:0]D;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire data_sync_reg1;
  wire data_valid_reg2;
  wire enablealign;
  wire encommaalign_int;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_inst_n_6;
  wire gtwizard_inst_n_7;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire p_0_in;
  wire powerdown;
  wire reset;
  wire [0:0]reset_sync5;
  wire rx_fsm_reset_done_int_reg;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_int;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_int;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_int;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_int;
  wire [1:0]rxnotintable_reg__0;
  wire rxoutclk;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire rxreset_int;
  wire [0:0]status_vector;
  wire toggle;
  wire toggle_i_1_n_0;
  wire toggle_rx;
  wire toggle_rx_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [0:0]txchardispval_reg_reg_0;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire txreset_int;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_GTWIZARD gtwizard_inst
       (.D(rxclkcorcnt_int),
        .Q(txdata_int),
        .RXBUFSTATUS(gtwizard_inst_n_7),
        .RXPD(rxpowerdown),
        .TXBUFSTATUS(gtwizard_inst_n_6),
        .TXPD(txpowerdown),
        .data_in(data_in),
        .data_out(data_valid_reg2),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(rxdata_int),
        .gtxe2_i_0(rxchariscomma_int),
        .gtxe2_i_1(rxcharisk_int),
        .gtxe2_i_2(rxdisperr_int),
        .gtxe2_i_3(rxnotintable_int),
        .gtxe2_i_4(gtxe2_i),
        .gtxe2_i_5(txchardispmode_int),
        .gtxe2_i_6(txchardispval_int),
        .gtxe2_i_7(txcharisk_int),
        .gtxe2_i_8(rxreset_int),
        .gtxe2_i_9(txreset_int),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(encommaalign_int),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync reclock_encommaalign
       (.CLK(CLK),
        .enablealign(enablealign),
        .reset_out(encommaalign_int));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_1 reclock_rxreset
       (.SR(SR),
        .independent_clock_bufg(independent_clock_bufg),
        .reset_out(rxreset_int));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_sync_2 reclock_txreset
       (.independent_clock_bufg(independent_clock_bufg),
        .reset_out(txreset_int),
        .reset_sync5_0(reset_sync5));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_reset_wtd_timer reset_wtd_timer
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset));
  FDRE rxbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_7),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle_rx),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle_rx),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle_rx),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle_rx),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle_rx),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle_rx),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle_rx),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle_rx),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle_rx),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle_rx),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle_rx),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle_rx),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_gig_ethernet_pcs_pma_0_sync_block_3 sync_block_data_valid
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .status_vector(status_vector));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE toggle_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(reset_sync5));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_rx_i_1
       (.I0(toggle_rx),
        .O(toggle_rx_i_1_n_0));
  FDRE toggle_rx_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_rx_i_1_n_0),
        .Q(toggle_rx),
        .R(SR));
  FDRE txbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_6),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(reset_sync5));
  FDRE \txchardispmode_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispmode_double[1]),
        .R(reset_sync5));
  FDRE \txchardispmode_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(D),
        .Q(txchardispmode_reg),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_double[1]),
        .R(reset_sync5));
  FDRE \txchardispval_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_reg),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(reset_sync5));
  FDRE \txcharisk_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(reset_sync5));
  FDRE \txdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(reset_sync5));
  FDRE \txdata_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(reset_sync5));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
afmoZr9skqIZsiBY7liE8HhN5PT9cSTlE7b7FkfYEnmAz8/sYewHAx0YfvnPtVZDeaUQ1+SA/YSK
81BVyxiW6Q1+w7v5E1h+YJTA02rq79x+8OQbPa0mLIjdXTtNmx+BnF2Qrq3QGTPNoSFginwNNM6R
DHZuBkJnITzBkDYC/to=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
oFhR7iEHB5cYBaPdDcFYa+fK3Gj/00Z1GiQ4ZVkb1MGu+Q6dRi7Lgvhbwd39WT10uvi5m95gOjlX
gGa0fqW2XuRf5YGzXsxqVh0xVmshpf7o2GLRr2CmqpGBtEUVv4LbXJ4WiaEo9wVMVr2rBg6MtuPB
CQBdJcTka6nWZivdNblYGd53J9DObofIqJ9W/NUzFP/DmzVmGbctoa4amp6KUecoKxnO4tESoQzm
lz7s+Zslv3tCv5wVOwV3SAR+Y5ul9hQX8OljyF82Z31ipd+bzojhPg9RhmrvIMbFqzM9eU82pEIT
IZzSVKYMd1P6t1qORfwdgodKT0ZApoyFVxfVeQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
FJ5kCtzgKdagetGF6bdHAPKIu2cq7BcnQFRTWMN8j6rUq4ZaiydIPtRo6ybDhCzgm0ZRPth8Lgd5
+HXk3g/SdsTLZoOUuXnEz+TkfRJdu8kdJ+0y8sJMNK21IEW3pSWsj/n4toksSjLJ1wENuoq4XqDK
9VQo0Gobmx7OOqOYFuY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
d/VWmC3TdLGZzbtN4vTxv6kYGkcDx9TNbvTz1ALcTDtGFLEUmm0Z0/puE1NDYgBTJquGDZ+j8d2H
/il9HfJ/2n/hTxPKUXm83KvLOZ3eaRdjaEk1wHy+UsjHJ1VztyOuT/nrMaT3sUq1kpP7wkC5I2EK
lQZbp4ngkuiSjWCad10Yoj0TX6DfE58qXjaybvWsINo7H6gMzi4JvYHWbBnCYQS/RkvaQvSsne0i
YvfJuYy63HVFs+Afh6hOO34nbqzXvlsXI7SIicsCUu21AgLUNio4IsL+Uj92NEPjaJnw2wTXrMuH
A1a3i03K7vrZ94AIVwtSXRwfN4q33BWP9bjYdQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
3xYA7hcw+q7P0QSxtus9heaAij7kiNzMgPuCXFQTIhWPWN0atyWMkDqoxytTvKyCd0IM0n0DheIN
XngnbZmTQpitsZh5f/e8iD/+iWxl+7S7I6wyIivHhUA/yP87+KG/vFGPXu7WexVyPmZuOgSS7sS/
Fnk+QS1l3qltq84DH59g9EBTV7YkcgLEdICkCR0tgP3K8aSVjCptgXzA0onYGrkQESEdw8nPfEvR
g/5lKhHOmmE6kTh/XfO6eQpuQBKOlD7RaVWrPxnTkM9lxrjRZZb3OSfaBLMKhMGrOdEY4RIqfHMG
MBOH8gAUoIRrgyLztGTWc+RsxUUYHq9Hwixqkg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RyOZOz+qJ5WH/47JnOQuMFPCUAyXetmhoeJiNflULv2xv5JJeXWSOURz6kOeJ5/Jbz2KA/psbOVw
rwZKAlTJoEVMq47lcrgzkIZPdllrnwbNl8hiziEb/YEq2vvpC9EJzY7LlRbQINiQY3hAzP5B/s+N
lYRpAUjOhEst2xZEiFz6cr7zAMz2jn7lHWYuvu75NGUv8JJ7PZ6vUISUeHOPW9ljH0TH4CnOaTpM
Mey1hosrMsp78mwEmy3l4n56Khrq2FOUzGZbi5lhqEebDEU/90LVUpUSzOkhJ03OfvAgBuj9ejqz
WgZRD6DpCk8BVUByKAGJqQZsJ0NcEnE8/btwhQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Q98gl70eQ6J1AYIClxRNKl48Tz7BoxVypGMCmvUy8HqLl3jjFsIYy9amcj+/0Qts3PcCHOutO4e+
ptR4cjDneD+41DiKHcGc2sgfdKq4hG60mda13C9qtjKCuBhg+Zg1jHi3TOh4bVtsLuydPMwaILNj
7ORZo/jzBQ4OUPNFy2tkOMJArdeTOIuydxhUFtpy6FAABlc1SE1Mer24GTLYw0pJtOOJOX61uJuc
ghuBuHaXV8kExCP3IU1qx/oyfIpY9TQiqLKEf0YUrmEzBzV+JXQqn3Jom9PXTay5YJ+PqraK9iFt
ai1YIDay5F0ncLDXiT7OMRFjFG2mL+C96DFEmg==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
DgXXbyNB3XMOH/gmmWb6uBYjn7XQuHgaTP3eav9Y+qTkta/ItNcuqONFgEZl6duKUkQI/YVDFLf5
JwZHmI7U9IACSpdRAsITpVXnabTJhDmcKIxi5anNhIeSW9RerBcxlMYtuoI5FgWJKz6h+0yZmes6
xYMyXTT3gJS5ttPc1n6kBHNZpxPgl/Yv979A0/ZxYkRTfuWrCyaS5T08AajGwOyC+2sH4FjL2xLX
uR9a04wQespOimJ+InC0FAfYhqUcjs5b18x0hSfPhe4G0xKNvmpukNOhIflD4bBgKXl0kqXnpatt
7ejKBpSWKRjk/M7rYOxErj8EahuRFH07pBR9b3XyXF3MfQBwAzV2GIz+A/XzIUDQOcBbojfGwrVA
2HhHfn8sKZB5ktpwF8MBjdlwrtn+kdQ8ux0ZNrgndrvw4nS4TWxZtQeRc+wfb7Q/lIzw6Pc+Ifnn
b7UJlJvNr5ZF2XNxuHGz5L19UYgB0gpH8xWvYpn0Od797z3FfYIiGMNP

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
YW19UgddTwwnJVcciGeEKvH9ZuBWuoo4LgtOhQvaoLIGVBAQAVfk5VY0pzvluZUn2+81fwbTPSLA
O+/RYs39A761jlViEBPYfmojlgjyiOTK9aHXNLsD3WBxfTXGOS3fHhhwgCmiZ0OncriPRHcNAK7N
4cyj3anKb5y4g9gzt9YeJ7T/zkLQuzT4zqnb9ijTisbH6HxpAVXI1ydKtGzVETw+s55Jb0gVXyGi
Q1hVe4oSDBTETiD85J2/A1KF+2DHKkzOJhr53qxvjND2l/LqsA0G/4J35IdLcmuYWAoCJS7cHkTk
wnGOjEnNbP+gboLu5UrXAFwXCo+jsZLVqIzGqg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
58apVjNnALRGpDI2bjJhSWJPQlj57Kat42daGg5JiM8yXDkjxm/BmzBRp9oBmOurwCnZOZcKlj3m
HlexbUqO32l40m0mMLkLzlGYPsduAUijMIb7DMl5HUf9XunE4fC69CyB0KyecsJZI9Ru4XxZrniR
CYqqjLtNjfFEppiRGRN6bXqv2T5Vgbh9qrMDIrkZnrIUB7RBd6xTPZ0xU1V6/f7Oftp0kyRv3C0z
HF7RFTnZ4y90YOsvuCM5CoAyyE/0gTXvBlB4icz61XmHsIlwfUu/xC6yAitX3dikrJ3DWeMC5Soe
vAifs/1pf/61AlzL055eu8EpDBQRH32ikamDmA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GkhzmJz9o93IJx+1EnrQfOwa/OOrgHQT2+XhwLsfJNRk9bxhLz472g/aTSOruRXBYVSa7twJJ6+r
lf0nvm4VQfQ9bG+fbYjOsryrSSKlth3/Bq54UcBz0wLrrf59Jt2KtKCOUQojHAIinG8CLxMBgQJR
wqwzAYtmffGsCYbobk0Y4IPkjQhtQamQd1BTI8ZwvF5zPN8kBYuoFLMFqwet6s+kcH/tQSrB0o/K
lHILn0b8uovehAHNWllP4phB4MeAOgMZbbsniJgqPQEhSd4nOUZs4WnWtYOsf+rCLliHTc9c6HIS
0u5f7mdvO+uC8Pi/nwNFr026nnYUX3YcG4zdBQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 153808)
`pragma protect data_block
QvU5mc84h7gRfAQWxIqbVo7VqxGrR7g9gAR/OJKWBYYv9XHHZlU6Fl1P1EEY0F25a2fKu76j9eHr
XRUATBV6YakHhjPfyo2ljO8zmBGcNIh2tvh95LVUtAqZKgtaD9lZj8jvECNBh4CQ661Plo8TUUCp
MU3lu8Lhekhk1ynASR4ifAiRSB2EEOQ/AcbbFe8LwJLExiJYjtqhKrtg/xYfP+Ri89cHffKG37yL
NYv8N/lr6uY0KS5UMBwuhv2gZW9fX4paV8Ng/Comx2xJcj1istdQp1OCX3ybiuzMbP66sIjs4qqe
GwaBRXqUVQhMFeSrpcNKEjaMBVw3Nq46g9n2FLIDAi/UUUM55/nztWJdB9L8xbu07kHYyLocxUX2
T8mpywgxGwcs+AA7jOSNMrLvW6L0hBvhVKgs/Iwa4vZ3Kd0mk1XHN8Q7V5W05BVMY90sDsKfP/vM
0pbi5hF633gdlpsk5QC0ZHKFZQNFeLJih/9ag/P2q0t3fUgQm1HFTnAjqwS59lkmC4+9HxiLGWJl
UxmWoVXU/Ehv8DGefoCDeJnwHh8bCkx5BfKqa8c8t35qZtE+4SBBZlpCO06Y+5eKantadmKAhY9S
zUHASEb+Xp/kQ//lQ5dviAdqMX9xjDN9FMcIgc/UxIqcDnt0ob0lLAfwMHDfGlSwFYd9PpXkkv7K
aTcOjEHfZ3UIHcbdOak+VcRscN7MMfH+bXxx3F8AmcvC+hjQlDiqOtQpL1mnalGa++TZHUpaRRtb
PzpbQrSt3CcF0M1t9pfy3KqAc1/RO5sVvTeRANUTkFCPt2TlfyucR1zWhbvZ3XReuCcGGPrBx/W5
TM+dLMtunNVFXDqMIXhmnFSlo7wIx31Ixeh6gyNaIwllDceVW7F+5rqIeZAKm7v+H/wpClaywLST
ID8fKADj36So3N2GeIp9MzPduqjVXWmkEah8EN0Jw8PN5+Eo5cYTPYgugUhnXmmPE95Z0Z23o+Z4
OK0lrY09ClBRzV9rupfgbWXZ1QrfDUaNBNdWDmtk6bLgQUjhZ1cvyu1sSPu7q7Ba1GveNKYVsn1B
HrdyujmdlGhSHSbpeNCEhoiVna8O+eNN4GEcu63IRGz9GYoXaiT3UdJKtSk4cjbkpffoDgKRtQek
eoy2I/iLVEJU2G/2e6qp0Yo8YDoFLnWohevAEeTA373qGqT0mo1qhvI2w9bnyIfh3Sz9dRGxTg87
HPOtLCNujd4sTL4iXJ2vX10qn7KK4rkXXhTdh0zfUUO0dHoxkewRcnROtOJHfW8DQ7bQnA0NTE64
IrNElqy4Mx7A06BUrjX11CGgoOFzmJeTHJLfJJiBY0TZQof+yXiyUN95w6eIbfoRY+XnuulKrLPm
YdV/pHJM1XpVMbSD+beSEi4HK1JEPimfan88UNG/LnKdLv3f6Y46qNVrs1b6mbTAxMsR8u6rmvnx
E+/bDQGlXMg00nr8/JwVSc4dGLHfGYxg0hOAfay3yBfHOLeozh/8QKMdYwxI2Y2aC5mlARMxK1hT
ZEvyn7BZw2K1JJnvvpERtyQcJFENW5z3eeneNmnpFPdZ7oeb9YgUAOd86mlZ7Ljq3RVRYJq3Kjyv
lzGl0NErRWebr2bpbg1fEPkkHOIJNtzW0kd+i4wHLFue38bSN3hV8LR4c+bw9M3X44AihQyzTNcv
iFll112mAeE/sxxjj+J7E9vaAlgSPWhnlbkRRN+YVqMrsTIz8SFlhz+BoTzjor4ltwjfxdBCHT37
zwXb/GtawdDKlBCDexSDW+fMIOzTxiFi66qSfEa34L1RbQekEQAH8FxNHuUN7e1rNSTtU8bzK+JI
dYgZTFGlJQ5s6ie1HBib/2SlT4cUSbtKZTwuj6YInwwI9qSxU1Qc4M5Ktw1Ap6aMLTr+JLojluvR
JT3ntbHvIAsApNGY+jSnjd7GEZlxfschzIAeqKSIWaW8WQlZ/q/KZ6Il2VgU+6n28/wB3M+TEVLm
VfdlDZighbYB1/9qrNE4TqFDObOdl7R3UzBVASQCpsu5vPahR6qfaa4o8g81Twmf8quLC0SJM4sW
UU2lPMOlqnAJBP0g+YzVbopsY1PzeEQCTYg9CHoDe1tk1/pTB3dfbGOlmFgTQNowp5QGd4IQ7Y3y
MsX1XhSrEnffbUyfRp4h1dLl399xBJy2Inq/N2aePKiR7EsXpQfHRmxbGgOpMjuOu6hy0w9Wvt4X
4zqQ4RZe955NVaJuBKcXfYM6wZJAjyKAIpOp2jujUJSH8FzBQCD+KcDmeRXfCbYi/2s6vgvU8WUn
cODYnxxwFYS7JnK8czfHdCPPjboGSANQBVtmmsBOUOvFXSXB91gGWYabP3jK2WyBxkmgUgBEM802
BXq8J5L/FciDGnwd4lemoosmcCRe3faZzj5YFyH9ZYclx0jkfoIik48AhxH405c4jYJuaOXmVmM0
DCfTbO5etdnWqeJ8l98iYxyvacKIfC99PbEe8vGLt4lUvrmrEuGq2n2IAq9oUfdyIwU9DWmGXTJq
aYKKLdESpqOYEeiAMCXnK6wj0kjdHDG+tVHWB6jBk48+63aZibDPdXOAusGS58Ge6iHupqt7HR4/
/wzOmDLOjfdHIEIA8AbkMKbk9OsJIB/mrqENUBwQPJA0UcPrgzwAAZhWd9JWp7gyKYMLubyT5L/2
9aT0eXeq4WGtmujpR/5Xan5ah+/yBmoGXuQbGjEYwxjQaioXelz3enuVb2rYjvmd1t/XPihWzTy/
E8whcGO9kp6Wv2lYDPbUNdkzSm24HA5nhSvgNAIvjIMQ7GccWAnnjufDhtIzgIVxgsxTGCzz9S7q
JuhudkF8mpdXZXK8MS2o5olnxDg0v7hbq6/SJx4KksUTCwghMknZ8vHtetHwPS83tYbuOKOIgAbO
xdRq1qK8yoq6SN7MhGT9curKduzPt0eQu46fU7BMTUbo6M7+rNswTTuMNHo82U7oiMw7wkqh7ucX
Bet6nkxifdInDl/lw/EBdFDWExizpGqY5kndnCDIm2I9Fc1YenT4HtpNG8mmzi9UK3FkjSYYmgEj
L0sshkcz46d5/+69xtQYe9MOyRTKPoSEiX07up6so69c07ld9Ac6zbwmDOdl64Kgy9SxWLRepM0+
uyhc6e+eD8TfgR6pV/TzCVbWAotiL9+QmDSX/FIkgUOY5PujBDlVIGq/LLYnN9whiStRGhiN0DlV
xIPtp3CTW/xPfnygrra9NNRI+AMS00jHm4tGlzYUiSKV5N3K2DgZZEQMbkcE9Ldac/bn8quOAncy
DHewaXXLBRD88wXeJ24mS28yHYOVpoisJKOJ6/+OrUSbZHc6kKICFis6esHMfGSIhCnpi3gO7Y5n
HtdSWMh81wswgvgu/mweQMOvuGc6juNIb8k5Xb2OrkwFJukGY3I+y8u6KXUadpqTfTJG6ZsxmHzi
9LFYD2Qxk9BscoNoOtBYZPds/9qRSJXG72zMBj9MLevV1wyk7es3WVQ3yAN2tJ9aP0sq8bI/AJZG
BkcB+Axbmcf0AHtfjHq3YnDtsz40k+MDppzoQyI7gKXkbyB2Jh7PLXu0kKFlqIJxo+hYojnPdPRX
OU0A4tEC7V9tqi3abOlFmuREkzyB8HxSWHWM8iNwVsdhYIrORece8TljYSty2Kf6jxjyjSqJ35e5
5eo81i3HoIiKXjw6zxCq0nCQ9fcHhiLdIjL7/Had1zveQ/kYDO3Aac+p0U0cwq0KDxnDotXVR6no
BZlr/AX77IFzmQuroL5LjZE6UbI12TR0CAktXp4CB5SSN6emK9SL4/6Kx6YRlC3cSlQNEM9KSi3t
mOf07hqemf4GXv/yev2OAy3sZa2pJghRZhT2N6SGj6tOx7sDu7U7sOh9o8Ujbi0fu57zhiH44mnD
gQ3Ts9HhsgoRNserK0dJdyQnaE8zfPjQHkK26uHE/YVn5WbhvIZpEM80kGi9Tv/OtAhTxaUk0tor
t4syWmdFSOsCBOAaK2AGY6knMR10j63xbH/G1mEYDVVaqNgDj1XugeVz2Sort6xMTqAO9GDX/tS7
NjEUS350hsrDGinbuqHVptRp7kgkffAalsRw3A+SFyAU9dmvPdRqSrMQJo9X/8YrPfNSKSEhBR+t
uubovDXYc3W+dez91DjFZFp4Lop6ivLUFkY+8BTxzvGEbXAJPp3U6gM6/hf3Ts38cG8/xXmEB3I7
FvHAH1aBLlxAT1IGOeYmYpBOzlLni7DNLj+U7nJfGRrZhfW4dlS8wjJp19yJ42ag/ZuSwmHi8MED
cCSC7h6TI8ojs1BhZ9TJ7oqEPDo/i2626qYKRBa0KTVkPGFhTjDAOqMfaH0gQ1GLZQd0cYaou0d8
uvgp7osiBq3hhovdMsMkHMNEn2Rm6CodHN/FTsDEfq2nyu7vOYvj+Mhgi+SjPQ/+6ktQ0vLTn0Og
IoXnHGJegLICTKbEVwU6DKdhCcmEOfVzn/bK37sDGkpSbCiGpsztD9/fpjCfnriXtkfzQlGhUJmJ
xwirqMREmCCti9iXx3wJlBOB8LZfAq1ABi5W379641GtzYd0Aq/lnLKjyPMDHFrqF7RJ6zHMQ8Le
wC7ot7/lAL26Tt6i55HVC4ixeHrpfveyHMWvYu1U9VVM20zvrMxaYIENu/MfrL7x/wybYa4SGAfi
oMA+qE5bsnlnSPDptwKWJrhaNjsGWW7N5KXCR6ujc5L/dKa4/XWUjX7pm/KCnAulq0l5n0L60d1K
yANXco/1F4TKTRlFYuEtXz9s83issx1lhtYV2AIvlu56iMnKXzcPE7BFPxWVVqkbryMslgFaV63o
0tXaSR7QljXFbSbRXJomjnBuxbTKpBCnqV+NmTUXlWpfzEoPrwuY0eEDtVkbtPxEHtE2yPHTTVbC
BXQNscGnnu2W6jXKG23Ai1LR2OmxFC4/Ga4nNb2zJt3GpxWDIdgU6PDDx8IkbLwB8v6A7GdB9jes
npYdt+VXMuvnYxgaPiw9kqJGe4NGSV5LpufFtdVgLY2wqU7Zn5o2ZpMc2OyeCV/baq4MA7127e1L
ySXU6ou8L3AWHEsbdfeFGlObC1lQB1XFyZuyebgufP8mm5hm04pkQC8y1pS41SaOAE4Aifepb+Lm
OZUcbiXMBxLepvrUM0eEYN9gLrbzYbkQ5Lv9OY/W5RKOuI/ow7GO6SQ1P0UdsAHp85uWlMayl1l0
8GeXOQ+3L3ePQBu7K+bL28s7hnwPa8giVmMeO6bcm41dYgq7FD1z9H7VGxieF0D4DjvCVIcpMFya
NcW9KCaBiweFkjbwsHG4lc2IFnuVoly0s3c1EcxOG/jwThQ5g9UBb4zxqXldxDzHpKsKVzlwJT/2
J+4uKttd3x22sHU7Vh38jYVaUP3TIYI8Go9bYOzgjvU+arx6rckbou8mstHTBTjkOqA0GaGAIxwv
aDz+lZr5X1kW3pl9NyUcyWz3JcwTYVDd0eeW0Z7p/oZpil2PL6XbG7tVRvY5PIiYbLdc7SNTI0Lt
zREskYhuq1bOub6v7hI+ckPcOd4gn86LTeYmnXKG2lg9tykU0qwlxWua2yz+7g4GFk3cnSHJ+NUc
TA3cwXxvsY6VNCnwe5IEoBznxQLZFRzIQGTbZPNawF8uueDJXogkLhqAnZO+e+k/d0JBi+uF5Fr9
e3OUxVn8lg21AYTn4rqBG5+b4ZXF7AedhcIZLnqtmIXRpLyurymLvP796ui5O3eVSOvSgMV/cY08
bZ6JRzSg5q1xVmY3JLffbhX+dQS9Xyld3ErMc66RoafxUoR81pbMzjksKhWNB41SVwg8IV14LPys
cSaQpJwiDYPWnzCPvxL39Up5+ay3JJoZ3tqAKzNwD5NBzwAKOQ1L8qh8yz4UBdoA0IfznHKdhMM9
bQ8SMpXvcUCZG7tAkWiCP34Zgmjl++EMbRRhP7t0NxzDv5d1NPYOvgZOArz5Sc3jeHlbBGnmO5AP
vBUGHfcGyOD/d/W3JKa3tw+o3XYxXhR53tb0U88ibz13EhugpbXc3b1B/DA5v9IeWaCbFdidtHtZ
L3w9TK8QSg5G/cIPfllBNm8P5m8cSnGxVzRMOqrlCnb/3f/awDDeVZt1qw9OrhfmgSBxtoh14UHv
Y73J1BWcySgXQ7kTUWzWzy03O3cJw8CsL07g0xOyRgdOSQ98X1Wc50I61A4B4Msw2DNoXo4Kf2pf
deWpnx1GGpSDH6m3yoUhyBPRB9v5xIPK56ob9hElIjIuZXtsBRzsJpyCqD0pZvsEmzhkQJMjwEyR
1cbyScVC83TZTMJjgYmJtOsgMks+TQdT+8xTiSjyCGTVIb+KMFYsQpfH+C9KcDX89X1uESB6/InK
Oiv1tMqQVBcmakke/eiiSK+tmQ/HxKJjTGMq+GULIO5jBupegwcY+uMeyoOxmUb978eAXSjTvEV0
MMCCvRzPn7wFbZSMYIkCOULRU9c97OZNqfVL+9PB2iIw5rIwuq5DYfM//XB60/7diEOWbklx8AOq
OlCiOMLZ4UKsxehkZZYEi+ABX6bz5eTfZk0tZJn2Gf/uVqNa/PSCOOHiICcsmK54MTda3/o317u+
Mru1t5GTLYj61t+a/c/t1eb8GtQhnSNbwF5gvUTsby7hkqZ0gHNPEnT6XQO5Vgw2RoDWYSGu+mc9
7Ff5+5ywlMBPXprmRaxJ58kgPbWgGLq962iP+sUQiHE+NqdxNcNMS7k4Z3smGZZtQEvYg3rE/TeZ
FUzP06EqTcZGVGhppZGmJglpQXDCsSLaRc2fIxEFcceVPLxBr/yDAo7oUeRq67aXkvYqtvy9iIpt
nEtuGNRYnmQZwdEzqQLCOrjHCMaRyFmu8cVB8FIaObeFeBmoA99bBdGSc+uz627202DAeWfkEjHR
wWuAfY83Y8X88uuFOa29/NkFygapaF1wyC4im4l+rZSMtz66jZE0Qn/TWqvfAE+eljR1PxgqrzrM
5GEsgBhuPo2Tto3UOMHIQEYfTkep+ZxncBhiJEuyGZgRmTsesyqaeY1E2fqSStmhEIASUsV1EZUK
Xw55v5nKAuKT7gyk7f2Y/JHyavm1vQxdLhutza1seNqhc7Xa6dnL6gotGUUv1BJGyoHxvTcIklKE
GuJwrGsfAl6T25PTWufG6YFAzShP1OK5/aCiXCt837MrjQ/znI1I7lCkRqEY9O18K80YQ6m7InYi
IE9yDLm8Ji8R1Yvsi3wupPW85AEDuDDNpA/vpmo9wot5Kjp8+1ccX2rXQvhIo9b7MYIkfIo1iycj
/VZxj0tb8Nz0dpowJp0W+R9930KNEn3625cmZ4mZfOPfiY8ApmguBTEXNjLtDLVkwtKnD7aLY/CQ
2K6DtqzdHk5Ao7KIE7T8Nw7n0JApl/KaXGaDG6AScX3i9a/k+NNY17hoVrUMVRb2LISX9Rlgss5L
cf1n+0kAfW2gXGNLIRn7iUk2Jm/kJatz81D7EOUfpWaFI64R7iyLZryVWJGp7/RJFDNqVuabqOeY
N7absIP55VRgHZHal/t8RDkJ/eh9FeKbVrhyZe0YRIooy0dyRSNqg66AfuSeJG3FLysvP03NUV91
rTFESrWUXyiJd+lcLDga2r+aawK19sSSlec//dRupgflbUwez2nkeTFXDbL2XGF/dLGAYcrR3KT0
krSHwtv6dQL1NKlpFKmJuf+N0RDjFwK9J6mpIHoM9+itzibUcVddo3kkUjdB0xSGAuO/S+wX8GKu
BC501aznW5eRhZhRcmjnTAzr/gJ7NpHpMmSXXf9cGV32sJOAQqw7YJON8rUv3NhTmClZOXKQoCxw
yG4VwjKDJ4r864I3X5uA2eWE5YtN2LaEYOb6JCPwdVerrcjndBJunG23ozz82LhsLIm+tONYU9pQ
eVU3YJLUsPN0Vth6rkohBl4NdHRrhdH/kvovMXafYVCD7xxMrQ0hDQZ3K+aXl6yCtkyyn3Yzra3/
jjysR87/s0RaXYodfvv55Tclgl+advIIgN0d1SnVMxjcoeC5Hu4n9Pc81K2FnSBopAKqPJoOKsqA
Fldrw07yTwAo25dhL0Ar/X8nw00RLTLaAmbHjK5M+fshePFQF4u5yj7OmtXtvQluNjGV5AR5yXvp
od6F2yxDI8CzjTgtda2M05xJ2Jy8NGow0J0cvu+FqL9v354galH+qym6yamAhnswre5YZJVsd1JU
v0F1pzlDtGNkOBBr522yoKfiiJj0RhOZX0Oh4SoCwmPguEpnCKyY+sKabaA5/LIEgsqFspwToAKT
21auK5nSMmG7FCrhdL2VSPZbrxdPZBUDdg4IFxOn7Q12jNy1Ky2YVaymzgqHdZZUdYRp67UBaZ5g
GY7PUSO4t25j418Hx6j1+auNUXEwHwSSL35WZTyI7tT7wyIZIUysncyK8DYarHNPEM5IbNpnwlF1
vod9AfppUcVFPw/TGXnhphA2hm5xVzue6wd3iBF/enf0Ztyxtctz3jCSAZqW1G80derGjGBkQGoA
HxJjAWNFCVYYnUVSflXHfqKzrvm2qJ9O1iP8GQlJMM6tDuTCyTBEWLQ4iqXcVsD78FwtMH27bp8o
Fhl1WPuG2pFlrPNUvx3JI2W5CSOuDAlUrnGRBNe2O5JLVYkU2pw194/fS5prJx/P7pW6yA4V6ne4
uK/tB44qoxhnSr2p9zNn1hdPdsrL7HK9GJ+P2+UZi6nDkFwnjxmjtCR0OVUyAgj1mxJOfHkcdo1o
ml/WKhbTQ2Oi9dJmVYQUcOwqIqZOzQ9aDq9VscF9UW33AAaO1lPZKuUpx6ajVMlRVAbS6L+uM69U
IozVT1dWBXHoXOb3FOp2f1fFdR8284Dc3d1xzwjfk97wG1av+ebqgNV1KsdeDaqXr40pU8tWSPSJ
ZDYXy0S76JJ5l2jG/xs1JES6lt99MxDt74wtjv4PZLNiLanqL2xmH90byc9E4A2pyXTiXTTk/ojt
c41HsSDjJAhi7z2NO9jBwzH32GvPx18HIKMTC5o3bo2MZzc8vTG9emSi8X/dXcK5/U+RrUlSuSQX
SIMBKtfRTl8bFTg5Pg8HbxhdDqnU3E7tx05VarvTPaxqa7frxfRhbzhcS9i+AciRRcpwiLF9TMjF
hmMbe5DcyT2J4x89XjD//T6xVbmnXFqkXXUN1jN+wUTo7pVpBb/z7bTyP4RVRASoOJksy1c7I+Pr
zPoB68zs1ajI6LsvL8ylB7HrPyQbFgXg5MBEHL7sqUANd2x4W7TkinjLkRZGU7tCXpoC7SwW5L/B
NjSIuGEb0ehL+yLOuI7iDfqGSEnDlrTqUXixs0vGQVD3Ta9ub/LWModDzCrqHy5P0eKL3buxVUuW
fBnCT0nwNQpNdGMouNFdQlbLJKWqJY4tOKOeeSe3tQq6HBB379qbbuFXrBy4wUvksRqUn/Vl+JMb
2rgJh+NkahbN0IYSu8XHHp4hYWjsNsx43shPJSlOiNog6MEbncvLDCHGfMLSueTWVoSr4mbnhxwh
/MnFaGoIF018qwh+VqNXCf5QyLAV2J4C1cwDuxtKoV+7rijKyolL63+kqSp9b7sg9oZ7gRMEN4kP
eu/QgKfL1uktx1IU3p+ZA8hLXdOmWNSAvSVD3maVMhWhSM/CycuaIaKdB9VGAgAO48ru9pOflvLK
Yae7kQAhLwTxGKLowcR/Zbk5V9aaBkxzUkCy8MFoiNjIMqDvydNGNZ82rpYtTvRFwsg72vcpN6W3
FodBidAXemL0cvy7w9HP0UQ9Lp/sSDAMCDaeQBuk0pzc/k76vmT+oFTXuRrg5Prp7B/Nd05xr7dI
Zaz+lJvdwG3AW2wOPjRfhn7bYRHb1kqX5PiCJjYXyEWHcGwObekWFtYmQV3tfguRiz5KwjlUM1pG
PD75EGmxqlXW7okIbx9+aDGmkP9Klt1/51QmH3Q3U4Ji59qzMXWbsvPngebm+/JrI9vSlGX+mRCJ
1knNW+n1k5bSpgDvY7jENpFs0i1mi/uDdd5VCMreotqGpeyrfQlaO5MhSqPjpUYLzV0SKZha+s1e
GXZIErLpsmzfpgRkywL6sEhqTqAE1Dyb7bywIZj4d8VNspL6rgbSamztaYHu3zcvM/YwAnHt9PjE
VMWMdr3XrPwpkFU/8Ka5hRaYmeXuJJpJtRVXHEO0eDpwaUV/uaDiOJXz8UDrt3XrJD3OlcHeh75Z
+VX7Trz68EXFWuhELTStf6IwOVyXJpy0YzVRGP8Q/sFHeSJl8XKHZCXaAMyeEGdG3p0X0WR3jBFs
a2Emci1XwkLXhXwLlivV5pa3FXr0+ggiQHmoRzsimBR2jhjJQloypRqmgvO5j2b34HxGlKpHfhMx
xwfRba/A7ieP5jzRiCUcQP9dLhyVIyU3USPRcRHOn9sOI2iVzGypsjsDGRMJl5nnFQoy2JqCK2Zb
shXl3lNM69HPgoeG/zodxaQ5Ku5jZ3RXRtCPWKS7QscKiaOSZZkA68xcW4R03Y4GL0HQ9M3OC/k3
lhjDpfadv4i7ODuCJSC2aIaFlYhXVcUxeW2tOgrcl20G2BY2p0XXwOHy/P+g8ENS0TIO4hl6l2Xk
Dbwu0yOLzjt2DDp3indgyY/CaK7htGlIhbS6YsWaxxEN74XiegfxLgNX5fKCvbZWWyh1LlLlDFxM
NWOPZuFjf48L9eONXzl+YvYaVfD9K+nRI5DrZKLHD7MLKWRhBjCxPYJ+ZxBnQffWUROCYQ7Kzmk0
uzPX0YR74fOORCexbURKlwrfPf2pzIZw8L9FMWUDON0b/8velfoPP45wsCTUYrpOemaEofta7ya7
+/Ndzn2ZG3AiwaKCBBUx8x1dlhhKafB9APvHR9tbUnz1Y/jJgiqf/P+gKa9Eh27NuhcTljC6708Y
ngpy/wP+F8q7i7Jw+bkGV7s+QblnuUVcgzEQCq2M5x8GkLngnfr6SM7zYB1mOMkTgqkGNwG1C/JA
4+mbhM04F2tCewh0awtQwJTr/q6XxVAC/OwLJLPlolu85Ip1P15Ri2zHOxzSwldpmG4UVcahvABz
IjRv4a7xRxGIDEnH8tXad+Xdr3LTTWsduCY1OL5lOK+1yTVKmurbXbdTf4qvj3AhOYp6RhIeQyd2
w/I9W/+zgoi/uzfHct9fm0ibMpbH2yDllFWsVn+is/0CY6IEWp+yARzpN97gpQ+DmrOMJZ6qf/dj
31BHdDm5MGrda4MDb+0NCN2p+/59Z3klTlXOKYJEc0BvmO5FWqxLvdv2zUcLAJeQqZxVUxzqmexi
jzwNO/T73WO7Szg0aFkBvjJu1MtgHtfsayvgAwNz8zyYpv0iE6ozUmvDdqwms2faz3O2fQ0O0ULC
6rM8o5TBiO/WNyvnr0OJlVOufogZcCrsTOtqXV9jEwRcHdZIbELuIrKQu61E+YV+L9XtsOj71ERS
lQTMECOSUePdpaVSsckFBjNO9OrrpqBXAqnTiuHDT3Tk5X4tNfFy/v6hjbBthMwe1TKTGLt0OPk4
RgER3DZUTI8wLpii8GJZowSsiP3MGRx3cfxooBRvypr9Q1N2hwaiuhLY3EB2Hetn/yY7uElO49TM
4k3lRKmh66uiIRjdZjCygBSA/7B5mDOs3RGcHIKSqz8/Tch8x504z1hGR4lml4y523W3oTwe9xXG
vOLZrGI9qFBiF2LECbQ9YuSF/AbZGvbNnrS54skJSJgwgJtbB+2VSRa8fsn5DFcsr98yOCbx/WcN
2uLPub0X/TTWkGlfCYNuovdRPl7mFIDnyNtouyedBTvIb5P3th3rkucwL5Qq7hLaX/cUz5Eeo6eH
/J5+8g2Mu/W6YwGzrnersD/nKcA04iFFuVLOCFEsMVlxFOSGfkL3S0NMZ7ovqg6vQnaKclgfqCvn
n+Cf3SRO51roeQyjhCWr6COZPKfD8+uUc+dJGfF87kTb1ZGvi+ziC8gtJHZigF5hZiikNV27nTOy
2LoM2x3LjCftH/nB9n0rcaIXTT6YuBiDKsEqbFfI4j6+OWFx1tP4yYajiSv2IX+qH1m+SZZFgwhh
XvQ1E1PJjc16umdaFPDeusqDQ1RzL5ssuaU4aQ4sSHBUTe4RBLJCfo/RhskT+CGaUfbOamZW1NQR
tvCno+/7Sk/Y5YvrbcE//RK8a/0cfP+Bk02gKpUFuk99dTlVQusmRUrSedf/uOO6v8AgasKjbdeO
IYmXOrtCNL1QSCK3f+LVJzI/o4LkyZfZkvuEotCYIYdR4RO9qe2XAv/7MSKY9q/TIrlGjiHH3lMb
CDqSjgsP7a+w2UeRJ9BW5hp0RGqF4+E8YAWfD303voMal7ggbrpKHK7dAM0w2RtpEGGpYw+UH80h
XveI2atyXSqwDZAY/udo4lOlF8qQgXm9UfQajd96JOiDEiUOdGIioWVEFEcCOwKttmE5DeNyCmPr
RPJUSOSGZ0GWixxpWKmZN4jlgk1SONmvgvgYPNrQV09jZ3ma0zgwkp5E2eCewL5sCVTlMfuXlRSK
qSLrBmJjKbT17qGyB9nqJSo29L5CF0PbvzawEEJDwp9OnzMqo0dxs6hv3bTTfNiC0Y9PPL1sNIo+
qLWA7RbhQ2hKgu9kB576GbEBLhK9ejYL261lsPqHBV4c+HoAaKOByokZYOQ8ODBEzPPSlYy/Fi/9
h6IFfFyovE4Tuf3nhCV6ihrIlBtfodd2i7EI3Gluu9B2xifU5QVvuGXzvAVDfGIqLYxXzAA8fHSE
aOQh7mcwZF6QT7vteFn/NH/Oj5YoZJSpEhrjXSJqEZWxaiEqL+QIdSJofEaeQROLN0XSKMdiUZs/
Roj6hiBrjcsh9rw+He+LUUtY2cRgGzGYES/fp22dHQabihyMMTJcSuVRNYi2e6siWqXaPMou6rPt
ejA2FPQA5UYoVFu18NiUcGhrycKT/og/HQS8qtrLsITgLm2zJpw9Lx25KyCIl/+DGKuKHqMB3N4M
NQpeQHu8P+zHYs9u+0EfoYw1oVpqM9nQ+OSuzsZkIbb4U9iG5WwdAdbirzbGluFG5m5FaKCIKlJn
xPCYskOl2aBfwXzEJFlksthCkKBiQhL/bCFWZWuaxt2KNpIQCIvganQxOKcpEeS69hY/e0jUSPjb
lh8BKI3ttq20XGjCY11L4blDUbSPNBgVXFExno6J5hPUQqlXRjdK4Uk02Jc7zrMgPaoIPOEpgVJQ
dzv9oPjJvHM2gPBRwAwlJrZ2gRkKZAr+J2J62bZygPa1laS09ZGzj1Om6q+0ousszzzavU9zoCyC
JIkwwTUdL/LvaBPb77wrxagv8b1b/5KhAyv7JAH8OoXMDfsK0Rq0P+vDRLJ6fqSvq4mIWrw9drZh
KuaoA7+PdUiSobVyTdVHWypQmzzZaZCUmSYDeEBs7okN3vdj2cLy/0GOJg9Mo51mPfla300eCOSi
FcJQPnU4OEAQBo1tQyMVprbbX9VC6pxet/pccFyYXQThNef95wmVpuzOVlbneEVNlg+lmcS2eo3e
W2Z7PfMtWuyfUZxTAt3H6cwg5eWkzfwzfNRaov3BjrQ/BwnbVtfMNHWRp21AFbtXp8t1fKGq8iot
o3YwDxikqISx3Bi3QkFm5yekF3rx2jpuaPB79wGiHT292TbTwiai4tGVr6QPWY8i7Li6dhuBpg0W
HavEcDWcx3niOlXQ8qKsC4NafPD1dDn+CblgNagQxm7531d72eF0NcQdppk9teCEKEYHlw0VwH2d
MDQrWBuRE/mnmzIiXyE/v/Cd/uYwFQc64BV8Ot/aqgRI5bFXrGyObG/1/SSDWARm32Spx+gbnE4T
vGlH4YSmlzGT9AFOMeyWA0SAa9KqyU0oAw/Z7j1PM4+lsQytT9qBP9BF/zVGopI2hm+LswjE3nQT
1Al0acmXTjLjzBj7G2PzHLA/7kKaeO2RIAZ7PguacgpHoh+1EPDIZTavA2oK7ZZN15QPcVop0+4a
hpqjtPPap875pEA+j1VseZtRVMnitWtJZ9MQr3q+h/byQ+sc0S4PTByxIZh/sWgiY4qUmvzMoD9F
CLhDjG1ZaCPcA+4ZdQ5miBDgJRo5vJZkZ2EwIeyzEBHr+PTabFrUNhANAJ0SOwPqrb5U8+KpkrkT
ILTapZ9TW2n4+VDZwEO854gzbkvgQuDShpx9L+BuTdm3L6/Kmg84nlmP+TaVcKYCSyYOUXG+7DVn
45mhe3jilfzlU++6ByM+hXqfgnvKK+D0n/WloMslro5yXP4hQA8fSywcImHMBnJMV8yQvpd8r9iZ
cj7wQLbjg7oooqENmdqcEPoRPydyHTQVccHbEG1NfCK7Mclrlqx7EDaZ5Hq5VcDyEgHBkrHDaWcq
yS/Sys1FEvuVV9bIe/rk4TDlljkzoMTx451zplH42UOj6zPK9sTxXrYagRoDDanWrU6YjAiqphbc
hwWjsUpmT4K99H7iWZZCOzjTQZHXwUo1v5VILTDSzUiUI1Rqo9Wt1ZHAxZBirK/RjRenSoVIpc3w
UCRNADSMHfretMDqb8qMTmoKHzcpHodqX/Oa6AhC433uWLc5W1A9Qo5Cn78U23BBzfmk4KQaQViU
2t8i8PDhwWTt/pBz4NHbvq+UKj9bKfw4plYnA3juOJg9G7sq+TbdD/JrI/xw2mnj8xVFhUVvVOIE
TqkX//CORWw5DTKU78nveuaP0vQjPqPBGSGIW7ZQUqcjx9i75JvUCXHKhdS5eBNCoKe9AJKT31Ai
Gd94H10Ld0BF1A1heIfK91EdFh5juIFFNWzHjlKHL0sFkxafyk5ZQ9K4uzaEH71syjemgJ/gAAHB
QQQK1N8eFoo8SqiOBmODuxaFLzOs/K2+uf+k5viLT/SvMl4xbm/S23ljkrd6rKEGTu9AQlDl0hTF
X84xl2FbF/ZU4hRhtlYYYsKqrjPmFZLzJmtMQbDPs4KzFfENKczOZkjs5M1ytNtEt7XKlB4vhk+I
P8wGjg79YORxi2fp2z6ii9MidJ30gFq1De3azSYuwVr6UoXK0kJafC143q+vB07Kx+fELx1QNqxQ
0gtKdw4iIeoSo1UNOoPjh2q5OZMXqLRDPlpLULQ7A7UC9owcpHPU+o6JFEKDoTykIbCavFjmIv2B
RqrZLvln8OrsUiBkWT7jt9TYW8oUbDCuYuLGOR7KlJVCjce0yxyk7ac3JLmFi3ppfOfCbPDCIteh
jXe+J3r0SWnzp1ZuD5gc6v2zf+z5AnB2R/kSMRgAWSfDR+QKQ7FlG9sW3mlenLMEvFE6wpTtRJY+
WQjCImwKU9/L6htfeVcXwCGpYRBxwHrlBnYvqY+Rg7aCeGZ4HyttO0/dbp+89/mPFdIb9CiSyuSp
OnsmzOvg1C2RF4dYXFVwKS1mz3beHYSmsIy4Tv0L/7Jx91b4QulC+dyNfd6jL3Nk4EWQkp/+8yfS
FQvlsQQoEn+NBkgSoas8x7a1FDxuLU5lMEDfDN8FYkspEpmstq/NP78DyR0p2LFsq9npqRfvI+As
KeOTQ/WBN0Z23H5jXjSHvWXvmvghxKeFXWlthg4HIUHnMk51jbqfR6DL1aN2cliYagCjvBWetAGW
w4nk5f2etWIQgbt6t2pbCarUFp4z9zE/iqiXE22mX2sol4BSPQmBy3Ww7k2BaRjzur8tkv/9asMW
hCqHqdKOojjNNyz47dq+h2rLhEQenzISs3KehF48sJla2hPFc4pwXQYnfZvhZj9WSBd5Ybng24nS
s4ddRGCjTyT8AyVgVCR13VFotuotP0yZoNk5WR84XgORntPGlfcYXKIxuA0zqmjnuon/QuXsrIqX
dxuDW8jgVj/KRjtsarJ/WUp0NfE1bIduFQxLOU2FRVfz7U7IPxwUJGnvwfiVbdIAdF/MJyZTsfnc
uLNQhtr2TszKYXf53HrclwGnemIBOet8M7AvwjZUtQjkZsxteEX83H5QvU1ZqfuOb/4gS47dCLRV
gBVi3XvGEctecV+n+z/mPDL1QY8OCRFg2HKi3wJVcmbP+pHN7i+F7YLPA3tj5cXLoe5cbo3NgnB9
aPfkfK3ryoVbemj0bRTE2IkdtiEJwzPk0OzDNZM9Q/7hiqfSjv6PUgsduxITGDb8rv+47w8xKOsF
04wrnVrTNizCX9NTDTHq2NT8ndu3a92CoNSFaD3t2YODuxfHlfYefH9JVFtWSiMBP4TchT7SMKHU
4IPVHtfzbyjTvNf6myojuuni40ii4pJta1oCuT6HeuwAI3kLrioWFQf4OpYfERiWBq6hRW5ciWAE
GUem08moF0D6lUgdoOF6G82qgGd9L6PXkyVVgijLVc2b36fAOSN8xWeUuFihk47BWmu7MAtqFRVl
xjqpem+FzoURLXH87RG764v1fLrGyf3YNK8kvUbwZzEwKm/DhHgpJynJkhtb/flvry5x5elOHR/2
7IEwIxTaK9AFYGIoUJSSx58+T1Dj6OOWeQn4/hil8Y677aT21auqU0he02Rhz8Q64EOWV73U8dxA
NLJjdLfQqxpUL8A7JDivcSwySB1Dd6oeG9AJ1FdSyjqyCXE/dNvmPkeDEHluevVV0s6IVK6f+WXo
fwxnOTUwi6qJwA7ZZ8IG1i+0bgw2q3MBrzp5iZiif0jW4rf1uF5Qz9NYoX87V4UF3NmYsCcYHvXu
QOwdNLS8Tk8ILMp+tZCqYV7eRw1TQBOV9oSuIDWRlA35i7f56qUc9D0CuZZyOxF2K4u54UCEmlrl
awceFZqn84Nj8LFymhjMXWhfTPxZ1EA//KOsMNoWQPz0nMeHvQoIyOo/xXqDb+No3eS7mvS+M8Bh
r1dgpz260h2QwSL/LeGKpcrmPWIq2wKRX4M1n3v5wHYNuz77Bbf9gH4YWChI6KKBXja0bq7PBnT0
/MP1gbIPbgdzd3KHoYyLPCl1gANsKD7oV2Mfdv1HACxuxVp+sUk/QjC/MG5uJhBW74zHTF93UmUi
o0s2pckZTGn6IRcT3UaK8tYIOM2M1yGdF8cMrGirrZ0kDKFmA3oDYi0IredaAwisXPFT+ka0GDKQ
s57I18cHKmPD7luk5seGJHsbEhWQnWxGXKYlT/13Oxnac30Zn9x5EqyBl32hbInmxAKTnLj1zhyc
6tpZE3v0d+47SqVaFCWtyMaObSNzlXLbk9T971Cm+INACX/S6mow1+COM362Kn4HuAWcII4EnbNs
4Uus8JSIPYbaHOt3FEmHpAchlsUQTze5ZkKAi8it6SNTYjtUvJlVdxMNGMnyaGelDoH7nK0N8mv2
5H3YIBjSi2wVyUdZL4eHo+MIZEYN0cx+gKkfab1nJihY+LJbtvvwta62/nF3yJYEOHrq2jFpJThn
dUn9/8J3HfHg1u+fJAjURcAARNP4h1GdKH7gwxIuWgrIORy4CutbAzTmXRorhx34eCtb8N2/imva
SXr1FDIXtREMLchW/zS73ubkbVcqAcSZ5BNh114ciQw9tlm2byjZ8CozNEDBPLjHsPgffCIjjhfU
VMcjvQbX2r1xsrDX6UcxKxc2MzBwa2vYmPmBvUe/GV1HPGucariZoRrkg9oV5pcY6n2X3D043D/5
eOhxNgp+YuxFj6BAsmEd95WPX/9swYrTRprUGHK9CkVnFJzZrRTwVNZA7sXHN8C4+jW301rq6I4E
6q2KVlnbT/va7OwBHHX52VlomgxauYMgrmqM1v9v1QPb9D+HQfXNVgVfQvO2d9yqrVr9bF5Qn7n6
8Y8u/pzqk+qe0VIM0J1Zd2Az13cRoyPpiQqnArJmcU/TIWi+hehpKptiFeuGegai8Jh/lBOqrVoK
U6AABHNPIvtbpwnftxwouAKNZHs7GGYUr0MAih29qTG9cHYipglMFj8zmLBzB4KdVJLsblyZEiCD
FhDRsbmcyk4BVKmMRp/1RXUggrrw4E6/VEfcS9hrQ+nWY9gdymJJMb+benVcR39uLloWjbmtcKpB
byEFaGEEO+SXyZ1A0He6q1TehvoYuCMxv6WC31q2PBgiUHgEg5qIZLvLYI8Tk3wG50bjyOCu6MkH
ZBHL9olxowo0C8gRJznfgJRlhuJ4l22AVsg8o2TSN0H0j0ERNPxCYohkUJG8R//nU99DG2MJBN3Y
1zV+b6c6NWSTwdkL5cOXU374gM2AaJXfacLNY+//kbvVlLQ0NV2BWct4rgtDomAFOedOE2Tx4ixF
cbOP/GjTmjQQbEVksXFfvnCQqZu5Dx5EDVfWH57LXLXc/EWpZq/U+RF6jNWY57fw78TiPe72neQM
zTbxbAA3K+sDHK3Qa7sP6znHnAS5jku2pG03PZbfyZQ9y6s3CSYFHYNBT+9m+jtn6CjpGGsZYQkr
wzw2P9IIiISmwJ2802cQtXfe4o+/7yo55OZKUGCsh7NSxGdDH+WNexP2A7iVDptuxHPL7I4/mweP
tEP1qovKIK47zE51DzkTk9mnVSqqx41aX8MYl+Dw+uO4bTLuMbqXqGutcSnhA3s4Yzjewq3azLQi
KbTdRd6AuMRczwhRNsGJJE2Y3sT5rbw4vvkBmGWjINrh43DeX/atMLL0ItwpCvoamh7McVREeET5
1UXfP5OEdPVszM53DmBpdCk+JGZ0f8q+6uo6kaEaB00aIV0eky0vpapPNUnFjOC76Ys8h9JlU0tw
+bL6jFPPqm8i/GbzcVem2qTXdgkz6GU6OjMILA8GOyT+Gqs8cdYrGQwxujoGvY7mzdpW+38tFU1J
r1HZP9bu08TeYoRek5eVj8usYkug7oOyykMiHg/uXjJzm+vMQ4MbJ/sCnH0tvGiBG8xS6DoZOkKr
G9OoCnoz1MyhtSSqPLZg6wpry9irpd6xnnbX3IYEQvYmIEts2Y7zF1MX/lJNwNxKdcgWepQiCtRx
+Z62jFMR8D/i1Gk+f0kStRR73Atay3MI2oo9SOKhBmBX/4VZLfjPrxjRMRBQp3bN9EXTGgCY+Kme
CaMdNRoYyM7tqBbqF5nmh5xy90LQWIy7KxjKcv/FYf2GwrbZ3a1Z6yfqxuw5+WYA/vXMVrCtkGu6
1wx7/QA9StrP87Fl92x0Q/ELQ0JwOK6G2nkmyOY4nKBUtvNJlxDh/cbxi9UFH3ZcwV0EblKM44Wx
lHyF4wswZK2rcgt3DIsueW2t8FZCnAU72WwSSJTlGpx2+d6HvtdNybN5HYicbfwIFcCz3XSoXPpu
mtJ+gJjTc9gQy8QlvxB6qUovRNEZFyGRvfHkmharD/NTQ/824u/nGY44Xu/mbFmwKjtRw3vSPVMG
BTHW8GKIulBdybGfcdyzQDzJzvBbeXJ55CTEU3VmPO1W4PO5ahKh7TiaoL5tGHs5+wDH7xSxHcMN
Bc54/nLE4wIB8ZjnfYafYoCw/VLjMbTAroTTExvDT8ugyC/xv1O4hLxKxUWci8mvYRjc4gMMu4OQ
ea1zn8co/Q7mKup9twQEq2krx12Trabj6G+Ovi0axkJ1y1c5CqArhS3jC9juaCZdNHWut0/2v//a
/+qllNTL81MfTeN9UEIslPMftIjaWSGoGxntgx7Q8J20cvCbefEnzYNI4jx6Pds5GBy1/4TqOE5N
tSzQX9Iu6Ba7eVnc5LZNeWe8vb5VXicXyhVoYZwlPg1bIv5gyIoutwXmsg4n7AoYc+EXOGoS02sM
QngjubnDLjcGAxrgWcA6MmL92V49LUG/Q1iB/Xie3+7hbKMVXGDz54nMWQ7pR88XgrQXmowiF8YV
NlziTY2Gmc7cCslpf4SFnXb/vHrN45uPdpax96HzXbHi39jd6U/2QWU3rKwIbL79UAeio3T6Lm9j
jAdYYMMRtWyTdbtQ1asNx1AVP/yn6jFJtZAtlHp0V0n/WGkLdrRhd/5d27N8zl/kmhLhuk8HnOBg
4GBlf2ad/eLAkW5Gf6EQLOImS/wBvkvyUxAHq3irC25MkE0cd4dBYxfNUQuWswgTloCDX8yQ2wfq
rVchaJOma5U73LPPITUo2M2E5iMl0XyvxPgwe+kdBzJkHML/js00TOCoMZHbJRlW3o47Pw4Alt51
TNdGrli9h1elBEvW8/F6mJMInR8dLNX7buw6JEOAg1TCGwk+w4my28j56f2RtlKXCVn/3PBnZK6C
FsyX4oTzgUi85m48H8bpzjk5CcAq0yJYRmesb59UJycqoDZhvcvpXwPAWoFUwlqMsaBrG81eiHbx
IOA+/htSdahnysryDAQIrhMpdyOlBr6yxCiKLOhBDrqtVdFtF15wb8Rjx3ShSq5uvyx3gkWad3Qn
cE30lJKcMCPX7p5nvNCeH3aqYYRScx83VPBlhAhDZTeU2EIlSeSLDiQdQREvphBtLA27R2KcYZqf
V2tJizoMzIZBTqkGsKlBlVOiUji0bxXOuQaysZ+8kN0wyAuJPLIaFVAxkPCJu8r1sic89nuiByG0
MzKIvcO52SovY+nwUNNi1k94fr+WGwnEQeGKi1smce+hXx6qywp3R334BpqB4bG0IntX1v7IpaTR
1KzaMEZwfO806ys78+zZ8hBwxRr7o4RsezkYTm5pf6JULNhZ/QxORt77UoKWj5geI1DdIXASh3Yg
GpqH1CGMM+4oC4oC/15i4QR91LlfbaRKs9wn4SwrsiXH5DX/C6QdfxN4Z5gmoEFtyIMZ0bDofEtM
8viF7T3KcufXD7p727NRcZOrJZxVD8LTw1kzjd1uLlIcBVzwh1PT7vZfkqVPZ0UD3rqWedXcsI39
1Zs2HPbHL8GUL/CVCesePwtOCg9yusSt9nGwP9Tbe53A1ykIjkiYnRYSwyisT1Tbt+UnPDEN4qL7
m0ta/aHC4M7BZtZRLOf/JUgumD/vvqbwfj8ZzPVwaiuDDsG2u0zTWeCKX9a3Xx7Ja5TZ4iqN5KfA
jCbjeJEb3syE6Fw3tOb3kT4hdUKx0Dp7g1TSh+vu+lbAMVw0N652sh3X1Gf6znBb7gtGWZwejAPc
ct3T8HP2iHa+jfDTD5Q1XIYupxp3FLzPDv666gaNYZ//kr8YXY6IT7dwfenaO2xkAemGYAGPNn+/
b/SbKHiuY8UsgfYIw9O6cBQyhw4P3+p4deicjFeknGv5cIgpa+bWuRi+CZ0PMVN57MxxPkL5h60T
YtG8Y/nPnUlBOZ4pYYTukAHPWY37rwNAFGhkIsSdISeuQIQcZrx1l4HEl9d3kp57ocVwMxHBiyMN
ZmzZ0y5ROAKOhM4BUGTnXr5qB6lzbOBg/jbNLY1HgjyPO8IDov3J/dt5yEUgVJ9djAMMrhUOxCGD
JX8Bj2KNxH/CZKhBSRHZtkoMGsaBvkEmbZ+iTXrw4SSGW9K8ZP0tCel70PTKOUquoNZrRfaAY8qh
Jeh868fB25/2CVhilMO+6Tuc8ZIamW8tMtvwUGh63XulqkV4R+L9oPRiJVjOIVV/7wnGa5+LxhIB
2KEoLgZzcrshNdLEeJu588fX6dAeXmCrRHUoJEN063gXqM5gdTDEBPa34W9h6SqvsXlwdtFr7dqq
abW4IDihIFDDZ8sxBcY4oIDrGUsCXM+DzPGP3u0fasnfTXNL3jWVvNwpURF79SC3u4/enTE8y4bB
pVJqFj5KhJLE1pT/M/aO8r3mUvWpXzbEmFoHMBcxGAAKvtZXfhkQRWx1A+y+t/65qPlqbHoxN3zu
BRkIc/AaFZhVKSrEv1TGJYCQ2CEQYTckFvKqaPtysYNfRqn0ca5r8Rff7QOBc0HdeuSp4ipV4ls2
aMXDj8y6hounvO1Ft1CEpo3CSlRmNCabc8+FZiGFWlJkZiXJ2gQPBqG7uoamnAuX8vFSWtnNEsbe
g6SpNIpW8e+F46SmyMFupPS++kZ/f4dDWF/cq7T46UXgVEOui/CWlfVzPpq1AoIB9bxUyIOLIfOY
7t1xjuUz2AM9ynMdW9Qoi2/nXJkqgEtR21R5VmHkfCYrYbBZAHvg9eVYVinv7FvZdmdWkR/FD2DV
BahAaaWUaGV5aoQPqiZyxr0NbwYNjJFlOSdxsJSc4flxuXB9D56U9atywTyYxOqO0VEJA0r56PNZ
kreR2WIRHMzxNbylqTPVmyphZrOeKF1QS2yj0kU1HjEPgZMaw8rhte5Ef9CfdK4WoDDA8v8hFSHP
yHdCjnZDZ8pAsHo2z6Dmtuv9ZSpCzNjSwHRtZ+Z7vWW60CcJUx0Yqw4vC5i9gC21Wfpav/fDLMKw
8mP/TscKjbVg0EW81ayLXb2+yIJbTly2xm5tyoYk8SSFmfDOD/qprMTBJAiQyyjqYltF41Dp1PZP
y2xm86XMUqzyLzmFwyI0ODhUeo8QJJXcSSc63Wb5m4Kbx00Z2AI4oXubwWLnH9D8yoAAJky+Zz+H
rY/5P+NLhMv4MDr1032IMbCUxG6wjOioM5N1FYbLXT+Ej1uBMCRf/Xt7gN1r7oXo+qT2lJt+KW8h
PbQGp9XDwVMzXLtF7R88r7gxS3wHAxN4jqzrnwpQ+sFORmN0+/XJsTm2XD8b4KzwEd9qSOqk4tzM
PLZm3rGsQJR0yy151mdX4UpqwXOZ6UdGxG4nkgn+tizTX9CKbwleGzVBy+8l3AKmERVbpLze5XJC
UIADh9LaBX7U4fjD0W+9VwM31DQ1l1x6ZFzAvnKPoUqlwSzLy1Nc/U7rxrwjl95ucX1he5Y+d2Hd
Si3lOTYfIbLOWMDenMNrHBr6kPzYMIIr6FjTaPQfuTzEAYCnpFDqtdQcgqlZmrpRdIJEdQ64cG/X
NNXoQpXHWXxycfb7gV/j4maMlznPVGIepshLCvZZgku9+F4GPHRuBWq6mUtPJLO2eQP7GhrKDqCy
djhC3pyix5IDYYhYjjVKyM5cP75OZq8UFpXUyXgZbpxMa/7XfLx5F4nGrchBQvt/h1NToHSMLu2Q
qPZbOn3URGU0PPwTbflpNzYOqMa/HHhwUV2lUjmjg+yqz2TAWG4hxAPKl2eMS+9R9KqSPylKi9DX
yWA0BYjxB5Pt5wEGowA0WeTzT0PWoP4H9utG/2hZJxO0/63QENq38qTWFUWqrMxmJni8+jaCyZLH
3fSRwVXYyors6s30WLpqQQc64tZrt/SHGE9lE1gV4WAiueq4K0ASeYbm7hBl5DQHxbTAD72Q5okk
uRuLqqyrWBT+wjnfgtCbMvoTm3obUrVmRGqvA+/BSgaVi0wpV9ozBWeuX4DAzy619vhBuFWKL06c
JYv2c+LbhCD4zRNpuYq8MgD58JHkeNtgVh257sETvZLJ3VshPFTB9qXLYKOlBXzwYJHOD1ptF5D5
eLIHnFhHIXenjWMZX4XsNPu5kz7y8C1wQnw65oMn6w6D9XF8k4H4FRkqakEH4fmh8q+1nIutUxxM
An0qWq0rH4ejl6syyzPHAUT19Rn1Ye4hwIhGFqfP8ERnmWAEJmQmiB1uRO6fL/jvN9ZivPDpMItQ
inznZMb5ez6w3ZrYl2Ld5/qQH7DVU43Qvz5L/m8rmNHBxElMLmdE/dUKp4mN+N9nbYIeH0d4+rvE
GwNc5xiqiVuYMg5jRH/wP2ypkt+AiXjOp7H1xhcLKfVjOKTVsScRXq35rUWkFEFqOfwtjqIjnTwS
GW/YlpFLd+SEK3gpX03OT96qcYDLPhNX8ZYsZjaOf5dUIvZ2LlUFztUQ/A87RTnIRVUepKRQ61rp
LYzKJEdp+g3k92ote37QiehUNWdqiQ05zkskZqMIQ6Br0qQv8fghbMzOvsqRb6Vc543yadFyHhGN
RgaLpO/sxoSygeNYpQX7fgOgj3oR/WDs+6o8XalVj0RYWrM/7pOaSIO/DS9lUVlfaSf0RUyxw6hY
LyDf3eAEx/y28qQjBuYuyQZVa+AD37KIL+MsY1rWcC4/yZlUOBKzWFwECrf4kYDne+MROq6y7yJd
9bIVtSMg/FwwIxc4LFhBpEzIp7JqN5Y4/0MaT7SLAVVVUjNB+kH6QwqJnMAN+0QKKUyc0pIPZYe4
3mAQdkTV35NDWGPLfbrOx6Dfgb8eCGsrOLlRSn8rdEaDvuUPnUQLqBx254Um6aGN8BGo43NKbT/l
GiWeIfx9pNebEdwsBFmiiohHi9PiZOK26POfd3i78MVtbzULGxFZoNDHJo5JSjGbvj/iWN6mfc4r
trUb9OwxPybIqs3Jwsq6T/f9fIGIoFVWRxvs4fVWKhnowRPydop/zkDAOTRhZFaOuAhKxHVrzMR/
wCOJJIwhtURa0kWrYOcVo0OzNHk4qAxNXPz+rX2u6ic5M9wSjk3bKXXmj+Kwxibzt3p6SqtOuT17
sy+H/0yqdTWU01WBbDgZHopSB/MGIMghqiN7xI6x9UHsf/XLWIJoH57F13vwrstq2qRJvgEAOOR/
Novz/r5/pBjbbqPyx1NA3O+PJBQC8v4wbtPaAlIohdSNspFVvakE3Go/TJHb5zfYBnxxrGtkHlWW
Nc0o5tQ2gQLY8ytVp4uHgZbtRLn5vS2ykIqHW1osBZByYCo20lt84fBM/7nNcKqDaBRzg1Lo1h6D
/1aMPNpchlVRPrRPjGsZBVwXcOMg5WQQYZMuT1j+mIuxd3DDZRZrKGwisCRf2ecR9H6Z5pk24vpZ
CHIIABbEfyGcWkuyRicH6cdEJ4uIyS+NzzzjBXSTs/QyMvH5k1C6D/BFEqwaLq+78GwXQ/bAfTPB
+DEDcDBD8FFolg26FslaD7/aDDEKQj1zrA1hQdAjn/9OUhQqaN8sVxKFbXKpLW0Lqpmu1ZtG6DFp
rBXM6N6Y9LepaFmamRqEosPMzhfFyiw9c0KIWeICptzUlUdnlLgwlDZhUXnayfgAcJjn06g55FNP
iwIUadHSLxs6Kkiir5zYRpXoiQssdjDKMN28LU+vsui1Tm/x7Qnltrq2nJCrkAUpdPqW3OCHMud0
cA99ToRxEs0ZYNrwdO/CfqSByRKyg9avabpVf2BOPBkTJXFgQfoBU8byid9L6MrLzNTCS519qmCi
o9u5drbaJT+eLa6NFqOictdFKQtMBGP0MLcNqLyRUIt+mQeZ10oig1SlHmPw8nMRwGCe/Knb6yFv
B7CYORZnvuoo7GIelvuetqye0ljSa844qPEpAFTY6IZi/6CX9WkkCcT79x4i/KGrP4U5eO2630zX
iCmfVYCd+2BUaCeHHvmL7aZd6zZxrmNWlCfUVbPPkjf6Rw4TA+s3TaBACvwaMQ7jiuEFkrR539l2
qjQdsHkkh9bMgBFDAvZIRyxqC7OQ2YkADfctaNzDEiZh44Ii1BmfkHG739lTscbc4CEVvu/xzBPi
SjiOnGWCh8sDNYpbznVLXPJf/RF0hGKPfXXR7w/hj/unRAGoBrhjdCF4tEhTMMJyv9C3qBmY2iOo
nt/9+210MO2Bc2/qgUQUXBeZ/w7N1lPcOWUyAW9CauxOIQ2+VV/aSvzzd2VzR8eGqmlbtgoVdrXr
YRZxxCgpJpLApsk5ZZ+2sY9wAiJHz1Pes6js/UfFVQJTbliwfHVB0smWFSpUk9jFgRjDbfGGd2mx
bcdc8ug+J3nTpqJ2VXSP7S2HU2Vk55zb9NmAirB/UKq0l7pGA463xgKyb/zqizjSXeON3cfjXT+n
it/nh4rLiFmbCYzkGkBjKVCJa9Tm+XbVMev3ZeJiweOBN2efsNZfA9bA94QUngX/HgWwy/TOf4UI
7SDCg5Zrf2JAfKEjtioKB3URTj6nmzQ0VYelPTapLwbD5XyUAqATJ0/phGC2wP4lMreC9oXl//8Y
bUzfb5yNJi4UZitzZ96O7uAZIA1E5B7OKlM/9DxJKdO/3mKibQn3ZvoU15Fjn6jWuYQ1YOhlKJ6T
bgXVnu0Q1sFqKSuGEDKBOC2KHhHopXpA8ZVu+1nbEwrNnVUqyHNkqdIeN9fQedG3lTadXns5SDAX
gk3Fqjz2kUcjlLrH4lITMwq9Qc8YQ/MoWW7LgVzkXtPxahmoM0VlgFEMvJ7R/kuZxBRd166LgCOB
bLa9xcO2Jchw1bQ+BKDVYIHlVjMttr51qNp3YdvfUDz62Q2lz4HHHHcV7dQuB/X0+EEbHn+IrcuM
nOwQpAkTGHMW1QHVDKuVleD+a4CfqbbMuLbFZlkICNNCAOEHBrx+Yf18f9kYM7p3H+5HkmNs/Fga
66TEvPgJdjSOEgjDsK0yzasiDAQQsZWIAJ/nNurxy0eqMRQOmxz2/W+AfxjT6QQPPpkemFRYwIMK
d4zNc83urzO1vglFVAf5DHB1MehKSaUesgrOn7rPuXfbstgF4TirBiijWVYx/s6IXrEogopFtnzd
L7vPQpFL1Yh3qpp4VZEpNv3PkWHFOaRmjxXc4HskNpfu7QWQmMs6IaVMJHosA2mHrasjwbn0JI9L
oRG3sdmfGhL+Rjs7Q1811kAl0RRANkQX4jHFndbAbaufCpzXqGEdmmJJYxg7ZO2by+pqr8egZqb4
5cuKkHWz9wr4o/6o3Z8CjDw24RdUzF9lwCOyvdSIAsbSKmP5d6JJOSUdlK6v75rRgOEQOO0zYXqM
t+g4ozqdDrgoWfWCYyGKvu78Nvq3Vm9UGZGkB+5tg8lwjqiCYNgnNeHLQv3ct40/rrAQq5LkMBK5
EvorJbRe0sGtnY+0dcVKe+/ovI7XpaKIdFbyKPcuiB33+/5+b26jTJmItB5j02njTqhiSG1KmsJy
xbrs4szp/1VCEIDSaeL42nDXvWcIG7zSdFDSS3Uoe2/Wpue/Q+TdYC/334KQ+7O3IftcJ609jLhG
9HovtdpTq0ql2U/9FbyEU2BkqbZJa7eSQx2Q5OPLmASN3yDbhUzrAqmeKBRzqP/Xq5iJFYwsqfRP
zuJRQXE+Xbu6UUiMkKwIh0cC8zzEulmJvauKc152SuJc5jBeI/Gmu90SefPJWQ9xOfA05B/heQNb
GLKmTPOJSwp+vb+0EaVFH7XMEfQF03UJxkHstpC5KuO9TZDTNqaicO/YdfDxMSEH4bXNH/bJF/kI
CwW5LsW4UHD7a1zPD+Yq6kU9EXztaYhk02oMdFYaVYfbP7FVui+jOGsnee6kfdF4cjoY++ruo885
AwvMDLx7I8ZVyhdWmnoyQszIVfstKR6D6HoJ/PigB8hBFWLZ7SLH+Nwjt0WBHdtHWaG93wcLvtSF
ETaPGo8C+aQpL5UZ91qcGdh1G49HmS3ReFqh5h9N2C+rrhTcMRploq3eCign3Bz+UWmVt5m9tx7w
OvOVhvntSjzf+4Y3/vJzP2IyjIGi1/iEI0NOUdK16l3hPOePM+l5+o/srly/XrV2V7qddmiGB3vC
BtbHSVPmu2ax4TgKusSNzIrMSw7nhhy/iTXF/h+PBqph4AGs1XbdWa2mLnFvsoqgIMXNVe2TMr97
yXgKYbbbFPbA+IQqTuNmGpFpUhBNKkqoc0e173Sk3Ew8lWhaGxQEGx9/EuCrIR3xsdkOKRv8lNtK
ZFVMlrrNPZchDzQc/CA8frjtRTcduq14D5fiv4bEfFTb8XkdDC6Fhgl8U5vqI3+4M+yeDSf0YMwI
xzC8nRxbhQMuxdD22JO830eh9TC9Is3hN1mWVTEU+96hEL6UWrZQMdLE1LJq++dSQQTDtVrOkWmb
D5EiehE6NS61W2p2z5o4KS0sfOLc2PEw9Tg8bgInpFWK43c6aQ8/pRp3Z1M9p1L8BVHXZibmc05u
7Ugm+AWZJR4I74iDHS97hf3DI1CgeguTSy8UBW2COK66J4ATvJ7FvLPGsBA7ee8P2qSgc6QB++ZM
Clbwp80Dz7tIzuQw8bz8tyPoWRpVPzXuUzDmtkrVIaEn6InCD8+QS3tV9ogjH3ANa89Wj/9nTFmO
maWL1pVMbs9d/o2oqwxJ0aumG5lJFBd75FkOl+pbzOhhvI6QZ35poZ+MT7iFAu4oaJdeJEN/veOJ
MCkc1dwheUVaXW6moOpWGgf0Q9vKeSXvdVdRforq9PWzQAdSgmUY6Isd+ZTE2MQSh1Gv04+c4fqb
FU0E6/ZnFu5RoBjV+FAaCgjb4do9wLpleHErEJprlH+JW4qPRY7udZwPhFjeu9JilWminKo8spsr
oZl4zwiHw1dr+OrmQsmqUZhZ9j/81MRwRVq+PUuQCoLfcFBcJwAX3WKJti0sG3dwYlvterTACSaX
y8PRofRmzT5E4peJA+Jrb1R19H4YyH3K/kqAuqCIKEB+Sl7o1pSPwG133PT8LyCiKepR0gfDndCw
dZhWC5IbIupsxipaW86Gz9remcS3VIgAwUzWbKMw/96alAj1q11ogAmTTwgc8WvuqOBE+spuZJpI
c80Hv1gc2QI1jHOhbUmPZUi/4XU03+1bFX5D8i3E8r2LI/ez2ltrE3PttXlvI04S1nFohpFS9Zff
arGT9OtNgxlDWrgei5NR3KeJ9GGME40T5IWukocWUqgNaNyNdJZTMQ7t2PullOGS2aRdJzMBF+2A
6OAYWdZ+IpF1lAgbWEYvG2TZZKincOvtb6L+FFAZGZj8CHrP4RIg/2HHfKXRLhVQYn78tgCfgeAn
c/BpgVLFxnSNmTESBn4TzubYyYtK1u/ApH8tDjA8NbBj5GDKWSQzqkXxEGyICtX+qRMOqRrAodqr
JwKOij1Z+PZbwIe2ytLmWwJsi1QyZ6ZiPfkYmDUsdTeqchpJFgsqy4+hOouZjnhFrFkIX4PyKaG7
2c1JYv1x2mCT9mST/E73UmPoiB3lcaRLo94xhUtKlya75X72dYC9yl2XAAZu2CcImpNm/6suGYox
69GyZv36MLl+O8mceWZQ/6Tw0pMquP0+mQg/7V91a03xOS7KVrV654s+um4BwAL1V11V7uCCWpXy
tl2v+YRmlJzICdsU417Ju3GhPXryNGyy/u2ZEB7P22tV4ZD5oZVsCK3t8F3mp9NO5M7+j5t8dE4Z
d2vbP43nK+CPA/P6r7yomFWb+2buWktmc5/VJ31+/qNIAPMBNnYdtYK6/sBtYEF7kV8w3+4Y8tB8
sF5Y8eL3TfQXgZPe9Kl25Qpbn8S6PrVvOdndMsYRnYdggAnNGlrKzn3Lzyvd9XdZtN7P/JxqHeMN
3U8pIpfqZOIWmfCt3blXkX3tBUYE5XvE+gZ7FGgI2nP+1V9Oh3dA2uMdU0UZaaNP2wtlbOu5d4Yv
H42IlxFVSrwmJN1PnMcSFmhthAUlUCZwrHHDWq1YX86OqRG7e+PR0odim1mABcLcRnNCAFXiUHpx
fvoWFHI8utcruFn0pLgXtie9YvgqaY1MgurNU4OY9REUwQDUBUOi53cB7UgoMY4xstK6gJ92ic7H
tYPFZp1uHZGRr/HBgNMX0hpP4NxXpcDyWNMVaIbEVbGx2sGC6VBkDErRQbO4Kr8GWrxrGrVAeSQE
/12hXibU9fYmOwvI2GdKSnrPld+fqFCDWn/wVWFT1HCOAnXyGf0IoO3srLgJnAVS/8uM/z6t466h
MN+x6+d3a3wQRVDCTAz0d8JwT//qughGgbIzsyQjgo4Y10TnafHwmC6C2Va80tyD4meIix7fcbJM
h/g7uSWomZZ8iT4wRfL7Oy/D4W356rp6Pq8h5KKWmyAJJA0uqQ7DNFDJ5Bbq6XqD2PJC18yA1D3U
lUpGYsVGXiyELOqddxliDA62q+rcAmwUSfJdPpFTnhGeZxbiRHNjFRSH6jXyZ/ESyxs0jdghxubi
5YKiQrx1QjD2FRWdj/aktUahh8iI8ZI2f+socPapHKYi2S85oBEo5nIZZpe5ij44nqppnpMOFuRq
uUS/wmHmk9LIuLZA324BGZ3h6qkaYwn2Jg3ajQqGkNELC7WCQSiMTc3liH0DRjFaOnSkfaEX5/oC
Sn30/LtJGD/931ksfDADI2hnDOzzYjobI7AUFa0Wo3YZaQiiieqeH/GcIItiLEQeJocs4AONqD96
A99ZoEwy3FqDI1SbH8vhatiDy+4LqJTEvtvBuVJGO5sZVGpdGAIB6P3t8bHQxJBpGL9ZM/tB2sL0
wqgOOtkEobeypATttFsjioqPWEnBWkj61ZAHZgLpD4jUkvDhkOAgp+oabY60OG6pDT71sK1kja0T
a3AkUGTvs4/PEWfKPpeuZ9vIqFTCWZnplkunx6r6uUGtBtGc3Qw19IQQXEBQBoF5rx23Dk9L4rgq
63C1to6hp1iGh9urGPkGNK08L5uV/3CTIC80sGH0lmRZusXguoUqPITyOVr6KOz1bYO+q5RaRJP/
tODgSInz9SlsaGKz3C6yKdjlu+Pu6KAyj1IkdGph4Hm0HwPUagYtaAZrs8b/aLSlCKcIQA1FfJxO
JUglyHTXq193D7lfqRE1O7v4OZ2lUBAfLAZ7kuUKvEWGCEvabcLtWjpWeIm/Z2fl/lpr5e/wMlFM
ZR2Z87CDcF8mh6BrDv/pferBn4SUtA4OJ51f9DK3ByaIJAVysg+2DPC7OU+vnXCqQ5epWfuyIxrg
r+NHqgmFFVQYFoVX85egQUs15k/ESpZLwUA90d+liZL3dBv/Zix11QcY6pI8z4W5wrXgulYK3q4M
LAR1X44nF+A46r7rpPYy68hgqUqpAaz0bjtbK6W5I4c+omr/S0qypBJyheB8SNIUV4eodJWykvgt
Oky6h4jPC8Dd+5JN99qC0rQOjsEO9zNbeLYGpobe06QpoTtZkgqCVIX9Rv97TKho9m4DGlU5qdgh
RrhZYSIwQyf7jSFeta2EaIoeoIBCUNQXgAv6HfXYC4kQNbio/+vp0TFMFcWoB0CTx+zoLAcqefCe
ZymiqZdTu2gvftK+MM5qSvKwHem0TG5GxFNsChQD67bfp38zof68l1mvvdSLL3rTaQLUGgSs95YO
WxG/y/v4EHWUWxepu8fBplSSzYrB02Ex8Pde/ZTGCwotXmWX3AElwVgCgHsbm0nXrPL9WpZuNnd1
opcGNB2uV4m9W5csDMv12K2MqDgj7FGBEOROwMWBgAOck1fm0Gvv6fs1foZKi/32nGG6aBT0dhTY
3aUISPRDKOxQdZk6JCeOWNiJdPvp2hOmdkmdUY6ZenLwTx4z22it4a6JkA/CC/55+Ghde2OXjEGa
sQw8TkJjUBAtFrbKIJNYmIfCw05CnTAPccyYdxJ0cJGw/bBzesNTLtp3Bpfw64m+zY3jwoUmRZEh
RTORhY7kgnZW0IyEZ6shI/qv88pp/JzEkqupDm5nB0vXn6zcg7tJWRaDVV0DoeHrrdPR5WYgoTL4
zDQQw01Io9M5hgacKmfzT4dP6UNCgz8muDC7guqAGkUaR/vYoyQARDQyKfUg4MGwVeu6C0kezWJV
utp3v8HAdfDikyaea1LzO7UbrjKsIUVldRxeeMFG9rVexKtKPF6442NixmGH0hStnLZccibLGvYi
gcRM01ndOqKbDD/pOcBtcGoz5ZhM2BSLdXc1iqWOB4u4/a2vc0zauS07XPukKPTtHTIM29wUGHgy
+MiYvf2HXKMYbz+5CyymwYoRnXn04taSKBPZFm/XfLA4vrgzJ5pPuiPBo/UaffxVlS4GY0f9aMrX
2Y1fl86A08YY+Uzs4krQWziish89zH8+SPWNP6ineoxSFnUk+zOUcSVjYuys+52RbV8ROyTP+Q6n
cEZLaL+8zlzqvqI9mB3DMRc3ddjKNL7RgODtZfc3sGGtlPem6UqRtSkMBODuVrQ+O3adbzCOgtcG
9eFUMHFn8c++mab1U3RNjfUlWdbi9gb8/UTQKFl/PJyZ5sQQmCXH0JLi4k8l393UbCZnQBcQOnXs
fYugJJi1nraK4gX+QuBeXtp6Vs/EVMIFEJ2s2gthyoxjVHCpx4srYqb33RZVl4tlQzsxhTwKAZGz
QqrSiLDavK+dGpBbcA2OvEQmDehrFRinGnW4k7FHr15TpNsP5oMy+MirsTOCm/r9JKbLYJH52Miy
OmxvcDykyc975Z+NbV7Jt3cKh1ONzH3B3hLAgFpOlJEpSJTcEsuIxCvz062LDdIASOdZQD4B5IlP
+nl0thwx9OkLoALuv/IpCwqKjKNEDVUXzu8+XY+OIDhQQQGc+CD99ecOScdTRzBNufvC/EGLkPQa
hTAqpzLQMHCtXbIIqTzltZyDNK3r/blcnMDhgqgWRcckI5GZi3R62ZUGGRSmTw+StCTAAsOFP3xa
GCnBqN8a7lNoWrw0zAi7+sRKZlOpY5bSGQ+wSkRtFWMTbRf/5bydBtprp0SKm01Zj2HqjdRPm/QV
o8+yrH7gXVNDLm12dRIzeu1v8x2lhUNE4kefl6+PeHkYm+fwcBckEKYgm3Ow/lw42LViJHloXs9E
P6DL6jY/L+o+xjLkSmgxIZ7zDYZOYje0+V4Px36Istdpa3O3AbcH95A7Q5zgpNTKc+ylwole7Xp8
PqwLdkTuJAu9gA2xtMTLpKqKS4E3+BtKJYudQyKwLMlEdzdiJCQk+kbS5CGYu/d9p+tu99yN+mBp
1QKFI7fGAB68A0X7SAX10N1Io0YHKeBC98f9EHwvTsmZCAsCzZQIfGlXMlc9Uff6qYSa0t230joC
dwaLqSzD5xQchAcK4RJbaJUkd82sPBmH3BbLNIMQcQUtl/A7ogczUeVczMP8uyuFNfQFh7bQzg7L
EprLxwU0J3z8eRo4J+l1f+wuyahfk0S/Yz4YtMRAL/1ocXhzb+0puA5WJdIn4C7fR1nT5jblF11H
V9xOlVzOVEEhiy/Cdaz6Ss1WWo7j2FNzzl3Xehxr7RRvsF7dFo642vpWM4qhiOeX27a2xf67bY38
pXFqG/Bhxtjafkw+v07YEqCYI3Myfb5nTZoGGRTFS5tt0fs8lQ4VF5sSsRYIctWQM1xzfpZuK3On
pAwi92cWuhiyWxm0r/EJKbs73M8Wc/aCvvXK/3opESzqGBV8FJam1VtztNGIpugt40l03WKrS+xc
zqtz9aTP0Jvyrg1nHJeaLH7gxXN49OmYzp3YmTKW3djjfKPRnPvHWz6L+34CcAZ77lVfE8x7dot9
9F1pEZXqXBrd4gs4ZNszbYZGEBAf0JUkmvPy2OieR3vkgXcJSuotD7G1D+G4JWMtHtY2nFApCP4g
LjNEUMGxFELQK49dkp8bVtWPqqVloAmgumefVRINUpz3pjbGrsPpcKQOS9l9glBoN4Inbqfs8cFI
nqZqyo9y0wcSbGYyy8vUtO7s/NdFRXFw29VdwNJc0YSlhQLr7ZZTu4/1rVbKLnPGQZ2c66xKknj6
E0eZ87xlBK4ucEsJzjzeDphNBz3U26YSXk3IOcOdLQ6CFWXr4Q3TSHTfS5pJu8XLIT2yZUdZjOJI
xbds1tYvN/t0tnRCM3/eqFLsLci0f0nn3kV99fHWNCXsicVcSUgN2eqd5afLzoJfJ1l0gIoXeL3j
lK/IwLuddxuVYZMWzlNpCK0MSOys2mSUfck/BogMjAzF2uyOEFmZ4jKFSX5y2O5ZnPtGOIgcTWhg
QmDCdkh6BpBSpPlGZlvaecr+aW0JIlxx1u3H05jP8Sc0VW4wx/rwOo+zHAKPIOaSkFaKbrIIJrdr
+vDCT6XsK9GoXmtpsaVoLrjit6F3xHpauCkwj9eC5eHakBjNp96bTFAa+QhIvJc5QuPJHLD9wfaZ
zlWg18I2EMEJqGCsZPjwtNGbSLfxrkNaJ2Nd9pEWP3K8h8zXO9geWZcIrRakS1WvS8ZW8gMPJOv4
lKaxyTPdXUtRQVylJWvM4/JWViB96UT+LqX7sCP/H3sL+xQ9CMIR9w3vSs4x0lkYN2W9xib0DhA7
vU4n8e+KeMjZSeQKxTqIYg8UZIjOh8tAJGT6QdNRj8OvNoepvL2wZJpHnmDoUQzj+GTT0v+fpj+3
2F5CJdvHUn7b01nuUJOScmvd60qjLmc0JbjxP2FLqa3B0KckHW8PrOqaqRU/7tYhizY0k9kBpnMr
QywkwWro2DQtxYH63lGRq1XbM7VCxfdGgAmUDu1dNppyI8wE//+NPq9cMx/RWL/GS88QahqGcC99
/QkTMuf6kUJwBq4ye4gmwo7ZaLJvsKo1QhTYZMT71QxirgOna2n+zSHS1rQV2qC7iwS5jVw2nFFk
PZyjYW2P+lnBLVs3qm55/C9mfQgi7kfID60N1Fwn2My7iS2dta2SlBvagIr16WAFCjH+ea2I1L5T
0WU0aOZ2rqUeAo0EIhFp1GQ/LmSEAxBoHSqkfMBtNC53ArWu7K+zsS5yHeZ5PEzK2kcS9Kv412rU
oqbqxOORocmYqZsr9IzEtVtzfY751kk3a+dTls5xdrtU10DiiAalMJxpUkTXI0kQ+jClyn1mWh/9
kjIvopOJuHpVwy+iKky47oyCEVXerZIvHJk2vRfwGnRcrkyKcpwz7o1G0RZIzKEKEhfbMcx4mtqD
o8t+GxElHyrtR824DiIgC3fZokUI+xJpNKa6NLKCZxM0tHQQOpU7BJwb17WDJXebSa0Mv7oYwJ7y
AqjozNiu2NKJFvllaQ+NVF7OPAC9gtXIeQTw2xo22TNlUFYKaKcg7P5Y6KfOLfPmn8onsM4B9QGb
4/laKwiaqBTEylEsGfAV8+zzSaG6ntg3n7pOY93fAkKZ+m8CgIPTBm9YKpEHsu2molaXUFyP0/CE
ZxlFpXowpEXHnQasxdis7owucbwgrJLaZJ6M/LYrt2fGoeMpcT/D/5Lq1ETukfBiJu/PCYU0Xe67
TQybfR0tGKZKIVCrvbgk7uIBPf5fmiKBIBjie6L4Zqy5tHDm2L3iRxQ/z5XvrF5SZ/1XPabkj3Zj
Wpn2Rup0GEkGBrWn6laxSgh+dz6WT5pLJLlEcKD+/yx1HPs01hpwLKQ1ZiBoZpX5nQnWFPgDZOte
km+dZOc5J3HEc/2NNi4WbyruHjDvTt3X7oL2FSSL2loYhWckIrtyXgYF0JreKuUADGITcg8cpLa2
TVP+D9rnVjXtYT9/NnUYGGbDgfYcAATq0F0DGZO9MfSu3WXnioYRzbqO0WwRejEOpiIQElHpkPnI
IPiMNzShNe/eKMPZgc9j2Aqns9MZ3tcpa57J3WoXnnplP6yN7kBGBZ0wrmcSP3wt8HqBK+xO5Wcv
O8RWCtHTW2h9CihfP9jf4/Lz8fLqnT4rLPleHpdR/4tgyxneK3jzUwuN6TGfmY+LCRt3S3NfXD9i
RC78za9bW5orDzV1JXPVhbkPW9ppraY2u91+hKS8dXx96PYo3ln/gmFCFDzD4j6kN31UlVw5LI46
G7ioZypNq2tbwUC9Fa3FGW1Oynd63H3D52OFFpQqlH50kUMGyp/LycJ6Dv3uxyUQ9lGrDhlFmc1x
JoTvXCyg0MoTNVJHQeq96vf1/gvjFkVMCKu0bRSvlkga90nDUtEDCH7lQiXf8PdGumydZ8up282U
/eeWYZy5FNlzpIZq7mGm52IxVogszLweSoixSYXMmNknP6PKII/UvPLY7k8xAzcha7025tzLMM9U
5N4qcp49Hng5QBz7CwQQY95i03AbYTrOiNIW+oIDKMDfB7pbYRrPcg+vEa4ms13XhNaGGw/5y6BE
bt5nomsNC1kleM6lL1gmgqnaibdzxAdJKO+c0X5JbgMNHK+qB6rL2Hk22IHr8AO9QelQzz9rKq1Z
Ch222He4pcRx7rRjH+FvPM/V9KCWVcRP6Lex99wUR5TVN5+BgG/Sq0LxeQ1QBi19ZZ3K3dLccLb4
JL05nQyn3HkoY51t2pZF9jH4oAGahBgAR1fxymipEXeP2vXDD+wtqFztJbB57r92+++DD5xwc9sb
Rllnp2bIT90K7NL4pXDMHwYDKVmmGupPO4+4CJcjIq1ac+Y3ZjM72n+phZwj9NYZQ0+jhsge17bQ
GIf2NXNOMrDTdi7TrrkIIpdiYTvJwxflmxNYyXjp5zrr7MguJA7qFJgHmyVSFp2FoIFerxUk9Qy3
Emnc9Qqqok/PnJYzdCSAGZxhMKsRSoGZfCUrk6CDqBuC7gHbZsN6zRadIxxOvvHBTov1HWRR0sE0
JKqqe/GdhVpz9qPZ5CGNCfNbSxat5ksG/6SgCYMNFWNVtnEcmbdBO1j9QW9NTCIuGNL9+50qTyTl
CIaZTBan8irilTaV9ckuYrTA3YDcWLUGAbk/uQAhDHhQJObc9dzByQiW9ye5cWUAzIluVggxiwcb
epSvp8XwV3kL7kd/2NN5FPKdbncRKOs1t3TD13f/6UVMZG45HON2vVJ7YS3ryq673jE7ywDaPn30
tFaZ7snWuKTHbHynh/Lp2xMLiWjzyYTzfXnv/hZoEY6wc8ozu0qyCkK+hJcGbjDFH+miOCJItLZi
/2GnyYaaYog4evteikGNIiX9pfAqXw3KcYx/HoeDypLWwbn/AJyhw4QTGWaqdPA2jnT2zMH5UNgv
7G/RH1vSeuVa+6RdogaiPvTYmdK4+HaxkI8ZXgUQ+9viJm5d97xANTK1whcc9Htom5ayPYT+wXOi
PRhnpO6LS02ayCfxUQfoxngM58XE6ipUXlZczO7K/zc9McJmAaiwBizYfG1hzOHYZuOjGtGjBGJG
Julq462TIa5+LmQIaG4mnCyFDhfbhRuDBj4cFFoYfA7+cRTzRKDuoD2uzvoxpCkMq/OKSJ8eM/hU
IbqWmHi4Uakvsq6sVbiI7HT3L2gghq+ls9gjdYOu+OowkD5c6a2YbyTfmcAOctdMsesZVjTVkRz+
kMmACtESYX/VKaRnYt9knY3adNKRIfBmoGCGHQOwHeB16nOLm3kZBNx5Ja/u03xyb8mPrWAMWmtN
acHOHSgeGJ6HxS4m4jLpnhVKmN0n7/NNlofXxvgxxTHf+j+13Qsh+4hQ86Yk4zPyn6gBVI3AdNIp
ZUhmHGYEco/agWPDPsueHeDyZfE++KjDuFEIye2DH+NX4yosfEtL0Fz6uNJbBVDYecwQy8aWoIkI
T8ppv+j7sXfcGY9H/kThfXTFT8kDWHknpGq6pwFGXX530AUZeYdTyFLYMRoYaebzsKmG7HkYGMLj
zATwzNmD68AB2o52LEZVhcDPL4B0tWiK0mpCp9GdCBVHe2JT6vV5eT7xhM8OAbKcQJAhTWR3j8FA
fqhXAuP5MMMbiouUygteil9GiVSedMW7H8jlV0XxeSvSIjpaAUUV0EvbZsgI/QDXakitTfVLnH3Z
267alrZoqK2CdO4DYiLQk6Xko+FpQ4H7Tft+Y/qVODskKDvFlNEX5u6gxgvWbYdqNvut0pnWxITh
qh2SNKjk7PbVwUoOoijJnGTl4KdyJHq8cHhH+jUlccXE0Ee6YqMy32jNcHxjZ0HVLXjToqLiyqen
soyy9xERmGyZCbKqFgk1vHWUUC4nwo3N09kzRj+zlEdO6xuEMzKeGamDp9lFmF8wQCgX/HVFxYwB
3QurowONnkBV+gx7zNUcxW/2ONgYEeAD73jSKXjjI9UBVSJwRYnWnSaq6Z/yPbfqLul5cxTiCBss
FpT8UoiJLIyjmYXAShopn4PwU27xUyL48CAcvH6DLAXjx03vPkzsYWTa+5LHzvy484LCB9AR6inP
DwihWetsfrjLmxPUGNP2gG51HCH6ZxzPzi+DS193L2uCUA9fvC0hJ2okmdJnnOx5Me26/eoftHmg
RNfLnpxTipeR87UUvjWIC3lbxocsfU8DYorWDBassrWoIpLonhjr80ffFp/ZvdfNZU0KN1wmzMeQ
ue6XYgscgKbry2bO/EJAzpG+L9lMcEkvBsUeO7w4aGlET6bBJB70yZyxesD0reEYfwKaKcUSY5y2
vXPyfWJFH0uxpALn39d7NDTUp4UwkmcLEL2WmHWvEwfGyoXYQ/8uEQ9DFfu7PQ9o3DZv6DcUbzS1
rytDt6uL1P6vJQLPcEZgrq7hYrAzdqCQCIL1LpIfGQspb4u3S5TSSjEzn59MEPf/1L5Sl0td4qy8
+PGTszNT+pTyUnH2v62G/tjrckdlv+ujN+ztqlwplv94zaXVssL00quWjc5UWb0UdSbnxAyWB1e6
+KOtxNFfvgHRWzBeZc2+FQINn+gEFWkuyBuTTkypCJk8ef7lrJOAHyoDKH1gjpN+ff0HjPex7UmZ
brHXNCc6QYDLxxpcecfWo7SWKsYQ3K0k8Fzy8578iswmvxGHkDvO0siuDWWbvJR6JPeh2SYQiwWj
j06DAUT3RMcJWLvfXwDOjUEFWLXBWGbuU6Azgjo5qx97NF/OjvSos7MFpcV7Jqlejg+ToNRD456n
YjWPf2dR7Sz5nFTZgXQd6Ie7hgvEWdlOdq5agOKvC7vPasB+52lTGwgbaTc12qzmwbY8fVSqg0o5
RjRO/xGycmjIpYeI2ixeIvuv5EPvFNxRe2OHE5Q0YHyG9Xwbs7Gr6e8hv8qbmk+IGZi1r9SbPjLZ
Br19qrQbz6HzuLOgx/SgwMV5vclC2QxnLQ+uIhu+GWesd8twg9UzWCEMdXZkvB56grdsgZBglFMz
XFbZ8hFB1B8d/siQCOLcUy5hZI/j1Yrwr1CP/B7FgV1Dk04N3jtK1pji4ZZcz7Xaznh2muE/IuIh
JvAIhArgejUnaEjOY3DF9cIfp2lwoWhslXAsPoTQqNAj89n0E5Dbrb6dWjrgH+xsprRBduBI71vd
vjH6FD45u3pwy8IJvhBDzGoHoH2rRYGdUWw1SmcnOzWPDhhzpiyUqHITQi2Qy+AbeTiNniSGuNC7
fCpj8o4JaBYpxhu29Me7Swi5vCFpNimzxrk3RTgRI2hOPhFeCdCMhXyjwAPPHdpU6kAm6b0eass1
auNrgZJPH12L0qjKGkA2duVsvlNS5dmAPow9yG0ECR7YIczucGzPxfVJWoR79sPRxP0L6fOZewYv
RdCQnJYTuistWxWCFYjV8CUCX8T840yYFNedQ99hFjk6xFhGMwi8CeILsxDvrIf8lm8jdy5CIbgs
VYAERUc04GO/zqX6WhI6NXx1yTCqP+VBOXVCPCKYdpwwnn5GorO8Jb6XfZPX5Cor9EheWzvYtP/a
vd4zdUXWwWKznelmKzva9T/SNMHwKN6MXt6tdDPvCunAUUWFZIE/yJPWI58ficBdnQWB0awG1I+I
Q7bojody9ry8wRreVHUQWtWQUhlpewpiA1WkAGBM1Nhz2vaB+8dnmM1rYE6jV4aiGZB3GB152hiW
ZdtUZj6XLovRXDPp/zJqsjyhcUNuSL0KaNOFVsRARyoFHc+9IEwGl+JrmWjyKvsS854e8az+heV1
r8BiTQz6eTvvp55zVIcf0jJd3WGExbX/WAmPq9uHnS1tc6+zM4y3f+zGqFHM5TAKxvdcjx3utDmM
GNSbbNSYvAnMmj1GEBtz/PnMsavW0wpUPIPrt8zwYUxapG9zfXG4puEwC7U+ZKJ6/zf2lrqFCsjk
h8+cEMDDhnqk1snBAvKVNocJHCEs31YYyQCxOdmhTzCqBs5+Vh4B5Bzmb2EdYgwPTt36ZsG2hXM4
UoMLRsJZModNPqe5opxDTNWmdonOYn3ChBVxgpM0SxkXFUQtbUgquJvIttOlq2DNWuTYvL2KxwY5
+EdiFbjJtkIe1i9bqvhvmAxYpDqR4HYBrgjEksVuBueU/UtyjcLJouHdrLfV12sz6i2H2DTMANIo
bCUcM5dvZuBkfsU8W5ETXqdscT33ejPAWaSeL33nyhkDUUehOOz0jyyk/tJ+muMSJEU/VtTzgvf2
KP4cPkdnk5kfxRvh5MdyxWEKwBc0FwJusj7imSUwEF3OsOKdkpoFIvaJXBBSQ0CH/HERCbG+sGLL
h7nnsT/51yLleHibaYzfGAOOb+OIJ3yBY+1wuJKduiKEV7scnZtS+DFlZnCQu5OWWE1lp0MvtenW
IgQyLN1mhg70Rrs/wrSlPPb+oga4rDhielDSPq749JftN+tyJO2kfnMzlP4Rd8/bFNVe+4eXDcc3
Xe3UdNB3+5Me8pb38mh7Og0arSqnOKdypE3Y3J8XAc3tgceJxuRLQv6RmRGfh48krKmyjdkgzXzf
vzA+NwrOTD3AVg16z0lRa8Po4c1vEhzNJG3KFuW4LSeDqz+dcovHt79iDPAzxlq6ZR2fW3kpHMrB
8nbG4HryS5Xhz3ImQ2OOd7mXVlhSH6/YDYYTVYlanc/OcGlnVKCcbJWbPOrMmQJ18GC58kagq3CV
4DDaX/bnUoojy0zQEgIaIFtRi8RHSBNzYHRcf2Ey2YMLuasl5/ZwMQ+ECTaWh+SGctd0ZvcrCVN5
n3vIzFu0Hsk7y+Uqim0agWyAJtLnsThwLCZ9Xo7hVsm+1pddFFsEJmyuyGyT+74SGny4XONYLX9r
W8aOfCQEHciiMM673KeFgy4j4rplYgCckIYvT5HjNmJWpG/qjW/Dde4mMTW3gaKjqOTelA7z/bN6
72T+AtcPg3+go60ECLQlyejwtxqdwT+rMfiPiEjJtjoJ+7hvd5fEydZiw7nu0uYUWtSDavt5s8Cw
2WqhihblkIctobQZVDNtpm29oieknXIwshrLKNf6XbqFJnX9yxY7SBNZHWzBfIgWM1TVGV2ZchNJ
gaGhRFBzK/WMx8UO6bKObIdGRZCSjgIeEPLPfcbv4z+/ttCXcAvgGd1MXSyEnUftbMm8rwvXTU/6
6F00FHawnxnDKg1/MHerz2QurxbgUoaVxSR1wD849XRoi3dRvxtMISqQvd7tuUigI5TRB0lM9qpb
fFsCwAznBufvBjt8pOTKZZyg373od46hWlz7VOnx9iAEnCai5C1QaNcXQdrhEMypIl0vEvO6lc5O
sbp10LeRjVAgEdObAqS29f76he5J8xYADz7pphXEmvkJf1uExtY3MDpo0zSjOrLHWDyDcjdLRXpt
hA73YvZ27b5WrVlne8tRQaH/vfZo6ZE02d1S/ASQFblPTK85j4k/r1C8Gpz+Ax70rAjd0rc5oNxw
M3wpxd2BWvHkEVeLN5bWyzgB77AuPEsxnIeTopvGvjemiZ3h9VLV45mMO67SKXamWaARzvN0pngJ
G14JlOjEpRzYnBP0Jy55dtGRg4FW6EilX5IgeEPrfDbhwSobWUaY5OrYCAra4dO/5p8EpuhdC31J
5AZrz+8cfGUvpZH+3fdYYuO+E+loYMaauuh2OPJlzNI/Xl1mH83x857kK6601E6dVokMOT25w2Pz
G+QlziP7UlsTLI9aTNt1EzevOIV0yMt4MSaLJR5ucP5VqwdlxGybQZzraHWUf1Hgp/mQ9CdkeBfM
xS0o+o8KSfFZ6A/etCyVkulW7izJoF+eDMZb7ndixi++CaKW+M9dcjIBOMxB8OH0PcfcogIhV4/q
8O+MFcT6RWjNRkx1rdt7vUnuv0zBUb4DrhZI3UOztOHrO/N+rFDVwxrvtRF8NyB9pBR6A16dPEEQ
ziVibYlD7rqEbF+WJDmeTh0PfDqw0Oh8+I5fwa0UsZnxD41Rkp4/UKRbvaq09pYS9/zbTFzM2jCW
j9YGOtAE5ak+G8f4HapYfX9DnEM4k9JRgtWTptAo0d8KS3i+6FUt9KvA5e06kfpI6s4gtdzGSXbA
0152BnGd4r7YV0hgd9d4BJYZfQ7MmygeZcDVBwS6NL6yKkWGVetRFt/D0J4hZ3HWLHqwcU6G+5cc
/qNqkPdsz3OnY9i+KHk3N1dHu3E3/jMJ12AihDhCzRkeSYcwN6ojmzCfhbSeVd9nplRTGHZJqP1A
wqlC551YhycndHnl4SkQVj4E18ai+NbgMIfGCCzkdWXJ2CGGpU1mqQX36gmewLTMSXQzlWaOQMFY
A4vMBM/eumiIl2Ipqv01Vv3DI/J+MhzuiM58pQ1kYS3PFq9mKBa7zwi5Qt8azf1Pejom55DZnnmc
pWDR086tnw/iaFT0Qay0U5RcGgjGda01wn/bcstzsQJv75JJtBD+r+zPO9j0fg0FkBNoBFMyDWuQ
KIRSxy51Kdbz/rNR0Z2LvGHiEQhrMSDJFEHjOaV2H3zt8Wg3R+0zaKXC1nVqhkL5i6Nzb9rzB/Vx
TWWIyYJqlajbzbqPX4qDNjMXjojSxeKRzcEZUGUkQToXyaSpEh9ch/SocGz/B8zfnUNhT7L3lBe5
uWl4cpMa+ZDvYYkFAEmcsSCTgGLPiUJwDRJmGf6ytQFEXpwB3MCoe6R6YF9V9zSOUl+N1FuyJqzg
UDHoxW1iS4AFBfvTHyynTY3iUYTPcUxrUKPUc1hK2WWcuz9CcV3bV834V/idcPOm4SJXwYbcAeRi
r+q5o8q/bIuzMv2gAxY4GgN+aWp22KYZG7uFPJium2V/57reLCj+2HGncoEfgISyW2A/hdLhmekK
ZMoKcJPcqjoJTWFSzVj0lZDkGos1lomcpvEew+e5pl1E+nybcOYrvR6Hq/qNff9SerxEp8rhtTf2
x5iRWv8ulgYKEdJ9/L6UyjtqhfLT2c3Cx2hK2vgLQKAJflAmyYgMVMP+gxNYJHnCcoB+I2H4xPc1
VNNnO1fC2uTCpPoKO0+fG9Ud5MImcTtkLY1Fx92Rnsn/bb1Bzdz7xzgQ0Z5vstqbOiXyEWYtUMZ7
mNU9aywi6JnMtdI/uk4/CG+E8T0ZlsXXQsHJIj6P8+QJBHbzIC9NNa8eflvPxtGhjNxEej53+aYk
OPK8xrSQXCZqWIfiv6goKMvnw+AwKZhRsg6+J93n9C0Om8uMXRkWBLuWqBusx4DGKc++TbkA7/x7
0ffGHcLzv8Rlxopw/6Q5Neh/9chWSZoM1F7BEae+uy/Djq9XjQ2WIcdToY1UnqTb/VkNLfAs14VM
bryAqZHwrCTyBpC05Jb2a53RdvODWpRq9DEc0cQU+MNWVxsV1hOKPCRw1SXZiGnyaEdwRZyRJe9Y
pm5Uuyynjwgz90VY8CGI+ffLh1B3pTRIywa1PdSsg+M02FnuOBHSQwqFNdYq+a2NqkRTuPz+UM4Y
o7yuwvalzEh/8BpdGi6Bj6uZUyWT4Cb/TB7uarOqyrhidK4EDZCB2dxomsJ89R88GpOV/BLMvXLQ
4iEwp6yWQc8OIPSKPr0wwMsmykl5kcOzAt36lZBp7kaFGcgBXnrLzohDDo+Ux6WpOmR7WIVlZoiv
QwovAthig4dQZ5Loj6B5om8lHb+0yCoth8SX3th5seQHVN6SiH4HlSBypwHLeMVsiIRs/65XotuA
1hfLuHpvd/K5gc9dTuxKz1wt8z5CWWtKjbTB1qJ4PB9F6jXqMQj44a7ula/4GoTRAwNZ15QvlXwk
azElJTNlVMzi4gWtjYWu5g7SLBW6LJJwxy29Le0ph2W4EjpwUSAZrgcS6yPb/yQ2OdZsIW6ok57P
oD228Y5PW+CIufsLiLIBn1/WCx8Wsmt10VX/UcNMZJ0ASGdurvf7CUoOQLxjPv2dnla/712wv8vv
i5hi6/oP4wSmLQ3v7J/YOCTwavTbKZoIxFAj/Q1cOhQHqzn96nh7xVE9uChSHOQYlJtVvavTt2Gc
cvRlGAi9yqHlsHvQtkNDTPg3WSdmniEDQGKQ1d86NylCJ8nhNitdUBENhVBKfw9fG9WEbv4EEgHR
Do8OH9A5sFzaXJcv0RcKm3q2lX55dCFVhapXpqSD0ChXmHAuYKVFW83ouvA1h+JzW3SDcUn2fFkL
ZWTgFzsYqbjiIFoPuO8BatOzjRpBo4phcBzqAOW/XXYS3BiyFV838hP8Y/2bUXwQfXsQ66PQyYwT
6qndoBnG0By22ifb2g3oLRhYVhL58eXgNumm1bS9+cethHdFWKu+NqYAUhXxzFcZXveRhIW88oqp
R+UUfVZz6hIN3uharaPX0d5jqbCRXxXh2zxdY7Zd7eAYwl3kb8qQeYCI769sRxjxGIblCpE+lnfj
TFQPV5s+S4d64TjUnZaJishwVPHblxk0pzTuMYfXQULdNCpRGsYuWxmrYWFBgfUS1XooL+jXbGXS
4gZQQgU4CZiOQsvizE49t3rmKPez8TJIcncG7x6D9phqrvkQcjSOCnvbVd8z2SkucclZ2RTdbsst
HEfH7knCkgh/W/pBJ7JVaycWOdzLvnTuuZARLvKDzWlXc7fHsmbdLvrDfNVKNXJVXJvnSrstYw+/
N69S0Za3ubPlRb3y2NIN8LJSeP0cFoLRTEK+XAUGxHiX+RT/e7PtpV7Qr1zCIz1pmSmEs/sRHRaM
mDnT0Y5fURwzHlkFelqE18iZT9WJthyCUxcYsQw+O04E0jmKlS8/jeac8HdKhwLrI1HqCYonZa4i
xhVp3BWEvWZ4kFtUs3cOy5zxo3WOjhea6N2FJ6M3f1uN5IYAG1EHu/102lP3u91S5r+tNMT5dCKN
sq7SbBK3yObC1Y38UEv6tkK3dLKUGSJHAWj/7gymqTk9W7j/ZFCcGWjKO46HZb36a3ZVcqX/l95J
iFhFr/+/R7DP1xt4+uPMyOQi8SeMnVl6HVmp6aZs4LZUyUUXbzB+nltXVxGcwsDXUU14NeaddMwQ
U4XZc38HS/8I1PvnnAGSd3wAl0efEoRbnNcZucT7+QJ2nZ4Fv7KfkP3u2Lj5D2JTn5ZncHc0Hn8G
GUFu7QPPmKQIXLTM6t1gZqLraEyztSl9vosXgBORWJGYmTeCBzVZ8d4E0P1t9JYUw9Sp3u4kgAfZ
lrpKoMB3bK1trulANv7GFwERuAfjMMEgXbaul5xY6AUJbkDbo93q2j3zWPx13qmk3f3dZ7oKpoYx
RyXDVPRENbMhqfGcROUt72pTlstKHmrMgjCq5HD+n7UrNCkTzR+Rl1ORFZ+54ObLhrjuUk30BK68
EyqZCApAEVpI5pUUrw91NfyLXAjMFH2sOEx5IacXJresfsLS9Oo2hOHk8HfSEeqLJFqevTAXMWKf
W6NMIz3nB07a7Q4TlhVLhuiQZCClqplv5Ecd75JonWDcGcRivEwVKnpDlvdCBOYQhoChBKdTd9a3
cQ8lG+nLn6eGrSoDTTmri/IHv0wVT4ESfbe5t0BajxbsEirdEdN8wj8bI5KoxcgNNaSY1JpqTGfH
Y/BdiWirVMVAyDIVTpNRqHUoITEmRQmeC9ZFTrdvDLOHCN4qY9/ikyQfKff/sB/wpORWp0G1BayQ
tlvxKRJ3eO8J8HeELNslCOsvukKS367Ci1uJFhtWxOsq2JRbC6LcWg+mV5lEOH7N4sy+0HjjtCTi
+jtV7yrnc1QgGWCls59XM3TasgYtrQHcKchrVtwg060Z3EXTNA0EsTWe4PkIF/6B+vIJsc41EMNy
LsLn4tezP90afTXEsfcd9Sq7ggWcXb9YLf6XbCKT5l1m841SySspXZBwyEsX8ZYZSgxBaTgG+XAy
LwtXcEkwZ/9oU/T1d0oe9iXRJ0zf61KDWvgjUL04+6JqroKLCvxse3SSTxvlP3M6uxCepqsk+14J
7lFWA9uun/1nvfOsXrLBwAU6QmhpFMLMYH7d/Asolhf86heW+u0J7e/PDs2UmVfxCmnWHW+CLqae
DkCmfxKH2W3/RshAcProx0BXXt3JJQA587k5M9JlRhzpY2LV89IpgKCDTBMvEeSekEil+pr2V2+T
+bFZ6O4CcFC3JECEeL3O93I847hI/cOAtHsdxAC25DXBtNvPvUTdqzBrzL9l9sJSsZqDc3iJdNGE
pAgp3kNuk6yS4KylnLhlgTEDXpbJOntXvB05c5TuJqOD9wv9cqyk8B7j6TRX5NUZA8F05zb1R/i8
YrY20e267Z9+RSizDzF9fc/J2G5t1it02L9PDQiFyypeqs+9eekakjz9VhJfc/xwa5SJfNvAq0Kl
c3JLBZ54pkg7aZcynZP0puoH3jnzZDLFahOPqh3EtGCUrq+I5vUxpYgvkob/SUY2ZFxNJSkQgpnC
9aZvBiTQvp+HcRF9UbhCTLOkDshR1WH4XrXrFQxHkMIs0DqiTbYl+e5j9qkyplFf1HgvkGBYgzUR
mPSZYg/t+Ra0rHLiGQBWOzz9X3BYMcWoyedvOEwJ42iB5c3WoGFeatgwevrRjFpFvwmHrQG3fGLC
9PzOaRNv+RtoXqP2ugKcT3TkH+p3xR0iKFeSJxUikGMcGhNeN21GPqflx/WQnVzrcwXW/DE3N5ei
F6a1eQYqyMzKIZbXU8D3A4i14G1PdTIWT1plSboPji2eklk7Y8w6PRXCUM2S/NMptahpuoFYBMN/
sA/p9RsJF1a5BKwsr/p8i+FdUZFgIfCZJO2d/arp+rHyM8IQrHllP4mnDD2gHRiuI9ra1omAS6b8
i/nGz73OVxR+CvyAM6RiHJ9sOTPA1l0tytHY0wu7xczqBHX4ikPWBYefZCM2EC1bsqb8deLj3pEW
tODv3zemA1z5vziD9vooJSureRBHJ7AZbkNQwcnHeeHG1w7tAZGYwbfrxEbA+Y/adXE7S4W/Ryex
LlKkebO0ulaNpWkX2pGvUeYNtZLP2omxlVwqdy00Wk/g9EUIDJAfrXoY9xzdTi1gnCJNUjhjTY+z
j9M7+TBZSp0Tj7Kza/o75pkKVoF5A3XfxygwetTf9aDyEZg5+wiE7S9Cva3wtaV4Ijw+BdJcMsVB
gcSACvL8TXhfa9FMfZBLkldSorRleLAZWqE8IebdOiK2DQ750NC7fcSV72QSKLKvgUIN8I2r1OCG
0Sq9Ue6fAC2Sl11N5vd2ydgtfRLU19wqZVoiZhcANbFb4vFksG1JK+gTDxFyrlr9FcoWYCp5a82J
x4VKKrq1MmrT191JV4F0uF5lRTVx2zXBX+3Xe9HA68rs3huS1FuDMxBhRbmNJH895dReo79in40F
NlENcMZr4FIUeKWt/gqS6G2MKVnGbLWLf5qK+lTTa0KjC/M/BS3KTH4rmMB4BNVbU4Vll8Jb6h/G
htA6QDyh6zRxgedn8+U0VvUXLiyg42DoJscc52y0hIVR/KRgyiJpfXO5C+1bf/QXa1cZZ9iS+Jfr
glF9EXxpOwx5AzXaSlrSnYXfyvZvvUDs1Zj7CRI6as9jkDT7fzL4HG1oI9XZFdJE1Nm7oBOVoXIY
ISfRE1DRJZ3oHWcNnLa5BlZiqZmuet/e/SoqoEPiwtnSwSfqk+q61Nb/g2uLqZ2p3MliJo7I8HKD
GhZaSNUWIGAymRtvIPhbKW1i7YPjWT3BoY0FcdBcCiVC0rnSK+ssuggG/hs8ciEEy3xk2gB+8LaA
aeUD5ZwqMJdllyjwCAu67jtENu4uX/YreVrrR7ME/vnKGxpRKjc53pYAy8uWPxrWG6+wj3acopDU
YnwL31/90QVZMUQFouLl+o2HhXPI4uDfh9wF6VNM3wwqStjI5tXVu5Uh7aTMqeEJ80WUK87J6EzI
lZccfGTZjjS91VXIvS2V6q38ld8wFy9MdAlsfmCA1XPUdpPfuIZ/wtZ6ZTTYJsvwqrfqwdr1cMLv
Bvxx5Brgzs/ASjwkhA8G9wAfTGUkhgUswEC/Vo+V1IG0Y8+0V/sXV3Lx9ZHmWfMUfo9pqF6CWWMz
f6+2eHbzU79/O+SPZ7WrTGC2i7cBpKr3+a8zSjETm/M79DAfHCWZZ16hzayHOXYq3uGuaGNxchYn
uKD7DWO3FyC2iWrN4srsFvoNukQ1HVHO5nirAeQm4iZh9uOO6U8oD2Q/GMLStgYmGVv+g/F9BbM9
iZB/OHLaC1vUHOJhJXWWhc8GI0jgMzsxFaF3KThJ5s2zhOTav/Ey7/XQkEBiMjGRbJR9mbImUKrd
2gzi9fiqfC6S6WpbXpDpsK8a2mjwL32fLEDAA0SI82i2de03s1Vu/s00b5x8Mtp+OZHWDy5JB4k2
8LI/gxUzPqfAOoFcSMRkD/noZcVpWjHL/wq30L54j0Rx6Rfw7utWP0PBfPOvoRkKcJUmS3IzoxZr
W1vF+2GmRsdvDOTN8Nw+agd7K8yM91LFNMVecfj3gj928dAj4WhRERV0TiHe9BLnV8N3TCBbRQZp
FWo9Ep6+AOLj4SOiuylJ856qBS/3AFxMQEkFoYBatfXjxr6dQ/kn6ozya3PY6eOoH9ap4qygwwhG
Xd/Lg+5rzQuY1EsAdqpc6OuysG6oQsuWfRcZ7QRSm/YCNhJfnCdMXPuyn+GYzfeby/HSip4EcfxD
JWwCTq6rX1yMXjDJnvsJY73WqwK/FbyrxmDTzfHBqqVMW3YYkMvKIAftlH+4zAFpoRLb/3qhccoN
/kOUD1CLawfPHZ4m1i9eNs+sNWfN0ykNo1+6VDWWdWmRhq+87165tjMHMwhZba/8vuaNCGz5hS45
FaDa57XEyM3b91mCUqFrT1Mm84Kr1C1ogQ+4VvwEcncmSA2ZtMe/115XYIbdskMcCtiJM4wBDwZw
vNjUcyi2WTyGwBQjkNM/VI5ybjlpjDTc+6exS3OUu0ZueokiR6y+VQh9Qpuq8b+uMLZZBNQCcaLA
lDwawxAIOoCsc6lJPxfu5nciQ5TA6q+frSNFMzSTj7k3OyupM0lD3ijNkhfFpD2zlS3KdqFjB+Pp
wELy4P7YszbsrrwpT5SVgf/d/vqsE05GNM7LXfaqOxAKVTv2tEKu9Ec/1PdyjAW9mOJsIWdiOPj6
rrDZ4j+1RDq251yZPjm2kt2SMsoXORjJQsDaw+z8JBsl77owoqjRHF72VaEE4pH4blFPumk5SbJr
EOOECMb/tx1rT6psZaCN0qooTs8PwSo644pcIx9oSDfd+/E1ImL3OuHglAzobM8WRpOj+W+IlZ0f
4qDZwM7iknFVMjzWo9wFvFSMpenvZV2K1UnOqSSG6Zj/GKL6/ntA5lrZ0ar6e8zEVK5s3cipE39A
vFfpCHwFCMpFneR9cr6G6/ZFkVAjxvbu6iHXJp6ka0kaNyBaUl8MwnQhupzJi1FzQSOhHmevxTv4
fh5zMt2Bfn3N/+dsuJaUXSc8ekPwNHXs8Nq01AYUOpWed5h5+UZzioIpFNT0lFxRjErcyOFZlm1n
N/Bggak85mrdu3FzkJEXLLA+DoqPiy4X362AXJz9op80Qul5DTO6tJGvA38D3L3GPik0N8k4uZOs
CevTf4GoMdzrtDWZuPVj1lofBGsHTXtVBKPw4ekasYfI8UjgtxQGDn1knRmVLzR3xvCcHpLRDhn+
3PVFvSwmmZRqIRLpzmTEokqJzVk+0V9GeObFQCFtlYEWaGtfxO3VV3Gosi7dFies6puksTYm//+F
I6Ek22tlW+o6aV4W4ZCKD/qkmUzwezs7e9uJ3xF16u60jHDIvLBMkBDpXnuWLo6x1GKoMAqxbhCk
MVa45YGmKtW/P69cVe1X9QErWgC4otfvkoOo0Hf4sdWcrFFw1RFx89JtOzHznj3Df2uuwAYZVnM9
0SJtYOOVdKWSZrtbtiyEQrm3iV8VLf3Ab64jpvuJnsOXfCawDf7VH9OI0Emi3jv3TsXmyn8R9tZo
m2PtzvC/XqGcEuibrHtPAqmQIWo1Cu+slpBQnj2Yytzea5tPH8FypN1teXbSiPtzaOk7XUeFK0u1
yACL4+t+nd3pinr0fr7jhRwRTvMniz8qf2x6fYXPueYBh0Cp0gsIjXeB+8BFkiGQHQDNi8YJ9wBz
rVlKVpWVwTIWCd7gj1Z6/XVRpPRc36lMTfe+BtdhykacuOJDKGCk0WjJsJfNiij4xoJu33T26wRq
qjJIzd/j60mXRRGzP+d7ha2jjt8W1nCf1sIMPEQa90x4NU5O85j02kPtzwdo4mHHignPbsi6USsr
tARXPMk5wzrRMx5U+4dwJ4Ipl04VXuWn2tps2TfjVdbrI8DxHofdH+1OXydoCUPt9ngMC3nTzPmr
x42hJoQSQ22cZeOkquantkQbsdO/S5XCjczhjx41flhXnTFeaRGN3RbJ/Tj2lKmE3J5rEyPxl3Bt
V6x1agV7lpWRhPWAG+xWSiVxQ2fKUD0TDPDoAlpaDMcllGEg+k8M9JZyqA7h1BlZVgdfIwUQG9Rd
cNmcJekHHa+5k/KqFUAlTN83IKIOcj7JM7dCC9S1DvUBW7JGKU8DEMI1v39geBOEnUHxrTbNhSRI
VhBVxpPQb/zGcKvAwJyCxWiAPElMGK71K8lTzKmQFemzGXgFq8bnj2JE5C6o6BZ14JUVm6kJhb8A
K5zxCpU/SsLF5qdENBSShIGqILTLNEpXt3wpnzzRQviMnr+toxqXfBqSA0adEfL96+2rV3chOlVm
c7p28zmIj8qXbiWlhsP6mTBK/Rb+fVkjQWn+IlROk8cjZo9Wri2nOPjTJyUvI3hj15Ka498Hy1WZ
EMA3ES3wZUH/QWNTxXXiV5j1xCuwY1xUbumh10gHAyVCJcuYVr12+v6DcUbw5X+gASR73IM9VSML
4T8z24THFnO+GjoXDIO9kq7aLxOv0tXWBTSKwyPN5iGJWO/oFCCgL+PPHxXI3MO86Uazh5Dyeu7j
oMdQZ/FTVZ//gk0mWwngN7cwwylDZt9JSWkId8sU9C8AGBah2NP60VLiQMPac38jI5Uc3lyVOyAs
CX2SikV3csbo6cIrZXjswRFj+FQTVESgDYe8L4hjPHqzrFlwsSbYvUy7hQwQsbqJixWSGB+lqpVt
ycp+Z1Z7QLlQirOK6PJm3pTeWUeKouGqtsADrHwViSWyhqVhnMDG1Pi04CCyX+rFV1gpSEV4lhPA
telL1DiG0/x9YECtvewCdvCr/nyXsH7MWmXDDKn4mAL3nqD+xTi/pAHXjBtchNlZoJusuGMOGkla
+7V7no+xOYmT9KDdplKFH13ytnV2hRwRRilFgAt2S+d9BZdO/5+Oq504RpD1jWxNpEdRwhXZjrvv
4fSR2zh8W9+VNeku67gnMgq3i3m0PfT1Y/RwrltJqXbNInvRQJ6UJvB3kPmeUo214GVYKfptIwJb
l1b6tklKI5fESHyoZtb3D4neJh1wiTy7N2xlLThFa1LLnTxk0n4aI47MAlTv9/1q8a2P0rlGn080
xJ2o+EvOXjdPjGa9KLUd/WI2GajDNf6X7s0vf+N+UNlwmr+AsZgS0LZn+ZlX9HftC3/7LqH6yPxU
u2E9B2w3GrtOWw+06KRS90AP9CEc1t6L1QIQqtHNFJE8ORLDv7AyeZUfNX8M7ZEj4gpH1S5eGYf3
JNgNG+WXRXgRvxcXq6LYuULTLLLTBkHsFSBchqz+NMxvqq4jlZA3t39esMp0sa+U/ajfe5SVvop4
ZvY4Zgs//EZi6HM4c0j8qyqXVj6EDxXkj7DVrInw/agYDLsNhnyLpfkuuBLC8Yj6etE4MC31G3uu
kDjEXLnA+bKujUdcD1HGtWoLuuGM1Kxb3ovDuBK0BzPqHJ7FkTDHMwWLsBUBFO+ApiX2dSxn3hWx
Xbw3pNJSlA8sqMjsLTYXVJFwgqmHRTStKLM10+5b05NDMmJQqh+kdhcnZ8Eh5gfqvIcMapfs7bq+
d3TCA1w/q9yzcPJ7S6nWB910WNZQZIRYJaDkSoSLiQ/azvh1ZXceCn5QlVVMJULM/Qa3xL/tG4kf
83MLxYEaVuWZLF2iw2GgUnw/bdMUr5rqTcZkIAk6j2zMUL/yhdb5z9CS4SurYADrwytX36QarGOc
uHv/6vm8nHJJoyoWfZ7kK3kzYSVIDXnhnReCmAyvhLyvB6+BrONVWZNQgutIltCE0BX7zM8IFD/T
OzK6NrKMOhjcC3eF+OXXWBRKICox3tqOToaGu4UtCe0DL6KG3b9YDlPWbHL21WEY22IBVZC39A+v
4j7AbV7xKHJ9LXX0yNYarI985303hoI3LAfWcmHi0hF/uB2X7hC8TMsNFHIhHQOvRc4uCFQVCJqK
5HrbaMU+rnV60jI1GnHf8pRjBgG1aEVL+TdAVDAqnRAwX4d3TVcHIcj+sPwf0ZHVJLalhUcaU0sK
Amp0FsloxaIICfNVDbMFlOzAGnvmQbO3b6DW9cx/NGGn0/DiKtckDNTRbz3Go7M0n2ZfiQJRWP+R
0kU4QCce+N/2+Yn8//BvraRRGzTkMA/ZU1+TWLpM4eiNRaKWclWVzex4D2qAdm0LmSNmEPgV/hKB
M7FNQmghSbsoeU/Pwnqqmja0FxT74GEcEB+Th7p7SF9+WslVFFY7DxJ5LYUxW6tZsoWsBHGL0K/K
SMjpUjEuvaZvvtIa5BY2pLUmR31ZSAWmVY+2B6R2o5z3Gf5ZmY4EcgJqRgrQjXwpmvV1LScHmuIY
kSXt3495c3K017bKSGpaDW7RzXW6u7cq65Awmx43Y8VUA7hHI7imBPno1W2wh43jQED1kdAn0fEV
2vQ25dVHv7H/+S9/rOPPZjBrCEtTEa3QND0Of3If2q4XC8U6iyF2WJBMUi68Ae0H/z/jxQ3rmyDW
77kqd1aQ/oCZEafzL6BWV1nIzbEZcU5I7fodgwCvjGqulWIEksFG2llyG7zI4Mq/YeQdZq/1tRAf
/gH5lwiLggMoOg4pfOuYkgnI/iJwMmx3VOJJxbB+nJNWszZ9B/WVg+tovOkTW4j0bDknEsQk9+Cs
71tvJQv9Sr4UxqXLo2AgXZRCN2QQVgEVuhuxLczU+x1pZ5J3AR+RxR9+OUr24hYoS4oyExSqyPGd
WsYKMj+Uo6BKKeKZhPoL9mN9+7koeAWfv3fXOLFAob1Nxjvdozhk6QJlQ7s8591/AjVftxzewdJX
EettaiUVQKmtmXQ+oHBc5vyQQB3GCwp4WH1S1OSczs5LK1a/Jqd2nAjufM6xadpkuXymZkRHDT2z
a1PSP73+/CP+2CRgGeax71jkbDfaxcRTDxPXcc/zFWrglmZDyxc+719H3bnmL5Llip910Ffh2Wbl
6yiQsLrZ4dBG65f5cPy8MRlYlHyc+johx2dAvUcXN2OcZxpLVS0765AZwWOx7woo/NYlBrSpI5X1
ZSUduMel3fqJtVNRrAvH4WxOt9x3H53D5IY6rySTprvANYpVWIhYK16PE1RnY8H8LmEVfZukENUS
8ZcKdgl52HsOxoVSZz4g/Vt36s3+mVq1XB/DfCQthtg168KSfC59Nx3OtUCy1z189vD6Ry5506Gr
SUMko6ImaiP9PciNgL4uzM3an01AnjOpQgYgnrxr134BLrVbedK8fKjbAokkhYN/OvIcx1uJmMLu
H9fBFaNH7Mm6OXhm581fb7FFLEb+blW2bIGKid1rE2AoSZkYGIUgWZn/z6UjCDRKgF2CPsRxQVTe
dQDaQuSq0Q0klno/n3XTvvumsVyfWJlufddSNpCEYiX96fIuxL2koRLutUrBsEtpdXBda10r8hEg
n0izlGOWx07WEtuyrkpV3L5FjhqjReR7OpQbTfsxO5gZoxHrMhxb93adL5rzth+jPz9EMH5HNz1v
T/AWPrEmjjIISNX+r53fOZQFNV6WIoVqpH5ImDjRPiYHmMp+i2pRVhaYYAJQrrSeWsJ3LoPlDC+g
t8zbd/Jvjb61kmXLDoKGKikTtB/5xkCqOON0cIQdZ+GMfHvSeveutO8rgNXLKcYCbrCcBhP2S6TT
ND6yLAv88cUn6nzblITDwxg3KLQeLL6OScilhmO1p2osgbXp3Mlo4t//nbgsrbWrRli/9DAOPIiw
jHbIxY5rpE3E8gszROB7B1QcK14EDPI3Nd/9Q/tlaKngo6r3hsGjW0NJQDk7Pu/3Pa4MUShGxze+
FXpW5Jcozt66Z6KuikvNg0EGTb3bQfBljHK5a3FhedSWKApO4fHrCnx66py4NYiSogNBanRX3Hrw
FV08oduTgTi1+BMZdWTEbB48j2ka7Tsv+G4eC1no7xU7R0r1IFDNQkRxFsofEbex3MDTENomQ1oo
w90QTp+UNposKmPIQfXKgBbFVz5CdaesLHk/7gq87v15LkImsE4FuvTPJdNMH0vtl/qvqugMTkAY
QiB64l9cmxspkkkIiNuwYQHbORvzSDgx0DhBVnGIteOIfLPrgNQYoT9GCOOWGFNYx1/UVOCO+Nb7
xV+xbh6ASBFm4znxWdjo6D3STavA5Xrh+r8Cyaw6u5uUc4mqvQIAwmrX6P2NNKATYx7N31iPPnR6
squ8lbWtqQDxtHhYFJ6x8FQlO/R8pDpAsJhcjEEx+vcaTLeoUlTQgSGlR/r401hF6NfPX2p4hTeu
UtJ5WTfsL9EV8cFwuoFREshT9KSuZK0eVMIuupr0OfG7WIeCTRi34Qb8bQEn5v2oH4hLuqfLsFQY
Od8R44rbpREFpljQ8O1H3fg1UaeLj9z/D6N8C5WMrqW7zdHmOjtMouz5TykxVNJK7vR8Srghnlak
yYk1XwKThgG6ZgzQi2qv2+xFknJN+a5XHzoUuDfPLgWhV1b1RQSk411KcqRyXh+5PuFnPTweKtPu
cZvqQct4POLN5+f0yIqU68uMGMFj5mKnICJCtGZ+VyPAELv8itVo0XHHIpPEv2E6OAk/zK0GbjpT
l4UwqUGj57ffunXL8mnCs/xTElln4cZqF22GhQQMwEFXUXZAozLXUWquThFYWXct0L7o/gaKR6pC
0MUkPiAMj/1lho7WhIOSU9SdOzu8Ml0tkkOkAuXtHbX8OXYwlPcgCpV8pUZJ2k7YhqVJkXcDruXB
xiEGy8hhdyvx3enlpIvtqEU2CpEEPciCm+LxlaEhBiIJceHUghKd7gPqBl78Go7VsHm7aC59Xkue
SJ+TeRh0+FAUoX+nJ92k3oVAeIQEiBJPFRk6S7X98til3QZbVhCO8IfyHRkWCYX9R8H74RXGB7yl
NDlFl9yVPqblrxUxY/ZuuYGsEF0NDdnMwmIkdm1WkFtiL2L1AYWqR/uXkigffqLapap4IhMbN2xD
JNN254bv6Ea4NeHW5UE0WIXOzgnPl36O4ZPHZeTOYiHBH9XeECcVnZceeIpFGupQyFfM0WNdYAp/
2qZNs1+jOki3+ScnYWGptLwRixEKV9lYTKiRJQg+wT2gNdEtZ2wVEDhoDQ6Lng3aWXbk2KSysMcf
UqMwK7g8zN3JwQasvMRBu+UvOi2TDze/Ambwi5R5sD66qbFHSA5ZhAeK3nyxX71pyK2GE0yfzeEr
2hqWbPolHAtd6IoEoAEOnE0RW7L9j23yDfPFMcbAV0VaLqxvGRxdXWp/mKl7AHGcRl75S8hjnxXa
2dP+hsslfCYXumwXjT2OXtbKilbw0WGbLFhp/ShJvk+YUmWkMOhifCj8kPsnNgdUo7d7CJqd/2mZ
r3KhQnpUYmcJI8uOP483qhFZYmEJ9kbgP6TKIrr5YEU6AzGdJaunOVpu0e/k1sp5m6YhRI09qeQc
rPBiKpY7DG9451L65BMwwyV6NQd/HzmNb4lbMV4FWZPz8Rfi5hLDbN1DMHKkCia6GqDsBV++RIx5
6vmO6ZCMZTpmNFVo/m2iUesfPzIhktPC3vK4asT2vIU+TWKKnVxcb5wPuRwJ8oTuL8mMFE2Sd90+
Hr1inye7nG7/GrC/4QJfOlSG0CxtouVc2KUFF5pughdFa83y9UJUCN4YQsptyMn3VfVhhQVq7YWN
M0frdW4izk1JTHowkLcWA2ovYjofxYWWt0JoZ6cLwi9/rCQBAyMPdMsjb826AK1xZ/7jUvtjI8Xf
eouoSnJv5RwsYEUGJ+QG1wjPJ3qLz0BM2KB1WiaRXjFWw8bAvRzxF6NOLPrwTXeU3NlsAUS5DqRG
F5H4DCh1cNREi+jncwYdW2G2TedStXUQ0HlIP/ktk8KyUq4tcva92jlmbEKuxgZ6oSYXXgssV8OX
CDvGfvbhPzBn/7cXtuSD9gTx4R4IkESE74HtKgMIr1XuWYLamZ2yVWZCClksx6a+aHtuZ+B8e9oE
pyu3c5EJR6hylPnLXd+yUaTGAcErSFOcvzchBsE1EYn3+/+X5aoeL5z8prGp2Eo7k2fKnyNpW52B
Avv5LjZwMBx0FD7PHkoy39DE9yzycU6/IEhv3v4WLAG7C2RmAr0yZLrexvNPe6rv3GsZTCRG552Z
yLd2vSGY3tkkYNr8jWTyBt4Lh/VrFisk6/pXxZG9c6jQFmtV+O4oCD5sLEw67DnxW3BkL9WjGp9e
A0T2iPFsXeJV3SoxTxU+UkyR5AnCF+pz21dk9/QxZQZ1MX+jXyRK9U8mvqTblLTWHTEuzTiPUCHW
5skHZPn36cn5+7YJQsTKjG49p9TiRTEDWhS6xJ/u+1eym7p82ryed/0IkfPyP4QSZXMVzsXGVxSw
N2wfyVn9QYs4CuVHk8Tg2l/DXDhwz2zxiheTGEeBUvvSuBvKxDHSrJKqPUrXh/FzGCm4MqATnT36
8r+Nm6KPwVfOOsfWXzDkYw2ju2yFSdmNCuzcPt0/gZaAxiK7CvIf95zpNyz3LfhKy0FqJKC0Ukew
98Owy3Tar155ZDI7a076cX7YIpdoXbBbYpn06LsznenptnjBs3Hfm0dSoXDv7MiWEjdAZs8K9gvo
rg18meaOXs4qk0K89HRDWNEE8XpcF3KvyK+cKIyf1L/LEE3NBKrW2TPuJAU3osl/bqy6rjRzz02A
ddfpgQws8cdvAP6GoRxtz7bCPp9vGmbyXkB6SMJgyH3K/n+V9rSxmiq8gzuAI9gygn9gOR8+GW68
ebsMNVS+B1v0N+kbDvZ26o58hwBafGyST0rC5JtdAheIr6zQ6QbibPM+LzpjnqNlXS+x7JAppAof
Ba01NvSZ7kUUTxVZ6R3vq2TV06mhuNn/+AXFjkdvsPs1sxgt05UoBZcBTxJ1AiA9T6jj+FPz2hSV
Y9Akm+yPBaQbJX9w1Gim2vUcXx8EidgYJFKE1rhxcyMkl2woVILz0+9PyP8D724UCeiUIB5Ap+9e
U4gTLAhSYTb2jZAUqRfyREftaxrG8rMp13ryiDMuK11jkaPUbM0RvKJgkUQOQNTfhZ4RWzolrvx7
hA7Dhu1+RWh7fTj7pcKOwE54RJbLQbL6v/uMEx1Df0vkgqgfLLrzK+cVaC+yH9DwnbK2I8YGGl4R
LCpRfk5kvg84/a5AgCMzP6cDch19dShG5LhWk04VaGOz9D0IZkrvXHk44hL5pZQS1SPE1IHynMQt
/4dj8HfT7yiwQr/xvIvl7DADKzfEG7U84rCG90g7v0WNj1vdU/93LX7tC+/HpTItbSD9pTCmZmi+
BG3dJSGOUu59cq9t5oAYsJPgTilb0tvjkfJ0uGwxOOJQmULWm9q9s8U9ioZQzqkLd77S5D1a1DBR
iXF5YMLyQBsbYhN4/Gld02ikbOg0Ntn3yzzU6M0RUDhAhVwR39U7FQ7epMTVyo6JPM2dZDzsL4sZ
nGtYwFmX9lwleqsXBH7WGEfi9nZN45I/ZnBwZMFI8Lpw5kSh9IbnMh7U65iOmYl6AJOhPM4oQSdn
nG6Pu1bjI1WSc73L7crgWM63o8v6NGJsySGBPR4L+kd49RJrP03CJ82a90yvrShsGmOljAXSmg7n
yfmtVzAdY9nGvQJ3M8DxWz3kxnEduZ0/6IiQqjC+59OIX8RjieWUfXuLNIF7TXIaw8azdLh7psiN
fHLcLb9st+ksXoviCnsBz64SdKErh/jJVAM6p1lL/goemhK0g8WDupuUiDiTKEY0+agnSSbGLgss
OFOakzY9lyIdFnpIqlP1+/fDCus8jjhcwuj7onlNrhfiH4CP0KqNmz1vaedOJGvF9OW8akE4sf3V
EmVcbCDAuQznuz2w5mCEa5w7InwJUNAfHGjNIN09mT/7OTSlsxmSKKjIRhMKBF2YLXSeFE4Yw5/y
OKbxniDNSzSU089tMY/C7udmz+wsg9ct0S+LzDoLLWPEIXoLKtSC4NWFizFIeQ7lk1u4LLs6YuNx
M0ijzxN6aAX9umz40Y3JMUVbP38Bs40U82LosCI3geiYASPmw+l9IeedRxIPxA//fXaK0IKPPY/Z
szsXtPiLcwFMvn0saBH7Igsc3uNrHLH9Amm2wbNnogANk5KrVrhGGxLJlIrygrBKLe+j38Wbe8ja
UJ7nCAPG+MA9zOwdnGFDsqVOrxDNQnTgghgSkuDblJQcY1sY+DkrNSrUB7xuMplPF7OuH94J7g4m
Qk17kPKjrRvlXMwEH9iQvjooFmSSA3GgEoHn4pVXxfu7TqR0qaCuwgCht3UNcTvNSMFT6O/bOkGd
CHQrVTIL+Y6QbdEQiRV3t3L3zk8eJT68z4fVH0b+OjlU4a/yT6x8pla+H5lf/+2/6wDwC9QRYMRk
Q2XjH7wcSD9SpmhEAHTmaeCejgDu5qNFYGmtbw+Qjep2WMqjE1qyOLA9T9QQzxJ1xYhAdgsUIzrY
ejG+piZpMojXkCkkOa1I9wUpLlsZAUmxPj53Lz8njEp1kbX6CyW5aoYQdMOGON2MtwodGcaInyhC
Izcd+g8odqy2g/FGo+WxIOb/yxmahUKI3xGVKz9G+QvaKjWdOz1IF6pk0N6zTtlz/a/eBnB+QTpm
wjPGTiXZE8nyNSzqUJMJgQCuLbbiGPwAm/c4ggcA/hFie2jTobMtnIiR7OODGNDutmFWAl5At42w
CTH0x4B4YCS0xloBa6jQET9IIVeTngRZwDpNYfVts7lbPNHegGhNH8GQJ4XomwAAOAXfEyypNpYW
sngyvK6hDvZfMd9nVbWdW1/8E1+9XAX/fQX7GEGw47ykBzlGAA5uFrh3vLXgMqyb10LqSHwq/UIh
V49DDC2K2wB+eotRSHh3uga6t6gJOhZCOrXUlNUBlb/8+bqWuv9AIuogMfWyIWdXBi+5YoNHFWx0
Fxgiktf7IGer25YxvdQxHTAGS/dUPLzBuJ54Wg0CELYAv+0vvFoT8wxe3ZLdMEFSiL/r8/S45cBv
BPcB1LdSdXc6xP3VnseuR6aU2aCGh7nGy1zjKolBbfs1vmyamlTYuDU1QQF8TUq0KKgEwsqfSOlt
ndRKpzFFPAIbj1XTtn0tt9TcztgOCMbBIgF9QKjX/SaTozmyi1SA6Hy+V1K70r7b8kI41NGs12F7
niCVEcLaaRG/jyFk0fGltx+gaovcpeQ7svItkgZcQ+OFPWxA0zNeF3cmLPoGzdAMD6O7RM+9iVD5
buRut6uXVuF4BcsZziw9pr1ju4x8+3syTVFL+CuMOX6uxMqFC9aakThycT/maXgkMcqHb8G4Ye8x
FT7TpbPul50M+dm+FQT4a1iT3YcvsPMkQyGv1RfwJlMq/bk4heW3V4Au6rXdLgtwOEN38BzluXA/
t74IZoIt7748B3n23fHomCzeLFY1ul/EWoRYpoh+4DPdeTmPHPSlBBrseg+MMg9yicZnTxVctxdl
DxqdkAXXUtlWCpF4Jqg4LIoIb4LBDLKyYp479zoBkkwBMDryi097HcSgd/Wwhb+EyB1EDISkYto9
QHX9r/syNbNwkn+ch/qp+OQI2rJmLgaMn28UCBgT8xuxg7l4Eh99NimB9LV6f97nTVxLqfuyvdfY
sXZ/tLqqqYk4CuEQV64hMbchGtsrDYCRaCSnp9qDxQgyY9HyNeMuFoCYWB6Odvs5JDqBdOXDseUy
gLwYSK6XaYZQRu4wb0Kfw9jpFVznNL7eGyLghAONhg12D7pakgkV+jTD1fN42AXw5tMBpXqdkFIl
dUO0rM6d/TZT9PQy3w40wBj4WXAbmpmyCv0Z7sVXKInJ6v6XmgCmXgw2Q0Jv9n5zpbmlsYTFqHwu
IwI4v4p4uEmvHfJnMCoFgs5tC8sgKjr2FyT7QwPCzNHoaO2bLLnZCrUS3MO0xlq5ETWb+DtFx4Mj
cMXLcktI1SBCVJEQl6EsS22++PfPKTXNI/bxNmlZcCyUzpPIsd8jxzv/05NvZN4JQtCfY3nlgPFh
rjNBq9quUO66LHL8k4UXdRggTN6nL1ximK+6LVpS4m5/dpxrBattlEBqgmppOXOw50QlGAlU/VLC
mOtLkfAXPhexvfeE7RgM23ZQIT9+Brs91wK3tngxmSLmCL6e2W0zI+CLvhDBaWz/sKvO/ZRl7Yh3
QJKyrgcCrOkA5TYZ2iGQHAOAWxxbXYwKMKkJG7AmW5DFQ5MJ4d94BA+A/mnS+Or7YQVEWIMDoP0m
wVeB1wWGOjonMbbpMKESjCH5zNU+QyKAUB9xMQkOMixMnDeJjp2R+QTrjohU7GCY9obV6TgQ/vEV
jj0L8IAqswQiLfSh6KCQO6UeqR11OdcG6xqRBReNV9BObD46hj0Jth62gBkdTr5FO6mKi87y6Xeg
xAdDwZ69YDnntdc+ZUD2RcAU1phG3SKwlVmKoqk5Ym8LXgz3MP0F10E3rWd/vCYdFUdqeYd60YNg
rxb0p5uidG1VHpHmc25Vs2bkHyfG7orlSIJAeH/q7ZM1/DZqrEOuagSZiUsa53vT1n5km0LxAbED
8Xw+FqI24FoUeFvjjJnmzMpNMbOpELJDvwMq8F9SQVgrJo0VZ3q2QHzXoaBWWmzssQed3Qu/zK4d
QFExRiTffP8hrYa11+HqL3A9BC/JFzP8RywL23JdtDDyMdBf/ZsHDB+gFz8hjOo30wxaqe+IAbbx
76x1QJBK461oo2qj7rNpKjV6C/QIBmdg90MMJBIlRaqMP9VWWkJUkgtdTtFiF1HuxcDx4bGEkXfk
OC75U+Y4+jS3L4jzkufIDIV9iLtSSt9O2mESsGPUCu8dRkOx5CpEmm0ndCXcdK4WfdDjMnAbpJ1/
UgLG55TY3CKFkY6HTwjbguzpOvKPAgUaLMt3byEUuS8m4qwIckQXzISTFa3MZzQg5C3jsxmXddd2
9jpZnZ8Y4ZsNG1aBCEgF5NI76ziGbuujZ0rGj0W75qaL9llrQB3wGXSJUqwNWRychFMStbDaRMEO
ENI4oyyjvijqOLsqSEy6X1L5XunKno+f/V+yyEmjLRZ23c/GOaPNtc5VDvnffCvLvLqrZZhfyjXf
yeeIjxR+8Enf5x+4ZLDsbh2sXdIf4C+uk4sL+5eJiDLKLH0LeVVtcLA0ork60s3lpl77eciHE08T
UWRhjPptpRkL0qvKVlgp0DkBa3Ruq4Iyqg+iLzfRDhrXIJUr823nl7WYswyp50upqKScK4uFw/h9
eIxImTknJlax5ubR3ujNULGwAa90M4YsfXWNI/ln4NR4pveJt0icDtD2tbWCy1NemaQ/OWr8/soc
Fz50fTI2LheBOwZv2fghQq9KLOMyxZxRi5Z0QewWi/y9izPZmwip4p5SFLuyiXDq/KQGrhnUNXcC
RD8RtpUyRYkgdlyru5bhu1txNEnD0MxLbsigUkYb7i/H2tdi2GzpAs3tQ0G3dj4/0+RzzpBNg3Yi
1HRqAYm4QP86qWHYaQCXl+lgo/MW2ive6ysSMyaYr2Z4I60ZKlvKIg6O+dgdwJwzigHJovqBtsSH
iQrygBYnw7ZescSD2vH2Yx6tzq69/Uiitk/yr9QfPXR9jusgupCzamEqo1ezG+jwRd+WO1ZQ6BJD
h4P7VJhOGeQ6Zi1E7lr9wabJHO8SKIp3Na63EKGAXP5VdEwS5wJn6u25XizDELGuu2W8eY8xmw4c
vTR2SdtnmDmpVmJRLdx8nCsHtsahFKk7VZCWRGRaxZ3BQAlGE1CkP6DV99BX0fpR3pMSd5c2rpKL
qCDM+vkOulh8HQuQGh5KHM9ykBP4V7IEx32LxTZ4xU/6QgTBfBOXColeSGxJb5rYwtJyAEkfP1AI
bLzTNyb4JsTKuJ8/nM3LE5isA+a1b7jjcUlp0TqSYsUeRol4ZRGCOuUALFG2f4i6v5He6nlcQFQY
Dp/anRqzQ5dR40TZHVzXF43ux9qeJ/BmsOCGeYfMBNALiBK7zWGEH+shsJTgYze3hQsZwESdR3Cw
oULSflmyNgimpqDJ0Sp8jVJsOFW8GMZmopZnQ4MNaR9JgCCGPIUYgG7+ZVAbj6UFRWQEE3hHo36l
zZ3+Lj80DxV4Z9je+LgG0fX5lmLxYv2yKl2jonuRLU1zoOhNu8KtHQbpSS9bLJ5x/QdT0P6ftQxl
VcTJPbc6YVH/s2BDStoh1kPUdvgdPKh4biJXjO0zn9EuZldXV0PP/1m0BpxO1EVMHoRHreIFmNRg
Vxm9ESbreWxtkKeDgU6C+FqiD7u2MjAfKQ+yetF/87+JRL7o2wrZk/e31cZUzeWAnMq7G7avbEKX
6DFDsD7UlYIueoh5zceqoZrkVqx7STvd3QiSFGowDB/qk0u0N8FS91Qx15NMoNC5pKIXe5IhulN5
j+kpRQCz6vfKof9TSkznM2yIq7amafjTU/FC52IG8pxGyHegmMtD4Gkt+7cs4CZF2Mu42EWjgDTZ
eDk2h9/rCV5PEkuTGDHufped+aBLyUxZLwNmFoDxT/EhCGWaXsCFgr5GhMVCOuy16vCC6rx4TvE5
a4+ZDcKYccn6McPdMEKsD1R5WVOtAwBGnnorpNAdiqet2h0LC7TtQWE8TfWrpQhFGEKHMdfNjxf2
BkEW1UeqrMs7RuLN8vFFA1FwhkBvOqqvpNsKVTB2Urdz8EA06yMgLOSAcVKdqr2xIpxLOd0BsbkM
y3hvuWlKAwNNlwyvxExmdTfirf07cO0tCdO/j2gjkduB9ZkaDF6u150JQyg2BXyV6x/g8WsOa/Hj
OvxW9Q0T0GLK/0TJ0eNxVvyk97ftv8UA1Qq9QMPBvqcjCO2qUlxxgQlp9zJC/ohZEDP923jDl76x
uu65UFbd6ESuNH9Ze7xFN40B59be+prK/GSRxkbC/IdZqYxXarObDRiTUTRqYMPIczl13mDfAXRJ
Q9PR6TPu4BJryB9joTsDFWh4jfPd1Czs8iA3IL7FZBQKoWmzEI7gm2vxdyJueqC0yZXc5ZgHZb92
ZWnjKaZgfgwFR8U3evlycAKEshtj+sy6sIAE0GEvDpXU4MF1vfXZkHYyNHJnvM/fy6sml+V7iM3o
5FQn1ft5SiV0Jbsmw6DU4cRl0+BhRZ+Ndac9mBzFuVzllDp313XSgMEZAUoUC0aJEc3WjVPmQu6q
FhRFrV7kdcTxLPFcw1rFgCXQiqjq1Xso2LeQj/aW2yHpalUNZt23pNkvqEa3MJcU6Z1+KtbvaDSF
tdhGwVdx0Aiimw3vungVxd8MPltfJXKp/8q10MLyTJdYjZ20G54GRGLvslBw0iWwmwOPTNYS9oJ+
JurRlLI50O6xzsbPRqgaJ+2URWyEAVNLl5aq9hoqj934eIATI7IRhPvzdYd1ULC8H+0v3sXlF3px
zd2ULbrDlc8m/5d/VyfX1XlT4I0RNyARUA/iKQ8MAcYrxU2oRiPCD6i2I4Uvy7htKzXrZClxOH6f
Q4sBNILoURqBTM/INB9/5eEokae/YnVRkcWVvypXVt6N7mHI82yXCK6Fp/iSNxObCRFVY9gH8rNv
cKb/riVK9+tUwka7rKPOgfmFauWtiS1cM6CEFrUlqRLZHifgLlBhDjqFUc2jye3C1HZ3yzx420HL
stQN+c8wvOhbqqBxvgwYkKXQYrGDC15bDtnt9ozJp37IOAuY99tYy1cxVLENfhHzWo/HQ25lolbF
PLHdDLxoGJd3zi0ME8YT/jd0CL8R84kNWw/bVvhnm3FIXZgQ2wZnXUQTBkSOmZ2Y+GwA+VMyNYz9
BkJj+1kc7eUjHILBpfIV595tkTRA7bk2UGW4p+F7EbMhcrZLzasbLfZCCujr9kDI4m6T5G40r/Np
UNgRPnpWKB54mRu714M5ExI1WJpkmXnOPC4NNamocQ6CiEnyiIv/g580HIGK0VtLO3RkpuWeY9+l
e/qtW/WLho1sN2wPW2YgdwDsFHdLDuBRXEQG4JUnGmN5QXRAs+WJOSu0rsGANxqbkMk4m5l2qmT1
VUIVJfz3xut0WflZ/dDMcifmV50/t30dQDkz2VgMLTB0PvS4yk2u3s9G8MxDJJZio1RIuj+X76Np
ouVRRRza7yqsszclNPgqQBLFaE/Kh1/fov+terV4ui24IMfPQ9wnCUHIZjodOc9icjW/WCScINLW
OS/oDJm+QkZ9ynNHXV8/FZuYOWM7OHwIoGXOC9GP1SN1V31xfywQEZ1LBlgYSQxfUKmBjsDbopB/
s1FLtsYBGWYtRJsbJAOrW5UEAR6ShpL8c+H6VDn16ATKvXhgPbSiVmA6KM9SfDbIEKnhsJscONVC
9+Ic6T9K7fTP8lyxUq6SuNTlagJlOFbfJoymlLYCHFJiApPEvX8+5CntLA4RSZgEvBKQIfAku/2A
UfinMEpU3LtkpgQexfwU+JbLW1egqGPVGzKOwF4UKcMfltJONGMWhxsQhqFS13NmIIb4cugf2y44
3FGCJ49G5PG+8OZcmd75WQ4vXz310idT8yhl4b2hN44UJDl0JziHqrJzDlf49eTbO56heBPDbs6R
gvBdZ1NF96o/XXk4gVdXZPMv7FXhP9ACJNrjQhspC1rxngucgyRKiYPJ19++DbKbf3z1Arbx2A4H
0Tecb1TNVQqM1ww57957oZ6ocbYQdusFdSx7ONTsTAOq3G23Msn755InFMlHXVFwBdRdwFrDEbP1
1BGPOWImPsxB3eV1wR7eU2Pae36L6+VuTtZ+YyHkSJWTNcX1+rgTmGStrcmcOsE/WiJl89+BeWPz
YhIq6iVH1FLaHhvGbwBpB+8x50bG0sSjVRG0upCRiRVNCP2Gfr93vLRPtwNaEpr9dRhideBeaP2E
b2j2zZtdrqqueT9vRURY3l+8rQpb77qVFKJ7nTbl+zbdPkdcqxQL8bkJwEJh2fHBJ2PL67q+hQT9
YOG3jBTgWBn2rli/evQUFwVFPWcAHrSGhCNbvhNLcc8XOJX++kxBhf4t+f/B3wx1xQBfPCzvvXRK
5L62BGeQ/TE9pxMVbJq6DAuGmhyR8uu0dXjAFp6HZclApVf0JYI7dKE1MuEcwRvZGuOgVFfIEgWk
qTC5RUdSelx3/oEBzKVWuJmeXG7KyWJUIIdot1gelOS1k4xAYKQGruVhJTNoVzCmPNkn2BQiEF+8
koP2yRewYEXgXsA2qJHtF5CaxAe5C9xJxMOZXK0pFrvXMmJF4slMQAW3g1yUXhMDWKF+BniEyMVh
phOOuRoF5OZLMlhxv+/7GNVnfvUICLJugsvHpJy4zufjM2u6uk26RkDR5bEzuGbjsIBcPshXtlMp
aRa/K5AjBOU0pfqMlJmgJWTjckxTYINpvgVUXYRMhXrL5fBWFNqDV/ljp1Im+C2sbNN2uTvGIu5H
kB1e4Wa8Zrzxb2CtwY0ZaWvi8IwuK09/REd6Si9nLGPyHPLK7stwGvKmErOZ8DAX6SkLfUxbqPGj
q4B5ejmkHwkey3oX696ukZvbYoGSv1izMqRmCaO6NXKrBbNp98o5wIVuBR2oa6AH9VkKL3S05ZHO
q/Vpb1yjtA+JoeHBaNsiSvw1bw1zHaom3X8UQkkFiJcBMP7H9hG/UvcWTMgMOtIqAhTOZjZkd2Z+
Zm8eC6/dZXIYCZvYrsrhyKsfgBL7/jU+aWq7AumMcpdf3aWqNtx04QOVZxmpYR7l9adTyjN2yZZi
N7+3IbQHPU/OS8sD13CS8usGRhSsmiVQgZWekB0+wqpUuyBStf/+qaeXMVgS9OXAvY70TajMIoeB
JfbgAqZ5Y/muiCHxv76v/QGbHA79XUk02sCe1OZUizS7Qdsn/kJlctFtI/RxFf+ksJAGM2qImPm7
YkZ+/x+Vr06AR8j/048eZ5n8A5kNxDToyZ7Tfq4qgS7O0HrcP9953N5x0UOHtc1y2Wc+Kh/f9lrm
SJPlVCn9yHsOgdRadCNuEAz0aa8aMpwv3MKCoFqVxX4eCiUZul6uAKR581fRRSoGwS+/X2F6IQzi
KqKCQsKX+d0lwMHjM4oWK16bnj/1thsqU7wqRKmR/VImfBZI7GG0HVCRVlJ7dxByj90wKOBIuq3e
+SpvdFD/bgR16RLCWAzrliaoBhFNlJ+jZ5LhNvM6yMcWGes9EO55ThgjsvclehendUELPMOXNzHA
9oX94qJ8nD/PPMc02nBMs3Mxuc/RXANVHVYrv3x2bc8nLOOUIpO2RzGNGDhS50PiU0m1xDURwi0g
u35LM/aJVU5mLBcSjuGBxFoI66lReWQ2OsEN40xs5qGDotIZgPUAcsjhiYMJ42Ih1tb9GdiTkDvt
4Gt2beOl1Qrusd6ZX/8uvizTqZsjQMDNitWGwEQGFvFzIfXI5t8lp1rCZmnkImk5MC7RahTQy89f
GrvYQccOaYKbQTlMqsLZf+VqO0wh3D+PtNd7i4lj9488UCodTqsS4TqM8TkKtuxB/VLdYrXv2jvr
5jCA05ZI+fBzhuKwPwnfbQlNrgvNkjkTpALHH97RrAaG2WQZH8L7Z1qqHf+6P3r1XMyVsJEM5ao8
ifTnp9MKZI18hVMa4FYiPNUn9TmXEYnTWvRbWvD1lAluw2xeMhfzI9n5VJoqgl26rJnXxDty3/H2
dK2OpPcdpFq6BjSVenXzikCzLtuVgIiwzfUPPGz5RD/8XiQncL9UjYIEwyh3yYx5yyZgqySjk9QP
QnwuYA/XbHmVZjCYqKEUgx7oKM6I7+wqilUmnWx/Q7Q47fVTErVh/ahvdP58ayuly5P+8O2DOOaJ
52GDiLX/dya1DlmmyvGva20VuUG+J8rX3dMa211M/GM4zhX4KkYwAc0fdxaLzJVE5lDmlssV5gEa
G/ZflNEapGDCropIbGdHkW6sq2v/bEUOQTtXic6/ggK6gil05WLzqzgf6lVizWmL9zEb/RejvHuD
7bqOrR+QExSt01pJhXvGK9dRuxS0PPD/E0g6p5nKERf69bZ9iqvc9l25nwTfwmQCklzEclrt2O3J
MxCQo/xDfl/RZ0e5qg6TzImso4dtPXsp2b6rBdi02CqrB7yha7wf+aM2+A8oftYWAWlxyXCMMf+h
1TViQ39KICeUfaxJyygy88HrrPAXL8Y94UZXLemKuw7hjiTpLEtLPZ1lSGpUm3Pp2xDhIupmf/VB
iyhT+kfT0sT+Di67LiPllPGr5O/G8qrOVQXJWeFR3PS/RZrtDjEm1TAA1qy9kw3LKid+K4gXyJ0w
OUv0mhaYOZoahgHLjbhADVQYuQb2dzZianwrxKEP+zqg5CTTPf8+neFCe4FYIfN9JwgTOVRbUNay
QRxaKGUL8bhYcELNxABaNbfCEdxskMnQzrD2r918Jg4nttix2hjvMECUHZ/Gnmhpi80qiAC9D3kw
v7F6xeBg1zz7mG2d8RpBZuKEmFwtseBOh8oXoIifjElT/pHrbM8hjHdjVeAQVaWqx7B+YeU+vxSo
sPb4rcP5ROVQMzb3v4Ulu/G+WYX3Ibhefe+zWXmCr1safmK3YsUadrooy10pFazCsGlzJ0h91KvN
MB+6fJeY0szy7XX1mlB1Ytg/X+D2bAUIxXO7uuccX0tx5lmorQD0KxMmvOpDkGpLgr+YGoveyVgB
Hnwt/lKA2zJsbntL5vcYPHtnWsJ8Y0v5ssqGGsNRoyq6xelFDn6JdMq8Ry05w8KXPWymXD8vFJt2
N2T+M+1K7LMwvjWquNAzGAlcsWdVxS89ZN/WfpBrZPZjxTwRzJk/VMT328pUCWfsOK+YShhhIhwD
RAUAMvqEwZxX76LFMJIAPap0L+DSrxQejH93F9f9oY+XJnAXjTEcjLYL023xR94Ah7/uAFovLQUo
xaxkk4DoKk5R9nHtsA4b8I2J1/bftUF/ZHpkaEDiUVkljjsKbPDAKUUNGBv8rMV8Axnlg9b3BjD7
Z9aJiIu/PI3hse2qLtYvMAQrz5TiZu6gi6CB+DMlgbeFiaStLsPx5vUejxkd/1gSlBY0jbFd+iXR
3dRhjjLq+B77oo4/6zFGNSfrFu1HaWcWWMbnBl/xv8Urpg0UaJUajcHfONUh4hMP4fyKCCxBtvGa
qbF9dmg7SVY7cZOO4BWDQ953leDPKb5jHzbN3bkcrRft+aFx1bR7j/D7rfUtgmtlKirnOAbU/ZsP
mqTj6Mg8GkJd2pwjdlSME3EgiBabR9DCTT2VvBUFHGq6x7JIzCiEr5up24iAW9chHzr24r+XNAh7
eza/sLgrZlO/HvyxMYgVWaoiYJa/SY4ZBOhfK0EHrBZhgh+5VnURML1QIBaz5OaoZJRM1JCqujyc
ejmscKHDvsCHGUqfBacTLZ/k5cyn+I9+iT17X2X3TLz7QvOPPPAZh4iiLL+IGuf8CpiNb7cEsQ/c
pfSldyhAH/6U1FfqFtCtqO2wKZLk82u8BUM7lGMu2Yw+kZYzlX3mcWRnB0TAD1HNNLGlft12t8zz
NX7nPKPuTdIsZWiqSPb7mni6m+8mPgD5QvDyk+siXrkp6WgIIPk/kjhvBBuK/8AlndiFifjRAped
ekC7gyuEthb8dvWHgocpzokNf9RGfQz+0SEmCKLfSonv4tz3WjM4pes8L/PkgYqdJyR16k07SwcF
aDR6z4BP8ZIYOmVXFgtDjBLDiomMXByrZAh+8p+dpNKVcY53Z2ZkSXrTVVvpYf1yYmldFUBQ31qh
Dk9yjQkocPQR33r1g94s9HOgf4hpwLEnSy1V6kG9pA6GtAIBRA2Nn1eFT9zD2WAoZGjUrBWRH3qb
s7N08IGK5qUxj8Fn/jzCxim/EWE1uAROdIWkVjmVgt4og/DOU75jcgCJ7Bj6PELzBj7u/QzRz0Tc
+O4roZRLGSnWgs1gkJohwMMcWYnfO9Q0BOCaiontCQ6nkDFrf14eafSdxpvoNV4nZxn3Nlt+65eY
uNG+C6M0reMC8mGCeLEOsG5wOSpfyE81IqXT/hluEcdLZO2/zYIZmwUaV/jjU2I5uXyiejRf1Pcr
ZJsE53AHOPwc5yvGUved7UZpP+jED3LAn6gR9GEmwMQvIPdSLFQ5ZdHtUNTZD5w1GrBceo7uES7d
P5L2OVBZvY2J9GQoy4D3i5lyKb3Hag2Mxf7qvHFXv1cYFzuVcVWaMbsQaCk+OoSXvu+e30RvEG7X
xWnhWeONAaxmb//DTSsm7iasBV2K1lAnr60EtMd6WXSmvSdh1yEHizMC8c9sSykGOwdb0u8RRV7X
BSfZgrqd7RHWF2JRa9CuF8aiBBlL6CJoLUOOo6osHyeaHy06PKPu0bJpDoQ2wDE5emnm2T+1feM4
u+huEzypC4iCOYgvyZ7vL3kYnpQBjWiyRM0pluUsh9AtscZ134/jX4df3andIzWvL2vamJhIAzL/
gHlV+AkwlqpoVOccCW3Rjz4JvRo3Fgajl9IGzpE/bCTiPsLcFSty7m/FC0QuZ4e5jQ3VWOVAJ7OH
+F8LkFjhgVPr8YxHtiNZbM5MF/dgS9xbxwJCMeo+rLVNi9TPHvJcfdRWqRxhniRHtezNvFLO5fal
6MxT31zKgUiPATuGd7NafGZBONOfXuHJ73LYrFbJ/FiBk0W0YkHjrgw4KXlcdAHsDdA39qaOyNyk
SQ0iycP6peIRmLaGVTatGvuhOUrWk4LS3qwxgk4FQLdD86nI56Iyn2wlJIl7kt8JKZDeVoAuW7xV
Uj9402XxC4zZm1Ou5a/ksVt7F5gf4ybhEtWi8ZXLB5FxcCCxnbZQnU1ucb3AYg/k7/cNbnLV3AbO
YjDjP7flLK+2xjrps4POpLblL9aZksyOb/tvOu0BCa6e6KQpiXbw0Sex9ikAkhO7tQC+ivLk6oK3
pRVDFYMnB1btRs6m1sI5m89b0xdXOxXA3WS71ENUsMyt4EztCGg0Lske/1hJQ4FQ4SWdUTd2XGtI
s+4p81aK5z6PDH5aezxo2Gjm/U2DIgWlytFdtY39tTwh0Zo2n8TkoCkVr5RrdNulUd96hPGhuZcM
bkeixjgCRL8dtdr2BFkboKWG+VtdzV3MQFryyQ7nz9SktCJShC5hJTAh1zElaiDc6ne57N8X7F9d
AbMnn3Z1VryXa71tqxFdMXCT/6Jbu0qWBEYptCRgeJlapeCJe7+bZmLlP2x+0ldXCOjTCEjDZpTj
yLo2YdSINgvPwySvAKLvbyh7TFGJT9CxQlbdiN4j+idk4425kGzjbNddMI0EkoCfzlFX2ucPpXbZ
Ii1LSXd18888bLcrV7OVxyAUwQv/pJ/E0paqbQydiejQEP3oaPrZeJJiPzXTPg8cDSAKB73qvzvc
BlG04FULfaONAifP7PM+PmmBWXFOp0tuxjA04FNmVR3J9bmqhjEmLGTtomPxD0UoE97ko8z1BJt6
C+PViG61qzv7QGKcJ/dslr4Z3sOeL351qIR0Jgc1EX3qSazZ4QqIwoa5MbgmrNmG7FvvO+/OGhcy
FUjbwyfGTkrkcb9r/Z1H+dNgez/T3qLQMiuHw0YD8diquyW0Ivn3suZ8dcGPY9vu3yKz5qle5WC4
bKvzWa5SD9kwbQ4jLelGUasDvzUKduvYKWHuVRx6R+Pz7jAiK4SWrSJ7zZDe1PWCZoFubDxwmxAp
aWGHCr/J1KCB2YsXUKXryR0gQcJqbU4TDNm6g2zGTbnQsymNjXS61C7hJrLj7nPVwXWQjFtTGRrO
rQbMey+VXjGHhJ6/y14/oeKyuezk1f9Th7XPcu1v3cyB8Xr37x8wtT9XOF8BgTtCyvOgPK+sv4U8
VGYrKlmekr1aUy030N/KBuQGVP4DOzL6Cbe4eBxo8/p7LyL9mJanlJjtsr0Ma3s+12F8FyRcwhHo
ezqd9G1fRAd5A2trppnY7hOJY+/PFo5OTKJRmEgP6ldQbUtYogbSy2VMjlYxXhi9ffIoruCKN7KN
5P0uFSc0OSRJg9roXABZK5onBgeDz6e7aVIuf+gGkN85XCH6M+xQnAK1x0/wbCzbRctKABiYXQwF
zo+9jVQQjUTTvEl2aCtsThe3uuqlGsa+AlSwlVqZI37HJtV+Rt+UYpnNnaxM6EhA81ipse1oeE2t
WOzfnSqyRbjYvHxjdpRlTqkcwQ+Eq4Y9rjmsMCdGTOQz0ehfJMtjM4GTOHV6rowK6CS7lfnr9ynK
Q9k/DveBCP9PITUeW5mAOCsHkdRgKn9u/+5fxmbjUFmzIeLnEFRUKWIely3X6EmIzbnWlXV+vpxL
r5QuPTgsY9m/UwIhfAMolyU+QVywWOjc+kbq5SwlSkyA6BFm2rSkAjJldQpK3EAOOmdF423T16Lm
H18rDlN6N5yHIOturowM7BOPRapHGkPhtrAz1l7wkaQryXnwap3kM979pEqXK893+guobEkeNbVb
pZ0kCidmPsuBXDjpGbRnDDBvvMn79tZkGkGVxh2r+lRZYCJEM1et3je3tA1vVbGHgDPNjhf0xhLm
QnnWXTgOQBuXHVzTwbnDB7fqEZ+B21xYlgugLcukK2Ehlq/y9MCGW5nGlbfcJZiH7BM5aGIlFFat
bacM+4uSZRkkhu3OV2RxF0oTkdRaQZMpp7eB6SAp4zBq8pIFiohJIWehNSUqm5+tEn2NgM/tQy6M
xnWZD6w/PNUg0MstuuV3WiXJyMCWBJ3yh7Rm8m0lTEqzS0zWjCsCn4u6K0kOFos/N+DBnOFhSDvo
aTvcdHfmARkl9dW5/vzQUj98otqy7wpZq1CXFGYp3y2HtT677JU7QqGRxbONGV5rzz590a2+eOGN
LibQoQwo5jWTmefCUOsRp6Cl//brm/BFjd/orK6mz98UtozqgrzuFHTv6s/ksXJU51vtHnyLdt9n
FxsZJxiAm6oYvo3PbQO/AVJcWhvQA772Y5w6yWkecYGis05B2DXM1wakgT8bIItsIquJWnkfimAf
AycnMwjAS7LWmdMufIKaHnM1A0Tm9SYZG9TavRIOsa5rXiNKYZM3x97MI6RkFjYmgk5dWn9Ram4Y
0FqyBjIH5hsltrGmAigFiha1tMP/qk5fpLoz96Z8D7mwMjLpR071kLDTf/+ugHG3o2NgbD4SOqFN
bBJXm+ekauuEZ4s/R7X2lO+KUGq8tEpxZqLXL+EsMg6bsIXcgo1nhZbNR7KvVKDGg4WlWCAYZ+pZ
ARGijOg4StYq4AovdDj+Pv/fvsTGU2I2YtuQL/9Vb83Umh+C9DmlP1PpsxqD2RN7o6xUxgZPCPc+
oD5UNHMS7iKnMY+npSqRzx5ZejOS0KDxDDqsmEw/Rza3nb8Ai2YGpyT5OJFZqCSeWb3zus30Ad8Q
E0XS1f1CEMQM7jngV7xY+mfwpD2sb1Qj9CEO8oOvsI5Yx4x73G3B6dFTAc9IE0hLOtg/pSoAfnCp
4kQLiS1tUNH89qtN2PjK9Ji6vmkpH8JueY45gkENsPaF27sSqvm2B5BKqIzNM+v+FKD87AXw7ZLK
B6BnEVxojmXbsbBjDLdpYufANUoBY3oO52hX5/JSWmPGjkn3FbZxyjDT+Ajq6j7uyp+r6pK5tbOK
UvtPKyd1hFDy3SRMiHyU/QsN3gEDBj8TeSxw9EhoWnK9Au4VGvSXs5cTZgNS+XcyNcK9jikldnYe
+rAWjP8VU8v1zRNlFXcN4xmnQ5qk5Pd0DvuYPJnPSJjIuIKAivT3PGPI+P61493TD3dsLD6RMImY
dC5ti/XLzsyRxCxYmxw0YiP57alSEqUTEXxijvfQqoqpalY/vi+jV4OlajorjOZklhYhTdhVo1Xr
5akjB1xSqGh8v3EJloSYNlMn5eT9hxjQJHcqjPFZc1TZV1zevkMa/e0oNkKstg4kCOuFPiQB+KzI
CdWiZIAHejjyME+yXN1TZ9cCzp+3NfHNbsVVgUc1JKzI8QsdG6jIOHtspKkfP3xmKzr9QQGOInNa
PW3cpCvh7LFvbDVFdLoOiQHLNenSW1WL4YHpQ+dM9nokgsEYDUHrgMyRbS7s0tmvKAJboDURzmRK
h2wKKs7P1SMC+QUiCiUPukffD+6gut7NCB3iSe+JyNuAPmYfSkiw9mFPWoS77GGoc0Ykj+WCtfYZ
6XtcZVdOjl5EUqGm9bdbFlT248jZC//MZkLwzLRocYSwm9xhi0PX6hcMY0G4t2ecDRPQMqNSC9Pv
G7NT9VTMylxOW5oQcdAHX7AY//79JKfRlS9COsK10tGuoQHaM8KHAUBKHp5OE2A9SdH6Ba56xQtI
PW1ARwdDWQ8ay+4xNb+YUfCzpgG2VDyk+ZxnxgMl1IXEIokoH5/CKKXxCYrs2G0LXaK8prfTaEgC
8FTrcrWmF5V4jJuOCk9El3bG/ngPZzXGHQe0izL2q70wm+L4A4jJZMnQgK48QY92IUMO/FWhuqaY
GPjsyj2qRLP1q6Wgu9ssr3plQgmB8MEnIs4uxvtgzc9aXw0gN6w//ZcDhggC32U1vkFLra28e386
luYQpiZDB4gnibGOiF558chxWpU3ohJuhyrvOuKLR98JGL12QIUYfuFtwruSq3gyH2IaciYsC1m8
V5OOBGb5QwJmOTXsaNwAVh7D6gzlz9t4Cd4Ud2DTt8rXAhsiqwA+VaDgOI4nqAUzNI2LSkFFPQ6J
QfdZjX8SSsnbw1tKymvbam8sLb4T19XVSo26WkHkCFpqgH/dWhpU1VGkiZ4QQGmT4tXKx/TjuOAt
HLlCV0Il400zV/G/3fOtEmUSB7jYRF514g+0VSE5LxIQ3ZlXsVoXuhUoKaCZRclLinhlQy+o7eUV
vB1uS/FfWI/R80f7sCfrJhg73eoYM31p9HnUacyLyHr5wGdO+Wxwrp62lwkBnqJzFG0YSpwhMRmz
GpTyJzYbBL1bSNL8oSvHpl18oXw5OA1C26Tqx/nOhEloKmlpOVn5U0awcK+gCwTflDdUi2ynwcTh
2DvyBWdmLo3S8EOVgjq8LBjkrEu+Wu+cdEjN7DqfsZ/tuA2Llkt7HFvWyj+jjZHzAJZhkMY0MTXB
1xRWq9RFQcVqC5TS8hK7LmHZNz1eVBl4kloQBOiM1F6YFvjb+ggjE12hnAvRVg1ZqSqstDoybu/G
7ZJMjjXjlCRB4yfJV9pFx94vwQ6+eq/ix6mTu3SvPeqR+/GGaAfnt1XmzFToXz74My1EgwXkX2qR
/l2WlLMDHdPT+XexK9xM5qZedD/NlfWcrROTWc13N7Jd9Pozs7rYGHVsa+Yu9xLsP43mc56+2MMY
UeP8HcIdBGxZghOCzq4F1WI2PkeRA7E4iHrjbDTfJB4jzCfGg2FCzBJgQVgPHNeoB3OUUftLN2tV
+WFhMI0NXdemuqbUpfPulOUJl8SOOjBRmx5wmBIuM8WItqZl50z1S4saGO1AIzBzqT02yiEhMnM5
tTxCNZCdg482uYMkQ3nVUx1kio+n7Hut0p1269LCHPy8A05/f54ET8wyPZXC/sahb31tJctON0il
TU0Zk9iMO+b/vt2yuPGxV1SersnWNBLe9RwO0gh6tk0aJ3ISfe/oQQgM2lvr+DRftBVK8evqVwAD
fEFnzdqaKUhZ1/ku6MoNCUUA7XCP/4SUiDl5dK7z37O/qFGA8cgJGU/VuCtMIJw8M3k0S/YNOGmv
pw10q/j55XUEyGXTj6p0u1MsZUztCb9n51edg63xtrlVwo3NR9WTJwIyFIk1oF7qD+bgaPgQoZi0
v6MBpaSZWRsKBsfyuaCfqhbT7ZJCwUsoGchFxYLHotNfCuZcSc4ZfCeVINML5cujpo6QgfhRnW+4
8UxK9GGtHN5lXFFGrHddvujGU8dVquznQkMdlItSkcM9GV7gH7nrhT+QxhsJiQ6kM3PCB4RbUIKB
mmvLXjuvcbKKyUqQW6/fLO76cq2/5iAtadu47aZ1/S5U76bdoRUjjzMIwKRuGYfHE0OVjNqClaey
oNYYi16LPSKVj0b/WiyfdwsPhaTMGcOFviqb2PFKUW9FEsbxwRHSrzfvuaIhbtBNDFkVWRkQ2iuQ
Cas5H8ED8PnFltGKDt3pDFlF+i54gSMlo1VTX4Msr1MA1T/1gujJgdEyZjitUPq9gLOhBOisaCmW
+MFsJC+estLk7QEisxoUjHNHGuQ2QGDiQOWmOlqcJPKO6S0Dbuk2z/r4eW4eVSYkyKmOdyc+OpvZ
a3iJVarMhPfbelBl6FiS5uvAuNw508ElEpV7kCkDDFdZbS1bXvDt+car8VqRR/G3cjI2FP7amVaz
vBfR6W4CityA6ya8LZw8YZUfpYvqTAMeDLYj6zZ8KuaxzuxVplIgVE244QQz89Ld9lUoV0bEJciV
/1sYypD0vyM8wzFbJYELXVi+QugbM0PgvlTpYgyVBQRT2Yh+ovczHLObec8szspvcivqcS8CUWsd
Plr84VTl2h5Rvvb8BEO++u24gIo0YILh8zF8cZCzoZCXK3pX0bMnLg+/G4wSkVN3b9LgkzKRLxYe
ph2pUh08347TPPy7kMYcPGyLlQow3OHcukgPjYqe/N/gZZrzU2RR489rSGNG6BlACFaZwcafpCyH
pxpwkC1igmCSlcI6RduENZuCaG7eshWQ0WAKhKj68ahNqpLMgHSmNdnHt4ogra9Rxr5+7KmLxDmv
H/MONPSlbz1jBgRsN6fq9tLg5BkIEabyYheX/H+Mid069JTIBGDLyhc9dnmnxuxw/RIMMta29EHp
nlBm3ma4ex5ACRwJM0D7dUsdhVc0ZcvwgxkbrSnreW625kCVYkbxJN26WHXb9X/Mfw0MK4AQ524p
R3wnIggQQ13FZvUGJyEApeCHwqPgADzhuo2Hebj6KgLU0oo1Pwz7oy0KEmv+3U/53zCsS4Y/yY3j
HP1/ZE8Zvb8GB4CDGByZOuJyuS2ZScLqK7hxYxJ9vMPjC6CixBTeg+A+9kXiHh9LmDyHrm1B0s0W
wYXA0LJrzZfHg/YNKO5XBZxDmO+2UjIO+KdzYhFpdQxjtRAejDqYsp1jFemI8iEJLkhWjAn0pQX0
fcB6F4TYaUoR65bZQRvLbCcmA6Y3Y2NSwsGQdsTmTsGQ7f0i0hSZM7JQV4q14ru4HcSoSsWVCvAh
p9B4juknO9IYLFZ2VXNumXNVEwT/mBPPBvHKi6typkapeYYiVfyTN2wEvAoQK5gk+T1HJVRmepCz
3azH875cH4bJuC+2cXAlUA/PTgWehozLLPi40zagJradFC/ipcNpevDhsao7yKwqG11wVGklsIm3
RGUXT2T4iLi1Yfg8HkOLajb9sNxH/52gy4SX/sZkQr9e0ohz1lPupqXmgmM3ytCxEhXI8G7IsZFV
IcdrAe0cWwRa3IptdLXPmUgz0cNj8sCgut4OlBXpe1ROpYJJxEyS2KmSL9gKqCLHM2392gWLFfaY
bN5Wh+AuasiDqHg1MyXvbhRlR0HaK5yQ8j7X30vCiIwHPuoJhROHH5QMKLkqNtME0MVTgJ64B+pT
/gNJydgY5J/18rD68Lz5Ifxoumq/i1MMDD0Jlfm2yN74efV1PgrK0DVgOilObykHohzqnmZr11eT
rQqdJLk7N4dztuOvDjKEfN6+0kGyUjUy+9vCvvo7DHvCAGeiGDpmN7KPENRL2ppR8jfep6Ujoogw
+xE1P++HBwEo6MM9ce8q2XsqA4IH5DUd9fKphdCJYjq1BKTIRiNAPD3WYhdJDaXgVF/vlXuZYUSa
o0qpeJoZ9fVS4LK5KsqV+U84hm2CMk2Asncxy4a7PRq5C0yrzOky5hHe+k3SWoMoL40wN9KfJg0D
XNz25cG4X44RgTV+GRvZVMdmH8sc197q4sCCzZYEfGghu6Tdi7BQAJF3ONlQUiNXJmyp5TqfPtQf
KtRs1GJBxJxkXfq53hkQijvnmsengdgVnNqZ4FIm3YgfQd6FJfqkjR1ZzPS+tPfBbzJ9G7zeYleG
qcYC9iynXfoCI5T6dpquDR9x2O0PceDOp1eE5G7Yma3ae1K4we5K6Uk2btKG5NhJiTJtRLvDnhY0
Az+aOX0R5/YaGsPyjSWqeUXdHgnHioYgh1aqDdi0Qcu0KmYThWKvhEhDs7j5AfYREZgsppjPBM0E
habjEeMDQ7DuftkoC8k+4Fi/J4PE88ZGqyS6wjZy0qpi/GURPuGrnyjM7k25+pjYBuEOMJ9vXwTB
PZvWhHrPwgz59QqvgY3YysKEFlt44RFM8GrLDnqeOmmH3wc7XadQ6f1nb31S+PQyurDheg+3l/Iw
Y0GFmqfZ5/NVRN1O0du2wwfRprsiVbFrqyiWr/OKiKLtJohjKh92l1KWG+GfvaEXL1d9OpXLFdz4
CuVb6BCejh5KTDJVXeiWndlr93NReLQQ44bNBChSz8+66VsJi/NGSTxVYcoRF8MGHdZNAExjCDaq
+QW5Ns6htCB5PnIvXdvkNhs5SFCIBG45/AKsV9uq1x9nVU0lSxg0KCnm0xYWxdSwWJf2xLAh6d1K
WMXwU7UQSQA5Glh2FhtUuuIco2l39aGIEoCSxYNDRr8pBQwnBVWzkw5NgFejQnkIxXV3qsVq0zdR
1qNAsgckIJipIu0logmGUN1b5cZv61EeIlgNXZRMAWxhKWPf053ChtdccdeB2lc5fD3a/+o1d9ZO
DxPR0PNsZQ/6zYqso7oRm64RwUmQRisjcGdgP8clL3A/jsP2sDDve8fSUNE6AGWwO8ElXRv0DwfD
FhZ43zFbWewPo+eSNQGhKWygi5T8vcG77G7ZRuCNPnGnyh+19Y/pPFdaLSqSnT4XZr4HEspLuRng
fCmMrjFLcmRLX2rb+Wcd9Vh2/zlj2MZ1cJ6BFkgEwyyL94+2z1Q2L33jPBBHdwNYswa/seVqHOUa
prMwvHLLkWaOtvMMJFa8QkLVrj/AxYwN5p39Qa0eaksAG8QK1ZKsVu2GV1nokANuv0f5lrvBxcHl
0p7LRXpexrgUUXjxJbAQZP+yMnyuQgcpoVWD43DEUN6Wv1ijhja+nlR9mz3RacIiX81NWkIXIKfN
+WNEJoERtywKuX9wb7z86MhlDlBsWLFoIpTbjZ5jX5CELB2lkLTS7J8XY4pyJg2uuZqbDOG1VLi7
6w/NrpuIJ3q9wxxlqS6wT0oeKLfGEl361LyZH7pnP23H09VQNBngraOhxw7UdfQVA32QU/2bhDaM
yqeEKtqQHFQEEdAIG2QtJzreTaJhwdDZsmzbr3CUQfRZUKEFdQsSbkxGU/UCzITExyalskhcjY5N
B0tZi90uzflyJF1cQu5gmbqWlSINQTWiImXOuZ7PZ8ML1//NEURs28WsViJPUfyDMyWt3+xJkfPf
ziskwq2Ec0qjG54Ns9ffUI15QO++BGvpJpZKqgafEOiGGngzx7PPOVp0Idrclf7RZQtHnzczUMQG
9R/D/nbshwePn6e9yyC4ehp1qbuUrClHCDMmu5k7XYYnd7DmlcU2cmLLmbBEDYSjgKzKMc81OkF0
VBedojpjbOhKv3QBP/zUSslEXhtrM+yopFL+SYc2ZV0FaFmzrcemLIGePrq/yj1PouPFjb6ZrF+d
86bchxOGl3lAkqzj9te3StLNh4oX1T9cdIqhOkIkfS7cgNjj1EQdjsZKXWUIas3Vubuo05tg2Ueq
oq5eplgkMh66wnZlyCnG6+jrbf5QrE6mG8NqmEEgQ12YAGCuvEvbzGIYpDZRC88pp1aYcoGYm6jv
Vno9cEBvIMDKULuD1aJIaekLTwdQfOgZI3gUBKuDpfgFKMAnsSoFUSLaPNIRNdb3SP1QKUc8pnSt
sFIlschC/ni6JGrC/vhd2Jf5vGMkiwGtBKuntzM/xSNohMjg0mM6JheomYWWM0TvsRPd9/jbOFL6
5MUP4kO7NSS4ROtWtcIVwOo8ShmzHIq5oVQWZEB2re3JMtPbRRMV9HcyybPKlhZWxp1Gs251x/bH
nLCk93iENGurT0biwzRDgnP5yxcxFFEqMTxQiXMpsXGgXuTaXJfGmrzXPTTPpH/CKB6Db4TNkeMO
Jp42DbGVJdGZEQann2U+MDQu73s8Qqe29FTQ/uTeLdb1B+TOFH0JjWOz9zzVGv3NS7IEGmMUGwjy
Cg0f1BRATu3iSkOSdW9eIL5SuRU/mO4NeRWfhXH7TpY8oZHvmypPemmuR0IK33wUMBLtYSYBU11m
Zp914yuxFtcgWZSS4QyaGUYZs4WQ1aMnQSdxZAfJGASVzFbVX95Cej6P46q+SnDKUp8YbwzhlnaW
cvLL3kJBzeYwTXhtCB+6YGtZC52AGR5QEbfL+B0rwkCQpM3cO5JW7/iwxMxPg46oTfTvuiMG31gw
vKk+bEW7/LmhjSu7204M4v00mRkn3uujD+ENxJuEWefgXW9PuVaZaOI259Vh4KQ5YeQ41qgfy+q3
o0HpbC0G0lvayO0AB9tFddq7kDubUj2AzzOQ3dmI3TiY6dJe4yy1IhOX/oxCGxdIwoJ+LcEIA05n
Wdn3pQzaCqdNAUk2D0qZO9o+SBo1Nf8vQtSiAQ8aVt0sbqExhhBvEZ2kW/8H24ZUINSSqDNl1YUo
6Z0oD5M/dKTLkqbSz8yRt3Y6k4g/LHpAlSYZQxImZBtrWYYvGpy422Mq9vzfCgeIeqtSsCoMrnW2
AbAUchae4JeApiI+AHz1H0gLTIbfQmO9OI7OZu562Y/IqJj14DD6J4FMyhb2xxt/0pVWc7+E5uqz
BZXcXOjqD7bALN3WLIO0EQPwt1RWzzX5cxHxgAFJD3PVvyaa3g6+3+cfeM/biMXC+6dcFTD8jBCZ
q0R8SU3X37pOiXVMfxeM/goTvOxEyDQ2gPM+xXrS5ob9hxTjq61rZWUgEbz4ft7hWSGDSMBcuD4N
X7AlPbq+P008+GIENHIc0zetPxBgbpGB3wjqqIAD0lkV5eDCna1ZZ7kev4cHQVdHGs0/77QJuqJ5
K4vEIIrGwHsUUzH+oYuicpeQpzwCA2apKBqXQexzUTykxjtuzAJb05ta2hh2q2/Ezo45kRbZ3sSx
rnbWzprPNUegqN+G5OBRYnQbifvhDkWyYpkOoXdQqvtXc4efFe/iGlO2soshBhHm+tnYgFTvjtZ1
hHns2LaZ/9ZBxfvwUYaqqromomPehoRSDTCMHvyoYztRAjdBP2mq/Pc5m5Yt21zwM+gh6Eue1xCt
97oNpzX3dup80rQccDU+t6lOqkERJyW261TwM/+pZbGrUMs+4a0QdujKL2T4mdRk/yQ5z8b6n6uY
8ICYQ1yoGgwjE7rrZyz+c4cp6tQDZZudYdtfN0S87LDbJ8L1ufEYYr3qnGkiHOYq8F/m7yK1GJjm
OBEcI/4zykLpnQ/AZXhuP7cs8RGm5rY/7crnlqO+Jyv7s94ghOLBFYh8FErhjrfwd4UgOPMHz5ut
UhcSgIWNHUpJkUM7iMHf5TFKd0llQ1AfGNSfLvV0lzUtW0NVnEk63vwpcNVkBp42rzTWblDzR/i+
FVrpQZqiSTQO9873JmfqE0kgy5/rALHifDku9lKa48/8Ytb5ke8N8YK22EtNs2SzVL2NXbRl0xGQ
irZNaOqL0P5qC3JbD7OITTmrd82FXrFFy7sEiO9KsIUVEvHHvl5gWRizLneEbw31WDUa4dTqnFMt
kqFHv4lEyK+NXFWkF7dVy3EPGkyeWAIriwWyOqLfGZuVBqf/UTHBFwuhAEO61+pn+vqexVsS6hVb
zts4x1fnylAGv/1lfDBip5+CoYHyEwVR9DxVdmNDGppeuH9Hqhzw+qiAwpCtqvJSuzO1Xrbv8IDz
MR0nfV+GA9R0cFM46S2cblSbn8Sj3czwBbcOpQaywnocD6XrEpvkfVLjk+qMAcBuFaijwB0JJh5z
n6GrW2fcqxDj61dmMdUQ4R+9ux8Rxff84S09lE7KrjmEaktpvetN2qV8sHw62Y07mK3EWlB7K4gF
bSOw916HnBZ+b8BhDusx5rG0VwfivCOtCWnQPDgnmJwWyT8IpWnsB1cFXgaVlqQv9KJ2O2y/D1qM
CLE1vDCt6jxkOJHtZu2e8A1Y2IUinQqBqmBUvsPvHyMWLpEjQL6xKPo2SX0K8ImUTF24kTHHyUgk
4A9E+VKikk67crMlkJNEhMfEqgZBIAufho4UHwVTIkbyMeHr5SI4hrLQ31S305o95j2TjEVbP2vY
thUpkJ1794J3401mpeEtbCAFNWPEhdoyK9+5qRIeojPAiMwl+a5QQK79bvd34bii7pFp3h8KYAJX
3SoMWQkQm3H9QvXl4FCMzMvptJEidbbZUo0gyWQF7yOxIKrQlXlYfWez3oUZzjuFmxJ40ktoVsOg
xIt2onBW6zzd2IQaKPMhtsh3JcaxyZiEBK/xbB479sD86Fet9+SF2nLLhRvaiJ+7NABoV8kzJCdr
UFLNFmZiG2wYb99XfBma7sUtdXOpQAcvamHhS+1i9S+NHuDUZZ9NTRhizLkA3cSdlv0MY4t24PlG
IwQDi0U7fNTfHvDFg/ZFlupjI3pssG/1lq/ZHS90UuExona2Xxn+zyWcvbjKek5Jv+oA0zoq0d4k
Xbq/Rm8Pxw2Mzq5dN4UhuGbKuOoVg0aqyL6WU8Llupn87+Sj5I23LyhPLLU9MRINtjc5XYLCB/GZ
aeJIF5r9ffyBvPDOP9fZz7ooGvC9blCssgHq/Pmnf76wZVnliOi42vJ7Ufi9eGZ5L+pks959a4sI
5bZjOLMFML6wzEBt3vdQY57zDikjMpX2BuhKi3xzInruDWZOmcpU6MDo4L/B3FKKVj7stAe6gBxh
XCtBXQb5y2EHTHU94PIkDwVCciaDLzn6VAkdCAzY35rhYNCWW4k2LaZ/PNKu4FjsN3iXBHX7zeoL
B1WdQwsEyLWJogPNl4aSWRmcS9+eAf4E4niMIm2iodrpsHVOWC+7HYZzBKl0fMUOO4LaEpX4CLix
Kbiuo0DRppwf+l6xXs52QUW6jAe3wGCI1RYpSAGXR18Da8EYwTB66NDR/iN0xOIeG9Q4i+UCQaxR
X4AE1g0HIcOM82bdZjjP8SZuI8bnhxkUC4EKvgNyzVSA0ZkRSH1XnU/7zMMSMsGTXVHKRsERWtqc
QBg1paeVkJJBRkuX2wH9h5xYootMdrBPGYToQkCYTE4A6zmpks8fDN5HjKDD/tN9rfQ/bVpLcVkV
FXfTIhRo39VOk1KTtHBPXWv7qHhCU0VSYlWPzh9Pa4IQ/iVqBb8JQ2ObKjH8b/zyb/ZOF3Yh8qGj
s03HBu+6cSq8rtvMLR+MGDqubcQqp7Q20mx8JG5XA5k2m37gu1XP1NBR8TlqqaYg6/lNtwATqf20
biPXtXPsiP7vyu2pzbXkyUQurof0SIiyctmncWfQJym6NHmVkcO9F/ynwve+mLj5n6+8OtrNqPBt
AX6h4Y0Q3g/TGc+kzmj07/WxMWDo8XivPSpefWGtiBhc+2iV9gHZIXbrs7t2gnJUesp+YVDoIHV8
P2BjmEOrjRlW7oMhP64/Lp6caeLn7c/mUnqFsEp5OfhdsqZ58hp8MQz+heYgTBQoZ5HPS7Rlun4b
qc37bGVg+AuTFs4/YZ7Rz+vfCx701CKZGMQ+U4afYEUBkonPqEe/IQGeWDQQdMUzCEgCs0QPNQao
bPw7J/OtHStw0Cn06XEw8gQpXsxSlCPuFZxpKsvB/8bc6LUNtE7IY9sBDbZdhYJ7mzGGJV16PWhj
kRUiw2SHDNPIQJYgTUEjBk0QN/rkrAtDCD+BFlDrtLBPrKr+XbUhywXGnKMs+5uANW4/ALvLmO5H
KRHgI40jGMsBbWiipECA672fGUq10L+T6HSwyeStRPZLCztxNzxbf970ue9mwkdt1hVvp5EwCjqG
9Ki7vA+jU6qfmDJYxTSymTxqfBwV6R8ije0meltCIfAF0zCbO+u/ks+CIlDNTQKa/P0/1Up2fJ4N
WsKpMEHPz11pJMIeQA4Cc0WFwjBsVYpU1EBH2YoYCe2/T78aR4lb8qgZE/8X4dGZjppF9cSHSg0N
xMqpk4/0bg27PAdqYPzBUb8kjc3GTexxHypcUTRw+i95JI9OirS/2Vieq3bhWbhHX/1j8vYr+2pi
Shz7WYZMwLTUxodyANAbvJMIQ1ehnO4lS51TQpz5H4wrRt0kXXbsG/7dcaPZ+MaooXJoPobJBVo3
fgJvKx/14o/Oy269y2pbgiTgRr7L4+MkTjPmKBDmjvGsy2ZM/wwxBAP3Q1swxfgRN9jGraJu6zmz
YhQQOb3BtYPekcPmjcY1bWBXqymNsChtHFc7peQar9/PNEp3dzRO/OfS7Mgm3lPPfdKIDY0hg0Cc
DrhUaWoolMB2CgSN+cVj4Eg8K9fS4QtQRq3OzgLeZLOhrIgKg7e8HkymGSjIS6T9Sdsld0z0t4la
pWBq8jMe73OWnpVLHV2amUT2z+3emIpIciLq6onoEtz5xmbWXEFpB0ao7e2+ZLgdkp4glvAF07/Q
saHT3IM/My4T7sN85mW1JKGD5QdcY8UGBIOeaTJBCvOA2bJfd4Ii5ve9kYpW76a73fGmYiior/oV
BzjZMghaRyi79bbkM2nxcin3pYuZNgfR/6fi/jIjxCSp0xx53IAvl83hnwWpTLfKWy3Tp26Vdqk1
chUpAaTfUc+vHDZ6wIdTivZYQh01MgvuZkU9p1x5Y7E4JPa1l5tKyPMI/vlJSG0JQge0f4cbN2K1
u5CD9AahuMiCGDiZElHvlrvlxUAdhxBAuOsUiQJm4Mo2fyf5iHH/tadHEyDQqzO2iuEt/0KwEumU
SBrve2/zHBf11xIuYtvWohOD5tuIDhT3K0ir2UPT/HBzCJQjvJLhWT8iDcJQCvLRGq1xee33pLDG
bzKk7eP56dd1uG1yjWYBVz6O0ql4k/sXVQbfc/tskBfGwRcEuMqw5vUc45/nf90fQ5jjUp4LYzNc
YGICiMDQSJ69GFgs7nyfwryE3B/KI5Xn6zRfNJxi0nlpYqGtAn3g7ek0qk2/VWoB3+UvhQWgl3xI
7pAKvHF1zVPy6CIX4/oj1PiV2gPW1KtMIx84WMyJcy1VM6yP7Yx1OCZGuMowBPshiBOe2f7QkXaD
FdkMmbld5eZKXRHA0Ug31eMqL0ZIf4Rf51lUWpkK9ck1eBkSjVtaCcwJ7HrVfm80vS89aAznr3vI
T2225jnddQ7Cf9uSJxf9Uxc2t7/QcKloKrYeZ6bPTPQFuVc446PnXpB5dME9QxF9gl3PuvpmifEK
tPFX8MrM5U1Q9dpiuy1Utq11oTaiooK5QAFtjwfrUcyBRttsnzSUhkz3DV+OZwQ2djOwf9JyF9hv
5I8S3zUQq5zR7QaVxk4eGT4bTCU1whGw6SxLBksU25GIJalQdVkMRhXfchDQf8qJ88KRPRv/lmHg
qdXyAqkfDP9TI8ZRPF90ceqHhi3NnSgwGZD2JAi93e1af6Znop7N3DYT0Tz5vpuAAm02wxg8nmq8
foYHA8W3WCWl6JI2RDHpVAUMSsqEVVHaxtKJHC8vefcipkUNVBIkxvA8iTXg0XBdLMisnM0uEUge
ChIgYmBjM7HhLko5ezFN13xIHINvoscM8j+e/piock6PKV7xByTncN3dgNnIEhx/GMuacfLMQNp0
X2MtuUtv2P2z98suR/8SkutEh3+Rc2NEg+aHuaL0I91zVS1ytNkwouAgVm/kxDe22vNQJU2yi36k
5WJwav/UNHr5s9rxSdxhHwV5LJDyVJ6fiU3bLc9/JsNzU0+gL7EyUmCTTcryuTTKXzYOG8viJ7y/
CdrMPyrS8nkoR2KTrsA5poAGlnyWEuIeiigHXtlKvItpE32WW2cfYPIZLPoaoOIQu8+9EhxHZfHI
I7swYwBKZiK3cCf3sZUgPK1qMU6O8jKzmbnvktesJxkNI/GPn3wUO9RVnXFeC7ssW7CrtZ2eVWoR
c0tKcb6Eby9MER9AMChyITvnM6n5U3PssONtSEkwpuHNEUiQr15gwvAx+8F4ZoGWcuCQSnz3BXrc
1SsfoqFJu/iKbEmjRA3lLl5zFLafPx7M64eH54K/8F+aJHB3pEUena31nF58XQ+7dveEyIy48gTW
X2UWW39g9Mx/lqh/CMFetnvzKRYA4po9ZZ42rhv+bjDAUOjZbCU0Txa7xkCR01eMbbUBCq8sGr6o
g377lSECIchGgGFZxfn+6UpqzpCQLcnP5mT2Wf87vVAwOXb1igKJTDHLwJ6FvsDSZ0TkqmdF95B+
w7UqrZQZXO+wr+WDXpq3V51s47ZGvPJLMDN7JbozO/7s3TKXqhE0a0E+4CnfrYkwPfcGKKtvF+ty
Lv/2zkVuWF2bkATzyLIWFkf2yYJ3ro86x1BT9JErP1BXzKaOxujYA/HMaBhC49NpsrvUW4mFvHlx
taN/j3Y4Sx+CYc/GCEtpL10j2dD53Af+97x1sl7vHajfWvjtpbK994j9ZvQJXV7I1Wnm258IIqhJ
AGc8uNoCiUzrkAEKevap1K6I1zJs/FlOYRV0bdoZOEbHkEgpUOJUHJ5/FlvnGdd6wDRDLl2O6gmK
rSJk/Okyxpj2jpvHj4taRz0kyoHhexlbt7JfnsiC/JeZang4XwS5KAAyoCfenR+yiKB/5YdPx93X
UyMbrKUzaTgOeGjAkwgk6ZXdhLx67wFA5g1gbeatjPItEeLhbhE86VM6f1ljDLG1m3ZiigAltIBu
d4xZnYUY5fpjOMBTmJ8TibKYqh9Fpm/C0WAlMsYmq6xADyq1xkNH3+RttPJZbdh3U6Jrel2ieSQl
QyXRN2frq4LGtrg35uvuJMtZm8eDFguMpjF1Uu4ncZ0H9M7KpX3BgcPdUz9Cj8aTSP8+o2t3RIxG
nnT2ZcNy5ZQI7wD5B8QB7uwfnskLB3yVnkSSjV0d3hnFW+4qWjJ+wWA8Hjq6zYTCuE4SuUNW1JC6
sqk8RmVuRFnPu8cbCnHEznbo7+rMldIXyBhAqiZsi4vRLyt3XHU+TuTByn/G6M+DrHAsidWVTYth
h4UJTGb4E0S/mpGppfcQxM4zLMRkXP70+X9KZSZKYq6rpmdZCLnl9lUorie4mRAoux1aLtRmsuDG
rtBqK3F8QgxMZYtfAq9isZqd6wCiMPJOY3jdblSwvXvuzDMTjl5sxRUr9WOC+3/VuoK6c7a2KuvW
TKOC1zw1mmrqt6fe4uk4U7lnXrOboxl3L6f9j7X/TCqx6qWBWNMumvSjXxK0DV/amsKTODhGywLd
8QRTWIWeViYRyrr3vWDcR8mfEsMW3JVeL21pVdMQ+fDnYRbC99jnbF4vj/xyKp9nImCVHxjjb3ED
jmEFJawcS0Okzv7WU6/Y/2W8EKjFWb+mXNU48PMKwf0gjwAyTPY0Ar3242hzzNlM0XB4VyBdDKAx
FKOVgJ4zf1Lv1gOqjvZftrQhfBIuLFmN0Mz2rLyb/Eb90TINTd0V7OQiEeyIcnVfsvSJX7JZ6tKA
5rKmmHIae6WMyGjFSL1w5/4O25F0bfB3mNBeXZh5Kf67MrA4VKxFBbKZmFLfFVqKLXs0aHPt57iL
q+7U0+KTXjTMbHwn+ZDwaGDagXZKcnuFtm6zHMNyFzUNf0RR8OONb5h0HtPyjcozFAuXmLUaw5/N
xzpIaP9sPZMF1JGy2y6oGwfdB8vwSs/rcPcWeTSwNiqM2JX01PMhbnGFkm00Ri7oijACf59lYUcx
jqFO7Zz/GPv5ODJ5UGrA2Wuh+k4rxWohVtY5RdXLDl6vUs/rHKSA34YtyuwDI8NBRE/Tr0u/m3bL
y49QqdTnsW1RWMDoBkPqCil3k532BZnhKGqdxdGDpQuF4H4mqpXuG+JgBxA0r3raCBB+Fd75mR5S
XZmoYNAn84tnLsaIltLfJL/3u6EWm9OFLGOCK8Z8dAHAv2K1mWySSHdpHuQ8sMWR7lGEtJ50e04s
jxwYaMRKTuKrPNgZz5V54aAck7QqORiv9ZNkyt/IuvPmyJWHn6iANv3BPMSejpsKFvr3k+cpIkgB
+yC4pDfWQcOzzKqCs4dyckEGsBNfYTmoLEtEzGWfkdOznCNUia5SpGJoaU75YtMxASGIHYyBuZzi
SSM0vSnVjSMwNJaayZSwCKT+op+YG2pUY9kzVGZekZdP0B1anjmOHqSlPSRj7caGHNnsVyLVuoUt
e3rBx9QWBlq38v4+F55GKEfhbzZq3n9NYQz/krwEdC6YhGyMsNSkBqtDlqpPVa919eWBUxUGjOE2
ETj+MFwJahBivju/H2GQWDd7y6UEDjNYwCPRdn06dOCqfVKblrIPu/5iGEu4cnpJk/JoHBhizvSb
ghR0aXTSU3LEBbQ/F7bTRzO1wt/0xHDZe+WXU8bNYemNuH3zA0ncEP5B7VE09JxMqU9c+7SWksvo
48XtWgGpgunMT322rUj5zD9F/iKehJcA0FbIfVFQp45S9N/1jK78zll8VbRhykzApytTVPYmBDtZ
PQOdyw6R4Zdw79BC/LO2DrlPtcWpgzR7Gi75ZSW55IWCmKPhmKnND9eToVjOv444zt98vS/NBK4K
1kWCHgnHc8TB76e+Buun8VT0tWlG1Xn00G8LeSPXCxaeLgBdDONNeNbTf2xCCh8DZ5VVkFoDp9vn
fKu4oE5BJH2dDSk5tcsy2DzP1l4AaPrzxJZKKSC8ipFqQayPy7ML2HIJ8LLFI9fNJ6zL9I/PmUjd
QKdPU8/u7NBcmXw2jyXCBq5oqfj72iKBsaKke+UmVRnjqSsVzRgQPMJuU4QWUrqCu9Msh1nyno+W
1OsaD3HLjKp8iVfbNvS5igRAasESmQELXDyPfU0uUX8bUfR5nL1pEMSP7JOe0pFyB8NDLNOtYn/X
L0TxglP6jUny4cVppJNkNijTu5fjcGhpT1YRc1sxJz+W5gBoyOL+Upxi9LBS9ZJpyOwcsydF0myh
X0V2indn4c0EBOmthBpEy2hSJ3uyPUL/aWtbLAv2VTfwiUrAhuZi+O7TUmqrN5JjkWjNRb2PqxAv
oBma+Co8egWdDADydHiFyeWyaUCCae6hyd1tPkBQKq7b6s1J5IJVUrrysqZMVECIhE6Z79kkxxIB
qIp5S+sDytNI3GY+zqRL/WLaNIdGrOqjY77ujWGjm/6sp1IwUso4StlFkGQWvLEjUCSk3KCxv7lF
PGi2XJNamuhDRXvarKk3Log+GcbGXFIPXGJh/bvlw6jMQFFpuEf8URcyxAyLEd8cqTrRJ30E7o+8
0YnJm470XYGtBMzp7Ntpf4/v+S04lvg5buJF/lmgJdmI7LFqiJvhWc3QJPSoydiJFYZotSDVk+gH
SxYUacXhfzv4IVHFGDJOePzs7Go+cPW7Dpo6kFx6+YhZJFSp06TVmqRAu3snaONv0+BdSs4iiKD9
zTlpF9OcAmdihZasy2/0a3Ijwwt6HD1M9r4IEsd84O5XTPnjt6nt4rb9H9DkZw+p7+X1tcBsyHNO
TKDgbtg+4q2Vqtn9yzBTRPam8orYpK5Q9HQgmV1F99XTaEdnfvEfM+ecbMnYyzCnCoTCv13SVFrZ
vXyzcjohzWfOTchtq3vmmrxM5nk9cujRdyPKZe95crs3glFqXenoX2jW6PMofub5S1TCStvdJQLM
2yTnnxKyHvUPQ9azhmu+GitAypMJWtHjHmDlpgCjV0+vogBibpk/iUZMJudQzSd1+YgHMgeBGBL3
RPIJnY3iepyI8Ibmz1Dst1i+9A2DcII8A6r7p/cDVeL9aEj/TfskICNJzCCx7wJ92j3f4eogPe4G
tneC/bJ56qXLDE827VocwWDlxGyRH49v2FXMzkZWCyKafOoff273ZR5AhiiCzgb9dRXQWXhr0bYE
Qs5duv8J3/OkHh4Kiae0NeYu8ejs5VONNEXcLCVGsXUPEc1aWPLifH1iyuzhUqaV1uz3+1VHr5mI
dmk14agseib4yBOJg5Jzgj0kHGbLFBfrDWif6Q7KPKAoDmCvOuTj2i8xom4MlQC3Dcnv8LUR0Ug0
ZduHvd9GgoSkS0cG6mYC/5I4UPJhu+nMUrxzG1f31gHlfLwyeZxqKysb/S4ez+Q04srB7u5hrKuO
Ov5MvdOvSDCWRNkuAWwMGsl85VNQYSpQ5g8WS1caXiDfLRQh9InUTTB+TBlEs4sGp5qSfnwU4WP7
G4Gx4IEnrwP9/GI/frAEFRT3RPHpv7kKd2WjOOCYB5JTXzcONhhDADvJVms1SgcG0gLbYRcyhvuc
VZMOJaZ+rK3I0bAHPw3QEQIae7E4F9zmzwUq6LAFE2S3qRfJylQmuYx/hRkaHVcCtjezCm3ut+Gy
ZEkHYWhHWYjrSL/o44W4mXUIHbw76HoNNqVzLq+mzkCmIsG7B0L4bS+jmKnMFvM3nlTrkDP3Okff
Fs8QeLpyyhrjc3ThwPGmOb7Re2bzSEDlumCa+VXHeJVRa7IuxznlaGEf18eEUX+YEL3FBcOKkUSm
RJlATd02sJ5Hl0KnPR9Nfw1ujZ87Se9WRu6ytXA26t7eBVb8AUg1qvxOuhyNzKr1ZT8Vj8a3zIXs
BrsSgT3eiR1KF3we3qKQyMifJwkCvP1NunM+ER1Y5uqVNGc9OwlC++3NNprhCLFB9NAb0VGfEqBT
fDgdgNcSKR7VKuxOyC/erAbas22WXrqPNiQ1ObRIxtBO6F5YeqLia3JmTu4kpNljciQnFelIt411
Qe4QfHymg6FXGsRdjqP4XlWCC1Wp5az9Tso7yf9jMVAteCVIteJjiAgipZJwpeESnwfcFS+mLTY9
j2XMO1fMZTQykkLe3gXkhTXG2RzIYGY3bcRmrW//tzAkQ+rQuQpGapvEjHsC4Nt8/gBwMgoenNFL
hMfYawnBs2FUjyjhas3fv6wRkeEkhxqVwACLTb9Dq2J6mvD6pbUoV0U1JwVPyndsFWfwYTasawEs
oeWWl9Gkgfz7em6XN8Tx768SsNwlQoVYYIaG+hJ96LnhuYbCMbf6vITG+bIHr0Pj6BmEoCNBBRhq
XyiciBvNDSqvahtA3rREsGfK6G14vlAPxGmZhVUQKaKOz5QEAta7WTg7TkueZFlIg/FWtasdwJgL
LMbyw+PzFJca0SBWj3lrdlDuCgjOyzPX6qBIWlHVSoIUYjQvmS02rcdSDPisArLBLaveDqeU01JJ
Kho73F+T+65WscDV0Ax4FYf31ddEF0+d5iD8TpOpXPVL6wnMqjbDuzThOE1AnCvlvAo1JSCjvCUD
ONMPYYUsFFoEoO9jVdvRLIaOFYSlQPPA0PhvMCPZe29JSJVUmS9MIO1rIeW85RBq5ib16PkSUELT
/q/QEKs5BzQ1YhziOvkYZ5BLquP/T3zjPA1upn6O/Qufg1SGcs6YjCu0dObS407LpWSr38xIiGFK
sQnLMPDQvk/659a0tzcYPVouKsTIJqoMN/A4dEYr7fFEO7ql97Md6pPA3TrnhoXzQ89flrNGpgVU
slZ+TvfDt5lw3MJ6ezA5hOZD7VMwZ/UG8nSUXWTgDylqvqtl3rA83IylmvI35TpMeUGzBzmkR+av
J+QT7DD3hfPUQxLhfAF7GMTC48J8iRteW83Vv3YB7vJYHJ1I4Q4cShaHrS4Ut+PJzXMxLEh5rfjA
B1l6T1we4JcSH5SEcAxwN/EEJSzHUwWMYHg3ggwgnSE2cx+BkJCra6bHeTGweBLV92OEidfkHWAf
0SrQltCofatTPZ7eyj0U758rhe5ZkdGZ7GfhjNeQOdJWAgm0M/FZuVTcL2t3jPapfPoMD9hTatP9
tF7I8rmCJ9TCcFEIpr+VfR9RsIRP0/wXz6r9oX+aUMRW5+EO4Anox3lbmZgH7N3pGZZmiuW6yaRb
ov1hXj8VLBXnEgCGwkGe3CwDi9ffp59omz1WjQTwRExVcKOyVHcIWFws6X1AzGyDq9AXaNwPMcy4
0QoU0fGkWPDYjH4HpEEWf8Pg+VKbza/01gcVxnZoo4unnAYb/aHa/HloOvOBeDOcDG5GmGYvC0sC
IWyqXOBYD8uFDlufV+f8Ge58AJGRqNKdqL/E6Lj3m8vnHUW+D+D4DL0374KqAS9kgRQWISvdmbYD
zNJW6Zf1mTYGNlSGp3urrMJn60JAAEPLPQ8kDfHxg9pQ9E8n8AIf2net1Ex2JCj1cl3HGCMokaPh
E3hfStuusJWg+eKAs5n/DQM5vv0/D5wxg4CrR8cW4WkPPEcP7OE7U1Jb+7kJ3RAZah/TskQzx0VE
9UsY1TXWd+iPxekspZ7bPTL0DydMcI5kdMczHbEkuL+iTnqo6mqReZXvYLAjSn7V/vVRZTceBIkx
005NXIEt4kPEK3hCPgR5YtpqpMDkVJVS5zt9OrfqleQgz90fvKBkVJpqUb7MkgyksSlla1gBk4cJ
DcRBm59hOWn36vZXbgMj9o3rZ9VkmJR3uykFwuwPPVgF8H+lc+pQ3WayyScIN9nYvMhyp9+K7zsy
O0PsbOOpesShlRtuqodu0EVrOB/FeG34Nd3OBcWzIWct0iStv4jPiQwk0Qrt1m+peoQvge0nAvwJ
MRqUZzVdYdeisGSm3jWACO4OjpGnsgj7Fz8zjm216gYyG9WOrUzUO6DtOFAmGD2VuIhOVq+hkqjy
Q5fz+mDbN81oLs4vq0fn39Idua8yAshml1Wx6sQfHd1pE4UAKLucgkqgD0XwgwjZulIZZRjrwFHq
tdxpunsYh5gG6Sp0ZLwdANTnlvGHcm7iJ0aCU3BJTuursbme0qvBoyGHzBPvAM5rIM76XEdXskWU
Wz8XZDHRcyfBvBScB92J3hzAO0a1UCmrM89ZYTwvR037cXZzQc4+iOynMUwRSDZzYj3CnpayZhwr
iO+8pGiMorL1rGp7UgUXghZwH7AVW5dCFyZkZdX7HjwZ4ThhIWF4izoSetqNQlbBEjDzK61CwOOA
oe5o83wd3KsEL9BB0zpYgehaMDQOwi0sJ8O8vTyGTFGQlBEirLqi/ow7g1YRmq3gEiHYugcNi6s0
taQN45PUlUzcCtBEpBDfmBvxqvHYOiSCKmM/SqkNu94PgYqKfZU4kUVR36Hxru2cyz96B4aBVPEj
t+zF2M9SVYvggnWGONopma+augMHXHz2xB9h21679YbNSyVwWANKLknH+ybZ2B311ZMmN3P3AYhI
8JjKNq+yVZGqg/5Ugx8SW4B07XdA0hpU6AY6M9TaYym95URQFmRGeBOjgfuM8RbJa0NH0+RJlREL
zWukaBxAwtezQOyDjrqs60FWBzU2NYXcDvLKzV4caWXI0l3+lYYbTrlQaNctHM1e9nIf2wletdD1
LNAac0GDBgDfuw24g8Xsmcrc7nmDMGL9ehxUTXBLZSA4rsHMsxlsx/j1btVWVikE8IdFE5xSKHc0
LolKngFrXNWFINRxYNKtyV0YwaQWuzF+Wc+BtczRQACq3ibWbXOOLUX8nD1yJMs5ejyrHPxhSfTu
TjUhdpqMkUGs0cwvf361Htbt/d5ADhvI9tIqFt/kL2uBvKRzN0H6NTFXQFjKgYkfbHyYjqvDLJTw
IrtwA1CJhv0tDvrj9O/g1T2QiU9N+xPXsD87iOOKW++FT/EeDlUucBy3RIrsdKxb4YMUB0C6clOX
Gq8epoTrafSdMw86PS3Sd+oX72gH5onKmS6Qum9w8RMQgMuFJ9gakywGov2i4e4ooqBRw4EvOuav
Mibl3qXSz57hL2iDMfxzMRlDGKlFQrv+hjoI+X1LzdfKjt3Wi3FhVBmeyBGk7MT8j9XGyrpp+Hm6
DR3Gc6ipdYrV52jrwMFMGPhL5WyA66ikuKYSD9aK1jxYjHv7otxm+pXjAFyz9AsJdyQJrlox8CvP
FsyzP+Dsujzvs0ilMY+kqFTW6j+ywRFEeWM0b4G5ql3K6UoZsxXu/Pvn9g2ovPQ5MSvsAIbYmbn2
8a5qKVDvRI3pA9+jt1cD458Z78Va52ica4asnEyxIe3fKkdFTnDIm2heySLL2IrukJhrhwDrQacB
Jc9E0ZRaUc6Hu41rczMaxoSFJgePKoH/9CPOw7TAKijb5vqTYjwpdz8e8cj15caXQYSxu1NKgPiY
UwOcfLvHpDq6KGoQycw6rUsrQGn5TylMNP4aeWQ86Z6b3eiNEKDWFZAMAkQhwfyuKa1RbEyRI30G
nJIU6Ciq6vqvDhbOfWkNjgioQ2Yi6w6z3vSMFRBQidma6LxDmEpGcYGkHUAw466dJMS6OsBAn6G/
HJofcTlktEMrNw259nkUT/9tSrewd907WaIpv7BfwSmU1p8oej1Sp64nEabirz1udGGm8JAYQX4O
FCdI470TukG/aXlhS7QI5J12/sX5LjJStf5UfBaV9bVx2obPYjeGL7xjtc57Yri/tye5krLn0FUS
oGy5chWAT0l3qQSF1oWN5n+omKsJogwf+KC6Z4HieOVHx62I4acYI5T7JD7uvNxkCY4h3gSZzMxd
OETOxAzcNKQVrrSMgFMm4BTjEHhDNNKRQXaaczxu8Psf/tJC16rBgxAEdw8z9Pi3f1QzpSBq2kX+
NuYMOvSPHWpV5Veyaxvq6D3GKxmGwyOvAe5wxI+9mR/V8thzehYm6IRw8WI9wIZAKwkrVyQ4HkmH
uXQ2Pr7TbG+24PBq6OucTWcNZOFtyOI6PQh1jBDMVLQaIxpnYGmSJTT3ptPSMMe66NMTW16Sf7lj
V00gQ4gvwcHP+zQjsufEoBJkhwMyTDS7PzJPW9e/ZvREWJPhyruP6rwFkoTpy7aCNV8xppm8sfJl
BefS826N8N1zJ/NIZZ0lq0Nn8Z4l7u2Jbf5mdtmkQ9HixQ42sHRwzpyJokt77FW4XYG3GFYnW1Ug
HVv01K84m5jAGNPtTyt74rwwEbUkKhvMiL3xLBYtAG/O5A76AxBRK7KornVkwLG8C3nXCKmWrJFl
ZsgBW5IyAur1Y5aqTqTbGIOZibcPXbww+iLFHlFVgfU4bcF4THT2YVpIbGLskjh1VAaKwDI4jqbA
c16ynJqeyPZ2sLNyb2agbWMIWwqSeJCRQAAX3AdsGTr+vUM0ek34eIUhSQeg7h9WO/7eltH2bau8
nClKnUM72t2cBCC3DcvT53N1CCpYGHko/PEUOXWNgjhFIj59Uyoah4Fo2K3i/iOBQ9j+NqtRzgJK
8I/Hz32vfhowm+kOGgOzQnmfErsQvTxJTtpBDmr+2n0SpFmDM28zgV6nmJaBxmJ/AuVoVN2KXg5X
pqu1mv2xoKLk4MDqrALzeLNKSdaVCrTng2vprrobIHU2P7FdlZG6adbWVqXA6JjeX5UAlIrlVR/0
QMuhgUbwRXwUsjNT29qgUkGOrXv20hgvYU9HMmXsWN35onKdY9WZozlqpOk4V0G1jC4v9nmZ3Hs2
n9sJW2RADWDG6O8cGZYYlfZ64SQdD5boX/Zown5s0AG1xDhj7pHkxj3s9rqsJHH7zeeJOZ2RYqcp
QnHaK0YTOPxoCvONxDmSjHzBJqLmSUq56i3fddHozf08hvQ5cA09MzYPICTthbR1TuCKp0hHg3z4
5+Matnmrc0SbXQU86KmurkqGAdTfra1SQlZ2ZhmkHZirkU589qdkM8d8cGWLePADRMe7yZYUWRZW
e12/yP1Iwulw+LyDyWvWKBKevVuRimc5+kmhO16rRoo7XyWCEWmw03SGYtXw90YnKC+FRDaIicUU
uidAZn67KDfGJthKeki/HpzSeyPig1xS5xrFGsMD9DGUSZHiVvBDpgeicJe2aelYS9nae33YyDzU
OlQHf/qT+Bv3K8DqfCQEks0tU+l0mlMjPeQZs491m5JXJo0gwPEKXa7o9Dj75lRY27Zxs82mrffp
+3qKlbRCh8OHpNi0qHDCJs1hK08O7lEk9ZzzEsjuDKOJTdG1djhsw6VnVXnGvzfV8Qo7/5vW5SSU
01yRmorTbhBBmd1k4DKQDD3jpBNXqm7/rI+TH+YcTEp1djVhwQPwnADmOk9/949TdvEglhvOYu+H
xjTBMnpCm5csDr5lmf0w0+8H4VIo1dbMTBOxhaqbpVpzYtRuO3bAaLidOX4gqPvlBYAtwB4Fv2DJ
RdaO/WGIOv7gaO4aN12hJPSYMtpyUO2cs7VnoDfql1pTZRS4gM+KXnV2zfj2i/n4/vPYdOwW2ai6
gabT6j4FLGiU6V1aeUVTWmtwwSFVJ67UMPoXK7rqQQtRjousvs7eRewivjkEBIyYLtr95+R1by9H
b2IxmbH2cJuyYTn6FYoYRSr26h2UJTdItF6NiVmv3iM5FqN2P03W8776Jw9Id/LRpfPA0UPFUlAD
D03R5hO9P21V0UBnVIgQWwMEYUrAoAbBbOE/R+cp3iOhRRCosVJYMf22w7Ud/6ktAsQsxLFxTADL
iYww5voC9gQ85dvIIhL1+LI9ptsLzQm7I4C7CpxRgC39Dp1C4V36HlxClH7w7WXHZdxxcRNvdoAn
29qoa/OzHuLdio5w3NYVlXtaZTsPPHzt6qDhyJVjImtXX+PRyFRvzcXZErvT9uZS809QZ3S+tG71
k+38liFhf92riF7ugEl55fCnY28Y1n+xzpVCzEnKrsMa5QqJUp5fz1crde1ZUCsaGrdqyEvBDZQg
xcMsVOLgOzCtamJGY8KbNCxN4Jf7ejyXzPTl036pVEMMmSR1vty4L3n5qAlmHtyLhBoa3DeQyGcG
zMNfdRMkdMaa/j4mQ8/vpR+/lrjToe/wTA1RH1JstD8ouAmALxdhhmwoOR6bu8hgZBPStS9Y9uJj
5woAVj9MK8ec5RGoCazfLc5dpCDwhBCeTh6lxo+ED3nTuNTPxi/x1qvGFH/OODHVkAWytxubPILy
OpiiecSYm1roje9LroEwVY6Fsz4sRQE27JV4bAqaBncy/rOqE0pfpeIlsP2Ee9OAm9D41SCE6YKY
oEV2tGmCzOTeTTdRtlFywOkXaachU4zNj7OqM8z7xQ1c/EgdEmHlK15UWv4R2grlyr+dG2SEt+Xb
j6T6L7CMyGpeZe94fqIt8UlJ04LApDJr3teVVHIku7P1eWGW/xLZbeS0mdulUoX37VfmRTNTUNvW
iBgOUNFgqJZtq3B24HvoSn5q2Uh5lI9q0+M8G8ZAoAv55zlpHniSfegZRdTol/IWb6lEzBdXptzo
fikAnPhcM5y70oKNOnI17GbHfdiTvhrfv81mrN7Xl2+40n3zd2WvK8MPisClnP0aHGM5uW24tHym
CVuZ5cEwzuJFeApKwi4Pnk5sDC+auXxOU/1IX7/7HQ6PKg2+hJPGWXs+7P57K23KAXbYmeGOELmu
q7bVWJ6TFOLWSMClDjE/XBAuWsp+DT5UrkjDD4DCZTB0MeQ3wjnq0FpHWqBwT4ark4DFVkcNybDF
dyGJOHHOFmaRqL6PiyjwURKIcOedXEawKHkjAvQGhqedkDZtbFTjBU1GaS0Vm32t6FCcxG7blGy+
1jvYJvjcldn+IloY7OLLPvmkmSKnBw+ySFkvxC1o7F3+4olLDfNHbEIRcF9NhkMEamk4d/cXRzRw
v701OZLx0k6Z3C4KcfVVy4iGlFwSOctrWMrzvxl9mr7bEkjj+DNUL9ak7UTymIgQafYxfRYUYAu/
cQAZuoYQWVgZDPoj1luriqKBDDD15mK8z6cofGqViFanvRGwyAomczX6yVb3N+A0W48FiXlLfLYp
QMtDVVay26zZDGVPC/cFwExTJBlskx0iHcb93HLfWE1Kk1N58lF2ubQKJQuYXIf/b2zC9P62DZJG
+vRYXai4uB961by+qF6viWMfMNCzOZnyvkhB8unfg3Lq0SKEokhiueEqqXQ6x7w90Mwuezdo8haV
sHTbQaLKzj3pzvMadNAv02n28t5R6TFPHSCRTN3BQP72BQNpgGrZpZHUYfryMbDJtD6Ocy+zX+nh
GLgk4WxywIA5lAx2ppWTuEbUu8qGx+um1vS+YmncE5SkTatDSTpk6aEZ0pzSXQfNNRgBhBIT9nu4
Y/yIvjAgcUg1GBQisR1tv6PqekwdhurNDgQZsXSScXr7+9w9BP354z0NRKW7qz7JasZe0+oPV5NS
LU37QrPR60qW3r+z+eJe5C+Ya9T1dUCSGbFO2sv0vALj/sug/1gvu56mVa5YhnXhXLbpbESkLT6M
AJg/s6FPVzrTGmnWxE5d0/UHID8Yz0cjJzOqa2+nWaiOvWqQBlM1uPTtjEtv3f0o2ccUW8GLBuJu
iu531dZcFeF/cZG4n4JkcaS1y+Gyia6Bf2Jgq+0iW/JHgEQba8JgtUl9lrZv9wi/N+jFt4TxpOn9
FOZtqzPDQyIpcGq+1dWD1JW5aAGqCqSXMuBGUoOwvmOMzU+6cZ8gxmzKQySuqbXbKYklA+W7FAUS
mkCuahN2VCV9z1nCR1FdPtQNaK1fiBg9jjq/weWz92B9qdjr6xE/8bk7i2MYxRUbENqbVGRwB5Or
ZpAd8jR6JlmdBAPjN06GhMT/scgpQYLBARaVDZRHEgAf48bAiQv9/jdlECvLAZgXrj8GE5bu2XRu
q/PYBkKePQFJ1mceskTjvv4R06pa4y10wWVjRixoDElphjrRL6EKHYy/g69lHvsTKNiSC6DtvCfl
q9QIC2RlLcCNX3LDP2ARRr7PdPOpOeh28ladxvC9faJkDA+2VbfyYFMmrpZqpxXRaItozFsc7iBi
zN17Uxt3l1X+O8TKPUjXqZd0iBkIVztrDY4zKW2RFM70DlNWTZl93/73JgO2hDDZ/N0jVknyjL90
MjGL6cC+mgab2Ob7TWF2qpYBnScRfbXHG8XZKx7AegNORO5ds3cbcNcVOR1cIeoyRtIYgMstBxTS
RQsWV11IUJAbfJ9aJLwUE+eUPbZUeQtfwa4BORlAu+F5LYXuVSRWiB/jspnGSgIakzx37lWo3IXL
yeI6eG/wVBxBvXdRI7oPV/waZSl49+xOKJ2SoAp+N3mW8oF9zAHuw1JvO/ewPZOmk9vWgTmUK/b1
x64KD5nSAsiwJrpHhRYLx8i2LPwZVIfQP/i9xg8SdYkSub73POw+GA3twgXDtH8cKUulnxFOADJD
pdGkO90IdoE8bfzJIp66bchYR/qIHDkTSPVp45p2OK2CvxD1UkpQhNzE92LyibTQInt9WWq9sSTL
AeHjtu+uxHJs99G1Gzf2QiGZk+SxQGer8ZrIUWtydIDn/WRzQ3ao/4tabKcfWcSVzZ1sYC7IMDu0
ChaLf/HiiVl0atk33f3S0gWJ5sGVQ8fjJqbr1W/4ep1i2gNnRvJCRlGy/GJACM4wVBfBXNgKCQXP
ga22PMa/R4JFH35g/pYYagRSw/0xSQrw2elnHLA698oC9Pp35Um4EncKd4jfTpkf/cRQTFwga+B5
oNwTuwUnlwFswuRTdDK2TKDLBR4BXP6UKPADdGtHLjP8oqPAiDafvJOLiZH6t4hxkRpKlpVIstwr
Q86Y0h9sI+NbnXAn2qNaZ1gSbILDOuCRuhJ9Ltm1jVqpUkBjsKtL3k7EMa4hFfaBD+SYuXmj2VCH
RiEs8lXatUOSj9h8LHyYb/C6POFMDPXyZbUdLjy5TLKat3TRRVXNR5Abo5H0xrFDvDrZt9EARkvl
Gh/kQO7WeRkwZZYADCVV7JKgfWng4a5TH5tcsmaoxKA36nxhrLtRVa/9oIJ3nVxfTd81BTI+Efbr
KTgczPQALtK3Kkf+x6eDNyUjaGNBXBvAEi/wdKmQoL6LRm0KOBm7Pd/JgYJtDQdcOvomwYlBYVMa
KeI6VCicnbkqK2i6XhZq7TRMlrHdkvICcktZb+5wsiBeg5lXAGO5OLuaGvsEHmIBNW0KYLMPCx/R
o84W+LNqvhYoOsCEi5x9qANF4QnO4GiIk+lJE6T430JLvXutB04xSyp7xC2EnLer36bweOnJhE0e
GbQJ7kPlMcSCPVM9BbuiNjTOTtThSzQ97jo4fPOfzcWOua9Fdszy0VQXZvn9JQbNHzv9Od0kGknT
9RHIgqUrHPa1P1TKoSZ2NVzdLta0T+Tc7X72RfuB9n8OHTbGR2rJKk8GReLouhPspPUlvf4Zcizw
8kc/vDEES9P9XY6ndPcuxqV9ypMu1PgnvWBIpkcKmGL0dopxlHKNDaWICS/z6sdkmS5eSLtgrXHM
6FKwHO9kDMFYaG+yIxc7fkvRqnbPr6Hs68orHyBYHNOAbL2hsRNwO4VxN7OQ2gDwuV83HcaYRxW6
RYwRfqMPOme48LZberfbgD03RLLVV14v/kKEy+coe3ZFXL1nVFksV0lY2qHwIWom53IYAH9RZ9sT
Kv8cstvzO91K99VGM2ZeCgI74lh+dsAdeufZ5opvjrUOESGJWG7v3duuvhpVn/dYyrKrD1Ixgfzu
rTRXh77k2ud2nALq155T8DmH+Be6hMkUjHOm+wLry+RDuGiu1f/76dALnEDMkjvq081MRo7AHWLz
MV4RwKD+KtytPuZ6/bXQI4hkgsexDzmXaJJ1oEEW3MxJBc+6XuL2cGal3mn93pnSXQxc2jC0zHzN
aSIi52n2ckkv0TYxRpu6zYqjkNnKvYi7LrvFoZxZ4ZSaq6txKHSsz7HYotZKFLPmLaCR6ANPm0CH
00FmXzFaSwE2taJpJt+2oPKLipOTTvjLEwNVTnQUKOYQ78j+bF8j7CGlT6MEGQG8ds5h0CCfVTFk
UriSwHWMWIdPB+Dzmg1+hKwcZ1tTrAhEl6tpOrPYyLyb20P/ny9fFR9rlIYR9Tl86G5sQnynz44Q
Wm+BgoWVJA4VchIARyY2/JGFsxyJ3fuOFI2KTAjWfHMTeQdNjcOaEirfC8Zme8BeLSf7fWeDT7Lg
M7LBybYjA7a2/DTDn7cIgoo2z3A5tNlq/gnyLNiJ6hsLwjSGxpX0+jfyK/1eF43gvfOy9mP/IKov
hR9n+Iffrc9Qu1T0kj9Bp3mN52v3JjTPqGiJYw18wa181ftgb5wc8M6yM6LYcmiHd2vmgyfPg6Lv
vLSGmk+XhTLRGxol3YvtESe899l6DReI5cMxYJlXKV/o4xGO/WH9dtlD4mCpXbhqMvY4JsJuJP04
0Da+TT3TcXQAD2b8yAHtAD2/+EmyoOK8qa/L+GrbfiBR6e4gXDdiAHZtYsWyWxNNkjV+yKKHZ+79
CE0E/cwFXsK98pohqAugp++CogEOThtM5BpUnRC++w2CbAcerns4Q5wOjtx5XAP0dWvTyCKdISBF
u3HTvDwH8liwr0sz8NrFS9Id1t5Baij8eenm9nJ104/TFP164TewCJ9lHBjGt8GSFtDrEhu1+kZF
RIYLt4NsB4NvvOjJuHIhCx5ScXKVupo6kVWX1AfclrMfL3sT2vinQ1DEGyEk/LE2iLm78NG5I5ev
2jRhjPx5QAkf8zNJ1pj0jL8j3sfoQN78bbwvqasgwp6cPb32PqoUDbMMZ0jZvDjY7k8+hvTM9Ihb
G40FszXa28gwpSusq7vd5PGWOyI/86WZaLYbG09HmPmsRKR8CIn07OlTxA6TglyDeP9hvwHkortz
XF4zQTvPMvylRYPGqFQOMNJ0YSTlYkunO4Wg2itpWAnRSY5cJ7mPOmZ6dfSjV2E4GCBGtApf1DMQ
UmZUMRttZvKh+5nLdq9PcTmd2IogH9SfIetyq1UZAAoW0dbxNwgln6OgnHxrostRCxhv98tcfetH
keAI3hWzz80M1roF/q1+Zt/E/6/qRbbtbyd4meN9A9DoMRxKMDsFlO9UC3+z6DgCc6Nk35rVlG8e
ccVgjZ6/GhzhRvbVaZLqDUn77/7Sbw6/K/5jcyE6zeTGtkWJIS9MdmmWjHELTx2hbUjvlIAXfJCL
uFuDf0S2W4aaNG7h/l0o4hRfaQ6oJnRWYN3JJBk86U4/n5AD7o7X+FIroUB7fs/Oad5c6PVQjMt7
x9ASsK7dMSfdv4ikh82i0lHlcGDcIvsObQgf3eY42lFGKU6D3GVVY16vMMNm1Ya1ykqNV/BEnG77
tRkvqKpU1H5GONZVE9PqqXABhDDm6iaYkx91wFXTPWek/R0XxgLgTxRHfPrF/RKEURorM0/0zVLq
/AQG14pzJChpXzbO3rdIaCs2JkRVm+XXbAfU3Fjus8ipOtVU2meiawwJZCX/sqDxSY9PgFhNmQJM
yZKEsICHMs4xP/csttOM36s7//Xea/t43QJDXPyCavC6RyEh6qRPiLC1tiIAZgQIySVXcAwnslhN
boAV1K9aO6rSL8ptP87VwHL7Rzrgj2MsEdmOnBN7zfbEpnaNdiFva6XluybOcTHLVBvz8ml4pT7T
y4es3/zRlGDH+ygsk++qE/AvRp9IA3NDoS4aZyp2YExFYWYWGUEPk5xIr7LUq84wD+BOXRoaLgqG
AQEurVRNe3dsJj/MRMavvgjxa6UC33mHzLNmMnPU1WgMeKCiEb5h4l0yzASEm7iotblZSkNrijpp
uBxmHGDyTRiY9BH34VWvgYwzHAFykueSicdR6tgmDqC1Ol3fem6OBA0JolMO+ePm9CRDGO9wnGf1
Zh2v4PoulDgi8whyA55xQ0H3sbepTYav2oTKnfplBJtASpRJGO+/DtW/xq1wxD6NFe29rLZ7tlnW
Nepweafi7J9c75z0PL/3EBsUnQeoXzCKRqUTqi2z8T9QkoT3O/8fn4HhcgigsxZk7N+hrg94Qb6p
nPbtkxn8kbUgtiMdr45Vy1mqFYOtiYFnpTLhcnXM3pHkPypXOlQaqCB24qYyOQM4NdDNh2OYb/we
f4E24lcnXQA6MWvYW902+eeU9UgS51M6etbTUfIzaGni1WMzBPFeEwaOtNHEWEZA6Gy3hw2IsH5A
T9JjVXjsvgcvuVPcUtjSNxlrhf00JLNBDOVVMDkaqB4cdQwzi2lTPqXmGPDa8vSB0hGV/XlgWwd3
3tM7WeP3jzBDpd4FcJ5EoSSQIs2+q2Ibt265JLAIvxtzhQ4CXO+ACiWKY2HQfkWWH9/JzHxXH7GV
iDL9IXx5hh1UvZWpDxr54ysERUb3UHQjpjg+MkvVflijAtPPdBfVayXGEv9/f66/B+3f+RXjxxXl
AjzPyl3qmRfsNYCW5PKGI05wA//66ZtNGqoPeyOJStEOMQhDkuCTtynhCvOujpX/NzhzLg2EYQBF
F25qHhWTP0osUWfK63AS5W5zH/OtS1az4xeBbhQPfVXIAZFJW5zGj2XWXXrr+webQAm3fEiz9wrU
rPjAXijZ/cCDrYO8wzQjrypo7lCUWzJTa/kaMK4F8LYzPamIbWFRGFbn9NIy2th0VuCrQ0ZvFHqH
tSxs1afUJm6Bb5HzTGTbIkZ+119qOn18Dd1s5nPfITnV8LMhuMikrYpOQBOvFa/FhpWHZ7yqvvRW
P/Kl30a+Psyf3CgNo/1H8XtNeX4K1QOA5sOk8soro5jrrgMBE5FxXTEYfWkxsxfhVkJtF26MfTjt
PjZGOj0TFWDgFc65HW5F8Z0EgBIC+L+LHFOru6JJuXdt0VFRnNm1pQHgDkCtsIAjSAnAjdD9aS6C
zYGasgqItNYg7NqP5w3zHGsXyHbf9TkyZNW+++7tF2i12tr1rldeY5oS13tZjJ00UwrsdaAdZizF
yzMkHegoRSJR+EJSe5sW3C7r5s2BIkcfQ8D5PyFTKANvTdYoJ2Ax51ws8O/bM7Varz5v6baW/srQ
sqTo2ssq18wfhyXRNHmejlw7bEK/TdN5xJVCi1YF7GcA/0x3iqPmeeZMyJqFPst5F5k2ry7UDIFi
dWrjEpUSGWnXmZeEdzbmOgyRt6u+/GA9akUaHGZgyoV05L36hrssOScfPq1WmUraJCZYHqnANlX2
PhqYhSObicebYYyekP4zMH/cz83jspOz7q2WEX2SJNPC6qufReYVUXZnVYSGlr9Iq4M8zo29v35e
mFi59OK1xBmDFVxFMXis3HeQmjoQZMG6Qs+FzEGn2n/CoV1FwaOF4mGRUEe2is12P0fIdaybGGlB
5J0jzMRmHSn2Q8paBqvF9O4ow2sR5/oW+4gTKeGCwTCqiTYmcLQDcYzNDY1BPjbQe9NUxerCyY9w
w4KdS7zt4/NszdIulJJxT/OPpRGWaX9872k74hSI5hmK1VgwlzHbN9v82QhadXmbyJC9DrARbpPo
ZrA+7NV6a8dQwDwAZwwP5ykrVBVCvk7va8+EledknAQu1NRXLD5eWb7IfQdYAUPwCXG47HPCtfUn
ZUx9KMmqVye/T13RLSc9Id3HgHoV1SxeI4HYhxhNg1nJkZyp1latihBLUNU9Bi2RgtyG1J2HeLDA
CU0YcW/KwBRV/RnKL9UCffjo42lUtIBEBzOMQlNJ1fzpScjtmJGLvkxjnRjtAOggxKjb9zV+V0PQ
swtLEOcezloAqfP/qgXixbX9wAIus0ZK3kelULJH1eQdqjNxgVrbl+7GdNCRrCR/qENArQHie20F
SIMxSoRndSTwgPVVxvIVUZDY6MiTmCd2z9WWiqVnPhk9f3Poqmw4JhGS2lqJuPV8Pghf0SRZ9SlB
uCJgqthaGkdgsidgSL8bjiylMq/3Buu5lhzEptvZVDGm6lsuPa2ujFNJoc51erya371+WKLxAaQs
l+iwoQtvIb/ULkUn55hF0VLFLWw3sYEzDLBYSl7YLuXD0fYIavmDOX4qaWdci2QfQAJ8bJNh4sHA
Hg8sMUgVoV90HYke2c+RVIFQ9XOUB0hiuhfgXikFibKJZ+T2gT3qderHyTPG/V+aCbhmCRHGwR0B
TZ+h8z4s2HQWhFtVSGnVOaXF2OGS10aQWsLVDChAhA7gJdwTUASoZqR4TFN0V+DybD8gCx8CUs0Y
UC/t6ZvYU23lZrlj8CeVO6/eErf/O0zxLu2xecy21fD6JHPJ9OXuxbP4yCmshtEsDoWi7DY7VkuC
i9PsnZ6RZ8XDsNJCKLPLYEqwU9mQsOY69mD5afcnzzzL2ZWS3HI8YoGVhBfjUGfcSLMKbqeknA+G
rlGxsAOp9tLDxLqqbsW53s4UiNEHjtiVHrGbZ1/3c1/3Y0sbUWhJeTIZYTJjeTigNyO4izEQbRm4
QJJATLWqKsqfUmoAdixvT0Zk7ZQnNsZmu+CXCs0KE6sbWSmOIjV2xB/x47c2sVVX1ejQZoOZ4Z7h
eScBiurQq9RnNbHHAZ7qdLQtqMY0BUdWgnnzpAOai/VsUR9ZKlRFLznMZpwie1M/gsY6nft40xq6
7s5IKZnvpxPw8gtiXZ7uNGTzxvGB1yH6RDBaSN8fHuAMaIqDRjDeBolpt27yRPCxlw6AMOBNqdrv
4ftgb9Pg7tw0WpE6Im9WvOIYAh+ycQCacPTUa6a58q/+2tuPqvp6z+xtM5Igq/dshDLeteDb0yJr
RMkipWgZzNZezsjMYWHIfKwrEkJ2DGk4rWUieeVDnQL+Zh3d2ntL9SxfOPXQICLfwzkKl4aLdn96
uckoWeVOvIQMEQd4Djw6xMfGkh4SnpPO3yVt3hJBf3TCCHsmxKCH51drntxyvk5guFj1xPKWCqYz
d+1++srrvRYnmQX6bsDvUEPGyYMkm/J0N+655oNh96D+Izg74eYPTlBd2yKe0hoUbOrv72ogROOX
U1U4skYaasFSUehDiyypzI1bliZ5abMtDJxpP6+EHvsQwajNKLm50b97E5WFDVdriR0CkBdmiGaa
V2WALrOwN339BIC8XTi6YeLLYlL9mALuq1osn7gdxwU4TuXyymO1IRr56aYLSY6deaokJP4FVzCK
KT8qiPhHu3Qp7Mbr00zWDuhpqMw6sfKg0fP1rnphQez3zn+ZyPRjVOkpw/CjlQbg5wtDBJXPJPLo
61vxYAW1roPWVNFgDuehK8birdDkJl1DyFQ0vZ/UVnLqi/zVoVXRa9DFKfKFbOBS2/zWtQx4L+Yk
wFskHH5uXJ2F4XJLlxVGrcjIuXq6/5WZmX8g9fD7sklCLYwgTLSAM3/jV2ErREr8OMpXUFyhjv0E
+M0mKe9/GdmIx88QYch0P90ur/irgOPXLZtv+I71wAxEpxXot7Xz/aTXOEgi6KOY/gZSXK+1qA+X
XfyHxgzijxbjUt+NEn1BPS/auuUMxAbBuagSILIhZR2T7BxW87oQ7sXakfjlcWz4PK0mTa7ENIQ6
isFEWNV/YXyGpxsWhPAZE0bOu9xsdCTv87QxacTiKEbMQOqZe1a8fe1PDh1qyIQDGpMMNPMcEfAx
vo/cptjZ1XSZ1ZPc+AfNgq/W3zEZHtAtWFwW2ZAjvWVVVt2/Cl9B2b2kuKyHAvUDM+1Yfze+e7Ab
VDV4TGXhWEofJ6S6mIHS7yYgn6hqiLOJ4Qz5sLJErHaBstoNARAFcfcHpmpzv+qahqgWYeBIYJrL
Co+ihq/mLuAvQAoAqVgJxq8727ny7Wr1i85j0FgnZ6HZYISWd7RcSys/ipB2edi/Eci/TWOPg43l
NLgAXP8S49jZLQLo/2Ua7S+x4D7w97T64uxAmp9IfroUqgmhiAvpw9L/Cr50ileZvWRjq4pXOF+B
38zmOHM8d52KFUCXmcf1vfQyY0iYYOTjpzJ0qDf8kyRHOhe69sTxxMRM/c+XE6lSS82Q6i9mhjtB
7mCkoJKZrks71kj42vWaga5RmaGLtjmgRc5v/Md9WFtlIBXSYvTeAdhmkOFVuZ03SSfGlikbdW0C
ErptDLxuSyOvihwho6trel3KSmbwG7gw4UoxxLGoG5QklNXWh74UtFpYKt0AIPG42v8z5Yq+U/DC
/Yz87phwsrWuD/Yx9NEiE0FF3N5FrUV2LOaTLLKHoFK59U45uY1pMouL4BBHN3VNbnfaShag2LTY
U2YKI+nlnC8xWJFvEMq57LbKWxxXDqy1OO+3ydsk09q7iZH7zrrCIMrOH5PiLTSqyu1GulXS0usO
JLkjF12r094MVwLFOQ8ezzvC7q/Xk1YaLP1e8/pnJktxOp4cJrjpjBdYgkAj7zv+BSryl46JT+mY
glLqIhlBpc+wnJrhx2Szp08iNpu4I2tM9l50F9OdygVsrFvmcZiLJ4cSu9yP/49nGyMyrwZt6Ruh
mSbPyh8lVrViVQ1Hla2PsfEBkppofdbS+4cB1xTfGy6fPuwgdMLKbbFp0SlpOJn/xEQ1ouw66hHT
GojK1rJiAMveBuLokzgN9jOfxxhzxucf9CtKP2uWvjR8qzjZKwQrHHOKLr32gfkqYTcaV8qqnbNG
x4ohsyZ317bvv/LCvJ0ZPYTf1SXGX5RgSR5wYWKRU2IdFQZztWBdLsjqS7peIdycBi+eXIaSbWWE
+TyBeQeOPnvPYVgwj4xIHaWujzSe+Z58vWawhpRYG0Z5TZvRjdSYktspl5XIdgiF4zrPmeNVdHjV
TxCAYia/W1qxHIejswf9NYTN8dPb+J9LbGR58G2yQhhpEQNi1jD6Fxexpp+B65xyJPOgT75pQCY1
QtmdGm94gG1EMVOr5W43A708fK6ZDfux7xImgtsp7tNOxMDXsYwfK68wXWZSpxcDy/B5EgaZD8AM
+umac5/xGc5aKgUWkZYDYlHIW2pwe7bTTgefFawjMtfDQVSQ1yUVge+c0gGstD4LYn2J7+zKQij6
1IxsFnEURjwmYVStrYa4LkfYN963hN0VwZT5byETI7qufZrEEhhXBK7rZ/Eunu6YjeLJo5SawWWG
fi/JxTv4mKhdO5aX5mPijzwmQUPDP1u8t3S4yDka14kes578PmGHiCrylE8J6NY5SPzuWtnt8e7u
/FdQS1wPZHGcRgvdHnaLJDSOu5vEllFH5L/KIbB0OQhAqHP2YS0uJoqWfN9St0BwzFxgGtARvWkl
RaYEZCrGyPOCA9vY5XKlT7Yx3GRkgJMiBJAMDgvpjsf5emX5M5ZYoBUZJ6YheaMPQ+TlV2InZ7m7
BzISk9RyMkWRciNL21wEs1DrjCNYbYcv8tAPEzvkCX+LOrcWJGZQHieGWjpeMGQZViOS35SB8TCo
CJBYiW6vlktpL2CaidXjJU6XRavGF5xmulWNrrwYPLv/ZVPWtM4UWODZ5NWfzezSTA6vTtZHzarf
+biXeVPk8d0DE+HfAK/QkSl28kswK2A4OLYojXufwJi7JBVOjIAC4i/e2ycYjr3wlhUmHNfpy1wR
QFNzyOVqjysYuX3wpIgfqIRLhZuKzl4nIK9vrcs2jETWc970iW4vNjcitQpUuxHmxJ4tpbKG4ebi
oNEkC5Px+hFSCo9Mh2hr37pTYaGl2biEgn3EU5ZIXiWs4xG91Vj8vwEpxEgTQ146o1gVCJaGyS1p
cvtZH90rC1s95W2nb6ZwXQOKLgRNyrYcnyF9kE8y5P22HIHi5zWk29TDYe6xu605rq0zfxcXv/KE
ELjVti/T/55azM216HksRFiDWcieY/lnWfD4X88ZetPSELEYbmfgnEz+vLkeUWGJ68+1AkVCAaq4
GrHFCsf8uLaB3m6RpETpcSTM7c/9HP2s5CNWL/R2bJV7+I7IZbnYOVTsHh1hUHrrUKEBSE4N48XY
8HYxnely3gkv/A3IWU0iJDzf3FXKiEyj+IBxDVmJQ9HPtMqRzRQ9m05uabpA6VeQVeplu+kSJ4mE
mEhJ2zyYP1iqFGh0NKfLHTYk7KBG07CqLw2aIKV4gQtAF3johu6yOOEBACnydBYxlyp9dogc1z6U
zeauFPdcbsQLGEqimov/e3gr+gFBvN+Z7X1ciUfg7mE8SxOh2VTsg13Av8H5jZNZaUMBbCpMYCF2
wd9R7OM9u0e8cGLHMjBBYXWtQn6lDPOxUPAxFSg8utuAVx1O4rJ625zBJyFjSmmR+GWcSPio7kyv
AporQg+3tLyR5ZQ/Lfz+DfIYeD8m57OeWe5MWJboy/1tgDQOMfYu4Y+sVgo1ihFmDUx2LVzE5T7y
hzTC1GwXq7vNXdAC8Y559ew/2lGEOYYvLrV9PxDQrMZ+oanadzoZxN6sXFYjPhlyjgP6R6nEwhGA
1KzYwXJgLsXhvD7A9PUrrNFvCqBgU7Gh+wTFXaSo4HyfoO8NyQjRiHxrSAMwvb4QxX9w6F8vT0nu
c+Y2hyZcjf8VXJWX+Cc+Vlc4mLTEz+YmRy064Rqy9fRnywVFHBCpKYe41l5/Cu2/0OoGhqP0ywmb
ejt5wmrtHDGmHkNG1lgaioRKNgo+CEREnTp3fqCswdbKjUjl68LqlUq40Hgw0/+PvsRTJP9DdugX
Q8bN4To35SByJNPoSQoxTh4Vn3NjldkuDL3kgkhr6VzCdvPlx9HbhIVXShtplloGNa2jPxWjnGs3
UwSsaCieEtsDkWx5GsiK7L41seFeSmPas+bn3xw4vl5tYpmJmDMXAipDMKmy8oQMYjYuvJl7XxLa
ISNS7YLhOQT/qux4zPueB1QkBOyEv8xTSzhLu2RYDRB2Yw8qgZUQOYy+48zMrsvoo57/dIlU+2Pn
i6zU/mNgWnxh0znn84z0Dk2LMxrAhBFxozw38SJgCXsi4qRob2BUDv22GVhBZk+EU8GgC8KIgoOW
uvBcCHGQRnclY96EEGHzfqZr23cyHO/WCHNVWbY7IxLdlhrKCxSoO6RlLb/uvWyGLXCnkdVuRw6y
zXFX4p5OCqWIXFdd1D+8pwABEssya9GArYY0fBzFwbKMHMvnU7hnEYWJVLxx0VmHZI+mJCT5xngm
ibLs3/SaWj4XamW0PV5PxnSP8Me/UfhKoX25dlwsUkDAg2WsKO+X/WFlLCi4OsPZQR7UK7FrHLku
A/zXLmv1f79dJyhnp2lE/wZs7QIOLXUtQe4Z0t2lZNotoJYNkWITLqaTuFgeyRwJzvJsYHFbly/n
g+qlRAJYCruwmyD3U/TshWt7e8xL5fPdpC7IgMNXvpMecQxRrQ37oLUyAdohldvynW8+HUnaRKUK
YqgaKsBLK07fToSa0GnFLlxxNMJRWqeXDDD5wZ904yc5EhBDj6Pld8g+nJ58aLs3itmOW4rVDGiJ
3ppLt0kKhWJIKSBHLxBP9jRM+yFoaT0mlllF/SVqm01GaFCfykVYGC2ID+qXw1CiYv1ydn65TLNs
/WR1qutNT7aZ+FooaFWdrEQu4XKPor9d6CoYFHrkfHQdSFd8xUiIvVwIPZedFgIBy4xGKSQ8w67h
ZZT1w4TLMfYWXt/SSnyzvaoEdyOGhYPMTxwH99qnfutkOiPdCjGPJgIZapPCLyvsbfVxDH2sfUsf
rMRW9eT4+HoKddOxZGLhlML+0Si5AVVEqrt08WzqrkfIsUqKXjmolcHZgPTUwiGf98gRdCIaYXyI
bIdIbeoudEO90V65IpEce6c72xCJlBoyAvc+MYQ2Pb1n0kbbvE43l//QbjCTRq8pcU1AH0SOvWaN
MEMOUu7oqKwAtSPAQvtGX4cADtDS/IFKHx4bbqcMz9pK8EWLOVmkcT4cVEqWCANXZiiULXeJHIBk
PtLHwjlz+EkAlBdbZHtKtlNAddcButf5QLNMjBEPD/J+thxK76hWB2oJLDbBlA/16CzdoVbI4Itq
2Tlq5xSrnXWvXFrJbsGocR7hFoj7hvOYIsNqA+Vadgw4y2XPGrT4udhQ082/FE93y1gFXnimVpqr
CaXRWkb6GpXvfh39vlX1s0VHLaVPKRk/ro+wzuERseJAnUQ9jOu7OjRSRiEwAMa/zuCnTEISBbvM
tp6eZ5sa7XT+gj0vms/OY9m2NlOhAgmx4CbVaQC7ipCr5/1q14yXLCEgebaYHz2vw4OiaLQywkV3
uClVeo2fr0MerdaKXFEJl/3riAu6mF5MqTQlt7nFlXfHMYnLPQKklfYJ6uZxcbZViCofTr1eBlCN
Jvzi3DA7TRP9pX1JFAyZAHvfcPxBKRXkTyA2414bGH5lFQS22TYwyhFU+j9cs70vJwV5cEL7oqJW
8pt2OoxhyxUuwF/y+DsL3mHDD+P37JKPqLaVAonjKRy/NxZ8RWlEXFF5RsY1eXimordUEUmZKxdc
mARLNfzRpE5Q883wgS/0lAa4Bgea4vBqPi7zXslprz7P5FzUk69c/vlwJtHAYZBKxinul3SGrTPm
4zoeH9KmoTfmSc/6MyNCaqzLuDS0O/gBCQav5rJ41FTmHVNSCqxG599xSvNDgj7HEQ+DptsNcMtg
4cOs6/cC44BReRw/6q4008CLuYXQlEDlGRbqQtgKQQLceQzlx7uMbSQWgZAOFL6FkV4ckVOMQQnJ
78v7uIvykkaD0Zrmr7qtYNt/Jm0tHeeRtgOCZD+XVYt7sd6B9xlWR+e8rYrfaqC5zlDJXHs2Bkay
N+yCnJF4D4U5smQwac7u2ADQlTIIAinZheghFxPTwXSX8fkiIcKtuIzFWgwschZZnd21t7wtsYr8
t+vsttAymMfuqYtR2wb6l3aS9QW9mUtifVhqJ/4w1sKBDPrvh4wsFFfPjtXmLUUlyh97eQqCWaa0
/t0Ionyg9wFn2/mIBXirgpKqGnbhSQPH3iXQ3vf3XVZuXMLPCgk9L87bqqlkGQqe6UqHrmBSCzaQ
a3XcQQmRFWqK7HfNki6oVzotvCQN7m4f1oMy7JK1NxlA5Q9A+moyn6TfCR+xK0JSNJTZGrLJnzPk
exU6Bn4wzHve+fXiyFrwsHlnXGY0jSaFbJXxdC4WMj79dI8vSWtubDeuwCuK0SWOYzG6MXeEwHHA
lsnW7o7u2aTSzjQ31alHWy/X0n603dJV7Ao0iKJw/wsk4mz6XCkHb6MdexK+gCAGnmPIJguroTqc
GYbKbds8iIJ2AJF3fdtNf0bhs+sJGJ4Lnt62N28ld36uM1kSydBaQXQ9fMBWNqowKit/F9mumLec
XlSqEVu1Fzn6k65FKOEu2ThmVxQHxbv27PIRLJ423FSFRvST6kEBHPjIIGeEWklPw/ys0m8He9XG
ThopXf/6f6GzHf8XzgQlN2w/VUmD5yE8P2AQ8T/L3HgPph3G430Qx6S+l3FHwPi9hmhl9FPzrjTg
8eG31I/GxrreXF1Ms8/76ha4u4TXWlUX9OavV10qkdzVaOMgAt2z46E9NiPbs4rb/ZW82No1M3eJ
jmbGb4tDj2mqlvWHOoOMPmjB2XmnBJ+iH3yxqObsDkt819BPrNZ61d9tMe4USksWM2Aw1YS7jCAK
/a2ocQc5dM3qmTVYiuo5532gXin5CxDV2+/MhX2+TY/LUW0e4Qzo6RNZ3HXH1jBDavRat1teTLPU
ugIVS0YMrbevGJ/D2w5frNDio5BwVhDc2nRZwq/PuT6f5PNE61qJbQ/CJQ7sMmBV+4dsc8xf5ew3
ZgaHtfFgJ1syOjKvGg58YOVkCKI8dkYeHrvjYMm7TSramkJyEM6WISs1AzXGdyWw8/il41OsrDGW
qg5JXLl+mS5T7LhItdzbMi1EdeDnvEzIA9Ipga+SIutCQAfkm1cCD7MW7LGm8ii5IlLizitUbU6C
eIKy880HWzIj2WEKbDw5aq547j4HpbuJPBRmIat58eVbW73Gnrd0Xghh2MS1Pa1g0rIQ2i1VuUge
v5+VVE31Ndj0YczILbHdDRTzh4eErEa+GiaKKDVTQHDxc7/A9LS+U0+vOJV/pY8Jp6GaMCQSw68v
4Im6T03v6y3XW7seqxvO6yutwqgSazrdadOVMxqPDlAeOTOmfqwoqfUvsdOajnUuVEs2RrUj5fzP
M7hgdp4WoEHBTZkxFhXbUovAGRnddfy93dl4Qojk3gFo3F7TmRO/Cg9xH5L8KV9cGzRg5gnsqulm
RIBD3I9KPzZMV8iTjvvMFVBaV2xZRL+gglPhRV2HMcvwluEbUJBg9+f44vMhHEviF8gwu2ocIu/Z
p4JZd+B5VSBstzP3xeep9+6Vrpow4MyNN3jBmAbndQqpUCHuqGhUScfvwv4mFlUH19Hk9cRNlveU
+p4fOAvlGVP6APxcHkpMU5EdYDfi708ccakKeiBDFyHw5xfitid4S+mEizSw8wXBgKGKsdWznCxY
jo0wHE7KirpeQADqqb/KgM0HxxJg4tuAUkTl9emSKMDL7qgo5Wh3NjrNAQZKJRZHyyuxv+wE74HF
utSNqGmh7kMqFi2nbo62s/mfotgMu/l/DvgItzH+a/JQrdtbykQT4CqmIc6d1rCBMHBDpvBoAjtf
045e3+HCkZ+TvDl0HTM2mUwm3NE0z5s5MQ3AHnjfGGS4+KxcOQ16hP1KalDQBOdcEeSxpc4zxiyw
rEERoWayDpEBjU1rWvrZrcC6E7F1iRNbOqJw8MPlwEiy71PROG6076DFrPJDsykUhCbwTfJm4rQk
J1OWjxvsV612BEi4LX47SoPb6c+j+/k2KL5yJaPUe9ShaKU5MrM4ypwhkdiOL024rsmJhfdIUbNa
ahCNDYGJb1tZvNa9pV/xPdnLbwvwuDO02PoWVGNlq6C9DBsTYbUnmGeurh+Fx0WQxKfwvlRUoEmj
raCOvuGJdK9jR0v5e9yUM6Atasv8sCle/SKA0+QuP+o4PToYKYsIzmFz3OcTS/39yaQ4Pke7AAZM
T+EEnBSE0ZFxqN8pvc41ELpDH3nxSO336QZAIeLKb70+ttFYHjX8SlDA5k7/7yISczZF0U5dcSeD
+/fhR1R/z4pV2DWow2epBue0oTOz6Tf8ZTzztDOqnXH+ZVac7WCEXHUyDI/wC7TvLgE3TxKR/z3X
e9ucRbDVEA1qLCyq56/pjCw1M6ajkAy5cyZUGfroXZ8VHHOWGe4A8qReumBXiKhxA7ShLWbFdYIZ
rsQn7szVSdm5dpKWUcPSiMhVPglwWWD/b8KiewgtK389MMpZeZg7GH2E3rCCGSuA5jYhu5iSkQOT
PJygf+7W85JE/o6+fGSa6Ux+7tUNVJZ2YTC8rED/bYRzr1Jx5Vd41OjUvjX2fk2v5bMkW82dKlqR
BQlYvMHp7C3gxUDFBoXws2berxHBNu4qErti+W8KwIyzYUw0tqsS/pEGsjXh3G8Gjt/AdUl+AumU
buuygdKyQ8/36JS8ZMWj7VtmObZQ/CteE3Y4izyf77ja8k0FOiuMugsx0WNeI6SFL6vNnwAM3ioY
q3nnHNsf933x46GlZrFRmVNFi1hT8ec3oWLlnV+S9hINxAZGdD7bQET4Stx47JGtQ0Icwx65e9Lk
nuoc/6lSevZKmSUX4aJynIMGwywQSoltyOa43UP6eYdhEaeFaNh4y3pV3KHhLmM3eT1dltQkSyPZ
+ZHXVwE0zTgpxljLaXDA9SlyKUSLxHb4Q0DDa55RZl0E2kQ5962/43+s7Mz8C4dwePhrLqIbXcP0
di9OsO6ccQjWHQplIPrrldpXrNQUnjSzG7LiWmZO8RJHiTYBmXCA0wcLIpQi8VJjI11K5g+224Qc
gGQxQOU+0qEQpR7fIqRBfGSJcgzHW1sIpOFwBkt7n7yiLu65vwFKmYGwzQDa5tm5B7ckAi+TRMU+
LRHZKpsOA37ZKIhZcu30rllgTiPDmSY18qxRq6Xz3hM7BegvbxZXGrlacEZj2TgS/sf0g5C+7vDW
m6ow0Cc2A/xorZe9wPPN58EtxuL7NYETfc/WUTkIPDIGOfC+TKJ0YjmhgdUSTt4/Axx05uXlOHmx
nm7pc9G49jxogKnI4PrpFr/So292BeZ9xGu7zQvuQ2Yr/G8tuVnK3th5XhqSpUOMG9qvIjFV2AaJ
at59IY0+NtF8GwAahpEand0ylrTDXS1OzDjh5lGjo3t6rITp4bAqQ1Qi44l/ZXXaKHDp8ykRz6ZO
DEGUQ3a+xEP0Evrd3abgEEAQ4VcJOOhui5JNWROUkUwN5Yr3LH5/G8Fe2MEQufKLOPDZHrpF077l
KiPNb+LFyei8bmrFFDpxp4v2I0rz/Yo/8aHkP7pPQBoF5P11kblwCbnMTfO6DNItHOV6UwiSgAE4
liYozXdPX9wVKIYHeGvhZnh61Hu7imN7jYmS54DwBBErnpx/IS50OsNWxtGa4pnvQgpTwPkOwn+f
H4h4ocRuhBsxqAicNLQVkFqotkm+ByzQktbbElhPXdK9yXIe7PtckCNtNM6amzFpOGYebhQmPjA1
eeTH9PaHXkuZ9BjIxA18rwe9Ex51IzMiz2P2dObnc14k6HuSuVFKmtIy1ZW83s3tEFjw1hE7vnTy
JqPox6vlnLoioSgJ5U7DsyKBnmSALBR/NDPhucuqwkA2+yVobrH8OxIcoemmDp9C0MRK7yxwHN86
VSIKv/Yd9rHNXuMJHF+CpvbbmltiKhKYgVr9euqRfWEAjTy7yl7So4oem+ljgkBcZC4mXM1O2N6n
T49cUub5E35RXrEJovlZKoFhYQ47udUyBMH8TtcgQnK/maAr+At9yD1heqZ6I0xumA825y/2JZGR
D4dzb5dgG8i5OT2RnFYJ0hyqtZnT62RORnU9Mem5X0PYO5z2fpddbKXrWJYGGiGh9jDUr+x9azC+
Ld81GPGHcmaSjK32MzR+RPbf12VHp16hkLyviJqZyjxzPkSilBYB8JFde7EouWQR8w+vW0zjD/ef
9BWAxIIJpApnsZ3GCiLf/GBbsBGDvkY4t4yaMY57/vlPpxU5nQ+/84dGLG+PyNxce/efEtuhUabj
VLef2xpimMG0X/XClJjQKTVFXPbAJWj873CDh8LDDxybGqXCuPk0wX1NAWBOIovYMNzzmSmR5NMW
9LPjKmJjm1BsWpRnVFUQnFCxGeFWHjcsZcY/7+d2fY4LCWoKOHBtrAJG3/pCh1xmelIR4LWqLr3E
CDovBmU1rkZhSI9yvEvvIhDcxll9cGbyAPyC3RUIRWeCXappJcREwH8JdthEmpsVdL/hHH715j6B
K+xR1yqdujX2jQyb7pFwaWMaCd5gnZWu+Io+42XjzC1J5ULdX9nNNPRGu0AFI+ZxHPvy+LGKd7PN
I189RZ3SfQOmHcRoiXMBUv/BIMCmR7vK4SRWF9vuqlKhOXFLDvxCUdqh7CnlU9/oSwbfSyZUs/5u
sLfw4Knvc2FLdsQFXTBdaRSJ/ZBGi/xgIccJQgvfE9SDRgMwWD5rlr7oJ32SaKGS/NtbW5ONnw5t
4argXowF3B1U4m+EMZIPdWNUW3em8g0vXAyBgrtBcZhoG8SgJ96klwzpcqwTZVNV3yN4hAf7+a2/
TCC2U/oCHaBjXguTZK2gy3mxAtEYFti8VymBg9l9rkcrPXM/2A2s8E4w5Pfxmnp39kgZiweN+Fez
wMDFNyfXgbUc0+Z80F+XXZG3Ftbb5qIISTJHBpV7Vi9qMPgIHgApjuA4ZXEAb/og8iQv+hJQlW6Q
hnXNwPemVmPJjtQgKWmZXr75PESmykebUyXr+W/BY0Q16GZaLfROhPYxgAoj3j8oE2gtEpwoqYAM
Bnai47ofPUYADChODwjPqx2ia7cwEOX7gtFoFDC9rWd2/sA0jb8UCQieBykSpt7lucXyi90MLKac
0cmH12WNg+LG8ZB0x5/jJ75HTdJwQO2aHJTxxDjbZMuNG70JyiTkEMIDUjXuU5ubTCcY5uI3/0k2
70jPBSzFBdHeOYqsiPyvLQBLoOJTua6Tung+LNjH0FOyR0ALcUYkI+pGDlsDhfQ0bz33PP0qbJGO
nHoulNiFOYjQBfBPuBewUaza93bzp/WIrqReGD9Fo7J4wCD6lsLkwzVlIrXYQF+GUgDF7rDLSmBV
9QXiRoUnW3VLj+fpQ13cczAAPatUj9ZRbMUxx/9cB/5l3wjq0Scta05kb7DPhMEryZ2zXcDxGti2
k5LrLnFbVJy7MBc+v7VrpqAj6VFbHx0kGirgP+GKu+mUUppSGLIFmJvlAycZE/3MIHltLW1FHk7A
+WPLS+5jlx2nEef5EaUDNfGFYc+Ni+u8GkLvMlfKakUwrbDJ5Lsace36W7eHMpclsYg502tnZq8E
JbH5Hh4hfvQ29lFyXWmjcV9+OqxTHjYwjea0IW/oQw2qtLSoO3hzfSQTgSPFl51C6tWN8GEvjUkT
OMV10BhBm9YT8krDWSO6GtOmhzxyPybcMDVop7D62m2ODzAC+euYQEQp0VXh1CYU5qE6srwrZhEq
ZzC1IE5TVH4+Ew0mrvvUzNHrd+mB2vRqoAX00lQZ5+kqz9OIaPu3039ozTwKv0/uhJW7MvATJRtO
vhMmuNS9frzbyzNRsMGAVqH8qUagnjz1GCb5K8g19M7sUNErdl0zrq020forro8lq0pUg+WRTkWE
WTVRfmeDESXzZDmkgyv+FCizwEqkZYV/flszHOEfhTSk+5PCvl9RJtU6za09hYgYxrToTF2AT0ri
7vo/tBcbeTTw+7+GPXpUvGNqnPl6fn5z4kUHyGAsB3zsVIKRbp8zfmvbfTVGn01uk3kP7I6nIxHq
OR3sXj+uBbqOhVkI6mpN/O3yTCFdXhkK/d/yu3ltWc8qA2W10FXW3Cz2+KLQ0xINxzJ29QiJHrJB
Y2sWRTukQvfXkOM+hft6HnChzAgqVJFsFWfRFm2KWdxNOL8qpSSSjG2awrXnsHt57FoJ2Tjud3jW
c8XLH3bGh4MxbqlsLBUWMH28DXLweG/e/5y/f7n4V4cZy0S7JfEfali7uGGCjQCXijnHfp/9692M
IB28Op4R1QbVw1ZaLdgCSwAlaJbhn5ucWAH1eq3JIWXYusWCtus2yPbstEU85hK8CTaJoGdu3Dnd
VV4qQSbnal7xuNtdfg9dsHsdhlwFYNdPs9c8qof5dKIRP93Siht9o0hatiyTjpIZ5p05iSY3ObJV
oTNG7BGQEkkggSTb1zUjJNGBE+5GgjctO3gc+r25LaLj2jMmqhXQslMGaoLSV7o03paNRIUBcPFy
2dTmlS1we2AWVG+Hys7+omoZ1UnoC+rmUKjnlJC5UOij/NkQvA/WrH+NMI5fcSiw81W8OhcvzZC6
EE7dG3YHUAuhJLuzSPCYQ3NkCi+oagpZ/0alwXhLZPABslpPNKZA/YHgvoCVkOteD8/wy28pyehV
a/zqitng3UJPH6aTXHZt1R4j+Ya3GmnrnDCP7NJ3rxPbNHJH99ruN5vQZbEDpJosowqgqwhqWbKY
RWrhFyhPZgQKrF3PfsZhKSRl4PC9gZq6V3lrtBIKa7uukVHfrqugIEz8TXNusgYFBvpg3nKvN7Xn
BPgbAoZ8Z9HVjyNnT86uobV62kRubQy1PJVKxAk4kwZTetWtRQ4n15WxGR+9AU2YQOs58I+bsCiz
hjKhgVW9w9BAX8fj+vee1oj/8awdFO+6P3SuCvgSDBduLthtRElIOxLmPZbqQ9OtkmVbwlVxScnv
MgNHvEpo+RMVe2S7fA1eUlhpC1IukGM61CginB/uppkb8wG+fq8bNPGR3Xb4Jts2H+LT2vOlY6lp
/jzKxResy1MSIWugaT+42JeUCA4XhZfn5B+fybHREbXsnSsiPGXVkELaZA6kVSRo037N9UWFzL3G
FOu1GLAslOdBntFKY4XmyoM0NY0mVvUBOMnQeMxI465DF4AYYjTyEEmcCxJb6G9RAgglxvqXuRoN
D6zRyHnu6vbKE5lHywf0VmcvhImjnbIViSNFU4Ac8w4/20IX1SvEKeSwRj7vxW2Gm0wgWMloXFGy
cc80pXfRqKVoKMg0J/FL2zmw0XrDmMBfDlI+uqs3ZEee/+o91NZFUg/PLIPAk49I7d5ukVLZDNpw
byFRHS0i44AhG4q28ZLccMtYEQJl/NOZYBH4wc3X2UkGDoIG3HTIcvNddefvGYh3NkpLn4ds7XUZ
cHDk64zgOO4juvSmudQUWLoulOi4YHH/1G7GjRUkt+9xwW5JvvStq95NYk6M3LIJ3o8FeOyXlZax
425ktY3rsAGEOaVguBfV821kiDB/p/HUwGpPCNHFR9qYzr+qPu6T4+bTvpYRcP0nyGh4m+rdV3e7
J4KT7Ri5EZlQW/N6SaT9KkeZe3+VCYd0jfZLjJ+8/nRfwU429IGtsoa1uiDsaTBEwnFzO9Tqbedy
OQw8acWWM9mWiuBa1Bw8wVCzHAZDPiQ4751IGR+UMXzkZdXsQCMn9+C5ZKVkQ3Bln6CeqsOPT500
BjSu/o3s3GM05GY/tZvzJV66YhJk6Y5F97glS/7y7Vcu/5Qzc2ynA8dIrDIqC3/Ah+j9U/tC1ecA
CJ4ScCL5vvi09nwMEuRYO2RSjuMxnSRv69NWyfXQtRRnxda6I/9NZIPRZi4/UQ07nrUEYkhhLXT2
hmxTWnaU7qz5RO8lMKm3B/Ki/9mTRmnMfHggq1jPpBM1dVqqmKZ2LI40bSzME2WVOjt6+p2GlF9V
QyI+w+jjzDobNQy9jg88NY/g3tf6GQywFWzXtbhHoLMwj1jji+JQH2dteUDyHq3GkqlTxHXDPDtC
q9PEvcPf66ZlhNziGXvY1kG/4UC13gGlhzOQe3kJwJzzjeqKWiUeFleynZ4agpfkziF6ATmkzeE8
09pXlWKtLeBc/W8UZG5GM6C7P7uSlRVj4B2FKX0ONdtDTKSphyskTzhlwszpLz8zFUOvDnAYDntD
xWZOxWVMoyWAkvWcivrajz0xm+LvsCA88Xu8eGMGH8U9b274Zon2xGyKw1dCPd1kndcDsqMLhm+o
1LjU0mAbUUsrnWaIL5wv/f0bHPMKq2wKX7m80QQpL6Rx5qt1PhYgnDITj4fio3WWLZazLeYsej5K
XzVg9fYcZdle9I2/NmVp/iJKdtSK6tllT04Wft5wD4M1wXPLY+q5kLdiX2XswrAYfXbQZokHFNg9
0cZSaG8YZ/VZjsnMk18aYbZfHqyPVxPZDFIqfeF1ur1LBfGtkNgp2hfxNR7ULcLkUaqDhbVUXOtA
E8e5CtZkO+7OHFT6hytiVmU9xTHl3PmaSh2T89aO0uRcqgqptqiDXUuVFa8isEG+EUJpmbT9EUd7
R7BQl4bIYiP1c0qwQeTuEOt4C8zBuH0lgUhpxgFkp0vfsf4J8nxvUPEykYoLpu7PRK1AughO5Ucw
SxYVJ6n9jecXdeQK7a2ZcWQQLSzKWoEu7bOcSSiKNZ1M0W0VfBsSyX3YpIvCDo0I/hrd+UgHOVII
D9U+v3TfeYg+9NWbWSAud0SwsZktywc+8+G276j9bItkPE8QQdkWTbBpsgIU4vebHvzr8ZjgN/Y9
wJYu359ZJRNkq82G6qxLP7PkpWmi3kwZBhqF5sHW/F8iXUv+mSr59C0oxNKt0nMAlAIeX3Xc2kq7
RGByVD/eK67ywt+0ZbWQpxT01/rD1dg81+nX8S/4CAux0xumdhc2ktrwuUe2tXvA7W4Cm2mGchmS
WAqTSsDJ0k3L00I78mrClZEjWlRdDOrJdTQXIA/VU+v1LaIDxbPajAB0Y7psY+qsUqbKyg/0cc3K
OV3v6ZewuAvsH3Cn6EZOSHSl+YYlGSQ7zlxCR4UGfW95a9vWijXTZtctaoFIfQl4ar/efD5ZUDh9
UyxBHcbvLA16mEL2MBYOzAEq7agNq5Hx3S1Owa3XwEVN37A7RowdgN8m9t0qRo3fu8DnCCFauOhU
+bjIWIFM5z4Lby8e30XY03PjYKirOsuxifgWKy0oVFvuh3waUrhJ2NBzz3hJybboHmtRi5ohf7Jd
AvJLVtd65UwudB+b1w0FkDTiBw8z3jXhDfJeGKSZ8bgHEAmlPWMkmHp1uMV4syNqdnQENANGVU5T
YaUuCiTOPnWuQfTEVYZDg4UmaVPMUmG8HJKkQM5kiRX/n23vDCr31GwvWzB3efs4mxEIIPyMBvuf
5+FrtKG7vYn4wAzxXOE1ZESDxh8TSzIgGqC9o1btAsmxuGSOR7FXEYa/L2AiupT/cIqvLja+8Hz8
iIAuw1urKTBOq3ya+dmyC+fwyKR7jyZ0paraYiEx8Rwpk3ivraCZl5XCbFguaTnijCyKqWYbteUX
Dmi3cbAnDN8hESBgYGo/xB17KcD9I7m9rAHI3LQvWP19ojPYnfkkW45BCATaWD1QV6kCko++s3HH
m7IXeFLyp2tQcYkiirZsmwrxw5pX0eenigPUUyZCyu6fjx/2vfS71uIyO/5/3hII3MU+YgAsurL7
RVjdgyOYkVkYhpcXXhM1F7UEjNc1J7F6zr+8Bw+yNmCXfO30hryCCvMJTjnRx3b1ymkqfuIpwZ2G
FBaFzmyrDdnrDj4CPadC9bngdtxi0TOXuXG3/SeMnGTZEdAX9FIeBn7Zlo673xKLt5LnPVvgktPJ
ycZUhN9Gt5EGWpA8ZBVWvPBMHH6BO1hv29klParPnXMmZ3XznO5XMrTZ9nBdrNt9Yb1F/T8Ne4fC
P2sfi2d90axhh7uwi2bvz455iJkRyncxJ+CmRF75OjsPtyPjsuwAMI8RjTLnJNXFDpkG9vjtC1Vn
C0ES+fYLEF6wlS8mZanEYOtDesYXnkTn1Di5tpPYuLW3zDB5Rsmuu2eCchl45ziRBqMBSjKxEDu1
/4zydkFOBt3wV3C4eKrVEtnB2tJlXIDxWF8Bt5CWYdk4azuONvNnQGOF1i7grb5PeZqOI606AIBl
+PjaKM5Ve6j+K69hS+tVfKukanwQFa5m8xzBeZenGzr66S0NgHAezl3DQRBwoYDz45U50vwDipnu
lBan5bRSNCcjkMu56gi2BdFslJBrm0hzQXCd5Ohzj8HELc6QJ1S9WQoZHFziQtd02PDp27wFF58G
aKzO+zLxW/ecbGE8/iQ/5qYSJsJ7Mdw8Qm0OE9hTgRAnoIDXRDstgKdm0OLowDzeiwkpn8v3aHqI
5wgXxrj8Dgk0xnQs30zhQEGc6sklSf361FuXgGGGVMSnhFqDLLmoWNlNYE0LnxrSmLHWBM+mFizk
8Z7kBw6ASGhFNlfJH0iTbFaTz2mvOBqL85Cc1LQV8On1B7FABFxzDsoJjEVjtQhBsHeWA7hYEVBl
BZGZkbQicLVWH3spcOs+jK1hgHAnn8l82mcFGXx6bJKw9RZoLKWUEcE+X7LD2uoFGEqB74tSNkxF
1Ojzo932+XdcUery1Rt6HKkJ7adb6teHdmvHrSB+aQP06cmZIF8HKeQgX6o+0vZ62JOXQE8iVYwe
LylALsGz3eYHw9RfoJn4McWp8UUdDhSs6+d7mMxb5L+qJqrfKJL8HtojyeFarn9OpGgAlr59zQNR
QvP1mADrX/hmhp4h0DUNiKlyPeK8xci3BAIqTLQiE0w5ZUQlbpL4//klHAM5wAXhv9lY6zKQObZH
8GO87sJ4d+8k94YchxZs6PBmVWKMgrECahGMvXZnrrqFJnkjc2EMYiyLiO0sJB53ecmoWLZpbpfI
1IVAmHm5+q5HHtUrSLLIJuasnvmBLnS18nqSBaXGYtrfNclTTkKvVGXtkfWi7tmXrW9W+OZsDcYP
unDD5UaV2HzTPijZ9+fAJbyWYmQaxZNsj4IPzK/Jd4Q5pWKzEoJUXIJB8QXnBopgA/ewjvoI/qz6
AKmcQOhv0L4PCOcRSyHTR+eR3UNJp46tXQ4DCoyWkqpG2FbNmakbiNMUPikPorjHh4V36T5oMBck
p0xADycfbHi8oo2pqhWFTCgL4TR1i8OP/Pe3/q+TflBqhuaUo3AMaNBPptyhb0WO4Ce+uXaSE+89
11++zL9wZndpochn4NmKDw4q/dM8G8Hhgegs0iaHgIdDfqnnZkdyrVw7CoUlZEOJ+OS59QdfMm13
8RMhdfRExvbxySPdfYW07n1K37liffV5jZnsQBUJHoMZ60hsHfcdbM7ZzkJh5oHF6HuDNmbHM7QG
QxsaKyJcRGh9HL0fmg1gjrycfPg66zC8FgDq3+LNS9GjkKm7oo0gdA6x3zjydPTMFGnNqBvCm2JP
12uwfwJpACB897FArME+VgMgGluUCA6rSKocw94APig40tgLGJPvYS7Bjt9RGQnpLg3HUSZUqA3a
e3QEpyNI0Fk+bmZZyHK8xU95WQzA6NRYx5tHERFOgtk8eRS7lO3DiFycc0GCbEGrma3t3grJcReu
67D/lLsVIHrLrrnsp14UoC4KdPrma9JlXb6my0DKLwJj4qD80xXSTx7A7MNg6XuO4M6mWYktUKHB
ntwQCUCFIzEYboVdtab7PB7i9jvdX+yhjjso9huuIY3QmjwY72A4TP11L04VQOHclZjWnCuqha0H
tfvI4LYq8htG/2y+b5qnA7nmgY44/Gvgu5fHvtBh2JqzitdTL9bLF5Ujsa4tlZlOoBXGbkXz613R
jXu+o6XSVinuIbMt9v0klVeX+/mEwPP8bwg3kYjzraOY5mIRkWFi7hi5fFeqZAaM1kN6eBjLDqt0
PhEWkpNIYFdlUINAVbBRaQEHL7lShAoP8541dcpPHX//N0lFtDOGdgEQGO9ySX4cIFZ7DggLUaoL
+TjW4ht4CGNDGXGb8Xfv+Cn3Xv3Fg6jfS0OHsuoVrZ9XNBDSC7GIQix5c/FERQIaaJG7jxXmTKax
UepgzAlTNdMEw1Idh73U0hnrwp0soYVlKWhS5TZAWrZbTpY1j/UVVteaz2J6ACuAV9k48aZTY8Xn
eJ745XEmzgVOZk/OQe5FcTF5idENPZa2uyLUMJZbj1GsobFMBR1xSYfWzeZME2PRGjtgFSfBV/cV
xalb1g1y6rbMk7wN605OJ5S0cWWeBx3bGE7XhPOcbaZKhTiTEsYEjUMW8ye1kdlRusL/xf2FAjr2
5NX602CEzNFZNtKveOhmbqM9vZY/zl0sxe5YkNr+ec3MBpzNu6xEvn6u1De1My7Fk7QMktYd3oVr
Ndb0S62p425WYsoNiHlWQu5o6LwDis8eLZ+uSTuSN35KA1xBxVWfRUa9QU+TASBy5nguPRB1EfZV
CFg+jWWtgl+rJzzscem2/zrtoEo4oLAqqW/bdQgCft3PrxyWM4E21mDmo1H0GHfXM/wQRUAGZbRG
tTym28xa2meB8r/7UtUY+8z2PsaHCSdhu/4T8kDyNp6ofPXL6SMSvyB1UWx4wDOfWPlYN8AYN3Uc
WYDJLRiAk/WmpPX20Jhy/4UMY9axXn/0JXE+Qz0lpgvkmTcBEbDO979r1UQdW+fpAj68IBw/0W4L
GGRiQARgutgJj6pqpFKT3mTBhKfpCCkOiCa1p4XrBSeVeGiCn25X2YtpDPZaiJbHeD6p1hYWybUp
a0xyigBy/A3QCY88fC0SM5LNzoOzMklf/bXIAlGmm7cDZc9vqLoTfnLBaBzqj8havB2wmh1Fm8jL
RKXS8ZyzlXBlhJ93KgJuCZObHD5g73kyWZT3hmgvYLwJAhded2Rw/SO5LypDnF+uo9rgXtlYipnq
q7kf0eKv/s5SEVfF5qQYCLQhrjsA+ovZZJ/XVSeWh93PJno2xmX7eLHjG4l8IqlIm/IargnI3Jxg
mXkajGoxw2tXPXi0INDa+fyV9IdCOVAvMVDEiQUQCFXsg4PEAC2/5K9D5aEAdViFV/i6M+Jpe3LJ
tgCdFRKTTSyG/LSJXDTLpTyxnI2hrQlXWhJnVGugfm6IKm3n962TKX5OkLHCU+DdBXR6UlsXFqcR
k0PW6xfptVRPAYIfnsfeQCpzSCfXsjs7+VClqZMMbBEgxhSmABcVCPGWPxnj1kCsQHegLA6z21wi
qmP1irMF+DI4vZIEalXu6Rda8fCGjnZn2+urmG58K5mjEXVPugIZnHO4AIWKSFEtrcHuR1Orq7pr
DJ0Z5vXa7Ktv/G8W6TJYASFY29ZyE161evzgjQiR8397X6hXprR3c0ODzG9O6OjFDajzRDRD49oN
6dMqKdglkWAMjA/gEP/qz8c/fHjyd6XuNxMRHqkMnsenccxg/bvUv0asxFuTn5UW89i6fWPdM1uK
9GsBY9GxBbm/FjYWkCJB9DTk8waeiTlJ9LSvSfm+6f2ZvvTuNBCR3T63JUG+wSy6VUeezXxJt7TV
z0rkd4JddPBAHqITAikWkOr3PuKc3/afeYPfUrJNxLTMPUk6OJi35OGKOekkEoYwcidJIvKsPIU9
D7ke6Egy1qZ6UpYsSRUBkqPLVNXcVDnEE8XxHN25jTo/8OVkRyhKICkbVhOdLxPT+MxH9BjruGH/
t+q54eDZ+ZB+yXOB+J7gCVKgzUYQLt7wI7K8f7K0p6PKKfsIH0k4SVQeQEP7/uAFdnWZli6QjslF
6bfGwsGJfGAtAjQNwly+PuO6PwdYoRxGuQ/k4bIyw5S9Pjnra77JLB4lcZSbCw+Ga8fkugfB0n0k
OlLuSMPwhFzh5eTjZ8hX4hznomCEC6S8rNxiemAxApNCt+As0Sp91ncDYwIreIJXNvnMJdYFV8jA
XHAa3sTXTFSd0XH88gdcfnr6KdfnQmy08IJ6EmTTmpGExaxVMVMCtdae5Fmu/zNqdnxEoLtA5I/3
+oaiBuX9ML5+cPDZy/AR5zw8n+uXB8kTB/3erANGJmb84b7d8zYZCtkKCjRmlxgJlL64O5aA05c9
+b3G8309mpUoRsS4oubu6rWhqqCYkFaizQpvS6lqRkRpVCvRUgpj6vL6C7H1CY9lGMBAR4rplA/S
SD+Mx5A265RNuoed5b3x0nxg0gLJqtmMlGKaKfL2vw/Pgg5mqmAmeSAm68P7k82UQjgMwYvZyVMz
mIX0D+5zwP1BPVIpLzUga/GNjLT3IVfpuIlCFLqNXd34DPK9jtKXMtX/fol6ymX3jk8/s5G9t8Jl
MLOzo5jhfT/DrlWP4RWhCnwl0FwPiLSTwgLlblOGLTuZwvCfZclcIi8V776GAArM3B+rEtiyDYp3
gVBTwnBH1tpVMH43dhovSvsk1EwuvpEk43LfsI9HauAG7W7p8D5yaNC9aaqmzft/YTYiNeDPyZZM
Ok+xV8MEBLaA53aH34GaA7I4kLwfAwxXTym3BnHadnSWZ353eunvTNmf0rVaJJ722VNKCNG6DgNL
ieos9S1bBWoTpOERxhdLyzGnutfVkHWW6k3aJK7qVN6BeVWt23BltiEPfknuF/7cARDzktdAOpLF
WuH3FCDKr8dlNgat+p2QDd9e+vD8InEBQB1ywwowXh5QGeGgYJFJJ5TRwalJ3IcfXED9cQ35Iih3
67Z5hRynbmBCt60IOIj/Pnc/c0Cq2/WXXK3BGV6Qlk4f6O+NtI0zvLdonKx1uAiBRezU9Qs83Jzy
d5ELkDiTUTArhAOYkrGy0aMNhfJcvqnV83RD1BOhQEf8X1zJohjA56gFkMpcBYNZOB3K74/GtVTa
+CDDTWC/fBp6qgkeb3ku1S0FYfr85QkGzIVY20EljZQ7kS54J0xSo+HMn/BEuAq91h0eSTJxCIeL
EKd0fPmpAXKGsmWsarEY6CWAty83PSPL2Z4uZtmK38ZO75NEAAgJKjZsMxCOrWmL1Xk8fMvgln4U
QY6bz5dH1b5zEsvFYx2GmfvWcItBvOYxDOSXDvQY0HBJ+qofWJhfv+5B+8bKlyIx1/+ypfg0AfDK
3m8SMfiOifYuyki4SbbKgC9M5p0RSmJEexL1TbZKcaqb5wKIYbpMA8DetATs9uBuTf1F/V2QtaxJ
0LzpsIX9oF7n+t8Ta5uiG81/VohkOJtKYBfei5HdZeG225zGbqtMuNCkufjDRaet1p3k3TZXysR+
HuCVrRcepPEklGKbgE8b4MNOHgLSFXftoOW11trig8NlY67FKUcaUsq21dXzyZrlLmH9PCzwTIUI
eq26yuebw9o6mv+vLIgjRqLM29B+VCOtXtr1bYEp6PjJETZeAzWk6mClrZHpSu8h5ZHkwd0PXPEF
DoREAT4nGwAvs3RNCpPOlHT0HHHylmezykBeisyXCORl4s6zKkAl4gvb32YKbtTZb70oKPytbIiv
1l4OcxjlnRauY6pbc63vSRDszGAdwOxZ3TETsK6/3sH5G160luzkJNHlB22FuQevKD4T21eC1fg+
MJ0MvHxH+dp9hHkV1cC3i+7yeOkPFKMev28tS/YMknWtEGeHwmFWS4h/xGN4X9iWp/nlxhdqsaGq
sr7rxLP9GhAoq/3N1Z9hXTUnlfVPVkSIBMqn88Yq7LlAjtuXQ5kfchsPtkbVG48VV+DYX9Hq6C48
yze7HOGLx415cIeYS/uaEEYJD7scIyDsnqaZ4H9/5/9k+ERwGOSOfEjMl0eKBMxQekCRCuZn4x9h
aU0Zr6pcG6Ns0BpI9cvOIpFR+DZ/wsIX1AfDyX6KP2mW5s+GzeG4mraPTGdVza4Xt2ICMzK5gxo+
gcROPWiN2TWmcVK4jaEvaJw1tbO5Bs2/zFVLl976TOlM7YaPK7l8Z+/F58iiB+GbcM9WxrLbuA4L
YHwZKUnOGHN3uWYSJCy6AjsVt6hEfzZdnOG58OSaUJWKg1zn9IkR2nFPGfMTfX3CM+bISWgU5hTW
fhW12wOvP+7gnbdk0YJlc03O63ZW2FEGPLpFHTYgbGxnMb4Qm5IG91zzspfNvvr1F8CZqw2SIuoQ
Hx9GyUVnZmJeD23KSHpw16kIEC8EcUdZj6fC8Sdq8DFI7EHBjdHxahoyMpLapvznw+IUUQChCtu7
6elK/978YLi96zyXYvEx4p01S7jJG8Q/m4ZLjbBdbk2DThRZEbPq3IO7GMvcjsgUgTK9sKJ56Bxo
2T5zkxXhPnUPvKGGrSReekLF38CO7mhyZk54m9LaXKQAP5Uoza7LoQiueupnTrGe+f39in+7mS2m
1YUFtgyB2VyMoFP28FWomczBYdsaim8obdWlgd4oLvtess4YqSuk8qnx20Msf3xTPnCszVUAwHHD
tCTeAfnpIQbOQM2oKVk24ZuB2va7qJ/VDe+Q5i0wUzu2xITXnweBpGcc1lnjmzo+M8JXE4SyXFla
BpsbVUwZCKInjNcNs9mZRcvlCDYfuD551NpUr1kvN3yfPmTo2ieiAnyYR4WK/ZkfVgaS3hnROiNi
w5Gvho8fhUH29HoiHBVh8c2mTBzaRQyuxay9iwiKGhTTw3hCWZn4wcpB7KKapgyUp5xkJvTVY8GD
3keTiTMTVKL77zkKCPxXoZUpHe8hEP2oB8/cmfltuike9LooAscbA+sp0gF+TIr0vIE4Qprk6ofC
cx3vUn7olBqo0CYxL9m2V0W54scXGa5mG1LX8VIwKKthPAv3qFk8xEU+VNWDXY+KP1xvgx26jYWJ
erOCluqVIjzgtZ3jNuH8ONT0sSMKYISUjuvVgU11Avl+1ERQOl4r6e7UbB9/q4Lr+4hVdaGmJFZN
8/eq4dC+vtXXYdo7SHDQLi2wk5K0KgJS+XF+Wi2j1wA0bD/sEiMTITuseLE5sy/ZUdR1JfuYVufm
ZVHXEbRzhg61tNnS/Pm8ua/1ObqkwBPd39RZByZ6DY81Xi/OFQRk6SUr416MYHAAi3M8UXUvlNPe
OJKFs7B5WPNguLaBw4WEipziCYZj0NWmaVVpc17yOAtl/LdfkteApRA3LJqEBnTEjEOxnumVq7+O
cTw9cu6ot/R6PGAfnicLFri+gvmIs0b+psRiY+K66dJ5RaNGcw4gNUiMUsl55NSoamI8N6f7ofoK
XL641Wsek5A5+ch1sZ8dqRaansG+eiQUf73AdyWvYv2uNOenxoFZs3CtXuFVq3u9hDXOqZnmAEn3
eWSkODhc7Loi8VFyN6JmxvUSqWDQUGc66ZXytMNqzLr087eSv2kxOtDIH0kBlqw40uTbCJ/2mJiW
/DgxzqLp6+ANHSdGpBA7ptynCINkEX4VuBULwr9Oj0zk9umGH0ZFycTYhIvxC45D83myqIAQUtbU
qTxFrSdHfSobJBzMuDwMbgKKfam0/JFfwqBETS9lhhO1RX1WsajF0tl5sUSyqujeV1oixAz0rjeh
yUCT6MCmvXCSTbw57l2LShPSH9yZkErzbDeL0i1JD8UBkJaYSDg8BClB+PAOwGA5RycUWrap32rU
aEWNuLioFWoE6C0IIIT0/U3HcG2Y4jAxNHJGv1CWLB/SHxyozZQYNShvYRQFHFDn566g9mBLzHPS
ZQpPGuP2y9XriHd52TmGI/G5Zus34Kz0vaZhFBP29P/0A0XiHDjZqgOWDxK6jfWgpMf5vE/1zpnP
Gdg8mkczNmjljmWY4eppqrKQ9Us1PjnLluXpYWMFrLiOHEAcKpfbUWMqWFoIuD2q13oBKhxl/R4u
kqHyyZ/Br9kGu/6yG70Ljg2kIsA7odYywA/C4gdD/wHiX79ymXAuZvKPiA4sGYhbEuC3uttMMYnD
CFB/e1Lr/FCw/2fnrUl8urA6MFO0OvtfvRG9eQNruoMi0KdNT+diWApwRums8GkI0g9Ny6Fozjr4
dHqXAp2b9uhedB2LI2wuo21vO1L+BOYcncrlG+YiKOZaPioLhavshXBAvvWP3SQ2kK5+r418kUrU
qpkqYd9lKnwhwcRDnML6IHywsgXX/g3dndQltf0sAKmdCmV6oFb1LZV/K64O2mqxyP/ZLPKY8NaQ
DZhlymX5AsldzAOrZKOXmvfsBmq6hUm7rqSnXgmh73hMoO7pVzQCHMx2dWN9zjcY5C5BaF7dz6QX
H/kFVvxUnlDbiv/LtZXbbPh0OGraGwK8el7RkUMTE/lZ5vwOcB5bJcXYKFBIfO9G4XaqBnUSf+iO
7kknPW/stdbJkE/Ez+pLu6FtR3SmHva2cLH98guaoV9By5eHcDiksJDFBEF5ZeFp1JRv4H+NHrOU
hAa95zEQ0J9EK/qjGZTmhLN0EKKOKMeQJpphIFvR1f3jTQlhQJdqsm1VFrAqM0gfVx9ew0GuEck8
imCOa+E+UTrbAB0nghaIBqtTsTX04CxcPZ6a5tnjUmsGbvZ9sWm73atlGV1PvLidoeXDsqHN62Tw
Z/SWrdQsO2SQYUiphtZg4Qctw2jliT/4ETbLM8ie+9a2CDNhkI/iGn+fdzaXuWwJoHKJD+TtZ1fW
Pukkch5V5LTWAB/bsAGirVRltqUA/0zjzqRlOD1Q0yQcnOEJsHFLgT68ApY2JW+kWEhF5kr03x9L
qAUpAxTTIDhwlGXLJ/rTEOTYO/mOKYbsQLnAg42OPMo4rgtTDXWQVaIhIRNvxzxOdGRSIhtw9sWg
CLvkbF8Gd9Roqi85oWqI0DeJU+fLb+LXwr5jULuZAtSTiNnDnCoutJ/LGT5Ue6nqjAa+GzWtrO5R
JzuXCuGs1Z/dQ8Mx6ecfhdMwhCrn5q++MVREDWFMllfFGO5DRq7ccIrbaPX3tA8eafYS9I3ps8bV
0bFKAkRET//Ug+B2FUU/cXzFJkqV19Aidmlt303UapofghQqyYsBVtfuyHLLEJvLc/BG30KBrQTN
8DED+obrMGu4NZI06HXtzj9fN7gqrWLYdXlfT5LeLc6/KpMIdYHC8Pv0VDm3uJlJTjWevnakV+hz
EPsarIUwSF6Vb5UQ8g84dvZ/o+ragMsgWwNtJ2pPQbETwJbczhCcvhGKSXJG0Da1P5A6JRuMaHMS
ui3eTVTs80sfSdIlxpZKfpM9cO4XhWWjiF4jIot/CFYivl9SmROsIb5s20TWYDVRk8z8XIF6A52x
vAaafotbUDFeKUQUTzPhPIBvyUWRBqO/kcbAICEaRhhzQN9PhcTZRZMHHikc527KfBxRHxHme8S9
5XDQw3u//uWyZopXYq+ICDu4Mxdeh3ZUwQUb18L8Rho0wQUuqLb2v9lUKUelv06TlT4npKxuhLYn
RcT4q/tMZUkDaDFN1+/dd4HegoS6u8DIu86AiIz4MiIebhK+4xXLMBMnkYeo9V3NWLb1rsoOuj80
vPzAR4YeJC8h69NcmRPV4qotZMfp5wQUU1lqot2WaUxEhgSfm2B7jGsRsz0jhEFyjxiDSqVl3bOC
DSetuJjoju04u8nOsvEeDKvPOgwPiPqNbhDXyjy/cajQF8ciaG5C4WnZBCvaOcjeKZ0935TeXM8S
ZElLOrogWh1y0Iw6TbOjlCx468G+Ha6KbmSQ2mAuhU6cm+O+Y/4kA0HWwDtZVe1OQw3bfuRX1eGo
2NYa2599GklFQd7A9ofj8HELMvMcisr7IdhD8PqpFXExK00CZXn/Crc/Zl+4ZJtmFb1Yo+yzVmlT
+9XuICjFEJ73dpNeSKngBDMyD88oWRVD/xuJ573mBtBjQ8KlBcwSUaAsK9yD80TSexNdcGKwlu38
YpxXWd6Xe2jHjnrnIS7TRd4cGtZEE3s5VmhpdTyIBWurclrM08j0+Dk3T8aHb8Ddfmlb09/bRXZE
WIi4Q/j5EyB+UNZPpP7/AUgKgpeaPgYBFvyDVJBdw539Abfd1CWvNijrd7bEvBspmAbMHshAe0Ri
DewWu7KLksskGU+DNdeTUqA/v8fF41gfck4gsKTlD/EwliQUU35pzwud+960B4+pMuQxmJ0TVeZf
qokYEOL/BB3ksYptO2Kbu8XrZ30o22yvF5zln0GxRFdiMoiDBFodTtUDezEAz8kKFwkwtGLFJ307
lhT+1F+3ciNCGWMqv4PT1QYck36JqdbS0mft3jFsHsc06z7SRGQ3bhqcaXMErjM9XDlpBwUSV1fG
5cUjszUpyXRnavjO6daTNaYRXnHbHN+Xg5cMgCQJwjfS68l//0lmpvaN07xEUF4vJBlxKF7cHrCB
dVEXXTJGXo8KMvrx4nXffP3AdSelHAtnCahk1++Jbzh9eDHKhlRDFWsl6y5iGxsUTGPUl1hlDjdm
Rx6IJpYIzr7HkaZaUUZLFLAM4QIpy2BHM4qup72C9qKe5U++DRJXxBDwcDfZbO96wpcWVVQ4NpyJ
mtL6+6OTMNNF4tbdWsFxUK75i3MyPl4/1pZ9il8ZW+HgiVMaHm/g42tuZGwWmVDWREGMLTHRPZeL
fdP0aV3eY/ck4c7dEL8H66aSPwyNoWOvN6NT+6w7aT97X7u/aXyc0Pn7WwPo3vrKpXey1M0+FRws
Nh5et46T7qRP7Fkp1xEiidJhA3wwxst9OA1Q/kNEQvqMIVSQ+wRXa15GC/gQ0PlvLIkG1BN5Ea6s
P+0r3h9Fp7svnCeiHMws8Y92UDNqmg250M2yjPzyCVBZSC2kJV0jabnT3TiwXMdrsxWQgdvE6SJ8
MvbDJzNnPcuZizXJa0iXULKjSUtuXm5u66HP76fS0zbnQxqXoaFKvlyQAD+IE5DfoA3WgkFMl48J
3gw7Zt5rjMi5roqw+kZ7CiFwxsrXUAKiznB8uT6djSvQe+xfOatUCpE2coS8+BWzIOmd0KqkJbj1
98SMPod8Tj/GGuB3UYtid70ydKTFxAo1+GRKFwR2g8t+RUXrQWp8cW289acE4tZrSF3PVWHDvfNp
7mvBpRcMLWcODCWt7DvfemIxOtIvRKMW8FGu2jrj4nWZ1yjdhhDIsnGo+QHGVaW8BOfg0gTYyH+k
ldDtMhN1BxfStKn3OGX05CyU+nUcx3hlDavGx0vDWGgDfLdwoG+1DLJ0WFFl7N7Zz2g25U3XHPZk
xW5mh6OKIwSwoPrPAZgcul+MtG6D34HDkWhkTLxv/5lelFKeEB1xyBakL66zNj1iG5h0ZMjeylGG
viCv2N3meLARPXj9hPVAzJ4zdGG1WhDtyl3WmuxkjWg7NUCX3BjtPM1CitqiwvpASziZOxz70ykH
DrZU3STH+fiTpr9lNjWVMqS9Mk/+YqsnOolSu7uxI59p0kDDHhUhmrltkWb7b1GUDIsZgqPchYXu
C0Y+hGg1OTdtvuHXI51Mf1GyTJkkF4wW7RmvxTT1ZPd9Q6L+CTu+W/BhKnyIwtUSlDtdDeMqCkZ6
k4cluHSfoiGI9PGvW8KiAMXRazznnNbmXH1HYn6xE6Olur1uCGnizuRgMXW0ddsLWNrJ2sbqPY31
QDBi17IuEE6Ja5cgP1XLsX8/y+5DspG6eavJlo5Ja32U3Fq6O2uPMn9exWPjSX6sgYUKLVPhTTFl
h9CCWk5i8P1lht3WNSwF2I02eOxgEBPzuaiFyF0JftR5d5qFaOrzzWquSg4UZBPtS9yvB8Sdvf5/
TZi1QbNjlBjM85s0NxfCNXXJbx22yluGLBWV/lfJTdIrFcjQ3BQGxAV/ZRTanwslZ0MkO8HdDGlH
9DGD3XIkBf1/rW+hcWdorOJKuIrifaacfyYsvccCltq3cLwdQjMb8VY7xaz+69zNYjIr+t58j1aE
MnFAYMhUBm/jiOHMIqheKDJH91FHdTSOYG+QhcQx6gEw18NB/c4iJVjab2jPRB1f/uPdqHzkuxax
a3WA1Uj85lzUsg7F/VVAX7AVtm48Z8hUPDJHt/3fjDPTWR/F92T3Z/rNEFHrkerseiiTAevrzPB7
7i9BSr2/lBAX5xVEYgTaX6ZCYCE7eRrX53H9r2TsqMAv6W9X00egZBb4iPEZ8qzarJNCXm8469ZD
+UvkRbbIxPqRZbpph0xjs/ZKSgvHj/Em/y7/3tN+bpG1Ae9zFBcO/sW6SG/KlJYxJu5a2rPRH9ye
hI/+X1g+sbTkHnMP1k6xwleHvl0ugbmJqF678/GCRkqApLxTJFm1OT3ILy1/MwET2ENZhbOciNV9
L7Fm/fAHgWETXXX2sGe3J4OcE+8DuGaO5QH42F1WQJ6StDHbbW8VAX+b7wRizXCmeDf203ua1g2r
mNn/B8QqPUv1j6b3SXigbaWD0ZGMSNQTaT3WfcooriCNKpRyyoIyJ0eDWLndfw/cHOxvKOrIlb47
WAkRbtfNX+ny4/1rgY6XbuXO3Nukkyab0KbE7tQ/89YM3AxlYvoL7Tt7UBzmHJXLYWk5B36SKdss
Za3jonssYRweArmDOb0/f5Gd2hxAOwoOyt6Z8/rBftqkUyjdIaL0HV3+wWwSsckVnzI6m1yQB5Pn
WLtE51FZaxoXmpTp6AHZWT4IAwk9aOHzkpbN3zggVh7+dwAcWeo3mxRhF1ajXcJaqJlQBBNvWoA2
dTlnZ3EyxzggBhVpW67XaetWm6No4di++um2KponqRtBB6szw473oyHEJyiwtbXPM+WBZTZuFVNY
YFMT+CtKI11GyEfZ0/ORprMN5hrwrN5YgsgHUsT3kGFHQmvp3WOE0cM6G674ibV+0+4osgq1bY86
GDQuvAwv6Y/yAmC8ZF1WbBzPFsjgf98df6JUChQ0xD3NvaWAdbTSqKor+3XzH5NKeco+GWlK26bD
5BoHjvpA5NBgXQQsAPDt8uCMez8pW9SCh/F6RVk2IAYDtoBrB/3GuJPHUH+mWOe0N6dMq7VzZmgs
KvOaXbotxmJt/JGXM0YPBAieJF2T8no/jN3uLFXfat1oz3EsXPXyvwTVyxIhU6cERBjWjSRQiijz
iUvAr371KM5MdzFWLTL7JJR0JqdIli9DM/mzVUJKJm/9L9HIj5E6k6seBohdX3QjcXWZPhC2DTdQ
6b3k7d9TBS0DlpXkRJwhBaaqb7G4Oue7rd6wY0Qp1KLlWLkpwlTzEzKYD3WpUGraEZUkjiEkLspe
Zdsy8GWRu5z90uxJp8ogdfZy3cHR0muI5y3whFAugOnjcARfM4aEGaw6wM1kxmim4YNmIrEiTnkb
DI7PDk8FAeraJtTn3AWY3+wkrOkedzC5GXkmr5iw+730f3mou+rC3iQZ5OQJSqjNr56Wk3mgme3O
XHPdAFm5f/hKDPwW4I43qKDcoqQa7uBH5WAbO9dPorxC0SIDrPS7jRUqQlHibZkxoH/9KI05OjMF
pFyDW83+BGbr6NIdwnhVqiJlAl0zg03gQrhuZjFzWgexFXI06sHubvZZqxV4Gz3Ei4U1flfsUcn7
OP3yiMOypxdX5cw4uhjcnCd6v7gFCTlHv5NC4SqLwg2ajpeeA/DJOq3tEqrFd5MZBZZq+mUUt79Z
aRR0rmP9FElaLyVhNvIk08satLArBu49Quq/npB3x/9kHuVneodV8sdVOBubGa9SX8nL/8tyHvAv
AkB59DhqwfGkle2TecYeNGA5GfVHNFJPmlAUH4FiLcX2Vr455NL94cRV3Vo4fjbFENqb+fJwF48m
wS34m7p5v7GA4v588+q81qljED93OlYtG+bah4HaVFilID/QIaSNNLxshuqYv741XAYQiozjBA6y
SQ4Sl/fwWeruFyyK5VceSluA0nyQQ3bU0YUAX9IXZE55P/ZAsbrIz1oxhjuPT5ggMpD4bhcQCqG5
2vsZZ1N5OLn056jyHpbd4xZ1pxIwZHiMTRxh5UaFkzXhxgLUf1T8vd8Xw9LFOjH+rCsmmDR9Gw4U
Z0ntVErnRPN4a7anjkoB8qNYssQ47mkg/G06UzWKyI/3obx7B2WCpAZwjmIzfgk6RKF7sP8SLz9y
A3O8IPsFB0PFww89WJAjKJEOy2suQEns+WCNBtezcIeIECQ+cHrIc1U3D246wNw2GusxQsC8hdIc
HjWAPuk4zSe/XyBZSYcEv43gIXqlpi9Bbw9RMKXj9VH/l8CpGCWHLDxaSZwxFqZtKxjQeU5CDXlF
ZzhHk6kD8W8jhwrbeIJ7ngvaWBMvF3CbKeRvvQKHPHPGYd/Vdr+faR7pLKs49mBF9gnwLxNiNRlP
BOFTd7GbO57tRPviYGNoWjxt6VTyJ0i4QNFY3BU+IsLMdxfY75vnUSsV7mKQeoXQNeDVW3KE9hfc
6jmuSfodAucKdYGD/OXggNN9COaWz/sA11AeDJHtpXv5DpnNtTEhwtZkJe1IC77t/H0CuBUGWbf5
PxlOX1N8ls/mYDkIxeSV1XSD0CLraCme86/ifyBntyHafx/5VSije+lv2zNhuHIf6QrFz0RIU07D
5aGBH9jSmnBaMjPh63VfTjt6aMGzR4jKYHSylfTf3eVaiqK3sNK5qjSQIFK4utFic/38mnQdfV4h
72LeLwUaNLSQTOQXF2wSl6o1mkTQiYpJImLQOBRVW04HMurLjcfN235tJl1Zs+ftnzH1gpuVHNlK
RiiFqe6u4c1+nogph/Mu/oi07puCWy1MMECNthWi8ru9ey72m72juRBQJj5Y9Cu5c7++WdvrQSSV
fr/Vdf6EwIakPgZgk5ClSmg9LFS/sQs8l6xf9i1GqgCfuFg5sYxKSBGQs1oHHQ5jIOlPpX84/nNY
sOO//D50bwSSaTNnI8+eSjezG3s3EhO5kSaOW6+OX7k22ll0WG8iHr3dx3F5NC71lB2LLCJPrUdV
yBKDZ4kQVZh/O931vTsIOzQQoOfoUiSplRfDQclI/WX7D2oYKscO0dLJl/laCzXk1R6fpHOQNncc
5JnUczIlQ4p7ut+cLu4Qv1Dg/ugGZ0AOSTfLBTrk5yh7jOEs+ZC56599u0kvN7cepBl8Nf57xqJV
6F394ZWrfJIWwjqr3V192lHUPyHPswkfWDlPjCfviWAvK2+8b6Cu5mQ6HP6soyKx7CVf4ksTIsKG
ZQaLWmi/F8HA84uG9Zd0rAvObkc7Lq8iLyD9qKgIJQc+aBhnFdPBipI9B0tzKILvtjPrVnRiMJny
F3DzUWkxTeRUvYE1hSAOnLrAY5f8uIa2UvDalleDxa4tJkOzX+qTlHsmK5Qs8Z7uRYrVXYeY7e5e
5eJvpjvxGuOe1TEHegzG4bwxXBgEjW5DIFs+SRlYxCTUJ8OzVJxp7V6go2I7ngXMMYY6jLmV4r8n
i/Wfu+xbKj1IRHqdVlqbWLOk4vW1U/ZkIFPQ8r9AGkHJcXPghM2Md+nFc2ApAFpsWODLCJH7qYvE
adqIoWkYKOg4dmAE405eU2zwQ5zGlOYocRviF0bPYyZZJX7GGTuEH3btgzorKXcGG/YQRzaeXIph
GDGCiZFQg2W8w/vMAg40TkAVzE6F2RZ7SjNgK6iSexCuNfV7YglU1QIebMpabUxoPP3+OcrIcqNe
e7tFojhMuaHjouWKgGKjgEm8YYpNqdHfMbH1ZE7J/Bz2Wqk80389LAqxn7w5RHPR6yKoIxQ5FBFS
6r2IxonuL84g3UEIOG2V7UYZ2HsJ6eCIyZ/E/YwH3742bzNuc+daRFUNI2rBbtKugZXU8/RugzE8
tLwGjepYGja6Lk+jqV81A5k9oxFxYZ3HN+fYwguPp3WHB/omf5fRjhcuAF2Cs/U5FBze6Qq2Kc1/
tahYBkFZO3vgRPSMmsRffWDB9Z23DXBIq2iHCa82Zjq97FzBkOqXp3R3Ga8QxjRkjuUKT/TViN76
cEMWylgOz6N3Nrtc327Vlk3+qj6ZhyTIxryd4wgasdHkWbDdgpJhdou3i2koH8zo8shf7zCcy6jg
nKdvVIoQdvGVCopm4V/M7LgSKc6D7Pz3LoH3MnqWCAtCDGWYatIYfxJZteZjyfZFEWitmq2Kv1kT
W+8eXixKCLwzn+rTPDDEcxWD9gg5o6g20AVjhSXxaoq4wK5Je1H6Ggigay+gHx5/PMbZi/226Tua
vho0ZDeYXsfiahL1SPSOkU3AOhXDco/iaqWLc6yn/TpHLx2WJTaBzoIZwKSQ4o4AjYzl4bjcnjvl
06zrKN/Es0yvwJwcosl4H5jlMXDE6DOTdsox+SRHrIRMjH0bcRmIkz8ZJxxjgvOWUQVPhuucYOL3
ySIRDm/tes+vCWd8SCASxujM5iW9Hl4eQl6bmMW3tK0gKlDdLr+7K+idWh4tJ3dW3JFAIrAnFiYB
sahqX8hZVr1p0Bfu3t7QxvUL+qRVodLvxHXEOwDKYLbMhJ1hE1WUQAkihmJW0eEZA14nd/qU0Bsi
7ZSkEEyWVnMJzw1dqMbbyZlS4bj6OAmjdKCckXAWX5bgWOu8n66SxYFKCz4/XrQyveklfdXozAwH
szFp+3pXIKbc6H0Y0BTdnso3stBMIFORv4aUidlxdPDgbfZTLemtbgYD2p8XboVDL0Wad5BRwoO4
TMUVLcdlk+i8AEh4CzI39CI3vqy66g+XIDnDbEq+uRrHYDwJTY/5+vOwBFrU/keTUIBNwCK97fWD
i1VoEJoWj5BFHAFvsgDqEk5lJ1Wn9A+32/yjspfVrE4Xy0qLVBkrBfnC0JpiH2VftC0iq+DFJlEf
tpfs/nepoEfrORI8vjVJL5xAkbYY7OiMU3alKCA9sUNXXAJu0Xp8I+Qq8zxQaYokkWbpU5bG15Iq
6G/5TnFtf7ZjAtj0QA19b7F+p7ra2gThRiloaTMCBpaLDofl9FpIOK2vnDJM0iayZXSUDK8jEKA4
DycHTZL7IHXEwo0l6T9EMNgjLX2jYqM9harjqVG8ZV6Lf7ckrmt6RirPORhjDVYB8xWA801JQbfN
mu4zBNengVqDrR2cI6jc0h4PCME1Y4J5x7tQnuEVVqsl9nq1oHZnKhsxIVY8wutKkliMVaxhzNSl
1e2uuyQ4FsinVhWXlIwG49WMVpWcbcQg1LG9X/q5tTi+soDDu2CVVrS/u3AvjHvpBKOAJ8XrsJKM
d7/zac0xMj7VrFEQpARRWQYI0CRjbzrmO0E4rqn4nau2MW9fieOdUnRPs3Xmt92GeUrvbZmT50nz
WQnjpNSSSypXuc5QElLGJJzn7QohnR3JoZoY5wD04jZuP75BlvDYpS0pRYooR2My+zzWhMO0Zwd4
aO00YGUWv/THv5WeMWWyrufNbPwcy/DugY7UmQRHkDiXjfSbzBZDcfXiI8cGCWc000dgiedugO+Z
v/e/4ZIN4dixcbVrkyqu7HdzI3YdzhmC1Jt8CI41DKowhaBYyRoieGfROjlnS5STsW9BaPgi6g8Y
AHtHonxY4K9JtpF3QcoQ2n6NoZntlDoT3ZXPV4ArCl4IaZ6C8veMpwcTCV2H4UMyvhHSXZE8elTW
Enyjessesgr6WRJQeL8UDIeC0ptlmJmO7WcRZ+genuwikbqtW216r1bPEjbfJs1hjTVE8vnfEws9
2EthkDLR6xY97gMoTHTOCL5xbC6q3B9XLyQ9312aVt4PtyXGKASZEW6R9s1GV7xFo1KgJ/0Z0odK
wOlAV+0RslKlJnN44TWB2hFkMjomRCndsdu6RHdAiq9AxW56A0NUETJdVU5nfm4KRxLNSt9/LSvN
X1pi2LJ1FXcnbs2e1GG/05fNlpEK+k1swTFD1kIELbLcbez6skyrYSlGN8LRMHCnEU6spdW/ZZba
9xX3foDsM8VjrXl0NlFLeq7fUier0Wslgkb2+4oz/KMjP8C7rnKEOJYlYve/3qhI8OBNxAHCIz5O
Avh3Z15OKRqM3Q/MYt7ViwV66VowvnJnCTMAxlXLKHB1ElZzk8pDnuZfUcyGghNevRL1PQUmkVHg
uSFqkgR4+tAA+5RLfBlrPCRM+VVPbN7vqiLtBJLjWTcXR8k9o5jXLYR3RdYk1sgcwhLguFTlfi+m
zWXab8Ljmk6gCvKcLEzMYdnflLleX+FpR0gEO14fW94WZUeQ5co0oo1XqjFDAtGY/jyNQuINFlK/
RkkQVWO3pnRpILzCfdRYZuKOyPfCatJyVXR/0+iUfWpJcnGxDkSt19LmV5PJHpYIMnkOOyMsq0Ia
mYe77UoYd2E5cp7+1Jry5NGuOhRUhvgRlCrViOqgu9aCmv3cMfR0XkE3VyiUxV6mNCAdWKcgAxP7
F+ElPhiAHvxs8CHG5iIbbAa8xHNJrcW5YNBH8xzjDrUxohwNcPNdZaXh04efgL/hOqXOdRy1QwjX
iJe2y1KfFDYopaE9e25zubML9GjD5zkYpy2/eGAA8eoCgVuRN4LFKRCmS7v42lcSdHxYWMKge5Ra
MRuD0iHADLwf77Q2fUuatg5xgYA6X24rsyLJvkpzKgYaDVnTfefXaWdemUR+ldbXaTTkvc2CLkb0
x9uH/XDnHQ3zuUfiRBpionp3WG277QDkYByky5dcZmHjjBojn3eaEc1r+muVA/lro9qOwutiMZ8v
Q3EMb7x2eU61HCfQpap156GHbQTkRQGoiXVIIPcqor9UF7cg6spaFfm6Q5xnqHsGcEm29BFrs0bX
Cl61xhpWi7Rwnhx3gHqZJT1QK5gC7dCVKDZCn687uSAMLlo04H3wh6KkwGwRbnaNaoVPqK9/lFkM
8fQ+OLR4VvEvw3u6w30tR3ojQkVJxtzlGGL+yKu3IySeIsiUdxO4VEzM60iuJea7Qg7OHJeb7zZy
V/EpJWQfA+Wq4bUgccjWGC63cVyWiAEVI3MQgOwZnLWgxNJIehcdUg7HyUk52F0j6rYU4ly48SuH
y7PaeRhTpS+AGD+Pt2WSZTsTRKUaMV75I/70sigqaTLRl3803PVQ0XxB7CW7OYIro7F1B9nPEN3J
WaELF0iZeSofyh8oFHAl2As95qD05AZdfGwZi12QsNwHhP9keF9fxlkcIVPCSbjPdoiaGW2io1U9
aIQAcHO73hHn3uX3NKkEF25sAf2djfAer0OSC+WIQ8+gM12vMvcTspVXw4ZFauzS6D2V0Me+SI+S
t578OFWzfX2/+SeAi5TC6wS2vXyNHEKLBGtjadM7d2YrvoDbKxAWcB94tf2Lb0Er5BfBSwVeiWPB
womdbaOo0aKT+lQjUZBpRNP18GcbF8QJS0EVd4RAymKJPeyQBnZNZR7Krs+/o0RlvU5dKdvKN0Kl
oV2buIuycFCbk8kZmKzUwxZHzaNylnCwNaiDhyittsQlz+Jn/ZR4TwlbS6okuEEGfH16wi523M6N
QYNlLcJb63+oTsdO3TPwxppJt6KvDtJFlrysJIkcCRFWLGDCp4X5P+jM5tGoG+OFMUDooktyIP6G
z8ZqqvsIT3ZPkDQ9Ozcq8L97SgTqM//761PE//OTap81AClErD0td47K+q+gvv2WjDzqHswhFwda
UPxpVGZ478OwLrv3WsQkrF5WH1f1xtNUGEvfkQDiGl3DWoEruzOl1xpEwk7u2V5uCojJw9vuyoZ/
uiI38QmMNBmiLh9Fh41oiPN5eDI/uqZ/+7gHP1IefJmUyGZVimY90+irK3Lf1oR6MW/tVg99SibH
vw9HlFomHcxXPltHl6/X2erJQww4w/QakiDjGgCSxLwxm7eAv8J7Yuk1izA78x4Ct1L/n4cZcPqt
g8GHLrwf6DT2oNF8Ksi4eeJJ0Nh0oNJyVnSmtfnM0hiTKaKf7tQ2Qv7dqaYyiPrAHZ6YxywFUWzE
BQ3UYn8meXk3sbYttgMKt3ZtUvx8IF/k7o6+w6OGZZY63TFKM60gZy/i1lwhkTXd9zZ7KIg2NfH7
In6AY4ngLWF9jRcYmUlp2XR7oYejBanNqOv++uoM/bTlGWvUwBlf1CCcbADFUFN8qkPyEHipJ2dw
J5J6JCBqbpYIAs7/P8tktEv6Wvyhmb15D/AJ7p553irqx41L1/m0DEa+p3RMkKFaZjpb4xVdughz
+5S9JBjUNdSGL4Rs+lTy52m+vzeo3fFj9/b2cTTzdKlhWQexbQbJwokKLey37bIfu5USr9ojpDV8
YDVLhxr7tLRg/StnLRKpGP9JTRP08wVRKtlW1kvWZifAK/h5VuNrEYVlyyGIzdSyu1uBfuHTCd5V
uB1fXvO3RMBCz5TtBeq2RvS0jX12xOgd0Tz9oLXZEyIW1WTKR9LxLxuwZ4esfU7lvJHDdb+KKhmZ
AK5pOQjAKzsmUpRP6fTXt/7UF/J++nJ/y77i3m/WzdLmhACruN6xNh4EUx15GXUxFGpYOJAoKcgt
glONyFyXd7MLcFpTtLNO87LuJ2RFLmRDR1zRuIafhKNHgnWz9GbLiG6+ewoT8LtlrRZctpQZvC+2
Mi60ymSJe0BGN2yIPuibnYkRxSDQJ7QRQvavwnlLO9qttWXsP3/FahNiQC3Ck94WjYlQwUuid00d
yfQEhD63iFoQYV0xevNKdoaHM+B7ses4pF0DAY23Oa0grjErrsaTvyNOqtxMjwjwia8XmCmaoVRd
ELYbJQMmBaEAZvORC6SJ67w1bebnl4UT8b8hLIpRW4p3IR5vSXYeZNklVfVXp1ACjnVSou77DzCk
pRVQROXBElqi8uCE+VxAqeOUQAqAXzvzXdP3gvycOA2pvcYNVOJbNjpm9993vGjTcqcZE34/AfDv
2PZ+8zrmtY6xo1Xa9rVv9ItNiuG+YPnMdR4E6/JEf3NmRKyFvzXGF+WBTnJ6V18bYC6b9XRnppZj
EHf8MYca8TXvpZcmzI9ce97B3Aau1MqcD4ZTP16i0q46ya5SGiomC4kTmt9qFtHzpu1F6tMBHDno
Mg5wL91Fz0WpihbCZES6cukq8zkuOOpWN+ztq6SIt02bws62gJvctxVBT+QHnGaKac5LKfb3wVg6
bHfDS9u1DaoS7KzCWyRyzLsfKwWOyF5CBrwSKutBC6COR4vXTx6r87aDk3TdNrqWIwcGTSDaF4RT
36pOshUKVz/qlhSNmGxIgLBPsVSUw+YUp2AX+Otg6GoDy5Tb7RjYmM9Q2Iy9kN9KSEC40Mc4ep3n
/eHaMwK2nRFEVvxe4iXwtle4b4RiT8qs0svlArpe9bVF9lm8CK7scyD3hj+l0FSNGQ0QTkX19SoC
8ZRxTvbhsSXcY/C8VCLVSSLfxOFHGVGUjyHhnI4SnzHw0+Qt+dUkw69XRMlwJgF+J2UssAFkq+/I
RLmqV+WAAJA4Y1ytAKZZ7AavXrIgj8aWdtJOlpm7l9BIcMKwquxwKsaVP65cHvEOQo85gPeY5+AW
7jqECRoS3vAIvcZkuXPM0eZE0IafVU7ZFnbT60LEeqsMJUvseMHIlWVdtKcwuoSFg3DKWTENAdBh
EuWwNnq72jDM1yeOGcfVoQNOjPY5wC3iZkb/IXoHxQ6J7SUFxVk+b+Z3K73bX7oMTe2lCtA9eq8Q
KXOjeqE7/xGvQry75vBDCM5jtCdHIRXH7Isa9yiYIzyaJ63GCb5nglJyoapGpdLKOE8PkFOLNl1y
AqAwxE13pX39ed/8+pghGHzRCw+FgkYDfL9kMYcA+dNYMC3w/YZgEMQ3qa1yhAOw6VgJO0VJA4BU
e2HRsbbfwBC7miCqBkM1Bg6d8hroEkvvKJCbjkY+mYVg8d816gF8WRVKmt9pgEM1XHWdUbj9srgE
DtUS5W++1Hhz53zTUF4SUBr06WaNbCCfIxSwbqjRCJVwE8uUEHtjwjCZObBYMSLiwAWe9HnlbPbP
ywSUNwiEZzg3rDUStmA2asK29MSoehXuwoDbHhaxVspCPVx0y3x0Jh81uG3SdwvqCaG5fV8ejLnW
STToWISB5GFIj2hFCvDY0A+hyVdhW1d/sBk/r+BAQSCHja4cRFLyBr73ORS689iia+64rBgA3Fot
cU5qhyVxrdiUxkiDKibVR0wMKu68U5Vba4MTC8wCc9MR0ylqZYreCWLTgIbnGrtti20NwwlF8Z+p
Bxi4J6VWSwIME972uuCF85DXo8z3rsUWfrsX0BPMKfID03QPratKbJrZgCmelflmpl5SZrx9PR6k
kas64IANtcon4trUCjsJbcZIiUMcfaL9qv5BUDsOcymSeOejAT77FmfP0V+vLVtMmDfVH7VYCZgD
adaqRf05/yiQnsLrqad9zUbEXL3blL+425NxuTtGjmRa7IDWv4U1Uj6U7pd4fZ705xwIHA9If09u
+4ES6FcJ6ZemrABUQHQ8NuADq0oq21wZua38LRDp8zo67GUMiJeJfRQNSmQmnjXBONZWKDq7O45p
ufO1zu6CUsk0elyesrpIAm59ELiJDA5FMyXIOHGiZP8tG+rPuDECHgHJ/pEtLGg10j/MdI2woXq9
a1s1smkHCGymKzhAKYimwXu+DyTmQmPVQQFtNZqWs3zwEBfG91Lsvs4MZJypQ/NlRBLGGVuUI1LL
OEH+WsUgD5rrC3WECHeFfhB/FbvFHrQenL9+Ol0NoZ2pBtvASnLFH9DMtRaupaAwciXIdjtNUPK6
2v+SWIAUbrZUY8NN/WWs/Ms+bVi/8xYrmnzwTeDhoZqw33jVy4edlR3+wsIYH4M5EZwM8eMHGtko
k12B8wDtwi7WZ8ZtVSJvjyGn4Lm3UuQFNyTNaiKKhDBFJ4mVBtQwi7QiyhSe9T22xQr46k1Jew6r
M7HNGOES7x7b3vkLVmIEdcVga2PA9U9esT/2Me0HDWkeZZguQVa8jV+nj/BUKUl94+6er7YeLckL
vVo6heRXwKsGfPcP/vrnc9E+9A5UjTQ+L/ayoRK5FLQmAzfNU8QzE7j9DUMLAPbuh6s8TjjXAnsc
6fsXZ7VDj9DzSKLWSRchx2CjQEh3gADX6GBW5FzCtMl4i/i6GVHaicZgbv9ZQ8ch5xwTI/WJ5MTM
ho2R1SkrSiOHV7v2v4QCBmhS8ilcz/4Z7GIh8/icwBv4JQ43xOypGuOEgbReuYgy5NxM6g3CNsuB
eZKp8kcrIpDNsruaynt127yl32WVu2bTQwO85keaUziFbhdVVCFKflOMWI6OjewkqShyAtNmPUMb
fYp3ZsjCmYfd2InrJVbhpLfuhzlHZzdw8zE6pd4HC1ZF2R646VW/NJ+4ttFYbHcBYpUnA4OK0A6W
kZEY9t1WvpRQK9CjON23lopmX/CWiavi6dB3O/2XXFj+IwPsxF8zCGCdeja2RB6sG7PH1hNdgihO
2vgt5xjMpObxNf5Ctwtl6kXEgQdbYZTxu6m7fP+pOZisGbljQWbRtfhF8XKWiIR6bd6hihsoceE5
ZgSRswRTXpC1chR040wAjUG9fHSGva5VxSFFBZKENHN5B+d/77Dw/EmN2BN/8Ia5IQhJ7cKj3WbK
448gUrmlKhrbjXCfw8O2KLkCy3ZA20M5UfmoH3aWyXg4Xt0qqlvKx0gP6b1hR8i1cHiNIhFUPTul
pT7COI/0GFQeLXWfFv0goTikXO4iKsk7QV3D7gt3rilDlbZ9G5a+wVNnDXl4CsxDN2EV4cXfa666
L/GrM2KjK7aMEbqs5xTn6sDrv1wr/nxZTDqXl4CiwoZUkpWy4RTfbAx1UNLKr5NmOei1iXpRzwHc
2DqGx3XbXlpVdruBV5uCny+dhxe+3y1fekzcKiKT58i4QX9jBhIpEL/oKJoaeZdkrBVaUid+D8vo
8FVMzl4nHH3i/vMpIbZKP5bkQfS3/2038La0LHyvQHKFiSvy6WauS7v+SwiymIBmv/yupQsumyGO
7D+/OkqSFkiy2d8R3Tb6/YkC8jdPFjsqYBppr6Rms6ixpgnbhc+UU7Xf4rTOBNzHQQexrnsUFuGg
QzHkQZXslhQY48FrjmjpHUSERYGSMPoDDv5+eEa18TMSHYsJ0W2yqOsCkahREu3POLj+q4vE+Hb7
GDbLX0iaQ3Y2IFeO8I5nGH+WsZOkVLP1EAm+Mp/ZO50mTWB3htypAOH9vkarY0NgpRx0tJionZY3
RVIlwaiK5s6BRg+KPj9E98jxUg0xjrJ3M/027NUjSbrFOJEwNC8njO7cdbKlLVmc2ui7CSy2d94m
+dMp/zpMcs/c4ELCAkvVU+mcEehsaJd6BjSyOH22w/DDfhkW8JpHvqMR7jiILCUQepRaAmDyAgvo
iw+0+ezezqW37QCh1BZbPzCTc84ZkGHOLfFZ3Dqg/JNI9CszKitfzx5wJxTmk0zgU4Z6SBjVjhf/
peGZXYst1jWjXyTEb4vLHUsbnx22rF8T4dh1oadkYMdPJydSM5Rd+5YGj+hUJnwkB096OFAtoUgp
oIIXq94nNuSgIk5Rg3+ZjKuF/qae2sU2RsX+Jduio/RKeXnpBv0cmITn5yvUQW9T98ixC7XHwyCW
IjpRSPwJRgqTY/HvZLGAE2yHuCYYaCt6w0N7Q5e2kbylz7uqYjiMLGeMMJZrUe0YZOkklWjjijmg
9Zf195QAR3lLHooEpgrFCdnIiXHzL+wuRBa0OTINgi7/jQAtsIhoON0BwnWPL5Meolp/pgcW2gGF
jrOCdX0U4xeEw5izaxyftB4liKPNW4+M0Jsyx02Q1Pfymn8V9iHecYbQKss86S+5Sr5yVk200TsA
GlLsRuF9Dbc9tUlCIycOGBzfuYRWwCMl5TNCpUlCfF/YVb1DDieTm0a2vCaULXWa0MP5//eGRW/7
0w0mlQrkm5yKLO2DPPTjCjORUYaDexQFc4wBTlmjpeOfeRLD2DIPdNhlzzvsBLnIlhRcWkse8FCm
ah7XLC4vD7Dx382JmqZtXJuY5JzGYZ6LQCq6kpmk7zOxeregiBhuK2O/DcSEH/zm6pHgD7nqL1b/
hg4fSUhdAFGFUkacPMcujMGXyA+C/5DcvE33Jb6Anv97gXuMjAvDKKw4fl/hpuFnTuVATZNcbOaN
KiU90GL95hSV/eyx2Z187+Gi9+ZkVw23FjvErQECo1tYnL5H+rN3rKlDMGQ0/fAD51F9yLj31H9o
qbn5SaVvqYfVwUyiJwhHx2bVJ1Gom4WExMGLAZYM67N37CkuXnwKSm/EZlKa97Xi5SUSFccO9sN5
A669GPU5XnhnRccXfTOSnsnJnE9Ts20k2i3YwC8kBEeGU3D/oQ4CWKOriI482egiJrkmk8IiUQpd
v9sVP2YNp+IB/h1CbOUjTraB8iGlz5pNqc1fYTyvypLI8fi+Otvt1RQcDK93I6JQFUiZJFuvEcAO
Kr7g1wCnMxaqRT/dSSMPIDXzLxjnuASB8f2qjaVVL6VHbflA9Z9hbNNfKyfzzcMRxpRq5ozAioML
2ZX/etb5KyS7/TGKptXMFC4xAy2zzH/kIx2p6YDW7tl7QYDaXDdEhGhZKiTBPivwuVz2ZbroQEGI
QvB2Q3zL7NFfgPLnh0ylUAU8L4bsz2SnwZLopJzulKXRuhhJIDrlonZ3fUSnFh6qRWyRsNXFfjbb
oOVskJfhIBLWo7P4BjKYtx05MzpimRa1SfOEYUbCITXyrFhNgN6+KobPkAmPV5vNaL2LDLimSy82
Y3vi+D3XtHTTxafiFfSjv079PXVtI8QzyceBVwUf7DpuZDRdORN+ARZ8QUxJYtO3zOB8W7TCrRE+
RD2LC/161QygNlks+OO4636ynYG23KAxrcg+3y79nnZ8ThL1V2LMtI1G1ogncYq0r1ZJ1Fv3XmQr
2cnPSiO3s23Fv8ElLneI0U6Kn/S18a/PySY3a+DORh2Vwv6f7369k9oTE9aJwCpCBE8oxIegF0OR
TOhyzwSS9Puz79J7y+cdz5xfnExxiHPFTW3hYiHTsumV8Wot7598Nc59CL19j2Oku6ttSh8XAQSQ
9ipOkT6BRHFWwUaxubtNCmUq90wz/Wml/iMpkt8MXggosVGNCPRtcOv9U3jWO7f6IGXL7rRnGHIy
iJEKNiToX40rfacmZcUL2pxCy8jj4vXY1ArMH+z23WNbnSuj0VVs1DwOo5QjFQ21x+dqx0Av+61h
HIoO6kL/fKNDp6y6L+fzmBIi962YDQRCu36peAQUj83wm39T6N5spQiTt00lePM2ozeWrcjxM87p
WXTWa4UIkGt6/Asnzysonj5/AExWl25K2zFYcSuveb0PFPKAQ/aXjVHYTrseXOuwiYzzsg5V9mWi
b91AKmsWy2uvh1jVCpsLS0RQ+HybkHQfv+ZFFeHzBszD3s7kg80kDg9HWC+6hYR/XFy/R/bdei+2
5nJ9hQQm4R/Xfl5vUlMiMRa8iNWF1lPpKEm1pyW1KB5lr95vFEl4fGc4vmVILOiV0SfNXbnf+3Gy
AUPQ/M8hUs0zF0KTVPzpzbDtuIynuuD06gDm9JBhXvApr4ShtcSfl8mdLDMGceZlRkvRHxGy6Ok+
H1YYxOs5uVjop9LjWVdk7Dht5JvkaFuoImIcSYXaTT5XLzWAtbOC+r/zs68U7shC8lv390dBlqg1
44RXrued8PqV510TzbacTKmGb+t7AtMmjJ83yi61miAg1fT38sR+gpFSMgLCI7STxyXsaDkz4A4g
tNgv8nJGfWRHfp/6WcxJYWSlCOkOr451sOGpBPtudla+PmaSvVGwFNpszs1ig6QV3TyjMpaviZ+W
2u47ZeQ7w7Lfg7CaD/QcDgQKsI3naCJmLsPuv0veUhJ+OGo7AxoLt1SrX7Jux/XB4e4yUB9lL50/
oig8Vn+W8RfdBH8FSg0gjS27lVziwNHLhYdWPllS6bAaABb9hdct6bG+LhYNfr8NlRmLdBHog9u0
lkmoTRs7XlWvQKDg7wa3w5edJ2r8nD2IFQZm1kVQapXXUlCzm6Zua7Ubi5JrcTtm+0tsfwARHoWq
e0a77nG/tktkUe4gF7fKA1JUMJqnJaaXS8Qd25xTL0aZt6BpxnOaYyICnD+7/5Ylk3FufmkMjcYd
v9fpfKQ9e+Unnge66XhPbn1diCAQrrB3nIkp05+ca34GGNNut9r5TTNXwO8yrs/7dHYKKOA+kVZ1
p8Rh/U0z3BaWAcFm4znASrSuzf8WLA+lfEwbLRIVarhVTp77JbtoRAA6qgMPcC0dAHbTJABBo4/s
ZRYrE+4YU+SfWTEXPSZ3zKQA0keJKUAuD5PxAR0paS8SbQYcJir/OTLDgy/iC3lAcn5o9Uv3+SRa
K/Cli+ObQeMhgzyfGNdrKFv1iBuquWnaP6s83e291DvA8uRrJ5hsrqgfcouABh619EdEXqzN69CL
yg+e8HuodfjEsfGbqXQGg1snpUwH7YZ7kINq4U2xCLfjZi3X+OyNsbSeiJjhGeSrAti34cTYY9cB
x1PWIGqSbLSpHq8vB7wpwB8Lm9xt4YOz+GBaYdjl9omTGxYZLKSdCJaPcQCBxYT7nmFjuXJx9St4
uKYTY892ZF2TSu+WItEwdm8v/4m3hQgCCvYp7++w4j0nvNhOtPlrB67NRjfWy67QwJNmJq6o9DT6
2N36yhblxdsw4/wI04pWx33LyZPZH/XhWiWWCHLQMJlFuFmazHOBe18rv6lEn4PD0cxdc9tRluvK
tafNwEfiFmTqc31zdOQXKnm370JkVc2AjUeSTHb9MTchyzwWle7QOrZrN5bkX6NAE2ZAWDacSHIl
KNPpStCnrbObig4gEovxo2fceI5gntkkt9XYlFC4uV71GEzWfRUZq9jVitcWVjMukLawOqyJkTVw
wAuEFRo0bB3ZbeEOZD/XzIe8QNFmr6CebTt8Dw0/43T/9L8xMUGY6rABn4Ee2Cins0h7aI3A0oc5
3rDZnU86qqGdECDytHhVscLc7Z0oDOHkId4CW2pT/NDtY5J7wYFKxLLrroacjYuAcq2PdoYr+jSa
o4842N8Vo/PPPeIqt4XxdGrrrj9wUBipad6Jra7GDXiDkbphzrgl6bBwFjfG9IknRn05CKAfmimV
SIbT+mR3Xk6CdVmuAVB3QEQ1eEDhhItZeQq+3SqNQ0eH258p4ebMSUjWb0PxsXn9b1LIu1I8pYbl
K3vKw3/KXQJZf9IpZA8MbLXryGvQmVLy2jIteGFPt8Qsc7eXc2z3tktCZ5PkEqRqIFXdKcWO4F9+
Ax6tNq1vxFTBdDOnOoeTr3bPZrT+0ujQdVvzjhfzAzm8x2WUFGFKSYuZQoDffRB+bm5eUmNG5GUa
JsdF9VaBfSBQ6g9Tn6iN4yEnNjejCX9R7HfDNvEOqukuMqdpM931dRhAW2h9Pez7+KgUJODBSkqE
XSfoQ87a0joDnYjox9jlQEEvtxF5p3+Rnxcnh2Prjuw4fsG3H5qR+83WJWpnRQnKyqky5q1UzlQZ
DYfUKCQ8h9DKuaG/nNqGTjG7RsGpIi3cYCljqALal+z1rcKJ91sfghepShxR90qI7rXj0yFf4fkU
m5UPMhXlmDHXl0q1Vp0Sa1y/9i1DT0UoNwZYrfBgLeYYJnFJR18ZP1YRK+46KtVmKk59ScO4Vxwx
2Sa8e4iUYzM9uwjnwUydPh+a8REdTbsQsGpISNFhmcwW++dW8pvoRZmHc0OQHYq5dyiyINFa6ba8
aHtIB1iSt2a/0E67pB1cZ3dFrNYAQ7STlK0duLNNbhLSgDK/+LsUZfKo5W/bmVaWWe8GL9fG3YuY
wbD8lHsPrBmlPzGWb6chzrV4cUOp9MFVXlbqENMyY6q21pUocyzzvppvtqEze0C6tS1PSP6rkwq1
zhTK5nFixqQ0XCYurxVfi+Z3tXLC9vUxJ2gOPiwpAhT4a13hkEHhrMRyciD3LapVSXt8MeuTi+ZN
3XXBXtsVKJ8Ber1X+Hmfx4KpyxXfgx0ZPzZnEJF1iLCu7QEfdb787TgsGL0+3vuqcPhvTP9cDEjF
3hks6pa+4ME3mxtX04OD5TOUZrkpwUO2Tl8g10FUFUWrxq3mmot93yO4QudK4hJoLQ5TN5hp9Lr3
7wE+2U6HY7rkhcrvIY5sbNbGvVV9MA5ImheNx54nMo0fJM9OYr1uI63DbOgTMbh/5tBuW+lXHrIO
SBQPxgC2xFyZAwizqeN30kdB6teG2HrWLBtvbuX11j/F7n9qETnbJHvAdFayFnl7QtP5AoHpQXoj
BFPADb5+vyXECc/Zsy6mrsKR/pqO6TqdG/6Lnx/FzZLN6z96T9hh+b/yK9enjCREOc7p+iokzsZz
g4qpbzvtqP5B4xFnGaOp3EHVpxesu2tXKYmBZNN6AxooGPScHp7L7pzoAYUL44WwjC2RsDic0UlL
WIhobTKOIk+YKWRWvn25Ycp3zd0LeaExDIgf8YKvUk7tAKnE5hCf5g2dsw0s5eAq+2ZidIVebzTg
9ajJbMk+WEnOSZCgmD4g3xnCfWxQjHzCMngNsb8skllbuGg5NdTj2OOzqoe4uOprb7HP52leLy7o
pU2zkDwbozqVY/k5SU2kJJ/VUA+TCo7eWCbZQjBQFgsp7RHsWCO3BxWIyyDesxkgSmpRsC+fCEz/
SDUt3oVjAS4jeGig+c6ZwJxnSgIwrzIFCV6qdE3o8fxlwhKE9MnfkI0rC1OkFspsoQCbDZ3gpMzn
ld7glRMcQxraLScDxC59W/w/BzuOhw5FgWc0K2MjrtO/H8nb57tyxylhPSjpPaDMptBzUHHqfT6n
icExL9+b03g3hBkqnZ2bnFmCc5FboP1vNAuzBeipPCS4sf9Zpy/l3aDyeMOek+TfKWZcQrrbcSMi
cezkc9wr7k7BwqrsrW2375UWu0ZQUXm0rlTdvwVLojw8WB52RuzX1pvrMbzV9SAiQM/KU38qNOAQ
YHniuxye/yK1HWWSodxr9VZG7KND3yWyr6SAFg79Ot9B6AwMRf+/BiAbJl3zLlOqmTCRbmrP1aRD
IaGY9YJm+A6+kJgu9ytrIFqSF06TAQwtZWQlOFlywLhb7ymOzRWrbJGStizo2k25oge1nJU/MJKL
G8J/oSpDaZpAE5ozs29VWm61/Pb9sZYkGXfeEiuINhW0YpJG8pvV799sSJbvxmLgxs4RuhVFGroQ
6LRRdtLFuNyEyM8DX1knPfXpaDM6Y5+WG+SHub/VE7Juyjmm6RsSkFIb5iVPXvRe78zJ2eZKRi61
euF9PF+DOYmFo/A85d9FFKcCv7gIvWidTfAFYshaN/rkPD9DdMQwe/Vlx03F80NuhcHj9B/NS6pY
pzZ3RV1L5XksjxQt4RtVjGjgDgq1kEFHR6V3D0IJGeckx3KKVcz5DO+FBPoGTFq1G+TA4KTXG8bv
6NeNqjxtGP+y0KsCmv9qAWbRcZHp4V1q+IoarulTjdz20AQi7FrPII+ZYanUSFzlIqho3kTV+Qo3
Um8hTx2qSnIKFtqM2AvvLMENZ0/8ydrV7I1fcc3A1zVCrLalHgAck5KtYFnh3FpSj4zxW5+ZnkPk
zK2uKQ1rrN8NxIBUQAFtu0iLBbBEt0hFoGds+gm1jfoAllksCxHg3VJq0zTxcaN2o62CGod6YhQP
Z7TPA/Un7ljRgNoBl6uwsajAK1bYTdFHHxxeXf8EmuqIX4HoCigTRT8eYIQmuCrOI84rtks/s6aN
XrSZi5dRartU0K8iFnCTgZdffe9ci89XK0JAm8xrgTeLZmiN7saB4AckNtQHk9+Ekxp4QuCq+HHC
fnhkfaljz8pi0PP8xEWqxOyTZeE7pRS5zdKe1u8OhEQB459ciO97MZlmI3oYCmEOxvNsQy5DIVp/
tKGlN8H5tAAZaINHbMXIs2M9rdLF6XKzEKhmAG1zI5ASyrCWdPoH35jS9yZSvY/iNZ2Kj8Dc5qQV
Z4Yn3FBn/fYoIQa3WaVqs8LwsGoJPX0n+B9awwOxWYfcx9+V0TB+4bEQ2seBrwJ6pVajvuf9imr4
jUcepB8AZ7JuotWdSE/Xtqssbtuhv9eTSK34Wtv/wzeo/sn5OQ8SDalOQVs0bGEBXyt1yh/o4TXu
Tjz+xWYk1LGPm/9h5WCFULAlJsS6u8GuLQUUkasL4wrG8uXfIXDhZoRs8+QtQoBervkuK7n+4TSi
QD6piThpLOWCcJJ+Qmo/ebqAJ/KcU6+qC1WPI39SR7hwQl0SoS5LHmdSL3POOfQ/QXSxRtjVOWED
qXtepeiSWYQWWgX7Aql0AWWaPdfPtYg58Ihf6iRApV1X9V+pvxJ+7Zgq+Hh45+QL74ZBrUpW9UBK
MN3jW3u8oVgZGorP422hopJ7JS79tYhnXph0jzGEflXoq5fSE/mLnIaisc1yJEeSIiJIEFLMNvBX
VpGwVWvSXdjTEjxwkE2/WvNR5mlR1FXiNUAJuEsmMUmu+y9imKIlVtKla4yQ6kGsLV4d96R29eeJ
m55faVFv81HE3v+capwZjcHCuS80CwnMDXJkEFbggjR48qrbmTKycdT+QgMPxye5yPXYSnMBZ/VG
M+qNK5tvx1gyrHZmnEVhxqeyAqP9hS9jwtTOWH2hM8ij8Mxd2+kUv7uYTreQy9ynfEEBSEMvj+9H
lfgHvC36v8w7YuZOAS1kNGGOPc0MctwuHIIhwfA70OeXKaNvMY8UcjGD/hUJ+MgYSDr0eQ19llek
VsQIy3/odwCeYEAjmoqEuMG4N5WPlWJ2JeWY89ylfY6q3kIZ9ba3p5zLMkt21+c0X/xYVRj+QjEM
w5YfDw5CaKnXUxt5dCNcdO1RZMn/zpY12BAxjV/IV/J4MbyYcTB23ZWN3Sciy3+hOBl46oH+MpvC
MZOc1PLM+7e6TvFmpqLLKS78eMt00AT+cqZ0QziIuiHTINufRLwG9EpThG0rlMozxMSpmWQuc1GG
33aLRpi/oCoA/USOmtOsJuZrDxr4Pp0UsW3Shp+oa9cbdB44PBGSkqNHCwEs77Jwl4KMLyK5Aevl
i+mCl5cyiurITcK5zSI8DUASE0Tb1LIlOgZkFfU19+HucvmPSww2XMmpsu/w56cezIfhJe0B7aqr
PLj1OyRKHFKocZ9fKPxZNGnJ8WYnE20R/kT+Q0OFGAYzi05iNS6NyLXR3hOnGVK8HCroQJNgZHu/
W73K0jxGCnDONsney5NSj0ibyXtp9pA/MTkaO7xcmnBrTJtT182Q8Cp24EJkF+3mnrNVtmqM+BqP
5zhOvQFCHQYJ+u/MOO18WJyhw27r7ar/TYq5jzE9H8RiJVyako8le5KsdEj9D/ebGocGUddp/H5X
D6TIBWw3R91TswfaeLVAXMsYCkkQf3ELzkOy/pYr0saKOXW8JWZqXKhr8lbCaMqQKAqW466ay0ki
wkE3SHU99rm+g1nwKjWg4oHijxouE3d/RXeIDplzV2g3HX9LG9Rwz1Gw5K7VKRwX2A98XMrj6wPz
TlHHAbwbfSETq0VZDzAeC5RJMRRZ2jNy2eAgaSiJL7jS1C6P8IkoQCjqIIiLyJfyrp3GUTG6k9fH
a/NUO61+JP8WfhyUQIIYreDMdtW2RfFEeu2MM/7sCwh7apl0AL8vkkyPyKFydBQ6g+Wg6ZMb/MaN
lfeYANklhrQlKJQhcP4Lej7KOfTuuF95spC21rrXqgzry7kHjWbLE5K979LAEbAZE4enWnttKRoK
ndqCJGeC6VAOlfVRmlCTDwdcPBK4F47fJQAhym9FNvZMRwkh+/ExNuwIl5y3QjyikcUXlMdreZld
VP9Zh+8ynpsNDrgF1Fq0bJ5CaX2EaW1NDjqPc5CRRiHXv8OryT7K0Mag3GL5n7Rzh7Ux2Ro+gaX5
KbWuGfMUc2I52YE6MpsedaoYi8R9Wvuki/F5JawoWEjZTbVOv9NJA05+4mpg2zMLMcgslWAH+BK4
WWd7SnSed/F+alv62zuBsp6YhcK5suGcjAhlxrlkSLZWARWtta2ZYc2bsfHhBItjNSef2obVtwkH
t15n5dIihdxWCawkXqxxUUq0Yo58eZn9oRaErFbdZnlt7q3FRRfCbmVkJ0XCY+UmNhpfEz1Xt2ON
kmgkuQ8lbrm/Vk1xXe0n2WihxZ4yxVlJRjOjjihaQ6kqILPFgQfl4vDtLRlu+rBTyyAUgJJOKUR6
SxeB04vj7ysScsX8szCPUuMRjEmtywTbiKLQj57NLafpcWSyZUSXQ8QtSTDB4tdP9k5wPrFFSKvH
QczofY04QocNwjQFmknDIRKl9OCm6QKzcr8Xha5bKE8DRHMMlVwpUbKLUPzXVFUDXdVuhKgV2clO
wnm8giSpwruG+Xb8BLOgekE/LV7jiS+uIeo3a29xodAcfZ+LogymFWnW/Rxfl/6AARtyAPig4xU2
czrXSVFg6nCDJ1OndkebW6J0B2DbATgp8qyO4WWbZuALdx5SDPB/mKAREPwsr+JKMUbWRV6ywBKE
UP/qaKMyqXbNQeqwtamgtnpgciphRifxDomX3NnTDHGVX7b1ykosNXE/8mz3JHIY5PalTHzkjtii
JVPbXrz2CHG2r2UHRZoUUKNe9emAX0114xviN6ZnyaMSsDTZBN/Olrfda6M957hKCGOvJjehFopM
5dl/VA3Y67/CVtEU66XIO/dYu/DuWad/Xu53lP8b7kOyyBy4Pkoo7PAO2u/YN9dD2P3DkFgB0nsU
gXYahEHpiFHmyfC/vFn5USMG6T3j8ixxU+3Yi5VoMhDNHSwtwx6dzn8ppaAhF+5B7wCGSMtn99YT
8ndnkt0UhdivZaQAue5g6T4BcuS7HBF4DmVopIFvAqeVECN5gaQYtj4UZAZkenj7YfxtD05yswG7
T1KsCW6Dk2tKQL8aoiL1H3wKWsS7oEqnKHggpGOYcs04OYSEVQoOkH6A4sEtmceXL1U0JQuYSqaN
iFvpJ77ArI2WTGdLRjqHiGw4zBIWeYNUux29GEUSBO+e3sbM4gXcsLSndJTjzzE+ZnXNCFqMbGDi
JnXZa33NZ32DwekKwCBi4YOlv7P81NwA9JJ6iB6q/K+2xil2H1Yg8/3ZfrAl7ByEZVSuziyfirCP
D0QJDIwVy5aqHzWFnNlVAOQcRkBcIjmlCQbmzJ/PE4SCQaRkwIfOsAF+VCR4keQ7wptSnbQJkrPk
Po4wQJHZ+X40vH+4Y8aQkmUVV4rguWrXqQfHRfohzGMCzhxkKSmOzcQOLuOhoVjE7VNHwuuCOJn5
vtKYNo9JanN7hl7Kv8C5+GaEYwT5ziFMW8tTP/nDjYNfvxKUGXwpb3dzHsoj6IhHwJMLK/ICQF4X
/GuVA/tQT4baiWTemQVyaTa9P9gnKGdhGlNJvrFDcTcTDNfz5XtzyqyvW3mu7C2Dz+2HsJ7m60MS
IVafKECMNyD1aFjSKEU8va5ujt3QV90BzN1/+Tyn1AoDZ5oHqy5hvM48j9IO+T6rZuKWgqzGFYKS
LJBHPBjg51+x7C5YySEvwgyWo7NDLq+2Rrmnr0IFwdNphUo7uqdpsihXbc1FNsclsv4rsb3x477h
50miIxEJ5MIcKy040D50zoO5DvV96ikZZ39pTTFNGYpaQEJpZX33ulQElSMCqlrDnkzvGbozi1TD
B5XbvDOZn/ei7WT9TnMipp5AD4+s1ozsuxAbHPuIqQYdM/I1+dyIfd4EH2RYSCQ2kawTsN0g0B7P
XTRtBuhPgcgfjSHgEJsXC7QCJGQOGSpLiWcK8RQq7eWv1aZUjGVqRa3MkUXvRRjB4XcVyFTfczj8
d7ODfis+WjYvx/nFnE4lgpaNNfLTHYuqupLjYhr/SwmaT+8hF7QqhVkb2VOmpZDMx/N6m/8qoN0x
9RRexpVj7ndSQso/WtP79Y4jw06VKkUR12vOCD837i7/Nk2f/UyfK1fi44O/k6pHWh1g6CDF+vzj
q6fjVurp7IfgCsBIZElqhL1wpqLcCZehlPqdCrW/yHbMZVccA18/6vR9o/qFyOuAsKIWFdVG+z8U
3SFmTjFQQg/FgeN/TPCavmoXqdCDOqyRwXjG4K8Kr4B+TO/esaEDMELiyvBdP7EPuTNIwlmQfoIW
djuCGNRRuaxVeGegGbwtfySyy0GtYfAnc/lEOCbZGNMiGg7QzWcY/rGVpSPIWzybE6DvV4rLWtxd
Q7tMmO7xECjbScCB5ffIxC7sm7WRqV0UL4rq2ftg8ISu+y5oBDRK7KpyP7TspoHFKE062gxFBYU4
S4cQTLLsjv9iUL/+7wEf2hHZxH0TaFdDVceR/u+SEnSovZ34NjPLZKdwvv4FgM835rOqKNEjrAzF
jZ6/J0TUewlh5Ptmzsv4vZQhsdpdp0yJCtvijZfw3/wVsuz4Hp83qLZ0VMw7Iy64OEhIlcMH42f9
1HA/x1B7TJkHm50WKadjpoMLQz0nHPWbbYZ7Ew3ZQ2g1g7oVTtDzXJ0B+CuzhdH/hNBiLBYWj8mG
cnwh/odRMoWWQJ52m4yG856IpBTIZr37/sciNymnyUOEc1NDLKMNwUpfDWqsZ22T5A+A4FW3wrAA
dJcBGSj+A38ZU9OBmWVsru+uGJ4cj96LkrKlaTTxoUDzHby6KqFkVTDZioSWiK84qgpRjgmP7UeB
CTT+uBAK1E1ZlHdIXAr3iV8yBPs7T8nIt2gFNbwTZCRvCLtIRPvHwI2kgnhpxpZQbL4d6vlZh2jz
iQbsxfX+vrfvyX+BLxCdeB7lvXJVS7Z8l0CF7B8/FzSu2VSG67zHB0R3arRiheaQasTUdYRyfvlb
e6RKVVCO1APC1d6L6NPTKY9zR91LjQJG1plQhCvGADebLOSpyVwxrQhjnqjp+h2RZ0ZSO+5mJEBg
qCIBliHAURVZexjlly6Q//G35sv4/93gDanhAQDOnInkZh3IsvAcwldSotJgW0I1unyjULbtz+cx
UzVDzbbpUl+WeRf5ZZ3qdVZkJZE0SIqTcKIw4mv1bt+fKH61iGeDYw5fN6ZcVvuKbWLlFxeDOTHX
HA4N1dXH9Rbl8A0/UogUTTRYEphCpRXak48GdAM8bMWX1wdEYQMggyy7I0ID+SIwyYaLqatPtp+b
lt/NBmOxq6YMLM6lSayiUWY9fgEq7rQm/dcWJ3bZ4JBwEKQmRftikomM385y9X9stzC07FtRsYVZ
xMovXmu4lwSRSPHjwoytUjQGRo6JFey9agGiWfxluaOxM7b9kwMG/pHI1ttkqLh5X6WFmG67TAwh
8Lp7f9/Zp5f/oQRvukGDs20QFjS0TB8RL8DBd2gF+WA7zMspXOoWbZxw/QgJ3ALyOeh/zOWPj3id
5t2mLLh7Kq9CnsdbhgYJGDN6UJ13fDs6DNGf0u4CTr38Z0s5utlUU5wDT0zO0g64+buzBohRHvBh
L/cRYwaxBLlteuNSOd1frd1NwmvFIJFFJu7kd624xRjoF3LO28YeO0GpdDcdnSgoCvrUT7Y49lB+
hRV08KLgNspmWqW5a0RItSNSbAR/V0J/WMiL+ZO26Jz7srDNpBZX8vHTTusPbxG5xVrE4O+mDJ4f
ZfX8zUjC9ubdMGk6ABkId6XWNOtSU08TaUw8r0Wa9YVJIciOrRI5zHVlscdcERA6LwT8ImtskcN/
ziVPTl0p5o5ZQqCBnUjuWSyjPMg8HWPrIMRYjPtQb/HbryPTTop09H1l4n5gC7W+jmn2ecQMS9oM
8nn62wA/ujR2hk/1YftCZZJQetzDst4qtF/5dAQYsDquIe6YxEyfaUUzMqNH+/iIKgReyc8btGwJ
wfSvdfp0zN/DJJ2CXZjNLXWvk/M+76tJTIWcAysbG67Fpe6pCl8Mbeb9iwh0FD5rNcxlFvU8IvjL
IQ1e0icYEEWnyKvrFJ1YYj957dllN/B6amf7RzhrcoicsSekLAPrKtr56+CqrfeJEZC08GaxkQj9
GaKJKCKHxNwhejBb7UJNwvW5rBBrtNaJHZpwVWvSETjUQ8IXI6gFTs9ERZ+t8KHBrIKucNZEyP4m
g5lKTQAijpueIiQZ5QfqvhsIvBaIUtOCJw4OW3P1Da+IAZFfwDhvBgbLVVcLbfPuC2dseuWoPQ8F
+Bifvjtwe9ErUXVCXSzcoF/4fSR5DQZBjRiHLxb8bPd/G3d+q2xckndBzOIonglyrUQTvz9Y+0jN
Vm/Phf4wnHAQzSAawfE3nJXly6+DyiOr3Jt+Pg9U/Pto25iQvV775Bzgu3eQLXKsibxwGmbEi4RD
qPJ9GbySsC4UDiho3O8yRnShxzc3dDL+YR1JUeJAUL2M8YhWClI5K0Gs88vnaem6q1CdNeXrGRX6
zFidQbWamk0UOcifUH0KquZGPdYnT8Ld181Clh72XBvQmQS0uwPrWYLTYud1W66NEKE3tno7Mmv5
/8koVqU5X4v2tGg/dykcAbK2efMLhpAS4JLNO36oIMlmECEC5Gtu6ZUXTPYIuVRf1hsdR9IkzTmh
U3fvz2t5udzZ+jMzSz1WonDOEJ4qB5vTsY+5KTRNBMcNSArJ5+hk1zA6q2U2T+apkM3qg5RssYKW
YM/BGH6rrcLAGeZ6pm64bJCiH8ZSO7h/LnkwStPYaZdkyAhONSDoE/lMvRD1oxdq3Inw33gxRjEy
AQQmORuZnRS5eGDdFdTxCNphRtUwNXT0bJ3zXSB/Ljb+i1Sy0ZlmIQ74bT3YSbXn+KQ6hQT1+JA8
Z7zoKHqshPArkEOn5Ugw9LTdc3x6bbciXTgE0dWVTMi3Gj6VdAL/QLiZJqbhwF3I1WEwfR3uyt+C
TndEYfSoIu0rDNt7NWNGO6ytrXP/+Ku02cyLxgIwi2UcSi+wTKpBPI8WK0MGjl9OsDVsA411h8lg
zIkYvQHKF0s6K8hO0rDykrt7ewf8+SOQ72BCmCkPNux0au3rNslg5KAlf//BN22RY8PJs1ot6aNx
mS5GCyj6ZSL1gh2AsZ117r3urtFFJav5WmTmjOlMGihRkZD9enxYyxZeWi87PUAeCAHKzjjtO9Tp
rCn0Pen75sTUYFTuNpqCipq/Q6+j2aGEScAj/l0vTgs+kouFZtzXnVSX7+6RXJl2oYbW6E69MUJG
6kFkrEqLvsl7fCWOt7MO7OUmis7RMKxXdZIelaKInZ+mUEhluB4z3vr0OKaZuMMynTJyYSTD5PSs
dyN9cSoF2eOplOtaUoXwk39fe8z6GiGnohNw/y7G1rck1iW3MwHaa41C0AK0x78jy+mKlNOqEv4b
/n7AmcVZFG354xbJyx17JoTiOemEEfUgxkrEkCnsBlyZ1hp8cX65i9qZ2vXESHGZVdKdX8G7o0yc
zphSNVAk5c5a6x2JN+9Gav7WVtaZN1+sKqwGxOOhTxWzcOJa1Ewl4Fbenu2Q5HAoAMGrzsiBTnox
UZfaBVoNIFn6VT2Oy3acixrWwIz05p8iMh7HkdgQazKTKjaZqD0rUm98Ec3Hp1CO7gPvAoXiPbNO
ZkS6qExB2F8oXoCYTS9xoYqdvCKwTNk7quMO/eSkKXp6XQGHlhyAnQ476Uk+NWBnqF325EjJxRoA
FhJYV7CX8h17pTVXyfcP9/4ax6PFQy71BDQJXr9SvSS5cSlAJ2HnVBM8chk1Gs6HBM7XGdHyXtxm
GoTwOAc3ktsAoA40vGyZeT2Rw7JiQvx/iaBeBM/inSteTSAHIEn1NPr82g54yDE1ARA4e1SGO5zz
O3o/nEy5J0qtflSPbhrJ9DjJ8hYy4x6CWCtMoFz7g0tfa5d/1wwZ6FF54apAYKpsj8ccsh6Ormu2
Wh8kTBb1KOhCDy3shF9zI0x0Ba9RdQLbErHyRFlL01iyPIHGD1YiQYV5ZxUHiZTOr1Jspy48zS0T
kLJnRebWMzgPIz2x/0tnmiEHMV5kdtciBFEUUWZlop/h4ni/6xaItuV/GDBLbpRffX4djraGkJSh
LJ6zfLAjaCOl93L7B/uTdsspNkB1bn4na+6dMDvDtSdsDu4rNug7bpwefZOdh2WLRw4TveQW25MP
x0FGfojhDq7LaTmHzvELeSaJ705GRaCJ36tHokW+8xMavIvagFXSYuhpkmynMq/RYcv+6gxJOXiX
XFcAmWc4EMS7mkbNqOcoE7M+Wae+8gr277pLFni5aUg4Z1CVmqSeLCdI+OkcZCKzaSmd/0PvAnoN
m0TppF6WBPxWxa47HDjHFI1PjEkuXk7F0FHUDPk8cDIkbQoyZp/Y91g/ygqfO0GX1XxKBHlI2XLF
llCkcXJaQ2CiBRdrMV/fs34txMVb7+ySLcoopi0p0U7yOl3dRuL/9uLCUTHWY//QQuB8/Qc21f9d
c4mT7tt6cIQjXHT2oOQrDrRgNz314QOLKUsJIBFAUH4evoazaRda4pbzvPsqncbM4gq750C4BtF8
rRz3p7erJDp0Q3vAc6PpsPW6As48q2psUhhlVHlDwX5uFOUwYdOmawOgO6vknBNiOqaavwI2yX7v
CmfPuMsjXGNhx6moq8PRvEh5R7UMStYOHXJhaJFPgtvLSnTVgY9eBza29kECRvkl9TMfNaU2lKup
bYPRf5OWRIRrNBolfNfA7NExAaaW/cHd8nTFkg3cDsZcI9GZv2BcYKy2PRgLAlwBy498y1HldL31
x0X+sDBu6cngeeNl1DLuiclz8ffyztQiA7GnsDNkdxqsHAvbJIZpsnwcq5XW6Jvd5OGADbOfnAKi
RXcZgb71HeCyff2M8yRIwmXVvxTOJuQKGB26vDMS7XjkCR2ux/4+gt8vhJh707oVVZBaxnSRE9Zi
tlECUJSeDA6pQ5CcI4lqaHSsw7GYDfKBxjFEgrvjsRm87VSICWUa0/BQIqQ4mJL2HQ+95/K3U4Ra
3gF7WuRCl1vASwYvlQR/Rn6DZBB8kAVrrywj5cXt5pkPWfSHFw+NChWcUCeKqREfKt5TpqKWACER
Z0h+MU4pXo59q0Lmarwgd0geAzm7H6OuvbYUQ1p32lJHkJws3qdyYWnWDGDGsKtMmf3R+PKLPMfM
uxEfXmOFiwHKLnHH6JBYVCLW9xkiJ57vCSP1HdkMZkqJhKxkmCRTCtbLz7iPmRsfatDcXe4DcwgC
YDFYqalg8CJ18XUA2UeoeURrnnzWRw8S0iwey5razwUyAU6Uh8+YjDLfiErPRgKy78nqK0+ZgaOj
P+0oaCAUOjKyOxbV3Ekeo4Q92xv739Yp3kkq41nWLR8w6iw0I0IHsMVsNmNBIIrXq/fINGHcFUlI
MA90uzuLa1bIT/05fRpli1kaIVJlFD+nwHz3IrmA+5BCezP9wjZLC59ARnVYOd/7MVgwL3muFaJY
PJ/CzLOBtt3qq9BXumfAFs6+ZVrdBdb3Y6TU9sUiLwRpWcDyvxlyz80OQ5L5cbn3ivlTgoCL/lvV
ZQxmMxUq37DGjjfGs2XG/0GmqWQyTY589rXiX5/g38a/tVCQdp0lTK8AZzPEFyBgIPqiG+QU71wG
SufCs/pCFfy52JagePno6bq+Ihf9qi1e2VdVZCt1VRdEXyhwwoNOQf+u8widmFYkajAifZzUY76z
cDVIPWexb6sX3cLVC2KXw6NkVhfZw82bKtclMoOpVdBo4cGlO2x347UNyXTsJIokVeLZyaYo5wqK
iJpHDwwW7bJSUTBz65DgpZYTCiwYO8ZVgykyHZE7F7DHZXigS8lUYb1uTFal3c8PB5fTE4d6lrFz
LiUq5rctGfOEoAwfxezpg9NfOzu0uqGbCCyKGpMVuOkSkLEJ8iuqzOBuKC7uKzseE5j42nzyRAMQ
MsaMzMjWbfLq9ksVKyY8CVlD7LzlV/CuVzlptICvJPEWdrv7JB23awFgIKks7Ur9OBpR6i/Zn1YA
ohGoS/FP0flw4721stkaJOQAWxTQA8fQOJ8x/Fi/LJWSeOcA+VRNGkY/SEJ/kb9AthZ4g+K9WX0W
23KiKQ31quoYqaTqnElMhRRMRxRITfnK4WG+q++SPNBed7wsoLTI8jhtHeoHi0DxYl2Kyrc8lkzR
s0y0Astf/MprjeoNg3c6GF+s7UMMhOkUIdL+gfdAccqetNkPSki21lNyeTpB96npHE9sPB+SxpZ+
AfMdgf3x1LPc/UT9OiLwcNAWW4PHedrRXN7J1CPQ+vmMO8H3eYPgyI4ZWOeXhIAdTmMElg2gD+TB
7f/B32ug84/HPCdGeorLnCpBzO5pmygX9Lu4b6Qip+nKdUqEY8l6Zwi6nAn2ZQYw8Rid82vdE+TI
vO55s69UH+3EuET+7aej6qXuYV52oQlQiLzWox9AKlFjiYFCcwBr4gPXbpYrLfKsOU+T087wycch
5PhgnrByoyq96ukqMBt9r9ae4+yoXL8Wfj6RsnDBPuqiJcuhIjP172T1ujsH4/4Z01McSy/WpIFt
L5/+QkZxiu1Zwis4FNpjuOLMABoCRdwPgsduMT5fDzxZPqMzGkZjlPdE5PSL/r3Qn2/GinQ8fcD7
6Ai14nAPKQwCfH3CFqAADIhipCWhV8X5CxljekLaAU2bk17yh60dF3BUrOtWRRVte3m6PU+FLvuc
L0b0Zi3dXlkF+ewh1Qn3uH1T3Xr/cPyf4pW/LCdvbteyLNyqByrydjmqY7FnuCPy5/EFMkp4USfR
lwr2q14+2mkUCpsHAVKAWiG2vwwnucQl9n0L96SfURc486CwYMQmEZDmy3RWk8EiPU5PDkzIT5j7
ta3YEQRwYFEx+8PcVVNHGnRcdr1N7W/jSkonW4dCmUMgZPDHXdtKsGgNbYdBepGKr0vlilgLzwhe
iC3/o3ZeA6rq4BwSBlWro00YqvV4nP+QVkvkilBxHmiO9tvX7qssWvf1daLCHKp2GSiyDacWlDiG
loSm9oYA8nfb01Bx73iIK+Wbbif0Jlr34EbB+xFnbtHHR/EPYkwxL/GJKirFbnN1lrs0ByhxMKR9
WCyx6y7fdcr4XysN7HQBhjGeGkKOCaVj/+idadUAx/dr0oIKsWlK3o2HxWDT5iQQGUbG/JKXRFKf
N07JsRz46CVzigbCIw+VyTjxcHMEhYd179p9uP/VcHPJHKmjJ5pZZtFg3SB2l+DRGQlvjQkY+SgH
N/EklLHg2lDbYlK8e49qqDX+u4y6kpicfrnV3SbiVQhIQlS9zMI5VvqkXXIuLdEM0btSGpGZ7USj
ul8qhVTGnlhcc7+Qykfuf+DVoxTzGMbOiIyA1KiVABJFR4HaRojhkkMXhgTizzPQGc8DyEhKq6j/
pHsiAvwVM2mTSeQ3rRsiy0F+nhXjnOCg5d4To0bAuDiyY088kEQxg7Maq2+O7A82xaaRPfaQYfI6
wFHKf43VTT+TkEG54iivQomXnyL+nG71PhqFJwfGF+FsiXRSfy9HjjZy1K+j4Ob3EO16+lk0xikV
QvJoggV6RmbPi4LohhrBk8x87HoL2wkFyogNt9Jmt/DS5JvvN1VP4zzutX/DpiS27wHSanClp2i4
ij6JQ7DkABOBfEmAJ8g3KynzDLn4RF/DRS5WOUzXr45qpkLNQMA2P/XkjOt2jBfCMGGJAfuB6pFo
aO/AUmm0gwEy/lFfPogBHjmELPlc/KhzxkrgO0dWHLsaEz743Omxpy6WUJcRdYScpjOng1TyhOV0
2Rn28ZqsqDuPD7p7Au478cVO2f7jrNfu44dthY/+4MY9Rsbp2wtw9rMtF4Z9Ke5eOn/27DrZU9N7
6ct/Ev+ewX1XLPTaDezD/h5RbwyeQZLHKE8M9vJBu+U8iZSGqbmPsnP8MFYzgtMfmZhCAODxcggn
26SRTyxDjj15vd0uTsHTCPkW0Dr7ucEKLDQ0YXRNOd6UoyYIkkNnaPG48ZzqnB8U5GaUt1ehE545
L+to/fJKj3jdvswtGk+vx41/MfBtGgSY3yp2mi2Nqp/r7NXu1kZEog2M7hIr1P91Sa9wmVUWldlB
9PdPqzObYvqUSaLt8Pvcg2f8Sl8MTbFx1o6wvTADbDbNWsaKr9Htg5m8OjXr1XLwqQ8geY++WsfD
ibW3E6IRAYQWimYw+4B8Cqp5Ga1S91prnhN8EtC0rq8AFCBexh6sUKKO+mcx2iu6/vB9Bg5cLv54
/7Nhio4YUNanl/GwFagVbXyijHMLeWaUYfNRMhDkquJ/x9y8acGKPBMSSyaNnOGwtA3anLbeX6dW
obMbeRUvi4QY7z09EBwKkQqCSJGTeQ6z9dP59trxDtYi24X0OTlwUx6bu/xUhF+dCqKbAxU0hSJu
jfC2dCLLhui5XDEFi6QhhvIcBdZqzdO1kc8lxfN0hO2BBbocK2AP7fGwKqQpQvQHP7rj04lXFQOg
YAqSqhpc1/yB5ah96it/8ySQW1kuhs7+DVG/bgbqMAiA25DMdvCZ1aKZYlPW9P8B9pCL1iwZeCgf
SeYzKvRjtRkrboVxCLs+VYSXQusclyC0++8uzRAjSTKgfmiwNohY1XB+x9Z6p9lL4ksvF93whjAY
pj33FG0YXwMCtlBHCl9JALp42wbJKD4gFtrtW+OQCEjDMdeBpkbsts+65HtO3rpYcP7zpkjDFum/
r24ewPE7DZYiAMrjMlHlGQcr/Np6p93mpGjfdgyJaOJnWfgcUaYSqqNor20ZP/NqDt64LRQ3phVa
pLmYW7l8ymC5H+i/JDZqfQAlJ4qKVitPfDZSlufWnygopECNEXebA8p/cwx+zbEYXb3PMVWzW40c
ZSh02hBVN6ozEjhtHxgyNklOofbBgUnmOisAJ6N18hnU500JZ3/U8dbPBXpa10673nMnlQzk04/V
qRjV6zQa87JXMdy2iccN/h8AdO3vg/6iTiiAF6mRMNxCfZb0a5uL6TUQ42KTKcqDLkAp/MpwVlMe
sXzodOny8P73KZonXu1vZs6n8ylg7SrWznvMQvChZTemOqpCk4ItbWmoLClMr+d6Xp4u8YYTd4j3
X2vtozRYCwbvxQkm/e6YTWdWGU1nHoT7moTgqqG7b/HTKZD7GHsUVQDjV/8GeLVPeAm4tS9EpzCM
69tHCmCnNTUcctxSyF2zp8e5t/+KBWVFQ9b66zVkc+Vvr5b7bKeB5s193Xxen3oqrimlQJ4up+xQ
3Fb1WCygjFDchxeIN2GzIbedq6JvwKYN/zS3tAhIMwOYnpVdj+PqUeCwZCU/AwKo+TWE3OcEcecv
i/lHvqiHfeRH1ovoIfZQtg6mxTiCpFtJLtQSWWq9lH0MRiuHgnzNXVWEvfUHcnI8407G086NiQ5d
7OOBW+EfM0kmd6X6lLu0CS/mukRMCg8OtceRCHcHx1ryXhLmzBmu6DbYQkfHKf84EKJH+uVFW9ec
BP0fECN7hOuD0n1U9VtlxJew6Q45hQ8nE0JlAGd+BhpxsTjN01sGEycKuFjmyX0k/5vYmRNJ9n6j
TBexuyplFq99k0wI0Fmd+/QUJK1xWigaGrQ6Ttr22Shkl/v0mwVu7JB2hu7MH7+ADW4mhZWgLSO+
zoLsBBgTtQFzj9VWrzSvPuL+EbrG/r+IatHee739/uT/3je2wnFnUj4Ht+VnPC/uw/2g34RqMQfu
1vtWB0orWC9FseJlgcI0Utpr022PPec4Wg+y+YaijjusrGIl1LN059raJaVE4iKG979TLZfOatNV
8Qq4MNbrFIdZyZs7nBVP1RctiX0KkCT1PjqLg9lRglXyJoXmMNcXloBcbRX3p5GtmADZ4wEWKXyO
n9VCHGNXWgjsFUobSXFxnVX8ODsgaraQlkXKY6qGTTEJKW6QAKlXhcXxD9kAvEzRUUYyX89RbMZe
NzJNf3Mrxh03B7NkSN3fVfu/FKaUCI1ARz8USe3h/r/QK7cIhMjdcSVX697jjE1oMlSElUH4FFq2
LPJFu+dPIb5w54xODtIs2W7yzUP6yOPtESUoVtbdTRH91R/hyDa7cq2haCAM7H14fnsEKododvZ5
4gKhqtbwoUrg6Qmyzg4XVh4bLZ7c1Kqw4mcBqgPDjhTdAQMgSoYULqFB2Fqm9vuEp+B7P2wsBr0b
JFEIbr2swaN5yVGKxzCYPiOvyhVlCOvWzwOMq17wgKaHL0olHQRriIqntgTUWTK2ThvM+RmSjsZ9
ObkH1gvGl5/cQo0JVpHIfFNusxQW994mIe8zPUgO2fqvoCcJQpIpEJbqzTpiU66teWYyCkTF+RZf
NIZPtCbJXHrXTPFcucD4KCL9thGllDIjekNZ79Y2PhVkqPyrHz3opmkqqZT3N+kr4Ptz97p9ck4K
1oRYiN0tRJxekLuZkNX+5kz7d8jwaER4PLqxyA3vJIg403tKq3tvYBF8smqygNl9bJKQ5x+qovdc
AT0dmCZIMYQ2hQE1v085aRhjoX5+GHouoNEcIn8//t8OpV8GBKWKyc/jEPuHV4aJ/xPdrFkB1dGg
d5rzQh8Dl27emjGdRO8GcqarE4IjDZJ8Ih9yaFwM3wv/gsWrqvyJZzoGvveApFVswQgHIXHFK3qb
O99Xz/5+PTbj5mFnW21IVScAoHU7vN1ggm1IQSSSDaHPwsPZnYUq66ARHuUbXIGApFTuypmfKyTM
k9gUhKSKofGFX1LUSSHavdKQT9qLKJgipHD+K4goIe8VcMySfUS9bG8KayR6Ep4izYq+whswjNzG
haSoLQzz+8eEFJ/E+kimPxxnvosB1wqpbRJeOhpAQT45/Or3VRbp2OoVfVmUc5Jrm6jhPYdFtQZy
ZegKEPWGf4NWulgHlfCItvFJeim7T12AEm1+0CdOfH9xrK/dVxSSaiUFi5Xt7rDhbTIaMPOq1YQi
CK+OzNOiepZBbuD5JBYNhEsKsB2IriQnAyAJNVlzsBvZT1zhElpgjbi8pKslYFcwTX9OVGLhOvgf
HyfqxMssED9iSngBio7xyM2AKP+VqIjSCbcCcvQ0QY4DFWT55BqhXA+A0dAkmWdQA4fqwp2yPAvK
jSO6wCvEjkeu4j/yGjfyhw88drQS+Zk6Ey1D36KhCO6HqIzhrVHtvinAUpj6aBGCPdzoxstWL6hp
7+fFoVf1VtIcBesdlJGL1XUcIOQlYfPDKxSEza9b3WPefPJwhRszyGMvETaG4kZpEYQN98QWcsG8
YIyDJkx26LQ8q2mEINZwu5hBbsV1wtijS1umVrPmomK1XMudwwH63o1ASIogOZJ2GKKV31BealIJ
WUZMXquyBjjcSjDdc7oupF3DYq7N8TO0foz26AEWgKCMUeDoPYibA53agzSg4Hd3rvMLv8EsueJK
k9PN5XfAbZEHZIs5fKloXw6OVzrb1p0MsWFtF8WxL5Itu6yjh2JjmrbMJ2iGT2N9goRY1eEfxxBQ
Ve+nHTn6EZwPu+B/YBBMo1N6s+Fj7DdC3Kf1gZopq2JjwebfkkQly356KnndjVUOvsn7nxjeiEgZ
PZvPmDDPUbo+tZsk7mZjCcmRNLyo4/6F+sUKuARcpmPrzEvAP579cpu2noHB6ItcS1jrPv/sCSol
a8rfZonPz15WiqDugeVJaYAUwA9NGoonFFkZ60XYGyq+P2WZftatr06rwJj7Lm6Aq3W8qi93EDe9
y7UESM3fUpX4PFi4tHzpk4kPU9uJ41jT+5IgGsOXPamHAxd23jBB01qbAC+r2nIo9Q1ZmXE/gNc5
bbglyYcQCzj7zy/j8oUqOarLoN/y2E9/S4GYxYQJAY8KH4FWQhAvChiJ2wYEbnQ8wk1v3wg/Dudw
4VZEEZ/5KBgqJTzafO/Io291wpsXJccH3GrsNnxD5NJwdGe3De7VqKiFTGwaMYGUmEg9rYx6vJko
OCs1pAK5qg4oI1FfADodcCmMnTHIOi/khDPBOT+jjUayl18JfCxUipLpA7qgJXI1NEMMK0xcIaRO
/GSkWcYQ1ZVf1QqPfS7RxhOzbTwlXmDQTfd4AF52gh/SKQ4PpjSkYCqFcRgJJ37/7ltHj8eGawKw
sfLAUUwMAnuX2c0BkQOh3sTEjiS6NyMSHX0focN/tzSjQzPHTVEjRXkby8MqjZnuoaJvSRWc/ws6
zZ7RcApQ6JCChq9Ul39SdEOnEWc1SLW2Q5JWd4hzqRRiIVp1xbNBPxPQEVdEshkoXdT8q5CpbTXR
v0sfdTRS5uKSLJxJZMovKKwtiQkg8olA5Y0nOegST6CfRzISQ/bV1K8TB0V+iHQ7lvYIGL57dm+U
lCXBm3w7C+r531i2kKUtyr53ti8R5HCXiFCEjLlcafRlq/XBdIhT/+UX9owpWYF2vbu+dZhzRrmX
yEtzM+yk6eDGAcjPPvUV5fO/92dOA4UIVPmyPG2qAXs7QWCcoGq8YXNsjygoKp8gY0DxzeT03sEY
czlH31Vm5evT59omufPOBYPkvZ7P8Sq8eur7k5t2UwjwuukAhEDsjdyz9yeTIo4MWL3azh0JnkAR
hxUwSwRUJSSVCky13UM74m09ftDGzgneN+y1hXX3r80bKFxQSNLD5G4bFMj880lyk85E+okdSZkl
K9J4jFcVelidcfVHy9kfa1Ah26CLCwwHixESv9/WxBcPNINHmo//w/sghZy+sb73SxP7WmfdG+LY
VAuaM9YLCdiICBZhGT3HoPCHD4W8+tyh3mvU3ecC8EOmK3TvWxfhnTOLYky3uvpo9v1hhASlG+Lm
tSiCGtffv3nsjZqhu8OBdWbeUWc5aZ2vdrd2w3CaeY9ikFBVTTXiAxPIe42+7TVzNP8CzaAqha8l
sUoi9HulUciX/mrvqANmzNf8dnha1NYrlOykXRdqb6WdiAJMlB8jEyORWhlNtpqkMDtjPWO8yDeR
yNUVVXuDDplYteEPDD35wabJxSo2DUNHqTp0nHTRN6JGwmlBdCdeX/ICQv3uXkDZIG93Gu2cbdaZ
TJpYacbdUvMAZ86PfzIXCrpq23NsjR0GPqNsYh+gZSs0JlMX+b/tUWZVfdzKWM5xffbBb/tjKWRC
gUsIgvm/9h9dKUjafQ8ti1jw34zXy6OgRlqIRBBM2sr2/rloMSk6IyDyLNU2yEXOatxEpiY2jW/N
+2zQP74igwQ83qmlyW4wk/KV0BAmx3UPzzdiMAepeYRByPb1aWVxHq19jpTgpa8K/cSIiOzSqI91
mvTSF8RU7p0FreV/8cBi53bZ/lt6WfARX/+7cv24vtdZoEhKFj+2cpybf4kdr3/2NLHMu7PhRWg1
P+ZKVsZWTiasDdU9jQV1APbhPzPBFafaYztw0SCQDkOFZX5my/d7PHDJ9d5S+yj5GMfvz6eb+8l5
1B1PqxlIYTbgwoucft7ghXjSVGyyjFQXwBMBlJ7sziuOGiZL/hEpvy8Wxoym2RuJq+M2QVS8J9es
VxYXc8Eh2SR/cxUnmihhNkaTNSCPmi9cD7I/EriXBjTCwusWZ51R/xjmJxOUvvfrZJ+RH4iPCOru
gUaqZPlog+xRSIQ+uJbRfCzjL/8pSuXq3Kxf3UMTsudn2kZcmgkVMKShiiqi3lncZ64OL0e6M68R
HVyYWRG7RIy+jcwfo/H3WjSWbiqqUKZBDgn7R8hjH0iAyf9UZMtfcJAMX9XShqDheNnBuOQqG2DY
9eh5L4Jf0NMFGeLQdsc3qGYCOrjz167DMqVocr5XtG9av0zx4vo3hhtczwuqDzhqbZ6o+SB0Y0Yb
VZMoiQhepW8AWH7g3zb1tyvBGp/HoNGCeF6Ca67btwYVtD7segeXBaGsAj1mY6BdsVNFiRwbcF3B
avlVZz6iyMZeQPKs2lYqzgqkl8WAsmA+wIulleCSA5OhynmuM0f9OwqdQ952b+ZTxJWxvFIEyn6o
dY5BU/uiktSx0vTKSNflBNoEZtc/3BgxV3aTxCwa8gOmhPv93ZvebiWYhjtjLCQLeLsMHFTebjVH
J0ofsTO1Wjtrp7XypfQ3m8qzeSavW+0tXx78ngkpd6D2ctlh1MmYp/ot8vCBQocKDs1Vn3A4ubTc
iyDR0KVQ2nX1PAoGqZOEi0SyQJWppOIx5zZPhW/2F4gcd01poD+05ydIoSgUu4Ba+53dSgtH6G2o
AsIGlgp5XdJF90hGUM4oChCn2rnpx1RwWs9LKFJ7TqzbSY0mLSs+lPDGFV5zuzLvA+pXZrizq2VJ
lf9NDJS+O8N1yBPxH7lvCZHbkMK05XlhQMY4VHfuWdXm1cFmDoZZX/TmDwRRa+FodJaMvqm2dLaR
Wy4dhDtDhSBiFP3FzbvFuKZcFNmmOJR7R0kNMCEQAcf4qg7IP81maI0Yb3N3QHraYmYlr1GS0VBs
L/skfcyHVRo+sO7LLLpv8ZykZvnS5JOGDMNfRPmcC5jq+EcK6yvQtUgnvwh/JeuAX8Vh2O8f+E9b
SzU86/lGo8034lRPRLsmbVCMbKhvgssBoNn6/2/rFFj9Az/HB/FX/qzjq2Rg8HiaWZnHv3+k1Exc
hjg2uLOUJ4I2wTTyLiwz3b9o/4S3RN/100DS/bz54lpSYjYxtvy57/8tDzFXKEnC3yr5aMiL8tnO
VfF2jQ21yOZ2QxMXa6cuOQhtHUzSvpQBR9GtlI/H5nag3lVTrIjlvuyBb+LKZM9+Cq/awqhbJ+yU
+MK2YHFVx0qHWsuhCSzcStC1lQjZDqbRngY0D7uUsQ/Vz78tvChG3tbIvjfFwijHBqVHjhI/RQsq
wuKEL/VEnrKiKXIXvpm6zz6zCMeE83wnqjRB4up6Yd+TqWFM2G6ixHt7ACPaB1dSnyXPiM5jmuEi
X4fxSU7CcbOXJBslneNKX804XNwlRKOGswHSXwDbO5hk2NqlWNu9jNDA542V/Q319jHQyQxW0nyM
zVOMR6sM2FRS5l0PeDZZQJ8lBbQVDxyk8gkc15mudDClCK134qh2Gpe7Rhduog3ol6UTNx3mL+xJ
qgw6Kv2V7EV/HOBsbbGt8yxtUAD+pIbym1Kv/wBMz1ELo4zS9zyddz5oVUfOZxBTzazJv5+KDVvX
rlSPH2t0K33YNA4LNfp2YuekYAzfPrU5KhzuraEkVQFZzf+Npq+A5qrdZX5P3rxbLLJFXu4dBJ20
ycmJT7bm3qfzBFBZCi4/J3QdZAI9TQFp9pk13lhQcEcU/55SxYs1vobIaQADJTkd65P8Azd4xeoM
erBqi9f2tuDnaz5ZeJ0fuqBSXrS7IdtBDr/72zkP461p15TEJB6XVUKafSVpmlgFfovfjjhnY9NS
9YYq6yKD1vdHt2YWm+ezpLN138vVs8LmMaGYnqPfe9w0iNL/PKbp2QC0JTb8S7B1M+qwnTUNF7LE
nD7mB5XENiySTkXr7bQT6HbxQL4YNmL+QjDlflhi1EWUEYukZcc3bTOj3P0qmltkzUr8LJdl9lZP
xqGZfGQwQ4jLJL4IGNz0r9xt18qrNEpX/A7/a8AuhphCiVG1fwsKMGYlhmTbp+If+4c4sId61OPJ
VBzXEjN6i7LnCRw3bUmF1yHdVxiE9+uwuvuYFCDy7vL1lS+u+zbqB6SwF/G9j0JJ8nz1/oRsZqGz
LCPBeq25ItQc+rg/bWu7IGEup2l2JWj+64cIryghcZYc2TmQQAR2x1EZW8xQtS1o+UEOLGhfdvQk
yIUP2f4kK9tgS/L56/XB5C6HmWvutw148KOonNYyCe+OtaXaurjzrwY2Si7FDpOR+Qw9J8aFfTsg
HakpcVeTA38+0S4Jvul/Of8z4vF2ym1jtrFSDfLdYMmIojsoPrsf4WgDcFWugVf9tErsz67OTlgo
zEZ6Id+Bfy6nSp00Qc/DGW9H3Q/anw4RDZweGiMXFS68XMxRe+ekDA8rgxjY4NrG6xAPoYfCzjJe
lzg6S9KVjBv0T+Bx36RmCPdWHH02HP27xNOQF70b+zQdpKeMC5tHLM8YH5QX/CBZpJddjJJRPkmh
zrrIP2q48/sgbSfW9/S1P0yTg1j5IClmBjWzXBN7atcxH1LzkhE9YhuWD4Jy7vvJlqcu50ELMrFT
10ZHeD5rhe23Z6fP93Cje/B5hgp4DxuvjdCuhBNMrJD8E5/81bxtv3DqOhgjD3M7mplJyAVbmXjw
+iC+xVwSGTwQn1BXQsJn9f/V//rJwgNxllLg9/llR1Xsno5j6bIsGHplJYgljHFY4PjfbayWN4Q7
nJMIexTEQEN7F6Kx4kMUVMAVY+jSjXroL8jOwX2i7/UbZeLwOgvcBCYB2jRIiVxrwAWUuJMTKHSN
86jBxJaunJNfcJlJp3p+eqhgUURfrgyTw1DIpAhtMmdXSm/4w/HI1/MtR56wxdK47G02Ns6UKvEC
DV/6bFpw1vCF1apB+hvl0RudvwmjB3m2GY0Y+lfCHfPN5CesXujgEVpv2aW5i18UQ0RSJxgS57Y4
adordBrfP+Zk297dG2ju73I3lafPcwYEOFLHOLLfYoqovet41wluPdO5O+lgYGVyWBoJzdZNGvP+
pwiK+fotMrJNutDf8lHV66ZVkm/PafmbMHl/UGDMjGtxHgBb/KAQCOMyNQEKBY57I1Z3Y2T1ilWZ
nJhOTWhr46x6JnHzxw94+iC64dyf1eJ6tiFglMqoy2922hB1Y1S1c2QEdD1nZnB5T/fYDf5pWWJn
UzAD/2wEEiZEWUOSqcHrNthInHgsVyHhYn3sUnQcgobJ5OVriSyzhV6B/5xzDCgT9FH1QugC3Y1m
mMbJqIvgq5qMbT8J67h29D5rOVplOVtJtvFjBbf9v3y/PdgUSkUFuX4r8A0jUU41KUOpyFf979Zi
1lAVO1ovtG8aUp0zAjpjNi8F8XEJPnNETyK+OZfkJiF5w2uipEa4miFkQeU27B91hq/BQGmy+KYk
sjMXRCYcLuZ/DVd4TtO0t7PP3BkyV1qLiw0xBpvjx3TBk1KAA/+q6maMzLHkbiUcOME1HC1Sf5oO
dvou3TU2X2DJeumPNLk2z5NRvsbc092WPWtQT7y+tVuS/+6h0TF6KzjY54LkriUo2OxAVqQVt9wM
Pl4L4Cy8qH66++kBhjLtzXV2sVAt/958WJhA0j1VYz6yjvTBN4MnaTntaObfm8gMf36dNo5m5Wwg
G34iwEAraIT3SLK1swlT9VCKnpx/CQwRTDJlgO0P9UmD+t19ahvxXtjI1TepgUnGxWdz8jXxNMlW
tB8Sf1PYbcGKkEQK8xwHvDagMdN2sDsoyEh5YyqXvlKk4GVoIthhAP6MB6UJMWOXpZnmlDkf1eie
c38uEx1mXszqW/+CdgYiIh2TLerKtopXP1MrPht4CwGII5AtHH0vZAdIPnqMfmjPypBMwsJdrioy
mchOzJGc7t5mrOg2c/Q5wKAAdrnCZ4FSm73JfGvmc1T8fddKVPVA5onTT06/k4WIMNhKBDVCLyJ1
0/2HV4xAX/dd+ZWePIlPFskn+gFedl8BZOGAqPvpNUeQo3xDPmUPVd+CqwXE3VIfL6QOqJJp/ebR
VxZdaBgIWLDrUkpBpDAQzzLijTfJrI1e1i2llEEglT8ITMEhBSj+iyB/y3HkwBtOLm3YXh+mSMMZ
eLLFP/ZvRvFSSgYSNauHPdX5Y/Cc4xBhyqvK1ZX5VEuWbAC1DW2bPFzdiD47v8UgHHXKYUEUWahy
xInGf7MRPAyyH5IayYuNJCPuyEUhmo+UcU8rfZh9GG2rR/RGqdFd7JFAkhpOa7j8Q8gvnbTTfOMH
Q019BScnK19f/jkXx03a+qjdEiZByPVkLGnBqi1ye6v8INPe4Nw/lQ/tA3L0sunDTOCSRrfzVYsj
yJPElJD++bnPYd1h0/ohg0io837BaJLbtJ2iOeE8pMdS4UMiBJwYwhmv24scJ6B0hONqrQVSedBl
9B51aRapJpHKDa5heAG4zSRDfImjlX2AsAIdcL60UM61o7BL/OMe3ZIsSCZB/xtWrL5B1gSOYk8A
0LK1dM1dXaewTjVDuBIawCzmRCFefYSRn24hb0NNu1fqtkTxeDjG+4SjbI+YIDihDJWBBMKV+qcz
ETY9G+6pAti956Ip5DKIVO7Ix66+CQ47x63fvK+8RG0ksr5vb2tQJMBja4/z61GT4N5rN4PGxPH1
rCn4k4O+iwII4uZG1+Z5lcXN51VdIJ/EJDOS+RS6j9Ps4h6V/PlLAJO/VwWhnl+rrzJ+vbP5D8Ng
n2K2p0oOO0a8byy9y9p5ZptyBD8bAKr0GKh2k8PpM1h7ad6hK83NCsToEW1QO0Roh5SvqrpUT7Zl
TjKMnNEubY/6qtS+NELhkGmJglMEbeueTcqontTBqor2JoZgBQbpzXRzvKxE162cYBGsBKfV83em
cqwydTzgbFgrzLc5mCaIMp7U3/6FzpzIgcVK0Te3+6bE1gbOEnTT8v3ivcqkGsp/TlEkfaX0JvNt
QR0iDUuvtDVejBpTId4ipzyQerxKY+go1hCMkYZp11XEywFQdEOeLkVT6OMm4MYHBoCy+7srpTcY
NqTvaxJqFownYQrEYYV/UsSAXkxr8r9W9BuIyFzVrwUjrddyw9b/gWa553cPyvH7wu1n5fPJR5pM
x+ttUwZtrQ7Gu1Hk9uL3El9Ok3j/jNmMoBKiMeKNPnJ0yIRQ2PuqJ4gbTxaNmhelG6FGxgoxUgcR
92/9T6b3C2YkKGqOY9v0M6C01L4zaGe0ECUngfzYVBMycmDn42qfc/rNzraLLTM8PFk/7BMsbzLb
4QMahz2Hju06q8EYysFehmb/id7FnCTJeDrIJ+K/y+yK6PEWJh5lBW+W/lUy9fQ0f5sZkfJ8EX7N
hzh+fopAIIcxAEZ0AIDv0ojAI01ATxiQOApVlH5Se6kDDjocARoegs71CSWMAWjf6pd3NOKEjZbv
/SGZoOrra4c5sZjjp55FnHvuowM+PGCtZwlrindBWKKd9fpiYsDwvcSywDvAxRNNxZw27GSdXz88
Gp+Ixuyp6rPzJWaS4mOybNq8D3ifMI1mwml4khhUyAGcEaeaadOfsAwds/6XgvUi7IwLQn/AO94x
+bSH/JhwzKQunuL7h+ZkXK4o51xBqQRk33VrgRB53zEQlQlYMJbh1RQ2EJKS/DBo/4vzS6RwdcKV
QbAkZbUzVf1oHcE1XwnpxBp99ZtdUR9Ly3TIPluLNzpklwxBWIs3FYITcysqUf1/rk8XAdKVGtFT
/n5k0WKy4GhnTpusCfhnI9n+7NLoLbicl8NmVlWxLp8c6vIlEvyj2rVUBY1wBwovimYJJ3ROVD8X
UBexsisKn/MAHLTBhd8MwIHpM/NRpJbh1f0Q+MyiBylJ3cPTh7qsUjtJhh2siDz9Q3oMrEzZ0B6d
1YIScwpfM1cOYNbKFsqjRKDb9lRd8pQEytji000NSAbDqktcss8Q5O47opCtT3SjjsGlBneaIZ3f
X4cVfs+EQzfFfD83utvYUpIILpc0WsEqq7TD/67SplphECaI3e8Ea1pIw333yiVOW6qTN49SX0+v
gJMMVzHNnyy53YQXU3TB81uo6LacXQdN1PeY6/6IIv0T2k/SDNNIP6duV64s2O90Qc/27gnKV6CG
qUQvv3xUxY92kEVJWsK39zGqWn7jfOVMZRbiZI4lHhBx9oqGJ0OF+ytBPaVT9gGRbO4FO1NYruX0
wCfcVSoZDUZxz9fAIB3FhCKShl9Z0syp9Pmdubg3lLYMYKvQZsq8/M4jbePoXHvTFU/ad/+QWjkG
fYp2M3JbfOyluC1IpakOUkayYgqDONWUgobTAeIe6MuM0U/HseaElTueHwo/nP2O5o9dNyFIFoec
ixiCiFXTB3zLPVC1A7TzuNlWapMoXJjj3aarh8L7k28N5yr+BR1GDGi4NkJUndnWqYq5YmXHfZ8G
0E5ofCozNbRMInFn5/R5mIgZr8dICBgYmKYsApLQhAkvEISkNcoNbYcY7s+VVUaW1l801i4wDe3V
RsDC0Tz9/ZESai/LCRGHZB/XCIc03Ur3xbL6qbAp7mwOyUrVqEz6vS+E9tcuug+wbtYJA+dibJBK
dIstRhBxq6YANQippjT01b1piw4c6qTT6HwLpGhqHAbyNlgaHLax8EARiFSltY0F2SMqlaJ+8xzk
90lK6HH6O7ilPWYCQ09vKyDTj+s6fMbX0AkyY/oMQNCWyjkE7307RMCgdszCPoSzrAbQ4VyxZT9E
GkmMzcJEOTWcyYERCEPx/j9TIXI1tpgUhSnELWDsfBhH7xfywcIwEw0a3mTFV82hjqjg1vw3n8uf
0DB15sHXi/nes7s6xIJAxfV3skKrMaG44pJTsGNrwk2oPt3IffX/o5DIXNqF7fwSaePEL35oPfmY
7nHCD/GrqgScnveYgCMc9M3YjIEZ76akLD/DkHKi920OQUNJt2NGL+kW/N7z2N/6ttzowAq6vnB6
bb7N0HFb/pSSwGxGlpmoPu7G5boQI+mkvTRFm74Okc8r6Ad5rlKxKmMroSQYFy2vDvCY0izGN/Xn
4JnyLGJdRmjdk9Ev+bK/L7B2OGp1d2ozKhWbONHy0NCLjimUsQNH47322HXWO0vW5pX3P41Fz3cM
VdNVCvSdkfqAeo7I5GvjxTl+0pRK+1QgpOTgPUeL+H6aFoceIDPSyNiwyoTJjq/5QTse5YnKBN0o
t35OJQxvr7CWcQnau7J8SstZA5I91cAKzcZ0LOgbo15feqLAJR8gDHGm+MHABmJMscIeLGn1blUM
WF+CNOh/gfsrLIbcU0aP/W34+HzlssJ4QaMkeks5ejUZVuedJ+ic4RBG0yiJsYPM7+RZJEcFJ3jd
YvfCBu/lRPqYbRWtdhhO4KwFY22kE0vf2N00HDo2xSxkg81ctzfIXStFaDioOW6f/iKOwWhThWa1
bQ+fwtmZPlyA6cV+01MJbX1PkZKEO2+onEU9Wbm/ENntlFvOG1H0PF/8F3krmd+R72dQ4D3EWX1v
Jqw6iGzxDt2ap9BYX79JMQlNoRs5ffmtjCYD1Bky92rOYKO8WPzRlgjYr+oLaUEKVUqBWz5GdXfO
XPOv2NUGePmov1got9Xruecu6+we9G3X1mGTo+M3qNZyFv1kWrHvrtzf7AqVC75Q9xPxzMJEmd3L
pqgSSPZJ2Mi7kcEtpQNCIJHsstZBdKIioGCvv+vHK5wUUJiM2iYyh4s4Z5siD6tHgiYoLskIzeC/
mazqfP4T0Q6OKmD7B2n1qW4X0mLtfmS3x++jIMVAZlw4v/sl7JBXZjjJQEKS0ZfNRjuRWk0Hy+KR
U4VQi2Re/z95xwfBVhyQFdAqWV+5p3JNp8Qm9tFzkTrQTQ6glCa/SzMJTYSzmQldzPIqhxEP2np3
CiBD76wHRoR5zwrnJLJWNVwyIZkT8taGBcsAv1Gydw1nHeTlBBb02qXTPGq4MSU2S+94mvh2TIiJ
VtC1YNY7dhHhB+PPIkdV/vkhAjktShisEVzOBHPlFsly9fNIHbggE1wKzZpN8jpPz2ArC50MmVVQ
m+ZI2g6R9ZzKTGm+cNEC/4RfrFE0zlrN7tJXs+TzYPXhtobZUEP8fcIM4ioBN68qgogVkc5/WW01
xCua9Ob6vL89NIMfSO+MPpeEqWH49gqr7qNGPK1Vxly0DeCSH7mMBUsuIJnUxCpwX+Z3m5uqBoPn
ukGAVa+lLKYcqL7J+to+WLqj4Q5QwABa/zFMsU3JBGisa2L4WDWL0tusHv0vdKt0d5AdHlXdsfp6
zNuDynJYv10r/fAC9zhRQSgeNJFM6kBhXUBZywcnlSPvSykgVhY/klC3S/il2vtMkhG5C/LlLclq
YjfDzBjNU27eTdwD9LeP1dGm4vbyrcDe/T2S9/C8x0Lt1RzSr7o/YsIvQ8+UWc7UMnxUiGpPhfAN
5AUo040ZHfcqjIyM6yCI7AZCdaY/v0Jl1Rou16gw1SM4TGQYce/vRxmKmU6Kgt7J8QRkh1wFMZj6
vfvC80DLu/Gz5mH1pyDRaiJX+hgFWR/Ws30LhXBJVUFW/iotrdsd/vhY4Dsv6wOY7fSYFWddbeix
SOUMFXnS6fvagwNHeHqCOnpZYGI6GLqeZXelJ0kXrCAArQTkgEykvynyJsDq2nOum+hJWeKvpAvL
WaFmAH5Pvp19heyuj6nlhNjs2sBC7lqlcERDZrG9yXkJ7OCOmJ1ebrTPpL+dvOQVyijTyPV9yxJ7
VCqJDN07CVHp941TmnLvZnbJlWdFV+M+yxVFu7mVViDDcc+GT6pkAbEGjLiVOJ94t0kYTIHlM/mk
OJ5d8YlGCiTOJdqRyoY2jzEgGa70W/n0APQGk4rURsXaJ5KoDx0hjvZ5Ivz8bFAsw0gmekfrhXY0
tdymxquwQ7tw6Vf6scqZIN4QqmA1QkTpS5YUN0vKRxezAPEqR5ZB2mm5bUnDL8SzbrUHGBTi/XYF
pbWM7CNOI0YpDaqkoTtP6th0jn7iMxwSYajxtTPuXV62pl2KcWiKdXyME1GtFYCvbSmKxwpzucBB
DRZE4PuwBxu2qw5f85Op6F7iAFq8b2JxDkR1h9czp/k58lVNXJ983mhwDjGGZOC9u6lyIURu49/L
ltF4wY7wvyv+2JfEqhXisXVfYjtUl01umlUcm6MsNH4Ltiz6x0o/II5KD1SeUdo5Z65lDGtHJPzy
rpUAGv1CY0nOJ9nymibn8GFEw3FtaECaKQ5+YlPPptiBnFRxV8T33+RUbBHYGhbJ/eGnm+05n/O+
e8b8ma2no2eQksd7jYJFOET/2OQfksZ7Lr0bNobHHR+PnEZkClJUnVBJT/T8f/8Ezzr2aP/vTQcQ
r9Pg7uTQ1gQs8U8PNkMFwH3R+o3AvXZIhN4v14jz7jJpNHbguJcgm75jmjuaY6TRWOwdLcTgZ1ht
o1nolPpEIysM+MxZSYl3nuRfP4Fp8P641NUI7e+MDWl8yF7dOjvIw2yMlNmjhqd4h2n+lHbsi2S7
/skos5oWqvLUysKg9wxl3/W8O9318V0s8OAGjadI6KdbjhgiOjz8FWTfEDsaAlvgEk//7xtYUdp7
dyTqKbP/EP/0EP2EDKWfLDJksAIl4ldAYny1251jfZXY1rq4cNDPcRqkVwsS8OF2MQGTyOdyBJd0
vXKwyQQ0Q5pxzotsTS9hHLxb960hCTbZV1P17nqz95Yx+TIwaYiqSsroQ1ymdK3a4wFo9lmLM4LW
Va6hx8uXL5Orttuj7OqGCBNq8jYCP6BvtktSsdkHM7TruWnifvz3XpPGg9kfozqsi/g9fWJ0Im1K
rK8E9IeucpZUUflrMqfnJTZAZkEEtv+6y1Pz/3NSUb9zfk4OLTqorM2wDMWho8jIXaGjXyRK79vO
CdHamTmWOEvGM0Mqf+xyrB//4hHP06uekvsFoZuFMednwWiPv7clQn8vRsl7ri2ev4cV8dF09Lie
nUeipOtlqqdDUSuX4YcBU2As+VJlwv3F5iIvj0di5ou5X8pX4e4swEymf6IX7ihZ2TmFHaG3QJCs
I66xaJEfZmj65/8xpqIR78gN1d5PoN5eIXyTF1DSlNwO/CAHlB7boFXG17xlH4pmYw7ySxhWpUWq
8yDKohOMg78lmABKZGPKjmDeAhG2dPvTYELRrX7tUJGc99hZBDdK/k3ShUpsXutdhgzcDnRB49+X
ncyzXKBolnmsVGRWDtOU9xzmazM0d/VJspeIQloYbBbC32dqAmbjDrLFPWxJw9VdrOkr9a5ZW8qo
64LEK2gaXok7eGYed4TruO07kS3PS/0nAmt+1eo73b4whV+gkZ6Hn9PHDjqSVKt66MxBmqR5aQUa
HIgVDZ7LPzflKE7yvboHV5AwwODKjKk4t8J/k1QDvdBOlVuSs+uTn8vryIy7pgE2C4GwnNSEs5u6
JqZNkV019KA8oFwzbPLyfpiO/1BLkjPEJtTs4hsNlOprNw4U1MBnOxyoBlzwfgwthg9j50Bs83UN
Kpkg9G8oqGtQwtX2tF775JFgvqpI66GPSzwNxlvYb8d1jhp4dBtYUDR4Mq2RDSQNuemAaR0nlqdA
7UwTcPL7GqohYcmS1FMWPxWeCO2/tyZX2V0F386eq/pyZ5sQUwkpGa4MGtrYH1qnH4eOoOZeER2V
1uFHV91vVVqNxTU9t4uGcAYxtc/4R3XTlsp8UNr/lhiNveRWPf0rkP4Qs2DE7dVe2OP906D6kcEl
6sLcZYh0arJHBJocFpFfB4ffP2q0GBzwhI9AUO+Ho2w4l+ZZLneWa2rfwv70Q4unMeR3ECb3MLQ2
5PkGDJcXjsDqPIgvD2LBI68r0uI3iKbcGN2HlVtow7L6MSBpkLPVnciafDZgcleoHk1LOz5mUOT8
jNE/5E04so+oUdKSjsMqVcXo/GO1Pj88E4l++w1IhzRyFhdrvt12u7/7qtHHYowwqdhhPmKnMI40
lhLmpJtU3Yvw/rF/YNyca1m1iHBY22idi2+OjXRvn+cEHkylyGE9FTYV5sbewSYseB22qUnRO6y8
Dw5rBXua/4htP1QZ27lpcpvyniaVtwC8nYLmpgy99cSnLVRbafx+sixgvLzEMWAfa5MQpuyLxf27
5VlBKnm9JB6uTbgv0ELrsJ7QegqN8cC1vvrvIeJrkKxqMZAMYl2hhQDVnmBSawwXz2tbjXVQFc2G
1ZpAfc9KhwtZfT9BTeyCl5sWNUD2hsQU8jUkVd/o1+G2rwEmjAW7OgdLbda+Fhn3xo1s3IJLkc4t
KfhDY0zpeH1n+4zfVX/TZO4pM70gKRt3LEpXNjlSSzD9tbIJKObmVv8YGLQYm2qmK0hPEvDU0ga2
s+lSHoGpiI/x9kcyXiNFvYl4klrB2IPUcBHtrl2+P9MvkeVsBGCCH1cGWNaVV+GN4DTPQQGRBqsP
I2dJVUUWPR76J1zm35a4CXmSoHbKyIPk5ycOsfEBIHFVWoqOVd++nF4ZzfsV69t/3KeG+7U7LAo2
95hbsyxB0hW2Cbjhc84X9EwbbGQiCVow7/rh+Mi4Vdwun8luWbq2T0jxVOD4ZCtmDRyqHebbg6fj
9vzNYdYvL2xmzrt8b8LCXgV40EKTbvqZiYy6ugw76bjKKegProbIkMagreHayrNtIZe0M5TuoR3N
9hnjjCrKS5PkLl/XL5CMq1A4WqR4MYla6lb+jl5KjtSmfDGJ7Icv+dJrLSYxbbSVmrnPrxeFtany
czRrXOv8+RBiWL+fP+IB/T/BHONXU2lULCF+opGSyzWzdVaJiVp/36EZLAIrfW5vOOxlI/YLnkkX
Sl7x7cblmstnD7vWXu4RK5UbY3+tqjFjPz4rUeBhX0I9NIQ4StylqhxzLf03c+2tmr9OYyKdyLah
lmuUokc8/wYRctXZeMWErY+mHG0vEiyHYF2q/SK5fYMV5HoMP26kcXixdwqT5e5XlKwSre/MUSgx
piZgILvZ3A4ABl6gBseGDMruHnUS3CvqPjXiEJILrJP8KmSAHRoIhuNsXwRDVRCcTMMcOBygw3gZ
yQ0++D/NBhlKmVXG6K52YAgo1CaTdezPGj8biStoX669H6KwfcwXoW4n6CiAfftejvFYFVArL1Sg
grw6HoRtxVeWHes3uE34HuPeHoZjwqPftVNPq8JXBwdFSf9WIZoW2166/rCxDuVD2hVBH56AOsxW
h5EfEV9jWj6Yk1PC39UdG9meVFkskLJRQ9TZ0cy6j7rSQqmW8QKzmrMbthzvR0wjSEiAuzRL7fzv
fisBKz7t71hpsgYwvfQrQyd/UyntzIlKBV8P9K/xJ1Q+veNBJwNVls92e22rb8NgKyld/OXUP4xW
YAYyo2gLh3m2mh2U5kDL7cJcPxrp3IukV2J4DwaprlhLzFih6d4UIS/Q75ilQ2Z4ZNd0sbRrph/Y
ojI/+OztDGw6OI3y2SfThTMU4eDgh92mZDej2ALnmMqqotc0K7cV12HoGu8yd7oYzTD/C6hvZoes
s661U52HMFcYz2+/f1lG9YKwwqG/UoTK39MLQgpulf/hFXHXc8iMQHQ7AWI0cqNdqiFnjB0TfPeQ
+TAGot6/frl82B/bzulwEWh3heuWiCYUzP2uF9OYcXeSJujXG+EQuK4ZQ3Ba9pK0/mtHpzqOX1ii
HFyVJM7a9QLqp3TqFd0/l3Nbc43mrxMpf7IMput+52280v3VrOBVnBNco90Idt8a2IeZzE282cCF
lL31Fd7HJaUGK7AmutxzLijMoufEo5i+mxggqMOBokz9hTgiA/e9Sir5Ghjdhl7/ZhSYrZMdyBBk
yxxBv14xrEEMHfrCz26bqO79n1BgRhysLYwFAxnbBCnBYKBc3YSTE/iaOL4YPJZyy/n65gEVzE6T
eymxclmHrMxm5Ll1FSkJ7Qz5cN0uLkd+BjV6GsJTcLOSH2Jbi8w5Nm1xeUA7kw8nQaCVROUd4Bod
yN3wkejBJqdCGJTIHzrBoGtT02OSnVHVob4Aat/Wa0GRBOapQE4Khpp5PMXcoNuc3ew3rxQMJeJ9
vQTGzBd/RtzqIt9YrYBL3jcz1++r9Xk4txuU0dgm3YiqdBNg711xfhmUogUyxjslz85EFUm/Fnez
8YZUFIJpdK3TRuJWzURoHnuttEUelOSHO+NK6eQnPfQw/a7icEngMkktaWZJ6DrWan/oo8ORHzCK
KIy24FLEHvZH9FLkxVCYvTIn/zj+IDzuNdyHT3Ye1O4cuXPhhOpbtG7gaPs4wqZiw7fiN3HgMPYQ
m1h4ip13XO9r+wZ3oXjQwXZUzX5nizVExfoUOIuX2SNkWtmR+ORgbnu27z2zYgvMehuUptrEiCWS
Ama7ywIY8m+5RMfrp2ZvrMMVsowV88tMH3JYpGWCCyYIDcNn82z71ZnNFilKgHknzssWK7L4tURQ
FPNfyV+h/exQ26UlgDeRKLIQTJ7+zeJF13noYuMlyugwfdyRwz7XS0ipnsl03NqW1bYBqeWbwpj7
Rsclq3qg3q3tHbzQnw5Wf6GIp5GiPd/vS9HikPlgfOjzRk8/SETr6RCgGonU0jvJDiWO8a7Ba2vY
UPLU2Dym2TyxnOHR0tZ41BsIo73kIZmHxgthaSE69B8p5/1Jc6po7Yjz4dgfMdYL4OTxG5VoKzKe
3AsvFz7vZAncMHj4KuacvC0Guu4GInrwyWne8s2yRtS79zZynmQQLP4u06BhXZ2Zo42YzEuWiJEV
ifH/Mxl5CaZAtL9NyMdHe+diHhYamlnxvBBhwkskOK34j6QS3qfyyjHwIsGrl37ApD3Yk2UWQHZY
k30fvsyrePMp6zbc63COK0uwTrNxR2x11AfYLbBw61DWXXqNQXYSUrQUeypYNhFlwSWKchMVNQbW
TziUGKLyP9sMlD+y25vpHwRPR6z/Ok9q9BNgPIU/P5jmvuX9nIKxwoSaW4tmOV8KF+7MuLvECx55
wM6M2so3OEeATQrCGepw2+CePgM6rNybsr8ktmvdpxOBmjWbrShXh/mq4PHbPQC4qhvk8pCy6YN3
IVvG2tJ6k0E+H1A/MANr5nhZep5iG165oBSotz2FKxQy/HfQTMQfwzcatgKWzUJIxgQk2p5Y2Ze7
ppF9yDZ2LL4RwVXwpSAhbxMNKvJsVTj532uTwvWulsw08UhFHCOQ4CvF2/nS+KokY/fY5qqhW/P9
Z8CdVNH77+A1q5AzigaX6j3sAgxFAwgxy2zoWmZaM/yY3PNr3mF+kP5/nBh3jSBsdfqnhGb3SOt0
CXhCG4WzFf8SvB61J5/ItRYCmlYo2kJat9ObZXlNvHMyqmC5ptZhG4AN/V/4if6Yz8jt74pc5+RQ
0T54Fv8lqH1CSivjeGxIxwteGbWBLfQU+AaxerGwbnIDVznpHTC3+vX5d6PLigVKJQuyhGRX6NwB
LGDZl8cAiHGh66y6wnnKcOw7OLtYrndAifwXTuZKTurjFFEz+1XXo9oe8FPaYwvkWPCe0mm90AGU
gVZtIm42BAKHwsCQ56VmvxNb11Wpso2abRf0+wTNzXEgyrPW6I9jABko3NqF9sEGnAful7gY5ciT
IACMb78kYzUw0uIteyt/7d6OBFZ5pVfsmV1SYkFKEelIX2b08R1RU6RMk9WBim6zFq3hrb3drDGH
PZLMrq8rq5orV4YYTV+qpewEUd+4/hql5zF8fz5DjCEuEZAReBfoDicSeQ5Z7TMTPuH1iBK4htoJ
kTDWH9golXxOIN9geXQc+a2jIjFeM49zc3aWuvTkAw4V1zupu99NEUGcfrjhP8RPx9qMxosg5/ZU
j3iiB3pebwSkAAmFZUszOj6dAD7MXWuDbFNaATThh4BjhalL8llD43hXvrPw/MPjx1SIq37ODwTE
UoHZvLPWyhlalAsa2vHUgf27o9Yd8Zjnb83e1ZS3CpI3JV8dOzb/jckADi0uC6FxZWkt90+0/LU8
WKe8ZHBEwiRdDGpNejTiijq/kV0pqSLDbJTLCieRKQYLdhpyFJ3h4MCVHhrdtaH+KuPiBCrpZz3B
NvfCVWfkj7mBJOCLRdWqglHNABoSQXLWlE/A1XPGoxFHWBLeVDvKIi5YppK71DZcWaHE12RqlDs6
CugW+JUtKudSaO8jyTc3KAttas6Q+FC+pqsudRMSBJkS6XrKgnkmBoKbTzoD67lLGm5lAFCEz2I0
znd/6ABPdJY9N/zLn7Zydmskpq+CkZATexA+waOZ9bgHdzNfBLkeCMUBBuWyM9h2RVT8KWL5Ux6E
+WtxMLSu0HFUXSFxPD68JdrjEYU831CK4FCJf227hTBZpDieLJ2/0BLlZDY05OhEKy1SczbIuG/0
Q/g6NmN7Du0bx7O8w9fQuAj6qI+iLk3HZGrHblmG4e7BqJ9+59702sCPfrYC7r6crW1j9VcXSV2t
jdfDRSBQpb11oZPZDsg2B2B5IljBGMV0FD2LVXzYZoFkgVhxeYUqmuQH+mN3iwkJvhoCkJmsIpgQ
d/YHsAPeB+k0szWKhtcEO6p9nFX1IQ984jbQvL2aWOxsjid2divZSPua8ozgzQnXoIVYKiUdiRR2
zxhhg5GvnjpcgToHOgAiNvNEH7I5+pHS6JQ0rnR3sjAieuYdLPY6WjPlpLhDt6Rlf8zMAmJl58uf
YNGLeXvvNDdl+9mlqkiVxJRiFJIwb5K9C3BeA8SVIGx2/8sjDCqXa4ecxnSPWJuiX7dHXcAcjerJ
ObwxcUE9Kn4BpoY5VQQIQekVsL07BJ4h3Kh9qkXezPPpJlgkOpzXAprEELpx5dzUUL2HEj8lk5wd
lRjrgYGsXroMxXjulUNLvZx3xFDK0tTo3vEuh0LrIy8b2WrZP5vCJfZh8IttI4HBfz9E4Ye0gU2T
Tv6RvgAUOk/e4xURkCkD4L/cmQeiR7vw9Oj2SSHMVhOzTvWRWizgXzypMB3NEf4vqJ2U+SueKbqS
8FOPqQN4XeeAZD7NvHiye7a8resmGZAb88PuxmqivubQuwWB5kKeHj1dCLresiRFRrafwqZT6L0B
vjRVbtDilxCi/JTKebZkDkrcC9/OXflGfKEhiqBTMu3GZMAXomV/3BpFztx6S3WSLJO+iU4yDCYU
Lhsqh5kajDn3Eltc/Fz3DSH9UInbcb0IG3wSQKEnMDAxXurF2lTSRezRXWvzrS1K65wrlQ4uF5Dt
N7lbW5v5tcA0u9ee/cSMLz4nJZntr05rI9FFBDGnaMmno4EZysuGCFmOya9GTRMMTG34IAb20Q9+
5/xnA1/9vPPdjogV2i3BVsSTM7s9iW42CIruwQ3/QOsEaR7UcBJ87LOPcZ3STERcoT3vkSNjk1qc
IdANjOv/PO97RrhCsb5EEiV/oogF2Ur68MDiXEsduXfM3FwoMOSiVMlFqrEeKdlOJje7z6H5uTCw
ffTpHsD7KLxlk/qi7q1AQgHyoVmICv7MeAxdL2ZHkaUHU9CwiZxmafY0QRAHBT3q9sx8nBlH+xz9
7HugyCAAW/B+yenrm3PA9Y5M9iHI6MqG0HK2Recxl0Prv6tDzM5xtkkaHgtTsQicrIqOaNs615gI
xT4qtOJbqK+cDrpAE06YxZXYcMf1i0fzkYYR+BkpP03aGPry1kRjTwBdogHiSVQVctQY4d2+tBGz
oG9Wm8ai56pKyKVcKs/K3mNS7XJ8ojHsr/Jyf9BUmMXY3u+I0Q1mOFnwOmILIg+XLAqQzZbyw1R3
LqcoAl7Uv2wCHwf2RIdcJCTORUFLabMjI1ndD0E6VCXzxtFW2GO9QSin3tjqoh0yD4DsTlapIbp6
mDjWvNaK3smOqxYeY24s/aokQX9zFPwzdvFIh1PyaLAe+HPp+XlUHZMTfE7GgyIJMiAsSagakg0I
G7/jrhW9v2d8FJYQ6hZ1S1cn8yAhNRLuCqUgN1y9cdkEFKLb1glqQL9ZHy21GYGdUI/0VD26DK32
U5Pdv/qbTbA37yT3wvpcJpNbz1Kc8Zt0oqiNu0GSuHw8sZYMZaxzOkACeMddmT5BZq3CGkRYWAL/
BHnEnHltRYQmbgplIxErxNvBG3RdtmdqscCDtX5LzKZj1tYdD4b92lM+6Ux8k/cxPziDi/FC3uPe
D8cxyRyZAUAzDLxyApF6w2M2Mahi6CuTGJgbJdz2ruoHsYu2MLnsDQYGiyGpLE5BLr0ewN/Ag1V1
xkORoXjyDh+3IHxxlNyBOIoKSM2YfRKbDWb+RRXmOLm/0wmMgSomoaJ2hI8QZAj/ExwDRtJRC5Qc
+brhCknRHuBj5ptCJ8itfDyrSYRLHdXd7d6TpBxUDv86Y1Mn8V20wSviN95TfYSbXJpqZLxAGOgk
GBTuAKo/xGwR2Vsc/iaojo+TuUzZTOEWgWMAZmgubVImiK2RiK+EaIsKZjPKEknUs/rvL39XyUEJ
vBXVy3P2V+eMj058qM+SFrO/f5IRqvEfcd5KGdfg0fIzEZ+o2xhPOsLpkudxndfMdsvF0xyu336R
Nqklenjy/lwj/97OasfP/CO3CIL4/rcJa/uBejGH2zFzi7VNvyNF+sskc9uAKLgS9csJnxxf+NRb
D05dkDLh3aM7ZqsRbwqbebzbxMVoaFj966MDJ88BVOHwf1mzAaz44tPiPjhSybiFuFTkRQTPVhC/
sl1kpoIAE7i2l5Axsf1vpqgdGSqw6eNYiSwKbThdy8N5HPaxQuBRS9XAXXBILY4kfeWlDUocrLxE
ny+tTIPJAGsAOXKPsBc3V3qsFyIQSPHcUKFXtvEi7s+0JktzYT+N4FRwSOXRiBhsfSSW4Z5aZjx0
UIOJPG4X7W1+hn7hiD4VzqrDlEgwSYNSdTcyqunWxZeavoB6S6SXFka5ZASQXBsOM+q+x+St4uXI
2eBD9dQLujQheZ/Vt1E/HpKj5hguxApXQLSajaUhe4PwnRmrk4X2v+ZKCB/N4YpYn6nuA/OhBRga
yyMoiLOg9nn1T4CIwi/pfJxxgiFi8rZd51fT8EomzL6/4AXvc5PXk9MV7+p4l22CoFy4yayBanRB
tbCtLcwK4BqzwehYAtF+MSeQvBNsHC1Xy7vG9tk7AkjchKT2vurKEFsRvH4DoGgIW++/R7E6x1Pz
mEqH4GEAD4MmPB+CQ1HVh6YbHoC2N+YA9dApLJCmzADzio09Zzx/1bfrYmc+xqonniglqYvDJ71n
vPWizwTqs1tR0vhPQPut+qA22mVwwbML7QmnwUowANJF3eC5nWKfnW4F4teq3G5z3X5nGctUvFEN
t4vSz6BQ3Cr9HM+7tRaJBNQ9DcVX0bjB8hmKA+CdY2NeIVAVsm1V/sBFttgsuBuvy27rBUNU5mv0
qmGP/TIYKsEEDsagITCXaVB2q9W8ba6ZmDuKOZqj8Su8F79zOujy+Kc2FuFvbj/zdZaMg2c5NaIr
NpXFmpZAbkfhe6niLhmD+9Qg9eReSVVhRTP86Tnr1l/vd3XWaXEI0RwU1lR61qJ72qtOqCgmX07+
egrfz5ETQNVagDqag9HQHBZmpeuty4kGyRnpfRXzjd05PTBqx6Zy5VZsXumobvOa5+qXRXJ3BH9a
zhML/C/r0iOkZoFDEQHR3k0ktxJKp7R1bYSH8gYx5nStXem0/M0Y84b421VlA297qZmkytlwes2S
l3dI0S/zFllCdIXYHpfzolGc26h02y20eD5wmMmMUXFl+6QQW1t+e3zj1FcksYp7f8TR2NVUBZhI
MzZg5M9jfu7DDlt8PoN3Sq1wKXVzv77nrU5WwESJEennFRoWECdjVeJnrm0dbvvMaBWU6hyzkCmC
WIQ3BFUp7ugAnGVS9+2NYA8E4tw15x1wjbQARCtrPw1FhneXr8uB9rN6B3t+Dc1SGIbw5jjecSt/
nUZhMuGva8Zm7mt5fOaubB6RpS83CLenebjBM3gvVxOJfBnO3A1jKnf1L9EiWqljEvkZ+OWbspLT
gz8gl04rsdpx2XlTxzLhCJF2FLwR/OCiv9W7gCsBKuWdUqmI9dp3FkxqNCxJN3wWWw7qIwupKKqK
EE5YZt+Hu3HOuSMkFkDemBbPi9UL5A/idB08gMzP4Yfa0CdsZaJFrXvLb9yWga8mtbUPUMihQMIa
CAvrj52i7wiWDOFo2HFs2CCMweZzZ7SgjMHTQohYod5JjwrGQku/dCAXThbb9+DXNE41wALKY5j/
bZpDBR7CdWYzKwdh/g4r3kwure9amHZ+0IAbyLbeKuumhn/AxCCWcnToihX+LPNO3SFrfcSLglZ1
ohRM6ye7YPKx3jFPBs2hDM7W0wLm2QxWCCI5jxKhvRdnxJmx9Kidk/SUuNJw7ozIO6ogDYGCT6U5
5prdb8uoa5XxVTcbUzvYkNcS8nvy3NA5BLUS5/m3kelS4jNCetGQ6rrYfD4a/Dw1oJgK0NueNH2u
Cw38QgekeRjVbauWVXyMXfT7UKTmTuISQD5JRRCIcFq20P66f2kLCmhRCPfD1aya1eUH9uM+seQm
2TUtO4RJ0JC+ou5uvzlXQCz6bIiuh7ajYaR3psv9jujdLAjMZxYrytzP/dtGozhavCB7+LwcJfV5
X2E6SC7e8/fJKrHVUQghaGLuRJrpTwlPu4JLY8ODE6Mou+Htqqw7DJfVAOKCSxNtuu4sTipOVN+0
S6QUb0GEfSuePC8pKGmQOFM/NRFWA7lQkdFMs/oOD5iyIQMdZgo9v4aBgt1u2kgbFHfj9RPJDPPQ
cml7g3OFaVDKPriGPAsGQnD449MH8LQIfqvkNIRp8q29NTQOWvygGp8aB12/h0hvx7OWbr8VnPzt
hZZxyXVFe/j2bKd8gzlKLP9TJVeoQjCCCcFqC+lNpl91tS6junVQfXokjdOwebEc4N/8lsbRAnMo
F/bSHsyHli3S2OEuDCC7Yme6ARFwTpv3m68KF8l//hCk2KsWQqC77TPaqAbvAr+4FNPemrD2aPUf
1BzpWNjvLtFhXDd0Wfg4gyLK4KEcLJHIWtjYHs6Co/vQyWqATg9Fu8qYljMYN26vmXzp4oVqkqFR
/xDcQIVZ6eV5M/p6vm88YLaHo03pSUAu7JiIC24sil262YeA3aFmXBo52Uwk+zJiKVFUdNEWa3Og
oRFM7NXwkzbgMqsNrPcmgoXL6CqNZRwYjbFW25qu0ArcmBLvcj+FMIS/l2/XhIxuqyi19xfUOtAx
Bjo7PXT/oVdpSw+jo/b8AknNhXbYrVbBW2DYWNmi0Knf39kNZhAIjA81zpfZJ9LJJzOQyHvhiNoQ
x0FNGS2TqB7vzLLneZ5k6vbHqHbHaA3e4kx9ZrlLI9kdyziCM+hxbi8cVImgf7SaSGOkQCga94k3
rwK+wb9Mf4Qq2KxK/glOTMU/r3g9gtocRDDNPHy7m64b517Tp6GT+Be1RxS1PEP4vk6JUHsxjB7Y
4S2EGFJekVgEQfW+h0EPGZx6tizbv3xn7X2IYGycedkFSOoAe0Rpg+ZVbzrya2jaT+d4Dk1qKA7X
exjZZTMmN5s4h8O/yTaJsWP9Fhrbi/h4I8atq72D260ivAfpZqpb3eIhXc2J3CDKCm2XOIp1HdLn
T1YLd6ntSG1c04J7pvwGQlSZB89xFEn8Ak9zctiqkvanQoOZ7asubpb/3oozlUG/94SGv6WKmlfr
WdRUePI1JZhXDrodCWAAIX66rws0iaHpDeoKTsn5D/TVmAcDOz+9ichaC7moIHGPpKeSS+eJm22V
T4NJLumRuCWrgNb89Vhu1lMPSrKf7G5L5y8zm6QmU98MJFCeuYo3rz++7ClAwmOvAn+8FB67Ip6L
iuyuwMcdiRrP4RDrLq7l6VuKiH1PyjVKnRvoh+hEAK0Hmtxfcexi051a1Z7veNNBfVRljCcnFAnh
2GpstAkJwsEN4LyKQkOGc7zB+qI8pdxWk7Zmi1pbwMIskJXkBiZEObWrQ+k1GYJAOkWNIsh5OzT7
vQoOeuCY5Z0/xB3+olx8ZaWI4nLI8blAHLaIjnqk9jhp79tP25d3/9KyRwH0cLPrSzBmLGbY+oWx
F63ZfspcUSi1powIx9P5UjxT/7D3PXWOwkXVRlQzFvu52s6H0/OtOesHiLZjghGEx+hKEFbH25Wt
84bgaNDjBjEvD5/oLEN8boMZQMoLqr7nyugHYWDkmqPhXEkSn5PmlP4JR1PaFhVbovpKl/sMXCy0
PC/WSFvi884UaDimN5ZbxfDhRfgTvQcw7JwLNlYYAR653JmlZalhoRTdpxPLKesdcPAxGgFk9NlM
vzSdXgF5QiCg5oy/YTIZgftdN4WdiDI8e5ujRk/qQwlQztSNmjxyrLzKs8QyoeE1E73ADC/HJE20
HX/Bi6QQ7Xpajx6vfza0RZ6lLdfcwvH4LBUQgmYRkiXmcm8quRAKID1L8J2I42SNHLyGQ0XBkkRG
ISsQ1kGgdJnhHLrENBdf5ybY3WEu0ttNeYLtw96qofixVTcQV4r5kGYEh0xNSIhUBPNJT7rg1AN5
0zv/jyjjNcERI+v8kYKvIrL7r5sTDc8isrLHDO99Ew1p4xJxt7Qw6PVLqbw3Hfb/hvBZx4Ab9J4M
ec6AXlLePgoq+ng5aD9t6nt2JldRn4e1NXzeHX/b//tFfOPSOuAs3pm+wieaZlUkdgynt8CETMOR
+HOMRs8a7umxYI/rvl+NWMPy58XDs4ZaphCuSDRoTJH4VmkVsq8edkGDfvzG20Xc43QQqOwX3+WP
Pgixor6zDb4jLCO87z2QNUf0uVHE8JCVx89cmooqIWvZ/vKnuCouIDCQrclBaH9iiqKD6ISjXgPv
Gprv2RlJzEE9Zu9McsnWZjNdb9CLzMF1R3RzSLo+daL5QemHJyz24dPFeO+sUjAusdtRLglUNsNO
SSoDmpqI7w7KA9dEmqq3cA3Z1KJ8YxF6D/zqcGxUS5VTKS6I6WIvbhkNoI77xKxxyHckuQ7JlGJ0
/U1uJwzCqR8K0kV/hKyvGQqWz7uIHh4Tg7MnWRLeewq4wqCwEspjxTTRdmLABfv5BQKsYk7sKn9I
SHQ+WdSTW8hg5RG0qpWCa2z3f316s8h2EzP3mAoyCLxEUJO4/IW+RbwNPPcS4pT39DGObPRiemp8
bbj4DwSpL6ommagu4Gn+TGJ6VsBpz/WhazzKUSxhq1/ElaFN5qdUT/hggsinxXCUOR9qP9slBjPn
KjAs/AItPGoIdPMPpcubp2fwGE3KA6uy/3V+yxAjGCoNFHtEdF1BvEh2Dzyo1AF6XMhyk1oRcwm5
t/c52SsxY+AXh8444nIiQ+UzBcUpsyIuJzOjicdH8NWDACvY8X9RRXPC+eaX0lmnZlbi1dnzi8v/
reKJu/OuKbkSbbMgtLshpn/jwSNVItjZAF2j3zYBSHGjkZlZtbMdLJytjEowpKCs4A/pe7nH/xrK
6e5JfG933EUOHxDLfPgqtLnaL5PlPi8qS/WtgInFZQshKcC+f+Js0RNfyQGqNgar823wUlpPYd/Y
Cvh9Mtw70SDPxqWUjAfs4+JRpx/CbAt/evagJyvzmzFimMcjCDBoNQeNLQJvY0ia2rn8LbT7Zsdh
yNFrmXHXYligujIZoMbH8w0ZSNEad035P7OND1RkopMlO81r9M5vgoqzmDTTz8udTf8uThzrUf+r
tFIqzHsyDh6R+SDNfmbxFPpC5wc3Gzsr9eYI/w5Lq5yjjHdMKQEOSJAgK1ZCryRNf8vaRlKOxjFr
YGmB8x4aCLL87oiBzMG+Xx50GuEhGB/Hc9kxEsfaawe1GVqG2gWSGjqesQfx4omuvbjU7GhMzE3i
SFRkoQXuPR+fiS91wqWUkQnhVpWjH8d5vxznp43gHJ9/Lw2WFJa5hsNbQ6tCBjkezD+uPjdcz136
99Gioeq2ltEvTUnUHkzcEi+7fw0O9HPoChxujoe42b7lOR8t1zUdr7UB1mfyrW1pxd15B0RN9cJp
YwZtvs3ymIlWkpOE1Z9zxGg8EquXhO/GAxZHEnIXdDMuEz8ix4OEXIyKC9ofXI0crxcR5oE9juk9
cKsC/XTCOk0AgP/YVWS72ADhOsvRtSR+oVo8X43vCDE++AhEnpSV4x+y7ode66vb/ICueswbgHCF
l75vEyJL2Ue1g97WzAaKgMc+vB/JtuYmI9FX4HDCJHWlsr7YsuLP8aXcfbhxwcGV2NyccXTAgGM+
OH0YNhURzA7ATrpTShqLxxx5mK5ep5zo4+exDyyIkgXstlJnXQa765qXWIdHNjYhfPTMtzExcW1W
LJQqkPubujZ57QHvjzMFn/ozXqrJfMyre7XQdRBEu1UJDvj0U9+BR8adTdKjjkJW8ZKQcgYddxSk
MQoIHiyU5Ioy/Rphoem0NmpabiNI52WkG3zyVOYnZ8zlcfOtDxEqPJtSIjHspoCEejGSQzfOCVQQ
p9KQFI+Cn7higJ6Y50Cr2F3jUKWmJ5qIk5P03eJna7qN4b7+qGbVDfgz4+C4gL6p9v4WFPAKLse/
UBN2KXZzYfZl6Sz5FYHHqpg6eplLYbUpY6EpIu/zwBU5i2nKN9tBl/KkhYp6Q9noIOZTVRxrkrw+
Jd5w0HMggV43vucdA2I47155Gqf0sI6nYgXbbemf+Bo0XMUTo8PEsd/ALaNCbMedocKyJjIivHwr
gZ5Q077kqpNZUhuFPew0wE1wqvxQizyZsjEbnCQtrVdPg28nJ1R5MXYZa56D82/B/EIPN0Sxb8lC
EPjC6k2gOV+lO5eSK9bziwFE0hkKWGaOdjLvEJp1vWPPfaT0iZejVMwXVqi6ME79vdnpjSQj9/ot
A8IBTEN75d2fTkiESnL9C3grbrOMw2kddng53ggFypN2a6990TedgCaOCv2g/ZyV7ZzVIrJIbed9
eMjn4X2FRhIIMxAFqkhU0VjqG4i6PXXDmkxplJxKxQuQMupFegVtLGdhZ05dFGL6iINvbMn87OJV
bYS4EdHlejfdiOp8C/t5BWmJ5Uk6urv5PVNb/p29K6h10ooO9lag9UAWBDGGvogPdfaqkNUn9r7H
LRff722aBZxFMmHGtEbm2wUzDOP8srM5jMS8R5C672risoCokGiZJ75Wmk8YG1h/bMg90PSlEf1X
b2C62tz1RD5tT6a/D1kWTenDS3NOOkReMRQMyVx5xmFlN+WmQN+c5/u/Ly86pGflezoQStbwo2vW
hsc1QhSJ4nCBZBPXZhO4Jt7OGRDf8cjlmDA7tIFRqySCSSqYWNORYjyve5qdOKwDQbh/BZOAfaDc
KINSM3Ua/pvLy/tpovBhMWbm9xTwh2E5WpkrZxe4n2ZbC47q7QOHbHpC6ULeUeDwmFRW76him8LC
QgUbEdYOgBH/f8e+vY6sttuQV0wwV4h7S5gHaZ1Z1GOJpQuymm1UEFqofSklKm3ASacHYQMGCjcM
um8MTJtBnedDB8Pm9nyQ8EvU6pdEvmkYJ1O4Fx5AAcChrU9Ld9cAK/z9t/dAKR1lmuIgYITlsX+l
FCpK+hzRWNvSwFRtrvCEeOyV1QXwi/4xPQh8iDeEmPEJgdf0jB9HWJZ3DS/Akb0YL1AbaSSXEqnt
OEit+34llR3lcLsZJ7wnHDzijDUKc26k+7CBlN+jh2YeIS2mosBimnSn2ciIol0wXdJd6ZoUr9ZX
8YlAmmUB4Z96LIm8z7Bg+AEqfrd90UpD1dWuQf2rYOh3YLYcfZZt18TbmFUIYnExuniE1C3uzicg
41tItogr5EBrZksQDTB57eJvntYWPwerkHF+X1SHPnFskaAzDa1DBKPkCUmwXdQTfdfiy6SMCPRQ
r4+coz7rh+iUIKzcWYUphnAPtf6wZQCGIhtbZlfV52ZJeH6Enrm9t/yLaG0XGWUvmt2nBRw9eMdR
A2Ypu1yq15MbNKNpzQvxIwBVIULfA2MGdcL80kSRokSAKGKyZXiHBw3cl71LglQtCFIrfl0c9zoA
a0pSwi2jo8j0c3ApJj0fMqTVlA/tek5QYDmjJj1C0Td2e4ICdgDglKJoYfnzRbmx5JGZ3at/4YHa
7Dr3bqD/RBp/ZBdbUatvy7FpkFla/IBIyO16jubak4+wE8lBk3Cfc0IXA6IdXlWTglW8ZhiUJLmH
pbQPdIgwR83JSP/iGjC9s2+doeAOyTQgwpjJNAqT/2QCf2Pnk0XFgseFg27B9Ug2MzU2IymcOCF/
QfEYWbPWIpfwlIq234QbBzwmNluwGkb5s+AW6/TDhIRM9T/JkDkIaq01oSdqRgC2Lq4EcXaDek9t
W2smf3zz2Y1CJiJCU2l7FrIqjosbvBhYKM6FserEX8rNDJx6rcmtklVhNP5M8m2u5VypmZmfYDIz
RIFN7licO8CdT3TTzkz0UnaGVDTMr18IZCsmPG3H3z1eZeUfZ7hXK9Fig971Lv8Wd0T59BrdkKv/
WRoeKgc4RmO163lu/RVt9NZdn6wHIj683AbOBkDYxGkXbj/8t4fneJUTNvrE35WxxHbxQOsjob0I
AOEiDlDjsckkxJfFLwU1+sIjlECu1i8NPiDWQ40FeeVzosN9zOOcJIfsg9l3GV8slhqchETrFUUR
yay71GbIrhmxP9Tr0vIW95c/agKj32Iw+1jHFR11JnJkn/ZpkeYCOrbg47xYRlxBUthDsGjxQUy5
e9rfncpG48FqIqPTbk1lpzuiG/giZY8JQEkxCEP0oSWlSriY+JLalBaB22xmgcWTxy9GuAeSONVE
dOig+aQsdaMxOHY+9U68eJxc+pqP9a2PS3g9fJf2/Uzp5NwlQ026O7C2VUx3RahKYs8r1tXBXqYl
SGEpVLdMn5py3CPM+m/vmWtwQSgtOVg+E2BqE3HPCvt+heCxCElYv216uAwNjJGP8H5kcUu4yzHi
3qoq/dqakQyTsSrCRYmUYP4fQcWuid6AvRoQxXiI239Rf+xTmYSnX2dFUuhK+7chWdqH+q9eYF9A
9Yv89P9WAXtoRtlGbEE2C0DYnZJjK1qHRlJxhlSCuxedGqF19kJZs+lpMKlWIQAGuYp7M4MGa/aG
+sz/TT6LtHXta8BFToCvny+RDkuX2nUACbP4Jr6XLRAln7R78pP9D1f+7sH3dF2UsIBQOgJ2VNuX
fhjtKWjrRclVPJ19MVxd9vHWb0lGSK52PHdau8IA7nMW4Ari7YufD7MdmQuyu0PW8B7LQaPU+5uV
n10pmKq79i6BKqg/bSTvCc+EDIgVtv7LqTvKNolvL+MJE3MXs6KjAVAZWLWZN9Q0jNvbt+c51Gzz
FmDDZAk07XpHfBzNkN8hH4a5o65ldmL7HgPczRWSw4SH4qB0CjvTlXGDnxtrtrH2SlTX5ricpfyg
pZfYQjj/Rg6Zgjqf6LIF/1cyCeOHLHVmw2RuTVpp+VIjT0g88TLGoEhopnjWZJpOhQveGOOqGbpI
NmWHGsvZfYy57TR+mbBLWSThZgSvFlVSaxQTfIkDZwMqIeAjq5PJcymuazyj520zZQGXa7gTsLuJ
I3RapMB3cs3gj8cytgidBqht9c9pgdhZT8qmntrIYKEj9a6gf2xKRzyC6v6ErvP7kDQuFgLkgveL
ja5+EuLPXokqW5QH65XAAxliA8sjoaFXktRXhlvhobSAU1uJ4jx19vIUxS0mWJH09RRX5EyNnh+k
b+d8gzu8bLA627DF08uxV7a5P8OOfyI3EMDF13ffJc9Y0WTNA3ugbfAY9u030gu/HPSX1YhJIT09
XrcFBHW4kQtL5Cs02X+7sTgr0E2dZySKpQywAHtedUE2MId7PsVbNolsBntWlV0UDpRqHQOiUKw0
YDUADM30Jes9myz34islnfHQJayQhGHs7eFnvZ8+d1LnGczMry1gEBOkrbSDvrrYrOQLNT4raWFH
HLRCfg8VaZfkMj5RNd4fXXYxcpfuo9VXEGf0jt+nqi51eoxR3q/CbaqS5CHsu1dseiWVT29OrUsV
fyIVDoPPHpxhrY5xwLXKza4sScrKR6zjAfH8d3WgtqCpofjQ85QvfM2KbqJ6Y+Yumn8EY5JBnh75
RVWtZH2DAJUgyDMwz68mGg7K+DDKCTAM3ai8v40e+5LBahyqaaOorbOzJqIRRba8Wii1Ol1lZ7QS
KK50SvfWhTI1EdsSZKcoErNoEW3BXMnIyEIAmG13KHWmisXr45GLUc9nbLQz8PVhMsMyuH1U7DSq
byoAwGGAZTmEmGrCAs6OlVl/EZrYWO0k7n448VLgkLmzOLYIbRzmIk6nlwsRXg6DqixDM/xohfuJ
UKuox8zC1qN5okpsBH8VMit+lhkEm+vAlZxGaVglQq96U2S0T6BQ6N77T2RaOclb8/DSoNF44JkD
byQiQ63X+nxt2QoxYtXUGXm2zU3wMRA4ncItQ9fNRYk7fBsZ2YCixYCwIqOMx6bKz+k35NMSVDlE
fUDVoZmuNMwzgRemPc/yQ9kwVvFDY3jUzBbV3ADHrzTN3NC6i2qc9+KNqxSXrZ/ZKrWZygs8xPQX
F9RZiWcjaS/+1o7/2wi3fkKTXsmnvYGS8PV7BI0dMD3ZC7uFHhOKeS540ouap1aw9pHyMz9lWOFD
773ex4XWPGrhYleBUIiNFZwcg1Bo4we+/dF3AusV3w6CCjsYenBo8c6r0ClytO/ZYoVr1xt9NwRL
MXCbo55hO4fGSZ85xPaSEHPWkGsL2eRDBQcBcTMiTxRExVWaM0f9U+iI87riTOuZT3d6k4YVHB3Q
51AmcPzrT/hnfHErdofz66/Z/WJPO9KwL0K/FRIMkSzsguxBl4BtOz89v4ce1kTRjLyk7RQQhBDQ
4zTsSx2QJQMHWALIh3MNyB3Y3CiznFbF4Xe4svWP115mfS0xk+Ml3bGHb8JrBlpHOqvSyo8wOaSe
fcJr4WCJcMklLgPbEY5ui5Uv4kB8aHjCzybmY0k0ImsJxgStEM+DTo9MhULOTEMxWDB8NakBv4wF
jkWvlQkV3TGPIM3xmrdrsnuCAO5EDwxtPvyymFCNlNd44VruQUcVc42gxylq17k7SwF1w0eYFbPa
bs2IRWVUbHB/w9ukvRbnnk0YB1DApAzg9AgMyZOuRGM41h0lGQKNfmNxXT+LAPkZxKp9XDHOWJPq
Z6Yze9EzGqn2YVZDoanJ+MOffpe7pvx1kwBhOaPCABVoTwZWOlgJ4ZOMpAtLtzJLf2lBXoEUL98W
flOo+oCRsXWpyiTqSPBjImFQuG6CGNhm8WiWml2MAdd18tJl+gJE0XogqQBbW66JKX1+u3l58Orq
KckiIfKlQ/1WyPshGw4zxYiTNwO8P2T3/LFbepW8EgnIw+Korzv8pCtW6G/UtV+Q6Td2HUTsbp6+
FFrTl88sPrZqfBP3fKRA29HanN81dgz0Vnu3Ao0qcCvCommcPBMgtJr9EAaGe2ZiaBcLNKotpge1
8W9uGUVG2Jaunl8xw0l8SnYU58M8VMSmhIMhqZUBd5VMLeENrCO6EYYwbO7F3lKudKD9Jp4uIPJa
S4LvYBP+kmc2qxe4oGhgreukbwLwpBgwsB9U2ALgRdQ1poNC09KBGKYqee2TSl/C2HLqfEXQ/Reg
o3+NN3BV7R3QqUV4gjvNxKpysW1XKlEITmOSwVW184cAdF3qjaTOC/e65FtujFcXmgQGRFPzjdds
mPgAn7G/MtQZV7ZbHxchX7K1q10C2E3OHornKPHKWk8PJfum2ZDl2fIj4/yJ9S1kXClM83Yti+cq
33oVHcSDZDCsHiiCaZ4ABkIoLbzCqCGhVO5B/AjOFwz62v98sbXYaK9U5w/ycmTvQOU9meWxsSWi
AAuXL3boxI6YweyrGpcdgn01pRgRUvljcsF47WwYtk1VbYXShdljc42N6rxti7JA8JZTq46H7vvn
Ad1/E5O2neLMtHUbQIsbojvS1TxhjojZGzd62l+JBULrW7rbtU9Vy/8zqCpWu0LqvaVADXLLWUcb
ceq9kmg8SD823mMBcY2dtRC7W+9h+vsW4w3xHxhBojwmx923bh6c0xgqDA0Ee2kYmPgdsMIW/Bna
wJhf+e2/ujKmvVxqKPtElzTp/VlTZAEGGL0E2JV2rh1wQJE2s1XYERT/7bi8NJDMdHv28V0aVaQG
hTgq7gxnY5eFSgqde+OqFz+r3RzIUf0HMKRBSSrC3L+lmqR9QdkX9OOH+E+5SokvIrm+s++Po2WQ
M226ENxluSdAIn55zLea6l76dYW+V+ZSNbjeIbOG5lvj44QaZbWK0nEoinpt8O8bb/ZVY82lCo+2
HgvCXMZWfHIUqVoKLRwR3/Osk7/XMG8sEym3QvNo66Sc9uDNXE+aQtcT1mkeoNtLhAnzB65pm6Of
pAOQTcme2CrG+0ldpIEmYU60sqZlRQ1J75Qk/zmeQBhajg0/vsRmvkRxVm1IibB2o4rHwoa39BvI
41urPLCw2fN2kWwaZtvJsmFZbSz2e+cNTLQUa2fQTrynB+d3w+xkSVWt8BVCOZXmvyaSv57Y31dt
e7FhjDrza2TL6UG3aFkir2hf8dQZ+EoMwOoYMbSMNnDam//QblmJ45tG661bPaSOvoBRsyYLDzH2
hDZB/HLjl7jC+TM2GWRLMeQRBxVr9X4oL7LYSlc55Gdb6R97AhDP3qSaTrD2Z/T8yEKkZqZsJJIk
fMjPy7C4khle9fRTMkJigDA3xWjM5FCOZQDXl5LrcOsoyuAjpd+IsyzTIp+8OYH2mhx1jzDt+6H1
/tRLeFdYX2He5LpT2EElgXkRI4FVezG7q52OvNNUeH9or7TAtD5/g+HWgsC1Vwbun2Fo7v3NMy8q
TEbVJMgTLUBKPzkQXqMzoj2zQ7mZLpOWecoCUEAddjYRHvUe3Yk+daP2IQmAAVHJBfJbnRGkOB+A
/WE6O1wDiAgZdCsGVYDyjZSruBkbt8ST/3NtrvzdCeSf5AtRARHsnPsuP9lL3pT5EpF5fi43bTUx
oAzCSB0FSnAjxcuaVXrpgZZ/UTGHKlyB/ep9g8WiTN9a20H0zmtyr+sAUr8191xJ1N8cQ1wJsCJ6
tpkeQIn821enRNht+pdE3EoqstbbcDnJMFdUpTXVgEDrN0EpJWpUWYMjGEOxE3J6+mmR8uZyAq2h
o7m4mWk213YLUaKhXRy5Woh2i73CIgu7VAkdncTY6INk7MxXBjiBotXR6DpK8RLsl7OOgAurFp/5
wqsUMIiTEdD5ZjDJuuKjFm/TLHRa9NDtU+GS8gTptqjioTpVWndIFShYSD4TxpmjkJHjvxmIIX7s
U4fL9WLWe0iI1d71mb7ezWFT/KuKwS++QGG1kCTK30liNPsfMasgUzmRw7YVnb7eXvhDYa6VU3/7
Sfv5LZdrhLCirhb+NDAuff+Mh2b5Ed5daxFDk8kiBCZSW6LNfyqHluYpni+ox2bGDW2aD3twOk5V
/o+Vbh2vl5k88ahItWtgXhHY2zvFKZYV8rczHEpEhFoxx5OjUBqHs6I/yN3l1Kypa6X8ICj5fIci
mK7nY9NWp7mZq1ibzmNZ8A3fUiN8Hn6YztXjhwJ8SOp8Y9WUxkV7zvkZ6ZAW+8b+AOCc+9bpE4u2
cgnVdBJBcUIoLiO9Vhf8a6IinIm4C5uWEY2O251lGtmQi0lEWr3X4IHVJ0rPbh/ogfl6JXOSlIVX
8hNWsdVte/qFzvFrznyY4Daa5jcTLT0bNH+wkyXLDzc/nI9BtmfitZtpJnHI79PBruXpgjMuNuHC
Ce4ngeunRayrx8Wi4p0QcTti+MWj9cvoP4w4WJlXLBfr+0V9F4RJ4loMZqgXxn14bz19thkuMLLM
ed+tvrosz8D4AI8Ox57Yemi/vWjv2/EIjIThXWACNAfBvQpFm+fA8wVq+bSDwJ6KFifG6rsQP4Il
YbrW+aNEXkntFEFn6bCPMuOWr22p0rTpr8XGU2yS1F4EdO72wXm0zGdX+ko9DXDznt/Ap4QgomUE
7WejcjggFgW2/N/fmzVfda4CwoNCzpPIJZ/ZLnapXPedYIRZm8UsJEHo9yigbjF1n2JRELwslC1k
AC370Ie8jtyCGfLHGeYnsnGK/kiaL0Lj6+baVs6/rHqQzU1hb26IIlXsYurPSMHvHR3Zk+L9kNSF
yHDXetUcZXTMjhufwaa9XNUZM1jNxMTpzNaHIVnI2MkN216shuTpcw8I8NmLKB3c8WqylGVsXydY
wqWJsocQvBJdYAeUYsc6R/i739yvVvwuer1nOFU8zF7+2hU1RJRb+IMUiRh3O0Ltd6M0n+++kkvL
sRgC109PymclbXptHeIVb1PkpNI94scodCCB43c65Z7qjpma5pFFxaDFYK4LRIuwNP9ZFoUFS/Zp
3WgBRk35ybiG12CWe7C7Vq1sv/zmnCT/qwBU2tLDZ9WVg6qnweQ4SgnGLb+W6CQLDrosLa1+E1nC
UzcXQi8mX+5kEPwn4IJWWeYGoMrh8LmG5wLGsqpHhehc90nLi2YYr1kWR+CLGYv26cOJnvARRFPL
KJw0ewxNwVC2DGl/ywjCCEY9VLYelA1z9uSLUcXU6GVk35W927ljoV+ya3TqFscBUxx9PrjTg8Fk
KKQveF41ok/AdgqtYCkrYj2kAtLmCrqQirKYijohuzJA5HoPpqk/2NDcaWbAQoNHPpdGgflG0pnt
g9tO0jbG78i6j2BG06s5eCJjuO+fnhHWS1r6hVqXPb8U0Q2caoUWXGxZpIsIeTxNfaxIORVmYCmJ
XsJVoPn7E6fsFhSH03e9MDoSgh7BNWeNbElieX1JLooq2i00wgdaZGB7QAb31Ny6CxkWpD6MT7bw
8Aw3osbG4MjPvr+pHEGgQmNQo41xSD5x0jUM35EsHlF4QvvBF+NIPX4CbNqooPYgWaiYnOVSqafB
OG7iCx1NNM43AW3dH2dn9Y2ue/3LfxOyOAOwf82xNCWcQV+Z1/9cxDJ9PLGrU8SfbvplYmnRLDJC
1edw71Pr3bw6RasFpF4vxCVzOX5r+Tq97kGE37lsTL99DmNfUXugPA/2If443QxeT5mOzpGABmXz
8L52gdEjypROM74SBSRjBnM4ZbGJJ5QT/37nRMWhlCD15bj+2Itu1tWIXAEzqdtOmX8dekR3rQgS
8X1ng2F6B1GEBIQA89uLDDaNnlKANx9yCSbYrLKs3I79iFaKKwqx7ccXoW6py29XwVMF5KkfcSdh
i1E67Q4pCR/VbpZO8gb/QynAFFAhplhvUDE/OQDb4mffBTboitLXoYcwGHQXiKM1ZRcg7zNjszRb
fxkTPg5QB+XYxSMwt7pdTGQqP4F8Fqsw6h6wodncDe1xXvvYYHLZh3b6cqDS4Q+AWbZIu+ptcX/U
K5bIxSUpJDlZxm1OxXYj0pCIrKejIZATjMRQNL1uLXGybd+SLYQJE5jxT00bWRQDepp49p2FG3MX
EorarVjVrsVd0KPS1XvLHa2AkRtvgMnrtU/QIvW2jKF/z2sfa/leztTO/25Q5aP9w3I+SbrU7f94
eWMa6TALSOgnw9x71K1gDWaTt2V9NmxCnpZ5CyZvrgIWfaYp0REsCW4dTlTnZ5f+LdNZB6hgaCFb
wJysLkOL8ntL7dkjYPrpSox2tvOr0064mdMlSZD3AX8f2B+EM20I2eGJ6XVfMbsURPBi5bO9gfl4
3Z33A5tDz9IKkPQrBPULtdYSs8U8h1YOcaMKJxUumNbPfB3WemRpm5Osez2KOyp5sNkLlj2wep7v
+qHT4JDBxoklyCrDwDjB5Whj+3PLa9E9aGcecmcc3v1NRwX8yB3xOSJ+PHp1ukgRaCbceGBdslPb
j/D2GdATCCrjaBatC4iX+NnErR3bDkSQP1Rc0h5y72ExakVgXpblzsSx6pFkpW94CNYJpPRudSs7
X7mc+PLEW6JH7Xv8Ql6m/dARPKptjEGjLgtEbakF+nNpugXqLBRmOdyMclwV2uykcDQMiK7P5I7I
u+1M1Qfdq0STVfsMDlzq2UbvnEauCQP51IhdKZ1MHe/gXEZCekv6tWzsGtWqX+TrWuf94qVRffmV
DoS46DNc9NyeeYnH2vMxsghhxuy2xSdty7fa4a7vdIVO/x5/7/dZYaDtJ6PQzGqg+HGZ5nkWblC6
w3JzCSLXGUObITN4dQXLNWQawoLPYHNc/h7vq/PTokLRqr+fXxSdunwxrDBuqW34mnnv42lWC/Lf
yCUszdavBy5DE6vlW8V0ufDzo+sFnhrYf1rgnoz8//j9L6J3gUbYTPQ8mqZPNpkxVGAflsosrrts
2jN91ELbj+OmbDBlMLy0kWUYFxCySHOc2hlxb1+E6oHkRTGoI0lCxm0RBPN/ELO+HhzM81J6BQoj
7bcl8gapStPoy5XVj+6j8RJJWfUQVWL+uzqksbG3LQCCPc+saIUegAzJcIPqj9mE0lLKT86Ab3Bq
p9/gb0M/rNNZMLKufCaned/uB7hK8ycruNDxQfpCs8XeyAc8c+ZtHKxevHPIYqSHkYdVFiZ4jkIt
im7Lky0+l44TSAA2qltr+XBfoFYg7iCi2/x+kFdjuhvdUoyhVOfMvLcrBJoxfFM5lHBQZIvHwrHk
PriWMgCtLbADhQOQAdaGQeEMDseJjbfgKYOKviRnUg/VmnzwEMghCQbTo9296Z/zUIEzizvwOKBw
S4eK7iyC3u3NKEYFir4g50Jjldj+3A4zkf6ThvFPilGfG4BZMdEo2i/PAHqsBUD+GsD1O1Trmdsp
XLXDA5e7XHVNpKYmf0fJzBIXPVTuQOw6eR5KzYzJsBmyIXoXOauvyx1mjcMXRQ3pWy10kgAC/MBm
emHtiZuQ7a8S9uH63kzzATJIeFY7LRgr0QpbWVOzHyXy2NyDz2Cv6mOH88HH/y/69yhx+ncQHqZK
p/DcwU6YGlWNCCDTBFFwfVz+VpTluNp2ZfxdHN+pA6LxisgQNX1cJTF5qgpSk6FU9pmfoYjqvpDk
+oXB1tYQyQ2D7o8pJip8kM2AHhjtZLUWkqdtCJANpA9wYseA5QXk5m6y0dSdnSaTneB6oMURp4LM
f6pEpJb7Ek0O3X5oHrqHWGGnpOGVLkuRwNwg4IjSRibOhikhkknPFGfVpJchihpZCHqH6z1/Vcnp
usNrQNkhJxRjUYa6wF69V5/fpVRtOJLpNJN/WuLq64F++qEnCc0m2Wz6EHo9wJnTYMJnP4bjE+C5
rHDzVTH067zh0Ut5eTPJD6r5eVRxJRMjktCwYCfnbQuEfNDQymY0B0q8uHmPO6Swm1osMhYVVRyG
/b189J5mMW2eYffdkRRtBSo+ERc9TSTYnDHnxG42OIHjAu6NO1GHsG+WiHPCEyCR4/8DOoGqOhXd
wknWZCoIxxvbUJgPlKicErCKS70O0huFUOCYJmiCLLBwDBT8Y1FhZbMJ9hCKnPcLu7NaUUDYWUQY
lYk6cK7b8JFjNmS7UzR365L+ck8iSPo72Yi6Ea+VsfgQ6vSIZygHlsG4HjPnvs93fsmohSTY/8t+
84kLO8m4LGbpRDCrfQ1TsespeQ07Tp/g/LruoyWqz3yVHrZIbyO6v+y1eWLIo/cgsVHBSTkFjvPV
Y2y6Y+kzwxsQ4eI9Yx+PJT3+negv5CvbpcvA4Tj15+wsqgjd2sszjXyRqu+7e0co4cJT6hohiyeP
luKe63h1nqBcWI7pK5PFrl/vXWmARypAVvQfKH3LgZ0Td8scabjwBRR8BuGDD81D5QiMHgUdox2C
Ofoj1zdj38cPzza6KaTYau1+yhpFHelJT1Q3lHueBurxPfMPpt/EpnkSuwTnnnILF4d3lstXDAk7
8+2y5X7MaJY0IdcT4Ld3D1xANuZHR9ecnu/v2qdAD/s+YZnGWmKDEc6MKLA5XghyhSDCncWQDAFZ
QH4J37VCVk5Ezwn59g8dx5gTDGqwQyjgW5vc5yMAS49QqdGYPymT8r9XE9vZytV3vvZtu9yA759V
LB+LWqnYOXwnIRTuAnR6dbcTQv4VTPNZug2W2JK14O/PXevgaLRkwNx2K/EkMLlXyFynw3urq0xC
Vz6HF/fDXqXjuIYEhyDHi1NGtiS1Gp3dXHoX6tIOc9/8+H6MUmgbQTnJQAtchbVVeWVQHvmawin+
SJ9Swekzs5MkFsx6aohj7XNk9dMBumABTt+y2vfBpAesCBbdlZc3m07WGjz/n54YDILD7p2XYwvP
brgnqny/MF/ElWmZ6o9bKKG807XAhC0C9tj6djrU/KqhOcXY7HiafJfm6mhe0elzUBMoslTZbe4/
G2D8t0l4Tw34GoMjRQRVSSliTiuNaXRmrZa+gxlI9mOwcloeGyECoSndYi2/OTLgb/IIorYQECiu
MYW2R+yS5dPn1b+tdKJ5avQawWybBM0vZIzalD5bI+cGtVqYuLmPkS4eIE1kiJGvcngpb/9KiiI3
GNyalEtKxmcIuRYuSVwntRedRmZT1HU4L7Cvgepz10SL//x8l+2BKsr/tdFP91SgsnPDvZf8hDA1
T43whzgtT2GQiSNieBjnnD2/mR4OciCrAFM7u2Pdt0E90cMFtmL1WYGulYeJsV1qEkB7HPF9s2UO
fhFg012lJCn0UuyHoKOjxjOVOX/2RdbmbZLn2NKoPRhD0X0XnhtRNy6qkt7YK+REQwG0atXiWOhc
KQaZ/XX5V95m501x+9lMZuf5nTXC9lOKOC8+v48vVLqhsBswo84Gq2zkqQMfpF21dD0dKVgMn8U6
tBzL6+Ns5PXCUVPe+kzmx2RrNoUJ9KaXdmUVcItITNsfIh+sKbH9Ex0NfWDnMDaJoevJdudNnnS7
o1HHIswrnXqiRJYQMEx0EJdK9WiheLdIkQTcMMNlAuEdx+KYvTtOdRGQe0/oDb3uWEQpYR+AHnM+
4hvlAiEBcHmmlhYEVBNHa0TBybrowu1iLpeBfJYhQkm7s8ou3IkO09NnxH4zRbqyOFEj2BkgNYY0
EmjscnA75rBwMr60b7OtuEaO0ELzZ1JxGBds214hf8W1FDUhQoqcxrtBP9lr2GC+UJs+BmgB6sEt
pK0zfpnGA8CiKs8t0WJ9DNQ9gyAPK7pQFrAAzVqy3IRY/2rK4H4as1Eo+2Zvqnj9fgwP5DoOV6Kw
4Hfu34GcJig6abCpcWjNCt3ASlx+plnxq97S1z8AiiMPjNU9/YLBs1UELohrA5l9YVzvpw51pmS1
J8gPEzTeKrS4Te87nFLJLvbKiCIO/Cy1LgRa/gG9GCLjSm/gQYdc2XzjCBVZMknpdAG27xGT9Urp
8Xtk1XzBHfrwNwM8hMC8eOR5kICL3aAc4IKBFOI+jzTN96Hk/ulYOXVIdArq6PrEvR40+IETOFzs
t16UkwclI74os+P02viSU7H3aWsd4aVA10V2AjunSXeWUXp9VBc78iFkuW/GfI/f/ZxASmD5YkyO
sihgY8HTqr7CqpbhdWaaukZhmzOCuCJ1v4xW5gk17rZbNvwYfQI/9QSlYlhoscD43xC0lBWTCwzB
xM9l98RPG8rGsuGcPRlrFe5UhtBLd2WawSpqi85mk4zx4Rk0mbDnhVjoNjQ749KWHBi2JGhJfaOL
ml4GbggmtMzSTmOt2RgXLgNbD/ju/Q2F5TsKJrpvyy/FprBJDSju87hd1186XghHVKAsu9rH/Evd
SaJPCM8kIry7dd9JQgRCe5fHzYQiZL0kLomj0swOmbBLJzS9ZfeG//1knPfZpOThW9UwKyMQzAPp
MYDYAlcX8NBTdwfw9fXrMRxhDy+r600+XDAcGJN+vR+LGe2VLvbJ574xnafoo6IkcshU4NZOaHEh
fUyltTGzgJ+JpBVR1/OBL8lhlw/lomePpkq2GxTdsxeN+ZRrt5LLaKbFZX3fg7J8VtFnxYUZcQw0
B31fZmumuRWq34oJEKFs5R4Xjz06Ex9Ie3emvqOnDaubTNtVgGSsZroTb3GJN6R16/n01Gw7oEjp
qnLNHrromSgLmOOfVCWW1FrliCPEnFo84ta9BWer/4Xzc0rkDGe7GJ9mCcNEs57L1N6NHaYR7aJj
T9Hs0u9UR+5T3I6hQyLeqXUS4D+FquPyDp/TkG6/lnendsobJeZ4y+4oyUaFYXcSux1i3LbpZfHN
MBfO7oMx+6r4fSP1Z+wcIAZ9Nj4ACTrf1X91lVisQPN2e+Qo2S+LpakeyAx9RB8F6EeD8cy1/siz
XtNl6Bfy/YgUsAFaAI7T+PCix2uCTst6JBdSn01A4DL0y6BBsKAFTxrBQUWhmOZAMFOUxcnzzY3j
7UXQJVbCVNYuf0RGHyhouWeU66exFI2ycVajCGlyzYuD4zPpt3/A3PPaO4mGcNTivQirdovaqNLS
yCpNRiyyWgK1b8lm4yOzSn8c0FsMYTBFPx8UjEKWqqgJnr8rtsCjy+UeoLgX8cORcm9U9DPMWoVe
NnqXdEcE4f35N2zsy/6cWvxpw1beuOakE5qEEeFDbu8CRJX3Hk3vrplS4R/i49qAyndR1xHIgQfV
cFL/45tCMVsC1GcuxHKdB7sDceJdlCkoizD6Xkdf8OwlKr7OzMFd4TJnoxqAEZm7gU8hqku1GDR4
DmAPwp4wwrWISywlSJibmaeGl5SQisSdrgBSdoJw4fx/ls0MjZ9SIugDgQqdszEOYXO4QjSeu2Pv
ze9jnkDQ8wG6d87+UaejxIO8Rm1dCj9vlyf7zDMyMiDBDBem8F0gQfL1R+ZVOK/7gqWhzpKmx2NO
vAsuWF8mdQoPIe7eYCgM+nO6lonl705X3zmeMIFjayOJOXmmEiqMucjblPKwwmvHInOxIUrrjtgY
FGWHrJi9ISUV6Zks8auVH4WCZCIAkWeKgT3xVSFn5Uhx5VBd9dA6zAeAFrhSQSXi3uZiJGwA2Mvt
ZYaz/jNerQjkA+nzuryUnPM8tfp7QGBgMJSqeIYh2kLM7MgYUB5HPrIzIdzcM2XxCON2TNJYbkR7
AU6lce3QskyGRtMumGkhU/4jp52qUgwcx9JX+1gkMG7G4aq6ajrdLl/8f04wnkcgT1p6BhIuNUDz
1BDwG4R5c+j1HTPYp0/hlC3uWOvye/DTrWrI9W8NT0CTeeB6mQDBVya1aSZ8GIPWXnD9FSD03+UU
QybHIXXUgsfx07JS/7us/H1TRGhP1OJsrZ8IntKsyjBFbmGR73oDReG64YjCcbP0xuCuFDPGf7mM
hU53SrhQzWhenMytOz5NY4u1xMCNBlVmbGO+pPOu+cEjyoI98ByxDuvOo7u6+JCQGUQQQ2Yoepvr
nWl9Beh3SQaO3fncvRCne8nlJe7jU5Z87su41CXZaQef6s+dNxEkIxc0FwthELFdkhYUYr87DPZF
SyK7oBdecL239ZLZueUKsyT8GLqsE8A7yJmcfJMxQChA+2mwJewqHEcuFIzzb72YBrbHb4/IFePf
pujREewUWb/WE+XOI4z9SXcL5YeMitTxMnK//34AFw+h/7QMskNJJxy27t1oiVyyg/eYqDal+Jds
h+FKyQUYALuw0RgppOMswhtNUxNy65zzHU/b3ZFBc7XwHZwRJH8WlyCZjQ+cx0flirCtP3KRHERa
J+AEte1gDjYGwkIIBPLmCe0b/YEGLsmHEXpDqNu8PDZx2NZ/ubJcEeoVnySWmkBcVfzGCXHPbaP7
uK2LpFXPhPTgBHUeYNU02+XNIn46i4sU5rybXA+vEFTWffpbEtVCkCVoEFFszuBA5dWBXQRtb5wt
8gRiTJeX2pjbMrf5yAKYjPwXSlpRRE7fDZ8UCxyd08NevNqeXpe4vWPsGKZg8EESyl6o41ap0ra4
Tv+aCN5swINGhYqape+HHF2S3duUXsIKB/I5deQvR7ka3LJWwhljuWCDiaWR4/AIBADn1ebmyJ3v
PhYEa+BkQmtf6dReNY7xU30eNoXmmRq8FKFWUZbrjF9ZpBmedwfxbnmYUiyjcfrwCc3qD+3xecDK
8O2vEKYzO0wKALSu+cj3XMjMoh1fLzsgwqoBHn5bJn1+RenmpGWpDvv3mnG68FOaQsB4PWNbYWxk
jEJZ7qCzvHBovueUMZqOjdH0fqWBsVagfwqbtvP0AHCVWStlnms1a6wSjHOKvmJVSjnsnlPBQ/xR
eHbRpxvtQHttXJyChzItsEmDa5Ih6rBVYoVceIAhYUcrHf9GmJ9RK+2R9fBsC3P3ufnRrfLKBD0J
75QM1X3OowvvHGEpv28VP0cmTUhum+reQGw0INunDcqO4+RDToXubmYCu6dVsu0aMSlvQz8i/OZB
R8r0Tm8YqExRF+HKyzc5ZxI5lL4U0Q==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
