// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2024.1 (lin64) Build 5076996 Wed May 22 18:36:09 MDT 2024
// Date        : Fri Jul 12 09:13:08 2024
// Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
// Command     : write_verilog -force -mode funcsim
//               /data/cms/ecal/fe/SAFE/firmware/SAFE.gen/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_sim_netlist.v
// Design      : gig_ethernet_pcs_pma_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7k70tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "gig_ethernet_pcs_pma_v16_2_18,Vivado 2024.1" *) 
(* NotValidForBitStream *)
module gig_ethernet_pcs_pma_0
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    resetdone,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output resetdone;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxp;
  wire rxuserclk2_out;
  wire rxuserclk_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txp;
  wire userclk2_out;
  wire userclk_out;
  wire [15:7]NLW_U0_status_vector_UNCONNECTED;

  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  (* EXAMPLE_SIMULATION = "0" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  gig_ethernet_pcs_pma_0_support U0
       (.configuration_vector({1'b0,configuration_vector[3:1],1'b0}),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg_out(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_locked_out(mmcm_locked_out),
        .pma_reset_out(pma_reset_out),
        .reset(reset),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxp(rxp),
        .rxuserclk2_out(rxuserclk2_out),
        .rxuserclk_out(rxuserclk_out),
        .signal_detect(signal_detect),
        .status_vector({NLW_U0_status_vector_UNCONNECTED[15:7],\^status_vector }),
        .txn(txn),
        .txp(txp),
        .userclk2_out(userclk2_out),
        .userclk_out(userclk_out));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  gig_ethernet_pcs_pma_0_GTWIZARD_init U0
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .data_in(data_in),
        .data_out(data_out),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .gtxe2_i_0(gtxe2_i_0),
        .gtxe2_i_1(gtxe2_i_1),
        .gtxe2_i_2(gtxe2_i_2),
        .gtxe2_i_3(gtxe2_i_3),
        .gtxe2_i_4(gtxe2_i_4),
        .gtxe2_i_5(gtxe2_i_5),
        .gtxe2_i_6(gtxe2_i_6),
        .gtxe2_i_7(gtxe2_i_7),
        .gtxe2_i_8(gtxe2_i_8),
        .gtxe2_i_9(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(reset_out),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_GT
   (gtxe2_i_0,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_1,
    txoutclk,
    gtxe2_i_2,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    independent_clock_bufg,
    cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_8,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_9,
    gtxe2_i_10,
    gtxe2_i_11);
  output gtxe2_i_0;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_1;
  output txoutclk;
  output gtxe2_i_2;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  output [1:0]gtxe2_i_7;
  input independent_clock_bufg;
  input cpll_pd0_i;
  input cpllreset_in;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_8;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input [1:0]gtxe2_i_11;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [1:0]gtxe2_i_11;
  wire gtxe2_i_2;
  wire [15:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire gtxe2_i_n_0;
  wire gtxe2_i_n_10;
  wire gtxe2_i_n_16;
  wire gtxe2_i_n_170;
  wire gtxe2_i_n_171;
  wire gtxe2_i_n_172;
  wire gtxe2_i_n_173;
  wire gtxe2_i_n_174;
  wire gtxe2_i_n_175;
  wire gtxe2_i_n_176;
  wire gtxe2_i_n_177;
  wire gtxe2_i_n_178;
  wire gtxe2_i_n_179;
  wire gtxe2_i_n_180;
  wire gtxe2_i_n_181;
  wire gtxe2_i_n_182;
  wire gtxe2_i_n_183;
  wire gtxe2_i_n_184;
  wire gtxe2_i_n_27;
  wire gtxe2_i_n_3;
  wire gtxe2_i_n_38;
  wire gtxe2_i_n_39;
  wire gtxe2_i_n_4;
  wire gtxe2_i_n_46;
  wire gtxe2_i_n_47;
  wire gtxe2_i_n_48;
  wire gtxe2_i_n_49;
  wire gtxe2_i_n_50;
  wire gtxe2_i_n_51;
  wire gtxe2_i_n_52;
  wire gtxe2_i_n_53;
  wire gtxe2_i_n_54;
  wire gtxe2_i_n_55;
  wire gtxe2_i_n_56;
  wire gtxe2_i_n_57;
  wire gtxe2_i_n_58;
  wire gtxe2_i_n_59;
  wire gtxe2_i_n_60;
  wire gtxe2_i_n_61;
  wire gtxe2_i_n_81;
  wire gtxe2_i_n_83;
  wire gtxe2_i_n_84;
  wire gtxe2_i_n_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED;
  wire NLW_gtxe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED;
  wire NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED;
  wire NLW_gtxe2_i_RXDATAVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXELECIDLE_UNCONNECTED;
  wire NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_RXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_RXRATEDONE_UNCONNECTED;
  wire NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire NLW_gtxe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED;
  wire NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENN_UNCONNECTED;
  wire NLW_gtxe2_i_TXQPISENP_UNCONNECTED;
  wire NLW_gtxe2_i_TXRATEDONE_UNCONNECTED;
  wire [15:0]NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXCHARISK_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXCHBONDO_UNCONNECTED;
  wire [63:16]NLW_gtxe2_i_RXDATA_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXHEADER_UNCONNECTED;
  wire [7:2]NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [2:0]NLW_gtxe2_i_RXSTATUS_UNCONNECTED;
  wire [9:0]NLW_gtxe2_i_TSTOUT_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_CHANNEL #(
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b0001111111),
    .ALIGN_COMMA_WORD(2),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_CORRECT_USE("TRUE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(36),
    .CLK_COR_MIN_LAT(33),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0110111100),
    .CLK_COR_SEQ_1_2(10'b0001010000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0110111100),
    .CLK_COR_SEQ_2_2(10'b0010110101),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("TRUE"),
    .CLK_COR_SEQ_LEN(2),
    .CPLL_CFG(24'hBC07DC),
    .CPLL_FBDIV(4),
    .CPLL_FBDIV_45(5),
    .CPLL_INIT_CFG(24'h00001E),
    .CPLL_LOCK_CFG(16'h01E8),
    .CPLL_REFCLK_DIV(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("TRUE"),
    .ES_HORZ_OFFSET(12'h000),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CPLLLOCKDETCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000000),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h19),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_RSV(32'h00018480),
    .PMA_RSV2(16'h2050),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(32'h00000000),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FULL"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(8),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(72'h03000023FF10100020),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b010101),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDFELPMRESET_TIME(7'b0001111),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPM_HF_CFG(14'b00000011110000),
    .RXLPM_LF_CFG(14'b00000011110000),
    .RXOOB_CFG(7'b0000110),
    .RXOUT_DIV(4),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'h000000),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RX_BIAS_CFG(12'b000000000100),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(5),
    .RX_CLKMUX_PD(1'b1),
    .RX_CM_SEL(2'b11),
    .RX_CM_TRIM(3'b010),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(12'b000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DFE_GAIN_CFG(23'h020FEA),
    .RX_DFE_H2_CFG(12'b000000000000),
    .RX_DFE_H3_CFG(12'b000001000000),
    .RX_DFE_H4_CFG(11'b00011110000),
    .RX_DFE_H5_CFG(11'b00011100000),
    .RX_DFE_KL_CFG(13'b0000011111110),
    .RX_DFE_KL_CFG2(32'h301148AC),
    .RX_DFE_LPM_CFG(16'h0904),
    .RX_DFE_LPM_HOLD_DURING_EIDLE(1'b0),
    .RX_DFE_UT_CFG(17'b10001111000000000),
    .RX_DFE_VP_CFG(17'b00011111100000011),
    .RX_DFE_XYD_CFG(13'b0000000000000),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_INT_DATAWIDTH(0),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b100),
    .SATA_CPLL_CFG("VCO_3000MHZ"),
    .SATA_EIDLE_VAL(3'b100),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_CPLLREFCLK_SEL(3'b001),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("TRUE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("4.0"),
    .TERM_RCAL_CFG(5'b10000),
    .TERM_RCAL_OVRD(1'b0),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOUT_DIV(4),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPMARESET_TIME(5'b00001),
    .TX_CLK25_DIV(5),
    .TX_CLKMUX_PD(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DEEMPH0(5'b00000),
    .TX_DEEMPH1(5'b00000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_INT_DATAWIDTH(0),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_QPI_STATUS_EN(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXOUT"),
    .UCODEER_CLR(1'b0)) 
    gtxe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD({1'b0,1'b0,1'b0,1'b0}),
        .CPLLFBCLKLOST(gtxe2_i_n_0),
        .CPLLLOCK(gtxe2_i_0),
        .CPLLLOCKDETCLK(independent_clock_bufg),
        .CPLLLOCKEN(1'b1),
        .CPLLPD(cpll_pd0_i),
        .CPLLREFCLKLOST(gt0_cpllrefclklost_i),
        .CPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .CPLLRESET(cpllreset_in),
        .DMONITOROUT({gtxe2_i_n_177,gtxe2_i_n_178,gtxe2_i_n_179,gtxe2_i_n_180,gtxe2_i_n_181,gtxe2_i_n_182,gtxe2_i_n_183,gtxe2_i_n_184}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(gtrefclk_bufg),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO({gtxe2_i_n_46,gtxe2_i_n_47,gtxe2_i_n_48,gtxe2_i_n_49,gtxe2_i_n_50,gtxe2_i_n_51,gtxe2_i_n_52,gtxe2_i_n_53,gtxe2_i_n_54,gtxe2_i_n_55,gtxe2_i_n_56,gtxe2_i_n_57,gtxe2_i_n_58,gtxe2_i_n_59,gtxe2_i_n_60,gtxe2_i_n_61}),
        .DRPEN(1'b0),
        .DRPRDY(gtxe2_i_n_3),
        .DRPWE(1'b0),
        .EYESCANDATAERROR(gtxe2_i_n_4),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(1'b0),
        .EYESCANTRIGGER(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTREFCLKMONITOR(NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(SR),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .GTTXRESET(gt0_gttxreset_in0_out),
        .GTXRXN(rxn),
        .GTXRXP(rxp),
        .GTXTXN(txn),
        .GTXTXP(txp),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtxe2_i_PHYSTATUS_UNCONNECTED),
        .PMARSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDIN2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLCLK(gt0_qplloutclk_out),
        .QPLLREFCLK(gt0_qplloutrefclk_out),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS({RXBUFSTATUS,gtxe2_i_n_83,gtxe2_i_n_84}),
        .RXBYTEISALIGNED(gtxe2_i_n_9),
        .RXBYTEREALIGN(gtxe2_i_n_10),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED),
        .RXCHANISALIGNED(NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED),
        .RXCHANREALIGN(NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED),
        .RXCHARISCOMMA({NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED[7:2],gtxe2_i_4}),
        .RXCHARISK({NLW_gtxe2_i_RXCHARISK_UNCONNECTED[7:2],gtxe2_i_5}),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO(NLW_gtxe2_i_RXCHBONDO_UNCONNECTED[4:0]),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(D),
        .RXCOMINITDET(NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED),
        .RXCOMMADET(gtxe2_i_n_16),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED),
        .RXCOMWAKEDET(NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED),
        .RXDATA({NLW_gtxe2_i_RXDATA_UNCONNECTED[63:16],gtxe2_i_3}),
        .RXDATAVALID(NLW_gtxe2_i_RXDATAVALID_UNCONNECTED),
        .RXDDIEN(1'b0),
        .RXDFEAGCHOLD(1'b0),
        .RXDFEAGCOVRDEN(1'b0),
        .RXDFECM1EN(1'b0),
        .RXDFELFHOLD(1'b0),
        .RXDFELFOVRDEN(1'b0),
        .RXDFELPMRESET(1'b0),
        .RXDFETAP2HOLD(1'b0),
        .RXDFETAP2OVRDEN(1'b0),
        .RXDFETAP3HOLD(1'b0),
        .RXDFETAP3OVRDEN(1'b0),
        .RXDFETAP4HOLD(1'b0),
        .RXDFETAP4OVRDEN(1'b0),
        .RXDFETAP5HOLD(1'b0),
        .RXDFETAP5OVRDEN(1'b0),
        .RXDFEUTHOLD(1'b0),
        .RXDFEUTOVRDEN(1'b0),
        .RXDFEVPHOLD(1'b0),
        .RXDFEVPOVRDEN(1'b0),
        .RXDFEVSEN(1'b0),
        .RXDFEXYDEN(1'b1),
        .RXDFEXYDHOLD(1'b0),
        .RXDFEXYDOVRDEN(1'b0),
        .RXDISPERR({NLW_gtxe2_i_RXDISPERR_UNCONNECTED[7:2],gtxe2_i_6}),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(NLW_gtxe2_i_RXELECIDLE_UNCONNECTED),
        .RXELECIDLEMODE({1'b1,1'b1}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtxe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMEN(1'b1),
        .RXLPMHFHOLD(1'b0),
        .RXLPMHFOVRDEN(1'b0),
        .RXLPMLFHOLD(1'b0),
        .RXLPMLFKLOVRDEN(1'b0),
        .RXMCOMMAALIGNEN(reset_out),
        .RXMONITOROUT({gtxe2_i_n_170,gtxe2_i_n_171,gtxe2_i_n_172,gtxe2_i_n_173,gtxe2_i_n_174,gtxe2_i_n_175,gtxe2_i_n_176}),
        .RXMONITORSEL({1'b0,1'b0}),
        .RXNOTINTABLE({NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED[7:2],gtxe2_i_7}),
        .RXOOBRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(rxoutclk),
        .RXOUTCLKFABRIC(NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(reset_out),
        .RXPCSRESET(reset),
        .RXPD({RXPD,RXPD}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(1'b0),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(gtxe2_i_n_27),
        .RXPRBSSEL({1'b0,1'b0,1'b0}),
        .RXQPIEN(1'b0),
        .RXQPISENN(NLW_gtxe2_i_RXQPISENN_UNCONNECTED),
        .RXQPISENP(NLW_gtxe2_i_RXQPISENP_UNCONNECTED),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(NLW_gtxe2_i_RXRATEDONE_UNCONNECTED),
        .RXRESETDONE(gtxe2_i_1),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED),
        .RXSTATUS(NLW_gtxe2_i_RXSTATUS_UNCONNECTED[2:0]),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(gt0_rxuserrdy_t),
        .RXUSRCLK(gtxe2_i_8),
        .RXUSRCLK2(gtxe2_i_8),
        .RXVALID(NLW_gtxe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TSTOUT(NLW_gtxe2_i_TSTOUT_UNCONNECTED[9:0]),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS({TXBUFSTATUS,gtxe2_i_n_81}),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_9}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_10}),
        .TXCHARISK({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gtxe2_i_11}),
        .TXCOMFINISH(NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED),
        .TXCOMINIT(1'b0),
        .TXCOMSAS(1'b0),
        .TXCOMWAKE(1'b0),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(TXPD),
        .TXGEARBOXREADY(NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(txoutclk),
        .TXOUTCLKFABRIC(gtxe2_i_n_38),
        .TXOUTCLKPCS(gtxe2_i_n_39),
        .TXOUTCLKSEL({1'b1,1'b0,1'b0}),
        .TXPCSRESET(1'b0),
        .TXPD({TXPD,TXPD}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED),
        .TXPHOVRDEN(1'b0),
        .TXPISOPD(1'b0),
        .TXPMARESET(1'b0),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXQPIBIASEN(1'b0),
        .TXQPISENN(NLW_gtxe2_i_TXQPISENN_UNCONNECTED),
        .TXQPISENP(NLW_gtxe2_i_TXQPISENP_UNCONNECTED),
        .TXQPISTRONGPDOWN(1'b0),
        .TXQPIWEAKPUP(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtxe2_i_TXRATEDONE_UNCONNECTED),
        .TXRESETDONE(gtxe2_i_2),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(gt0_txuserrdy_t),
        .TXUSRCLK(gtxe2_i_8),
        .TXUSRCLK2(gtxe2_i_8));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_init
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i,
    gtxe2_i_0,
    gtxe2_i_1,
    gtxe2_i_2,
    gtxe2_i_3,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gtxe2_i_4,
    TXPD,
    RXPD,
    Q,
    gtxe2_i_5,
    gtxe2_i_6,
    gtxe2_i_7,
    out,
    gtxe2_i_8,
    gtxe2_i_9,
    data_sync_reg1,
    data_out);
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i;
  output [1:0]gtxe2_i_0;
  output [1:0]gtxe2_i_1;
  output [1:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gtxe2_i_4;
  input [0:0]TXPD;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_5;
  input [1:0]gtxe2_i_6;
  input [1:0]gtxe2_i_7;
  input [0:0]out;
  input gtxe2_i_8;
  input gtxe2_i_9;
  input data_sync_reg1;
  input data_out;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire [13:1]data0;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gtrxreset_in1_out;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire [13:0]gt0_rx_cdrlock_counter;
  wire gt0_rx_cdrlock_counter0_carry__0_n_0;
  wire gt0_rx_cdrlock_counter0_carry__0_n_1;
  wire gt0_rx_cdrlock_counter0_carry__0_n_2;
  wire gt0_rx_cdrlock_counter0_carry__0_n_3;
  wire gt0_rx_cdrlock_counter0_carry__1_n_0;
  wire gt0_rx_cdrlock_counter0_carry__1_n_1;
  wire gt0_rx_cdrlock_counter0_carry__1_n_2;
  wire gt0_rx_cdrlock_counter0_carry__1_n_3;
  wire gt0_rx_cdrlock_counter0_carry_n_0;
  wire gt0_rx_cdrlock_counter0_carry_n_1;
  wire gt0_rx_cdrlock_counter0_carry_n_2;
  wire gt0_rx_cdrlock_counter0_carry_n_3;
  wire \gt0_rx_cdrlock_counter[0]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_2_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_3_n_0 ;
  wire \gt0_rx_cdrlock_counter[13]_i_4_n_0 ;
  wire [13:0]gt0_rx_cdrlock_counter_0;
  wire gt0_rx_cdrlocked_i_1_n_0;
  wire gt0_rx_cdrlocked_reg_n_0;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_i_n_0;
  wire gtwizard_i_n_5;
  wire gtwizard_i_n_7;
  wire [15:0]gtxe2_i;
  wire [1:0]gtxe2_i_0;
  wire [1:0]gtxe2_i_1;
  wire [1:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire [1:0]gtxe2_i_7;
  wire gtxe2_i_8;
  wire gtxe2_i_9;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire reset;
  wire reset_out;
  wire rx_fsm_reset_done_int_reg;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;
  wire [3:0]NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED;
  wire [3:1]NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry
       (.CI(1'b0),
        .CO({gt0_rx_cdrlock_counter0_carry_n_0,gt0_rx_cdrlock_counter0_carry_n_1,gt0_rx_cdrlock_counter0_carry_n_2,gt0_rx_cdrlock_counter0_carry_n_3}),
        .CYINIT(gt0_rx_cdrlock_counter[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(gt0_rx_cdrlock_counter[4:1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__0
       (.CI(gt0_rx_cdrlock_counter0_carry_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__0_n_0,gt0_rx_cdrlock_counter0_carry__0_n_1,gt0_rx_cdrlock_counter0_carry__0_n_2,gt0_rx_cdrlock_counter0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(gt0_rx_cdrlock_counter[8:5]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__1
       (.CI(gt0_rx_cdrlock_counter0_carry__0_n_0),
        .CO({gt0_rx_cdrlock_counter0_carry__1_n_0,gt0_rx_cdrlock_counter0_carry__1_n_1,gt0_rx_cdrlock_counter0_carry__1_n_2,gt0_rx_cdrlock_counter0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(gt0_rx_cdrlock_counter[12:9]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 gt0_rx_cdrlock_counter0_carry__2
       (.CI(gt0_rx_cdrlock_counter0_carry__1_n_0),
        .CO(NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED[3:1],data0[13]}),
        .S({1'b0,1'b0,1'b0,gt0_rx_cdrlock_counter[13]}));
  LUT2 #(
    .INIT(4'h2)) 
    \gt0_rx_cdrlock_counter[0]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ),
        .I1(gt0_rx_cdrlock_counter[0]),
        .O(gt0_rx_cdrlock_counter_0[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \gt0_rx_cdrlock_counter[0]_i_2 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I1(gt0_rx_cdrlock_counter[4]),
        .I2(gt0_rx_cdrlock_counter[5]),
        .I3(gt0_rx_cdrlock_counter[7]),
        .I4(gt0_rx_cdrlock_counter[6]),
        .I5(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .O(\gt0_rx_cdrlock_counter[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[10]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[10]),
        .O(gt0_rx_cdrlock_counter_0[10]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[11]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[11]),
        .O(gt0_rx_cdrlock_counter_0[11]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[12]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[12]),
        .O(gt0_rx_cdrlock_counter_0[12]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[13]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[13]),
        .O(gt0_rx_cdrlock_counter_0[13]));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \gt0_rx_cdrlock_counter[13]_i_2 
       (.I0(gt0_rx_cdrlock_counter[1]),
        .I1(gt0_rx_cdrlock_counter[12]),
        .I2(gt0_rx_cdrlock_counter[13]),
        .I3(gt0_rx_cdrlock_counter[3]),
        .I4(gt0_rx_cdrlock_counter[2]),
        .O(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \gt0_rx_cdrlock_counter[13]_i_3 
       (.I0(gt0_rx_cdrlock_counter[4]),
        .I1(gt0_rx_cdrlock_counter[5]),
        .I2(gt0_rx_cdrlock_counter[7]),
        .I3(gt0_rx_cdrlock_counter[6]),
        .O(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \gt0_rx_cdrlock_counter[13]_i_4 
       (.I0(gt0_rx_cdrlock_counter[9]),
        .I1(gt0_rx_cdrlock_counter[8]),
        .I2(gt0_rx_cdrlock_counter[10]),
        .I3(gt0_rx_cdrlock_counter[11]),
        .O(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[1]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[1]),
        .O(gt0_rx_cdrlock_counter_0[1]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[2]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[2]),
        .O(gt0_rx_cdrlock_counter_0[2]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[3]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[3]),
        .O(gt0_rx_cdrlock_counter_0[3]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[4]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[4]),
        .O(gt0_rx_cdrlock_counter_0[4]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[5]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[5]),
        .O(gt0_rx_cdrlock_counter_0[5]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[6]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[6]),
        .O(gt0_rx_cdrlock_counter_0[6]));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \gt0_rx_cdrlock_counter[7]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[7]),
        .O(gt0_rx_cdrlock_counter_0[7]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[8]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[8]),
        .O(gt0_rx_cdrlock_counter_0[8]));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    \gt0_rx_cdrlock_counter[9]_i_1 
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(data0[9]),
        .O(gt0_rx_cdrlock_counter_0[9]));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[0]),
        .Q(gt0_rx_cdrlock_counter[0]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[10]),
        .Q(gt0_rx_cdrlock_counter[10]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[11]),
        .Q(gt0_rx_cdrlock_counter[11]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[12]),
        .Q(gt0_rx_cdrlock_counter[12]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[13]),
        .Q(gt0_rx_cdrlock_counter[13]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[1]),
        .Q(gt0_rx_cdrlock_counter[1]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[2]),
        .Q(gt0_rx_cdrlock_counter[2]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[3]),
        .Q(gt0_rx_cdrlock_counter[3]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[4]),
        .Q(gt0_rx_cdrlock_counter[4]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[5]),
        .Q(gt0_rx_cdrlock_counter[5]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[6]),
        .Q(gt0_rx_cdrlock_counter[6]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[7]),
        .Q(gt0_rx_cdrlock_counter[7]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[8]),
        .Q(gt0_rx_cdrlock_counter[8]),
        .R(gt0_gtrxreset_in1_out));
  FDRE #(
    .INIT(1'b0)) 
    \gt0_rx_cdrlock_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[9]),
        .Q(gt0_rx_cdrlock_counter[9]),
        .R(gt0_gtrxreset_in1_out));
  LUT5 #(
    .INIT(32'hFFFF0001)) 
    gt0_rx_cdrlocked_i_1
       (.I0(\gt0_rx_cdrlock_counter[13]_i_2_n_0 ),
        .I1(\gt0_rx_cdrlock_counter[13]_i_3_n_0 ),
        .I2(\gt0_rx_cdrlock_counter[13]_i_4_n_0 ),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(gt0_rx_cdrlocked_reg_n_0),
        .O(gt0_rx_cdrlocked_i_1_n_0));
  FDRE gt0_rx_cdrlocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gt0_rx_cdrlocked_i_1_n_0),
        .Q(gt0_rx_cdrlocked_reg_n_0),
        .R(gt0_gtrxreset_in1_out));
  gig_ethernet_pcs_pma_0_RX_STARTUP_FSM gt0_rxresetfsm_i
       (.\FSM_sequential_rx_state_reg[0]_0 (gt0_rx_cdrlocked_reg_n_0),
        .SR(gt0_gtrxreset_in1_out),
        .data_in(rx_fsm_reset_done_int_reg),
        .data_out(data_out),
        .data_sync_reg1(gtwizard_i_n_5),
        .data_sync_reg1_0(data_sync_reg1),
        .data_sync_reg1_1(gtwizard_i_n_0),
        .data_sync_reg6(gtxe2_i_4),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gtxe2_i(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .out(out));
  gig_ethernet_pcs_pma_0_TX_STARTUP_FSM gt0_txresetfsm_i
       (.data_in(data_in),
        .data_sync_reg1(gtxe2_i_4),
        .data_sync_reg1_0(gtwizard_i_n_7),
        .data_sync_reg1_1(data_sync_reg1),
        .data_sync_reg1_2(gtwizard_i_n_0),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtxe2_i(gtxe2_i_9),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out));
  gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt gtwizard_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(gt0_gtrxreset_in1_out),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtwizard_i_n_0),
        .gtxe2_i_0(gtwizard_i_n_5),
        .gtxe2_i_1(gtwizard_i_n_7),
        .gtxe2_i_10(gtxe2_i_7),
        .gtxe2_i_2(gtxe2_i),
        .gtxe2_i_3(gtxe2_i_0),
        .gtxe2_i_4(gtxe2_i_1),
        .gtxe2_i_5(gtxe2_i_2),
        .gtxe2_i_6(gtxe2_i_3),
        .gtxe2_i_7(gtxe2_i_4),
        .gtxe2_i_8(gtxe2_i_5),
        .gtxe2_i_9(gtxe2_i_6),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
   (gtxe2_i,
    gt0_cpllrefclklost_i,
    txn,
    txp,
    rxoutclk,
    gtxe2_i_0,
    txoutclk,
    gtxe2_i_1,
    D,
    TXBUFSTATUS,
    RXBUFSTATUS,
    gtxe2_i_2,
    gtxe2_i_3,
    gtxe2_i_4,
    gtxe2_i_5,
    gtxe2_i_6,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    SR,
    gt0_gttxreset_in0_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    reset_out,
    reset,
    gt0_rxuserrdy_t,
    gtxe2_i_7,
    TXPD,
    gt0_txuserrdy_t,
    RXPD,
    Q,
    gtxe2_i_8,
    gtxe2_i_9,
    gtxe2_i_10,
    gt0_cpllreset_t);
  output gtxe2_i;
  output gt0_cpllrefclklost_i;
  output txn;
  output txp;
  output rxoutclk;
  output gtxe2_i_0;
  output txoutclk;
  output gtxe2_i_1;
  output [1:0]D;
  output [0:0]TXBUFSTATUS;
  output [0:0]RXBUFSTATUS;
  output [15:0]gtxe2_i_2;
  output [1:0]gtxe2_i_3;
  output [1:0]gtxe2_i_4;
  output [1:0]gtxe2_i_5;
  output [1:0]gtxe2_i_6;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input [0:0]SR;
  input gt0_gttxreset_in0_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input reset_out;
  input reset;
  input gt0_rxuserrdy_t;
  input gtxe2_i_7;
  input [0:0]TXPD;
  input gt0_txuserrdy_t;
  input [0:0]RXPD;
  input [15:0]Q;
  input [1:0]gtxe2_i_8;
  input [1:0]gtxe2_i_9;
  input [1:0]gtxe2_i_10;
  input gt0_cpllreset_t;

  wire [1:0]D;
  wire [15:0]Q;
  wire [0:0]RXBUFSTATUS;
  wire [0:0]RXPD;
  wire [0:0]SR;
  wire [0:0]TXBUFSTATUS;
  wire [0:0]TXPD;
  wire cpll_pd0_i;
  wire cpllreset_in;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gt0_rxuserrdy_t;
  wire gt0_txuserrdy_t;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire gtxe2_i_0;
  wire gtxe2_i_1;
  wire [1:0]gtxe2_i_10;
  wire [15:0]gtxe2_i_2;
  wire [1:0]gtxe2_i_3;
  wire [1:0]gtxe2_i_4;
  wire [1:0]gtxe2_i_5;
  wire [1:0]gtxe2_i_6;
  wire gtxe2_i_7;
  wire [1:0]gtxe2_i_8;
  wire [1:0]gtxe2_i_9;
  wire independent_clock_bufg;
  wire reset;
  wire reset_out;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire txn;
  wire txoutclk;
  wire txp;

  gig_ethernet_pcs_pma_0_cpll_railing cpll_railing0_i
       (.cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllreset_t(gt0_cpllreset_t),
        .gtrefclk_bufg(gtrefclk_bufg));
  gig_ethernet_pcs_pma_0_GTWIZARD_GT gt0_GTWIZARD_i
       (.D(D),
        .Q(Q),
        .RXBUFSTATUS(RXBUFSTATUS),
        .RXPD(RXPD),
        .SR(SR),
        .TXBUFSTATUS(TXBUFSTATUS),
        .TXPD(TXPD),
        .cpll_pd0_i(cpll_pd0_i),
        .cpllreset_in(cpllreset_in),
        .gt0_cpllrefclklost_i(gt0_cpllrefclklost_i),
        .gt0_gttxreset_in0_out(gt0_gttxreset_in0_out),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gt0_rxuserrdy_t(gt0_rxuserrdy_t),
        .gt0_txuserrdy_t(gt0_txuserrdy_t),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i_0(gtxe2_i),
        .gtxe2_i_1(gtxe2_i_0),
        .gtxe2_i_10(gtxe2_i_9),
        .gtxe2_i_11(gtxe2_i_10),
        .gtxe2_i_2(gtxe2_i_1),
        .gtxe2_i_3(gtxe2_i_2),
        .gtxe2_i_4(gtxe2_i_3),
        .gtxe2_i_5(gtxe2_i_4),
        .gtxe2_i_6(gtxe2_i_5),
        .gtxe2_i_7(gtxe2_i_6),
        .gtxe2_i_8(gtxe2_i_7),
        .gtxe2_i_9(gtxe2_i_8),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset),
        .reset_out(reset_out),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
   (data_in,
    gt0_rxuserrdy_t,
    SR,
    independent_clock_bufg,
    data_sync_reg6,
    out,
    gtxe2_i,
    \FSM_sequential_rx_state_reg[0]_0 ,
    data_sync_reg1,
    data_sync_reg1_0,
    data_out,
    data_sync_reg1_1);
  output data_in;
  output gt0_rxuserrdy_t;
  output [0:0]SR;
  input independent_clock_bufg;
  input data_sync_reg6;
  input [0:0]out;
  input gtxe2_i;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input data_sync_reg1;
  input data_sync_reg1_0;
  input data_out;
  input data_sync_reg1_1;

  wire \FSM_sequential_rx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_10_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_9_n_0 ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire GTRXRESET;
  wire RXUSERRDY_i_1_n_0;
  wire [0:0]SR;
  wire check_tlock_max_i_1_n_0;
  wire check_tlock_max_reg_n_0;
  wire data_in;
  wire data_out;
  wire data_out_0;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg6;
  wire gt0_rxuserrdy_t;
  wire gtrxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_1__0_n_0 ;
  wire \init_wait_count[6]_i_3__0_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1__0_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1__0_n_0 ;
  wire \mmcm_lock_count[3]_i_1__0_n_0 ;
  wire \mmcm_lock_count[4]_i_1__0_n_0 ;
  wire \mmcm_lock_count[5]_i_1__0_n_0 ;
  wire \mmcm_lock_count[6]_i_1__0_n_0 ;
  wire \mmcm_lock_count[7]_i_2__0_n_0 ;
  wire \mmcm_lock_count[7]_i_3__0_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2__0_n_0;
  wire [0:0]out;
  wire [6:1]p_0_in__2;
  wire [1:0]p_0_in__3;
  wire reset_time_out_i_3_n_0;
  wire reset_time_out_i_4_n_0;
  wire reset_time_out_reg_n_0;
  wire run_phase_alignment_int_i_1__0_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3_reg_n_0;
  wire rx_fsm_reset_done_int_i_5_n_0;
  wire rx_fsm_reset_done_int_i_6_n_0;
  wire rx_fsm_reset_done_int_s2;
  wire rx_fsm_reset_done_int_s3;
  wire [3:0]rx_state;
  wire [3:0]rx_state__0;
  wire rxresetdone_s2;
  wire rxresetdone_s3;
  wire sync_cplllock_n_0;
  wire sync_data_valid_n_0;
  wire sync_data_valid_n_1;
  wire sync_data_valid_n_5;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_100us_i_1_n_0;
  wire time_out_100us_i_2_n_0;
  wire time_out_100us_i_3_n_0;
  wire time_out_100us_reg_n_0;
  wire time_out_1us_i_1_n_0;
  wire time_out_1us_i_2_n_0;
  wire time_out_1us_i_3_n_0;
  wire time_out_1us_reg_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2_n_0;
  wire time_out_2ms_i_3__0_n_0;
  wire time_out_2ms_i_4_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2__0_n_0 ;
  wire \time_out_counter_reg[0]_i_2__0_n_1 ;
  wire \time_out_counter_reg[0]_i_2__0_n_2 ;
  wire \time_out_counter_reg[0]_i_2__0_n_3 ;
  wire \time_out_counter_reg[0]_i_2__0_n_4 ;
  wire \time_out_counter_reg[0]_i_2__0_n_5 ;
  wire \time_out_counter_reg[0]_i_2__0_n_6 ;
  wire \time_out_counter_reg[0]_i_2__0_n_7 ;
  wire \time_out_counter_reg[12]_i_1__0_n_0 ;
  wire \time_out_counter_reg[12]_i_1__0_n_1 ;
  wire \time_out_counter_reg[12]_i_1__0_n_2 ;
  wire \time_out_counter_reg[12]_i_1__0_n_3 ;
  wire \time_out_counter_reg[12]_i_1__0_n_4 ;
  wire \time_out_counter_reg[12]_i_1__0_n_5 ;
  wire \time_out_counter_reg[12]_i_1__0_n_6 ;
  wire \time_out_counter_reg[12]_i_1__0_n_7 ;
  wire \time_out_counter_reg[16]_i_1__0_n_2 ;
  wire \time_out_counter_reg[16]_i_1__0_n_3 ;
  wire \time_out_counter_reg[16]_i_1__0_n_5 ;
  wire \time_out_counter_reg[16]_i_1__0_n_6 ;
  wire \time_out_counter_reg[16]_i_1__0_n_7 ;
  wire \time_out_counter_reg[4]_i_1__0_n_0 ;
  wire \time_out_counter_reg[4]_i_1__0_n_1 ;
  wire \time_out_counter_reg[4]_i_1__0_n_2 ;
  wire \time_out_counter_reg[4]_i_1__0_n_3 ;
  wire \time_out_counter_reg[4]_i_1__0_n_4 ;
  wire \time_out_counter_reg[4]_i_1__0_n_5 ;
  wire \time_out_counter_reg[4]_i_1__0_n_6 ;
  wire \time_out_counter_reg[4]_i_1__0_n_7 ;
  wire \time_out_counter_reg[8]_i_1__0_n_0 ;
  wire \time_out_counter_reg[8]_i_1__0_n_1 ;
  wire \time_out_counter_reg[8]_i_1__0_n_2 ;
  wire \time_out_counter_reg[8]_i_1__0_n_3 ;
  wire \time_out_counter_reg[8]_i_1__0_n_4 ;
  wire \time_out_counter_reg[8]_i_1__0_n_5 ;
  wire \time_out_counter_reg[8]_i_1__0_n_6 ;
  wire \time_out_counter_reg[8]_i_1__0_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2__0_n_0;
  wire time_out_wait_bypass_i_3__0_n_0;
  wire time_out_wait_bypass_i_4__0_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max;
  wire time_tlock_max1;
  wire time_tlock_max1_carry__0_i_1_n_0;
  wire time_tlock_max1_carry__0_i_2_n_0;
  wire time_tlock_max1_carry__0_i_3_n_0;
  wire time_tlock_max1_carry__0_i_4_n_0;
  wire time_tlock_max1_carry__0_i_5_n_0;
  wire time_tlock_max1_carry__0_i_6_n_0;
  wire time_tlock_max1_carry__0_n_0;
  wire time_tlock_max1_carry__0_n_1;
  wire time_tlock_max1_carry__0_n_2;
  wire time_tlock_max1_carry__0_n_3;
  wire time_tlock_max1_carry__1_i_1_n_0;
  wire time_tlock_max1_carry__1_i_2_n_0;
  wire time_tlock_max1_carry__1_i_3_n_0;
  wire time_tlock_max1_carry__1_n_3;
  wire time_tlock_max1_carry_i_1_n_0;
  wire time_tlock_max1_carry_i_2_n_0;
  wire time_tlock_max1_carry_i_3_n_0;
  wire time_tlock_max1_carry_i_4_n_0;
  wire time_tlock_max1_carry_i_5_n_0;
  wire time_tlock_max1_carry_i_6_n_0;
  wire time_tlock_max1_carry_i_7_n_0;
  wire time_tlock_max1_carry_i_8_n_0;
  wire time_tlock_max1_carry_n_0;
  wire time_tlock_max1_carry_n_1;
  wire time_tlock_max1_carry_n_2;
  wire time_tlock_max1_carry_n_3;
  wire time_tlock_max_i_1_n_0;
  wire \wait_bypass_count[0]_i_1__0_n_0 ;
  wire \wait_bypass_count[0]_i_2__0_n_0 ;
  wire \wait_bypass_count[0]_i_4_n_0 ;
  wire [12:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3__0_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3__0_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1__0_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1__0_n_7 ;
  wire [0:0]wait_time_cnt0__0;
  wire \wait_time_cnt[1]_i_1__0_n_0 ;
  wire \wait_time_cnt[2]_i_1__0_n_0 ;
  wire \wait_time_cnt[3]_i_1__0_n_0 ;
  wire \wait_time_cnt[4]_i_1__0_n_0 ;
  wire \wait_time_cnt[5]_i_1__0_n_0 ;
  wire \wait_time_cnt[6]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_2__0_n_0 ;
  wire \wait_time_cnt[6]_i_3__0_n_0 ;
  wire \wait_time_cnt[6]_i_4__0_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED ;
  wire [3:0]NLW_time_tlock_max1_carry_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__0_O_UNCONNECTED;
  wire [3:2]NLW_time_tlock_max1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max1_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h2222AAAA00000C00)) 
    \FSM_sequential_rx_state[0]_i_2 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[3]),
        .I3(time_tlock_max),
        .I4(reset_time_out_reg_n_0),
        .I5(rx_state[1]),
        .O(\FSM_sequential_rx_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000AABF000F0000)) 
    \FSM_sequential_rx_state[1]_i_3 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .I2(rx_state[2]),
        .I3(rx_state[3]),
        .I4(rx_state[1]),
        .I5(rx_state[0]),
        .O(\FSM_sequential_rx_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050FF2200)) 
    \FSM_sequential_rx_state[2]_i_1 
       (.I0(rx_state[1]),
        .I1(time_out_2ms_reg_n_0),
        .I2(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I3(rx_state[0]),
        .I4(rx_state[2]),
        .I5(rx_state[3]),
        .O(rx_state__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[2]_i_2 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_tlock_max),
        .O(\FSM_sequential_rx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[3]_i_10 
       (.I0(reset_time_out_reg_n_0),
        .I1(time_out_2ms_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000050005300)) 
    \FSM_sequential_rx_state[3]_i_3 
       (.I0(\FSM_sequential_rx_state[3]_i_10_n_0 ),
        .I1(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I2(rx_state[0]),
        .I3(rx_state[1]),
        .I4(wait_time_cnt_reg[6]),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000023002F00)) 
    \FSM_sequential_rx_state[3]_i_7 
       (.I0(time_out_2ms_reg_n_0),
        .I1(rx_state[2]),
        .I2(rx_state[1]),
        .I3(rx_state[0]),
        .I4(\FSM_sequential_rx_state[2]_i_2_n_0 ),
        .I5(rx_state[3]),
        .O(\FSM_sequential_rx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h80800080)) 
    \FSM_sequential_rx_state[3]_i_9 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(time_out_2ms_reg_n_0),
        .I4(reset_time_out_reg_n_0),
        .O(\FSM_sequential_rx_state[3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[0]),
        .Q(rx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[1]),
        .Q(rx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[2]),
        .Q(rx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_rx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_data_valid_n_5),
        .D(rx_state__0[3]),
        .Q(rx_state[3]),
        .R(out));
  LUT5 #(
    .INIT(32'hFFFB4000)) 
    RXUSERRDY_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(gt0_rxuserrdy_t),
        .O(RXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    RXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(RXUSERRDY_i_1_n_0),
        .Q(gt0_rxuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    check_tlock_max_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(check_tlock_max_reg_n_0),
        .O(check_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    check_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(check_tlock_max_i_1_n_0),
        .Q(check_tlock_max_reg_n_0),
        .R(out));
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gtrxreset_i_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(rx_state[0]),
        .I4(GTRXRESET),
        .O(gtrxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gtrxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gtrxreset_i_i_1_n_0),
        .Q(GTRXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_2
       (.I0(GTRXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(SR));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1__0 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__2[2]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1__0 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__2[3]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1__0 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__2[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2__0 
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__2[6]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3__0 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3__0_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1__0_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1__0_n_0 ),
        .CLR(out),
        .D(p_0_in__2[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1__0
       (.I0(\init_wait_count[6]_i_3__0_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1__0_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1__0_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__3[0]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1__0 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__3[1]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1__0 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1__0 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1__0 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1__0 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2__0 
       (.I0(mmcm_lock_reclocked_i_2__0_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3__0 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2__0_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(p_0_in__3[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[2]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[3]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[4]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[5]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[6]_i_1__0_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2__0_n_0 ),
        .D(\mmcm_lock_count[7]_i_3__0_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2__0_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2__0
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'hE)) 
    reset_time_out_i_3
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .O(reset_time_out_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h34347674)) 
    reset_time_out_i_4
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .I2(rx_state[0]),
        .I3(\FSM_sequential_rx_state_reg[0]_0 ),
        .I4(rx_state[1]),
        .O(reset_time_out_i_4_n_0));
  FDSE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_0),
        .Q(reset_time_out_reg_n_0),
        .S(out));
  LUT5 #(
    .INIT(32'hFEFF0010)) 
    run_phase_alignment_int_i_1__0
       (.I0(rx_state[2]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .I3(rx_state[0]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1__0_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(data_out_0),
        .Q(run_phase_alignment_int_s3_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'hB)) 
    rx_fsm_reset_done_int_i_5
       (.I0(rx_state[1]),
        .I1(rx_state[0]),
        .O(rx_fsm_reset_done_int_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    rx_fsm_reset_done_int_i_6
       (.I0(rx_state[3]),
        .I1(rx_state[2]),
        .O(rx_fsm_reset_done_int_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_data_valid_n_1),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    rx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(rx_fsm_reset_done_int_s2),
        .Q(rx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(rxresetdone_s2),
        .Q(rxresetdone_s3),
        .R(1'b0));
  gig_ethernet_pcs_pma_0_sync_block_10 sync_RXRESETDONE
       (.data_out(rxresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_11 sync_cplllock
       (.\FSM_sequential_rx_state_reg[1] (sync_cplllock_n_0),
        .Q(rx_state[3:1]),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg),
        .rxresetdone_s3(rxresetdone_s3));
  gig_ethernet_pcs_pma_0_sync_block_12 sync_data_valid
       (.D({rx_state__0[3],rx_state__0[1:0]}),
        .E(sync_data_valid_n_5),
        .\FSM_sequential_rx_state_reg[0] (\FSM_sequential_rx_state[3]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_0 (\FSM_sequential_rx_state[3]_i_7_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_1 (\FSM_sequential_rx_state_reg[0]_0 ),
        .\FSM_sequential_rx_state_reg[0]_2 (\FSM_sequential_rx_state[0]_i_2_n_0 ),
        .\FSM_sequential_rx_state_reg[0]_3 (init_wait_done_reg_n_0),
        .\FSM_sequential_rx_state_reg[1] (sync_data_valid_n_0),
        .\FSM_sequential_rx_state_reg[1]_0 (\FSM_sequential_rx_state[1]_i_3_n_0 ),
        .\FSM_sequential_rx_state_reg[3] (\FSM_sequential_rx_state[3]_i_9_n_0 ),
        .Q(rx_state),
        .data_in(data_in),
        .data_out(data_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_3_n_0),
        .reset_time_out_reg_1(reset_time_out_i_4_n_0),
        .reset_time_out_reg_2(reset_time_out_reg_n_0),
        .rx_fsm_reset_done_int_reg(sync_data_valid_n_1),
        .rx_fsm_reset_done_int_reg_0(rx_fsm_reset_done_int_i_5_n_0),
        .rx_fsm_reset_done_int_reg_1(time_out_100us_reg_n_0),
        .rx_fsm_reset_done_int_reg_2(time_out_1us_reg_n_0),
        .rx_fsm_reset_done_int_reg_3(rx_fsm_reset_done_int_i_6_n_0),
        .time_out_wait_bypass_s3(time_out_wait_bypass_s3));
  gig_ethernet_pcs_pma_0_sync_block_13 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_14 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out_0),
        .data_sync_reg1_0(data_sync_reg6));
  gig_ethernet_pcs_pma_0_sync_block_15 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_16 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(rx_fsm_reset_done_int_s2),
        .data_sync_reg6_0(data_sync_reg6));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000100)) 
    time_out_100us_i_1
       (.I0(time_out_2ms_i_4_n_0),
        .I1(time_out_counter_reg[17]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_100us_i_2_n_0),
        .I4(time_out_100us_i_3_n_0),
        .I5(time_out_100us_reg_n_0),
        .O(time_out_100us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    time_out_100us_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[14]),
        .O(time_out_100us_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_out_100us_i_3
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[0]),
        .I2(time_out_counter_reg[1]),
        .I3(time_out_counter_reg[15]),
        .I4(time_out_counter_reg[13]),
        .O(time_out_100us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_100us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_100us_i_1_n_0),
        .Q(time_out_100us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00100000)) 
    time_out_1us_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_1us_i_2_n_0),
        .I2(time_out_counter_reg[3]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_1us_i_3_n_0),
        .I5(time_out_1us_reg_n_0),
        .O(time_out_1us_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'hE)) 
    time_out_1us_i_2
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_out_1us_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    time_out_1us_i_3
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[11]),
        .I2(time_out_counter_reg[6]),
        .I3(time_out_counter_reg[8]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[12]),
        .O(time_out_1us_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_1us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_1us_i_1_n_0),
        .Q(time_out_1us_reg_n_0),
        .R(reset_time_out_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'hFF01)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_i_2_n_0),
        .I1(time_out_2ms_i_3__0_n_0),
        .I2(time_out_2ms_i_4_n_0),
        .I3(time_out_2ms_reg_n_0),
        .O(time_out_2ms_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    time_out_2ms_i_2
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_100us_i_3_n_0),
        .O(time_out_2ms_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_2ms_i_3__0
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[18]),
        .I3(time_out_counter_reg[17]),
        .O(time_out_2ms_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFFFF)) 
    time_out_2ms_i_4
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[9]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(reset_time_out_reg_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF7FF)) 
    \time_out_counter[0]_i_1 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[16]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_2ms_i_2_n_0),
        .I5(time_out_2ms_i_4_n_0),
        .O(time_out_counter));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_3 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2__0_n_0 ,\time_out_counter_reg[0]_i_2__0_n_1 ,\time_out_counter_reg[0]_i_2__0_n_2 ,\time_out_counter_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2__0_n_4 ,\time_out_counter_reg[0]_i_2__0_n_5 ,\time_out_counter_reg[0]_i_2__0_n_6 ,\time_out_counter_reg[0]_i_2__0_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1__0 
       (.CI(\time_out_counter_reg[8]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1__0_n_0 ,\time_out_counter_reg[12]_i_1__0_n_1 ,\time_out_counter_reg[12]_i_1__0_n_2 ,\time_out_counter_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1__0_n_4 ,\time_out_counter_reg[12]_i_1__0_n_5 ,\time_out_counter_reg[12]_i_1__0_n_6 ,\time_out_counter_reg[12]_i_1__0_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1__0 
       (.CI(\time_out_counter_reg[12]_i_1__0_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1__0_n_2 ,\time_out_counter_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1__0_n_5 ,\time_out_counter_reg[16]_i_1__0_n_6 ,\time_out_counter_reg[16]_i_1__0_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2__0_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1__0 
       (.CI(\time_out_counter_reg[0]_i_2__0_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1__0_n_0 ,\time_out_counter_reg[4]_i_1__0_n_1 ,\time_out_counter_reg[4]_i_1__0_n_2 ,\time_out_counter_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1__0_n_4 ,\time_out_counter_reg[4]_i_1__0_n_5 ,\time_out_counter_reg[4]_i_1__0_n_6 ,\time_out_counter_reg[4]_i_1__0_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1__0_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out_reg_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out_reg_n_0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1__0 
       (.CI(\time_out_counter_reg[4]_i_1__0_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1__0_n_0 ,\time_out_counter_reg[8]_i_1__0_n_1 ,\time_out_counter_reg[8]_i_1__0_n_2 ,\time_out_counter_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1__0_n_4 ,\time_out_counter_reg[8]_i_1__0_n_5 ,\time_out_counter_reg[8]_i_1__0_n_6 ,\time_out_counter_reg[8]_i_1__0_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1__0_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out_reg_n_0));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2__0_n_0),
        .I3(run_phase_alignment_int_s3_reg_n_0),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    time_out_wait_bypass_i_2__0
       (.I0(time_out_wait_bypass_i_3__0_n_0),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[11]),
        .I3(wait_bypass_count_reg[0]),
        .I4(time_out_wait_bypass_i_4__0_n_0),
        .O(time_out_wait_bypass_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_3__0
       (.I0(wait_bypass_count_reg[9]),
        .I1(wait_bypass_count_reg[4]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[2]),
        .O(time_out_wait_bypass_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    time_out_wait_bypass_i_4__0
       (.I0(wait_bypass_count_reg[5]),
        .I1(wait_bypass_count_reg[7]),
        .I2(wait_bypass_count_reg[3]),
        .I3(wait_bypass_count_reg[6]),
        .I4(wait_bypass_count_reg[10]),
        .I5(wait_bypass_count_reg[8]),
        .O(time_out_wait_bypass_i_4__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg6),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry
       (.CI(1'b0),
        .CO({time_tlock_max1_carry_n_0,time_tlock_max1_carry_n_1,time_tlock_max1_carry_n_2,time_tlock_max1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({time_tlock_max1_carry_i_1_n_0,time_tlock_max1_carry_i_2_n_0,time_tlock_max1_carry_i_3_n_0,time_tlock_max1_carry_i_4_n_0}),
        .O(NLW_time_tlock_max1_carry_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry_i_5_n_0,time_tlock_max1_carry_i_6_n_0,time_tlock_max1_carry_i_7_n_0,time_tlock_max1_carry_i_8_n_0}));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__0
       (.CI(time_tlock_max1_carry_n_0),
        .CO({time_tlock_max1_carry__0_n_0,time_tlock_max1_carry__0_n_1,time_tlock_max1_carry__0_n_2,time_tlock_max1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({time_out_counter_reg[15],time_tlock_max1_carry__0_i_1_n_0,1'b0,time_tlock_max1_carry__0_i_2_n_0}),
        .O(NLW_time_tlock_max1_carry__0_O_UNCONNECTED[3:0]),
        .S({time_tlock_max1_carry__0_i_3_n_0,time_tlock_max1_carry__0_i_4_n_0,time_tlock_max1_carry__0_i_5_n_0,time_tlock_max1_carry__0_i_6_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__0_i_1
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[13]),
        .O(time_tlock_max1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_2
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(time_tlock_max1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_3
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .O(time_tlock_max1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__0_i_4
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[12]),
        .O(time_tlock_max1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry__0_i_5
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[11]),
        .O(time_tlock_max1_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry__0_i_6
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[8]),
        .O(time_tlock_max1_carry__0_i_6_n_0));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 time_tlock_max1_carry__1
       (.CI(time_tlock_max1_carry__0_n_0),
        .CO({NLW_time_tlock_max1_carry__1_CO_UNCONNECTED[3:2],time_tlock_max1,time_tlock_max1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,time_out_counter_reg[18],time_tlock_max1_carry__1_i_1_n_0}),
        .O(NLW_time_tlock_max1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,time_tlock_max1_carry__1_i_2_n_0,time_tlock_max1_carry__1_i_3_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry__1_i_1
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(time_tlock_max1_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    time_tlock_max1_carry__1_i_2
       (.I0(time_out_counter_reg[18]),
        .O(time_tlock_max1_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry__1_i_3
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[16]),
        .O(time_tlock_max1_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_1
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(time_tlock_max1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    time_tlock_max1_carry_i_2
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(time_tlock_max1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_3
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(time_tlock_max1_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    time_tlock_max1_carry_i_4
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[1]),
        .O(time_tlock_max1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_5
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[6]),
        .O(time_tlock_max1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    time_tlock_max1_carry_i_6
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[4]),
        .O(time_tlock_max1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_7
       (.I0(time_out_counter_reg[3]),
        .I1(time_out_counter_reg[2]),
        .O(time_tlock_max1_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    time_tlock_max1_carry_i_8
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(time_tlock_max1_carry_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    time_tlock_max_i_1
       (.I0(check_tlock_max_reg_n_0),
        .I1(time_tlock_max1),
        .I2(time_tlock_max),
        .O(time_tlock_max_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max),
        .R(reset_time_out_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1__0 
       (.I0(run_phase_alignment_int_s3_reg_n_0),
        .O(\wait_bypass_count[0]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2__0 
       (.I0(time_out_wait_bypass_i_2__0_n_0),
        .I1(rx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3__0_n_0 ,\wait_bypass_count_reg[0]_i_3__0_n_1 ,\wait_bypass_count_reg[0]_i_3__0_n_2 ,\wait_bypass_count_reg[0]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3__0_n_4 ,\wait_bypass_count_reg[0]_i_3__0_n_5 ,\wait_bypass_count_reg[0]_i_3__0_n_6 ,\wait_bypass_count_reg[0]_i_3__0_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1__0 
       (.CI(\wait_bypass_count_reg[8]_i_1__0_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED [3:1],\wait_bypass_count_reg[12]_i_1__0_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[12]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3__0_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1__0 
       (.CI(\wait_bypass_count_reg[0]_i_3__0_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1__0_n_0 ,\wait_bypass_count_reg[4]_i_1__0_n_1 ,\wait_bypass_count_reg[4]_i_1__0_n_2 ,\wait_bypass_count_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1__0_n_4 ,\wait_bypass_count_reg[4]_i_1__0_n_5 ,\wait_bypass_count_reg[4]_i_1__0_n_6 ,\wait_bypass_count_reg[4]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1__0_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1__0 
       (.CI(\wait_bypass_count_reg[4]_i_1__0_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1__0_n_0 ,\wait_bypass_count_reg[8]_i_1__0_n_1 ,\wait_bypass_count_reg[8]_i_1__0_n_2 ,\wait_bypass_count_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1__0_n_4 ,\wait_bypass_count_reg[8]_i_1__0_n_5 ,\wait_bypass_count_reg[8]_i_1__0_n_6 ,\wait_bypass_count_reg[8]_i_1__0_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg6),
        .CE(\wait_bypass_count[0]_i_2__0_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1__0_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\wait_bypass_count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0__0));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1__0 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1__0 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1__0 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1__0 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \wait_time_cnt[6]_i_1 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .O(\wait_time_cnt[6]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_2__0_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3__0 
       (.I0(\wait_time_cnt[6]_i_4__0_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4__0 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(wait_time_cnt0__0),
        .Q(wait_time_cnt_reg[0]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[1]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[2]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[3]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[4]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[5]_i_1__0_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\wait_time_cnt[6]_i_2__0_n_0 ),
        .D(\wait_time_cnt[6]_i_3__0_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(\wait_time_cnt[6]_i_1_n_0 ));
endmodule

module gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
   (mmcm_reset,
    gt0_cpllreset_t,
    data_in,
    gt0_txuserrdy_t,
    gt0_gttxreset_in0_out,
    independent_clock_bufg,
    data_sync_reg1,
    out,
    gtxe2_i,
    gt0_cpllrefclklost_i,
    data_sync_reg1_0,
    data_sync_reg1_1,
    data_sync_reg1_2);
  output mmcm_reset;
  output gt0_cpllreset_t;
  output data_in;
  output gt0_txuserrdy_t;
  output gt0_gttxreset_in0_out;
  input independent_clock_bufg;
  input data_sync_reg1;
  input [0:0]out;
  input gtxe2_i;
  input gt0_cpllrefclklost_i;
  input data_sync_reg1_0;
  input data_sync_reg1_1;
  input data_sync_reg1_2;

  wire CPLL_RESET_i_1_n_0;
  wire CPLL_RESET_i_2_n_0;
  wire \FSM_sequential_tx_state[0]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[2]_i_2_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_3_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_7_n_0 ;
  wire \FSM_sequential_tx_state[3]_i_8_n_0 ;
  wire GTTXRESET;
  wire MMCM_RESET_i_1_n_0;
  wire TXUSERRDY_i_1_n_0;
  wire clear;
  wire data_in;
  wire data_out;
  wire data_sync_reg1;
  wire data_sync_reg1_0;
  wire data_sync_reg1_1;
  wire data_sync_reg1_2;
  wire gt0_cpllrefclklost_i;
  wire gt0_cpllreset_t;
  wire gt0_gttxreset_in0_out;
  wire gt0_txuserrdy_t;
  wire gttxreset_i_i_1_n_0;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire \init_wait_count[0]_i_1_n_0 ;
  wire \init_wait_count[6]_i_1_n_0 ;
  wire \init_wait_count[6]_i_3_n_0 ;
  wire [6:0]init_wait_count_reg;
  wire init_wait_done_i_1_n_0;
  wire init_wait_done_reg_n_0;
  wire \mmcm_lock_count[2]_i_1_n_0 ;
  wire \mmcm_lock_count[3]_i_1_n_0 ;
  wire \mmcm_lock_count[4]_i_1_n_0 ;
  wire \mmcm_lock_count[5]_i_1_n_0 ;
  wire \mmcm_lock_count[6]_i_1_n_0 ;
  wire \mmcm_lock_count[7]_i_2_n_0 ;
  wire \mmcm_lock_count[7]_i_3_n_0 ;
  wire [7:0]mmcm_lock_count_reg;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;
  wire mmcm_lock_reclocked_i_1_n_0;
  wire mmcm_lock_reclocked_i_2_n_0;
  wire mmcm_reset;
  wire [0:0]out;
  wire [6:1]p_0_in__0;
  wire [1:0]p_0_in__1;
  wire pll_reset_asserted_i_1_n_0;
  wire pll_reset_asserted_i_2_n_0;
  wire pll_reset_asserted_reg_n_0;
  wire refclk_stable_count;
  wire \refclk_stable_count[0]_i_3_n_0 ;
  wire \refclk_stable_count[0]_i_4_n_0 ;
  wire \refclk_stable_count[0]_i_5_n_0 ;
  wire \refclk_stable_count[0]_i_6_n_0 ;
  wire \refclk_stable_count[0]_i_7_n_0 ;
  wire \refclk_stable_count[0]_i_8_n_0 ;
  wire \refclk_stable_count[0]_i_9_n_0 ;
  wire [31:0]refclk_stable_count_reg;
  wire \refclk_stable_count_reg[0]_i_2_n_0 ;
  wire \refclk_stable_count_reg[0]_i_2_n_1 ;
  wire \refclk_stable_count_reg[0]_i_2_n_2 ;
  wire \refclk_stable_count_reg[0]_i_2_n_3 ;
  wire \refclk_stable_count_reg[0]_i_2_n_4 ;
  wire \refclk_stable_count_reg[0]_i_2_n_5 ;
  wire \refclk_stable_count_reg[0]_i_2_n_6 ;
  wire \refclk_stable_count_reg[0]_i_2_n_7 ;
  wire \refclk_stable_count_reg[12]_i_1_n_0 ;
  wire \refclk_stable_count_reg[12]_i_1_n_1 ;
  wire \refclk_stable_count_reg[12]_i_1_n_2 ;
  wire \refclk_stable_count_reg[12]_i_1_n_3 ;
  wire \refclk_stable_count_reg[12]_i_1_n_4 ;
  wire \refclk_stable_count_reg[12]_i_1_n_5 ;
  wire \refclk_stable_count_reg[12]_i_1_n_6 ;
  wire \refclk_stable_count_reg[12]_i_1_n_7 ;
  wire \refclk_stable_count_reg[16]_i_1_n_0 ;
  wire \refclk_stable_count_reg[16]_i_1_n_1 ;
  wire \refclk_stable_count_reg[16]_i_1_n_2 ;
  wire \refclk_stable_count_reg[16]_i_1_n_3 ;
  wire \refclk_stable_count_reg[16]_i_1_n_4 ;
  wire \refclk_stable_count_reg[16]_i_1_n_5 ;
  wire \refclk_stable_count_reg[16]_i_1_n_6 ;
  wire \refclk_stable_count_reg[16]_i_1_n_7 ;
  wire \refclk_stable_count_reg[20]_i_1_n_0 ;
  wire \refclk_stable_count_reg[20]_i_1_n_1 ;
  wire \refclk_stable_count_reg[20]_i_1_n_2 ;
  wire \refclk_stable_count_reg[20]_i_1_n_3 ;
  wire \refclk_stable_count_reg[20]_i_1_n_4 ;
  wire \refclk_stable_count_reg[20]_i_1_n_5 ;
  wire \refclk_stable_count_reg[20]_i_1_n_6 ;
  wire \refclk_stable_count_reg[20]_i_1_n_7 ;
  wire \refclk_stable_count_reg[24]_i_1_n_0 ;
  wire \refclk_stable_count_reg[24]_i_1_n_1 ;
  wire \refclk_stable_count_reg[24]_i_1_n_2 ;
  wire \refclk_stable_count_reg[24]_i_1_n_3 ;
  wire \refclk_stable_count_reg[24]_i_1_n_4 ;
  wire \refclk_stable_count_reg[24]_i_1_n_5 ;
  wire \refclk_stable_count_reg[24]_i_1_n_6 ;
  wire \refclk_stable_count_reg[24]_i_1_n_7 ;
  wire \refclk_stable_count_reg[28]_i_1_n_1 ;
  wire \refclk_stable_count_reg[28]_i_1_n_2 ;
  wire \refclk_stable_count_reg[28]_i_1_n_3 ;
  wire \refclk_stable_count_reg[28]_i_1_n_4 ;
  wire \refclk_stable_count_reg[28]_i_1_n_5 ;
  wire \refclk_stable_count_reg[28]_i_1_n_6 ;
  wire \refclk_stable_count_reg[28]_i_1_n_7 ;
  wire \refclk_stable_count_reg[4]_i_1_n_0 ;
  wire \refclk_stable_count_reg[4]_i_1_n_1 ;
  wire \refclk_stable_count_reg[4]_i_1_n_2 ;
  wire \refclk_stable_count_reg[4]_i_1_n_3 ;
  wire \refclk_stable_count_reg[4]_i_1_n_4 ;
  wire \refclk_stable_count_reg[4]_i_1_n_5 ;
  wire \refclk_stable_count_reg[4]_i_1_n_6 ;
  wire \refclk_stable_count_reg[4]_i_1_n_7 ;
  wire \refclk_stable_count_reg[8]_i_1_n_0 ;
  wire \refclk_stable_count_reg[8]_i_1_n_1 ;
  wire \refclk_stable_count_reg[8]_i_1_n_2 ;
  wire \refclk_stable_count_reg[8]_i_1_n_3 ;
  wire \refclk_stable_count_reg[8]_i_1_n_4 ;
  wire \refclk_stable_count_reg[8]_i_1_n_5 ;
  wire \refclk_stable_count_reg[8]_i_1_n_6 ;
  wire \refclk_stable_count_reg[8]_i_1_n_7 ;
  wire refclk_stable_i_1_n_0;
  wire refclk_stable_i_2_n_0;
  wire refclk_stable_i_3_n_0;
  wire refclk_stable_i_4_n_0;
  wire refclk_stable_i_5_n_0;
  wire refclk_stable_i_6_n_0;
  wire refclk_stable_reg_n_0;
  wire reset_time_out;
  wire reset_time_out_i_2__0_n_0;
  wire run_phase_alignment_int_i_1_n_0;
  wire run_phase_alignment_int_reg_n_0;
  wire run_phase_alignment_int_s3;
  wire sel;
  wire sync_cplllock_n_0;
  wire sync_cplllock_n_1;
  wire sync_mmcm_lock_reclocked_n_0;
  wire time_out_2ms_i_1_n_0;
  wire time_out_2ms_i_2__0_n_0;
  wire time_out_2ms_i_3_n_0;
  wire time_out_2ms_i_4__0_n_0;
  wire time_out_2ms_i_5_n_0;
  wire time_out_2ms_reg_n_0;
  wire time_out_500us_i_1_n_0;
  wire time_out_500us_i_2_n_0;
  wire time_out_500us_reg_n_0;
  wire time_out_counter;
  wire \time_out_counter[0]_i_3__0_n_0 ;
  wire \time_out_counter[0]_i_4_n_0 ;
  wire [18:0]time_out_counter_reg;
  wire \time_out_counter_reg[0]_i_2_n_0 ;
  wire \time_out_counter_reg[0]_i_2_n_1 ;
  wire \time_out_counter_reg[0]_i_2_n_2 ;
  wire \time_out_counter_reg[0]_i_2_n_3 ;
  wire \time_out_counter_reg[0]_i_2_n_4 ;
  wire \time_out_counter_reg[0]_i_2_n_5 ;
  wire \time_out_counter_reg[0]_i_2_n_6 ;
  wire \time_out_counter_reg[0]_i_2_n_7 ;
  wire \time_out_counter_reg[12]_i_1_n_0 ;
  wire \time_out_counter_reg[12]_i_1_n_1 ;
  wire \time_out_counter_reg[12]_i_1_n_2 ;
  wire \time_out_counter_reg[12]_i_1_n_3 ;
  wire \time_out_counter_reg[12]_i_1_n_4 ;
  wire \time_out_counter_reg[12]_i_1_n_5 ;
  wire \time_out_counter_reg[12]_i_1_n_6 ;
  wire \time_out_counter_reg[12]_i_1_n_7 ;
  wire \time_out_counter_reg[16]_i_1_n_2 ;
  wire \time_out_counter_reg[16]_i_1_n_3 ;
  wire \time_out_counter_reg[16]_i_1_n_5 ;
  wire \time_out_counter_reg[16]_i_1_n_6 ;
  wire \time_out_counter_reg[16]_i_1_n_7 ;
  wire \time_out_counter_reg[4]_i_1_n_0 ;
  wire \time_out_counter_reg[4]_i_1_n_1 ;
  wire \time_out_counter_reg[4]_i_1_n_2 ;
  wire \time_out_counter_reg[4]_i_1_n_3 ;
  wire \time_out_counter_reg[4]_i_1_n_4 ;
  wire \time_out_counter_reg[4]_i_1_n_5 ;
  wire \time_out_counter_reg[4]_i_1_n_6 ;
  wire \time_out_counter_reg[4]_i_1_n_7 ;
  wire \time_out_counter_reg[8]_i_1_n_0 ;
  wire \time_out_counter_reg[8]_i_1_n_1 ;
  wire \time_out_counter_reg[8]_i_1_n_2 ;
  wire \time_out_counter_reg[8]_i_1_n_3 ;
  wire \time_out_counter_reg[8]_i_1_n_4 ;
  wire \time_out_counter_reg[8]_i_1_n_5 ;
  wire \time_out_counter_reg[8]_i_1_n_6 ;
  wire \time_out_counter_reg[8]_i_1_n_7 ;
  wire time_out_wait_bypass_i_1_n_0;
  wire time_out_wait_bypass_i_2_n_0;
  wire time_out_wait_bypass_i_3_n_0;
  wire time_out_wait_bypass_i_4_n_0;
  wire time_out_wait_bypass_i_5_n_0;
  wire time_out_wait_bypass_reg_n_0;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max_i_1_n_0;
  wire time_tlock_max_i_2_n_0;
  wire time_tlock_max_i_3_n_0;
  wire time_tlock_max_i_4_n_0;
  wire time_tlock_max_reg_n_0;
  wire tx_fsm_reset_done_int_i_1_n_0;
  wire tx_fsm_reset_done_int_s2;
  wire tx_fsm_reset_done_int_s3;
  wire [3:0]tx_state;
  wire [3:0]tx_state__0;
  wire txresetdone_s2;
  wire txresetdone_s3;
  wire \wait_bypass_count[0]_i_2_n_0 ;
  wire \wait_bypass_count[0]_i_4__0_n_0 ;
  wire [16:0]wait_bypass_count_reg;
  wire \wait_bypass_count_reg[0]_i_3_n_0 ;
  wire \wait_bypass_count_reg[0]_i_3_n_1 ;
  wire \wait_bypass_count_reg[0]_i_3_n_2 ;
  wire \wait_bypass_count_reg[0]_i_3_n_3 ;
  wire \wait_bypass_count_reg[0]_i_3_n_4 ;
  wire \wait_bypass_count_reg[0]_i_3_n_5 ;
  wire \wait_bypass_count_reg[0]_i_3_n_6 ;
  wire \wait_bypass_count_reg[0]_i_3_n_7 ;
  wire \wait_bypass_count_reg[12]_i_1_n_0 ;
  wire \wait_bypass_count_reg[12]_i_1_n_1 ;
  wire \wait_bypass_count_reg[12]_i_1_n_2 ;
  wire \wait_bypass_count_reg[12]_i_1_n_3 ;
  wire \wait_bypass_count_reg[12]_i_1_n_4 ;
  wire \wait_bypass_count_reg[12]_i_1_n_5 ;
  wire \wait_bypass_count_reg[12]_i_1_n_6 ;
  wire \wait_bypass_count_reg[12]_i_1_n_7 ;
  wire \wait_bypass_count_reg[16]_i_1_n_7 ;
  wire \wait_bypass_count_reg[4]_i_1_n_0 ;
  wire \wait_bypass_count_reg[4]_i_1_n_1 ;
  wire \wait_bypass_count_reg[4]_i_1_n_2 ;
  wire \wait_bypass_count_reg[4]_i_1_n_3 ;
  wire \wait_bypass_count_reg[4]_i_1_n_4 ;
  wire \wait_bypass_count_reg[4]_i_1_n_5 ;
  wire \wait_bypass_count_reg[4]_i_1_n_6 ;
  wire \wait_bypass_count_reg[4]_i_1_n_7 ;
  wire \wait_bypass_count_reg[8]_i_1_n_0 ;
  wire \wait_bypass_count_reg[8]_i_1_n_1 ;
  wire \wait_bypass_count_reg[8]_i_1_n_2 ;
  wire \wait_bypass_count_reg[8]_i_1_n_3 ;
  wire \wait_bypass_count_reg[8]_i_1_n_4 ;
  wire \wait_bypass_count_reg[8]_i_1_n_5 ;
  wire \wait_bypass_count_reg[8]_i_1_n_6 ;
  wire \wait_bypass_count_reg[8]_i_1_n_7 ;
  wire [0:0]wait_time_cnt0;
  wire wait_time_cnt0_0;
  wire \wait_time_cnt[1]_i_1_n_0 ;
  wire \wait_time_cnt[2]_i_1_n_0 ;
  wire \wait_time_cnt[3]_i_1_n_0 ;
  wire \wait_time_cnt[4]_i_1_n_0 ;
  wire \wait_time_cnt[5]_i_1_n_0 ;
  wire \wait_time_cnt[6]_i_3_n_0 ;
  wire \wait_time_cnt[6]_i_4_n_0 ;
  wire [6:0]wait_time_cnt_reg;
  wire [3:3]\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFF1F0000001F)) 
    CPLL_RESET_i_1
       (.I0(pll_reset_asserted_reg_n_0),
        .I1(gt0_cpllrefclklost_i),
        .I2(refclk_stable_reg_n_0),
        .I3(CPLL_RESET_i_2_n_0),
        .I4(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I5(gt0_cpllreset_t),
        .O(CPLL_RESET_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'hE)) 
    CPLL_RESET_i_2
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .O(CPLL_RESET_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    CPLL_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(CPLL_RESET_i_1_n_0),
        .Q(gt0_cpllreset_t),
        .R(out));
  LUT6 #(
    .INIT(64'hF3FFF3F0F5F0F5F0)) 
    \FSM_sequential_tx_state[0]_i_1 
       (.I0(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I1(\FSM_sequential_tx_state[0]_i_2_n_0 ),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(tx_state[2]),
        .I4(time_out_2ms_reg_n_0),
        .I5(tx_state[1]),
        .O(tx_state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_2 
       (.I0(reset_time_out),
        .I1(time_out_500us_reg_n_0),
        .O(\FSM_sequential_tx_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_tx_state[0]_i_3 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .O(\FSM_sequential_tx_state[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h005A001A)) 
    \FSM_sequential_tx_state[1]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .O(tx_state__0[1]));
  LUT6 #(
    .INIT(64'h04000C0C06020C0C)) 
    \FSM_sequential_tx_state[2]_i_1 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(\FSM_sequential_tx_state[2]_i_2_n_0 ),
        .I4(tx_state[0]),
        .I5(time_out_2ms_reg_n_0),
        .O(tx_state__0[2]));
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_tx_state[2]_i_2 
       (.I0(time_tlock_max_reg_n_0),
        .I1(reset_time_out),
        .I2(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hF4FF4444)) 
    \FSM_sequential_tx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(tx_state[3]),
        .I2(reset_time_out),
        .I3(time_out_500us_reg_n_0),
        .I4(\FSM_sequential_tx_state[3]_i_8_n_0 ),
        .O(tx_state__0[3]));
  LUT6 #(
    .INIT(64'h00BA000000000000)) 
    \FSM_sequential_tx_state[3]_i_3 
       (.I0(txresetdone_s3),
        .I1(reset_time_out),
        .I2(time_out_500us_reg_n_0),
        .I3(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I4(tx_state[2]),
        .I5(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000300FF00AA)) 
    \FSM_sequential_tx_state[3]_i_4 
       (.I0(init_wait_done_reg_n_0),
        .I1(\wait_time_cnt[6]_i_4_n_0 ),
        .I2(wait_time_cnt_reg[6]),
        .I3(tx_state[0]),
        .I4(tx_state[3]),
        .I5(CPLL_RESET_i_2_n_0),
        .O(\FSM_sequential_tx_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0404040400040000)) 
    \FSM_sequential_tx_state[3]_i_6 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .I3(reset_time_out),
        .I4(time_tlock_max_reg_n_0),
        .I5(mmcm_lock_reclocked),
        .O(\FSM_sequential_tx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \FSM_sequential_tx_state[3]_i_7 
       (.I0(tx_state[2]),
        .I1(tx_state[3]),
        .I2(tx_state[0]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \FSM_sequential_tx_state[3]_i_8 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .O(\FSM_sequential_tx_state[3]_i_8_n_0 ));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[0]),
        .Q(tx_state[0]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[1]),
        .Q(tx_state[1]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[2]),
        .Q(tx_state[2]),
        .R(out));
  (* FSM_ENCODED_STATES = "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_tx_state_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sync_cplllock_n_1),
        .D(tx_state__0[3]),
        .Q(tx_state[3]),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hFFF70004)) 
    MMCM_RESET_i_1
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[3]),
        .I3(tx_state[1]),
        .I4(mmcm_reset),
        .O(MMCM_RESET_i_1_n_0));
  FDRE #(
    .INIT(1'b1)) 
    MMCM_RESET_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(MMCM_RESET_i_1_n_0),
        .Q(mmcm_reset),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hFFFD2000)) 
    TXUSERRDY_i_1
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(gt0_txuserrdy_t),
        .O(TXUSERRDY_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    TXUSERRDY_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(TXUSERRDY_i_1_n_0),
        .Q(gt0_txuserrdy_t),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hFFEF0100)) 
    gttxreset_i_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[1]),
        .I2(tx_state[2]),
        .I3(tx_state[0]),
        .I4(GTTXRESET),
        .O(gttxreset_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    gttxreset_i_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(gttxreset_i_i_1_n_0),
        .Q(GTTXRESET),
        .R(out));
  LUT3 #(
    .INIT(8'hEA)) 
    gtxe2_i_i_3
       (.I0(GTTXRESET),
        .I1(data_in),
        .I2(gtxe2_i),
        .O(gt0_gttxreset_in0_out));
  LUT1 #(
    .INIT(2'h1)) 
    \init_wait_count[0]_i_1 
       (.I0(init_wait_count_reg[0]),
        .O(\init_wait_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \init_wait_count[1]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \init_wait_count[2]_i_1 
       (.I0(init_wait_count_reg[0]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \init_wait_count[3]_i_1 
       (.I0(init_wait_count_reg[1]),
        .I1(init_wait_count_reg[2]),
        .I2(init_wait_count_reg[0]),
        .I3(init_wait_count_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \init_wait_count[4]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \init_wait_count[5]_i_1 
       (.I0(init_wait_count_reg[2]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[3]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_count_reg[4]),
        .I5(init_wait_count_reg[5]),
        .O(p_0_in__0[5]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \init_wait_count[6]_i_1 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(\init_wait_count[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \init_wait_count[6]_i_2 
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[0]),
        .I2(init_wait_count_reg[4]),
        .I3(init_wait_count_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \init_wait_count[6]_i_3 
       (.I0(init_wait_count_reg[3]),
        .I1(init_wait_count_reg[1]),
        .I2(init_wait_count_reg[2]),
        .I3(init_wait_count_reg[5]),
        .O(\init_wait_count[6]_i_3_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(\init_wait_count[0]_i_1_n_0 ),
        .Q(init_wait_count_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[1]),
        .Q(init_wait_count_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[2]),
        .Q(init_wait_count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[3]),
        .Q(init_wait_count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[4]),
        .Q(init_wait_count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[5]),
        .Q(init_wait_count_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \init_wait_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\init_wait_count[6]_i_1_n_0 ),
        .CLR(out),
        .D(p_0_in__0[6]),
        .Q(init_wait_count_reg[6]));
  LUT5 #(
    .INIT(32'hFFFF0010)) 
    init_wait_done_i_1
       (.I0(\init_wait_count[6]_i_3_n_0 ),
        .I1(init_wait_count_reg[4]),
        .I2(init_wait_count_reg[6]),
        .I3(init_wait_count_reg[0]),
        .I4(init_wait_done_reg_n_0),
        .O(init_wait_done_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    init_wait_done_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .CLR(out),
        .D(init_wait_done_i_1_n_0),
        .Q(init_wait_done_reg_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[0]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .O(p_0_in__1[0]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \mmcm_lock_count[1]_i_1 
       (.I0(mmcm_lock_count_reg[0]),
        .I1(mmcm_lock_count_reg[1]),
        .O(p_0_in__1[1]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \mmcm_lock_count[2]_i_1 
       (.I0(mmcm_lock_count_reg[1]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[2]),
        .O(\mmcm_lock_count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \mmcm_lock_count[3]_i_1 
       (.I0(mmcm_lock_count_reg[2]),
        .I1(mmcm_lock_count_reg[0]),
        .I2(mmcm_lock_count_reg[1]),
        .I3(mmcm_lock_count_reg[3]),
        .O(\mmcm_lock_count[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \mmcm_lock_count[4]_i_1 
       (.I0(mmcm_lock_count_reg[3]),
        .I1(mmcm_lock_count_reg[1]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[2]),
        .I4(mmcm_lock_count_reg[4]),
        .O(\mmcm_lock_count[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \mmcm_lock_count[5]_i_1 
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(\mmcm_lock_count[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \mmcm_lock_count[6]_i_1 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .O(\mmcm_lock_count[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \mmcm_lock_count[7]_i_2 
       (.I0(mmcm_lock_reclocked_i_2_n_0),
        .I1(mmcm_lock_count_reg[6]),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \mmcm_lock_count[7]_i_3 
       (.I0(mmcm_lock_count_reg[6]),
        .I1(mmcm_lock_reclocked_i_2_n_0),
        .I2(mmcm_lock_count_reg[7]),
        .O(\mmcm_lock_count[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[0]),
        .Q(mmcm_lock_count_reg[0]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(p_0_in__1[1]),
        .Q(mmcm_lock_count_reg[1]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[2]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[2]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[3]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[3]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[4]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[4]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[5]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[5]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[6]_i_1_n_0 ),
        .Q(mmcm_lock_count_reg[6]),
        .R(sync_mmcm_lock_reclocked_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \mmcm_lock_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(\mmcm_lock_count[7]_i_2_n_0 ),
        .D(\mmcm_lock_count[7]_i_3_n_0 ),
        .Q(mmcm_lock_count_reg[7]),
        .R(sync_mmcm_lock_reclocked_n_0));
  LUT5 #(
    .INIT(32'hAAEA0000)) 
    mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(mmcm_lock_count_reg[7]),
        .I2(mmcm_lock_count_reg[6]),
        .I3(mmcm_lock_reclocked_i_2_n_0),
        .I4(mmcm_lock_i),
        .O(mmcm_lock_reclocked_i_1_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    mmcm_lock_reclocked_i_2
       (.I0(mmcm_lock_count_reg[4]),
        .I1(mmcm_lock_count_reg[2]),
        .I2(mmcm_lock_count_reg[0]),
        .I3(mmcm_lock_count_reg[1]),
        .I4(mmcm_lock_count_reg[3]),
        .I5(mmcm_lock_count_reg[5]),
        .O(mmcm_lock_reclocked_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    mmcm_lock_reclocked_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(mmcm_lock_reclocked_i_1_n_0),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000CD55CCCCCCCC)) 
    pll_reset_asserted_i_1
       (.I0(tx_state[3]),
        .I1(pll_reset_asserted_reg_n_0),
        .I2(gt0_cpllrefclklost_i),
        .I3(refclk_stable_reg_n_0),
        .I4(tx_state[1]),
        .I5(pll_reset_asserted_i_2_n_0),
        .O(pll_reset_asserted_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'h02)) 
    pll_reset_asserted_i_2
       (.I0(tx_state[0]),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .O(pll_reset_asserted_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_reset_asserted_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pll_reset_asserted_i_1_n_0),
        .Q(pll_reset_asserted_reg_n_0),
        .R(out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \refclk_stable_count[0]_i_1 
       (.I0(\refclk_stable_count[0]_i_3_n_0 ),
        .I1(\refclk_stable_count[0]_i_4_n_0 ),
        .I2(\refclk_stable_count[0]_i_5_n_0 ),
        .I3(\refclk_stable_count[0]_i_6_n_0 ),
        .I4(\refclk_stable_count[0]_i_7_n_0 ),
        .I5(\refclk_stable_count[0]_i_8_n_0 ),
        .O(refclk_stable_count));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    \refclk_stable_count[0]_i_3 
       (.I0(refclk_stable_count_reg[13]),
        .I1(refclk_stable_count_reg[12]),
        .I2(refclk_stable_count_reg[10]),
        .I3(refclk_stable_count_reg[11]),
        .I4(refclk_stable_count_reg[9]),
        .I5(refclk_stable_count_reg[8]),
        .O(\refclk_stable_count[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDF)) 
    \refclk_stable_count[0]_i_4 
       (.I0(refclk_stable_count_reg[19]),
        .I1(refclk_stable_count_reg[18]),
        .I2(refclk_stable_count_reg[16]),
        .I3(refclk_stable_count_reg[17]),
        .I4(refclk_stable_count_reg[15]),
        .I5(refclk_stable_count_reg[14]),
        .O(\refclk_stable_count[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_5 
       (.I0(refclk_stable_count_reg[30]),
        .I1(refclk_stable_count_reg[31]),
        .I2(refclk_stable_count_reg[28]),
        .I3(refclk_stable_count_reg[29]),
        .I4(refclk_stable_count_reg[27]),
        .I5(refclk_stable_count_reg[26]),
        .O(\refclk_stable_count[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \refclk_stable_count[0]_i_6 
       (.I0(refclk_stable_count_reg[24]),
        .I1(refclk_stable_count_reg[25]),
        .I2(refclk_stable_count_reg[22]),
        .I3(refclk_stable_count_reg[23]),
        .I4(refclk_stable_count_reg[21]),
        .I5(refclk_stable_count_reg[20]),
        .O(\refclk_stable_count[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \refclk_stable_count[0]_i_7 
       (.I0(refclk_stable_count_reg[0]),
        .I1(refclk_stable_count_reg[1]),
        .O(\refclk_stable_count[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \refclk_stable_count[0]_i_8 
       (.I0(refclk_stable_count_reg[6]),
        .I1(refclk_stable_count_reg[7]),
        .I2(refclk_stable_count_reg[4]),
        .I3(refclk_stable_count_reg[5]),
        .I4(refclk_stable_count_reg[3]),
        .I5(refclk_stable_count_reg[2]),
        .O(\refclk_stable_count[0]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \refclk_stable_count[0]_i_9 
       (.I0(refclk_stable_count_reg[0]),
        .O(\refclk_stable_count[0]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[0] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_7 ),
        .Q(refclk_stable_count_reg[0]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\refclk_stable_count_reg[0]_i_2_n_0 ,\refclk_stable_count_reg[0]_i_2_n_1 ,\refclk_stable_count_reg[0]_i_2_n_2 ,\refclk_stable_count_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\refclk_stable_count_reg[0]_i_2_n_4 ,\refclk_stable_count_reg[0]_i_2_n_5 ,\refclk_stable_count_reg[0]_i_2_n_6 ,\refclk_stable_count_reg[0]_i_2_n_7 }),
        .S({refclk_stable_count_reg[3:1],\refclk_stable_count[0]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[10] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[11] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[12] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[12]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[12]_i_1 
       (.CI(\refclk_stable_count_reg[8]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[12]_i_1_n_0 ,\refclk_stable_count_reg[12]_i_1_n_1 ,\refclk_stable_count_reg[12]_i_1_n_2 ,\refclk_stable_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[12]_i_1_n_4 ,\refclk_stable_count_reg[12]_i_1_n_5 ,\refclk_stable_count_reg[12]_i_1_n_6 ,\refclk_stable_count_reg[12]_i_1_n_7 }),
        .S(refclk_stable_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[13] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[14] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[15] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[12]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[16] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[16]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[16]_i_1 
       (.CI(\refclk_stable_count_reg[12]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[16]_i_1_n_0 ,\refclk_stable_count_reg[16]_i_1_n_1 ,\refclk_stable_count_reg[16]_i_1_n_2 ,\refclk_stable_count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[16]_i_1_n_4 ,\refclk_stable_count_reg[16]_i_1_n_5 ,\refclk_stable_count_reg[16]_i_1_n_6 ,\refclk_stable_count_reg[16]_i_1_n_7 }),
        .S(refclk_stable_count_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[17] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[18] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[19] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[16]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[1] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_6 ),
        .Q(refclk_stable_count_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[20] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[20]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[20]_i_1 
       (.CI(\refclk_stable_count_reg[16]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[20]_i_1_n_0 ,\refclk_stable_count_reg[20]_i_1_n_1 ,\refclk_stable_count_reg[20]_i_1_n_2 ,\refclk_stable_count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[20]_i_1_n_4 ,\refclk_stable_count_reg[20]_i_1_n_5 ,\refclk_stable_count_reg[20]_i_1_n_6 ,\refclk_stable_count_reg[20]_i_1_n_7 }),
        .S(refclk_stable_count_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[21] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[22] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[23] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[20]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[24] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[24]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[24]_i_1 
       (.CI(\refclk_stable_count_reg[20]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[24]_i_1_n_0 ,\refclk_stable_count_reg[24]_i_1_n_1 ,\refclk_stable_count_reg[24]_i_1_n_2 ,\refclk_stable_count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[24]_i_1_n_4 ,\refclk_stable_count_reg[24]_i_1_n_5 ,\refclk_stable_count_reg[24]_i_1_n_6 ,\refclk_stable_count_reg[24]_i_1_n_7 }),
        .S(refclk_stable_count_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[25] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[26] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[27] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[24]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[28] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[28]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[28]_i_1 
       (.CI(\refclk_stable_count_reg[24]_i_1_n_0 ),
        .CO({\NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED [3],\refclk_stable_count_reg[28]_i_1_n_1 ,\refclk_stable_count_reg[28]_i_1_n_2 ,\refclk_stable_count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[28]_i_1_n_4 ,\refclk_stable_count_reg[28]_i_1_n_5 ,\refclk_stable_count_reg[28]_i_1_n_6 ,\refclk_stable_count_reg[28]_i_1_n_7 }),
        .S(refclk_stable_count_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[29] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[2] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_5 ),
        .Q(refclk_stable_count_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[30] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[31] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[28]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[3] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[0]_i_2_n_4 ),
        .Q(refclk_stable_count_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[4] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[4]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[4]_i_1 
       (.CI(\refclk_stable_count_reg[0]_i_2_n_0 ),
        .CO({\refclk_stable_count_reg[4]_i_1_n_0 ,\refclk_stable_count_reg[4]_i_1_n_1 ,\refclk_stable_count_reg[4]_i_1_n_2 ,\refclk_stable_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[4]_i_1_n_4 ,\refclk_stable_count_reg[4]_i_1_n_5 ,\refclk_stable_count_reg[4]_i_1_n_6 ,\refclk_stable_count_reg[4]_i_1_n_7 }),
        .S(refclk_stable_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[5] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[6] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_5 ),
        .Q(refclk_stable_count_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[7] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[4]_i_1_n_4 ),
        .Q(refclk_stable_count_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[8] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_7 ),
        .Q(refclk_stable_count_reg[8]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \refclk_stable_count_reg[8]_i_1 
       (.CI(\refclk_stable_count_reg[4]_i_1_n_0 ),
        .CO({\refclk_stable_count_reg[8]_i_1_n_0 ,\refclk_stable_count_reg[8]_i_1_n_1 ,\refclk_stable_count_reg[8]_i_1_n_2 ,\refclk_stable_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refclk_stable_count_reg[8]_i_1_n_4 ,\refclk_stable_count_reg[8]_i_1_n_5 ,\refclk_stable_count_reg[8]_i_1_n_6 ,\refclk_stable_count_reg[8]_i_1_n_7 }),
        .S(refclk_stable_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \refclk_stable_count_reg[9] 
       (.C(independent_clock_bufg),
        .CE(refclk_stable_count),
        .D(\refclk_stable_count_reg[8]_i_1_n_6 ),
        .Q(refclk_stable_count_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    refclk_stable_i_1
       (.I0(\refclk_stable_count[0]_i_7_n_0 ),
        .I1(refclk_stable_i_2_n_0),
        .I2(refclk_stable_i_3_n_0),
        .I3(refclk_stable_i_4_n_0),
        .I4(refclk_stable_i_5_n_0),
        .I5(refclk_stable_i_6_n_0),
        .O(refclk_stable_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    refclk_stable_i_2
       (.I0(refclk_stable_count_reg[4]),
        .I1(refclk_stable_count_reg[5]),
        .I2(refclk_stable_count_reg[2]),
        .I3(refclk_stable_count_reg[3]),
        .I4(refclk_stable_count_reg[7]),
        .I5(refclk_stable_count_reg[6]),
        .O(refclk_stable_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000200000000000)) 
    refclk_stable_i_3
       (.I0(refclk_stable_count_reg[10]),
        .I1(refclk_stable_count_reg[11]),
        .I2(refclk_stable_count_reg[8]),
        .I3(refclk_stable_count_reg[9]),
        .I4(refclk_stable_count_reg[12]),
        .I5(refclk_stable_count_reg[13]),
        .O(refclk_stable_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    refclk_stable_i_4
       (.I0(refclk_stable_count_reg[16]),
        .I1(refclk_stable_count_reg[17]),
        .I2(refclk_stable_count_reg[14]),
        .I3(refclk_stable_count_reg[15]),
        .I4(refclk_stable_count_reg[18]),
        .I5(refclk_stable_count_reg[19]),
        .O(refclk_stable_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_5
       (.I0(refclk_stable_count_reg[22]),
        .I1(refclk_stable_count_reg[23]),
        .I2(refclk_stable_count_reg[20]),
        .I3(refclk_stable_count_reg[21]),
        .I4(refclk_stable_count_reg[25]),
        .I5(refclk_stable_count_reg[24]),
        .O(refclk_stable_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    refclk_stable_i_6
       (.I0(refclk_stable_count_reg[28]),
        .I1(refclk_stable_count_reg[29]),
        .I2(refclk_stable_count_reg[26]),
        .I3(refclk_stable_count_reg[27]),
        .I4(refclk_stable_count_reg[31]),
        .I5(refclk_stable_count_reg[30]),
        .O(refclk_stable_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    refclk_stable_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(refclk_stable_i_1_n_0),
        .Q(refclk_stable_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h440000FF50505050)) 
    reset_time_out_i_2__0
       (.I0(tx_state[3]),
        .I1(txresetdone_s3),
        .I2(init_wait_done_reg_n_0),
        .I3(tx_state[1]),
        .I4(tx_state[2]),
        .I5(tx_state[0]),
        .O(reset_time_out_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    reset_time_out_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(sync_cplllock_n_0),
        .Q(reset_time_out),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hFFFB0002)) 
    run_phase_alignment_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(run_phase_alignment_int_reg_n_0),
        .O(run_phase_alignment_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(run_phase_alignment_int_i_1_n_0),
        .Q(run_phase_alignment_int_reg_n_0),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    run_phase_alignment_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(data_out),
        .Q(run_phase_alignment_int_s3),
        .R(1'b0));
  gig_ethernet_pcs_pma_0_sync_block_4 sync_TXRESETDONE
       (.data_out(txresetdone_s2),
        .data_sync_reg1_0(data_sync_reg1_0),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_5 sync_cplllock
       (.E(sync_cplllock_n_1),
        .\FSM_sequential_tx_state_reg[0] (\FSM_sequential_tx_state[3]_i_3_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_0 (\FSM_sequential_tx_state[3]_i_4_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_1 (\FSM_sequential_tx_state[3]_i_6_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_2 (time_out_2ms_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_3 (\FSM_sequential_tx_state[3]_i_7_n_0 ),
        .\FSM_sequential_tx_state_reg[0]_4 (pll_reset_asserted_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_5 (refclk_stable_reg_n_0),
        .\FSM_sequential_tx_state_reg[0]_6 (\FSM_sequential_tx_state[0]_i_3_n_0 ),
        .Q(tx_state),
        .data_sync_reg1_0(data_sync_reg1_2),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .reset_time_out(reset_time_out),
        .reset_time_out_reg(sync_cplllock_n_0),
        .reset_time_out_reg_0(reset_time_out_i_2__0_n_0),
        .reset_time_out_reg_1(init_wait_done_reg_n_0));
  gig_ethernet_pcs_pma_0_sync_block_6 sync_mmcm_lock_reclocked
       (.SR(sync_mmcm_lock_reclocked_n_0),
        .data_out(mmcm_lock_i),
        .data_sync_reg1_0(data_sync_reg1_1),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_7 sync_run_phase_alignment_int
       (.data_in(run_phase_alignment_int_reg_n_0),
        .data_out(data_out),
        .data_sync_reg6_0(data_sync_reg1));
  gig_ethernet_pcs_pma_0_sync_block_8 sync_time_out_wait_bypass
       (.data_in(time_out_wait_bypass_reg_n_0),
        .data_out(time_out_wait_bypass_s2),
        .independent_clock_bufg(independent_clock_bufg));
  gig_ethernet_pcs_pma_0_sync_block_9 sync_tx_fsm_reset_done_int
       (.data_in(data_in),
        .data_out(tx_fsm_reset_done_int_s2),
        .data_sync_reg1_0(data_sync_reg1));
  LUT4 #(
    .INIT(16'h00AE)) 
    time_out_2ms_i_1
       (.I0(time_out_2ms_reg_n_0),
        .I1(time_out_2ms_i_2__0_n_0),
        .I2(time_out_2ms_i_3_n_0),
        .I3(reset_time_out),
        .O(time_out_2ms_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    time_out_2ms_i_2__0
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[5]),
        .I5(time_tlock_max_i_3_n_0),
        .O(time_out_2ms_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    time_out_2ms_i_3
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_2ms_i_4__0_n_0),
        .I3(time_out_2ms_i_5_n_0),
        .O(time_out_2ms_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    time_out_2ms_i_4__0
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[6]),
        .O(time_out_2ms_i_4__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    time_out_2ms_i_5
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_counter_reg[1]),
        .O(time_out_2ms_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_2ms_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_2ms_i_1_n_0),
        .Q(time_out_2ms_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAEAAA)) 
    time_out_500us_i_1
       (.I0(time_out_500us_reg_n_0),
        .I1(time_out_500us_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_2ms_i_3_n_0),
        .I5(reset_time_out),
        .O(time_out_500us_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    time_out_500us_i_2
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[11]),
        .I3(time_out_counter_reg[12]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_out_500us_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_500us_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_500us_i_1_n_0),
        .Q(time_out_500us_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \time_out_counter[0]_i_1__0 
       (.I0(time_tlock_max_i_3_n_0),
        .I1(\time_out_counter[0]_i_3__0_n_0 ),
        .I2(time_out_2ms_i_3_n_0),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[12]),
        .I5(time_out_counter_reg[5]),
        .O(time_out_counter));
  LUT2 #(
    .INIT(4'h8)) 
    \time_out_counter[0]_i_3__0 
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[18]),
        .O(\time_out_counter[0]_i_3__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \time_out_counter[0]_i_4 
       (.I0(time_out_counter_reg[0]),
        .O(\time_out_counter[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[0] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_7 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\time_out_counter_reg[0]_i_2_n_0 ,\time_out_counter_reg[0]_i_2_n_1 ,\time_out_counter_reg[0]_i_2_n_2 ,\time_out_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\time_out_counter_reg[0]_i_2_n_4 ,\time_out_counter_reg[0]_i_2_n_5 ,\time_out_counter_reg[0]_i_2_n_6 ,\time_out_counter_reg[0]_i_2_n_7 }),
        .S({time_out_counter_reg[3:1],\time_out_counter[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[10] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_5 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[11] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_4 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[12] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_7 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[12]_i_1 
       (.CI(\time_out_counter_reg[8]_i_1_n_0 ),
        .CO({\time_out_counter_reg[12]_i_1_n_0 ,\time_out_counter_reg[12]_i_1_n_1 ,\time_out_counter_reg[12]_i_1_n_2 ,\time_out_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[12]_i_1_n_4 ,\time_out_counter_reg[12]_i_1_n_5 ,\time_out_counter_reg[12]_i_1_n_6 ,\time_out_counter_reg[12]_i_1_n_7 }),
        .S(time_out_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[13] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_6 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[14] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_5 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[15] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[12]_i_1_n_4 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[16] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_7 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[16]_i_1 
       (.CI(\time_out_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED [3:2],\time_out_counter_reg[16]_i_1_n_2 ,\time_out_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED [3],\time_out_counter_reg[16]_i_1_n_5 ,\time_out_counter_reg[16]_i_1_n_6 ,\time_out_counter_reg[16]_i_1_n_7 }),
        .S({1'b0,time_out_counter_reg[18:16]}));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[17] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_6 ),
        .Q(time_out_counter_reg[17]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[18] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[16]_i_1_n_5 ),
        .Q(time_out_counter_reg[18]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[1] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_6 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[2] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_5 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[3] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[0]_i_2_n_4 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[4] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_7 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[4]_i_1 
       (.CI(\time_out_counter_reg[0]_i_2_n_0 ),
        .CO({\time_out_counter_reg[4]_i_1_n_0 ,\time_out_counter_reg[4]_i_1_n_1 ,\time_out_counter_reg[4]_i_1_n_2 ,\time_out_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[4]_i_1_n_4 ,\time_out_counter_reg[4]_i_1_n_5 ,\time_out_counter_reg[4]_i_1_n_6 ,\time_out_counter_reg[4]_i_1_n_7 }),
        .S(time_out_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[5] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_6 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[6] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_5 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[7] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[4]_i_1_n_4 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[8] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_7 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \time_out_counter_reg[8]_i_1 
       (.CI(\time_out_counter_reg[4]_i_1_n_0 ),
        .CO({\time_out_counter_reg[8]_i_1_n_0 ,\time_out_counter_reg[8]_i_1_n_1 ,\time_out_counter_reg[8]_i_1_n_2 ,\time_out_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\time_out_counter_reg[8]_i_1_n_4 ,\time_out_counter_reg[8]_i_1_n_5 ,\time_out_counter_reg[8]_i_1_n_6 ,\time_out_counter_reg[8]_i_1_n_7 }),
        .S(time_out_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \time_out_counter_reg[9] 
       (.C(independent_clock_bufg),
        .CE(time_out_counter),
        .D(\time_out_counter_reg[8]_i_1_n_6 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out));
  LUT4 #(
    .INIT(16'hAB00)) 
    time_out_wait_bypass_i_1
       (.I0(time_out_wait_bypass_reg_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .I2(time_out_wait_bypass_i_2_n_0),
        .I3(run_phase_alignment_int_s3),
        .O(time_out_wait_bypass_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFFF)) 
    time_out_wait_bypass_i_2
       (.I0(time_out_wait_bypass_i_3_n_0),
        .I1(time_out_wait_bypass_i_4_n_0),
        .I2(wait_bypass_count_reg[5]),
        .I3(wait_bypass_count_reg[13]),
        .I4(wait_bypass_count_reg[11]),
        .I5(time_out_wait_bypass_i_5_n_0),
        .O(time_out_wait_bypass_i_2_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    time_out_wait_bypass_i_3
       (.I0(wait_bypass_count_reg[16]),
        .I1(wait_bypass_count_reg[9]),
        .I2(wait_bypass_count_reg[12]),
        .I3(wait_bypass_count_reg[10]),
        .O(time_out_wait_bypass_i_3_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    time_out_wait_bypass_i_4
       (.I0(wait_bypass_count_reg[4]),
        .I1(wait_bypass_count_reg[15]),
        .I2(wait_bypass_count_reg[6]),
        .I3(wait_bypass_count_reg[0]),
        .O(time_out_wait_bypass_i_4_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    time_out_wait_bypass_i_5
       (.I0(wait_bypass_count_reg[8]),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[7]),
        .I3(wait_bypass_count_reg[14]),
        .I4(wait_bypass_count_reg[2]),
        .I5(wait_bypass_count_reg[3]),
        .O(time_out_wait_bypass_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(time_out_wait_bypass_i_1_n_0),
        .Q(time_out_wait_bypass_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    time_out_wait_bypass_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAAAEA)) 
    time_tlock_max_i_1
       (.I0(time_tlock_max_reg_n_0),
        .I1(time_tlock_max_i_2_n_0),
        .I2(time_out_counter_reg[5]),
        .I3(time_tlock_max_i_3_n_0),
        .I4(time_tlock_max_i_4_n_0),
        .I5(reset_time_out),
        .O(time_tlock_max_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    time_tlock_max_i_2
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[10]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[18]),
        .I5(time_out_counter_reg[17]),
        .O(time_tlock_max_i_2_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    time_tlock_max_i_3
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[15]),
        .I2(time_out_counter_reg[11]),
        .O(time_tlock_max_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    time_tlock_max_i_4
       (.I0(time_out_2ms_i_5_n_0),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[3]),
        .I4(time_out_counter_reg[4]),
        .O(time_tlock_max_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    time_tlock_max_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(time_tlock_max_i_1_n_0),
        .Q(time_tlock_max_reg_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFF1000)) 
    tx_fsm_reset_done_int_i_1
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(tx_state[3]),
        .I4(data_in),
        .O(tx_fsm_reset_done_int_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_i_1_n_0),
        .Q(data_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    tx_fsm_reset_done_int_s3_reg
       (.C(data_sync_reg1),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_s2),
        .Q(tx_fsm_reset_done_int_s3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txresetdone_s3_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(txresetdone_s2),
        .Q(txresetdone_s3),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_1 
       (.I0(run_phase_alignment_int_s3),
        .O(clear));
  LUT2 #(
    .INIT(4'h2)) 
    \wait_bypass_count[0]_i_2 
       (.I0(time_out_wait_bypass_i_2_n_0),
        .I1(tx_fsm_reset_done_int_s3),
        .O(\wait_bypass_count[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \wait_bypass_count[0]_i_4__0 
       (.I0(wait_bypass_count_reg[0]),
        .O(\wait_bypass_count[0]_i_4__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[0] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_7 ),
        .Q(wait_bypass_count_reg[0]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\wait_bypass_count_reg[0]_i_3_n_0 ,\wait_bypass_count_reg[0]_i_3_n_1 ,\wait_bypass_count_reg[0]_i_3_n_2 ,\wait_bypass_count_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\wait_bypass_count_reg[0]_i_3_n_4 ,\wait_bypass_count_reg[0]_i_3_n_5 ,\wait_bypass_count_reg[0]_i_3_n_6 ,\wait_bypass_count_reg[0]_i_3_n_7 }),
        .S({wait_bypass_count_reg[3:1],\wait_bypass_count[0]_i_4__0_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[10] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[11] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[12] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[12]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[12]_i_1 
       (.CI(\wait_bypass_count_reg[8]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[12]_i_1_n_0 ,\wait_bypass_count_reg[12]_i_1_n_1 ,\wait_bypass_count_reg[12]_i_1_n_2 ,\wait_bypass_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[12]_i_1_n_4 ,\wait_bypass_count_reg[12]_i_1_n_5 ,\wait_bypass_count_reg[12]_i_1_n_6 ,\wait_bypass_count_reg[12]_i_1_n_7 }),
        .S(wait_bypass_count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[13] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[14] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[15] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[12]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[16] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[16]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[16]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[16]_i_1 
       (.CI(\wait_bypass_count_reg[12]_i_1_n_0 ),
        .CO(\NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED [3:1],\wait_bypass_count_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,wait_bypass_count_reg[16]}));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[1] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_6 ),
        .Q(wait_bypass_count_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[2] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_5 ),
        .Q(wait_bypass_count_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[3] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[0]_i_3_n_4 ),
        .Q(wait_bypass_count_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[4] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[4]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[4]_i_1 
       (.CI(\wait_bypass_count_reg[0]_i_3_n_0 ),
        .CO({\wait_bypass_count_reg[4]_i_1_n_0 ,\wait_bypass_count_reg[4]_i_1_n_1 ,\wait_bypass_count_reg[4]_i_1_n_2 ,\wait_bypass_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[4]_i_1_n_4 ,\wait_bypass_count_reg[4]_i_1_n_5 ,\wait_bypass_count_reg[4]_i_1_n_6 ,\wait_bypass_count_reg[4]_i_1_n_7 }),
        .S(wait_bypass_count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[5] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[6] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_5 ),
        .Q(wait_bypass_count_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[7] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[4]_i_1_n_4 ),
        .Q(wait_bypass_count_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[8] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_7 ),
        .Q(wait_bypass_count_reg[8]),
        .R(clear));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \wait_bypass_count_reg[8]_i_1 
       (.CI(\wait_bypass_count_reg[4]_i_1_n_0 ),
        .CO({\wait_bypass_count_reg[8]_i_1_n_0 ,\wait_bypass_count_reg[8]_i_1_n_1 ,\wait_bypass_count_reg[8]_i_1_n_2 ,\wait_bypass_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\wait_bypass_count_reg[8]_i_1_n_4 ,\wait_bypass_count_reg[8]_i_1_n_5 ,\wait_bypass_count_reg[8]_i_1_n_6 ,\wait_bypass_count_reg[8]_i_1_n_7 }),
        .S(wait_bypass_count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \wait_bypass_count_reg[9] 
       (.C(data_sync_reg1),
        .CE(\wait_bypass_count[0]_i_2_n_0 ),
        .D(\wait_bypass_count_reg[8]_i_1_n_6 ),
        .Q(wait_bypass_count_reg[9]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \wait_time_cnt[0]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .O(wait_time_cnt0));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[1]_i_1 
       (.I0(wait_time_cnt_reg[0]),
        .I1(wait_time_cnt_reg[1]),
        .O(\wait_time_cnt[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \wait_time_cnt[2]_i_1 
       (.I0(wait_time_cnt_reg[1]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[2]),
        .O(\wait_time_cnt[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \wait_time_cnt[3]_i_1 
       (.I0(wait_time_cnt_reg[2]),
        .I1(wait_time_cnt_reg[0]),
        .I2(wait_time_cnt_reg[1]),
        .I3(wait_time_cnt_reg[3]),
        .O(\wait_time_cnt[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \wait_time_cnt[4]_i_1 
       (.I0(wait_time_cnt_reg[3]),
        .I1(wait_time_cnt_reg[1]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[2]),
        .I4(wait_time_cnt_reg[4]),
        .O(\wait_time_cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \wait_time_cnt[5]_i_1 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0700)) 
    \wait_time_cnt[6]_i_1__0 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .I2(tx_state[3]),
        .I3(tx_state[0]),
        .O(wait_time_cnt0_0));
  LUT2 #(
    .INIT(4'hE)) 
    \wait_time_cnt[6]_i_2 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(sel));
  LUT2 #(
    .INIT(4'h9)) 
    \wait_time_cnt[6]_i_3 
       (.I0(\wait_time_cnt[6]_i_4_n_0 ),
        .I1(wait_time_cnt_reg[6]),
        .O(\wait_time_cnt[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \wait_time_cnt[6]_i_4 
       (.I0(wait_time_cnt_reg[4]),
        .I1(wait_time_cnt_reg[2]),
        .I2(wait_time_cnt_reg[0]),
        .I3(wait_time_cnt_reg[1]),
        .I4(wait_time_cnt_reg[3]),
        .I5(wait_time_cnt_reg[5]),
        .O(\wait_time_cnt[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[0] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(wait_time_cnt0),
        .Q(wait_time_cnt_reg[0]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[1] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[1]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[1]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[2] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[2]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[2]),
        .S(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[3] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[3]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[3]),
        .R(wait_time_cnt0_0));
  FDRE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[4] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[4]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[4]),
        .R(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[5] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[5]_i_1_n_0 ),
        .Q(wait_time_cnt_reg[5]),
        .S(wait_time_cnt0_0));
  FDSE #(
    .INIT(1'b0)) 
    \wait_time_cnt_reg[6] 
       (.C(independent_clock_bufg),
        .CE(sel),
        .D(\wait_time_cnt[6]_i_3_n_0 ),
        .Q(wait_time_cnt_reg[6]),
        .S(wait_time_cnt0_0));
endmodule

module gig_ethernet_pcs_pma_0_block
   (gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    status_vector,
    resetdone,
    txn,
    txp,
    rxoutclk,
    txoutclk,
    mmcm_reset,
    out,
    signal_detect,
    CLK,
    data_in,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    configuration_vector,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i);
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  output [6:0]status_vector;
  output resetdone;
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output mmcm_reset;
  input [0:0]out;
  input signal_detect;
  input CLK;
  input data_in;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  input [2:0]configuration_vector;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;

  wire CLK;
  wire [2:0]configuration_vector;
  wire data_in;
  wire enablealign;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mgt_rx_reset;
  wire mgt_tx_reset;
  wire mmcm_reset;
  wire [0:0]out;
  wire powerdown;
  wire resetdone;
  wire rx_reset_done_i;
  wire rxbuferr;
  wire rxchariscomma;
  wire rxcharisk;
  wire [1:0]rxclkcorcnt;
  wire [7:0]rxdata;
  wire rxdisperr;
  wire rxn;
  wire rxnotintable;
  wire rxoutclk;
  wire rxp;
  wire signal_detect;
  wire [6:0]status_vector;
  wire transceiver_inst_n_11;
  wire transceiver_inst_n_12;
  wire txbuferr;
  wire txchardispmode;
  wire txchardispval;
  wire txcharisk;
  wire [7:0]txdata;
  wire txn;
  wire txoutclk;
  wire txp;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED;
  wire NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED;
  wire [15:0]NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED;
  wire [63:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED;
  wire [47:0]NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED;
  wire [31:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED;
  wire [1:0]NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED;
  wire [15:7]NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED;
  wire [9:0]NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED;

  (* B_SHIFTER_ADDR = "10'b0101001110" *) 
  (* C_1588 = "0" *) 
  (* C_2_5G = "FALSE" *) 
  (* C_COMPONENT_NAME = "gig_ethernet_pcs_pma_0" *) 
  (* C_DYNAMIC_SWITCHING = "FALSE" *) 
  (* C_ELABORATION_TRANSIENT_DIR = "BlankString" *) 
  (* C_FAMILY = "kintex7" *) 
  (* C_HAS_AN = "FALSE" *) 
  (* C_HAS_AXIL = "FALSE" *) 
  (* C_HAS_MDIO = "FALSE" *) 
  (* C_HAS_TEMAC = "TRUE" *) 
  (* C_IS_SGMII = "FALSE" *) 
  (* C_RX_GMII_CLK = "TXOUTCLK" *) 
  (* C_SGMII_FABRIC_BUFFER = "TRUE" *) 
  (* C_SGMII_PHY_MODE = "FALSE" *) 
  (* C_USE_LVDS = "FALSE" *) 
  (* C_USE_TBI = "FALSE" *) 
  (* C_USE_TRANSCEIVER = "TRUE" *) 
  (* GT_RX_BYTE_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  gig_ethernet_pcs_pma_0_gig_ethernet_pcs_pma_v16_2_18 gig_ethernet_pcs_pma_0_core
       (.an_adv_config_val(1'b0),
        .an_adv_config_vector({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .an_enable(NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED),
        .an_interrupt(NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED),
        .an_restart_config(1'b0),
        .basex_or_sgmii(1'b0),
        .configuration_valid(1'b0),
        .configuration_vector({1'b0,configuration_vector,1'b0}),
        .correction_timer({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dcm_locked(data_in),
        .drp_daddr(NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED[9:0]),
        .drp_dclk(1'b0),
        .drp_den(NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED),
        .drp_di(NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED[15:0]),
        .drp_do({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .drp_drdy(1'b0),
        .drp_dwe(NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED),
        .drp_gnt(1'b0),
        .drp_req(NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED),
        .en_cdet(NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED),
        .enablealign(enablealign),
        .ewrap(NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gtx_clk(1'b0),
        .link_timer_basex({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_sgmii({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .link_timer_value({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .loc_ref(NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED),
        .mdc(1'b0),
        .mdio_in(1'b0),
        .mdio_out(NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED),
        .mdio_tri(NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED),
        .mgt_rx_reset(mgt_rx_reset),
        .mgt_tx_reset(mgt_tx_reset),
        .phyad({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pma_rx_clk0(1'b0),
        .pma_rx_clk1(1'b0),
        .powerdown(powerdown),
        .reset(out),
        .reset_done(resetdone),
        .rx_code_group0({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_code_group1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rx_gt_nominal_latency({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0,1'b0}),
        .rxbufstatus({rxbuferr,1'b0}),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .rxclkcorcnt({1'b0,rxclkcorcnt}),
        .rxdata(rxdata),
        .rxdisperr(rxdisperr),
        .rxnotintable(rxnotintable),
        .rxphy_correction_timer(NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED[63:0]),
        .rxphy_ns_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED[31:0]),
        .rxphy_s_field(NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED[47:0]),
        .rxrecclk(1'b0),
        .rxrundisp(1'b0),
        .s_axi_aclk(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED),
        .s_axi_awvalid(1'b0),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_resetn(1'b0),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wready(NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED),
        .s_axi_wvalid(1'b0),
        .signal_detect(signal_detect),
        .speed_is_100(1'b0),
        .speed_is_10_100(1'b0),
        .speed_selection(NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED[1:0]),
        .status_vector({NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED[15:7],status_vector}),
        .systemtimer_ns_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .systemtimer_s_field({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .tx_code_group(NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED[9:0]),
        .txbuferr(txbuferr),
        .txchardispmode(txchardispmode),
        .txchardispval(txchardispval),
        .txcharisk(txcharisk),
        .txdata(txdata),
        .userclk(1'b0),
        .userclk2(CLK));
  gig_ethernet_pcs_pma_0_sync_block sync_block_rx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_12),
        .data_out(rx_reset_done_i));
  gig_ethernet_pcs_pma_0_sync_block_0 sync_block_tx_reset_done
       (.CLK(CLK),
        .data_in(transceiver_inst_n_11),
        .resetdone(resetdone),
        .resetdone_0(rx_reset_done_i));
  gig_ethernet_pcs_pma_0_transceiver transceiver_inst
       (.CLK(CLK),
        .D(txchardispmode),
        .Q(rxclkcorcnt),
        .SR(mgt_rx_reset),
        .data_in(transceiver_inst_n_11),
        .data_sync_reg1(data_in),
        .enablealign(enablealign),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(gtxe2_i),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .powerdown(powerdown),
        .reset_sync5(mgt_tx_reset),
        .rx_fsm_reset_done_int_reg(transceiver_inst_n_12),
        .rxbufstatus(rxbuferr),
        .rxchariscomma(rxchariscomma),
        .rxcharisk(rxcharisk),
        .\rxdata_reg[7]_0 (rxdata),
        .rxdisperr(rxdisperr),
        .rxn(rxn),
        .rxnotintable(rxnotintable),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .status_vector(status_vector[1]),
        .txbuferr(txbuferr),
        .txchardispval_reg_reg_0(txchardispval),
        .txcharisk_reg_reg_0(txcharisk),
        .\txdata_reg_reg[7]_0 (txdata),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_clocking
   (gtrefclk_out,
    gtrefclk_bufg,
    mmcm_locked,
    userclk,
    userclk2,
    rxuserclk2_out,
    gtrefclk_p,
    gtrefclk_n,
    txoutclk,
    mmcm_reset,
    rxoutclk);
  output gtrefclk_out;
  output gtrefclk_bufg;
  output mmcm_locked;
  output userclk;
  output userclk2;
  output rxuserclk2_out;
  input gtrefclk_p;
  input gtrefclk_n;
  input txoutclk;
  input mmcm_reset;
  input rxoutclk;

  wire clkfbout;
  wire clkout0;
  wire clkout1;
  wire gtrefclk_bufg;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire mmcm_locked;
  wire mmcm_reset;
  wire rxoutclk;
  wire rxuserclk2_out;
  wire txoutclk;
  wire txoutclk_bufg;
  wire userclk;
  wire userclk2;
  wire NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_gtrefclk
       (.I(gtrefclk_out),
        .O(gtrefclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_txoutclk
       (.I(txoutclk),
        .O(txoutclk_bufg));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk
       (.I(clkout1),
        .O(userclk));
  (* box_type = "PRIMITIVE" *) 
  BUFG bufg_userclk2
       (.I(clkout0),
        .O(userclk2));
  (* box_type = "PRIMITIVE" *) 
  IBUFDS_GTE2 #(
    .CLKCM_CFG("TRUE"),
    .CLKRCV_TRST("TRUE"),
    .CLKSWING_CFG(2'b11)) 
    ibufds_gtrefclk
       (.CEB(1'b0),
        .I(gtrefclk_p),
        .IB(gtrefclk_n),
        .O(gtrefclk_out),
        .ODIV2(NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED));
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(16.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(16.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(8.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(16),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("INTERNAL"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.000000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout),
        .CLKFBOUT(clkfbout),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(txoutclk_bufg),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clkout0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clkout1),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(mmcm_locked),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(mmcm_reset));
  (* box_type = "PRIMITIVE" *) 
  BUFG rxrecclkbufg
       (.I(rxoutclk),
        .O(rxuserclk2_out));
endmodule

module gig_ethernet_pcs_pma_0_cpll_railing
   (cpll_pd0_i,
    cpllreset_in,
    gtrefclk_bufg,
    gt0_cpllreset_t);
  output cpll_pd0_i;
  output cpllreset_in;
  input gtrefclk_bufg;
  input gt0_cpllreset_t;

  wire cpll_pd0_i;
  wire cpll_reset_out;
  wire \cpllpd_wait_reg[31]_srl32_n_1 ;
  wire \cpllpd_wait_reg[63]_srl32_n_1 ;
  wire \cpllpd_wait_reg[94]_srl31_n_0 ;
  wire cpllreset_in;
  wire \cpllreset_wait_reg[126]_srl31_n_0 ;
  wire \cpllreset_wait_reg[31]_srl32_n_1 ;
  wire \cpllreset_wait_reg[63]_srl32_n_1 ;
  wire \cpllreset_wait_reg[95]_srl32_n_1 ;
  wire gt0_cpllreset_t;
  wire gtrefclk_bufg;
  wire \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ;
  wire \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ;

  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'hFFFFFFFF)) 
    \cpllpd_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllpd_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h7FFFFFFF)) 
    \cpllpd_wait_reg[94]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllpd_wait_reg[63]_srl32_n_1 ),
        .Q(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q31(\NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b1)) 
    \cpllpd_wait_reg[95] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllpd_wait_reg[94]_srl31_n_0 ),
        .Q(cpll_pd0_i),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[126]_srl31 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b0}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[95]_srl32_n_1 ),
        .Q(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q31(\NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED ));
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    \cpllreset_wait_reg[127] 
       (.C(gtrefclk_bufg),
        .CE(1'b1),
        .D(\cpllreset_wait_reg[126]_srl31_n_0 ),
        .Q(cpll_reset_out),
        .R(1'b0));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h000000FF)) 
    \cpllreset_wait_reg[31]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(1'b0),
        .Q(\NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[31]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[63]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[31]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[63]_srl32_n_1 ));
  (* srl_bus_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg " *) 
  (* srl_name = "U0/\\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 " *) 
  SRLC32E #(
    .INIT(32'h00000000)) 
    \cpllreset_wait_reg[95]_srl32 
       (.A({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CE(1'b1),
        .CLK(gtrefclk_bufg),
        .D(\cpllreset_wait_reg[63]_srl32_n_1 ),
        .Q(\NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED ),
        .Q31(\cpllreset_wait_reg[95]_srl32_n_1 ));
  LUT2 #(
    .INIT(4'hE)) 
    gtxe2_i_i_1
       (.I0(cpll_reset_out),
        .I1(gt0_cpllreset_t),
        .O(cpllreset_in));
endmodule

module gig_ethernet_pcs_pma_0_gt_common
   (gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtrefclk_out,
    independent_clock_bufg,
    out);
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;
  input gtrefclk_out;
  input independent_clock_bufg;
  input [0:0]out;

  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_out;
  wire gtxe2_common_i_n_2;
  wire gtxe2_common_i_n_5;
  wire independent_clock_bufg;
  wire [0:0]out;
  wire NLW_gtxe2_common_i_DRPRDY_UNCONNECTED;
  wire NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED;
  wire NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED;
  wire [15:0]NLW_gtxe2_common_i_DRPDO_UNCONNECTED;
  wire [7:0]NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  GTXE2_COMMON #(
    .BIAS_CFG(64'h0000040000001000),
    .COMMON_CFG(32'h00000000),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK_INVERTED(1'b0),
    .IS_QPLLLOCKDETCLK_INVERTED(1'b0),
    .QPLL_CFG(27'h06801C1),
    .QPLL_CLKOUT_CFG(4'b0000),
    .QPLL_COARSE_FREQ_OVRD(6'b010000),
    .QPLL_COARSE_FREQ_OVRD_EN(1'b0),
    .QPLL_CP(10'b0000011111),
    .QPLL_CP_MONITOR_EN(1'b0),
    .QPLL_DMONITOR_SEL(1'b0),
    .QPLL_FBDIV(10'b0000100000),
    .QPLL_FBDIV_MONITOR_EN(1'b0),
    .QPLL_FBDIV_RATIO(1'b1),
    .QPLL_INIT_CFG(24'h000006),
    .QPLL_LOCK_CFG(16'h21E8),
    .QPLL_LPF(4'b1111),
    .QPLL_REFCLK_DIV(1),
    .SIM_QPLLREFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("FALSE"),
    .SIM_VERSION("4.0")) 
    gtxe2_common_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO(NLW_gtxe2_common_i_DRPDO_UNCONNECTED[15:0]),
        .DRPEN(1'b0),
        .DRPRDY(NLW_gtxe2_common_i_DRPRDY_UNCONNECTED),
        .DRPWE(1'b0),
        .GTGREFCLK(1'b0),
        .GTNORTHREFCLK0(1'b0),
        .GTNORTHREFCLK1(1'b0),
        .GTREFCLK0(gtrefclk_out),
        .GTREFCLK1(1'b0),
        .GTSOUTHREFCLK0(1'b0),
        .GTSOUTHREFCLK1(1'b0),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLDMONITOR(NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED[7:0]),
        .QPLLFBCLKLOST(NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED),
        .QPLLLOCK(gtxe2_common_i_n_2),
        .QPLLLOCKDETCLK(independent_clock_bufg),
        .QPLLLOCKEN(1'b1),
        .QPLLOUTCLK(gt0_qplloutclk_out),
        .QPLLOUTREFCLK(gt0_qplloutrefclk_out),
        .QPLLOUTRESET(1'b0),
        .QPLLPD(1'b1),
        .QPLLREFCLKLOST(gtxe2_common_i_n_5),
        .QPLLREFCLKSEL({1'b0,1'b0,1'b1}),
        .QPLLRESET(out),
        .QPLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .QPLLRSVD2({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR(NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED));
endmodule

module gig_ethernet_pcs_pma_0_reset_sync
   (reset_out,
    CLK,
    enablealign);
  output reset_out;
  input CLK;
  input enablealign;

  wire CLK;
  wire enablealign;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b0),
        .PRE(enablealign),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(enablealign),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(enablealign),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(enablealign),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(enablealign),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(CLK),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module gig_ethernet_pcs_pma_0_reset_sync_1
   (reset_out,
    independent_clock_bufg,
    SR);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]SR;

  wire [0:0]SR;
  wire independent_clock_bufg;
  wire reset_out;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(SR),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(SR),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(SR),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(SR),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(SR),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_reset_sync" *) 
module gig_ethernet_pcs_pma_0_reset_sync_2
   (reset_out,
    independent_clock_bufg,
    reset_sync5_0);
  output reset_out;
  input independent_clock_bufg;
  input [0:0]reset_sync5_0;

  wire independent_clock_bufg;
  wire reset_out;
  wire [0:0]reset_sync5_0;
  wire reset_sync_reg1;
  wire reset_sync_reg2;
  wire reset_sync_reg3;
  wire reset_sync_reg4;
  wire reset_sync_reg5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg1));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg1),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg2));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg2),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg3));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg3),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg4));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg4),
        .PRE(reset_sync5_0),
        .Q(reset_sync_reg5));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FDP" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE" *) 
  (* box_type = "PRIMITIVE" *) 
  FDPE #(
    .INIT(1'b1)) 
    reset_sync6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset_sync_reg5),
        .PRE(1'b0),
        .Q(reset_out));
endmodule

module gig_ethernet_pcs_pma_0_reset_wtd_timer
   (reset,
    independent_clock_bufg,
    data_out);
  output reset;
  input independent_clock_bufg;
  input data_out;

  wire \counter_stg1[5]_i_1_n_0 ;
  wire \counter_stg1[5]_i_3_n_0 ;
  wire [5:5]counter_stg1_reg;
  wire [4:0]counter_stg1_reg__0;
  wire \counter_stg2[0]_i_3_n_0 ;
  wire [11:0]counter_stg2_reg;
  wire \counter_stg2_reg[0]_i_2_n_0 ;
  wire \counter_stg2_reg[0]_i_2_n_1 ;
  wire \counter_stg2_reg[0]_i_2_n_2 ;
  wire \counter_stg2_reg[0]_i_2_n_3 ;
  wire \counter_stg2_reg[0]_i_2_n_4 ;
  wire \counter_stg2_reg[0]_i_2_n_5 ;
  wire \counter_stg2_reg[0]_i_2_n_6 ;
  wire \counter_stg2_reg[0]_i_2_n_7 ;
  wire \counter_stg2_reg[4]_i_1_n_0 ;
  wire \counter_stg2_reg[4]_i_1_n_1 ;
  wire \counter_stg2_reg[4]_i_1_n_2 ;
  wire \counter_stg2_reg[4]_i_1_n_3 ;
  wire \counter_stg2_reg[4]_i_1_n_4 ;
  wire \counter_stg2_reg[4]_i_1_n_5 ;
  wire \counter_stg2_reg[4]_i_1_n_6 ;
  wire \counter_stg2_reg[4]_i_1_n_7 ;
  wire \counter_stg2_reg[8]_i_1_n_1 ;
  wire \counter_stg2_reg[8]_i_1_n_2 ;
  wire \counter_stg2_reg[8]_i_1_n_3 ;
  wire \counter_stg2_reg[8]_i_1_n_4 ;
  wire \counter_stg2_reg[8]_i_1_n_5 ;
  wire \counter_stg2_reg[8]_i_1_n_6 ;
  wire \counter_stg2_reg[8]_i_1_n_7 ;
  wire counter_stg30;
  wire \counter_stg3[0]_i_3_n_0 ;
  wire \counter_stg3[0]_i_4_n_0 ;
  wire \counter_stg3[0]_i_5_n_0 ;
  wire [11:0]counter_stg3_reg;
  wire \counter_stg3_reg[0]_i_2_n_0 ;
  wire \counter_stg3_reg[0]_i_2_n_1 ;
  wire \counter_stg3_reg[0]_i_2_n_2 ;
  wire \counter_stg3_reg[0]_i_2_n_3 ;
  wire \counter_stg3_reg[0]_i_2_n_4 ;
  wire \counter_stg3_reg[0]_i_2_n_5 ;
  wire \counter_stg3_reg[0]_i_2_n_6 ;
  wire \counter_stg3_reg[0]_i_2_n_7 ;
  wire \counter_stg3_reg[4]_i_1_n_0 ;
  wire \counter_stg3_reg[4]_i_1_n_1 ;
  wire \counter_stg3_reg[4]_i_1_n_2 ;
  wire \counter_stg3_reg[4]_i_1_n_3 ;
  wire \counter_stg3_reg[4]_i_1_n_4 ;
  wire \counter_stg3_reg[4]_i_1_n_5 ;
  wire \counter_stg3_reg[4]_i_1_n_6 ;
  wire \counter_stg3_reg[4]_i_1_n_7 ;
  wire \counter_stg3_reg[8]_i_1_n_1 ;
  wire \counter_stg3_reg[8]_i_1_n_2 ;
  wire \counter_stg3_reg[8]_i_1_n_3 ;
  wire \counter_stg3_reg[8]_i_1_n_4 ;
  wire \counter_stg3_reg[8]_i_1_n_5 ;
  wire \counter_stg3_reg[8]_i_1_n_6 ;
  wire \counter_stg3_reg[8]_i_1_n_7 ;
  wire data_out;
  wire eqOp;
  wire independent_clock_bufg;
  wire [5:0]plusOp;
  wire reset;
  wire reset0;
  wire reset_i_2_n_0;
  wire reset_i_3_n_0;
  wire reset_i_4_n_0;
  wire reset_i_5_n_0;
  wire reset_i_6_n_0;
  wire [3:3]\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg1[0]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counter_stg1[1]_i_1 
       (.I0(counter_stg1_reg__0[0]),
        .I1(counter_stg1_reg__0[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \counter_stg1[2]_i_1 
       (.I0(counter_stg1_reg__0[1]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[2]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \counter_stg1[3]_i_1 
       (.I0(counter_stg1_reg__0[2]),
        .I1(counter_stg1_reg__0[0]),
        .I2(counter_stg1_reg__0[1]),
        .I3(counter_stg1_reg__0[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \counter_stg1[4]_i_1 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(plusOp[4]));
  LUT5 #(
    .INIT(32'hFFFF2000)) 
    \counter_stg1[5]_i_1 
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .I4(data_out),
        .O(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \counter_stg1[5]_i_2 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \counter_stg1[5]_i_3 
       (.I0(counter_stg1_reg__0[3]),
        .I1(counter_stg1_reg__0[1]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[2]),
        .I4(counter_stg1_reg__0[4]),
        .O(\counter_stg1[5]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(counter_stg1_reg__0[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(counter_stg1_reg__0[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(counter_stg1_reg__0[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(counter_stg1_reg__0[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[4] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(counter_stg1_reg__0[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg1_reg[5] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(plusOp[5]),
        .Q(counter_stg1_reg),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg2[0]_i_1 
       (.I0(counter_stg1_reg__0[4]),
        .I1(counter_stg1_reg__0[2]),
        .I2(counter_stg1_reg__0[0]),
        .I3(counter_stg1_reg__0[1]),
        .I4(counter_stg1_reg__0[3]),
        .I5(counter_stg1_reg),
        .O(eqOp));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg2[0]_i_3 
       (.I0(counter_stg2_reg[0]),
        .O(\counter_stg2[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[0] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_7 ),
        .Q(counter_stg2_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg2_reg[0]_i_2_n_0 ,\counter_stg2_reg[0]_i_2_n_1 ,\counter_stg2_reg[0]_i_2_n_2 ,\counter_stg2_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg2_reg[0]_i_2_n_4 ,\counter_stg2_reg[0]_i_2_n_5 ,\counter_stg2_reg[0]_i_2_n_6 ,\counter_stg2_reg[0]_i_2_n_7 }),
        .S({counter_stg2_reg[3:1],\counter_stg2[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[10] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_5 ),
        .Q(counter_stg2_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[11] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_4 ),
        .Q(counter_stg2_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[1] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_6 ),
        .Q(counter_stg2_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[2] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_5 ),
        .Q(counter_stg2_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[3] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[0]_i_2_n_4 ),
        .Q(counter_stg2_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[4] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_7 ),
        .Q(counter_stg2_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[4]_i_1 
       (.CI(\counter_stg2_reg[0]_i_2_n_0 ),
        .CO({\counter_stg2_reg[4]_i_1_n_0 ,\counter_stg2_reg[4]_i_1_n_1 ,\counter_stg2_reg[4]_i_1_n_2 ,\counter_stg2_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[4]_i_1_n_4 ,\counter_stg2_reg[4]_i_1_n_5 ,\counter_stg2_reg[4]_i_1_n_6 ,\counter_stg2_reg[4]_i_1_n_7 }),
        .S(counter_stg2_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[5] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_6 ),
        .Q(counter_stg2_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[6] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_5 ),
        .Q(counter_stg2_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[7] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[4]_i_1_n_4 ),
        .Q(counter_stg2_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[8] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_7 ),
        .Q(counter_stg2_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg2_reg[8]_i_1 
       (.CI(\counter_stg2_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg2_reg[8]_i_1_n_1 ,\counter_stg2_reg[8]_i_1_n_2 ,\counter_stg2_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg2_reg[8]_i_1_n_4 ,\counter_stg2_reg[8]_i_1_n_5 ,\counter_stg2_reg[8]_i_1_n_6 ,\counter_stg2_reg[8]_i_1_n_7 }),
        .S(counter_stg2_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg2_reg[9] 
       (.C(independent_clock_bufg),
        .CE(eqOp),
        .D(\counter_stg2_reg[8]_i_1_n_6 ),
        .Q(counter_stg2_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \counter_stg3[0]_i_1 
       (.I0(\counter_stg3[0]_i_3_n_0 ),
        .I1(\counter_stg3[0]_i_4_n_0 ),
        .I2(counter_stg2_reg[0]),
        .I3(\counter_stg1[5]_i_3_n_0 ),
        .O(counter_stg30));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_3 
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(\counter_stg3[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counter_stg3[0]_i_4 
       (.I0(counter_stg2_reg[9]),
        .I1(counter_stg2_reg[10]),
        .I2(counter_stg2_reg[7]),
        .I3(counter_stg2_reg[8]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(\counter_stg3[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter_stg3[0]_i_5 
       (.I0(counter_stg3_reg[0]),
        .O(\counter_stg3[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[0] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_7 ),
        .Q(counter_stg3_reg[0]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_stg3_reg[0]_i_2_n_0 ,\counter_stg3_reg[0]_i_2_n_1 ,\counter_stg3_reg[0]_i_2_n_2 ,\counter_stg3_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_stg3_reg[0]_i_2_n_4 ,\counter_stg3_reg[0]_i_2_n_5 ,\counter_stg3_reg[0]_i_2_n_6 ,\counter_stg3_reg[0]_i_2_n_7 }),
        .S({counter_stg3_reg[3:1],\counter_stg3[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[10] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_5 ),
        .Q(counter_stg3_reg[10]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[11] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_4 ),
        .Q(counter_stg3_reg[11]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[1] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_6 ),
        .Q(counter_stg3_reg[1]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[2] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_5 ),
        .Q(counter_stg3_reg[2]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[3] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[0]_i_2_n_4 ),
        .Q(counter_stg3_reg[3]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[4] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_7 ),
        .Q(counter_stg3_reg[4]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[4]_i_1 
       (.CI(\counter_stg3_reg[0]_i_2_n_0 ),
        .CO({\counter_stg3_reg[4]_i_1_n_0 ,\counter_stg3_reg[4]_i_1_n_1 ,\counter_stg3_reg[4]_i_1_n_2 ,\counter_stg3_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[4]_i_1_n_4 ,\counter_stg3_reg[4]_i_1_n_5 ,\counter_stg3_reg[4]_i_1_n_6 ,\counter_stg3_reg[4]_i_1_n_7 }),
        .S(counter_stg3_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[5] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_6 ),
        .Q(counter_stg3_reg[5]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[6] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_5 ),
        .Q(counter_stg3_reg[6]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[7] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[4]_i_1_n_4 ),
        .Q(counter_stg3_reg[7]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[8] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_7 ),
        .Q(counter_stg3_reg[8]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \counter_stg3_reg[8]_i_1 
       (.CI(\counter_stg3_reg[4]_i_1_n_0 ),
        .CO({\NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED [3],\counter_stg3_reg[8]_i_1_n_1 ,\counter_stg3_reg[8]_i_1_n_2 ,\counter_stg3_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_stg3_reg[8]_i_1_n_4 ,\counter_stg3_reg[8]_i_1_n_5 ,\counter_stg3_reg[8]_i_1_n_6 ,\counter_stg3_reg[8]_i_1_n_7 }),
        .S(counter_stg3_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_stg3_reg[9] 
       (.C(independent_clock_bufg),
        .CE(counter_stg30),
        .D(\counter_stg3_reg[8]_i_1_n_6 ),
        .Q(counter_stg3_reg[9]),
        .R(\counter_stg1[5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    reset_i_1
       (.I0(reset_i_2_n_0),
        .I1(counter_stg3_reg[0]),
        .I2(reset_i_3_n_0),
        .O(reset0));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    reset_i_2
       (.I0(counter_stg3_reg[9]),
        .I1(counter_stg3_reg[10]),
        .I2(counter_stg3_reg[7]),
        .I3(counter_stg3_reg[8]),
        .I4(counter_stg2_reg[0]),
        .I5(counter_stg3_reg[11]),
        .O(reset_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    reset_i_3
       (.I0(reset_i_4_n_0),
        .I1(reset_i_5_n_0),
        .I2(reset_i_6_n_0),
        .O(reset_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    reset_i_4
       (.I0(counter_stg2_reg[3]),
        .I1(counter_stg2_reg[4]),
        .I2(counter_stg2_reg[1]),
        .I3(counter_stg2_reg[2]),
        .I4(counter_stg2_reg[6]),
        .I5(counter_stg2_reg[5]),
        .O(reset_i_4_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    reset_i_5
       (.I0(counter_stg2_reg[10]),
        .I1(counter_stg2_reg[9]),
        .I2(counter_stg2_reg[8]),
        .I3(counter_stg2_reg[7]),
        .I4(counter_stg1_reg),
        .I5(counter_stg2_reg[11]),
        .O(reset_i_5_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    reset_i_6
       (.I0(counter_stg3_reg[4]),
        .I1(counter_stg3_reg[3]),
        .I2(counter_stg3_reg[1]),
        .I3(counter_stg3_reg[2]),
        .I4(counter_stg3_reg[6]),
        .I5(counter_stg3_reg[5]),
        .O(reset_i_6_n_0));
  FDRE reset_reg
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(reset0),
        .Q(reset),
        .R(1'b0));
endmodule

module gig_ethernet_pcs_pma_0_resets
   (out,
    independent_clock_bufg,
    reset);
  output [0:0]out;
  input independent_clock_bufg;
  input reset;

  wire independent_clock_bufg;
  (* async_reg = "true" *) wire [3:0]pma_reset_pipe;
  wire reset;

  assign out[0] = pma_reset_pipe[3];
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[0] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(1'b0),
        .PRE(reset),
        .Q(pma_reset_pipe[0]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[1] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[0]),
        .PRE(reset),
        .Q(pma_reset_pipe[1]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[2] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[1]),
        .PRE(reset),
        .Q(pma_reset_pipe[2]));
  (* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDPE \pma_reset_pipe_reg[3] 
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(pma_reset_pipe[2]),
        .PRE(reset),
        .Q(pma_reset_pipe[3]));
endmodule

(* EXAMPLE_SIMULATION = "0" *) (* downgradeipidentifiedwarnings = "yes" *) 
module gig_ethernet_pcs_pma_0_support
   (gtrefclk_p,
    gtrefclk_n,
    gtrefclk_out,
    gtrefclk_bufg_out,
    txp,
    txn,
    rxp,
    rxn,
    userclk_out,
    userclk2_out,
    rxuserclk_out,
    rxuserclk2_out,
    pma_reset_out,
    mmcm_locked_out,
    resetdone,
    independent_clock_bufg,
    gmii_txd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_rxd,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_isolate,
    configuration_vector,
    status_vector,
    reset,
    signal_detect,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out);
  input gtrefclk_p;
  input gtrefclk_n;
  output gtrefclk_out;
  output gtrefclk_bufg_out;
  output txp;
  output txn;
  input rxp;
  input rxn;
  output userclk_out;
  output userclk2_out;
  output rxuserclk_out;
  output rxuserclk2_out;
  output pma_reset_out;
  output mmcm_locked_out;
  output resetdone;
  input independent_clock_bufg;
  input [7:0]gmii_txd;
  input gmii_tx_en;
  input gmii_tx_er;
  output [7:0]gmii_rxd;
  output gmii_rx_dv;
  output gmii_rx_er;
  output gmii_isolate;
  input [4:0]configuration_vector;
  output [15:0]status_vector;
  input reset;
  input signal_detect;
  output gt0_qplloutclk_out;
  output gt0_qplloutrefclk_out;

  wire \<const0> ;
  wire [4:0]configuration_vector;
  wire gmii_isolate;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg_out;
  wire gtrefclk_n;
  wire gtrefclk_out;
  wire gtrefclk_p;
  wire independent_clock_bufg;
  wire mmcm_locked_out;
  wire mmcm_reset;
  wire pma_reset_out;
  wire reset;
  wire resetdone;
  wire rxn;
  wire rxoutclk;
  wire rxp;
  wire rxuserclk2_out;
  wire signal_detect;
  wire [6:0]\^status_vector ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire userclk2_out;
  wire userclk_out;

  assign rxuserclk_out = rxuserclk2_out;
  assign status_vector[15] = \<const0> ;
  assign status_vector[14] = \<const0> ;
  assign status_vector[13] = \<const0> ;
  assign status_vector[12] = \<const0> ;
  assign status_vector[11] = \<const0> ;
  assign status_vector[10] = \<const0> ;
  assign status_vector[9] = \<const0> ;
  assign status_vector[8] = \<const0> ;
  assign status_vector[7] = \<const0> ;
  assign status_vector[6:0] = \^status_vector [6:0];
  GND GND
       (.G(\<const0> ));
  gig_ethernet_pcs_pma_0_clocking core_clocking_i
       (.gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_n(gtrefclk_n),
        .gtrefclk_out(gtrefclk_out),
        .gtrefclk_p(gtrefclk_p),
        .mmcm_locked(mmcm_locked_out),
        .mmcm_reset(mmcm_reset),
        .rxoutclk(rxoutclk),
        .rxuserclk2_out(rxuserclk2_out),
        .txoutclk(txoutclk),
        .userclk(userclk_out),
        .userclk2(userclk2_out));
  gig_ethernet_pcs_pma_0_gt_common core_gt_common_i
       (.gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_out(gtrefclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out));
  gig_ethernet_pcs_pma_0_resets core_resets_i
       (.independent_clock_bufg(independent_clock_bufg),
        .out(pma_reset_out),
        .reset(reset));
  gig_ethernet_pcs_pma_0_block pcs_pma_block_i
       (.CLK(userclk2_out),
        .configuration_vector(configuration_vector[3:1]),
        .data_in(mmcm_locked_out),
        .gmii_isolate(gmii_isolate),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg_out),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(userclk_out),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(pma_reset_out),
        .resetdone(resetdone),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .signal_detect(signal_detect),
        .status_vector(\^status_vector ),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
endmodule

module gig_ethernet_pcs_pma_0_sync_block
   (data_out,
    data_in,
    CLK);
  output data_out;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_0
   (resetdone,
    resetdone_0,
    data_in,
    CLK);
  output resetdone;
  input resetdone_0;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire resetdone;
  wire resetdone_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    resetdone_INST_0
       (.I0(data_out),
        .I1(resetdone_0),
        .O(resetdone));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_10
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_11
   (\FSM_sequential_rx_state_reg[1] ,
    Q,
    rxresetdone_s3,
    data_sync_reg1_0,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  input [2:0]Q;
  input rxresetdone_s3;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire \FSM_sequential_rx_state_reg[1] ;
  wire [2:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire rxresetdone_s3;

  LUT5 #(
    .INIT(32'h008F0080)) 
    \FSM_sequential_rx_state[3]_i_5 
       (.I0(Q[0]),
        .I1(rxresetdone_s3),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(cplllock_sync),
        .O(\FSM_sequential_rx_state_reg[1] ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_12
   (\FSM_sequential_rx_state_reg[1] ,
    rx_fsm_reset_done_int_reg,
    D,
    E,
    reset_time_out_reg,
    reset_time_out_reg_0,
    Q,
    reset_time_out_reg_1,
    reset_time_out_reg_2,
    data_in,
    \FSM_sequential_rx_state_reg[1]_0 ,
    rx_fsm_reset_done_int_reg_0,
    rx_fsm_reset_done_int_reg_1,
    \FSM_sequential_rx_state_reg[0] ,
    \FSM_sequential_rx_state_reg[0]_0 ,
    \FSM_sequential_rx_state_reg[0]_1 ,
    mmcm_lock_reclocked,
    \FSM_sequential_rx_state_reg[0]_2 ,
    time_out_wait_bypass_s3,
    \FSM_sequential_rx_state_reg[3] ,
    \FSM_sequential_rx_state_reg[0]_3 ,
    rx_fsm_reset_done_int_reg_2,
    rx_fsm_reset_done_int_reg_3,
    data_out,
    independent_clock_bufg);
  output \FSM_sequential_rx_state_reg[1] ;
  output rx_fsm_reset_done_int_reg;
  output [2:0]D;
  output [0:0]E;
  input reset_time_out_reg;
  input reset_time_out_reg_0;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input reset_time_out_reg_2;
  input data_in;
  input \FSM_sequential_rx_state_reg[1]_0 ;
  input rx_fsm_reset_done_int_reg_0;
  input rx_fsm_reset_done_int_reg_1;
  input \FSM_sequential_rx_state_reg[0] ;
  input \FSM_sequential_rx_state_reg[0]_0 ;
  input \FSM_sequential_rx_state_reg[0]_1 ;
  input mmcm_lock_reclocked;
  input \FSM_sequential_rx_state_reg[0]_2 ;
  input time_out_wait_bypass_s3;
  input \FSM_sequential_rx_state_reg[3] ;
  input \FSM_sequential_rx_state_reg[0]_3 ;
  input rx_fsm_reset_done_int_reg_2;
  input rx_fsm_reset_done_int_reg_3;
  input data_out;
  input independent_clock_bufg;

  wire [2:0]D;
  wire [0:0]E;
  wire \FSM_sequential_rx_state[0]_i_3_n_0 ;
  wire \FSM_sequential_rx_state[1]_i_2_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_4_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_6_n_0 ;
  wire \FSM_sequential_rx_state[3]_i_8_n_0 ;
  wire \FSM_sequential_rx_state_reg[0] ;
  wire \FSM_sequential_rx_state_reg[0]_0 ;
  wire \FSM_sequential_rx_state_reg[0]_1 ;
  wire \FSM_sequential_rx_state_reg[0]_2 ;
  wire \FSM_sequential_rx_state_reg[0]_3 ;
  wire \FSM_sequential_rx_state_reg[1] ;
  wire \FSM_sequential_rx_state_reg[1]_0 ;
  wire \FSM_sequential_rx_state_reg[3] ;
  wire [3:0]Q;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_valid_sync;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out_i_2_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;
  wire reset_time_out_reg_2;
  wire rx_fsm_reset_done_int;
  wire rx_fsm_reset_done_int_i_3_n_0;
  wire rx_fsm_reset_done_int_i_4_n_0;
  wire rx_fsm_reset_done_int_reg;
  wire rx_fsm_reset_done_int_reg_0;
  wire rx_fsm_reset_done_int_reg_1;
  wire rx_fsm_reset_done_int_reg_2;
  wire rx_fsm_reset_done_int_reg_3;
  wire time_out_wait_bypass_s3;

  LUT5 #(
    .INIT(32'hFFEFEFEF)) 
    \FSM_sequential_rx_state[0]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0]_2 ),
        .I1(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'h0200)) 
    \FSM_sequential_rx_state[0]_i_3 
       (.I0(Q[3]),
        .I1(reset_time_out_reg_2),
        .I2(data_valid_sync),
        .I3(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF24200400)) 
    \FSM_sequential_rx_state[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\FSM_sequential_rx_state[1]_i_2_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[1]_0 ),
        .O(D[1]));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_rx_state[1]_i_2 
       (.I0(data_valid_sync),
        .I1(rx_fsm_reset_done_int_reg_1),
        .O(\FSM_sequential_rx_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \FSM_sequential_rx_state[3]_i_1 
       (.I0(\FSM_sequential_rx_state_reg[0] ),
        .I1(\FSM_sequential_rx_state[3]_i_4_n_0 ),
        .I2(Q[0]),
        .I3(reset_time_out_reg),
        .I4(\FSM_sequential_rx_state[3]_i_6_n_0 ),
        .I5(\FSM_sequential_rx_state_reg[0]_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFFFFFFCCC0C4C4)) 
    \FSM_sequential_rx_state[3]_i_2 
       (.I0(time_out_wait_bypass_s3),
        .I1(Q[3]),
        .I2(Q[1]),
        .I3(\FSM_sequential_rx_state[3]_i_8_n_0 ),
        .I4(Q[0]),
        .I5(\FSM_sequential_rx_state_reg[3] ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAEFEA)) 
    \FSM_sequential_rx_state[3]_i_4 
       (.I0(\FSM_sequential_rx_state[0]_i_3_n_0 ),
        .I1(\FSM_sequential_rx_state_reg[0]_1 ),
        .I2(Q[2]),
        .I3(\FSM_sequential_rx_state_reg[0]_3 ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\FSM_sequential_rx_state[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h0CE20CCC)) 
    \FSM_sequential_rx_state[3]_i_6 
       (.I0(mmcm_lock_reclocked),
        .I1(Q[3]),
        .I2(data_valid_sync),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\FSM_sequential_rx_state[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \FSM_sequential_rx_state[3]_i_8 
       (.I0(rx_fsm_reset_done_int_reg_1),
        .I1(data_valid_sync),
        .I2(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state[3]_i_8_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_out),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_valid_sync),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEEEFFFFFEEEF0000)) 
    reset_time_out_i_1__0
       (.I0(reset_time_out_i_2_n_0),
        .I1(reset_time_out_reg),
        .I2(reset_time_out_reg_0),
        .I3(Q[1]),
        .I4(reset_time_out_reg_1),
        .I5(reset_time_out_reg_2),
        .O(\FSM_sequential_rx_state_reg[1] ));
  LUT6 #(
    .INIT(64'h0FF30E0E0FF30202)) 
    reset_time_out_i_2
       (.I0(\FSM_sequential_rx_state_reg[0]_1 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(data_valid_sync),
        .I4(Q[3]),
        .I5(mmcm_lock_reclocked),
        .O(reset_time_out_i_2_n_0));
  LUT4 #(
    .INIT(16'hABA8)) 
    rx_fsm_reset_done_int_i_1
       (.I0(rx_fsm_reset_done_int),
        .I1(rx_fsm_reset_done_int_i_3_n_0),
        .I2(rx_fsm_reset_done_int_i_4_n_0),
        .I3(data_in),
        .O(rx_fsm_reset_done_int_reg));
  LUT5 #(
    .INIT(32'h00040000)) 
    rx_fsm_reset_done_int_i_2
       (.I0(Q[0]),
        .I1(data_valid_sync),
        .I2(Q[2]),
        .I3(reset_time_out_reg_2),
        .I4(rx_fsm_reset_done_int_reg_2),
        .O(rx_fsm_reset_done_int));
  LUT6 #(
    .INIT(64'h0400040004040400)) 
    rx_fsm_reset_done_int_i_3
       (.I0(rx_fsm_reset_done_int_reg_0),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_1),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_3_n_0));
  LUT6 #(
    .INIT(64'h0008000808080008)) 
    rx_fsm_reset_done_int_i_4
       (.I0(rx_fsm_reset_done_int_reg_3),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(data_valid_sync),
        .I4(rx_fsm_reset_done_int_reg_2),
        .I5(reset_time_out_reg_2),
        .O(rx_fsm_reset_done_int_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_13
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1__0 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_14
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_15
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_16
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_3
   (data_out,
    status_vector,
    independent_clock_bufg);
  output data_out;
  input [0:0]status_vector;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;
  wire [0:0]status_vector;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(status_vector),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_4
   (data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_5
   (reset_time_out_reg,
    E,
    reset_time_out_reg_0,
    reset_time_out,
    \FSM_sequential_tx_state_reg[0] ,
    \FSM_sequential_tx_state_reg[0]_0 ,
    \FSM_sequential_tx_state_reg[0]_1 ,
    \FSM_sequential_tx_state_reg[0]_2 ,
    \FSM_sequential_tx_state_reg[0]_3 ,
    Q,
    reset_time_out_reg_1,
    mmcm_lock_reclocked,
    \FSM_sequential_tx_state_reg[0]_4 ,
    \FSM_sequential_tx_state_reg[0]_5 ,
    \FSM_sequential_tx_state_reg[0]_6 ,
    data_sync_reg1_0,
    independent_clock_bufg);
  output reset_time_out_reg;
  output [0:0]E;
  input reset_time_out_reg_0;
  input reset_time_out;
  input \FSM_sequential_tx_state_reg[0] ;
  input \FSM_sequential_tx_state_reg[0]_0 ;
  input \FSM_sequential_tx_state_reg[0]_1 ;
  input \FSM_sequential_tx_state_reg[0]_2 ;
  input \FSM_sequential_tx_state_reg[0]_3 ;
  input [3:0]Q;
  input reset_time_out_reg_1;
  input mmcm_lock_reclocked;
  input \FSM_sequential_tx_state_reg[0]_4 ;
  input \FSM_sequential_tx_state_reg[0]_5 ;
  input \FSM_sequential_tx_state_reg[0]_6 ;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]E;
  wire \FSM_sequential_tx_state[3]_i_5_n_0 ;
  wire \FSM_sequential_tx_state_reg[0] ;
  wire \FSM_sequential_tx_state_reg[0]_0 ;
  wire \FSM_sequential_tx_state_reg[0]_1 ;
  wire \FSM_sequential_tx_state_reg[0]_2 ;
  wire \FSM_sequential_tx_state_reg[0]_3 ;
  wire \FSM_sequential_tx_state_reg[0]_4 ;
  wire \FSM_sequential_tx_state_reg[0]_5 ;
  wire \FSM_sequential_tx_state_reg[0]_6 ;
  wire [3:0]Q;
  wire cplllock_sync;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;
  wire mmcm_lock_reclocked;
  wire reset_time_out;
  wire reset_time_out_i_3__0_n_0;
  wire reset_time_out_i_4__0_n_0;
  wire reset_time_out_reg;
  wire reset_time_out_reg_0;
  wire reset_time_out_reg_1;

  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    \FSM_sequential_tx_state[3]_i_1 
       (.I0(\FSM_sequential_tx_state_reg[0] ),
        .I1(\FSM_sequential_tx_state_reg[0]_0 ),
        .I2(\FSM_sequential_tx_state[3]_i_5_n_0 ),
        .I3(\FSM_sequential_tx_state_reg[0]_1 ),
        .I4(\FSM_sequential_tx_state_reg[0]_2 ),
        .I5(\FSM_sequential_tx_state_reg[0]_3 ),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000F00008)) 
    \FSM_sequential_tx_state[3]_i_5 
       (.I0(\FSM_sequential_tx_state_reg[0]_4 ),
        .I1(\FSM_sequential_tx_state_reg[0]_5 ),
        .I2(cplllock_sync),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(\FSM_sequential_tx_state_reg[0]_6 ),
        .O(\FSM_sequential_tx_state[3]_i_5_n_0 ));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(cplllock_sync),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hEFE0)) 
    reset_time_out_i_1
       (.I0(reset_time_out_reg_0),
        .I1(reset_time_out_i_3__0_n_0),
        .I2(reset_time_out_i_4__0_n_0),
        .I3(reset_time_out),
        .O(reset_time_out_reg));
  LUT6 #(
    .INIT(64'h020002000F000200)) 
    reset_time_out_i_3__0
       (.I0(cplllock_sync),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[0]),
        .I4(mmcm_lock_reclocked),
        .I5(Q[1]),
        .O(reset_time_out_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0505FF040505F504)) 
    reset_time_out_i_4__0
       (.I0(Q[1]),
        .I1(reset_time_out_reg_1),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(cplllock_sync),
        .O(reset_time_out_i_4__0_n_0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_6
   (SR,
    data_out,
    data_sync_reg1_0,
    independent_clock_bufg);
  output [0:0]SR;
  output data_out;
  input data_sync_reg1_0;
  input independent_clock_bufg;

  wire [0:0]SR;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync_reg1_0),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \mmcm_lock_count[7]_i_1 
       (.I0(data_out),
        .O(SR));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_7
   (data_out,
    data_in,
    data_sync_reg6_0);
  output data_out;
  input data_in;
  input data_sync_reg6_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg6_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg6_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_8
   (data_out,
    data_in,
    independent_clock_bufg);
  output data_out;
  input data_in;
  input independent_clock_bufg;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire independent_clock_bufg;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(independent_clock_bufg),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gig_ethernet_pcs_pma_0_sync_block" *) 
module gig_ethernet_pcs_pma_0_sync_block_9
   (data_out,
    data_in,
    data_sync_reg1_0);
  output data_out;
  input data_in;
  input data_sync_reg1_0;

  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire data_sync_reg1_0;

  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg1
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg2
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg3
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg4
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg5
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
  (* ASYNC_REG *) 
  (* SHREG_EXTRACT = "no" *) 
  (* XILINX_LEGACY_PRIM = "FD" *) 
  (* XILINX_TRANSFORM_PINMAP = "VCC:CE GND:R" *) 
  (* box_type = "PRIMITIVE" *) 
  FDRE #(
    .INIT(1'b0)) 
    data_sync_reg6
       (.C(data_sync_reg1_0),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

module gig_ethernet_pcs_pma_0_transceiver
   (txn,
    txp,
    rxoutclk,
    txoutclk,
    rxchariscomma,
    rxcharisk,
    rxdisperr,
    rxnotintable,
    rxbufstatus,
    txbuferr,
    mmcm_reset,
    data_in,
    rx_fsm_reset_done_int_reg,
    Q,
    \rxdata_reg[7]_0 ,
    independent_clock_bufg,
    gtrefclk_bufg,
    gtrefclk_out,
    rxn,
    rxp,
    gt0_qplloutclk_out,
    gt0_qplloutrefclk_out,
    gtxe2_i,
    SR,
    CLK,
    powerdown,
    reset_sync5,
    D,
    txchardispval_reg_reg_0,
    txcharisk_reg_reg_0,
    out,
    status_vector,
    enablealign,
    data_sync_reg1,
    \txdata_reg_reg[7]_0 );
  output txn;
  output txp;
  output rxoutclk;
  output txoutclk;
  output [0:0]rxchariscomma;
  output [0:0]rxcharisk;
  output [0:0]rxdisperr;
  output [0:0]rxnotintable;
  output [0:0]rxbufstatus;
  output txbuferr;
  output mmcm_reset;
  output data_in;
  output rx_fsm_reset_done_int_reg;
  output [1:0]Q;
  output [7:0]\rxdata_reg[7]_0 ;
  input independent_clock_bufg;
  input gtrefclk_bufg;
  input gtrefclk_out;
  input rxn;
  input rxp;
  input gt0_qplloutclk_out;
  input gt0_qplloutrefclk_out;
  input gtxe2_i;
  input [0:0]SR;
  input CLK;
  input powerdown;
  input [0:0]reset_sync5;
  input [0:0]D;
  input [0:0]txchardispval_reg_reg_0;
  input [0:0]txcharisk_reg_reg_0;
  input [0:0]out;
  input [0:0]status_vector;
  input enablealign;
  input data_sync_reg1;
  input [7:0]\txdata_reg_reg[7]_0 ;

  wire CLK;
  wire [0:0]D;
  wire [1:0]Q;
  wire [0:0]SR;
  wire data_in;
  wire data_sync_reg1;
  wire data_valid_reg2;
  wire enablealign;
  wire encommaalign_int;
  wire gt0_qplloutclk_out;
  wire gt0_qplloutrefclk_out;
  wire gtrefclk_bufg;
  wire gtrefclk_out;
  wire gtwizard_inst_n_6;
  wire gtwizard_inst_n_7;
  wire gtxe2_i;
  wire independent_clock_bufg;
  wire mmcm_reset;
  wire [0:0]out;
  wire p_0_in;
  wire powerdown;
  wire reset;
  wire [0:0]reset_sync5;
  wire rx_fsm_reset_done_int_reg;
  wire [0:0]rxbufstatus;
  wire [0:0]rxchariscomma;
  wire [1:0]rxchariscomma_double;
  wire rxchariscomma_i_1_n_0;
  wire [1:0]rxchariscomma_int;
  wire [1:0]rxchariscomma_reg__0;
  wire [0:0]rxcharisk;
  wire [1:0]rxcharisk_double;
  wire rxcharisk_i_1_n_0;
  wire [1:0]rxcharisk_int;
  wire [1:0]rxcharisk_reg__0;
  wire [1:0]rxclkcorcnt_double;
  wire [1:0]rxclkcorcnt_int;
  wire [1:0]rxclkcorcnt_reg;
  wire \rxdata[0]_i_1_n_0 ;
  wire \rxdata[1]_i_1_n_0 ;
  wire \rxdata[2]_i_1_n_0 ;
  wire \rxdata[3]_i_1_n_0 ;
  wire \rxdata[4]_i_1_n_0 ;
  wire \rxdata[5]_i_1_n_0 ;
  wire \rxdata[6]_i_1_n_0 ;
  wire \rxdata[7]_i_1_n_0 ;
  wire [15:0]rxdata_double;
  wire [15:0]rxdata_int;
  wire [15:0]rxdata_reg;
  wire [7:0]\rxdata_reg[7]_0 ;
  wire [0:0]rxdisperr;
  wire [1:0]rxdisperr_double;
  wire rxdisperr_i_1_n_0;
  wire [1:0]rxdisperr_int;
  wire [1:0]rxdisperr_reg__0;
  wire rxn;
  wire [0:0]rxnotintable;
  wire [1:0]rxnotintable_double;
  wire rxnotintable_i_1_n_0;
  wire [1:0]rxnotintable_int;
  wire [1:0]rxnotintable_reg__0;
  wire rxoutclk;
  wire rxp;
  wire rxpowerdown;
  wire rxpowerdown_double;
  wire rxpowerdown_reg__0;
  wire rxreset_int;
  wire [0:0]status_vector;
  wire toggle;
  wire toggle_i_1_n_0;
  wire toggle_rx;
  wire toggle_rx_i_1_n_0;
  wire txbuferr;
  wire [1:1]txbufstatus_reg;
  wire [1:0]txchardispmode_double;
  wire [1:0]txchardispmode_int;
  wire txchardispmode_reg;
  wire [1:0]txchardispval_double;
  wire [1:0]txchardispval_int;
  wire txchardispval_reg;
  wire [0:0]txchardispval_reg_reg_0;
  wire [1:0]txcharisk_double;
  wire [1:0]txcharisk_int;
  wire txcharisk_reg;
  wire [0:0]txcharisk_reg_reg_0;
  wire [15:0]txdata_double;
  wire [15:0]txdata_int;
  wire [7:0]txdata_reg;
  wire [7:0]\txdata_reg_reg[7]_0 ;
  wire txn;
  wire txoutclk;
  wire txp;
  wire txpowerdown;
  wire txpowerdown_double;
  wire txpowerdown_reg__0;
  wire txreset_int;

  gig_ethernet_pcs_pma_0_GTWIZARD gtwizard_inst
       (.D(rxclkcorcnt_int),
        .Q(txdata_int),
        .RXBUFSTATUS(gtwizard_inst_n_7),
        .RXPD(rxpowerdown),
        .TXBUFSTATUS(gtwizard_inst_n_6),
        .TXPD(txpowerdown),
        .data_in(data_in),
        .data_out(data_valid_reg2),
        .data_sync_reg1(data_sync_reg1),
        .gt0_qplloutclk_out(gt0_qplloutclk_out),
        .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
        .gtrefclk_bufg(gtrefclk_bufg),
        .gtrefclk_out(gtrefclk_out),
        .gtxe2_i(rxdata_int),
        .gtxe2_i_0(rxchariscomma_int),
        .gtxe2_i_1(rxcharisk_int),
        .gtxe2_i_2(rxdisperr_int),
        .gtxe2_i_3(rxnotintable_int),
        .gtxe2_i_4(gtxe2_i),
        .gtxe2_i_5(txchardispmode_int),
        .gtxe2_i_6(txchardispval_int),
        .gtxe2_i_7(txcharisk_int),
        .gtxe2_i_8(rxreset_int),
        .gtxe2_i_9(txreset_int),
        .independent_clock_bufg(independent_clock_bufg),
        .mmcm_reset(mmcm_reset),
        .out(out),
        .reset(reset),
        .reset_out(encommaalign_int),
        .rx_fsm_reset_done_int_reg(rx_fsm_reset_done_int_reg),
        .rxn(rxn),
        .rxoutclk(rxoutclk),
        .rxp(rxp),
        .txn(txn),
        .txoutclk(txoutclk),
        .txp(txp));
  gig_ethernet_pcs_pma_0_reset_sync reclock_encommaalign
       (.CLK(CLK),
        .enablealign(enablealign),
        .reset_out(encommaalign_int));
  gig_ethernet_pcs_pma_0_reset_sync_1 reclock_rxreset
       (.SR(SR),
        .independent_clock_bufg(independent_clock_bufg),
        .reset_out(rxreset_int));
  gig_ethernet_pcs_pma_0_reset_sync_2 reclock_txreset
       (.independent_clock_bufg(independent_clock_bufg),
        .reset_out(txreset_int),
        .reset_sync5_0(reset_sync5));
  gig_ethernet_pcs_pma_0_reset_wtd_timer reset_wtd_timer
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .reset(reset));
  FDRE rxbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(p_0_in),
        .Q(rxbufstatus),
        .R(1'b0));
  FDRE \rxbufstatus_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_7),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \rxchariscomma_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[0]),
        .Q(rxchariscomma_double[0]),
        .R(SR));
  FDRE \rxchariscomma_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxchariscomma_reg__0[1]),
        .Q(rxchariscomma_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxchariscomma_i_1
       (.I0(rxchariscomma_double[1]),
        .I1(toggle_rx),
        .I2(rxchariscomma_double[0]),
        .O(rxchariscomma_i_1_n_0));
  FDRE rxchariscomma_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxchariscomma_i_1_n_0),
        .Q(rxchariscomma),
        .R(SR));
  FDRE \rxchariscomma_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[0]),
        .Q(rxchariscomma_reg__0[0]),
        .R(1'b0));
  FDRE \rxchariscomma_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxchariscomma_int[1]),
        .Q(rxchariscomma_reg__0[1]),
        .R(1'b0));
  FDRE \rxcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[0]),
        .Q(rxcharisk_double[0]),
        .R(SR));
  FDRE \rxcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxcharisk_reg__0[1]),
        .Q(rxcharisk_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxcharisk_i_1
       (.I0(rxcharisk_double[1]),
        .I1(toggle_rx),
        .I2(rxcharisk_double[0]),
        .O(rxcharisk_i_1_n_0));
  FDRE rxcharisk_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxcharisk_i_1_n_0),
        .Q(rxcharisk),
        .R(SR));
  FDRE \rxcharisk_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[0]),
        .Q(rxcharisk_reg__0[0]),
        .R(1'b0));
  FDRE \rxcharisk_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxcharisk_int[1]),
        .Q(rxcharisk_reg__0[1]),
        .R(1'b0));
  FDRE \rxclkcorcnt_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[0]),
        .Q(rxclkcorcnt_double[0]),
        .R(SR));
  FDRE \rxclkcorcnt_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxclkcorcnt_reg[1]),
        .Q(rxclkcorcnt_double[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \rxclkcorcnt_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(rxclkcorcnt_double[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \rxclkcorcnt_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[0]),
        .Q(rxclkcorcnt_reg[0]),
        .R(1'b0));
  FDRE \rxclkcorcnt_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxclkcorcnt_int[1]),
        .Q(rxclkcorcnt_reg[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[0]_i_1 
       (.I0(rxdata_double[8]),
        .I1(toggle_rx),
        .I2(rxdata_double[0]),
        .O(\rxdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[1]_i_1 
       (.I0(rxdata_double[9]),
        .I1(toggle_rx),
        .I2(rxdata_double[1]),
        .O(\rxdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[2]_i_1 
       (.I0(rxdata_double[10]),
        .I1(toggle_rx),
        .I2(rxdata_double[2]),
        .O(\rxdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[3]_i_1 
       (.I0(rxdata_double[11]),
        .I1(toggle_rx),
        .I2(rxdata_double[3]),
        .O(\rxdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[4]_i_1 
       (.I0(rxdata_double[12]),
        .I1(toggle_rx),
        .I2(rxdata_double[4]),
        .O(\rxdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[5]_i_1 
       (.I0(rxdata_double[13]),
        .I1(toggle_rx),
        .I2(rxdata_double[5]),
        .O(\rxdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[6]_i_1 
       (.I0(rxdata_double[14]),
        .I1(toggle_rx),
        .I2(rxdata_double[6]),
        .O(\rxdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rxdata[7]_i_1 
       (.I0(rxdata_double[15]),
        .I1(toggle_rx),
        .I2(rxdata_double[7]),
        .O(\rxdata[7]_i_1_n_0 ));
  FDRE \rxdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[0]),
        .Q(rxdata_double[0]),
        .R(SR));
  FDRE \rxdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[10]),
        .Q(rxdata_double[10]),
        .R(SR));
  FDRE \rxdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[11]),
        .Q(rxdata_double[11]),
        .R(SR));
  FDRE \rxdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[12]),
        .Q(rxdata_double[12]),
        .R(SR));
  FDRE \rxdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[13]),
        .Q(rxdata_double[13]),
        .R(SR));
  FDRE \rxdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[14]),
        .Q(rxdata_double[14]),
        .R(SR));
  FDRE \rxdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[15]),
        .Q(rxdata_double[15]),
        .R(SR));
  FDRE \rxdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[1]),
        .Q(rxdata_double[1]),
        .R(SR));
  FDRE \rxdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[2]),
        .Q(rxdata_double[2]),
        .R(SR));
  FDRE \rxdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[3]),
        .Q(rxdata_double[3]),
        .R(SR));
  FDRE \rxdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[4]),
        .Q(rxdata_double[4]),
        .R(SR));
  FDRE \rxdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[5]),
        .Q(rxdata_double[5]),
        .R(SR));
  FDRE \rxdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[6]),
        .Q(rxdata_double[6]),
        .R(SR));
  FDRE \rxdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[7]),
        .Q(rxdata_double[7]),
        .R(SR));
  FDRE \rxdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[8]),
        .Q(rxdata_double[8]),
        .R(SR));
  FDRE \rxdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdata_reg[9]),
        .Q(rxdata_double[9]),
        .R(SR));
  FDRE \rxdata_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[0]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [0]),
        .R(SR));
  FDRE \rxdata_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[1]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [1]),
        .R(SR));
  FDRE \rxdata_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[2]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [2]),
        .R(SR));
  FDRE \rxdata_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[3]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [3]),
        .R(SR));
  FDRE \rxdata_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[4]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [4]),
        .R(SR));
  FDRE \rxdata_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[5]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [5]),
        .R(SR));
  FDRE \rxdata_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[6]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [6]),
        .R(SR));
  FDRE \rxdata_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\rxdata[7]_i_1_n_0 ),
        .Q(\rxdata_reg[7]_0 [7]),
        .R(SR));
  FDRE \rxdata_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[0]),
        .Q(rxdata_reg[0]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[10]),
        .Q(rxdata_reg[10]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[11]),
        .Q(rxdata_reg[11]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[12]),
        .Q(rxdata_reg[12]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[13]),
        .Q(rxdata_reg[13]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[14]),
        .Q(rxdata_reg[14]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[15]),
        .Q(rxdata_reg[15]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[1]),
        .Q(rxdata_reg[1]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[2]),
        .Q(rxdata_reg[2]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[3]),
        .Q(rxdata_reg[3]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[4]),
        .Q(rxdata_reg[4]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[5]),
        .Q(rxdata_reg[5]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[6]),
        .Q(rxdata_reg[6]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[7]),
        .Q(rxdata_reg[7]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[8]),
        .Q(rxdata_reg[8]),
        .R(1'b0));
  FDRE \rxdata_reg_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdata_int[9]),
        .Q(rxdata_reg[9]),
        .R(1'b0));
  FDRE \rxdisperr_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[0]),
        .Q(rxdisperr_double[0]),
        .R(SR));
  FDRE \rxdisperr_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxdisperr_reg__0[1]),
        .Q(rxdisperr_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxdisperr_i_1
       (.I0(rxdisperr_double[1]),
        .I1(toggle_rx),
        .I2(rxdisperr_double[0]),
        .O(rxdisperr_i_1_n_0));
  FDRE rxdisperr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxdisperr_i_1_n_0),
        .Q(rxdisperr),
        .R(SR));
  FDRE \rxdisperr_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[0]),
        .Q(rxdisperr_reg__0[0]),
        .R(1'b0));
  FDRE \rxdisperr_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxdisperr_int[1]),
        .Q(rxdisperr_reg__0[1]),
        .R(1'b0));
  FDRE \rxnotintable_double_reg[0] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[0]),
        .Q(rxnotintable_double[0]),
        .R(SR));
  FDRE \rxnotintable_double_reg[1] 
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxnotintable_reg__0[1]),
        .Q(rxnotintable_double[1]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    rxnotintable_i_1
       (.I0(rxnotintable_double[1]),
        .I1(toggle_rx),
        .I2(rxnotintable_double[0]),
        .O(rxnotintable_i_1_n_0));
  FDRE rxnotintable_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxnotintable_i_1_n_0),
        .Q(rxnotintable),
        .R(SR));
  FDRE \rxnotintable_reg_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[0]),
        .Q(rxnotintable_reg__0[0]),
        .R(1'b0));
  FDRE \rxnotintable_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxnotintable_int[1]),
        .Q(rxnotintable_reg__0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_double_reg
       (.C(CLK),
        .CE(toggle_rx),
        .D(rxpowerdown_reg__0),
        .Q(rxpowerdown_double),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(rxpowerdown_double),
        .Q(rxpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    rxpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(rxpowerdown_reg__0),
        .R(SR));
  gig_ethernet_pcs_pma_0_sync_block_3 sync_block_data_valid
       (.data_out(data_valid_reg2),
        .independent_clock_bufg(independent_clock_bufg),
        .status_vector(status_vector));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_i_1
       (.I0(toggle),
        .O(toggle_i_1_n_0));
  FDRE toggle_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_i_1_n_0),
        .Q(toggle),
        .R(reset_sync5));
  LUT1 #(
    .INIT(2'h1)) 
    toggle_rx_i_1
       (.I0(toggle_rx),
        .O(toggle_rx_i_1_n_0));
  FDRE toggle_rx_reg
       (.C(CLK),
        .CE(1'b1),
        .D(toggle_rx_i_1_n_0),
        .Q(toggle_rx),
        .R(SR));
  FDRE txbuferr_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txbufstatus_reg),
        .Q(txbuferr),
        .R(1'b0));
  FDRE \txbufstatus_reg_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(gtwizard_inst_n_6),
        .Q(txbufstatus_reg),
        .R(1'b0));
  FDRE \txchardispmode_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispmode_reg),
        .Q(txchardispmode_double[0]),
        .R(reset_sync5));
  FDRE \txchardispmode_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(D),
        .Q(txchardispmode_double[1]),
        .R(reset_sync5));
  FDRE \txchardispmode_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[0]),
        .Q(txchardispmode_int[0]),
        .R(1'b0));
  FDRE \txchardispmode_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispmode_double[1]),
        .Q(txchardispmode_int[1]),
        .R(1'b0));
  FDRE txchardispmode_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(D),
        .Q(txchardispmode_reg),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg),
        .Q(txchardispval_double[0]),
        .R(reset_sync5));
  FDRE \txchardispval_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_double[1]),
        .R(reset_sync5));
  FDRE \txchardispval_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[0]),
        .Q(txchardispval_int[0]),
        .R(1'b0));
  FDRE \txchardispval_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txchardispval_double[1]),
        .Q(txchardispval_int[1]),
        .R(1'b0));
  FDRE txchardispval_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txchardispval_reg_reg_0),
        .Q(txchardispval_reg),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg),
        .Q(txcharisk_double[0]),
        .R(reset_sync5));
  FDRE \txcharisk_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_double[1]),
        .R(reset_sync5));
  FDRE \txcharisk_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[0]),
        .Q(txcharisk_int[0]),
        .R(1'b0));
  FDRE \txcharisk_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txcharisk_double[1]),
        .Q(txcharisk_int[1]),
        .R(1'b0));
  FDRE txcharisk_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txcharisk_reg_reg_0),
        .Q(txcharisk_reg),
        .R(reset_sync5));
  FDRE \txdata_double_reg[0] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[0]),
        .Q(txdata_double[0]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[10] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_double[10]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[11] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_double[11]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[12] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_double[12]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[13] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_double[13]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[14] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_double[14]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[15] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_double[15]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[1] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[1]),
        .Q(txdata_double[1]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[2] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[2]),
        .Q(txdata_double[2]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[3] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[3]),
        .Q(txdata_double[3]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[4] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[4]),
        .Q(txdata_double[4]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[5] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[5]),
        .Q(txdata_double[5]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[6] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[6]),
        .Q(txdata_double[6]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[7] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(txdata_reg[7]),
        .Q(txdata_double[7]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[8] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_double[8]),
        .R(reset_sync5));
  FDRE \txdata_double_reg[9] 
       (.C(CLK),
        .CE(toggle_i_1_n_0),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_double[9]),
        .R(reset_sync5));
  FDRE \txdata_int_reg[0] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[0]),
        .Q(txdata_int[0]),
        .R(1'b0));
  FDRE \txdata_int_reg[10] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[10]),
        .Q(txdata_int[10]),
        .R(1'b0));
  FDRE \txdata_int_reg[11] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[11]),
        .Q(txdata_int[11]),
        .R(1'b0));
  FDRE \txdata_int_reg[12] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[12]),
        .Q(txdata_int[12]),
        .R(1'b0));
  FDRE \txdata_int_reg[13] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[13]),
        .Q(txdata_int[13]),
        .R(1'b0));
  FDRE \txdata_int_reg[14] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[14]),
        .Q(txdata_int[14]),
        .R(1'b0));
  FDRE \txdata_int_reg[15] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[15]),
        .Q(txdata_int[15]),
        .R(1'b0));
  FDRE \txdata_int_reg[1] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[1]),
        .Q(txdata_int[1]),
        .R(1'b0));
  FDRE \txdata_int_reg[2] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[2]),
        .Q(txdata_int[2]),
        .R(1'b0));
  FDRE \txdata_int_reg[3] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[3]),
        .Q(txdata_int[3]),
        .R(1'b0));
  FDRE \txdata_int_reg[4] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[4]),
        .Q(txdata_int[4]),
        .R(1'b0));
  FDRE \txdata_int_reg[5] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[5]),
        .Q(txdata_int[5]),
        .R(1'b0));
  FDRE \txdata_int_reg[6] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[6]),
        .Q(txdata_int[6]),
        .R(1'b0));
  FDRE \txdata_int_reg[7] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[7]),
        .Q(txdata_int[7]),
        .R(1'b0));
  FDRE \txdata_int_reg[8] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[8]),
        .Q(txdata_int[8]),
        .R(1'b0));
  FDRE \txdata_int_reg[9] 
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txdata_double[9]),
        .Q(txdata_int[9]),
        .R(1'b0));
  FDRE \txdata_reg_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [0]),
        .Q(txdata_reg[0]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [1]),
        .Q(txdata_reg[1]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [2]),
        .Q(txdata_reg[2]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [3]),
        .Q(txdata_reg[3]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [4]),
        .Q(txdata_reg[4]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [5]),
        .Q(txdata_reg[5]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [6]),
        .Q(txdata_reg[6]),
        .R(reset_sync5));
  FDRE \txdata_reg_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\txdata_reg_reg[7]_0 [7]),
        .Q(txdata_reg[7]),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_double_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txpowerdown_reg__0),
        .Q(txpowerdown_double),
        .R(reset_sync5));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg
       (.C(gtxe2_i),
        .CE(1'b1),
        .D(txpowerdown_double),
        .Q(txpowerdown),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    txpowerdown_reg_reg
       (.C(CLK),
        .CE(1'b1),
        .D(powerdown),
        .Q(txpowerdown_reg__0),
        .R(reset_sync5));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
afmoZr9skqIZsiBY7liE8HhN5PT9cSTlE7b7FkfYEnmAz8/sYewHAx0YfvnPtVZDeaUQ1+SA/YSK
81BVyxiW6Q1+w7v5E1h+YJTA02rq79x+8OQbPa0mLIjdXTtNmx+BnF2Qrq3QGTPNoSFginwNNM6R
DHZuBkJnITzBkDYC/to=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
oFhR7iEHB5cYBaPdDcFYa+fK3Gj/00Z1GiQ4ZVkb1MGu+Q6dRi7Lgvhbwd39WT10uvi5m95gOjlX
gGa0fqW2XuRf5YGzXsxqVh0xVmshpf7o2GLRr2CmqpGBtEUVv4LbXJ4WiaEo9wVMVr2rBg6MtuPB
CQBdJcTka6nWZivdNblYGd53J9DObofIqJ9W/NUzFP/DmzVmGbctoa4amp6KUecoKxnO4tESoQzm
lz7s+Zslv3tCv5wVOwV3SAR+Y5ul9hQX8OljyF82Z31ipd+bzojhPg9RhmrvIMbFqzM9eU82pEIT
IZzSVKYMd1P6t1qORfwdgodKT0ZApoyFVxfVeQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
FJ5kCtzgKdagetGF6bdHAPKIu2cq7BcnQFRTWMN8j6rUq4ZaiydIPtRo6ybDhCzgm0ZRPth8Lgd5
+HXk3g/SdsTLZoOUuXnEz+TkfRJdu8kdJ+0y8sJMNK21IEW3pSWsj/n4toksSjLJ1wENuoq4XqDK
9VQo0Gobmx7OOqOYFuY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
d/VWmC3TdLGZzbtN4vTxv6kYGkcDx9TNbvTz1ALcTDtGFLEUmm0Z0/puE1NDYgBTJquGDZ+j8d2H
/il9HfJ/2n/hTxPKUXm83KvLOZ3eaRdjaEk1wHy+UsjHJ1VztyOuT/nrMaT3sUq1kpP7wkC5I2EK
lQZbp4ngkuiSjWCad10Yoj0TX6DfE58qXjaybvWsINo7H6gMzi4JvYHWbBnCYQS/RkvaQvSsne0i
YvfJuYy63HVFs+Afh6hOO34nbqzXvlsXI7SIicsCUu21AgLUNio4IsL+Uj92NEPjaJnw2wTXrMuH
A1a3i03K7vrZ94AIVwtSXRwfN4q33BWP9bjYdQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
3xYA7hcw+q7P0QSxtus9heaAij7kiNzMgPuCXFQTIhWPWN0atyWMkDqoxytTvKyCd0IM0n0DheIN
XngnbZmTQpitsZh5f/e8iD/+iWxl+7S7I6wyIivHhUA/yP87+KG/vFGPXu7WexVyPmZuOgSS7sS/
Fnk+QS1l3qltq84DH59g9EBTV7YkcgLEdICkCR0tgP3K8aSVjCptgXzA0onYGrkQESEdw8nPfEvR
g/5lKhHOmmE6kTh/XfO6eQpuQBKOlD7RaVWrPxnTkM9lxrjRZZb3OSfaBLMKhMGrOdEY4RIqfHMG
MBOH8gAUoIRrgyLztGTWc+RsxUUYHq9Hwixqkg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RyOZOz+qJ5WH/47JnOQuMFPCUAyXetmhoeJiNflULv2xv5JJeXWSOURz6kOeJ5/Jbz2KA/psbOVw
rwZKAlTJoEVMq47lcrgzkIZPdllrnwbNl8hiziEb/YEq2vvpC9EJzY7LlRbQINiQY3hAzP5B/s+N
lYRpAUjOhEst2xZEiFz6cr7zAMz2jn7lHWYuvu75NGUv8JJ7PZ6vUISUeHOPW9ljH0TH4CnOaTpM
Mey1hosrMsp78mwEmy3l4n56Khrq2FOUzGZbi5lhqEebDEU/90LVUpUSzOkhJ03OfvAgBuj9ejqz
WgZRD6DpCk8BVUByKAGJqQZsJ0NcEnE8/btwhQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Q98gl70eQ6J1AYIClxRNKl48Tz7BoxVypGMCmvUy8HqLl3jjFsIYy9amcj+/0Qts3PcCHOutO4e+
ptR4cjDneD+41DiKHcGc2sgfdKq4hG60mda13C9qtjKCuBhg+Zg1jHi3TOh4bVtsLuydPMwaILNj
7ORZo/jzBQ4OUPNFy2tkOMJArdeTOIuydxhUFtpy6FAABlc1SE1Mer24GTLYw0pJtOOJOX61uJuc
ghuBuHaXV8kExCP3IU1qx/oyfIpY9TQiqLKEf0YUrmEzBzV+JXQqn3Jom9PXTay5YJ+PqraK9iFt
ai1YIDay5F0ncLDXiT7OMRFjFG2mL+C96DFEmg==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
DgXXbyNB3XMOH/gmmWb6uBYjn7XQuHgaTP3eav9Y+qTkta/ItNcuqONFgEZl6duKUkQI/YVDFLf5
JwZHmI7U9IACSpdRAsITpVXnabTJhDmcKIxi5anNhIeSW9RerBcxlMYtuoI5FgWJKz6h+0yZmes6
xYMyXTT3gJS5ttPc1n6kBHNZpxPgl/Yv979A0/ZxYkRTfuWrCyaS5T08AajGwOyC+2sH4FjL2xLX
uR9a04wQespOimJ+InC0FAfYhqUcjs5b18x0hSfPhe4G0xKNvmpukNOhIflD4bBgKXl0kqXnpatt
7ejKBpSWKRjk/M7rYOxErj8EahuRFH07pBR9b3XyXF3MfQBwAzV2GIz+A/XzIUDQOcBbojfGwrVA
2HhHfn8sKZB5ktpwF8MBjdlwrtn+kdQ8ux0ZNrgndrvw4nS4TWxZtQeRc+wfb7Q/lIzw6Pc+Ifnn
b7UJlJvNr5ZF2XNxuHGz5L19UYgB0gpH8xWvYpn0Od797z3FfYIiGMNP

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
YW19UgddTwwnJVcciGeEKvH9ZuBWuoo4LgtOhQvaoLIGVBAQAVfk5VY0pzvluZUn2+81fwbTPSLA
O+/RYs39A761jlViEBPYfmojlgjyiOTK9aHXNLsD3WBxfTXGOS3fHhhwgCmiZ0OncriPRHcNAK7N
4cyj3anKb5y4g9gzt9YeJ7T/zkLQuzT4zqnb9ijTisbH6HxpAVXI1ydKtGzVETw+s55Jb0gVXyGi
Q1hVe4oSDBTETiD85J2/A1KF+2DHKkzOJhr53qxvjND2l/LqsA0G/4J35IdLcmuYWAoCJS7cHkTk
wnGOjEnNbP+gboLu5UrXAFwXCo+jsZLVqIzGqg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
58apVjNnALRGpDI2bjJhSWJPQlj57Kat42daGg5JiM8yXDkjxm/BmzBRp9oBmOurwCnZOZcKlj3m
HlexbUqO32l40m0mMLkLzlGYPsduAUijMIb7DMl5HUf9XunE4fC69CyB0KyecsJZI9Ru4XxZrniR
CYqqjLtNjfFEppiRGRN6bXqv2T5Vgbh9qrMDIrkZnrIUB7RBd6xTPZ0xU1V6/f7Oftp0kyRv3C0z
HF7RFTnZ4y90YOsvuCM5CoAyyE/0gTXvBlB4icz61XmHsIlwfUu/xC6yAitX3dikrJ3DWeMC5Soe
vAifs/1pf/61AlzL055eu8EpDBQRH32ikamDmA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GkhzmJz9o93IJx+1EnrQfOwa/OOrgHQT2+XhwLsfJNRk9bxhLz472g/aTSOruRXBYVSa7twJJ6+r
lf0nvm4VQfQ9bG+fbYjOsryrSSKlth3/Bq54UcBz0wLrrf59Jt2KtKCOUQojHAIinG8CLxMBgQJR
wqwzAYtmffGsCYbobk0Y4IPkjQhtQamQd1BTI8ZwvF5zPN8kBYuoFLMFqwet6s+kcH/tQSrB0o/K
lHILn0b8uovehAHNWllP4phB4MeAOgMZbbsniJgqPQEhSd4nOUZs4WnWtYOsf+rCLliHTc9c6HIS
0u5f7mdvO+uC8Pi/nwNFr026nnYUX3YcG4zdBQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 153744)
`pragma protect data_block
GPCNQQnusanPXzIVRiPRVYUogV/+QIkGM34LIYBNx9CmxK8TunRnL5DM163R7Ga5pcOjS7eOO3c4
WDwanyLBnZR9Nq11tKx4NK9X383V1gzYLFGyOSxvVseJ+BtcSvuTzzwrUDHpgI40wou4PQNd09pw
EL0fbqK1Cg6Rb7dmsfo8xOV5UZtr1IWM8NZcSmmwzZD7G40w22xbA1Acgqk4qFpGv3AHfXlRaZqP
nrpAxQ1jr8D0RcZ/0sMOf0kLc61l4m+CN03Rc/jts+L6OvSTgvAVLh4xpVXimz1LbhJWiZDBpXjB
/mtDSE1COIeHdtkUXO4K8PzTfTrUXDlm4ZTBeVYou2Ym5//uMgvx6cSJpp6LXwZypsAn3+J2+VTD
ThQEEou0ZMfsCFy1qTAUCOF2cm5ORHzpdHRpcNLHqh3QecyvvOqRDGOkuruOHGRk8Iu76/5KwlEL
UP/4q9xhVKgT/SOUYyl7YSa2X161Evp/08612OoMgHDPCJgue9WPLz5wVHIuNz9qfZ4AeSW8Pd3F
kofCTNVoz/4rPdN9nLZJ2ZwvagspmYq4R6CcBL/KGQIMA+mFmPKtw1/YlN4jBBCx0dwg5hjdhgjt
xZc6vY5TQM0J00eGub1yFBZ7k+ny7Vbd8vPyVKKz5oD5DjJKCSyii0r2pl+ajbFcuqKVbW7qehWw
/5HqhmFzKn0CiPZyjohgkBPGvSBUltxGHBm4QLIjRWiS0b7/5OCvsVAnqdrnxmdAhG9zoe0oCKT9
LDVNOPuGeqVElr4UvXcbBEwpcxuRUqkX6SHfdLWsxAZPfqWDKaGWXnGpfY3UZDCzAd0xWagZu5bZ
YsGsSbwgfZBZL8AS1f5houGBiO7MpPF2v0CTHz4jo7EJB7OSqSQZ+Q3l4gkObt9ylDGd9Yh3Imzz
g/BkmQPQ8R9vMBCYVqssBYtjMAFP4N1FpPtiINlHorr67/nNAsBv+vAsqUMQH2TKkp17uQxXFfyd
iVZkm2IUjRyEb/Fn/RKkMZG1kfhvPrvJw2Njhv5jMhbuIIFnWov72touJZHNb2t5GXJuvfs9m/aD
+9DtY2qVGS0DCJAFqDEVcT+JdeL8OWphGfkzxkB2u6CyKjOYYy6KaQ6TcmTME/IWrda/s+mIiLEJ
ZhfkUrm9y4AhX65ub0Cj9mnM44+jLkAwcfwwLMjs0FD8nPey/38z0GcI1QDUxOpzytGQmpRNCUx4
zkTmO9328DeGhbNi+8VCgFs7Ww7itR8Rsh/58216PqalVT1lzipAEcTfqy5cx0VSM/hWHvtXcfEa
MVYJg/bCNQspz0NojeuC1wxuCU/CVfzkMY7ShTDCWOe7Q4LhkGLSKKuhwEZC9Aig6Smo03v77Wp4
tbg3yA3Mftr0IrqR6Vj/UCQdPjOs9YVyNte16eEsjFabXzCzimDLTtf1ZXfYC/K7rVExsh6C2Wey
JNqSqoUEP9g76/mq5L1F3E51yJzmwaM4QioZXl1oEtVECd/3jIsTynZuvwYFThfSiEevMz2TzlR2
eSz568e/KiNwzHtQf2va0xP3fhdn9oQ5CXayRDN5L3mbCJiQcfERP84hK/RbiyP2BugQvcIgQZLj
OEbFrV5xurGu0i6Ko/R8ikL4NJ+c+qFy5KCol4v4ipYmKtdmbIKYfJ+UoK5jh1R2XYXIEMbmNUaL
Bhj2Ny7nd/JHLvlLzx5OrwK/fOLGbDHtETu7E1KJsmddqAhFEc5SJC/iHsTc7Vktr5S8CNCt+DBM
G+Jyt/JLBOcPEoPvcD7J4Lnp02n2+G80eYFOSs4u+fqlY12S2SpO4xu+YdGfCZx3+QqxAgmtcDxV
WBNqOkDIFZhM/bXkkllU8og4yOjXwlEZsIZs4A11S0cJBTxYAiEmuIcIIJhVf63Cr3qC2lLtfhaD
NuMmBVylc1fuLihjFT7BVSdFpAXBlqxjZq+wrufqAZi7tDpaw8ZkzBmNwf72o5ZvgQ/M+Wjr29bh
ZGMgCjyh7txJVdOuekHzRswL/QU98MhYyjdIhOGjl8LF4apAIzdOGnbaq/7HQEAo//VYZ14fKts2
4xQImbHsmNq2Cs7mLYGBbW9nsWUD8441HdGDMeyKVbnY2O/yP4oSAoU+RDLBdQAT1MjAVwnDJtTK
cHpvYOosckUR4pVKsw+wFnLNU8gwKJQS40kK5lKea9yL3UX5InsrrTlgphlndw0EO9qxUgbooi4+
HeVU/282Jn2MgDkIZnJ+QjFtb2WV365lsRb/MSr4NRdme7ffJMt6kz5f8sIdGvwzAYkQyr7MNYBl
1ThysI3J5CMOov4rVJmPGCyoqZwUvdaktp/3PvzaU69p1vx+imMMUgmKp0JDSxldLJxQJvAda8xv
FPtjedsK2evaN9dNl2Fu0d9MgwllTD5MSjPVy4e3OX3TA5+zpuXeRgVWc+ZrUBPGbpyxCbBRrvL5
Tk9Wo6TWssOqnjZNXa0GemtnWoFpgEMQqbdrpFej9ipRKOiccPgDw9Xg4iBPSCgdzq+a/wAsLJbh
Kns5vzR9qqol0cTCApkBALPE/jL/yie36LPUXdYcewtcB2TnRv9B6vULNMXFPKbi6FB8LQq4r46t
XrkrCZaWD/ffT4sekr2mVVxkHGtKUx88dYRNlZ5cuYK2k5r+yOUtNf0uj9S9/y556WCCoG1USmDL
ldquVsTXbJOZV8JfILOb9gbKMUiO/CKNLUl2eVoT6NfcOugbnuG612rzJxJR97S9vxzSLFeRGl6k
eZ5BR5YQvJS2qbK7GyzRSaEXzCuinSoXboiC6kDpeslpBJUFwdeyYYte4+8QcUQQ8fAXaH4SmE65
LZFBcZ3FnIf6mvdSAkOE+HqX8U9FvZ6z+0YlaFeQfP8Fh6Tq0cpD35J+lJx2t+K9/aF/II+EpmmP
96pvtarGCjOzfBD2xbYSG6FxOnyHpSCQGhUN7YUCjJP/SQiDM9cnpy2VFdH4pH19PvIeEV3PGOen
JUVjsgZFdWGgOmQv5WUvGo395DgdFEvj3Z6yyIXS+sC5lS4ievRdJeygAzSG3ded91hGQ1iqlcfY
6bUJxmbwEdIblDDlo5IwNH8f2PvVaawPk5x+odG7ttnEReT3G6trjbq3QhniWIMTo/qbgi5Tw+aQ
9w4rUO3EucmSDLv0P1lYcRZp0PARPaMYlEZXV+UkLSsAZGb6hAR2EixnDTzfCK4xQrGcjJfDm4FK
9N3TVf4MOHsfasLt6UsYsWqaXuppLbetaf6AvajKs83CICwh1CUSc8zxbBg/w6tCVCLTJo7fosX/
0PSYC6PJnbyeq/svEUmRCGtIMy4Y3Zatxa+jzXLLMiRBF9x573JXtuHAXPuRWjFqIEktZ06V3VTE
48+b2exyEkPtDTJD2+i8Ht6KaWlz0n5sJY+uuAC8Rjtqu7nvsI4rHcpfs446wj5RqboREj/wK0UH
VodEP5vlxHrzquP0p5vV1zpnzzJZYh7Ros1fkvBj6tbD7TClDGaKeaOPs9ButW2Z4QHpdN+Ilctr
IYiS9ItNWFLsHt9M4IxKN+1/JrOF5TXjrRaiWLuqa10a5jo3wsV4/akflDZt2wuhijYv6bVnl3As
PsbQKK/h+YNCXpwDuuEgUbeDDEw6RhVrx2VhEW+MENMbNMZp38e8kJuM4C9EnmapVi4+6mEwLxQJ
yZgHynyg6x3drURfDud8B8cq7fjc01B2na1bNK3ODrPgaYOE6oHHsmfXkMKb2Il1Ya9/k8J7zg+7
QLxfutPoOXrYliFHiGbFmA2uOIvss4arYJ55ieE+fKRvvddG+/hChDtZ71Wo7kHVd5/joBrPBu6T
HFtOWRtz/v7cfWiBJF/7wnBw7KEJPvuF9UfOcNnvgw61rw1ei2a1+/N2rXolJy1PT3sHLPszblpO
YuOxSnTic3EtBeIoBV0MkdLRGyucWcUmxfy09Vzqed0ydYjeBnl3po3VkAieyH0f1P8UBmJMzMt8
Pgfw7JkFJethGh6lYS269uIrXCJioi6o6QhHaj0wsAEO8in2uV3/iXiNSyHwLHAzb5WcA/0MTPXt
7BgqeCVLfh7FcMQgUt+ydKc5dugP0fE3UJGpz+MG3VefERXDhVH97UxCv7Lx3Mkn+zzsLFcFUjGw
E2xflfF8MKHWaXxJCG/t/pDxlStXcrBx6iVZ+i8oQzKaEFbkiuSoyOychdTxI3/FvCJx7N2cZ2+k
irSrzoAOmA76Uv+aU+2zxhLBqwCA7iIq9cxvm9yfAOGukjn9peRbOWfTDgVy9Vy5/goDNEw+U5q8
VB5N6dWRTAj6WiLHLn0cRnVOKf8gQ2pShjjcwO5fJwz5PLYEpZZZ6DBR9NpTq2Ia9eX4J4FuKire
0z8nhHwJumjXyoUi33O2F++8Hh3rS837nFLjCDNINfH2ODLP79ATGKouI0IZLQ6pogn5MyK2LPRN
d//7Q7lpNO89HxQ2urkLRsD5vNid5ffxDhDfYlXCMZW14emrU3XIl7awWL4Cjcp9vXQUMUlBV9Kt
I/2yHibF66cfnHebdA690wVd14b5DBp1V/E5HXozEnFFRziRnrzW7ITqxd6nqWobgXMSx7ewWkCK
tHWKEXRqjnI5xpQrk3raiwt9TkX5oOISuJHYpK4vzQfyaLcuC+ihZNvnOX4ggmNoE/QUPeddR2/Z
8yipqYxqXWWqmzn8EYcPNo/pzyjsBYhdEdOcrzb0FV3JnpHyHnaNa8tVDTtfGwFePreVMp+8DM1z
zR/knbR1oBtNsCW/NAWFSw+HuwRh+8T1RrGm7BK5V8jyHD8vrwnStUGxR6OjwHX8GdKQwpPo6Dmm
A2YebO7nXdALlahmCQyPmwSXEU/8fk69qu7h/zJBmf4QziT+iMywTMtSG8xxMhm/ZGkxCAbrWG47
xiUpd4iGUPgZjsH5gdVupSc5oEYXTq4fsG3TZ+eYg2j2oaD9jfVu4hFDw2mK8Vch8jMA9S//XGpQ
dSZ05CZhYZv3cjZ/axtb1FKYhCEXJvAMh5YC4BBTsIf4MQNPYKX1bkxOI/zVWpnPKsfMubJ4Se9+
LwuCwaOEEtLha6d+JxoyhMck89AKWoADm8RrmAGIwZgs06NtE7//Spw3nwia7G+7mylBSlLVfz8x
qEwU/MyLo4HTdUyvNEsk3kDdqutCHKT2EfzLdve7WDBQLzwaiju8XAjI+M+oA+h4NBmqNX08deyi
08jYojqCqnvPkvHQaYeOdT7EmAZyuiuTsWtqGlE0jaVwOyeYcWS8Z1MsRbtxQoyBr0lpC1YZG6uY
SaNzu+rUAJDgDxURGT3bAAAGhkgABHOo0y3QZSYHWyh6K5vXf1N3W25UJpVOeq68zJpbCRh0bVxh
ap0vOO8KIgH/U9wkcipVDM2w0bkdHNRH1KU89EdSm3k42bZLwMlq35c5XXITzvkA3NeKNSnuxIm6
+WwdfRngb8YJXl/mJ5GJC5I4+jwyoe3dGQvql5aePRUCYEpugT76fUV0xLqlNv8ZGlyD8Wwk8k70
1NEJkuAkEmRlKMEI2DHmqlP3b7tOD0ireLZQh21rHO+bjdVzwP8MAZMn1JueBPvHXhPaTylXMYZY
7WT5q2CfSZSlPltlvT1HkPH8zRoEPASA15pu49zxMBofjpjm70JTtD3lcoGciOVGBR9h7paxM7S+
+1vz49JH+CxkmoV/w0BxADWeElSM6wc9HOkjRVvqLTZtA3XQT/VuPDrj+Bt9r+RLrl9nKur4UPn2
yF7V7gMmnrrXXwWZn1jeQzTQNUSeEO0R8jZ0sokBGT9e9CznXDqcvaYAnVQ2yhuIosveN+jomXbV
EpPTIyZEjVTSQq5umMtbesCmBW09eBBNw1YJZhpmIfQkoIZV8zXWGfnJ6NRPBeSKbXdsn1j+bUvP
E1VzqmQGYhj+z8MjmZwxLJUdNPV50FTo6ch5Au3VjtkE7puNIJAAtPAcI+ADbtzIuILU4WzPHfz5
lsYRyQJdTQIYof86cU4YnDtd114hMSxLZNKFZxcVVs1AJ7/KgWXGsotq6ncV4TWDShlhDuO0zQdm
POp826NWjDSoTWaHA42PNeAuJp+1ZwLUAv9hqenZQ76eHdjyUAxe0o5xrnimuGNE7c96nlnnGZLq
hvk6EzkMOfr3OKxWZmaHX5R9F+CqtXc9mJYnu57BqIevVFHbmsFHRhn7mBYWpEjhETlL97hAZent
VacDATb+W6sOljeeCdb9wWw0WPuiL/BkBChbCIdUIgUElXOJlxYd+GGAQYK1VIB54Q3RAXRII+VO
WbJqx2zCg9VVbWRZ/YkHxCZqE3S/77Um+3JPp7VYWHj2HDDNZCZxuNanKn+V+J4USgDw6PUFn3V6
ZTGRyHEuMWtmJmB4o9W7xjmTfQOHB34S6JWy+0GJkP+A8rtUlI6+iTcER3gdwryhVD8Fgdmce+Ar
2kynFCCe+p97NYQQOcY35l2PrtDV3dTdaJygpzOgLFxL/ZFWyGf11qBH/gJuusVfbWHYSC2QN8Pn
PJE5WRqbY5n20HN+IZWgWjgWxY72TEn41rDre0bEZ7jQUQdaoEW6+H1bGxlhzk9yqH23vZovKN+2
zC4jUBpqInilWf766xrVA/M/B84HNkc8L0pZw3Uc4Q5FRfLqQh5NWKUNFNaNipF/qT0M/xY95ozg
Ps+kVy3SBzddQM/GEuK49CzfvnIOO+/XFqRhmTzZFK+u/lX9xgNqJ2Nx5iL+p4Fd1K+fDPECwKHd
qL7/TdUTVdjEbhFyFhqDsHwbAwAkqL+dkK4AENnhxjWPiorjbvvBAma+f/cLzqh6WGU74BwGD08C
3k8OHfscQDxjiWnUzJ1w2UZ8D4sKdX1lFZy4djgV6GJQPs+d7H6+MU/ER3SbgygEwRi8Dabq71nO
LZW1x9Ai3luOa0F0SKE+4xF++Br406sXYBZT7ncd0NqIqfwW7Pf4iGwIutP4XJhRcXvWntssqfVz
6GCfRAID9h6LfjV3gVmpQJSotMvTwPlM6xF+Oo2F+4aqe6aFyb6P71MKdgXzNctbrEHGx1p/KazF
gQ4PjiY5pwuT78j1jHZ0dTMAE01s0GBoaxgLxj9+Lw6zkNP513jyz1rSuv5X850p2ZF7AmuT8gf2
gZ5ClbzJe1lnP5PmTxLypAYZO3hgchk4tj53g8Nuspb8Y0c3tN1YAUQHsivepgCYbcWddcnYGHdZ
6d8UmZEojLGSopMCZWcTY5Q7Y5AVuheBmDXHWF1s9f4zcVFRqz2mx4OjPPeCMcQs1rG/Khffbpac
ka2s2GCzE40UcEH/HL6shfWSTvyXJ2rX1nxVpd5q7ee8b3sBreJYYo+fg8ZcluSYAW5kHQ0iLlVO
YsDipU3S6RjFGiqBB75lkm5QSHb3YXr0u7HuDMcgAb3zhaBqqPWU2QFhao1pEUrK/+orTkaE8FO8
9OeOEo4+BiYIuRX3ba1dR0qbgOFBzZL4N7Fv3Q3zxLYAuCIKbI0D6PXOzAN46QGAcKILbt8XGSa5
Pwu20OmMC4H9ujuAvHUhSBnhPp0fS60rEWTpA1NLvL2/AOuu3KjonwMM8RcS6fhvBezss6amlDcH
V11WVSlr0PttPWPhRgzKKOszYo+bUyiWK0/Lc75OG55yWkMs+I8Pf6wJlXacQI/jhio53mPRo4U7
wibMyoVMn7ZaQaxi+QBSK+RgKYQKPU0asPALvxnfFkGSWQnHfRoZOgpjPXUldh2zCN9FD9K6FcIU
30vnLpm5upnC8BAwTuy0141u7irqwcQvof36xEkIiWkFwbfGZbaUDazRDdvsvbhI3C/DUdFyeuWV
mXrZoQpd1aaM8I8BtUjqgfR84Dlk9WSbR/FckE10/xyVoTfFRzze/mfHj7I2upd+pDiSw1jLqoLR
RJyU5IsMkPPHyXRHHXM71DEK+II5+hyB2P1M3aBiGg2RBjoVzQ/UKmLdyYb70Pcou/JPB1j/E+FR
1z9aA1Hc3LJoad6miD/Fg+PfGHCC7kHPwXGFR2PeXzMTbCvVZ/V/g9Z621/+bTwzZvf80n7jthWM
3725K2sbPjtZ1MnpqL/B/BhYwk8zdgXYzRv87QWB5GuBdA/2Xj20SzeHtAcqjn69DAAV6QzMscW7
vu1TTHDNqfZeftRpART9cMU/OkBw/dOc4IveczUQyJpYcU8xZgHnpMsl5M9YmhcI1VwPBmaA51Ep
vpsGBGTxI9EFjfuPzhyJRoqW5zgKjI2FXWBTrKNIkEXljx+36fyE3U6fGp/eX4veo07X2dYPpLvh
5yAhnKGV9UyVMhy4bTYXH4CAQX31TwLVYZRr6HpMZsQN1jYv7J01B0d14JrgZs3DBbqBsLiDZ1a9
7sNjaPQHYjflUkf45nOjlJ/IwHlEsOFBfvugCd0JNd/AXH/zqjWc1veZmVdSNNWox51SUhKAq5W2
xCXpFKic4+BoDeraqnzGFg2IE4uVnZsrp0zxtFYAh0vWO99MqnzkIJmdjaZen48TPn0CCIt7yyDj
LWQZhcAdogTGou8ZRemd8JbJsIK1SoUE4Q9bBkAwbwfPD1OjZCu1adDCuKalLUqJVcQV7Iyj4Vud
MXTWU8+sisqpGMzRFRbWMX10pgO3L+slinCBZyg5Bbut5h13TWRz3zy6myw775+nY24SrMEdBN4v
MaqHg/k8I8Ux2ZO9/G/g6LBrL9U8uWLNy5PMo7DBAEF8TajLF6rjFiTk4NB8pd71olz0DaTHlevq
Dgfd3SRY2SRiG2B5rVvk4siOSZdM+yeapP2jDnB/4o7mLh4im31/9QE1bgyP4mnLh9RvBXlho6GF
Cr/0YPxx2hCfzG9u5YoiakJmgRl2FE9V62u2ZKRKXO3J2aFpvbf248cdkWQGymvHRRmS33T06Um1
4sNO6lIZuHX2fMyMxUuyQyIuSemQNCSt/LR5c85OArhdw0/TlmDpprah/+kfjlB/7jt1OplYhfgE
34GEdKSdSqowH1pYh31MiX7XBtMU78h/NKdJbI47ZYCwikwmOaRZT9tw5ZK0ZMZeh8n2V115SUBL
0lhlPpyAUyIERpNJcGdqGWBo+7Yy+1h0eiUttDKrxsjOjQRXFbaTYiyAQAEVsVst4c1b5P7PquJY
Su+3aKdoU23LMIsv2ElqkRERoxUjm1QJgyNVcM/tCqYlBcMmjAj4qOvns2YH+id6Oq8ozOvOciQO
C2RdgdGHgn5rxZQPBeFyqyv7NhbaeWacjrduBI98u0KLTWUf/wrbK2nyBnGXiXNcdtaJhXtaKwyG
zQzYgQX+2VH0hxwf0q+bKe4qYngx6vFMrSDIBKJKK1tLqVR330LeKHx0dOGkbyPZz0HC+xSCo2tk
sMRqC0ld0pJunoqMLmJ8Ot810ptVcanbLJjaXKuNKtjoLelQga4XJcvOE1sGWwgOr+Htm3Hp7Utu
S3rs9/icI2OZ0felvhVZmlVdp6/c+1nbRDkIjXX5B+fQ37wu9LVGNGMQ63Dns19/JRBGN3kPboq/
MENJkVDU6v9CL0LU9Du/DgxM//oKr4NgcKmaaRiwkf8n6IG5L6Ei/uPPuXlXTDubGz1hiQtmQBmF
EurAbRceGOsA469Y843r+GfjEFvkp52mFFp19WGwHlzPUMow52zw6MHhiiWIn2GRcJ6gOaOzmXiO
sIq7TGFWBrSYq79zyktxiHd548BcLvO9pasO5JR9Z3TbZf+l1j9tKJOXEB62wNt0mZPPfRU/B2i6
tXMieIitDc1gBmemJcsaLZIwoC5+D1/dsveYPnMg+XP6MVfXv/787Qx3cR/LvZZciLu9ZVGJ+u29
dN6SCxY1xbkhouGz4GcQ+0hnpwmFMvV0l0ajEmdzdvRT/oAA64up54/zBOYZ+It1yOpcGPYG6+eT
RpAKz9dAng/dbH/B7DBmp51ZV70LA3lIaDvBQr2fzMR6Bl4cn91I0x5wqCPGAvpxBtqbzFH4C89v
7EBpR9j18XjKkLR3rDgHl0skFty954C1YpfjPj92q+QU2VBkK1J5eDHUGyTp8y/AhqD4XvgoxeYM
4QvFVnD2roHwG3yYSWrLb0/5z/eC3pT+d9muotgpXpIFVTLsz8TSBSTJX2w7nvfazBAbZ0fZtI6n
9Q03nH6a6yb1xRtcxJIPNF09HaE6wSe/s/q3AISWUm1Z98N86hEcs8cfwNU9o2S8SOfUEXT9lMp+
tK9607jMDvELGYXKJQHpNDscv9IDOAX+7QR+1ZEFi3/ecotFfYy53Y4Er9hJh5uJ5yvIMyuL2Yl/
RCpYpHrhbYruNiFuvsO4j2H5ffggqnUfwwM3Oe0Nh3z7zKzZ00jWJAE0lbtkNskbBPVOnU++07d6
aF1b7j0bw1Ip6p+8YLRHgV9Lv8GOBPMqS+GA7WgU2qZ/wcUaYJgsUQOaMBm8TpiSvuefX3b4FK8u
U2zuAQDogCZ3b/pOjO4wQh2/PCCL9lsxrGPeVzGhHwFdNArYLcMQW2jarWrCfqOakcG3Eh32/Aoh
sXEgV80dj/FgIjccClrwo09rJU3Q1C+E8osi9D6QUpQzdRrwYPBTEMiXXe6eEqOU78v44RtwMPZQ
Nxa5/+zQLvYsF8+ToBYJTvQh98Iv00wRA0Z1Sw6HNo7di5OpGLtW/XyihHskL6rFs/1zZjBHWe5p
QIHTHxok0d9liO0ERD1AeVw+HlGLI33jPEDhcXXe+K1uUtLNuPILegZiJj3Y5jBsX5N2SfkYCVxM
LyYd1BEeV/LPFHwOT68sfxTucb+wC/gQ2QttKb0sorTc8s9mvBqYrPSMMlaBzJOW4yNRP6Apt7uj
KHezMr9NTSoAYw1vxILLnyHqzk+FfCEv7m2lg/djyGT5l+A4Sv1hO+nv7qF7100Thyeg4ba5yPzy
FahBzJxYBYNy/CBEqUNctU8tp2DCM+D4Hrzx0/rsur9RICl2ubn/mOyz2fC5gruDvziBJ0rk7yoJ
Sku+Hnmj/SqHv9akr/SmjXVwPtmLfxm59qmyi3/lehrK0NzMPErgqlUSo8r3ZVM8u6Q0JyoSgghD
1DVUGMn/520j5oHpNci8oJamF4MC05anOKu2+43N45i9rXL3koWp9+IUmIKlhfrVSHKd9stSv6GO
7KDxoY9rMwc2d4TsiOPV9QRCDUBxVVBn6+WF8/kjbQOqJmuqS7Yr1gFUHlNhsshoVx7eu8Oi5V1Q
g/Hm5Ru5bAycEvoQsrnIgoV19CMfJXxY0or3viQdkXNKogZCEFmY8B3yLyCMj+e+xzW8MlMjJfmz
WPtjgXIu9AqIdYkyDKI6PLNNKgOxUX31yqGIxOyutUGJWIFw8tyZRroybxsAe0LNTrTghlt0NHAI
8u/KDXadY/RjLd26e+TP58aOkGGycsFnJ4Jug8ZZXqEgnhfUu8NwjPwyKQtYxqdyuI4DtFYHFzVH
DK3MG5+oO9n8In8rGnogUiqBhzzCDEY8S4cxBdIvVzpmgJ0I6NyNCny9+mKAyn33yDx9L3MEsmM3
t9v7SnDduxDL//uHpXgHidWU1KuS1qlVERPt+b16DghjRxGuGHXpoz+LrAn2/3fp9Yk5CLrU76dv
pPFPAhbTP5r9ruO3ahaI7fqJfLz8QXklN/L/yFm4ixZyrWq2QfNdCja9CT4EOgslgeOdpS6KCJW1
HKUtdsXQwribhQiCmSvppbEKFAN685acc5d2JglB9q2hv5bVPrrDPBd+trj8s10tU/A+XOC+Faze
jLfDL8BV8scqoOU3iGHMnXxbNZveXWPD+zfjzk0+288Ll/l+wZ5runoNydWeJ+H6ViNVpWwHUW0P
JQohH7MZOMdYrjantxB4oZEiTZgcAxVZqQg2XEG5pzI/V3Q8F6014RiDkjFgY8gRzRyEZbWlgX5k
3fRf6GfAZEDDosNBjYfqE2XlYtl9d46mr9JrrOrhoSiuk3E00i0AqQ0hQ18eiH/2O9z4RvcPji6t
tKii0MbzWkX4DYzx0ZsXh7bb7UnJVIVF8dZwszV3Q7mf39RjujVIZiPI34zccBha6mvsYZ/eL7wA
1XFy6s7RMA92sCgio9lq8egXOcDjZ0lyDWXoREguywFF6whNaUZmJX6NRiUFqasUrnI0bEOEtlV4
A9U9N9oq37xbwkDWUXBPtyVU1bpiECfJvdDu1FMT+OFDopTac2GX294rluAG+76EiE6mG/CQ1ZXY
Xgg5RZ1Kx4zjhSWmjmYHtkiSqWcuByLNPp7HvdU4uli24LbxdaBK3okQLLKggY0l99C0XIr/qpZG
YETHVxvDZTrkeDpwb7vfsb1OQapVIAHW4Y/Df7GDPGhRPByHr2h0nbmXe6G8t35/a9yZgRGQdrCr
U4P22z+7TDV6RYAoFRvAUFhWRXevlbGnPOsDP3btvbkMOhbEsksch8D/IqECXE5WYNmGTA1fMGhx
pZnuCRjIlCeAwEuFTXffQDb1JaKPmBgyBQ0hdBJeeAB8/y0h1TxO9qcpnu3kzZ0VLbF909KMPJ5u
bYQxoEl7KfYjFQQ1QYhj0k+Hn2bMSlLSnMGpObstZkNYuTOCGkwUBST273bDeOfJ78StkOkYMw3z
gVI/qw8DtoJgv5GAgYKqztlo+eI9sVoExzCM1t0HPcPH9esc3Mhen5cIpBDXM/sNWU2pzdxt9oIN
uSSTZ0K+fOVq6cHTrXLWZETeJBg8Usx3lYBXglGGkZO3UY6hCziLE9P20ChrQPyoh+nnaUrfYs+3
n1zwu4w+KGgTz4bIIMAUTaFIcwumjqw6erB1wrqIW05K4IjqcIzPIoSpuYIyF7+6v5YigT3wv9xw
Z/Fo0m5F+dTTXwk+tNauo+ztGVOCy5gmvGtTaErBbGZANCF4IsXBO7GJ+8gRtXb+JUdHBBC3Vp9G
nkWDV//xBvuCBAhNfxTA1NbhWLnUi+RFPosxRDbcn5AHAsT5QQyLig729HY0OYhhOaXn4OuZqm6Z
ttahTxg8LHO3iFjV0aGvhyzxyjXBpPQOVu7Vr65qmNYiAn1zkn5un2HqkLzUSRqegjiEB0psgAb2
NRDYKpnVn5XMzM8Dub9xIMRfVJMcSeRbYaMCZszxCUAn90ojcnGduVLcJDXdMCSbbimfXyFs31Z5
OThWBbvNNGcImnKLRBGKLBmvv9eGjbWwCvMK7iGB4nGk/+3RkHLsyn2x7XTH37hXbFbFHNYvkDHl
TIUgfFDDK9bXy8JhJyIfiWdhM+avgg3BJzPKgDCUgLtX8NFsQP2JIbpHhsUtGRgTTBrCe0cS5/pi
wZYj3WYOyXsGwFO5kxLL6ddN20yguFHCINCVVJKhZ4qyg1n491Y+jqkh/3d8tS3CdSNY32nHP/Y/
v1RQ9dPnuvOuPjwr2qpoMy/lBCrY/2iQJUEyDI4IJrg+TiZfzRtGX+rnpEE44R+VPvPcud+VZePk
dsizzWKa7Ho67+pQxL85LmjjxD90nLiDXEdqoK0vDPCow+aidpLX3fOwfMJAqXazscJR41MActCV
a3zfFBkSyzC8MilBb2sEZqXAcVyQwKfgCMu1LX8lrhEZIO20JkoJZ9TJSOVGkXgZRJlBOkI+Y9OF
QgKJEl6YKk+7JTi+3+yqNalepDtTStsGtNktSGfwKKqyCAIs4W9bO2290VIOMtDChWz1fOQM26Ky
Tzkigm+uL1B6m6G9KO7asAmxitvBkbfuXHjAQhtlY+O5WWrFp+ky9JX0a9hwrWKCOKw6RDvmvtDz
NaDyIKokA17G94NHHUII4MiXDmKN6OD/pbGAqlGrV86NawvU216GMGo3HvTUjRr/02iBf8GnG+Zx
7PnuYPZjENWll5s3tKh8k6Y77NuSrYwyebG/p7a/D1FplvBrYhRO7SolXn3m9y6lcJaN0lHKDcUj
qaW0c+0JhDybrYdei10uFScy/fXb6+MJii0siSt+/RE814NIOMfqVttAaqCVYhGqI62FsJBGEgQb
FqFRIyPbVsRlIcrPck3HSHRM707fRRKfdC1XRGb2NGJtlbKuMnG+a1Vb2iWXIseVC4ATUmIH1R05
p6VPOG9brjYK2L3087t8afpMsvA/RInu+rNX2ZDRJc0YEhYNHBxN3eC9IP6dfLIv4JNbRtGHZ76u
XyhcoZ0rqlXaxzKfLujXCecBzZasEvmaSFCCDfR8OjWYX7sn78j24m4GZlEt/EXFhHVDilHEu+9u
Pbzv1aliZ8Hkhu5FgiRf6wvHyuPhmef9rbBs94e10lPHzII89M9T+wiT0nw5wyllRu8Z8zpHRt8M
1GQ4BCzOMWd5PFEsxNpeNxzvrxfmDarUVSzIVMAfiT3abQHZNTIEozCVwfIf+w/fp9fv+R0D3rwf
xgOoYnC0SLaoJSMJjfyLx1mCNgWw+3BUfzKhQX7CULpIQcIB91AfNzIzZ36mhhVttpu3dh5Q3Sqc
ayjwlPWACOCxNemiCMa0kOEVDoDemcmV/SOHthnlNYu4tJcnHreejDC1ln+ZzrwDbW8YjNOUggCw
FNJ/zNDc1WiJDT7TAczjRcyjWj1D8xH7bbfC9qX9iOD53fFUdq6BM90Y2R5xi0VCNqB+y3+j8hvK
acmnSVwzkMhstltpiWyd4IU6f6WtcRaKCZNoZopZIOb0eiXKfsxGnT3WBqwy0eJIm74JAft0wHxm
f3Ovnk5DGixRIM0039MnfOHkV2CljySPod180urM6eegHnQr4sth9S0iTs0SnlP5BHnaEynPXBnM
1uUM+pjvJ4me2JLs8nloX/H4Rg+Csk20eLYIIF+thhcmYwLgkDCqZLo6I5BbH2nykV32+JbcqzPl
mGq37MiOlSYGCaGKjoSL60VT0nT79i/eEZyPN2x9ceGRGnAdvpaUmq41nHKJgSnLOH6ro7mSBhAf
hKMgOre89IUjDK7YMygcxihB2iNQntaRcDTIJuCUMlzTYdIlhHVBYzydSGcTBGIgAkhYa7M8iJrX
BvHFYFhW3lP5CMW05h4aHWhoHl/z8dPr9s2kieK50m7QOTYrBMrMoWCJL2aAH8r8ezYWYIHFPQUp
Wjf48SzLLvYdepykmdofjcH2Ryxpc/QT0i+j25Eu9O5VeReYBAKCpyzEmumUwOvH3uvzHkF2z7uW
Yi8Q6mf/sO/m0rEjchK0PKT4maf07WBeuPY3xwHk7LGgiOOk+8egZVzkKNra30ZjZDUPt3yZhpPT
e3VaGegixdgNf7MfHfWvjgkCRDiMVAte1nPF9Uen0Pm6/UGJjuiCzqztJ4EzrXYzNKKI28eNTffp
Jk7WOec4R+yZJ+AOEQV3QFalJaDLdBYwEmkZ+sGutmM6Q6EPV5fa3S/oKjzsvRa/HyPpN8Vs2beZ
yzl+fbE9cuN4lPacW1LH49l4UQTnaMCkUW0DSwp6Q7oh0IWpoKQngqZR9mTEIApj714/N9zd7iP0
OVw/vqHWSl2Sou++s5qlLKR0OGGbSvQ6IB1r5iwJTOYmsCueQXlqFq+DvFug/GnTQpDHMSbBzlmS
Bk/jvikgbg1vJ2qV9Of+reEytX/2YaoN0f9raJC8OW7PAjXQzSKr2uG3nKQLu8Z6ZnLDdatju9Zd
ZFaIiR3QxjFStaXjLvWLOaZen/t23+7QyK6nVZggQN0ALgOaud808xmXOlqmlv/CLjv15xvC3qaK
aQfZ9U5I4+XKXx3aoi3WtK+Zc3xtXf+pyaSGKvLvUY0WI/YdlupQSDMahXiz1bTEJCSKqK3WGXg3
XRYeBwA+lwyBM/vzE8Zw1LFMIjLr88fkib9uu9tsJtyI2GpKZBy8ZHn2OfV6aHpgyKQ9upehX+8t
LDSuiYPbmfJ0eQxXitVSNowhX4k/yfENMTcddgitaRSE6woIw3EkZnTxL8Fk1mxvaO1B7G4U5NZY
GtOsGglRaxjUjXvrS3A2CEKjaYO/nuk5SYUKs7nSZnJYmj/q42AQwk7nSYDXdfJ6/Pj4ov9XWWLU
VTJOus7WBQysfe5C1Q+5LGxEp6X14lc/QtHtezyLOURJzMkg1IcE1SmWb4BGAAMpfCBmgeT5vF1U
X+TcMKxz3YsMhTcpwi5YS/0AFNpG465ZeKC4VDZUXdYH4CkUZQ6Ckq8dJ+lTRLuxbXzdnuQIInEr
jzSBJcbWkdUZwuSLO5u+c8sKEV0EecnCENRsZ4fBLyXjlVXr8tE5551tbFhAOTb+DjHz2IVByqMC
Ykbg00jt1ofFni2aHV80ciQdDw09Dmn6Fxeui37kfNSMczYPoNTl4D5yCA4yA7YZmbw365l04p4o
5dFC3ISVF8UIbqbaPYm5z5JO22dAQxN0XCHVVHZ9VBOAKIxHKfK5amTZjr+V+unq6TjqqbLQKLyA
lCcbCaADorELy2YChM0bcqYLHOBpl6jjolTdJtDtk8rSB2ypWfYpckxgM739B+z6UZN3zfdF+YQ+
Tq8H3ZZsIuZEcRLayiHsW7FxGQd6kXucVMRgqeaR3p3/OvANK5laeYCgaiY+h7yOdi9T6v2Q8zdR
tgKg2psZI2RhQTqCw9Ik7LzMzsm/G5mdHm2HrWVvr9UT6/Wk/CraPEZvWNTe7hE/EHDXANcrUWJ8
b2pJGTOYyXjSYE6QfiRTqmlEIiRoLcgy8/9fGXG2ATW34sklzcmxoYjEhxLG704vUwFF0EWosoq/
ah3Ytcq8ZzFXLnU9FKG7CCWGJhcpDBLfeUktR++O2f/NTqk4qUgqYvqP0dgR1LISVt1ETCjnxKj3
zFrW5KnEL52LgMg9aTG00HggVDEjDADqU0WjkAOyPpFRBejzCD2LSB4UeCmEjmo3r5bGDUHj31Bp
TAqgVYBHEEDA/uwpxXlBFJrwtiVLVT7y8zFv5SyUQmdmt+eN+w4pkhDtRr4WoaE1nqi+UImCKaiP
Qog99yLjs51UAHZBrfR9f5gZsptMdxkj/ipMjQXp8EXOUNfhU5hqapogiD9NZ4cr/NpJZmL8pmPJ
lNibzi0SGtdRM8tqrblOBXGJL9mCbvk4ES7miL87uPaCpfegTqfr1W8ipULDRBCjWMjZRKxo1CfV
ifsUpR00g5zrRbwFbJbbIiMX05wvILnKxKbk4FGkqjQw2v0sRz/VgbLLJFyb2vGoByuqyNnX5Nt5
yufWRmvrQBWScATsaUo0tICIM+xSF9p04xBYUXW6edv+JCEOoPbFrKryB9err0DYwQyFi9uT2x1g
iLoahQRHT3OA7LWqA0+114ZBa28nV46O9I9y5l+swbvSbQU0Lf+CWhtoMdauniKR0tdCw7n0WCGv
xACg2RIbVHluvyQgWIjQDyJK8K9arJCviRiP9sofob7xrRrWGdKYBQpD1ylKJC79qrccrMi7tiyg
W0heRV1HfAel9sPtHh8IYJNf6+ZTiJp+0o3HfR5QAAKj9gn3bgqMhq5Y982cwfVC2KIRQWewYSWg
MVTud0f6DE8GLbcTm/q810Y5OJ9QGN47l/7VgtkiTgfqE4jqz4G3sNLPZnJ+Ovk81+NUY8I8P9RL
XxN1yb+HSMQ5kPJTzaZnN0km6EDSLzYr1CMoVD8SWZr2ZIL6DP3zi/zBq+/TTXMTeHUQdwn4umcJ
VGAMwzqAuX5UIgpdviPxZmp9iEvJImsg9SJ++0+61gnbdwDM42Z+Us+tQuSexEOGIrOVDmzUJL6F
FoR5XLkrEs4jdKg9mQA6crqLb/0qbI6CT15pKeXryg1j9SLejsodo1r6R9gt+kfOjuSyiQArUNEG
0elZc0S9NvuDnYKKo0oY98r1dQMinu/7FTu5TqZTqHPbJEjoMq5pgsjcWC5gJ70zNjyo0iXOV37Z
10ZEpc81b4+Zuza0jI3AAcFBafGDTZEgwakP9nwSO4/X26ZLIJV4i4oPj+0IWw6iUnU8T4l5qBEd
kn1OHYLsVjCxM4yInTPo4xs3K6AU29MQdpPmJr6ONYfN8X0/ypDVwU4sTlkVbs1FLbbao0PkLaNk
mfei6KjTtGwHAmLZyENSHUVzkCODruLp2ec0QdTKiJO9cwU+OaRsmlDAdbmLWZQxgRKMzPbbvAF/
g79BHlBoMImlurnHzDNzRP52SKja23np7raN25NqFwWv1FGp5xaMWpv/gtImsKQJKsEn6akWxzf5
AMQaIqkmJxUDmxCjhA74h9xzQ6yCxVd195sW6F0teq8bvLD6afkImpxQ2PB2oiz3lrBgYBJrbaCu
cfDglKurAB0vFIOZ8korgnv/ZQ6rY4w5g69C1r2OqF5gD4xX4B4r4TVPIdv8HkPgyUlaX6KWSuSz
/0HOpNGDt+pPV5QDC/rMgb/8uwV9UMEVDblqKjnV/DnqIKuF0v6lAIWWVyOSAw9cYhyvTWzU/qCI
aZF6FmVU1ZUiuzqVwxjKEDTC6VbfSAbw/XeSfVr2jI9aortyMCa54nlhsdhE9Tg4COpqxXG3u1tx
rcSgQODqRq3STsxWXZJaH7iVT4qpB54w05y6ahZ3N/OvzGU3H0VjSGDNZeXNSymZgrsxrvKK2Zz2
KT5rr7anHDvatWes8UbF3VQ6IWgoAalRk69z2LqwLlkVLxNC4778Kdem8DMe4dhuUnbp57uqfpHQ
VdMVqvioMKpVf0WecNmCtzpWuQJUINrjcKhYEqLapH98Azb7vCtaZFE8wVrffHWbW7hs5Ivs+F2x
/dzzEKrBVMwZ8cD182d0WTSfbv+vdRnyNdxlFyoPKJhfAPxIo0OK9hB6WapMWLW9uv6SXbxO7/Mw
LbJeSN8mokCrSpu3BA/2lQZYM9bpxR53MaXRA9dWZPkVGfXvC7XS+JtGYt4t4PskqHwQoLXW29FD
nyGw3o8KUsmndjX8sJm/KHF2awIHAxhwycKlobRTACOyFlEX3SwRS4m2boIlJoCeiZVL0js5aZ9E
k3K6krbQSzWIAaAMYRQP2aSs6l2lyG54wkC6lGyfwGpew+3yIVgUS4SJQnfW3+Dxiu9dUgBndSrG
PJXLhA6lp2rVYQWIQ9MCyMEbyvhG0u9Mp/Qqpvcz1I8ezcyLDW57t+wKHJFrzzerEdrELOFBZ1Pr
zzNT9PfhDrdMEQzgBi8H2hn38l75+fVYWe5Oav3aAnw5KCL0XeiOr4bBi+vZAB+eJ703tmyY3Ce8
R63VnCSs7dxQisKCJKtIEtJp4VcEPeh3VoVM61DcYsZtMMB39S2NtMlv1i96WcBc9QB5lOcXmBNX
p2K3bCcXIE/RBIWhDu35EOPg0Z0pPZaB8g5Z8G88x09+u2QyC6G1h8PiOp+ogvaO2Kdz4T9vCX1X
6fpgg5lQJ/NP0kWkTb1nhrHfMAf7FJVRzA2aYJWjnj1XgSA1GkkWhlU8KNvh7Ae/bOrqGgP2FEUN
sdrxldMuc88czAqueMbVjxLQpW9dvKi5oREwa5MPPigKopOm61rDLyCtyj4877WcabsjTzm9y3HT
9DhXcy0zxl2ErduFLLacShLzXLZPpMoC79HRiwiEodDpRwHhYxFzJFuazAgmEEO3SkfPEs/Fubv/
w84ieoj9cgfsJ0yYFYUQ2fh2qlZZwpvyx8oEsh1VDQnDpx3uggCcYadZAmnoxFcdpRDyBRl6MQux
FMALUSEihfJjMcINZv9CSYHFcfPhvOiDOmqVM4w10oblnnGF5Z8JorXjiSa+82xaAjMI3/TVFHHL
yjSZ/1uerKyG7yPaLhLMGW3FHiY7KgJ3sRKZ57OF4ytJYM/unPZWiPYSxbFiOMiqmshYvSqNKgNQ
t0yLfHPCE6srtdFR+Y4Kyo46lEH2Lqeaurypxpheb/BqHaiVdgZFRgNLLrEmykw/NqZed3ZsM55R
OWWIMwtulHTXlBNUIVYu1B8iRmeHlJQ80tb3FI+c9e9/dzpvOVGgWqf16Q60kyLy+G9wY3y81hYN
NIgEcr6fMPIzKQ+y23V4gnShrvCza/kSHvBX8K1kuKW6927yu0CdxbPv/i/TjTUiugQSIdLnkx6Q
oIpSN8qNC636Yo5OIjG83Hus+806alnVl/Wj766u2jqxOPJkRg2Au22TA0Em49rL2sTsmDTqO/oh
B4f2COus95ugfX/t82M9ZckrCWaiwvIWFqKY5At6HH0Gh2DXG97YQh1ZYxVAg6F/0D/3756tJsUD
MA+mGq3v4d07x7d5e8AqaepXNVl6YVmt2sAXSmGDQD8B7YMlR3Swp+jZ0xktfhWkIgXh51PLXxN6
QVcm0L/cBNOojLE/cHcdCo698kte5e9Y3RDy7yHqt12O+ouec41ok/DEBWGrDWs21akpo10hNwul
CEJRpEs0tBEUCvEANWjqPAqrPdSwJNnW1WvCMFT5ZqUJvjbVn+x62SerOOJ2GNcuwBbZUVGn7cpx
Q1/MGCejX2vYZsJ17U/YVXrFv+8VGYAeY97g5TwXJ/VqBy+GmqUji1qWUuF1/t2mMsYbIJHTu/Ai
O7bp1pjVqe/FDaS5gemMg0YVcPHT8/8pS8QuTXi9XX4KGMM1S+fYsnhSsfsJMC1K3wjoV3CnFbss
FTCz7WW6ri9r6+a5L/Nva8xpXX8yT6na6Kp8qbEHxFPSTWWAyE50DgFNBNA7UVmfY8mSrAo/VgM0
AXbWtYkphQSyw2kSp7tV3rH4gPY4K+YbgoDOpJmZYSEI91TiyEPHiPOTjQXNW0/MMRl+s47cdo67
tcRxQ5IiOi6XP9/VNrckAjUVZ/7aEoDuBzyg13DxdQ2zxcQZdyGl+vJvL7Y5w8hHvL+AzLXf3309
PYtK9Vm/iZetjOGUixBXl0QuQT+UQ8g7X6LhZXmELem6NDVxOXBNe/jzf+vqtC/CXNbJHYtxEz+1
5YsjjQh9WD6VxPZK4tfrHhwXfb7YCLdIes4pcQA3jQUhTiq6IVmMbNXKAUTzEeIfvGzMZINe/fB2
F77/uz/1mCeN4hdo2AQEg297uJ0Jm1pI5hJGy9W0o2d3qniJ5fxwi94UHEveV+UqvNP5dCtoIa1J
2Qnwoo6ALZJ176OLEwzIkXdq3E/nykRXU5tsb2Hklr4JLaXJlnJ/rcXkYqkXRbPcPjEA6C5OWOtD
AQ8X72N/VvTE1cFL9P5Wc+NTYdh/hZ85Ekm4Rz6i1XOshyJ8rIfsiOqEGQsEOpvI/Le7lmfu6HT3
hJqu4HlIDhpq7hzkbN11PUHThcCE5i90ra7Wj2VMJ/4D9rZVHVULEHz6vdFR4PZ0BsITUFYnW0pI
PL/39RLHeeIgZbEXy4hBKOeIpHKRrYyWoLYSDT09iu1rqvkpFOLfY+Ea8Zt2ULhAK+n67GtjCqbr
/blZObScUdpY1JQPiogrp2GGFBchis20JMtqINE1lwPqQYIK7UdctbI9UXejpF9HmZxuHkW8ILPC
AeDUeGy0V2nbY5yClRAAmaO4udBT9BDaBxG5H/N0SEAeWzllZphFjhf7EEhBuRr0pEbsoQc/UejX
RAxrpRv5DmxFsastBmhnqRzFPc6XxWjjsiTLjv0m6/Z1CthVSM/Y2+HAfn6bg9ewSil4FBhVSVeF
J7+g1GiLTwTBnSvDTZRGTZdy/i5CGztYwBQjkyZH7RL7BdVonK5FkGIZrO7EQQeiBhZfbdE8BhdA
nntbH0hdgSbGlAcvp30zl6FicNfGHUKdIsXcmosHWFoEFL5f1N9eMFiJ+pyurDPOssUUpFGUe7ep
emI1L80y7IElRfCFBYzGfJAnb6DINCOH+3plD39iLJXUXc/7w9vc7h+hp2wZx6cFkpZfhGeC4Ihe
MSWMNYwvdSGEaqRa1wtkx+TSJj1Y6dic4wVKi4Lnrxxf23IVAVrCL0BwQiFLU6MyJ9rk1ZK0c6kK
m6yNWthxDGiDOoXD7d+0Eco8z8rOXTtHU23usspWRUeAA96Sr6BSO5a40VMlzhJk0DKz1KrYWqEX
hJb4mK6ZXAbAdtY8QzywakEtMyVXcAgRfaz0a9Pm+QW1hyZiOg0uQRL6epfwNHDGfWU/zXzvHGGa
z4wvw3so3kYORKT+6cP7WjiXqpk955RViK20bPqxOwwIKmj7Ps6pO2JSQqUiCk038SVWwTzNZRSG
FzPAa7mL0WP44Sq//igOjrjTWu0MrE5D2/nSJ3ATkhqTWGBv/+2KRNKA2z4f1Ry2zN7a5sl6fBza
Gy+Gkn5vVYZVllxdlI2L+AeEfqW2Wg/dp/NyAFUaaT3QyoBfp+dHNBfwg8lne5PmU7eEG/VhTTNZ
SLizMYqyfU3MvjvI9E+bdHZSILzHSIOoZJcp5ka21xerMEyyOi7YcUNu1pYdiDNKiOYU5JV4ssqf
YF/KRIWL/+ItZ/K8AtWvjeJpJgIrmVEOFzAT2VrrlGmsfz9L9ictC04s2EBi1itq1ZcM1hRdbPkL
XREZHnIaWmaupHxJCgAkfy/nnT1vXL9AcevVWZgpwbHNCh/QrVrmwcuxpaJDZ4p9sv/745AREUl3
l8mvTlpKaocM2WjfuOwh8xVgn+xPP6B5/4zboyibk/v4GUAXcOqO+je8gmv7M5ogEjPAcKkbZIlJ
KTeClAzBhJpVnW9azWwJqGrDntFH+t75d2d54zs7psSIhnKce4Mgk8OJ1BR7mHCJknOohXUBCGv2
PZCrb6/UkVdg9X03rboQTgZvmXAfAAKOpraKtRidORhl5kOUOJuxkosx6ee53Z60/gOo4kllNTh9
Y7Zlyx3TNNno1daOCVPUdfMEeIg819f/dRC+1AdUPN4AUUMyJYa5TU1O2LLHPrfY7VLEVnWfMb1m
OiEYdXa2xdyau2ojRG0TysWWz0myBleM1al6P2uWLwsVPwERF4DA7OyiP0q5Lewmf1ZAcqf8whzL
fUwlymotw3wRTs7lPs7TGrW0rvigWkZ+xJ0OZ14pcRhERSNy1q3YEiHLO6eZGNhMLfPKkh5dKO1v
jdgein650L3x54WtcWVhBnVHqOjiu1wITvCf4ZNiHxL85WKQmalJ2vxC0f4XsuiNvi5lH3K5PJn5
D/a1r2wm5YGVwvpjfbXzkSPkmc/hToV5k902fhHkKGex2YSSzfd9mB8Z/QSKsMD8t25Gv4iNFFj4
EO43uBLlqyG+tI/xNUnAp6DvhBObyQSY6pPRIeV4xKwFXYrUx9NUtPa4F6ecOOhCxe2K1WWbF0np
WqyhFkpIpWSSWPfmNK9/oVEtRA2a1VjkbcqU2XkzNNbeyvi21PnweHqrdpkknKte6uWZMEwWPgIL
oHIlza6ACodEwBzYGydqDGl8ttoetd14SitDfGUghnKPujuh3DLXUPrNIIgXeY90lGz25SmD4Cj3
7qfsfZf7NpRWPCPPQpggoGBW8D24S3SiB8zll+Nh31K9+8ilyCZOJdnpHEfRHy9KoauLHG85Ykjd
kkgxpspiuiOwG+qSJdYrBzTXud0i0UoLzghw5LPRYU8LVSET3wQMnu9lJ/rWJD3hntAS8dzLSOJ2
IbeITLpqJI+nIYGBST7TdMBes5pgeRibGLAn0WC+dMYRBuSn3xeLSuAf/DfiPKjEkvVZntp/t/VT
LCuqFaxGzyMV/BqKuRfXsoWYHWbemefTDkk3rhtX+b+R/uavWpOyJBuKyKEe7PTJyQiSWEqS+FX9
9rpW4fjaI/83oTFVXFH/Mbr8J/3UyitIVNI1a6kXMtx/McqBMT8dfSubnWMoCTJMjR2WZogoyEL2
CtJS7jzcf7OqVzTSClLqK9zsAT8U03FEf3jQfe8vb+P++y+aBfQaKQzUURfDVBm3E3EEQF8dDytG
6EnVmTL65pX8dArlvDnNMgX3LyeXyU8Af1u9QGHG+P2qbWyfuJx8aLL5h+7u9O0vlczRGkzFSfl9
pd97xQKK56LtxKOpKYoDwuSwsGZaGGvShHwBT1hxAzG89DuHkSzvsPta4vChpIl/CH4pX8h5zcDf
3R6Z6x93W6gZGnE6vntaaafxvHxtaAmgAfaoIaIaD7hHh7wVb+ZvWU/WV/lS0WzG/thEeI9aEl2c
mYxBeOjE9bnxK4LWqFhaNqfXQclXqVNE7gVZ+cWMbn8AKKf5zqSV596Ll7SRJ/Mdnbra87D6Wyhs
ARAhVoKktUlNbYdq9twuInocHI6tWeqsAa2gtU3FNVFWPR5lcc2qXXh77uQJ4aApN8UEn1IPUke0
hYm0EI8iUQnjIlH0PU1TAh5sp3ShT3ioBD9FMJ+NMTbcZwoQzo6MMP4lOU+uz/mkdLHZjGDusH5i
YH3+j2b5cb40gaRzriGkoJYaKyiXsTzLMFjouHBfhQArEhQyzIbcPaQ3Jzfy87hQiFZciW4HAxVn
yoeZLHlhkyGVDuSeh6tVYNtfZmThevA3g4lSvJGSwSTJb3K95AkknCEA/DXoRm0F1lTg1Cw3i77u
+PMUiGlhBZrg9NpuldBcKX1GKjBO71ldJASJB8nf0RFUG/Zbn2C+lraUFIXHNBFsOWzQkEKqow8a
S/KYYendvTLjNbKlMGEXAojSSh1we5tm6p4VFNuHphv8M1AzqX1wJTzp33bpeQBRpi3qY2T/MJpA
5wVH2G2Smdz25jQTebJqig20Zl+ZlIZkApPqhJ/rBz517r7BNniOvII5HvD3lJpPCkwna1lRyh9y
L1hO9kBfCvL6JasfkO43UO2B4RjOhVjcaTZXIzZNAM0bDN+JFpQIae8+lPI4Pbg6w+MG+xM6owNU
J+XMFPKOC0n4+/IH97ED6lPHjzQx7WZPw/2WYl9PkwMtCbqr7ywFacPpJt06mwjfBdtSdt5mJIBH
YZvu6sh8DEumcb5Xz/u0H1nrpBVsf4uhkJ2rd87h3vGXsK2fW2H2T8nftzZ70J/+aeaKM9fLdS0H
r93ctdaTg7v+VJ+sZUz1KjdqmLQH2MDJrAfHmWtvXjhrkML/bswVP+p116VINht+63whLyYnQb6M
7y9FyH5tCFtDaVesfg6ir6aG359PaTPsKq5TRiWbQ/xiKFByP00zDcgqP+UB0qOXuvMUInaWRbLi
2HNL1DZ16CJeCNJ5q1XGBgdmxAoLyEDcnVX02G1wbreJ1cbcXREcswbyvTdc9wCWbSvBesjpWjoq
5K/vIZdOD3LmWThJmwotgQ84i+b8z5iPX+Ss9bb+l0CqyxMUhkjr6eBqJHBWbbdg64Blc+r5Jaqf
ZhCV4MEXirF/k4c085/BGqH1TuWFEbiXW64Jyx3+qS0vzPj9p2yz0o5Xq+arPgirKEvAVfMgUJdh
lbjMI3gTiC2xM/qKzoZNwvMb94QepwGwiJSeS//AGZXFq2zQn+AFb2/2Ik2E8raq19Mq7IKiFn8C
bMKx0emam+kEjpF27nLfbuBdRLlaAdMRYqWm0GaeXnk7yYj2eu+Y7x4vXa2YLBSs5AiH/pBUeUyp
oWoqAX1HGWigkbpZQdgjcpqD1jUu3P2TtOJ3vV99G/oKdv0dGMBF0SnUdSPpHVb4ZpQDwHX9qhkj
XmBhu7wW/yclntY+7WWeLVhRdivMrXNTTUGpEZmXyN82YC/Vy4ONXSLufOs3znPIqNLXl8l/Y7PJ
BKHrgbj9r6yCis15Shsv8hoitOWV/7sO1x0dlync+t+V5ZoNsePkkHfJ8ReBXcHUuWrEZxKIEATA
5P83A4KpCtdKYxE1wMVsTrNMoQ0GTuuSYam1MF0cJxycmess43COm4dIUZzGP6Lv75FRtUl2MRPt
hjO2E4m7Bmz4sBM5/uruX0DzON7ZF0ljxYDtS/6XPidnGpDGU1alJznG0IONedb6l50h9bf0C/Yn
mPrTBN0qRMGEmy+RdTbmNND8c0fAhawNRMPlTH3tdnKsCY8Y4eP5WiTXz7Qb6mrE4BPDhh4eNuYo
twhF8ru54cR/DocVmmph4PePZ4G4Tz5RvQo8L37Ju5FLFtbSFbHsBRrhZbYraqve7ZptaNKJbCVI
ozt/slgaLqzoU9euuTTjjrAhtLLUIUcRxolkTVLY1ISbdqlqLqaE5MuPneifWUh6FAH9D9R4ndC+
RQ5aybTD6KgTzHjvtyYMzbNaWpQ1pSnjy3uyjvZHLiR71E51jNwkZ7/EmwRGhHvqpbR1oWBCTGHH
CIp3XYcEmSUyv7WXmCuhfekZk/hvmKmZ8+8mqTd60U7AzrUhKZKv6pO5ck3zDYTy7+MhQlhluGrq
1Mrk0mdFW47bb9DTCD240eruEGtyxKUXbnuIFjXssxTgiKRkCuvfL7jfW6IOVGg2HGDl4gH7Z+YP
i5Df0NDSUQ5OM80xJS5NDAtuRjP9NUGM5CpffH5U4OXVt+JaRENcz2019003QB3R/4Ldf0xiEZNk
1mzonB8lcaeXv+F5AugQKqFeYNXAFsOg5C7K2evpIGXT92xzZO3ueERFrVqHmZiC8Y0fhO6/S9OX
yndtJWOY3KEwiRQHpRn4kbDvc5od6KrHqSZpHqHGp1ZxHkl1/7Vec7mmzEeEmeeoLhTu2Jdn43eF
Tz+OzrcQprpd6XRNDDl3mwVdO8sfEjMW5sU6JCJIoKm/JIico8RYB1KxaTEflqme1rtxbXOKeWkM
VcaX80eK1PwGMHdw2SBhZ0+tPrlJoyNEO1qH0ZwtOjjYRmEEqA2F37bLWwqpKX+9zFyF43sR6x03
R3H6uVacEUgZddkBmnouswzLO+L64Wyp14bw4NdjJAHAfD2LdSk9Inr6Wv2x6wNSxgotE7YhMhro
uNDl81tRDmO99y7nOXoWsFLqbPXYlKI8l9nPF+ub0Q7O1IAp/Pi3Xo4PRwwGqI/Cdmkuhp9bA3Ai
ofzWwP91Gj9T88PfMCAnMC3SR9Uq14V9u4dBgC4Oi9Z4elPCT9vvg4Viajd5wH3eN2XUkObfIS+u
J4hAp5xh8W6XNVK+vT9IJCqWUzbDC3jELiWZ73jS0Tn7BVhDHEcZOi4Uf/0g94ONfXkLm4Onwihf
QWwaq/F3Y9BrVlPlDuL2OyK/h64NbTiqSXu6g7DgxT3ddOnQ7bMhfdXUOuakJ4T/GpfFI7RTGYqU
CKPj/Qn+P3BV29/+pOnVJWII4friuqF0QrgsV9CJYjx+1o8Ny5uKUuHtBL9FUvMcvQB0K173YPEI
wyh9trICkINByReRQJHaHMuiTqLUe0Q5YLpewl82ZXn64vGIN+0lp0oSlPSL+PnZL8BzxUvUON5y
smBCd5oUaQILMFS61ytgY1kp+oEibu5WwVpcX4YfXiUysG6ffSen9bVRUNxqgWVMBSBD0Q6qe9at
gJ0T/4i65vpNeCGcjNeTZX9+zuYBD7o+39+2UFItip+KjRYQFw79v2IJw0gvpgBRG0Y55ndcnu1J
f2jRi40uiARYGlgulJ+4iYSsEPpzHFCaE3s5gI+aNgT10MoSgbTc0rmy5KqYLTEbOA6ls0l8MQJg
YtuLNyrA1uLqISpksbTunCKp2SgRHiRJcZgvsxDQMn9zZzTeNaLUQLZMubBsdc/OV34AKJDc1tjc
jYHOrzPxwjWv1nO77xlnW1zr5i0b0D78Xvo22T54OjM9DUM/0dKemWS2rRg6M7mLZnJy6OtBoxFo
TZt6UIwhsrQNnOl5Ad2VhNxD2ohQl+6kdhjIEs1bB+xHTtToaP77j9+cxYWZuZODR2XQTwJHkMTs
Z/cFvWTBN5RqwnnmTV8HZTYY0MjSnd+Khx0gR8eumTOhcuCVghfGmL+Z5uJgmzwjPU8OtAcsMS7T
2Ootdsr3iFCBWiQeYGh7iMrEjXk64fM13Jtif1OmIJENcxVwH/SqhnO/fjdiu+2PhH0RQlAz4vjA
W5jFOKumlb4ppWaENkwx6j+6OkY62wBKs9iA7cK34qUmTP14dd/HOwzpwQQPXchj23dNcuQgr28z
K8cCP23KLwGJ+DZvYhDjYSFyAMKmabldDzbMGCbqpY0rY18kMkYuevEXMHYh89YI9vfohwopXhpO
0jo/omYKDQ2dCS6zEVdIzZmpkazwu8L83vtmBGezf0NJ5t1cafm4eJ6SRX1SdOv4YwOtsJjLxhlH
Dg7l/5ByLoR3Syh1bu4Qv+iaLOX17Rfr0zAmJlsfy0NYhg6DWgCVJdkzSpsTYp6xTERjEFEMY6We
qNB/l7U+Fj6TBsiH688XJaS2jRYJIWe6xWzOgLC73DPbp0Iq0VE7sh3c48F5+ZWySrUyOUOGIyDI
HDx7eXeltXedrODGVul9Jm/kuOsnrImWYcBQ6k3d8YK8rFwa5dap/XlQ9qXTR100hI62PLLQpozy
UjvPNstHcj8wna5exAsJIJa5R8nSHZj9mu5+ssml7OA3bapKP66If8NIi68q8LgEh2msfILN8Pio
Hfu1TaN1icwvc7ZvrSTdL6F3tHQYF8xGD4wHdiTgQ2xaljnQoTul78WAHKevB2MW4PJ5eKZEyFu/
AoxGyZhYFabDngfHl80Z5oiyyG1/vmgkoqqcvnyO3d8X8UFF10OyOvqZtqadhhF2o2mGhC2tztTo
BsB596d58cUcKCmaHSIuJEWLZcE9C2wiibKD3H8+hxEp7v5QA5IUjdwFHJ5MhkiEQyZMdvlKvLQu
mUtnGDfR12bkt0qMT8MypdRE4G5wRjXM0uPPUaxJTldYtCq4E60BeqLcOg4WlvK3dHMLeizLCmlG
Uqkc8wteZ4SuhXcKIzgE/rXpm1USGycLJ2ZGKjo+ssAUuA7SA1VMRa1/uzmdi6BCkhy8gWd1t2Xs
o0vaG/GJCVVSIDC30TEe27BKR74WvkDSZqy7jjCC6NWn7prBWodG1veGwhPC8l6a49mvbCnYLMyt
VWMyiKzyg3f/b0IW/fnkac0u8+Mqo4B1sBO4GSBe27+ZN+kiNDfNUV0tBHU5Q7hVxqzNfJ6xKohE
SLPJEOe+rHaVfZ5z5N5/BDE+RrPbOQ/PnZT4GPTFuddPrNPbTKKG3fN0b7z66L4qGY2NsyE2rLm4
I4TxHOfCPEakwv8HGJxHlIFApP+xFzQ4hjVR58smFJ/tF37vViDLeEZE00tL4kZnMxR3RT0c9G2A
E0ipvWZ1jqQKAzPmRYWRj46KIQBeXTw1kNbx0ZZAIYPPbqBnPL0Kdz5GoGe7G7HkepN81fBWIB/n
hqBs/rAJSLJIQWBKNFoRCYWAJkCh9ym2jlibL3xHfRUOkxO1M+mQyoHBgldmZc6dI2h1BzjXCG0p
MyFohzYZkuMlxDmbZcfA8xTqN5hrml6tpMK3Lu2d4Y1cQ+zz5vN3EnK9q2D6HzjKX6ANkkb7upTz
IlZ31Xv8hbB4niL3joK7Uc9a2IKoEgwxytk+Sp7BOtWGmzNzkCx+Kii0p7+culLcUvkBvv15QR5a
enoRvlUjfOR12cwOOeNQ0N1yzZFiLcKREsrKaVfb5mFrfcXonj7AZ3v+1o6bSB09ksAsyrrLJqA0
YS3I2uTiJmXgX2JKcvX0rczv6iDGKopIbqQW0K9gWx0GfQRBDqRK84C8/Q0PYruk1o6GIyjZru4v
w4qRUf5eYnlVpjGHnt0lf0SumM0ZIFmxl4mRpr7+PZXYAp39D5Rf0ukD7MpWc4ayh8aDWWhHGp9P
zZFVzZgboHdAcLYVgIGZ7eLmBxk+wiz4n9ZM8vuNTRibquhV9OB0Sl6nyt+CNdghMsJVtxtFVLwd
AONvsbKOQYX/bS6V/0+t9QjQE9cqh8DsaqD3i0hXYY5Wtq2B3CjHkf0fNej1Do/kpWkaTt3jOC8c
LMvcusNuU0NiQW/+h7LjAwW41dEmR/z7R9aMo5MWxTd/70JrlNvaoRcDk0nkm8VwxSGGobqjRRd0
dHC5UlxRXUY0X1XEuCfC3bi2BxOTdIVEufAGXrT6lkKN2qMUVoMHVItBArr0zEvGzYeCXRSgMPbk
q8wARNisTh4R2baiScCEPyP6RNE0mwXaE7Ew2I324xlvcK1Zy5PUxM1O9OBOAEW5EVL8Ds7LhoPI
d0FMf4ftN330YGc0uDFBkPtTe5qc243i8o6D6Jvkrxv+TQC+zRaYcQzz+WO2A3j/HRaDqqMoL2iL
bJRoIHIhbUOJDr9yUWNxIAJ1RMtw6Mb+uhaEtQXmFyGfrqdm9es//NGfxSuu1OvbCMFg+/PIKaj1
5XL9xVOdJl7sXRl2yrHGYk8eX+PfcSwBiS1vqZY54HcfR1NSFDIPB/BvCSmPNJszJPj+j525BzVb
y882wwblu/sK9MZ1bmxChTQYNXpxCej/zwy87w1uzcLSQJdaTqBbAhh7SbjWdHAFgwFMuitA90GL
CjyWxKtqV4BKp9/RbpfBDN/u2mxFftbVkQbMOeQ+YmvHCOOGFEaTWLIovKfWxuqCMm1bk7hzF3Iz
OFia37+RHQBVt2NfY7gnkEbpGOmzBiexc4yIU2BfcokKd6TbmQdu9V6DRpaCFVn5kmd3Yx5thbhY
lzfzNXwn06u5L5QgyuZckk1CC9auyBS2NfJts3L1/cdPvASXm5gXahI3mrzyjxYwwltF+Yz6Vocd
7qjUYlmymET8l3/O+KjB5zoAMlp0QeBPV8TXyBXcxHdBC448vuJJQvaysXTVv5SHzm+m3TSpUUTJ
xpZ0H8oslWrFXnNYsgRlgubLIJnq/yWP0w4O+9VQPDfXdV0AsqnJtlwQMNVMmdw0tuv4SjTm5EVW
wl/4O2V6DGxWFDrAmjfTbd+ipY3F+GXsSQ8Wdtza+5Xcuj5LiqQ/9YJDiziVPvH3bCvYFGgfOGx7
Trxq4JeQV/5sDR6Y/hsIii7TIijJjNh9qictxhQCoLgnoaSSTr6Zt6Gm9poWx/3I6yjyi33LhBbV
SSmH2c0uPMg4o6ghUH8Hn9T5pS/WftnvqgvQ29sECYAvo4RI/tEMpIS7xs5rvMRGfeYQjKoMX5/Q
fgHVMTdGJquFtfYREh71PpL8g5YtcjV2dbTCxkuGZtl7lohAlQLxPfMOphCdUE16u6hrul8NYPp/
L4e/VkW47S8Ks9wpCnzMKEg+lMeY6LvuCgZO41PxjYEtu8dXvlq7yCl/SAZY8qiR5mvTfVRbQtav
7QLYKDD6hQplz0uFphXQYtqwSom67BCSc0uengQNApoj7b4iALEU7cEQbq5f3TMZSephLAZ2m0fJ
Or+N/3AH8Pircq67TeUjyINnE6XRsZcB0+yanMaFTqi+Uysxsi2LN9/yLJYtDele/sh08NNKk9ss
bdNA/fmKEOs6xlVZue/CiA2Kb1kGBDs+WX+Z1PK33cN3dZUq9VX4IIcvixE/N7AW7qPXomGSeUiv
9PbMb8ZF1M6zGN7gOMg8DqA7S0ZwNx9ksLn1DSIiLas+Re0lvyc3VeE/bVdkuqAy/HalyDy5IPZq
hXEruAxb66nVCyiJwE3LYYBbw8al5G1lxxqjJbpP2/hl22dF1ALdBxlTsJCFMWK1f0JNAcdWZ2W7
gOfR0fDEpElB/bXBuZQrHWNBAsqIk3omgP7vd9lRzJ5nsFrcEXWcql0a+QW+81q/+fYnfhY3kdce
YL49gM3XGPhczwcHZ5XuK0BWw6LdahM94/dY7B/AAlzjliajCudiAZ84HeH7pg+VYdJJwMrZtOqI
sDUFVEfNKUPtnb3FVOlciM5X3kfP8eh2u05rda4ClcWV7O6jk4gKcddgL/51AEMS5/GUFOz8dRJk
gOyjT20cH6NT2gGjKSLoJ0ku5Ybxh+MDmIsniI7Gt40lAuYtq+qVVrhvlezseoPW6ccs4khg6vLm
upWMmVl2NEOyUwdUbDz53QnNb3ag/NiRvr1ou/ix6PIGWpv/nB9AQd5K1AduZ8QwY/m1JPWtJABB
qVZP8bz+vXu6Pf8i9EucrhPVhHmED6xhdrGH/JFkOzwr9OKhqqVMh/XO/M/3mjGJjFBPuTudWFQ4
ztyvKSBs2VJgolr4+au8N3t4Bm7TmBuYWClZ4CrFUmtt++GbCp1ezscJbtBsPikm5SpqMEghSkrA
ZqkVEKA/hcbc+8MnBIPMow5sQNyw4AanAvySoZyuvqEL2PZ3yzWSdYQK6dlgtcLP0iNqNmzKHeJB
2vw/152j5nWjsptHgRElk9jz7EtngLAE3pRdrgs8MqXpBkBxIJPUDUAbuleTTlbJI1XaqcNAmfPF
fW4w/mjbA3/RjY4KpOs4rLjEX1i3qfQlO0ZFZ/k0XXPlgNJAQwlOWn8fAM03JVe6DsJgsiPP4ge6
i0zNlvnCYabotkUdCzgFvRsXA8XzlAM5BFfvzZmL3kqHiSdgwTRZqQfNEimfZAfJR4fIQ0i/xK4c
L5S9bi2NCe8Kdr22OD4174IIUCCQ0tCd/DYSPvBkbeXk7k9ncSginzkXwY3uAmqbtVXVpU9aqu9s
FvItG5u2hu+SKKut7kj3oHU4zIRUjMj4yrzGWHa4SDa9Wqw1M94rCPV7tR3a7xlLDoLt2b0JVOXV
jx7NG9KU1DVmloGbkZUCRUmF7uAe6xDrCMPDQ3sn0bvk4lSjqxFZOCwmLU1qMbyzM8Xje0WRNrO3
Yc0d7pS4JYRMNsctE3ECQj4hmEWZcJA9Dhn+obcWErlBjhZOab70QlPXjnhP0H6k+zRzu6lf/gzU
ZK4qCK4eBk0odyx1BkNMjOhxzeOMe+8VdBmECFV/RRAATIGblsbbp/8qoIU1wN1XeQbnZ+HLLSpr
Bzgnu4lnhUqM/pO7daLfwOu4oAQ+jcF1GK97rC0Kv+mUtKz/0fwE2RGbANwyryL5j5fauwGDaEW/
UjCHp/ukke0qajG4b5xyLhq1KgwisbgXTbTTqNCMe35xIEcDMYNOedkkN4B9maedHL31n+W9q52A
GO5nLUnPP8+B+ec5OX81156CQWAccwcyadHwdXboj+PU+mq/5UZmWxBzutEmKu8DhLWlR8wPwq2A
kbnQpKZVX9Va0XbNPzWpZGAcHUuMTJhO5//h04v66yFD56UbFeU8nwHvwDy+ghn7HOQTYG3JFSIB
vzc/+QnNFBx1NsmI+6LKfsD1GwAiFbCfZpXDp/3BqnZ344W9LyGCJw5hChyJyhxwbpTkei5tvrac
O1biSc5xSivlvXI9kKj4Jc1FlKepptJDwLKcXlXVCnpZk0oJn0X0i76yhvBltGEkOHH8jJMiNIqc
gQBaHtp/inozUwk/LbkjKwihZ0R1CUBa/tnvWXDaUKgqS/67yjKFpcP+F0DGyrvHV5RBBvmbQN6C
1TR7ouedCQg6eCRK7ZXCPD/ZOjNuZAAIPxzPut2C/lcKSz8Z0o+QsA2LMAYxlhNelvaQGYtuTt45
u8Yj1wQU2VwRkKgFLo8iW6m8ypc5wwbbg1mKdHNvtjqxchLfIxxpDu4tB0ogK2B/mpcmyhWndY6D
7VcdkMbYN9nQ/aGb+Cj/O8sS7QWa+BZr/S21WuwAoZeG7biCXevD1jw5rc1BbGQPZhLruoL60t/K
N8UtP2QGDr/Otzqip0e5sBXLQ8KhryPLftC5fCUx3kReuGczEqj96L+7Jj1QmtInQ4jrcPk0CEeg
WicGodMak0/FfhuTRPLnSjLgz1PN6cJkB155DEdpDyOfyw4/+c7AR7NWBNMZysn7+AtBfZ8tYFwu
RDKDVbG++2HR3vgr9CpX9/DmOwpdfvlodvGSD1ePqIxObio8ClwdbIh1jhKCuxI5EdtgspajGgnu
D8Mc+fSEMcgYfjGYHyeag7XiEq/ii6oXclKglu885oX92UhJ5QvnvGTyogb468Y7rdJq4BGgUa24
AYHlsCjPwqzWxucIGtmPoBnFSK6LGKI4EtJqQRb58hSK2hDp9PYT4cGwwvkQJdM0CShs+z2er9Bj
3gugiIW1kQcVr3xNSMbpMwJcUdj8XVlr3QFSSWB/50Md5bVvPCtb8PSbMMRtqZI6M4n5oEmgo4ED
B/ewQ1L5yBwxnP3QHHaXEX/1mqVWtmjqxjgF4phDf3QBNTX9cszGs2KJ2Z/EDvpqHUj7AgyOCq/F
DJd/i+ZyeH0blHYaQK9NKsVFaIgNQGMo4QYzGvp1/YqGskL4vv9W3DQYGp1pNqRMCESdRGutgwCf
eyFvLQJ/dHz5YqTEWW8sB1hatVzvSRQUrpovBmvxdkaOzG9LjOPdeuYYjCVKOum0ldyhyELgkPOH
s6wd18DJ3HU2zNtmOruX6ev5xmj9E/uvRu/XBJt+tQfPcO4tzH/lqaS/PNssu2uiSwyGb+nz2cv9
pNA1WApePb+ZgqcjZ7H4pqSEy2ZO6eosE6nMYmTOTcmvLZ2LIQ8PzVE3K3HMlejRmNGe5Cg0v+fu
QHt1eeBo31bbiIRVi1I2AffOwkuZKDEHkF3SqSLNQJfjiqXOiksSENVmzVJPtI9Hlps4nY+6Th8q
fQl+/D1XnsmhJA9pJDX6I5u3GUllKnF5eidpbTzsDOr9mYBLfDS4Ig72a/Wvi++l7j7wukY5jXc1
Ap2IcyHlnRq6hZIs6tUowPr6DBcmsKd1im/iVgXGXv6Hbe1sk8+RD5eZ/qsAz/+GzEyVeYohPGy0
8OLfjgjflX1pwcgCmwUv4eT4pqSfr/0v6jGNMDr9eYbMvCVlF1E3VDZh8ceMduepuGani/40u9bM
Hu22d5KElBW+MeZ8pK6rraH3R/nY+fB7+i4fkSlfw4fCPX3q/f6QzN5Ym59Nq6gOmJHuQ50eJUNZ
mCu5ju3iOwox24PQ+3+bxonYC4jgT8ISywx7fjfVZdk5DyyoLfoc915bl8sH5DLIvwJVdLznx6xH
BvxywJs/QJOMY3saADTS3j/X7DptwOY1cjn+6KPP8cLQMZsilW+1GC/EJ0Twijx1Cl9TZh6lFXBL
Ql3i5dWu1QwGJKSm8Nb2YOHIvmhpjU/hfmRXopQODBYraEjgdyQgsgHYoOM7PJFEfHvrnznrz9Gm
EP8yKnVdCP/In7gQsKJi0zrplIEh3TXA6sW7iy5H6tFYkqq/SYWmweHSyJyMOT+gSf+q85j5eazd
AGpDVbNsWpw3+y/GNSaQra1UHaTPxjNT2OnvZw1JFOi8cWgvz8b45IAVbmr011KF3zIS2aWAwzJL
cBt/0hSMvaZBTSU5Rq3OjVYM4PGhjMyeAPoqyeqk5BixrYN9zRQvCknKhGwjVV7FCJMXsFE0Kju4
bICmCpLItP14/VX8oHB4CCkqKURJ9dtL6u0M8oqSkNFzln+CANEswXBjK+OeprpXJihxMNpH6YQV
bgMiD5dBbmkgMZLTj74HYTRSdbbLsWKIn5/6kkf2OQYLZj+nzOEv394OeDr/1SixC9uT9T9lWoZq
05jPxtebBzPYUg7OD5mZ8uORl2woodhgoprnSvlrazUV4K0w4VCTFrNK+kDdaJQEl5/QkAu0Huvk
3tOzBg/k1bDfBAtxnCFniFo4Bjuj3nMCxnl/SJMVEYEGaoiGbL57cget/zSmUxrFBw2cuvX7wcF3
xAN1qDLbJAkkXbxCAcy82PSge9xXyBIe3kXoB8UbnpAz00/BIZSdEhJysFUebl4Ps3cF88Viszs/
nBiZxoLp/HbeE5C4Pm6uW6JpPIGN89lGxsNVEOJ156jOxLRUIcTtB3/Uq92HUEOcEiVladsBu7BE
USQMTSbSHr2mfZhsEiyKD4XxtrdvD4EbXX3Iuoz3F8SgqOudpTB03sgTzu99OKts94HDaoh76iRF
O8bqg6rMLV5fThklbEjseRavke1k2ogN0w9Ko8Rzk/srVX/aVACUHB+fT/T1ylUsehSe+QuA1+TL
3IMCfO+7g5xdR9HhB+1CMx2r2CJf9gWdU2Q3rursmlXlDy23B4/xqhRbyq0mXz5gGisE5uOnBAKZ
GzEKXFrMti+sgnOwLSilaUofMNe8D9RgobMLSmTctizNhU4XTMwDgEPcpcZDZDse37fipCDYAQFy
QqdoDFGptW6MzeHL4rWaWcC15/bAGc4w5O5VWtKO9nv5LkEKlKL36tZokNVdWJLhZfD04ua7Psne
PDQr1hkLX+FR+U0ScFrEmnNyQi4oE72/S5cMvnD+RaE/r9WkTLdtFvHGQyxCQqhvU39lzlO46Ksf
nDzzyQppMUarlGXNFlsIB2CkAAaPK/GXrQda4rZn7aC/2yHn1Y4SK3NS6rl56V6YmSY+r2wiR5yz
Y8JHJt8Fk47ADet15i0mBUI+jyy9KK0g/PTtE4PHlI9gFVieIEB+jZ7chK3Z1ej+L50njQ0yip5X
S05y3onyqoteFlYu5aaWgE4rV99vJ2rcHhgh3AMxb7y6hk55GSTHNuHnQYs3nm5zkirv3YbAVJ+i
VvMAsHGXS9x5EPs+gwqXvNqeBt3oaj184YmwAtaDSphrttpr3kwEAtoTQHFMg186/oM+mzl73g4U
HUXVo6550nX+cxXi3LJjX5MqqEi9jI54UI4da5hB0g/CEkTdWCAK6QN2BmGy1GZVe9ehNiAxXd3l
nDuxjRDhnV9x9+iiA4hkWU4N3IV/oLQ698nfDn1kjGvPqtZv79FsrEjkNURoIqBSYu43q1PbXILf
YuZJ6EN7csTGdrF2ilQxh7xnMnZncJ0hkoRjfDbSwOig5/nAxGMF+JL1X4Oh+5qmH7A301zVVZQl
CfU2y/9azny8GhE1ROhbtIMKOMIWeznss35KQ4A5bJR0LslIlSY5qUqHksCIMCt3zoEj9mvqbMNh
21G2f1rLImhoHjjNynOh4xhgW3uZFFpd4T80U93wUulP8WAZlI2jqHaOX9jh/D8/pBzimCTkh7ZP
BQQjl8KiqYxczVPYcTogkzx9413JckZiEOdhrPvo5usIetnvr+BOvxRFWi/MgAa0tSfvldtiCUel
tCB2VJL9AIVCTjnqU1bpPOEmujdPRrEZBVFRsuXorYjVzCeWdGWlS43gEpASozItTYK6rBG/oujp
GiXnGM4w8zvKWJ05qiIKHnTsW45c0NjHHV6FVoBKpvwwzQrd5zBuKdYXSbXMTO4GLXOpCNF7L56D
212OFQZgWVa8/AQ1sZFDhoIOQNPSsG/UK6D6hf2AXDxkp6ktD9sKqc1DXLBMY9At/8Wq4PqacnIm
2UJ4FVQmw7R5HGmX8ZZucc2y7vEJ+cTEx6ShUruB7f+kyUp0FofJLKEjCcLepuAOKVqrIyT+6t6g
gMBnvhVffEJT3Gp4C6R0BVJm4eR/pzVsvFoSppbMfsIbTBoQYSpr335iZpG4i02hRzAyL1neU4kx
vMEtPMlHBqMyl17vddn/PJ4sPGHtJJ6L2g7zcj1oTcZ7+Ag5zdqkxnwpZY+sUaesIAq+qydXWqQT
3vEztxT9j4dTm6cHf9lmk6xGuyxdhVtKWnj0ol61nT988vH9yLbOFytMQ6EXToX/XxUZ5gOS7MV4
lYse1qKuvQgsnz2DVwTR0iafNh3PtaW5hUzI+iFIP8Zqoi0/xV42ADwxXdFNybTliwLzMnnv/55j
V6oCeg8WRDT2yg6i/vkC+w7SzjaHX62/WSgtCXQcay3GaXYC0rKkpjudSeYhbnpTkBUn1rxugEK2
vc1dF9iQfkC9VLH8BPJAumSqD1CECjieNQSgobDspeo7Fn5+8oYAwQZ8OwBKplhRmmj/85kg6TOK
8g7vBNNqmJsObccjBJZh3bQoyVAHRgfqfGmLNLiMtwnYN1H5BpquA9sWRITOsB5N21W+GzTNq0MW
JW21hbFMM3bc1pt0WlfkQfyKh9WG0BH2NsvWVOYX47WcmsCstn0expAfq4qjFafhq7SY3xfQefb0
R0QtpfmJopsxeGniwMujPqKXc2Gn2bOL/kt1jbMGnjaJSSMs4Gpue2ujZWfdfMCGmgmC3sd9zUob
TrTj9vgWvCyjF99wfvF5e+AoXPR0kVwiR/C/lG2LLiFqXHvrG+n1JPQwjV2oBsMJzW33Anb/219p
HF/P0zcOTNYUyLPXNfJYrqTOwSuQWGYOEtqwuwvROp2/0b5/R5G4JtDPLrce4iIQGKcpfRbz2czc
/Kk4DSavqDLQs4XPnU7CvWNBRLEmfH2pSgFBw2/azgp4yBugppTf3BkNJy74ivYCCtQ1hOXYGAPV
9QrG9RNRmXIYXkC5Qg0u6zvcVkbgoJJ6Jy3I98f8/LyVz5PxuuewaXV4u11akT7G1usVSMyiulRs
B+sVeIEj6HASstziVbKGODMIqUm1eIRi24RT3dhI6bauZ6zIZW3AFlLhzCLLi8W9BntfEVhpbMFM
v417Iks11IYrvZjg1nmN9NoZ96lGSKxpZaFmeaKv8GXT1n7e+X/qw8wPyT7gDPEJiAqhOOoWYd4r
u3IQm+JZ5BAgjp98Q0hgF6StwRkHUFhc+AImB+Ab4li3QpxOoAcMuSAEs7shm9QtAGg0W25kG8x8
XyctVy/+dYHJw66jW5wJSJ+kSnSlxIId2GZiaTSUK0lcgK5aALhYUILhk7bPz1YWEFBMUk3uw+AI
SaCAaJenzSJ5cDi3FcaNd52ScY/O9DU/KXaWszyZxhBOlJwKzcJdM2IFmX0aBvlio3lPyTGMx/7k
MfTICibTBiXke7rrXShe94vc8YUASsRgbDh9OXc4JxLID4JU25IN9DEec+OjfvEeRDXrdbk62MVT
CbhR/n7d2uBRt/OAj1zFcANvu2V7ER2kIZ5akG8l1EO8L/LFKlqQTaBMA6hjr26XUJSqcfZM9uI6
5VTLmsXRRW582l9Ni2TYwJ0jNs9AJBtYL3hoonL/T/9xbYsh8F32c4CRuh1S814LHAf08Nw2K2ss
bZPkVbviKxsBHzsW3zzQsGjld9FaGhBwNfTF1Mut4Avu3c2yCj9iQRlH4+5xq2d/19RPt+xOKApP
wRSNK8xJGkF9DUaUTc9/NTLYezNzyYCjGMdXQOU94Ol8pCcPoYbl68t1mohQACk6iuUIxyLxrjec
69gI45464iPxAtwp94s7ICBTF7Isw76+/zVCeenDYWGZhrsP1wLjklFeGK2mXsPMVDy99+D/Ya6o
jbbxXleOHVCnh6eketnsQzKwrSrJ2SWPKJlfsA0CsWpUKSo2s6md7K3PV3D1tS+1Fv2xqQtEbS6K
wgeSzL0fqUF1iwjLFtOEY4HUlhop8Fp/aMQ9OqnWKzDV9sWqXr9xp2jBS1cg0WY1LQHmTZ/Z/k0O
vIUqR720urP2UgnzNibWEFj7868i/uPJ5vEjXKj2tU0iX85uYJHsAMAHbKG13hL9BaH7BMftswEQ
kdRniI4VH8aNKFI0LHaHyfl4DrclN8tVH1H+KPr0THCy1GZ5PZlW7HqUJOLU2DV9uo6X7P8C7+JE
sLDuZ0Cs8Xuh5IRyEXq7ICKjz9mdZuEa8FXDwFQmeWE7EhPAXTuCCtAS4COCC6hrGOdB6xE5TnTf
Q7Qc/fV5e2wAzuwjWZQD5Nv6kbiRThYBNnHyDsJPDFY3doyHdatV61KZrjxr594TMvSETqkxrhh1
K7U3KN8/RlL3Fkjm+X5xMQVly6QhtY+msfce++qcFYCfMMoyVHHnWWqf2CuMMbVE9K/tfhkuGZCt
bYHoFDgmdjft5/DiZHwl1qX6Y1G1673IwtW4ZDhrxxpoAZfOUH1LPuGbfBYtVSoUZiQrdXp/Y3ng
qXvz3vaz4TEQsoqjnF9XbeRFCEnATWVecCHK2MxNvabX+TtnMDYwkVUk1zfHiJBRPgZjJAevv1dT
+0WIAKQrN8jP+o8zV03rtQTfrlSohk4XWkzUdjMcPo7A444Ul5B7H6w/O0K1H2HVxqfgk6IVeJ0z
pA7kyrp+c1ZcK7Zmm/du6GOT+vg3JRUuYvCVXb6ECJy7Fa1ad9cIansIOOuQ5o4cbMqWKhHJL1TS
oaUc0Fxkb0AfXFmHxBLZUZ4k+apHZvxTAGvCxjYbksg+I1O+aWw5wRr35EBim3d/tH8qikFCn9Kb
HzLKQpnfbsx36J1BzzB6IModn5cIF1gMwpmQ5DRwrHW9sR6Ztc31H0RLkP0m+EPFBqZlUwGx0flk
Yy/w01BbxREzLdz+xUvP50zU0dhaOiKM4XzQgDCqc2PiFKonJnF3gQ/QKraKeBCw3k+mqZqUkKr/
BS3xYaMt/6FkyIMo8q4kssXX3jMUSJVfggsInMJfxBM10lM7OqEjJUVpdUECkVw53YM2bttUIrmK
vff9YXGMmQ5lXdK1e8actFu6GGa6M/WaVVqO9rr1thGno9MNj+mxAtZMbMXKC5kklmYNAmEYiK5X
DWaGzjUdfq8jlrkzlMjVAUQFCLOe6n311bzFgWS9NOjl9AaACmfjug5tn82ip4Q8/Hq/bwfsGOBB
zIoE72sOB8ASOtn8++ewG6jdf/g80aeeTUkW9vOnI9YEb2S/LADMsKP0HjsxSDisL0g9BrvOS4Hz
u7J4Ba6HatTssEW0GdykCOAnAeNITEHw7h6b7o+YliXImBCXbPExQwCvpkquTsMpPqHflpwgccjC
qgzEW1Mq9Fn6oCuXOphq4UM9SV2BUJweZcI0FPJz7DfGtmQEiEg6SzMmwNyFMZZhdYnfCVgwVXtO
1eAWN2VMyJrKG92gzujX6mHoq2XKuD01W5V/Jz2lVpT881lsRsVyp/d4KY+n3Lvt40Zr3I+x3pj1
kICZeH2bFTBCQl/uIA1u9yIhVDMVlP4qKo1jaThdNa0OEjdzgkP86XQPqIKsJaa4366NAxHx8xt0
mrhxbVtYpZXMk4R24iBYg1biIA+NQjG1hXmO3xcmDdWAt/dP/p90iwD9IbbXoLTtVRkJrYAzyZgU
XgYf7A8ZEI8CD3iUc7OAsoabOu6buM40MHuvCwk2d9O45A1iw9msMb3FxT+aQ2O+pzmWNpyA5E2Y
zt5JQ0ZfHFW7O3rtbhF7/0mzHRSoOzZ5kPcp1bevt8nw1x1tAYeH9Gc+/8icQeU2c79XhAn4HrIn
R0gV1EtAEuL+VZC/kNLgSb63F+FI7Bd1wb5BoSRwzkvh1KKb6WMO9CUBtnjkSXlIED961IbkbK2r
3feYjJyoWmkyKQvbNvU+FBz4Jfa8iwCqtaZIvZmnR1q5WF1ZfKYc1KTe0ykEID1ubTqRs8tHybz/
CKn3zkrl1/WmYGPNeGLSPHL+Cys6a4w1KzsLFdaCcBNwmMMyZnxDXpoIY3/75J/n1f3+iFwmkrLm
MQOI7zePPR1Ja6hkWOlovkroZRNc9fXKc1mK/bmANnkcMg5OZUXWHTNu21M4as9L6uRJoHrdS0wS
YqMf5sKZmEvaCvYiJ9yCaIEvKQenXVR0piYVjAI9FSoUU+ZF+zzDphI753b0ZPC9ke0CmydUq2nf
V7njU2EZmFYRyZ0R7l4wAN0o5+50ImeOBge84AaiLTJYt5shLtZXds1MLOEdKcY0IfKqFlIx4UdN
+60V5i/B8MKpWB/h4SG1yFYMBT2UzXNwolk1TMHpZe2jRxxwOWxgpK9h7lLkb0Jp8jZ4FnVujNJo
IeCjNHhI2NlutQQpdvIixV4ufQu1ZZmIq7Ns747K/RDVDlhC9xFj5otA7NFrUgMgc+4TEdJVRQdJ
0WYkrDDnXT7boeQo/6XUMIE0BTHq2xzB3kvZ2lpHGtu1sdVIW5MAKxjOCYH+aY1TC2cHCIzns8Ch
t+IoKH1g8s1hjc0YqG0T7uYaJB0nlhqASJaId/M/LzRlmZadkklQjMx1PX2iVxmEhBDyIy0PszYp
TR1vSVxBUj3z/7zgfVRR8rT9+ZhqR29ChMHdYdTAyLCEMNFbmX15VZKSCkd2R4LqMiC8JKgxKTRn
KwiL48UdafgL0UCUQo8IKFWz3sfLvb022bQ7BfY8WT/gfO8d8mxG4B2+mqvUP0BHzhv3lI0hWz0Y
kPTGRwuFmYNXrPcLZQL9XCi3ubyu0DCax2y4LI+W3ilKSX5RVK9+J9OfiT9Cmr8qw/Doa4rW88lv
EldCxDps5xwEOlYx7J4Y2aImk7S69sMOJToV96tE46ZfpBHOd5mzJFpADYj46n+5LGUOGJEsRNEA
ZXq79AE1N1GYCcWl3GYiishIEz9t1v3LT0UcJ8AERyMREumltEr3W92ONFLSYxFIv5yhJNNbnyj/
Oo+bX3h4/T8NqZe1YRYAzyDu3X465hR85unjIHsoDEIcuSp9Wk94PReR4IeCBP0Q9WWf/ImFL8lO
buY+4coti5NwpAhH6A1uNxBSqFvWL1Cahs6HNGZdkXMucPOelnGCjQ2gGrgza9reWpQtdOkB41xq
Ka+0NiSjXZbCx4JR1oFPVRE2dOoEO1dHVdDcCJ6VsSxC7YFpgx8wuzWxuMTY+WLm2Rw5BSpUTHF5
wB44yLrRvqVRy3mW0j5Gf5NYLru/YccHzxeGPMU2jMZZMrHoBMh1b93KIk5zdCNJZkEIRHnw3u9I
YsRnPl5wMdY+w7YHSDPwl/X0HNAolnyUnHUXLKALF44YqpaNEeWxCqGSxPWg/jFQXZWvQQsTTGna
ft+D3YykqWxxJGTWb+PZj/LClMQUO+3VkxqvaZ1rcjaJiJwAWh/X4dJVrhTgX3pPSATcSUUqaJMa
xEvW480BqfCXE/svWyOGj8Tn/OGMqmgNhzgW3T7SlkpbTrX+v5F3A7FwSMyAwzcclKmyUiFU6zca
g+i7QDy3feMov5f5OysS6UA7AbsI/KzQ7epsWDZdrDGIbicmkTaSlyGCd2WSiKV6/tVp45svNp3Z
dctR4MGzr46AsCHWOIUpLitJ28dquf66h7HxJ/mGTpZHlhdP9+ChttDpsMGE5D+VVFmiyhQwh3sC
4ZT+ZGr4bA7EkVXiWfZqDnUZqoUYhQibYTtfsCYRhgqsEwyaPBho0zsnkAGvlfjMj9e5WmDmExfm
R3xURR/4HoG9iAUnvQOuPxAboJfnCxQ5FS2NOmTFTedbdzvWRkg5SA71qf+gGDsUJcGS1oEMiEaa
opv7Sk2PdUjObweHQGOlPt44RE9JMliDoULNCuK7Jj+jZWKwV/3qGN3tN91PLhFt5+gZgObC4bVY
LPp3LJwvlzyBLSQVZvQKfwe54x4SBAU9t9oETsnFJFkz0335kQtmrv7Tn8Mhlqmhvq7FNSLJcf0L
MQbhRa4h7TomaJonfWSUFsRWY2bRrW/wEEBXoVR8F/J6tv/9Y68fVl6VbENgiuu8cOXBgMgd9AF/
d+YfUQmEihQLxGinID9FKsrpN0JiyLf/cCwmtooTyYF5dl8qFC2A8Rcz50XhaCwtLvP9MiYnAdae
KTmVh0ee/aLtaOR5aMLwu8hReIEKjS2BxmofFO2qcZqiehD+uQknQ28Tb06n4UQwOLH0vDR5YWc1
bmtIoh+phKMmX1pJefIGoktv5wlfDUFadQR3/HwiJXARZNQ98HCtNfTjKK2KwlLGWvsN7DwIiooE
Gn4815BbwfJkUEVcLjnAu5e3TwiGvN191wbLVIBXoHTMZVil170rllv8pZ2aUpqOFN8cKvbdd+8o
qB7bdrrRcHTseajNrMroRV0EfXlZsyeQ/yhIseclMRvnOVPc6qOf/Mg1CD0mZokEm0PBc+RSQfHa
aQ43tKX4AKCt9VIvgoHwQJSlmYV6FI2Y2DZPrtnPpZPCJsV4FwwB6jaiAfkn/tcoi48ur5Tu+UjC
lMxnONuuijsTrIlaO/pCoIC9me3VhQwjmRlWRdD4YTGXuksKakr1j0xrdcj/zdyVyCZDxw1bHs7A
MxhBwOE6SApO4xcRhvRxRXfaJvGezVSLEhWhKj+Z3Yf7Ky4TEm0UzABcvcasCT5mYzkG92yn5Mjy
OeM01Ts0qzZ/Vdq8T7Demq7VmDYngoRQWz9pGeQenro64pPIeKnc6idVPJLUVOiKTD6+u2SCNiTk
UW6OXIX284nFeZNNWtOq8WjPLRTsncCMCYwUve25ubLK6wGdGgY2bW2jOq0iiEBdj9YX40dNaUJo
W+2bCdsqURPC4/DMQCJl5creytqTz1PsqW4G6ZssW8XscfX6CpN4sSgLKcrVoDWHYefRbsrXEagk
2nFhsNoqKLVmu9y/lcJqMGdoeC0KyPn/5iiquBQriZBkvfvVaYxYPMBOGBxfVjALfaY9YQjkcmvW
fJ1g1eQkknBp8MWXT/QWWHndYAXdzHkiA/t19z+GfAjDR875a1r75APn4kAomhyjOUHEhRrU5CLS
UcsKna8/SsH3kLt0Hopv2w5+pVBadNaVaDKYN3+Fl7lGeiPtImJbv37Q+oo7gDHWOvR3EpeNyDD0
ZDaz5e/Ljt6a33b3N3YWf1kQBwlbMDbJsz175SJnMVsA+bjEiHrL1ahuWhdhdXZFeMueiXoOrwPP
LUMHZ1phjmqYsqVX8Jrhin6fdBIc/dJKOwbkuv1PM8RiFbV7zcbCiMJwtyDtrTxzmczyv2/TXCHh
hvuBOMmT104f+/Lqk6QixMse3JDGXsHBwQAvdPSaPUp63yDYSiEHa/k+F9re5SvOiRMEMKvoVfGk
7B2air3YjIjJMU6hfTNOK+nNnyA0x7vUYK8Jmg1/PNiKEncRHCIe3oJzAioXzXQ4z4c5RCDAjuyb
JMw4DfRZhWXukYByza4XolP1M2Lwf8fy6/DiKVXSZk7qOPgTOQbqV/+jitwGpRzggUU22I8JGs+g
Fw03IqBHl0BuiPE1iorO6VN6RvvWvj/vS+ORrki4z6mXiiuXPY05s99UJcqWoMivUdMwjTHc1BRG
7QO94IbyGMOqEWowbpQ72WEx9Bm3+5sVYrdqxCOLfTASNUatb2gBfwqAFrK0yGcZpFYLNuLUnD5T
xFGX/TgyVcBtU9duuLU1BeLUkEnSbZOpsMHnOGKhBELOLK/5EF/E1traYX2EX61B3UOCiemW+Avz
VtbU8N+YMWV0kfOLsLeQ6OjdDqP9pRLGiSMnm32lBg+z6Xz38AS+W+7uP5Ar1XgJ4pjbb/gmZuMP
SafAuxKlDn1lwwm4GMyShDlveZBHThVOVLeOuOy+VyJGoHBBwDbvDC1BmxQBlGMjzzVjrOVg7dQf
d8MSlw3w0UcpfXmqi5gw0Ty6rkCilIDPRzQVK/nOLuhQJ5ih7BTEeh8FlXbWnNIUcH5yQvc/wdtX
xQ4nzZ7/f4vBwdYm+qKjsGcim/bXYTqBes+wbXl3hC1eVb03TGleVUqr158ljnoVDZE7gsmKKCk+
J9xPEZcDXmtHBEUBeBFVuAULNnGFTH9ipb3cRAn0qRcP+KRpEN83IdMfprPOgJIMleeAzB/764gQ
S+VGlhzLDoplSJKiGhknYnm2F8+10kTDAZJI/8dThGFkFcBceikWN7V2ELpDfADUTB5NQSA3o+h7
hJpqWWsdiB3PnzSqPPYEKEL7Ol/BNeZoHaCr/eksnRashGvhTj4Px7SE5AQL6UHCCWIwlaId8cFC
Qruy+pMniFccv1k9DHaJ943n101kSnBbmrbM+fVfhKksttUL2FaPBTR8BzSmUoOPg4LU+Fi+z5Jf
sbbhMNaqiVuELH31V/0nh8BeFuBxkwxv6flewQVrWalMOt1j+ZJgLsNsbHSL9dtAxhlMCSHHB1p/
ANaTArJDlvuJvQ0j75ZXD75dBcHb2WVVwaIneaomJC3PE+D0mQHIXJIX8qTGTf3FK8iiD8ltOOWH
fYPkwrxFbshZ+Yd3Yv9e6U4zTKqKrPEEcQUoVbxl7sYPs8Cl57c1Yb3z1VAIhIavKRC9eH7BPeoO
Ye/SwoNW2cPAjkK/r0hfKKWiLHilBsKxmau3re2G1gR5tUDFj/pHh+4/W9QAMemN+ki+4WYYMvxg
wksoOudYPdADCqsougDNYD9PVgnwkn9tEnHAWFdXuzz7uc8ajjMAte6DIura3uEE1OKAW0C40+OF
rHS/qx7RZumMCBEHPkt9V9F6Wn2L3tvAcI+3n1kN8JBJ7+mlMu5QS5bw0oauRAl1jagnTlym93fp
U1dzYQOOhk1KSDqAHpgqHv62CD5G+d+M3yNJXJ2taJYJAhQ1LQP8MnRq8mb+QTWafTiE8GEGDAIe
cHGtZOlslSRWJodCVgHSPKcnlnWxg820bYijDvGnj7A3tVCQaHLKR2Wqo2QB+yzUFOrkUZeI0YSd
OAaYFHZhLVUzOwj5MasWoyJmkIco5ZoMWClG5YxQG1Gn+wYuItY/5PY9Qm9qWnjMdKhRz67lFFdM
hfVCls+zMMScHNK0g7ejcryHQM601juSUbhHB8ar40tnO3L66oL996Yg7gSdAt+h+82ExYGqNPj/
XxEYObFwqg6SXdFiTTN7hh6EYeVV8iGcOb51xD94451Bij3QHuTIeC5e2/eo6UtROdY1MuJa0Q+9
lQEgjap4iLURW49ein5BC1Xf7/z9vQuR/b/F7B1dX0AR5BE4sMs5XwXCZxBLMTarSQnLLhdLTDJ8
FK39/TytIcoTW+XRwuZ9mzqRjZHS+UY5DaFdn5lyGxsPHNZgo0HZZSVB7qd7mC0aaaGIP6LDe11b
TLfQJILfH2qGDBQZKRwPyDzTYpIGCoKtCc6b4Emlnyv21G+B1sHfDfCs5JTYzyIQ9/VwHmF6nrOw
t1x6x+dpgqYA6nDLGLO8TJVttK6lCr3oXBfdE+0Fnz6aESMQPZ6Kmp8pzQQyHKl5F0Ek+rOyIKVl
HEwCkSO52V+yPKSfIP5ai+/LECJsZJyRLz1JRLch1r4E7SwKWp4x4PQ6T7mleLiRnrUW2R16XAE7
M1YtbX1twtQa2GDDFKATcVCdKR7F0Oz+WLnv9eInApD9gfWgqhuop0g+8z0bBCTFoIinPMBquOmT
JpnobgAadDK1MgV5gy8ChE/NjlwRvVIv02eKgK5VVS7Wu+3xqT/HGsBMJJ8r+AowlTdFOl7WIbzU
7CBxeqAEboxcpaAnvo0HVurZPH6+F60S1bHNscm+9JmYtEx4ZzqiG8rK1bsgSyZBMVoyCcPhaa+c
khRK2YLhlEunXxJneFWasJ7uNWkAH3VKGU4wT4/FUb2KpGTDgw/vdYlv7Szxg8ZOQiG2VEifKm5+
9lgHFhZ0J2iXB58yCRB+XxYUNDeoPrBH7y9eVwOS1JH5jWI6WyQ9zlpr0bWoMktTxoF/v6McfK8x
euZo2wOpzrINxviArAfFF3xaETqgWpbU8NDvuWUqvvNENhxdcE4VWzhbesOtLPJjTRCRfbKtWDRZ
IHx6Ko0V2BW2VhDCUahFTKsOmQcHd7E1uIC5A1NntJzOKnhPVlcB7Ioug4gU6pBLOik3Pc7YUlHr
zVMwfPJ3uPbeMQJY65f1P2GIHMSeith1dR09JLH+9zuKd5RK408o4W2h4BlHWcX0Z/34X/A0W1JL
jY0Tvv9kQgNtAXTuuaPJkY3ynszrHXJ+kMCSy//SwBGN3/OWFGXF3Yv4IqW89+qPSSDRe5zAHWHC
J4wCY+BRfvz/lpqv+FaWy+CSuN1zkQBufj/un6xaXfU+3AbvTuUHxRHLlv9iyfcUz2pVoTTDAc6Y
FLNBNttb1U+be1N8SKQEQXfsjxSCttL4p4YKdeWqPVfQB57M2CMhLz5+72CfN4pYxR54cdWK+H4F
PZNcaVJ5Hj+I0mQlZQgR40NYyhEH7LlMJwI5XCDejnLMYhmM04hSbBXBSh+yzN9PKhjGPfhkH5ro
H4gtQzeZ3Zscf6gHab2EIFRlnlr+bECawWgpwpSijG2wtn3S+HriSi1/m1y/+2We9/eey4gj6v7D
nbY8wJP2dAiFALBUed16HpZmWDoL0eR68BdbeL0zSoFHY0Jg4z0x1k8OFWoqxeBjZtiPxvsHri4F
+n5YUHKEhuRLVXeFJyxuyG15qPtv43A3QYs4ErbyvLr6QpTpjJQvp/GtcRjBX6PLPXqNGNrlQ3iN
YFy9QpDOQlNVi4CsDyImeVeZZLSW0mgkfs1a0UNdOD0w9DzP/EhTOuBcQRUvFSksyhgY17acp/Ii
6M2eeLJRe2NHPfxqmCWFRMuux/C/RfXdTcR5SppKRFfyQMb3lYHjepGaMlx3mnI3dLWxwzvB8Dn4
ivyb6Q93D1JH7suAa2fr3p2kEjPeu9pSZWPVnPupp9KdVMqVAexQt1DW125ITA4K5IfG46ClTu+6
xW8eZNglaqNx8VJ9HP+BlOylywhVanRjA/9j8q/t44vOaybjGBuj8dqau6B6g/izc0AkKUKpX47Z
dpLeSVyVEdHs1bDXiFll6MvmS++f8wtOIzFSGIs3mBfgsA3uQixlZJ2fM7BI+E5Br4GR4nICgQdI
zqsUezBV7jVDsJrMVIVQGpYhbORGKRbhjaV9NDI99IJt788he4gED8aOrpMy5PzfO7L6XcF7gNcz
+C2xz1qqb/pcqg194umSaENWwEbXfa3uHe5n7dEFgw7JhvlZMuA6/9+DucooJlbgscP1nayNZR0a
c4bUG3swwfspIFVYNf/gxAjz0kGpkCs3baD7aEMUcm0oY6pA0+HiUS8lbEy3+s8iZp1V2r0wIngp
vsCrV/PVJERVcSv+OjahjDYS7RCByC18v296NcJpFvfcZw122Oii6HjxYDbgVBcy8JnDKyLtxEND
Wm2Aqxt9uXggIxujuM2dX4F/fwL9yapl6ZwVhauETFUDMBjKZ5cT4W9JUI4V9tseVjzarSj2bUue
hJlxQBOpGX6EZNG732QC3fvwK3gwTZymbe5tCTLgWSqsWAK0qNysoufhuXd1XM5cUXaqhBD4QtAM
3unsIQ6I7AqjvSofDgjqObA3OyZxbZRtNVkD5UCg9jGvNDo0gZKMd0UE4wyfYGRfoMbmDAZh8/M8
aFnaaxJbiAjmx4/3GWpohbVIvWzVMMM0nUyMYzwGgUqbLtjc+KEeBkGTuIaL2V6FtKBXgJBkiLA/
bshEkgdQ0sHLEXyg2Olnq189BhAro8xO/l+RL1g8eoAKW7FXOoHwIg4yHMW86RLrI/O1VaVB/Ia5
aaDRtlDQ8XvwcI7eZSphvzlioZdjmnJK2J3UKDE5sNTwgk6nHGD/8S6o6g8mvhdqxyAmX1G0uYTQ
q92Zrd4VJxfs4tsKpO/Re5PtKtGTf50Xkip2nxf3jfX3VkwwqpahPzx7aYzlrKwGqMVGJ3pgzEZQ
AxRZaViis2tQfALd22nJMypwSEh1bbYmPWEEDGqvPtBRU96fmae7vJba2YqqBfJTMFhdXPhg3Zx1
ZwWg1dTPXRfStne96IYpG5+Hma3TJ2RAAr/a8ARwEy/FwzN2Prll6XS3cu/e2xdkZZdhoB8yqsyz
1KFEXZHYLw2InX1qK2/OD5Ki1H9JldPQl/zjvdDHH8wahj+gkQjnQNFG0vF5SsGWuSh0jkOT5kGB
9M5J/b5DFFLKx6NvwkE3RxM+1zL164F9LJHTN7rZti+XOdWNOZNq7i4ArbTkQ1/3aiJCm1C/gr2d
4Nfr/fSZwct9rQyGQ55Q+kH4c0eMrkmf5TTgtoDIfjsEvSgxVZdru621ctur6jcdL0T2zh58vBdL
FoB6dMyInP0e1Avm2TN4ROADEfe2/ET93iQh65IxSaVVo7QsPz2xCSaT2YO4snv2ftfQ/1X3aLe5
NDgyHdKPTg+BzGhOxLo1JrSTKDM2uyrKoDLasMnQ/BTmHm9nJLeMjaR+Dqisw6eM/AdR9UoTTQt9
XeqEYiuDfZwBGm4LdTOQHMQEgCX09yjWxGLwI3yx6vJ7wfur8EbiJL5BVCnVuCMT+v0CWonAopPy
Gd87Bg17k8G2QqJmsSOtJoq2i0sVl5SgLZP1sElNwvITJMi2j48pDB3ymPUveIxkEnA1/zNNQvLM
Zz8V+J5H7yxorESy2c7k4xv/LFxEcJSXC05QOOCOaSeqwxkia1QO3o1X5iZqOXBXLRGBAZ9LaLip
XZZ4Uzq6p5YyiD+D7KcOg7P+pnIolrrGlwSbiyRPQALyI0ibTdNAQcivhnOk5nGpYgkeeZh9zQpU
sSx1IuBOahaZefHAxtPiOn85PLpuI9QuT20Ha7Vj9cBf+0LCU49TDRKm9+PRSKMbtC1BSiWnyIa6
Qlfzv7GVOy94cG/qaWoX91/SAfv6wsU4y3H9uvXVwFi6zI0QT/I18ipd+o/lMC59sxVC1Z9j/gCZ
27e6AUK3WR7nh2Jxs8rD7RbEscidugvXo0hmU0U1vPcraYoxr5lTzaDRpTPrqRHRzY3vfxPwgFnq
QFpKEraHpZMLgNQnz1XeF4IOT3c3BPzVikGYqf1ys55K57IPH5F/WitX4oDZmYkmSY4ql10/LHWi
9pVzx5nGSe+5lfNkXCwtLOleBgqsg0F/I3Y5ZwAYlTwUQ7mHJ7toKkMfnCK1kvdM17b3aMdKSOWM
uhuhHdE3M3MDI7eV28JaXG9jRH8HQ5LNhn4TauV+DS4RiYjPonz8pzUexzVmJRaHCkx/cdWWHVgB
yJIOuECmSihNO/Rum8tAI7SfoZ8Jputsz5xzyKr+VOqOr5jwdoQwBn8jBgkU5UR7iYI+CMbNCKXh
hkkji6BqZLOD47ILu+7UHJPr0cp43/afJzCqtXftmRMHfFEkNfvkJNS0eBgl4W9gMteCDqkucRLL
cSfWXcuXiMN7piuvWP6za1rAtQJ41ojHb81kqmC8nu/1qm13irqferpEJ015du/wlfAKTEbH/32H
p48LLuZiHa+1PZ6G9JzYilrtMsr1Kp21b+X9Ab+iYjozeMyYTDyiLm0ey2WG9DZGUnlJ+3sm8gyH
6rzphO29ETaXzw0USC+EUD+mQF3tDlL4/WbB98pAMqCoqhrnxtupWKD/sgmrQsPv+PWoQGS77iWl
8+/47l/omTyUheNYF8XoG/MYWJ9TZ+IdZBbM+fnP/KBvEXsrvmJLUW8K7QXUK+KI/zx8nPol0Y0Q
5GZ0WVsn+q5bfBfjVceQN+RVrsec8aK+7c0rrbhxFUoLOo7OJqcx/jFuBzhAYtc+1qDVwOYYpT7z
YaqjDqzIjGEuCECYyB7t5ZsJfgHoo4pL5RzA3KYBW/jV2uy8vOpeDW3lSthVoJoxwFq3i/vvUrhC
XQfqiDF13fStGH7i6q9LznZQ2O+nYkDo/VJ40ngFJat1yyKOdskl15cHUQcGHIew+gwsqzs7y96M
M49h5lMhPmaKAwQGtCMoI2QwE/EjJK5TOqz1PPkv0wczNSEV7oyHAi09a0zfmDkyJzlJhyYgRFfL
jgDeOpvnzpWiL9Ngq/CsX1/tsS5M7ndqvIoLWVz8BXTHYCZefjcUn9DHiVQLrsQhaAcbSrIX/xRD
hYJiRAgQriNgEYKDtFIzZUXP91q1SuDv+0Cb04J+D3aWt9MfHwUo8bTaJlM9hp5+8afS8mn+YQSE
SMyrTExOP4Z3bJ2pMhKabqasGJKF96/QY0F1jO+ftpeeBP15bIpzxhnvyO5szk0yUoQPfH/euUwm
6NLAdo6y9tKSyw+0NmqwTd7eSy2fXdgonbrUgCTV/Cejt9oa/TajaEEMqK6EZG99iRXCr5l2wVZC
H/RPpn4MABQwx6z62AJsLfFwa/m6FaIMWS4OvdBehbt4PnDPekFu0yGGkprzIUDKtBlVQGM3yj4g
UoQrDKXbZgFdKf+qlFXeO0kX+I+hpyjXi/VOPBfaYjBrnHm3/8ehZVj1ZO3cmsJQT+CM98nW7hiF
bLDwLnMNkrbJrxVL36ByeEV2CYuOhKHAXFpVOW9GG/DSnz4LgCwmGtMVmlyUzcPzwepHjxoH6I93
v2YI8qxDKl1ncqGFI6/UH0c3Z2nd8toWz2oTekkddV9+CB48SUJovOMYZZbIBLkhvkByLeJi4csb
nM6eZGKg+zog1O+lOcYH0NZ77MhXHvIlinTcThieRRp+Ha/U9RzomXqk+gkLtf7/rSIBbx2Otgti
4lcJq8c/9czn0/AEldfQYYe94Rmepc93Z7AuU/+7nnRXK/4u2JZjoJ2eJY2qewWukO0iIwAXsge3
Cct2DxIOAoHzopLMleLycvqlO8h0FVsDml0epNI2A1BqFUt3M3n71olw6CAQ0LUf6MEaJoW4ZOlN
YttNo8K9VJqonLuJ6tWP7Bf6dnT2LN04k3eWW20xvYlzrIByTuqctx/zrF4PIuGCoihxdIQxVai/
pDJQJJ4WlWmlEAX9N6Coh1/lOan3mAAavX7BjUB9ISxBY1OfyvgdmKJ7Gq1iPLWfwg79fBtZs9gC
iVhZCKepoI2bxDiu3NinC4u/RVYibKpHub2LaNHtopDJBUzpUNbtxBN9QGIye2O+w/8iaCH6cDnP
bQpvxbFUHvveQtBi9/gRg3uEVLz1ZOsh9CNqgh8r+tr4io/n/KfzNl6rbaQCW2jrL+YTRDn2K9HV
L1stfe3BVKiCG+JVRufQgyxBScnddAw6ozzqj0q1prBmpzBYOivzX0wkALhopdWKSZPoBp+CLi3q
dzjLM4B/m2CS9K8Iq9S4OPIgpP08TRKp8pF45YHJMFyANEE8WF6Zrueo2H0lE9mtX7lzf5dwPT9Y
k9zOr/30mN07GoguLlt91wWapedzLhZbbFPizcxoaKvMnzbRuxJZuouN5dXde9zrgjOoKlqo6XFd
boVAue7bHNoiarln7iQI6rEF5/l8BVZI0X9NWSCsVtV4VXUROlis0t01KJSStMawej4SyU4fg5nv
aWXdkGGntAco8jfH5VIoAj7N3j8EIPXiL1uusiy+5ilQGzYkcq/c6xhmORzP3f+U/R2uzoH+k4QQ
jPrcW5yDzPteNGx4VBwRBkwXXagGBRT+lfbFMkLMnps45Ia+p/EbEheyhj9TyL9MJ+2R9tGOQYou
z7k+BHeRxqWt2gOtZUvKqztk+mb966YxFNoLE6D1g0xnTOQtRBSOY5zuwLugLcnbr22eISKXEkzq
SI/El/U7jxNIbskRW+zoF7uQZC2FRfnuCduKirSzMjcvZHZ793W5qCrnQqc0FuQJQMRZKWfuWoWm
XU0QGHSlGO/6vFIAdhjHfwCs6Rl11B0ZOPX9yf0Y1gilaQetHJQSzMRP/FqeprWCo34UscQdyKDN
+eLufY69q2JDEQkr238rriTu0YaPJATMVu1MhHYacn1RO2QlYjVE4ZZv6QSlWANOOHPqDnI+VXwF
vlYEtwQ78zIep9eDGAgzH33VIi0cYu1GBEU4co9vREyMKRk0IGmwzjMENcmwb9whn4tEiVAnDFIa
6m/+vtbxKOdtfkKOa+0p0WTOWZao97UfSHgWxED0qFMb5p4BEHrrt7dcKLGF8pvZVUzS0CNPn9qH
xDxocrPl8ZXScKmEGezB0DzVjYrU8NksNzh7idfBTTivjH8tj9tMChhiTPInCQqOoH2fYci2kg+K
zUK97qw9wDU4vg7Cbex/anajUyzgAeR+wCqND3h77RYCNtDvQTBdC6VBC9VfeQapSdxb8RMDpdlw
39B439bW+DCRpbpsGrJfiC1whIcuBv24rv8+DMP1U4tudOe5sFDmFy8r+pf/nSS7S5P6faXyyBAj
KKdXDpD3w3jxzqd1kxrf6UbTXregvaBNfOhJ3cwEZ7I+cdk2Hhe7TLOqB3gOf2HTpNL1NTgFJ1q9
17gzEpVw5/ew5Yxgaw2GP+OfJ/vvF5CcnB+RFLp439w8NkyAUR+a+zZBe8Gk4JJBLpdkjvMulrM3
XHlJNLKcU8KkDWDewmU78B97Z5HQS6CCH0jEZTmw5g6xMlgUXIh09JLqFJw9cyFcPIOSRLhb70yK
RiIkFAmohhcsdCn0zr+vXHceOl7uKVXqNlyLMqnw5TSm3lthpZjyKXPNM/fgbV1nMN/qAOL2kwJz
2e1dTaChZ42SYMJLo02B6z5tSGOZxx8bUeFsGhQpcV1wGBHviyg76Nt73pm/VAmQcheiwdv48y39
6g3j3+JzRcY/L4e+sx1VuvI4bbg3MkkiLnu6rKvjpEcNIf1rNaWU968u8+yUaV6rx9zljZaEWff9
AeIlqK1gvCVqj17IJOxzzdqh2v6jIIf4Mc8ZIF0k6HYGDmPZ9I3RZYQpER1pU3UXATu/lQHGiEkj
uYge7kvQvt/hYpndTMSvqetoWd+BVPLP5DnQittxvG2af1ODP6f50h1CURh5KSlz2sVWYIDt+JHh
r7x1V8cmVDXBKGCRIlVMOoSkktCjQ5LKGmDbR8NePCoJ16k7TYyjKWTNOR0OlxZ3GrnRd/ID2NEA
BtY5zTOM6GwprJlyVDpmmKOiV84vUzxByG6vflNO6Okn6mq74LBeywrq/T8xP9TfvSc61aafIZHT
LDCqy+d34kMYOXINkxGnWXCsRGZ1uo5XmNUoaQNKzpFX/liEjOL4Gwn7CXg23gdPUe284LU1JLpp
Bq2W1CxGjWlV5PwUvS5U3pWmgM2o1sNLtV7ixA/hsWpTSd0aK4yP0FkjD8xQXXpGCXP0TjMNdUIr
TcfJQgsw9mrxdBYHETG+5GdrDySXzcxT/wohbjfofooeBb/E+GRj2h8lG/mpV0G1cqWX9phm8+pX
3UxIaEG2IUuowH3FbqrEvYMOaZrwlLOPSxtRp/ZhJZyw8s+MnynojXgA2SuZgnSCqX9Vv4O0lgFa
ivUKnjTVV3T6cyDDbPTlvGPrMZ+BlQsOIr/CdnnUEuMTEBrsFH+EI6xQzUA4NungS9HJNBg6R61b
yKUWoausH+8Mlyb8un8GWZBNSCRfpwJh1ACLNlqfpGmRh2sYrFNZHR+GfdI3BniBt2XzRSgJXlzC
v1t0n6FN8rPbB0KZEyoqKf/cmPPl0J9khUWExlaQKO5gSFuLV9ZPyxAoQVbCCgMF5OdrPwQw5tR4
iy0sa7LNH9q4rNrvWFzYiYytnLabl7jQAaKxEIiTjlDGnsxdmpuDJptWgjicgkEmbwN8mR32Dagh
J3XMc9I/N+IeAuQn4wZuNwRIKJ5Sv+D/qjjI9PADOk+/wPPWGlghrXAtURHUhjdAqZW7w435KtPB
DPsrnfKxnxooctHedFWnb0AtFmZwDsif7A5T0xsbpH1k2I6pOVXqeVROInJ1B1E/fzp0SRNxinfq
gg91FJjArU/cjpmDGj0Izkslj1tRR7Gn8aHdQEPXBXSyGb+gD3iqGd4mlWCZzLR0IqHmM+IKSibw
9UUam1GBNxSDgmqZdXyPiT8zXiWlBWTjgm1Ctn3JufiN6t8Q/2oXQ7/GUUHrg+aDSAtIapqIxG7T
wii9II8KTa2dMzAPzVawnCUm0lwAdlGFbVpttB6UANOI8dNKUz2gVyW2+A61aDpJcEZnMEvb8e4V
KtaeX8NOhCbkwqni8EKiOrJcHjuRXAcmEh7fxzMNP8aDNZT4B3IMtNVWPGeLjjssHgv3IFvF1UhA
J29Iqs3gK/ojCEJAtgiSY5jXMLPiUwo3SrqnbWF9xYy4VS6A8yvLuJ5kEM9iHitVEB0o0AwXMkSm
j9lIWX1qlY82/UkpbBNRmXr1BKPYNKnKd+66ln13f0XYHcV9JrGk5Dfo4jbg/a5AwEOtA0b2luSS
1dH4JnCjMd2qa1GMOa1kMIQM4sqQZmxncpEqF4BO5wG5MKbnpo57+3kfn5ic987HSOzh4ZeleUy6
Rim+GWK1URsVjoq3BOoH8sZihRymIKDjJyaBZCfpP0qQzcROZbrlrhOsoLPksgnQzxAHVwYVyM3i
IRwzDOipMSxsmlghkpcgiFQC0IhcaB1xkUdvEzCKJQCuBld7NKEiDBiIBQtH1DS76Epiof+TR68g
KcOHWxh5bEKmGKKnxgKmo5Eg6hALV4H995+ET9YWuv1DIxDn87omB58nQtzMtGph97C1gexVhHqC
FGsw58bKI65+RFRdmQYyv3Zlh5BSai+wUUSa3yfIFxqOzT/PZengXpVaRDiqTlzILLx3mDhpPAnA
A068klxoimNKeJsdwWJaGtf10c1nhvg0UmFzGNxy7MEzdR6ENwljDSJXXz23r7+CXWZ/2DWOCQBc
E3lQKp/Z4OTB8GaPwUHtPmRF6da8IFN6Tf33TCCWI2J+HEnZ9jEZHCOFPCd3h2r6B2RnJOQ2UwQm
Ug5V+IXItXGxYXbBTav+85nydgZZjC0xb6yCU5dXKs5DZRSj3I5p/M7ovo7MosoS3/SjP7XQnHVU
tCPOO9lQ8Qk7C7TQb/OUXRuKCVIwGXYHtNoc9WTkcJoGrQG7G5gCcKjJhS+gcZCAnQVgbGej3GKo
xD9Fr9ZiNY49hDCgClZ8bj8mfmymyvLw7uCwsFNqlAFpMdfB81VZzSspb05vK2WohHwWnTVmW7j1
7+FAGHFG69cJjFhjDx6sBmGVUCiQtf28HKQiu7dsko1hmxEuiCvpvaMnh4CEX7PO3eSL1f3TxlNi
RCY1aMlihCNycUdqyhpvVsJ3+wPQdT7j7eT0axkMOsTVLdltz3dN2d1Guiyt0MCPxFp4j6Kv6kkn
3W3AyCN+ibQRfquFnEMzuzpsuBMYGRZOdOPUSbCdc3dKrBZFoI02y947/KpZz9KBgh4oQxq9kKHu
0Yr+wXe49m9h+LULXBm+Aa1ftPcVh6MY57wqFTPFPKhGZSBgUT77HTwLSq083dfcsflH/8o8eSHB
NMFEJ1fIlrASdQxLW7B7LO0tEq6zjJs8S78y1r+ddnx9Lk+FhltqyBOXlTEsmdMScLZ26B8r3/Tb
7mbHrdNFVXcUxYLlNQGe4mJI/0uq42q/JbyrXtWR0KYlljOcyyuWAPUpJ0NstpHwpFAzqcljth0r
cmmKIpu2inAtyvp1GVgi469a8JqDHfm7wrl4sZ2cb8yEj40jNIMrq3OvcnbU9IGpPPm/fTZHyclV
dNJj8T0Jn9yZ+5HrId/SdjtT7ggR7ADxalyu1DVT9ZCYjb8nTpKFFDobrfY648OulyRGN9x6Qi7N
jIQGt/UpQADWyXXDuuD/McxvPNoZVoIwDde/1p1vXplOuN9tv9DxqIUK5695yoI2Zx0KicoDVQWE
QsEwm+0wAio1qOqP39kt5TPoBPyOnenR2xjIOe8on2FJ/IHcZC3AXrePMgXqxh37nuC2IhqoXzIn
UA26YkeWDG3Xih8tsW09jwKqHRxXJL0X6Qt0C2h2w8rp63MR9YV93crEsluYWfcT3NryVPI34mkg
DZULs5XhJ4ASd7+HUiS8OW3+ZCWLyNEZ1WQmNqhB7FNX/7kxRwJEL5TeOE59uIUBw/nbUm+tWf3F
cV2ZBXeSQT2eJWHyIrMJN2db/iwQdZu1+BxxzB6+iMAohbajdCcvR93cxlYc/ZlQBh87jQVDlFN2
rOEmDM78F6Mr5J7bsV7lR0oGrG3buvNbz2B5JSZmW1UAyk4Fg9XeBTgKzcI4/mYSd7VeaG98FKYI
FCVImiX1wZ3PLE9Z4IRQkxhwCy4RxxggjnqL2bdUYE/F6NZo0/zdSbN+EcUo2ngTJbUfPFxPi2VS
k3Y4V1C+tNCAhFMD1jLAQjUCqhvzrRyRfkJOz1hZDDurAvkpuZdLAjcCbpLac2bRwNB4GqXf7RuB
9H1zkaHmJwz1Uo0yeV1ZyebYNHKtX6XIJ7GdGZ8yNCNWU/ebnKDyPuCoCRIPAlD6ZEKvQXShvplX
dz+oORi7I3FG0UshlTdgeFssS6i9tOlBQLN96Yzez5zgnxVPKINrqYz3yQtAO3Sza1e0HgvX2T6O
Z7ygUmDjZJqtzZZGpukFX6J0yCQBJAgQNMQ0HsrIPirQizyTOoT5L3Mc7qYPEPumvAKO0PdWHafm
zq+f19maGlZllwbLNmjizRGMb4EcJ6fP3viDu+oLi86DasLD/tXyIlvL7z/g8NUD2pp++Us8SUV1
2WamPsldOnz8g0/B97camPelqdH9D6Tv0IRD9/0AoA3n5pRtrF89dIkuHZux1Vt8K2+uvzicuBE7
TA6RUMHzewhW0JXref45+ylujNAbkX0k4BCu5IOd5+Olf6pQR3xf4UewkdGbsVKg/4bV9YMF/BOo
syen4Jf/bQOvkP6RGuGnTkYKP3PYIofg8dCC2jhTBqpFlFp2kUKvOLoT46Jlbrd/MxX+PJIbCthj
P60XNdxeiBljsAcmVYk5Vxi31cpXEY8+GNfETVHAKMnfTJhEl4g6ZeV2LktADRRcf11qcKszEeiN
3iIK5dLdJZ3FhUB93Gkz8cLfTlOpmjy5Ez8VQ2U4o2V4O/XO5KBokhf6vcA4F4SPO/ybyEVUKQTM
7Eai2afJp9Lnglpwsi68A2twUiRyRfreXKVDWsmRYZN7Pv7i6M6BkGVWu5Tdrl2wZBBSLMdaXgbS
SPZGNY55Ja0XufmfvvrCe1duvZKE/O7eA6fj+YthdYtswLL/pQPcNPhXf/KgRlNGGqHPaJXlAvxm
f3S9wkAv535A4j7/IyW1aY/ZQG5VCCyg0Dnu/UYiAZvpkqdShmm/qpz+zBtNgrTsHPJ4Ic2RZWrP
puIW/la4XATEDFATXK0H0EcN1QY6TUAeWF3I055CF3lFzy9tJ2EmMPcIU9Zrlj2s0Tj9ORuupJWE
hldSzWHpztR7Ob/aiy/+GRhdL21WBsoZwLQRhzfPG8x3+5yWe6jFnUij1LFRoIENj0zZcuf+jb0G
mA0bFiSbaYOpTsEhW2L+KjiDvOFWNeDW+KCe/jYAaCEPHNfqGZDB4J5C2eweuzoJsNMLw4KBlz1L
y7m1UWFHH6bTGKVZAb9XxpXaVTOiWkgxkOzELv41Cg7GgCUEMP4sGFDsrqWcqeiQhtHX4r9CaF9M
yi0o/qPaGWVC4Q4Nw9l7dC0qyL5x824Aaox4tDrsVbaYmA2liw8O5dvzZ7C7PTXgE5pgftCZcZDn
le5Vsj9+NIg5p70OEY1cMCyOo2oqwa4BwUw6YobpdZywfBOtyyJMUpWTTLmWk1uk5WPzzo1YXQ5x
z/AwDZFEuf85o2CeIP5pwsYQgGvX2ocdQnRNfqWE5+WjIM4TIjFePj8+F2AjqD/V8vpXhluQTBUH
zAchK1m0P/exFCPCMiMHsRH38FbVgXBOum0WNMNqIO+UX350rX7EnTbSs/xkZl1hg+tX/YTXd5zN
NqzLt4V4aM/NQ5kFJZuwCU/FQ9ihRGFRieXqOAuuu03ETcleKQoIciG/ZGMCNtuqULS9lRxrfRwE
qYXhhHdKHDxqjbO9o4vxAHaXJie6XAgJrIU2cWESVonjjadCcxrK15KmD0VXZdT8QB80L44s2ak8
AU5Lz+/GKBvb32IYls9iIe2HTXn7rpup2GowXAzpZHBEqloJgsGIk1kuKbTLGtiOeG14zO7wAnHI
KkBAO8LbjZLDRZMi+Chk09IRI/2QGRll9otCudfe+niVHsMUicqCAVUBw9SnUPNt95FskgBZ14jb
dVzTF00oMtcMp2VNVoVHWneqoGxwF1VFC/GuZknT/pd5KBUnysk7zegzm/sugCNuvnUQjdTse39k
Y87auyhojq8epbZIDPx5G6xA25dl/2rhQvlc+ICaxswMhJfzC9En3gwax72g6fA9wGxChrNl78Wf
mhtrub2BbxBR4Y3CDRJ5qKMZrB2VE9kCZfvgk7O9jecRAaxx/9zULmFFzBx7ihzJSkjGv+CoA1um
2a1TOfJpCvcHe6//QzxAVk6XYUc+SWg9wOw0cgyVSSjoYNltcgW9X1UY549RzPGj85CBJ1RTyMDy
2G467fVvIaEvsP83t+drfO5b+iJE8ovrnXX7/PS3X2Xz6S0HLsYk6ZYPTu7j39XCKNK6rWF9PAa+
hwOynsWyKzFOVDu7AgFZJFriS8YGq0rW0gU8bsu/BTqBu8M1nmjtlqNHBao3nx1DFMwk9rOY3a4p
VfAKSfOqpD9UmClWLRhCEsgbiISbqQILR2QhliLhapeFw8p9SLQm8uL4+ztLn5pk4A9dmb6iXWzJ
Y7ubhg4lWJq810iwSQ310+6WgubuhZTW29FBzwmmXBSsK3GzTPyvNA5LT9N1IVOSBfewivdFWFN9
hr8/STckeM+T2Fri4l/wRuHNluU7Ajpzjx5CmEc3Q/4A6ouukBZ5Td/xe+k4oalunIiKZoCTTNIO
N21Tcr7sZM0p5Imo4laEXHpyhM2HoX6VVOtSU6HhUx/L3VidPKW4/zAqNY6kACmClAiyyV/J6xs5
Y2jWdO4gAuL2uxh610ifuWzZOmI0NW3UkGcAjKRubJuERD9xVMP9VEMxdeHPUub/MK0Qn2ZiIxgK
YQeVBy3fIunbE4alXHlApGADHQEzq20zBnUAfsTALX5DPX+FkAwLoDbJq08QRW/GqHdA2Gv2N3Nh
OPCjortU22BRHNL2SSYXiZUUrKEmqiWzHnv+daJAjuq1YxUDLcRabWKas4PgvyPGrjzlhhYKlwae
2lSe8yy9dumZiHcuPHoPNaQxFUn8hLxiDDyYVIw5pWoaeVAhyCCgguieyq3t57R4+FRbSz5HgkZf
Th/x7VVYfdJ9Ih8LgdAjwXicjJVnGkH3JJzgGhTnsU0YvlsmusRYgqRJMDLbgyJrHxRxH6TU632d
1DoFchZdfC/eUCluOSjwzdOjDsBoi4HxdT2m29CDJr5jgVPZyY/7ATnp/5/GzqAst46nA1xXB1y6
sKfCwWz3Lxnatkazrgdpl1onK5QlbO6Z1DyDParmnvRrMS+HAIauI5Qyb2XAO8wPYRbtvjbcHA4u
TCkivlvtNrWDtdD8Wh09QmxH1uqb2jMA0qYvVbW6m4NegQA1YSIq5RmBo2W+9QG2UCesr/gutqGZ
8nZpixJXwB5ehotf9/GHkn+ccMmdHVEm5RietAGEsfZwpqNesnIoTLMgo9QNT0hWbr5OjEUap2zH
kYZfXMMROvpvx3YVajXXA9hSLiCOpBhc6iOrfTPYDtgUtGLuxW05tcCf2B9LB6CAC195bwqik9xu
BU+1SnlGbF5VsNVqFsoQsn2Jq6ggj5qh8Wt5/7j98LYohJe1jHbT3RqdU2dfpdBce2nYMQXU8UP4
xbYY4IZBlsutufc5x6VRTy+v1beLazYXumu+3TK4nCTd5PkDtD0AOvIz+8HggZI5qJzEYnV4FT4U
ykovRRHcwRc1XxxiCDg7PFlcZ6hU4UDkxCk8NZvRQ8I0MdZ/RFM96YPTsyqHUZSIiV0ccKyw7+uP
UoE/bd+Nyxcdsjz59Lj/ThiB34/XkuJ3NcyuegQxOCibGWFRj9A1IWjkOSD3M9KPR5fMjSvFhY6R
mujLrrA4ib019a/9djhqAomujBvMkIjl9nlosLOsYtHpENrL+wm03hg9Ld4fI9EYEzRQw+5RKKLQ
diKN3BnVE2QE2CanHrkSHxYHU+eNBwMWdmgaNuvzmiQbFQfn6j9ubXh6gui0j6Thteq4tFqom4dy
xXJaU0cTMbnXO8NHnAg4pSeRG7mxOyyNHskm4d3uQt3VpwWxQGlkhPFY40S3wvvHAIqH5nBhTj1Y
NT9Cq/J3QgYnmpEqoru3D8DZ8JpyGXVKloP1zt+6uddh6zITRbzaLoMuBHP3p30i9L5F+xNY0jx7
w8pgmgQKH/uojGeF/8wX3fjgHKlqWPn1NvlB6//2K8LX13siGoO9ucNjTUvn9XjK5jRGJ3s3e8op
eVVHiW0+Jad/ElXY0sgLsYAjgdlwFQdiZzoMSaaPiRhAlTN8hA96sRK7B5WtcizZLqkUUu+Lway+
kItVypx3b/cP2Fm/CJstlRscGPKAzPuaWa5iYPZG37qioFehtkAOi3JZuhxotkpkrDcbDPI1wQRk
H/3yRB1nvO8dRaAdzjy5YFLAwazy/mauaPvL8wvigKNLbNXgBsXKBlH0vqxog53lQBbysF4HV0+Y
NPNnIHcgX54zhbeLPq2DNAoAKbjEM7SQ5jzVD0lDxZO9RZ53kpvMz5FOs9yEM2Lf/3sjD0ZNdGIj
WVnSn3nspp9WlamgJcZfzdl39cDPjFoTpk6kVQ/SrJtE3lo5sQmvNB5MZzRGbiiPzsJpyeCu8CcF
sXW3cVNDPI5rsvIwQcA7yRgBQs3LHLLMWgrjMM9yoWlLMhuIM+KK0+f3SGXBeRMkQphN2TxJlt9z
2d3lPMHteTj9+zcn92pZ+wHgMK58jMldie7wFIbEODZ8m4ZkpS/2NAepVXAebYsLPp+pWeOfeSd4
ZpFkSaAW+lCjCh+jM2ifBFrQJjMjP2z1c2GKKky00cpnOct0RMCD8VllQFpgJTsy16D306BCDuBH
gEJaS8rvb9dD0g4bHQR/OMowVyfz2qWWFw3q0YFATXDgnjBckcH/8VKOFQuZ7yECtqH2jB61cYk4
rjoYy9BUlboyaHIuew0O86vbRbvFQvr0upzveA/4jdWS/evjMAokJLfJxnx1BGXHj33ygnkhWzRu
b1hXDPEaIpt4BM+NZD82YX8DASCjB2GwjVC/h5e8rriuv6lXTJbSXTtPegHrTwPF6ibG3cRMMPRK
IxUpxhNRpBl6O8yzK6kLDX5sArsRIipDP5in712tNiEvBlRuOXXkiR3QMMFDJP8QXS34FlDPNjjO
x1cfrqQN5A/yBAAvL9ntER9wInx477GyMgUL7eBsnXmzr1WTkQkhVWV2m8stPNxlPuqAX4PayK6E
FXcuQQVOI9XLUGajt8zJg5Yw4WWcpxEoFqL7Ek038ycVWkKjPwyHBlUlUIRZeJrxZlvyMERrGMGa
CQNAHwDKrr1moH9jd+KcPmoaNl2sQbcaW+i9BE6QJHKrLqw2RbwGFALNSUQaRHT+4lkIPxP7yKte
TMIW8MkVzENPm3nuCIW3CaRiZwzqS1h3c1933HH7Mwh8/EEOqav63+WNINCQ3WbWk0DYwi9foGAF
xI+xtJRiwo7fmSzt37k23IhKsUy5UllKQldfwBsSLpD6XvFBuGT3IdxRK0Nsiru2betZ0u7QOdwI
9QoDLjCMUDMKWTlykw2HWNPmxoS97n8/Ex1L0mkx9nqOpSEDVbjCoyfTqzcpPVel1MawJYLE4Hg+
91AJvNmTbZsKQ0rMf9erLgpHUmqe1Hi7EsXo4WGP2GgUIwu2AOx3MWvdsha293an18G/FHhI4oXX
bbW+VqfYUfiW7wGwGO2nNwQVy92R437KpEwiMN7Wc5Hj8G05DEQvr8dF97kBpMVnf+BeCJuJyPWj
0SjkV4dNCC9C3D4j8a8hNcGCT4sVRhsL6aYDpxWT9X4ORdIM8Y7R0b07m6/ffpl8Sygx354GI/Ho
ZDMaxedII4C2oAbU6bLqh1dpe46bOMT6h/bSoESPHpV5b678mIfGv5XHq/++idCiq4jUhAakXdO8
IRtW2+Fcn+rlyrJoT2NUop/jTGjxy8udd4Az8LEvZESA63c5U1wWyfeEmzBoBYlrwgOdIq2ywkdW
w2e9gRzSFhd4qNE8CtHP56f9pnmdrZmsZ8b54gXJmj60ckkrZQZg1blTJJBwj2IT42bexyx7Gdr2
1Uzi2MleuyqDvJVI4F4mnvLLbR+yVhBgGMfIjeSceaInqUqOqNoLtfWt4NEgHgGcxCri6sOPWZQ4
smfpRFfBUCQHvMBYNTtrKBTmqh22sBSnJk5Nz4gNNCp9j5v6jqBeyox9tvlC/Lti92rPh0n7tp99
eQKf0iHSYFxwo7rKMZeL9kFsx+EcTxY2tw+BKgtYmYLf6kxRSxam/te3xsKrD7ukuIIPLMIDliCW
NTEcrOr9jVkMelXoQT5NKqgbmRKmMYjZ0FUq4Qwkh5yBBxo+R133Ejk5kLFScnYb9wZQvUTtWQy9
j9lnOFJMCnBSZWlBOWpv13faTpTO4C8K+dyZ0w7Nym/pJsEwRMAMJPk6loG9hWG82hV7oOUhyqvi
8kSt4KGIQ5pT1SzYyF0JlBrZ1PpJL/mxiuWMMPAe87PKyJ14mawVScLO/zHftJrHCvFr0R1KEEsl
exrq9BdyO0YHjDJg0ZSg9J0XVRZyPxHIqH0Zt4Z0V7e5gq28tcNXqZXNbg3Iz+mJ1c1SVaksSo+0
cA9/lIc6pdFOl1Q519GRGlX1JiDqRGmewLqFBVvK0HGyx5eWjmhPGrRKuiK+DYJW+EOBNDfbhhiJ
NdiJrZ2V+7dRIunJKbF9IuzKEDPTqUxE18GAFnsfXQjWfJvhsYYCf8NMGEDcKsPjlwqv8044DVeB
RYzbwRS25IRa9u6UyKWuXcw957sVTdA7AhtL+aaZQAnDsb/ePdzMjxXBn94dXFoLHN3nVoZoTBOf
2DCHhtrbzjhGzDZXzkR6tjfVeEOI0jLaICcXsFPNgISLFk2/fYsL0wsVQva0IK6u7ve55HslrlE4
RSaq5BWt8yI6boXd6lV5ObzERIyQwaxRU9CIuKaeEwDCgq6Gv2Ec6LH542YF0pTucdWfzTErz/i7
SQ8wgcz4nRSKQG29yznG/514AdA+IRRNYnOvUUfDE1+OZnXkk6cEDHmpXfsfWVLxPgQBJ9FFUWMy
2JyQQNwrhetDsrJzKktskEpZh4uDHx/4j0l+ktQ2HwDUeS9vCl6aTFa/GDVSyIUq72wiOD26DThK
8LyldUAL6ZuYJBlCgsEfVlvJwAYMEJ+CyxbQtTFe4Jk6kwmXGDZvyCUzMMQq0HeJu90qFMf9P27Z
pJq+jX1F5w+OUUBRdGANr/zoRsaRrf49WyuzI9F6vyBqH4r+F7lMVBiX5D6nz3RcV7wg+miA2OfK
ohw5eGO+u0/6wchAqB377Gn2QDsYdh/XiuG3KmOEjijrAelmKga2J6yI4u6uDsDx31fZzeaECTE2
RH5khic/JNTy1QdNw5DTymt2ie8y1NoegIS3S8dAV+hRAO+p4YhG5vRefCurIgPfGOIdbIdJoUWY
92hL3DnzJm2/hvjIm/UjvhwC1exGTPtheedG3dHF9Wm7/Cl4KIvaY3E4txZ/thLAvE8q39B0obJk
jrt3lDgnnZnTR9tfRL1itbzUOGY4rBap8Mnc1KXU/fUmvM1XrXi4hiqAZCMqyFxcZYM32ixhUz70
zZ73DigkI/kOQQCG4vUvkMK7AZZw5uBpqbzrfjTMVq08x/SSDmtpdf1V2O+TjHl4oEvroyeHTkZA
fTJ1ht73+RR3TCoPoCi/gKlMgm4pa05xMkEWMlE5HfZYebS8Y63wiXICHDu8nwpMrlfw+gs4agT1
A5z+uUM00eIflqWV5irhg1TeXaQN1etIf1E5RgOQ7CTJSqCgj34CkvfDjxbwtUppBUOEMPFwr2Ik
NbPwrgxsZRoEOl4DqaKZMENf4fgIPmXelfg3Ov3Y6VYJMiMxHxJsV1erLH9vE1kAZzwWTdJcXlgJ
yT5cCndUFIEqm167zgyjK4w2NBhlnY+qxOMOnXmyREELtoGfbLnOJq+NdIOCIpOqr1uHagV0ZhP7
IJ2nckpqI/WhP0o6VUkrh0k/jPjNSOodUSTTO4y+JHlDHHav/MMr1r7f4tZYZYPCy+xrwiWjx3wj
0n2I3xtcI8zj9OTzanbPNEPx+Pg+sIYMaKfN/ecMjtV+BTQ4oiW47NC7FjnLEsmr99BTXfAeichZ
4sOIxekJMNTLkJQvboZOAN1weVGbfkEg+e72D3svQeWsDKfHHRXzMU2GiRaVKohxRUjw15PrkJAt
a+pKHsLqjG44Ti3iWUpFlJVxC4dfF7hJ4Pc2Z4OCaAX/vGrEk1DIBoM28YCweBOhuxPgw0qXzyPK
Oc+H0KrPwURitRTCqfpVLtm6xwGua4cfCsaOAse5qRzKLM1nit5YGoZm81z2ILxTLAdKcqu+nV6H
bZD4XPvsSEYcOt3tToNsxfJ66bKqqW6tyBpkLZNiH4GZG1uJxZ2uzGf4tLyH5oxDyzCum4C1emf3
txkNcZYomiHuxf7plXIDJwgYJcwcNzJ5Ax5MDdjlIV6jDo648R3v2nJqrwp6KdwoDfZ5PsvgoFUj
MoIxvXcamRScNxIvhaPl7g/7wbUSoFg4ehrZ1Y60Xe2qy2bOB3IATB3EOnuR0dddzdcKgQt7upaa
E5T+emCdMjwK1NvvudAr2A/Pq3LGVq4NfiQqLQHnDCigfuqh7LcjmimEzbtSyOrOt9qfrl0J0e79
GUkVlAJmafRoiwOu89bldgsi63q4reWtZOrTUzDYeWM4PCzUCNeI2mBHnSK/cPIrRML/nOakmZ8W
DujDVjsJ9j/C9gVbIuEAYbDQg+mBVYzBb7UkfUVTiy5a4adpKOGyst01Rx76AA879woLpZc7IQde
ZsIjP/B/QBJyUH7ZtIhd+a7xbSqeEoH/N3V7H/seuplLhqxrhCdDIhSEeFSvHzM1epeqyQGsoGPQ
0CNr78mjwVhfsjpfrrmclBcG/sbwjtrwtxUVKtYLCbVUBDWx45SWBtwsNcQqs2HbJL4lhqAUkwj1
pq21J3Qt0Aeq0JOF6yloQ0XWBu2lkMgXk/wX3o0vzJO2wDSgEJI4Dude52iWSy22lqUHyyeiFRLI
4LM1BEH+Afyb9auyL5c96LC1hn8RHqqLs3PJDM9ePDG8RmznfEOYqC090bw8iNebjtQnMWq9UHi1
GE6OGLhralk/+G+4mjGb8vAqIjHfu1zqzeeEOOu2EVSZYwk1c1G5KZUNVnUpY0FIxeUVOZtk5+pc
J20nNMm9rhH3z6QA3mM2I5usOvf1LmSEWUL9mSxkLZENnHneIGHIJqBJVBmQVdoV49pMzxsiUnhQ
XOyo0O5sbpoEazXFd0yILO8SMZyPEpoLNMCFcVbi3KdJY3Lu6DRYlecRmwS1e9QMAguRbOdkg0OS
GQRlJcxrBdOf5zJYI7osznRXiZX4YhnSNS7FnvhYoL5JobiHEK66lQfgI/9GUvR1MHfDKMiFNbDZ
CxW2QmP8CokSndIhoGwBr4fbr9pMxQ66ZMrgAWo5XR+aOLav5U/JABYC1hevIKyb2twrOfQTdxU8
HXSnTu+IdZWmJ8ITH2BGxoKtC0tca4KWc1mD3ZJqSVDkVuTFHry7err6meWdlTPLhIwgCCopK7TQ
HfT8G2L1R79j5lHNTP5z8w5XJxq0XANxuAnegMDBUbBJ+2/S9J98IaTOOgFopzj9rGita/oEmrTF
CpNazO5TeSw1uy3hLo8j2xX5sc5virRjpYzHWx9kzQyIsaTjmXeQimJeZYCEShy6fRNt4RVU2/QL
mauIp+Ecu+j+os3RbYa1daXfizrB9Fqq9dZzimVxRKHlA+t2O90/5lbu9ig3pyYdjcq1cdarjAGe
ghtPX06ccer3l5IMu3PPCuUEvtFHZoI6f6TmLJrKX94mWlgrkdqF5uzyQQzgNEWZdlKaQ/Slgrb6
gUQjbBU6vkuZdmJ/iBwlRv8yVtQ48366ow/UKzUbJqioNRUOEwl8Kjff5vHmjcer/xs9c3ALzFis
KcdEY6Rt11b4LOVHcOyECFlgwH2fPrqNXh72nj1Ebl8J2cn8nKj7Nat0fSBclslpVuGNmY13Z1O2
CsXrIC4Ek/qMK8k68zanzWXJpiIOWktL3Ckq963NvQNI284yCwoHj4XDO/nHZ0YaNMRIKocENJ2f
Ok3tuWy/8BLpXqNr5rXfhjWv/Px1AndheheJbC7sUCghmu8N+ViJRiEVmS5z6ZXn2o3M9lkfPyxf
N7c8HDdoWK4qFllivEMbfh2mS+ZU72BLlIOI22rv0YOKGmNAXrtXop3paHdowzYdI2i7z6tVYaXA
khkGPjNkNe4h5/LR9wkTOTAdL/i1mrFKeMiqAOS8Y86ga3Nkon4gnorCg0OAF4eVLlp4Gns7scP/
mbCT1RDz1Wn32BKHazYPdX8tT3IbSRYLFWBRQ25qcL4M6R0clCRGZiuXfZ9HP63WdZvCbz2nA47K
H1WJl/dTPNmoQ4mDDYsg57qkkkjL6uAEc+k6j45EW8sQR5RUidLHP2d9St2Yk67g2au2/9bGci5q
i1rA1niHzdY09SOOXDxvXRqfTRu3XO2LuBEXX/wagvEE757z0tYCwnRzYBAn2AZsfJGw6x1hvZDx
9AMtcG3B/0XoN5M8LB8FjNvjLYifWg4cL4BJXSmm6suJOncw/4LSO5vnQjgevBalwcQtt0vzfJp/
dquMBy8w/58QbgHbxRShZbSWDcC+K1NSV0CwAgrNrgoa7SnDlkf0MrYS9H4M5JcX/J+N/ijAVWsP
krnWEy7XIekg1HrrTrI17GO7/OXh77SZ3yY3TUiw+PGVJweCP/2qS8AHNFnoCpafj/Zo/DvYSF/W
eJ5NYyY+lN7EUnqEo3OaF06L2sgJWMd4cwdpQJ4rzknL2QXeE13Q/gK+WT4TEydxYmB0qNFAxUXs
+3seZWb3m1RSYriRr6VJ3U0LBqUmOquzjTr7NlBYvtle22JTvDC8dgTcnZ5nvXoB2RURhhLah/uh
kND1iQBOls/YH1epY4PKMOgnbaaljokoRi54UfanZq43KHCqTErnAdbFcmJFvGhgKpU8IGwN0QNg
X+O168f9ZwwEY+34gGwhO8LkapsBexiYlSjl/U0eGCjwlhgiYudX22RS2cu55QCPxTFix37qQMnl
ex5o0h/oFEUKhMKxOqq0kmgBQU2BmPaEMX3dXp483gIZOJbJRl5eiDKGqDq/2Thc5oTlZ+Q6bZci
tsu0J9MSXymjrOWC96zf3zcvDC16dBgddthoOi72u3Bmz7eC2SMUYC/wsZHDCiBUk6qePoLNy+xw
sindbIgxdKimlZHVQYz5+WOsa7/bcw5WErOr1SiHf3bO9nJMbxtGvTJ7vUS/T9EO74wKYxzfM0Ka
ToHNNxqpk/IRiRa3iWyu+jljWy+cT+DWfh7ojD0EBPZh0Jf/hfU+mbDdQM9ptFy54rU3FnxGQFnY
I+mbbVAl8WwgRA8rs9hqCVhCnDH1mCZlCQyif5Nfqs6nm9ETlJfKJDc4CFvQ4aIU2kkwSyAUU3to
NrFkUEM88uhhbMpMUHAHIrstyfM8xuEm4toDFbydUbHYrsAz2q8KVphiAXBBfD6m1J323Lm/IBlK
8GFM6m7+JFBqOk32wBy6B+8R+Kon5jPGSpd7dCfS6JK0Jxp71W1Ys+ZKUDQPe3E5YPajAvqjwkKv
aVRfNLb3IzHZkKajAz2Esp6maArWC4/wn6l2SSsbiRcBaXn5AtczzsmgF4SVcoKbbbml1yM+Hr8B
YiygRhiNiiQtWfRPN+bIJoX4t6KVotSePFj96EZW6QJw8RD/jlfugDTWnO+lpi517wIIhbpg9mc1
gEF44VSgFBmVkL2T9ey8cyfyyBKL8/fzh6/mjcmykz8pVxHQCHx4DiEugW1WvCGa0DftIn5eedGT
AjqKrFbNd8uuqtfytHvlAdPB0C5tUFY3Jf2gbhGAYZNHHRnQx2J7K+bvOceaU94TVKr5/7h5pgFs
ozzqNCIuJtO48s+WimoaZvdCfdNDIcMC91Ao4PLeQy83ixWqqb8mJrfhLLZ8h5rcQiMSB6opFBde
PYRZ7ha5jgAJYn550sAhQujx508nFhFkQzBVzfvZXnTTmL/6IJjD6Ljiwvq3LwDjQa8QRdtGB+dl
aUufMwUtCNnLksYDvFfWhnp70nRoxxB5LZvTtHtPL1BGiD6D7abH8f1eWH4y7Wi6u0ikbJ2yuyBH
X046RzvhOycomWDjyCC1qrg5axQ4CrReGdKu3EA8glrr8NL0tTg9cegHdwz7LxpBhme1/jJT3tjn
Mun7YsRAQkG3FmFjBVuwl3jzETvADNEbnfPVx3eFalXzOWc15SM43DMlh6//FgGhGTxanwVtkklb
0uBoJGdJfSTsYJTPCk/TkG9I4AofW87MBFeORCsApmmU5MM+w2O8F8uMM5bCDNyr0Nv2EdEPfjRD
Iz4czE6R6XIFLFAAikW6OGyZDBB1CnKQNb6Uaesairk7rpZd9wAJbFBFb+pNKK2jUWVrBXLwF19H
aHdATOcxCASwKdwvItsRPSENoSzk6HClhybqyFoXByEI2QT3Pyx+/u/As94PE8H9JX+NTvRTdehD
gt5AIJVRNvthkAVuHOSUUGZjysDD4pZD1Z1O23A9e9/2Hspe0eXa40MY1JXQ2nvkvLaMbUnEMYT5
D5raQB2p9veLFMYXYwuvbC3edIZzVBnTcpD0txcKbWv/5FP8FGMK7CRTCnvCx1cOuVdQhFLzhN1n
pA5W9KRcG1QQNPA6MwYl2djx/9Ds+Lgj9uwcpLW+EBPdIKjTjv522fyoSEByeOQPxypezwNVHW7+
gms8bDJwE5pJkpyLtxSLs9+YE5kx18bqtEWbg4nXdb398Apn/pM9eLpf/8jF7tFewH7wUeKk8tMm
62Kc5q1HFwG6fN0O159KWMKAiZrRR+FgkMC1jR0KCFtt/foKLYLfKZyDZpbmW3O0ph6i+Qz4CGJs
C8KKr4bx5xh4NPtmViTMd7H5iw/5mHIi4HfR3EmcgtTnCwVXs3NCz3h49GkReM3uF6TaaG1bT9MJ
vkifprA7nWcWLWA5l/77VpFKKCIDtFzOrhgpeFjJCwjsEv7AJZyrPGCA26+LFUSK+zHOmn0t7WjG
s9fLuufzRGykL7bKSiDfToJ64Lu1KwAo3TknJh7rjXoHhmJRdcjA66r7tV7etBT34KbDzZwRZxbi
L50h4Cd36Za8bfN9+/zQSW/TUw//7UNVixmDmv9FW3d8e3ksRAPFkiGix4YW3OzBqDJwUIYIA/bg
0aM0zcLmJx4MHa0aRTDGhG7YKI8eGNHQC/9A2aqnXvtHTrq9YW14AoIzgFCIwdGISpfzDZRUd48Z
NEGd+KTfKC6kAjnucASKkPZPmOkwU6vE4fWKMMkTixIZKsEiOlRNYLWxOQYv2yUrSW60a4qiPXx6
2FEpYbihxB3Xf0iWiLLr9J1M3w2Ikb0TQyCYZN+x6s3OJfnVUNK+xMrH9UUOHpddZjH1Y6x4f0cX
D+oG8e/pi+uY/5y7MR784700dou74+NWJ0n2MfhVHZICPsIs9xr0smXM3IXfVf0C9yZ4ry/ckdZg
5w1z3OEmj602VPvtQ5oKBwBrchCnmzzVgvlwgsukj8TtJ1ghS8WHny18u/ds6MZo+1Lt9vccHKFo
N6f19uphAXc1LCtDwRqZTan/ArfLCtziVdsrpPVHIMno7zGocC9CYcNamaJmlFgqYkW/2sSZDZ/3
j2cFVSQOisLodnRXGHJnjXtbCCaYVGvNhUYNVa3+/2uli5iug4xqvpIDDLANZoRMYGJjdMe5j0eo
7l3oFf1vNK/laPIIrgI7GcK0VwCWZsfCGGhk9/3V37hiVQwdzitvA7EsaHILL0xEJmjT24SzGqUv
pRJZ4jPB7w2MbSQrLP7ien+aJMoLKQsv65zstE06O0hi4sxrlqBy9sDQAu2ZAFmbQapxvN9lQBN6
6ZbowEohgMZLTrNUwOBv5clrugytCh/ce6UXHD9740pqXhz7DqGGPsSKQNvLlXucFW8ABLGmxWKw
dg0yp7sARcaysZaSHgEDl86XH3B3LG7v9h+RWz5VEaEwmKd2R+5Lgo3IVmlOXya6tLLiZL9m3/F7
SWEwAZQhTIGUi+9CMtbF0m5dxa7u/ch6/Ba3mZnktPdqXh5hvXnJWtoGIsDBYdrgQFyBBnkhNi3d
cw5sYfOWrwhoqxgc49R+Dy2HHh/BXN7S3oxprRH0aXo374/jND731fLDudK+VtTmt/O9E8Y8Uaye
WwrNJmIgWkwKxNmoBxEMGSzVUIdkmsNehJl/KcMuukVHE1+yuWeZg8RQAJx1lYokz0vz7F6OFa2M
ZliCCIsrU9r93z/7G/pW8iGSEtTInr6rvNNRCFiBQ5KEdW6XcGBJ4a2Y7Cd8HYxlCrcTasR+jrHv
NHWteNC36QDCzBtfdbvLSgK0RiuYaUfFb9e2A8tXGjbJ4S4tEIHHa3ElIrrsxhpdgildVdhVjWK0
TdGIjuyhufYsTO8FJFrtpSWkzpGy7oguU14wjYHE1FKaUS3jIcEk3zvLoMOgWT+YKjpYz3ciW9CX
n/tEwEADMtS8xEsj1vxqXO8xybwZzAZeabIeBMJurAuzwVlK/x9v8bUl0kP7wnTBaQEaugcx0m/7
fJDQbRBvinxk84oFuBUsqMF7b/CWuuic/vV5VkjRMlZqDbYs+K8cx2cokUW7fJJzfthxTAKIi34v
isq6N2Fr0R59de+mJX8IFMN7NCQEG5bq8rcBzQ7rCS+Qn6jsfI4K4mkrgQJWxaJC21DLswWPR/+1
PCZsn2UCcZ4uWqGriFKiX6ZOWOE1Y2kpB6YlnaAS+sRQFRrrM4N0zj5r6yY+Ffb8WvsG8rJf9rdL
+fsTtXLGtRKbIxs2VcqsJZom+KIakBHK0mwdw0bg0ECs+/aROg1MZs5LE1B1hr0nvFWG/nd1PuU8
5iwLZ+UV7aQADDishAY5z7KejvG/xmOHHWlSM/dNsEWFrY7hSkpaCm+wfJ1P5xYgXxdVGqAmaf/J
u0UO/xCwDymBhya+o6DwgBIUz7eP6yX8CEWmQpBNm1XckxRI8pNFJGrqJ/63zESxGP90cKuAnP/L
VeXinGxRAhf8PjdVhhjgTosjS/CpMxMQetRpYpsKLgFDx7O1VzMvdPLhhuy0LE93HCyRJSo5pza1
44LXIUBvofj7Brjo4/G5QP/vJ7uBXkPkemPVIT11ZajldNqrgyBRFJPe1X7k+i8ObKkjzHhP+hUJ
uzCVQ2XL7W6YpPEtQkY6O/XaAO9MyWnk8fpvVA8PZVjNlAyBfY/32HZNEdnbexf8t9wpTyjas/UD
LceQiX/Mg8YREBaHQMymnE+HzzJBBJcBVrGLitma90BicLUy0jgVauWwQiJeUQO4p+GfiCDPXaEb
5o2CtC2WabD4OaMzkTwYeVo/FL3NXU2OyTKe9lQUitgaDcqTIFuCoNtpIw5tDCHdLDDqLb0ZRxTC
6N4oO5ChreY2LemYvABZKV7HBvCrinTZ13unl9C8Tz7kEBoqCHFIgDmJWu6CZ7KXCf2cW8rSmvIw
IKp+ZdaquYpnU9H0+Em5Vmun+1OP1khU8/CdCKFwviiAPkYoyeFdFyhgYguJmlcg8OQchcLz8Ebf
cZBTUn9fjtD9staEGn0E0iawLxCKhvhpZf7+j/NDqKa+gTIYVF4Kc2X/qhQzIWOmmeq7OxgJzzSu
bVgbqGNcSNIEZTw53K/hdxuXvXVmL7K1sCjFGYozfuiw5MtLOyYa4dJJUK8wwXSoE8FRvkVenO+V
JiK1i6PfWvFK2+mgj2zbN28k9ip2aaRdAMgsvUkBqLvsJJMM7U3Vf/dClZ7NT8M63LWMSgxp62bA
bIR9yOS1rzw8FZ7AnuhyVka4sS+UfjfKsMX1z6fq8Ywzs4+xnh3PnCZwXfkiDbB9hU7egK/f1XN1
LtVbiizynhdMWYlEk7x9uwfsXqAUPV6UAmQU8id/FxuSiI/Jkltu8jF+2XUHfgoEby7gktzHsZ8r
JVLZPnPSr14ClZmWzIrRtO5/5adwZDOUZtJ+DOd47JH7297kiL+nZTjztnm+Ogjbxm32Nj/0940j
9w3QfVYsn1CkQ42DBoB3d+nAx0Ks7O59Qv73ua7wQeKtPN5K4dg8kd7Vvtl5BU9TcbO3vUtqhq+e
XPyoOC7vlGEQeVmffAyNsAGP4RlO5A+3KWdfWP50gtWaBosBw8/DtaghPbmFMaNx8VmeM3ExUTvh
3+T3J37lHUEytcsgpHKJRJzoIpl3k1yFd0iIuS0UXWXClDPN12v48L706+5GkFj+mEnxtMRPY154
B6E6TKuAYal/Z/vImX8WAGS5/Me7LHbmEn2ZUQkv8b+SbKe/Mv865JidSwsRL+uiiiRLpHT8TNB4
ASYNMTQkn3Lts/27Or4OEvnrxrKx+3os7VtSbR/Xt6VssI7tDCvBTNRsW0siHeao71UXeKNLFxxd
/fKrLWnaZMoSyypQ5ycDFzvXrp+k8tjT5zjHhgZhmxwSzDF+d+q7CbyfhyR+s8ozba9Q0w4QJr7W
7JCcof956zThEdrkbIS/dBbCQNEHNJbR63nHeb6A/XzcSRDlr7MzJNRbSazXs8T6qfdVXC39dQVw
W9LJW5NzBBg2rg27sMWOlBLRzhvrFWbM2x+tD+VEsIIGAjUVeQzp3GxX4Zx07VnQK84gTHzUxM7W
R10C3HaN8wSRtypj0DmYsM/34yzN6+63NFk2X7y0Za2uDwfhFD7qnbZi6kTjGf9+lAZlIAaGpLSJ
5GTBPCYhxK9P+ePv+F1IY5qhAmpBDNeNbi2st+hN0bkJucUxEYJ5Rs2DBknKdH/q1a4NP5YIkVAl
+bY0IymqEUmULFbOSylW1gjLro+f7rULVOr+KJ+NxHxly7fBNbl7t3A63H2OQqVkrWMaojg3RrcT
hNSvsU4flpYZ3S0K91Myo44sZ/XGT9xvZo/pMpPWRKhpRtwU5gJKm9uYaIDxs+AbhsnOnj73Mag9
Oe65v+jEZt3x3EMnyE5sGky7cwtXHUCg1qlKYy/aZR+eeoQSdz16OvUVrhhlgUT+fpcg1f3xnQJm
yZUlaQIfjoLczA8wT2QuS6o3ilpwr5HBkTEzs097HcuN56yQBFaDgDgBmr6amZJbSQCUW/HBYVg/
Va6Kk/c+0QrSaBiSeHufWUnXqdpwXIZ7Hr8KcMWrtzg6s+OGXVxJI+jaOEB0BAYjNR5xHOIaIX+y
+3ZOkO9fyQWHI0ESi4Y+asGTWkQCDmIYHlnEQvbZ7XOz+xHRLrqfVyvu//VEGs6wL+16QEUf0B49
hyR8GUb/Efi9hyhhzGaEEjSayDaB7msKTEtK5V8C9fUyr0sYi6eQWuQz1gAICHY4COEfO6bZcWrt
VVZnIgjIJoLlr8pW3SZ2DmrPgXXLIRuv3dMkbQ3NxGAS96QHgQa3EqmSPzOw2V9xnUln92p4ICOW
2x8QI4wzVQhYo5CjaaxRJqLql+2E4L6mwu47RWThTaIWMTglGRSG7ZR/Vxq4f3TLbECIFRROn9Fv
ox2+tDRT0np9gEgyKEyglvOMyssTwBVYVls4+3vI2C+f68+r7SZ/4yiFgNcKeLntMNpRqFeqwzEl
0mpsZCHFHviBm0VUajKbJ3Wxjr6RvtOxV1pOsbTVCWvz7sT7irqBfLcDc+1KuS+8kOfXMQQWO2mc
6ozCRJdU8H3h5o1/zGkUvoRsFXAXAj7cDvT09SAIGiAeEpLkcR/DMK/81GrASQJc8H+3WwqG9hzr
agDHE5f1DXIA++v+Whd9c849BpMlki4QVRzqhb5OSwwZYWqz0SFTbELpGjuX8Xsq/FHF6PEHoVge
r/mzZSC0rnj/PWJBmgSIfzhA1CHNnA3/A9uY+e2NrlEr4a44YKkWn+vIaQP5JctRx71rAMKU8xOZ
zfBsxumD749/K7pRVp75yhvcrux3PENapOJr+reYAyQ73hZBxuwaQ9FSsCSl0H4gwmWsuxULeH59
XcjxlGo9jmpu8jcGJFXM+KXL9nJXMl8+TXn/uJggzD7XlGu9Xk7aPtwi4iiXT6qCG4hpyoWOBy/t
KrDXU2QeDVOK7UJmUhQXz9ctXYO3VqST2+Y2h3GB1eLhRFyLMSzLrZalxYgnj0Lds1JIdz0RIwQD
HGf3/6xhSLOpQigmGZGsIdYhk00AXvQuullVnM1Os7UeNIx5fRmZhwq26UdO9FSpzn23FJuC62ob
chVchUsL3UCuB6ei3mrAr8l9WyltWfhezUZVt3vcvxCl+BVRc44Y7lAYQw5KML49bQRO0pkwWl46
hjtho+Sry2lSvZGRSthIDOS46eTJY71Zuq+WoWrK5x7EbsRGAQnFrGDB4GvZVX7k5z2pQeQmQ6e/
FkxcdbRHNcgoQsvorOrtEXqnBuYnpN+mhdZXgBuxlU2mYtvio7FwDl6fcWhnRd7BDIy1dDGfBQVq
rt7mvxWkGsrzqqJTPFm8xHktllVIQmU3QnjfZYyiirH9fWyI7iMn9puLNkQXptsCp6d9oR2aAL/y
+xQmGGysPBo3R5idzC6+bcUfohBdl2B8TU/uzegdjINoFwsvsNnYp/Iq9GT5jny6qQmFkOus88jT
CS+Yofzq7u0c1BPBgqfNXUzI9UWah6ipyMNpESxH+0bW/XZxwTBe8KoZJYquAulkMuf6Gl9+kacJ
VqEoQGTnAqcuJA4i8eWdGN60ATrd98RzukTVMbwuWceXIJczKpvi4V23a39LbNY5v8jdJD1diXYl
LAkrLtvuYulpU21d/jrxWFHJydpMG4WfkZvKz6WkB7U6z2Ib29M8bZTRDen+1RJ+j5inoMrAOTS+
8SOUkruueY5IOUytZ4Auw5+GJRACGElvGDa56T4SRXg7Km5zLg0RshNPd8t2LhvAp54ehEW0llbf
Kab8KmdTRpTFMDwSUTJ8I09qNvqrX349OaPFnzQnTdCGyAEhIQ5s7mLOkt2Rd947RNN5W/WTznJ8
IpoxHnud/4LU/1PIY1QDy8N6F+8iL47F3JBXd6V+JVzXk9xUrmgJdzT1zpbCpHJem+oLa8qAyhg6
08Pfl+GlOqOOHR/GmilYaPA1FtHOetrzc9E+R6AIVo430GgMxCmE0WvwS1QIqECpjSlrgdeVwBxm
nraGZfOS7DuGk382RXaIuOdKj/OGdtAK6WyI4VvF1ETs+216wPTlePMv6YN6/gIbpgoBfeyot7uo
sglr1/HgXcjGjEdqoXnfpOqvUHQ5Xo3D0y9BZVuhd6iCLnWYAi+iKfNJoNMKpd+Id7E4SBUAxaAC
wqS265WVmhNhsWB4oH72R7Xjkr84+F8FAfBnp6qXZCpWX6/8VW6vkpRjaKchA1tAQQdFtZEF0MxI
AeDhfLJE1ZWQfTiL4LkN4k/grBWp9U8h9AeYHhJd8QDPgD4QyzAXZbE3NgFUBdQopkGlKM4tkM9i
YG0ApaW/f5vgmPbMDjKEGCyunECAWztMg0jr8IHKAVZuoRHLsWJZkbjJy3hYiUOQgyIi98ilpba/
mz+d1Hr70X/GuLelxcxgvHL/nHIgfqHenyS/Q54kL+m5Nf5an+blg0bsccN918bnwAlO5SdQlc+4
SGqpS+TxZIrqEdpxVakBxJFzdFYjXSmxIh+1apOOwSv+jx2LE0AViV407dx4H8ztq2ohezd8ZT5x
2BECN1Gxafea8eukh2yfSrHR3XXRUxduecP/iTU1qIncCLoxlb+SyC698ekOwGoPQY9ybpNOTL3F
f3jNU+YhoeErIlBFBFciYx0YTzI7YILQ6s0rClvXQiYdXh9wrViqulpcYs6oLb/43kKe2GCZc3Xx
U5NfujpNHfULqTirI3GmziRpYxqT5EkjHMwRB5lQ3Q51XfTc/O9PRvSfwcAASpGrrfOC7fgdDxqa
MDRk4lxcFVoH22F/9SpBtU59CMtqwBkfGNMHXV+duYVUzHiiiO7n0zpNoshZIkHf1BwFa1m8ErBN
pNY+g0EkMmrs2mCmMCGSFG/5pHuyBLmG3aUUfTLgD8fbnpmQOAR0sKKkvgfRgp9XWzoZmHUUhwxY
gftjdwCJhTM46n7xSui7Bbv13sff5WPtAyWiaSXlXbx0cvXadRIRSe1klXkR8E0dtn29nv8hawJ7
5DIPDW35Zar7WIyrp2pPzL+dKsEAgjRZc8U7vMND+mjGBjyQmXUCDfBnTr2e6z9OIUpjyYauXIrQ
wyVttRdG5+d2TPtV+UXae4ScpZU1OasZVk4vSgUBO5pBOlJs7TLLEQ2GP0wLwRmAJfdu4ZAzciZa
tbe19ESn4tDf/AvVSu1YREoTHWG3LEuiCqzqi3D7vFJk7lHqfUdlm7J1jJrVY8ecPevJkFHsdWo2
UOM0jLRSaw+4RsxS8c00jSRCIxXYeVpLJbys8HHJE6WBZst7vOloAkGiy1vZxPvDVEFkf92zOz4Q
JjQSYkmidM2szSHRzIedhESOiOfk+boovFQ1N1gC0CWUj+n/5Lo3lh08UxRe6k9vilMFaISn/Qux
NLAd1KLtcyCIW+f5xBDSEkBnNpC6brFs/XrvydC87QbjJviZpCjqBfAFJ1LXNJe6XvBRcxMOyK1Y
LmwsBoOqO+4r2xhDuXrHdvcqpMNGRMt+BqwE1fzaM+Tb5LJTC1L0bINwYVMz5x60vQwme0k62mHA
TOZt/tSVj15E+ZpaP6iMwGpHXyWE9I5V8pq+ytRqtHlrybHP9PgXu4rGo47lQlScmCvC9FmVWPu/
hXldsZL8D5POP+SRCwiHZ8Oxf62Nou+Bda1C8jzcy4LsA79ho3DFZJeoV3xsa5mryjUu7vbG22hZ
HxnO6JLaEYc2BT5DjEVHdrYN+G9lylklCopzPqpqViZQZMxs5WgaM3KO5Cc+XaAkm4EggDuinZ8B
VdC2NuvbIlNwSamyHcVrx+MOQHlOnrVo62Aqt3XQ2GKXjBXfJn5lffVFgudrBINiDMhg7ojYf8JX
rNIXh6rAxY4HCEso8ZYnMphGr7c6R8Bx9a6gXcVHDY8g9wLWrTtikY/m7F8KZU5aIrn4lCt+mbvc
crEIbV3j3suMSQ+40npmy3TEWUmORdOOum3aDJyxmCIIE5YoWMGrWzHuhO8n3ueYjvzKUf7d2d9H
zMCmrYZQJhkZV/4/diLK6Owh/IPH7+vDbeFPePVsVXmrdervCGsAWWDGJVZqNrHKRBuGMHnhvzOn
QOGxP1S7q0UpKqp912z8FEjGy+ezhuxBgbDzjiabFGHSM09z8XdwkEvbIhdIWV1pTFyq3HvTwwDo
lXMzeaHm5QqSsBOVcWu56yfm4urV2v8mM1FXVPp3VZtMc5TNKmYcY6gdT+3zW8/QQAJ6vVMNLcv1
+u/LbGVW1dLIeSe1cc/11XoXU34ZO7zit9wGFFX/hYj83FxHaqHrbyoRXg9ow5kdU/RqMDKAaVuW
oNxa7Xoe6YeRjSQ8WVjHlhxbJHLBQJ+j+TmRAt1yasRr7QmRqvOcqYBrdC5Zv6YJs7uevnlQwncY
LFdRF2JGx9DebcZeSYeZ50fm7bE5t0CT42KqHgdPMcv4m2oYc2UmjoFW6oLswtYZ218CfQuKt1x3
znPaHDmeDtxwlnW2mQ/6/iQjv6KhplDQg8cm4AOzEQiwwo0oSLU1LJkM80Z7e2aL1A2rJHbHz/xB
WnGyzdxlg8Zz8dG4yfeWGDU1UsszUL2GnpITFxyLKGAXWxFHeX6brALy8VX94oszmN5tJ0g6sUNS
qOW9e0dQ/dNey0dwH8PdCeuMoNxWYo3DoEqlujer6DI/I3svBd54K1BVdElNfzjfRwIjocWsXVlL
FSaADx4agUurYtV5m58QVzVfphzOaRBv2Rb6+UK5FuUzarNzuWzNcbGcNBcIBOT86gJU5WDeL7ma
GQziXrYn1Yf9wEmDXerkjCHUKbHFnHxpz5qfrFDyUItNKQOlzXOgny++IVU4SZR/DRdSlsjYVyEr
3Bj9j5g5aIg0HtYHmD25GJ9VDursXdElo4L3HXDHobhmyVMJD5Ksp/klcjfv3OXs5uh7RPRcvkrM
0VUTHWohj6cl5U/62RuBwZl36J4qRTdoOD/ctguU0j4NL62siBtxm2oH+XFhllV1zPqndhSVLC2R
G9ZnVJOWJW6yMSRfoapD3Yec8Qx2S0U0UHoTkNNjoxS1RY4/6MS2fC/72hjd3vDOfraIU95DA+9Y
pVNPsIkfHWsKB6r3qeD/OaxBdUuJ8Yl+7XhORinivTu/OM3SMU87uv/GtzpcfGPMD9mOUtz5IITe
b3qgZoyr+MX+qT9filHRplxOoMU87q837uQRDXdcGMFqolDg19lA97pnPWyYzE4bc76wzvLrKC2c
cRQ18o/ZhhM7KaHNYdIAhTMhSbhLFDCc7MkfrAuin7YSrDo/Np48pAIUkLDhto9hhh6IqAai0QHc
TBSFwFS0ioVmCAWy+fWHDj8zAsOAGw6mqC2KeqRw3Di8/B6pW4kO5ARv+NhcfxrSiyBIm4BA1wn3
GSY4NMA6ZUJkui4fnBAHxUwyDezAcNyl93DTqptL808nsLAh8jeIsqRyAeHpABOn2LpNWBWkh5JD
Lk0o+QvIzaMsJsdvHvgd23F5QOcH/8Bz78F+N0YGc8SWEsjYAnbfKiNua0Cjqs9JmY4XnCgdMMV2
EFef6N2AZ5jlrpnKam6JOlv3R7IpDSJQf+xUowpNsLD9RIZ+J5qGMZhudRs7oSHB6JlExTufYy06
Nu+qJc5nXZHuVmaBsv3khVIHlw1gHfTJVtka8PQBW+kuTYda7viksF3gdJvO/e3njynTRwley1vH
o4voX9R/ozqrr6zlVZ7sTQ6ZAhFTCzaN4+5oRvlaKlFJT6VJ5rvmtmHxGEW4aUi/oEEPYDD5wbth
XijPttbULKOlJacFRp6v07dQzhaScqpB2AAqOE+VjIo7PoKBGu9n3LkYqy4xGzDMsZpyq2EGUlvj
aeHHOUE2qfsxT7Zoim5utdC17RgkUsBjLaf48mHHqXPPWqA571MSKVXSTYhXCBHYvBQunlAf1P7V
NVPDiTqzBFF6SEcyJ2FGIVYi49DyTE/pzuOvbkSgDkOPQCpzdg2KcvVZ3iSmmLBt4Z/t06FIWs0N
BnLpajbLTY3rzNspsmsDq0WEcW+aBWoZndcOIB3GHSjk4VmQI9bjmrXYg45NZPt/BXcND9awxwLw
bNUfpJDA/JpMjji7HYfbXia0C0yzZNNnaL92tqr0/ollZfkE1IUuruYZAGi2OZxPosNhp6K6AKye
da/9SrZjsqgfi10YzB3LJcUPQHTuPFTV+fDSQPOoriX5SPUYmmUOBNJxBUCigSo24skpHbjyXAEy
/CWFlNYv04HLkUfNr6G8uBvSXh06LWoHORIxsGTA59JpjShP67/f3XpIz/qaEL6x7yx64QeH8BzB
zii+rvuCcLkxS5BliqusemVZX+WUf+5cak+d4j+nIW6fVwnN3zVxp6fHMlLwTkQ+2+YCN61tfi7v
yJM14hGWSdAQ4KLFW9GeRfGc1QZgN4lTGxqXQ86WO9+2WZOfeBxDaapl9mtP5OBvXIprBH/x7QlJ
Pwt/oqWXpqCJ0dlQaUb5m5vtr6hDd5LSf7ExtYffA2TFI/NZS7tVl7H/D+/kcd8q+sFrNJV1x0gX
32znCSkyWU6S5A2e+I8kS2JPGOdobptRGsYHuYU586Rsdm709cC/Uiz0px/Fcs1x9tQxbI+M+1AM
oITBcL9vf5i6lyJ612PUADRxCyBjBt/kGijrO4z2UJmGf3/r9nD0fvPXTX0272D9VJtRq7OtaNUL
EZgoZBchP0lO1JFYFgjK+oV1A92VlO0uv3fruiN7g17HV9fXMAP31dMOB5oWEF9oVlCCls8WramP
1mpumPYnBTEZbaExnZ4OXGDHoWItnOp+NCTfCpxF+b49yickVGmx7sG+jn5dLA6kmv+egqw/PUKo
AEAUo/jPkcDcEkELSyJXwkgMuNcbrZr6t20RKVFLvkwy7z6XOtKpO60SlmM448ilbDLX2QBlN/sN
Na/gaGAPinF6dN25JpLdNmqqqu7jIeQgxBVoTVfwwXsdwL0jHXBjupuDviqYQcGpw4FJz0q5YV9e
z1zCINIcc02IQaFIzb0sqE4suonZthDxI2sJGQsgo/RuA2MDNUrHAGBdatkO1iQZBtcTIqg4H8qA
kf/uQGeH+/7rZ0mvCqT7G08gMZWmGOlFcaRIIgajn0mg+ERw5NSsaHIBeS8lC3YHIVhyM6uJ/Yyo
0nf6t3XeK4O7oChMC2kqcDmNq1b2YYcx0lc+l/PXpQy+e8glHdSTuAo3czddHMdmz34toP8Ohu2R
rRkglUu3VwcO8Bl0gbd31euruBA05VwoF9M6RMgxJ9lQ2G5F0t6KA5k4IrC0f/DeXY8+f5vuko7g
HYX07fjDn0hXn/B+Rb7zMzjabt0uUS2IcGskfK+zzyxvdq8slLJELowLVfu/HYR68wECQPeHRGjU
IRE6RReqgWHy8s4aNU8ufifg1i39XkAOvBbo2T8psRMvbgtmfAvFTHNMLUYkA8jNSNfP97+MhPvO
PriD226xnGTHIp+qJOvFC0I3s5zYHhszf2UCZcRrQE72rrIRLvzaCDWffUOFVrfcrdnWFj5/+tnL
PG0IftwW7XQiRolv74X35J26f+vzjaZzDksvzDyhUu2txIwUupvcOVb3pKdYE0OkzUSC4tpf/y5n
4jiZ3OTp/yaFn+Ayq7bhgiw1suEF5JU3F/0g7OvI0cXEX0BwahmEflU5b17rQ1QR9eqjoiuwDXPw
I7zM1TWKp25Yd8HJAu4z+PfTmUHPjvdJQ/EsrTDnXmDRTjTquOWo5O6h068lIl5MbBzibL1DCB4J
PU4VaUJyD4eg8Rgt7yzVkm7YsVAKULz+yuWvH8nO5r9blpiZwZKzvNttJ3513S/BGmzkFepZjAjV
P8Gu1E7v/Ry5+deghneM1ghcjREwa27v6TUltuzQobK05lP7l+8HjErEBv4wAuLrKU0pm8+wFPoq
xg6VzJlEuzsZ8Z5TPbkAq+T/7hmXZimOIStzAaHV9RCWbq92LZbL+hi9epho7k7uZ9Kcdy24HPNk
x+byRhL56o36JF5L7P7dCeKe4v+SRJpdoONHY+JKCEs+qrL5L/CQDPrmWSiQYQvjmAPSNgbSuVb0
cpKj48M4A1jtgbCgDSYo9KFPGhLfbafW7c2jN13XAqFko2ki/+8vRat2OWlgugp5UU0LYc6chczB
81L+3swI9getHBZdghZaBrp78HDmT59zpv+efmzJYasdYdNGmtPQHAf9lsPGzGku3fOdW5zfnamB
oAiTPEO9OMheNxK168tVLwlLQNd/ehXBKbXxe5E/lZUREzioOYkyxKFxFckfEuFapi2mQZCSpM5l
sTsBS8II443vOBf/+/ihYXQ39Mi0d4Fe/KGIq6zm5ADkF0jKLP1TIi2xcYIgbygCTn+Zqjqk1/BE
qA9MUfuOjL8V8c2KfTGcSKD5kBfX8+y1GJN800ZgcOuHI+C+I0qw6ZihMRClSOtYukYCnEh2zKBj
7/KAOG41E2j331q3Cxyr8DYkSA4OdZMkBiu+R8BOSILjSUzapVKDSVWVzS7WM1wEMAwDMrn0O7Y+
gPjmoZ/aCtntxpbVos0DrFo48Xv5IGt7l9A6ShI9J7nHMX3g9VrkYKpWyf6m948a1xyuTr7d9fN4
uC2HHusx2H1pZQ+s51w/J5r1D7N7/jwLfUnYPWVAtb6pElFqNhTFFk8KYLVaeZEXW62w3muRG4WZ
2tSIeumBBR0ItESR3bq0kuM51KiavYzWYFOYlhsgqfBr6r8yBjyADnoioXlr7vBqMrqpFc9ZaDxF
cMo7JoNMSah5amjlFQ617nBG4sKd1+6vXwu3TO3XfbwNDbW/LZdhyKflp1DpJk5vo+RaU8NKJMR6
yj5c+OigWtAkpkrwEHoUsvMbHrmIkadz7d26SyGpYpcfWxtJT2mrhHx3nU72e2V7ppOOiGVK4xlJ
4kSTiknFX9bgN7Ddn1a5h6Ir8oSZprxlIImHQHaqBPoffMZ7w96S6I2/IFb0XqixmvQlASLAZ+bV
5VNXoBiKvVeOvqDA2Ed2w/GvRcE7Dn+Fy/SanlIzSi4Veqb+Rg479m3SibkL5DU4BZ/2ETUoT/Yu
KRVHE5hIQRuUv/7CrXvkHU1iThighg061Cx+D5cADhGyesO4WszzcFO8qcSQxE3B9i+bnYU8zs+r
xiSJs4KiWfI4BHbrITKSVtg1/rxy638g2Y0gBPUb3uBcrt8XnibfMpJLKCd0e6++oPnl8acThl8v
gHuNulx8pfb7zRkkLxcB7LJRhqCG5k4+n0ogbQXr1A3Rm1HRkwuBAGupeehrYi9sXhAZlXAqoDUX
RxZHkkAHkicTzUA/ourd5sZQjovT3/RY2HEpYKwcw+qNtvQD291afiXKtDsygjejvRmbSsiAQorq
Or4ozA66PUwiJKiRIW5DrodQ9p5inkJA6hh/Zwa3yQSkEgWGQydI4HGXq6T2G+q0nyiTIBS5reF+
lu/rts9aJMOlk4L12rq1VzK+wkro3lwluLs663S+w+L5AmQNGPddVnAFlPTA7qfsZbkfmyWH24tr
bdAvNzQsi6s2GqRO1KSdScbhKdtGu3VYFj+wyypofRF05zg8NOCrhltU+iTQf14TI5zz49I9QZKU
QBVTCd+r+++WcteY6E+ysVrTyqC4jPD32c8TBfE82oBPfrmyagzR1P7ybp9B5wuOguCgborm+b35
Enlm/wKIXrrwQLt2CWWQf3CJVcYDMaJarjzZi18efYaCSV6CIUhgi5Uxc2vPWvb6lqHBGEOaG6BT
NjhIzf/EoGpL2VvMy13FYZ+wrDM9SrURq4pXF/FbJfIxRNHYipJabm1H9c34kbXr/HFzmos+mZbX
5K1T3r6zFkL80UWRH/Ssdo4n/rF6FjT4PTiCcoakve9v9FOvINfdxiZo/lBcsVe2saQgmGxebHiq
yYZIiod3DKUWgvkET0W5baJShdN9lQOsN0SAy6phhlVatewz8kZ+iyXAudeCZ7lSn/ZU5j3uqb+L
4rtTfyNU18RDFm2ezJl9/hrFEdOm7oOXB4j9cXzfBv740ybVGNkmvDmQZDUafsQgRlymoUIDtX4J
DLBesX/gNmKkfAJrl06ZOtstwBPqetmrknsPnmFEz+yOUqnekxk+UfQxSA4Q/r7HT4iFJEjk3+TU
oFsbRzPXzMMU6dxKPWF+3+sRuuAztKe4Ho8/IrrcTSEMnulfUGjdOpXMOol+K88ZEDEHL1udwjFB
6k0JPDSEAQQjoEF9XphBMnpRYprxVuIaJuIdW3x1X8zqAYVnVACPVQ/VxwKkru2HAHCN70fMyE4o
r6mnex8S59lqXZuyG67f9A9b7z5jRWBNOif8YB4J9k+69n6Z7jSF57Qak9UBpPdbikUMTCn7RBlU
YXbjGx6RKaCNvFFJP9nfLYfaEBwpoztqtp+x/2osO/BhnFks0bNQmi5s+di/akSaDbLpV2Knsra5
7elrtVxTu+tp+tAF0CsDC1+8ouOAFBIKnGV9nxsrZvOw44RlWOrNsghtQgFh1tBYcoT+xujltriN
y1W+OaR7MjQeSS+XPPVFSL/0MKeTeu8ETsacNJzzbY8M4yxKbDXZhexDmDIE0+Q5QMZXpzPGFjZ4
Xy+Q8+uu01/5gqWI/BgLp3+OyKeBfkezIXxe7vjZ9QuINyKRKxzLOzb3+uYa8gd81Jo3VbGNkJ4C
vdwD6OxBRvy1+PRA+zlIsAYcxD0xjezONfA+Srsbbbf3Ii3podQnRlJLBoZIZql45nWcRr3MvUqM
onSq3JFGVM6afcSROuiz9v+0MOzfqrJX1Y87b/IIvESSbJvdSuZUOV7Ioi3G+KqCn+DfTBQ0h6Uf
B4id8RtWmZuWJtQOikExDBqY+guXyDjs+8oGcAAvGlToH3q6AQn8aHkN0qFgMmkCqHTjd1AI8iuz
eFV5sg3pJwFO26am0GIf+86Y/+7DgPUueDPRZRIZxFGVWp8OfIhKRcYOYt/Gy3Idcoie3m0a87i7
WmbtmBnfd4j1WXpT4X649XleHIq16YoHCXda78m871jHEMFx9QNqMYgXn62D8FyvAocdmvE0eyCJ
oB1Ub8B+4xa4RUK3IqbbSap/Eom/wef+QOh2aS34dElAJs+QyUZLcWSS/bCzQFqdRkWSmOS/39EK
LCD4GxXWf1ypGZEmSt0lPMnvhw0myrbrpBOkleVKnVj5Nu23QzTw+BdOhRSGi3vfQIg3Dn5fKPnO
WeQJjG/ELsaIcukNzO1sHDBmenQa+Wf+Ey+ZStSW2lz8EoDreia/p2T3JcEVNROzTxAeeDRgXi8i
PylTpnG41nU9YhukkOcGTppdye1p/tqIUJ5zoOpADVGtlpUST6ZwvKzsBuNM1ERwpCIFZDGM+wZK
cVVCINseRCzryIjqlXuumDX61B785Ds9pZ80jj5pv8ouWfvvhVm7DeXotgx/niqziZ4RWZYPPbHh
w7i+KuB4KZYqEzjhDy0C0OoUqpBdVSYosEIqkBeIBY8Bf8VdF8NiAar4P4PYvLOECTl9590myL8F
qFZ9vOhm8mJvmkEIHUaNQcKt6mArIuUlypdpC/oJtQfiTdBNCnxWpOjzKG9j23wAum5ewbpoh2iK
FeGlyXZfQXyL472y4iWHFAPjrak6mQlVslfWKhr1IKOptJaSRn1h9YqOLdcs2ASN5KtMUr8T8k+2
WpON9kowtr3WBOuegRw+DTk2RG/NSdC1OsqglQDxelzyyeatO7oxllAHfzacPfAHOXyPeTjN7M09
POiwLAfegZNaLNAzJ97v3kczuA2Ff4nFs9boI0EkDRX1OzfDdY/VGO59upWV2lsj15A2P6/ETrxL
F61+zciX4WsqhCHpaFX/Hp06JjEz4ZIcxY76sDVkeZCkiv0IuvheOAstpQ5nUt2L3au/CaRScH3V
dfp23XsyoZYT4PEMh0w2gHNsXQtWmxiXkJaQTfQznaTXtVTU7laaoSBLGsPd7Qip5Ey9yjOyG+l1
turC3zif3N+W05inJ9zxcRhrvaWcGol4WOtSQNaIJRbzXWRtG7v77zGCVQRtQU9VILZz6s9ix6NS
ss+v5BPErIm5IOQgZd8ZhZ4mIjjFDprOu0854lSIxOl8gCUuEcH/f7L8zkdTMMgaiIIyeSJrzN7G
P7VVrdCJSxV/uiBC5XfqUI3C+mhLw6pHJWomd75PS4yYDV2RP0emJwLo1osUjh/caiNZuGJK3+pM
r3t6Ley1W6V5v0cgY35M17oHe981yqGu0ftVURDz6Euch/4uhLJsfGxsFM9/+QN6x5KwePSxb8e4
PRnCf5fzthsj1i0o3iUwFmRtiuqnYuGdz5KexL9/GO7dZZ2vltD2gYpXlpIQoo9YWaD2karePE7p
knes9D5/mJofjnNI40lScxu7g1PsMZlTlOcsify3wsMTHbTYTueHOqCXDj4J2k1gdczCx4CoRBDr
z4qFIXUzODmwdqBZqmQPANIiZa93oWyVzZjFgm0inrPh2lfs2pz+Opl/eCabguck6WlrfWo60JgG
R9q4HQhyghyjf7NbBlRz75yWlC+AcDf2B+kAXMgFhnrN3jpe/Iw1y34r+bAyfcUqU/FLx8lx4Zmv
S/Q+FdYjqXmR/shUgojnxJySJttfgT64VOfj/dc7OqST3aNMLBP35/FNYIxjILyYDTgGVRwDfnLt
X7WcFMrxncR5BaEjeQG1UzCuqyadqn9CJSDMWs+nOZW/LF9aLK6JUGImkayAHs7XFXbCXwIPDSBS
5tiltGwBTHWFr0Bxs8cjf+yhEhqleeDrLgTjKaqONo4uQrvDqL2ObwWLZN2Hcexrm8tVquJIukDq
OevI8qO1PeGJrpvj0dqJN6sA1l3Wq/A2nbZmnxlBMnGuN+cVZzmn206AVvjqMFafwBQ+gdCkxS8l
XosDvzNfPZcp3GO+otmEu9oOUmUulp6FtmNirmjhcwLfcF8fvAzLex4eM0X0y+lwSdF6c4Djlta0
u3jHqg6jweQ+UVOwgSq2HRUGrEidUl1yGCWaFQboqZPRQHOcMPG+UPPswhpj44tIB/h7Ocs1yV4r
FZ5dO6tGb3SyG9CcBtn5MKfrBXbNglq3PTUDvMUlFX2t8QaPKs07IS7naP3mJUZg3Dzo1RMfm1j7
pDzmsXliFCvAVvBwVfyCxXDQSVzF6DQlCcl6Szd5VXNKIz0qTDENz1pasTlfMQOJl4xYuHEHASJD
jLmlGEm0qyu1mYyVCbHZL1OpogbsTrjT8s+J7GzYlcW2JKXLv6lJsJ/F1oyNkbgRIHfLTT5cU2Ia
3mybxf2XFzqyyeTozkzqry64lCjiwb/AjUuhTjuZIy9Eoc1J+oe7Qghswb3koHRh9aVOu+cCcE7v
WyQBvCirk7QuKK0v1G2ET93Fco8hvsf/+xrtovfBdk1aAa7sTw5eD5MN311Z7ZymirdBUr0ro0eu
WPfJ5wMxxhfHP8+qeoT+084g8WKsNayaQDPWLo8AifDpKHtHX22tiRWDq6fycpoASg1PgRAqIuS/
CZwR79V+5gihBf2bqfHm+KMuPZjIb5ldj2rzl1wpZ2oHXvRPsySWSXQaheJItOqSkM31BcnB/GwE
yPLXZo1CXcC55koG2Jz7MtZ9YdPAfGyPn3NK+DDs5V4SNVYdybiJIdAg6pBXQ+CQJsK2ktrjdqfr
2yXCA/cD/5yNWoqISBwtEDu3nkTTb5Zv210G3ldCkYW+2pRplNbp6blyo1TNMuqEBgPoQIHk2woJ
ZULxJv7W/GurWTzgVZcHAkFP2WndJKkOFmXrz0AKKGynpgMXYSLczB39Z4ntWRaiLvpBvroADJgE
DYqnyko7THnI5VQ/krgBRA1SaCW8+vmiIVh6LRVq9PufpMf3LZnileerhfS+qQzcgVRxry1clfxj
bVfjJQbNDEzGcbNKvnuKDOc7Hi5R99r58H5fGH434M2snmrz9JKGlv9f1RFijaopfoyvmVoy0RcB
Janxhjs9AeI8YneQJDyU65AyUUvDMzIRBfd8k8F/5t+YpKJSdojTh40mdIy3iniVdGSsPJ00moM5
vEjR+SbNkQZV5POMQtlboRvS9H7sQpYhSF+W/w0iuOEoRnecUijHKsLB/yQVoC5bbEzybU1buE1M
+MUedBqhz/2XLRWKz/Uz5aPLmF3VyPE/2vdzOYWCVJH27q5jDXH4Zw3LA76jJSV4+gHAfBFZitqe
mL7xFjmwrpklrNCXuBjuuiyyXP2L4s/rimp3vPzvJp6vQWxauVNFp73mnuym8bmU1D3DQguZG3LQ
9kVfogczUMw0KScfgoZ2T8AM4tPIl+3d5voJacIPv+gt7R2y77rNXoEbSDOaue68H7b23cqGHMoV
GimO4QmNIfLoaEi8fnImkqg0s6vvS+7bRKgVaJN/X3CgFhuSS7ZHBcy8+lKjZPh1LwdoPeGZNcDe
tWKvV/NyWmFXMchnWOBGakEimcZH2kFe1FRxc2CYI0zfAG8qePFGeGOn4LIdjKN13ZV6BAh+tCgN
odQTFVqT7IHHmIwV41WLYV6vjVjzqWOmOWDSuE5+t2VXpzJfe0Z4oyuyHJaR7KenH+o6YNTa/UHz
x8/hKSW0rO3wPaE0PaA3Ch7B121ozlrTogwsQ63vthPnqKoNpzJ4hUK9u6PN6JpyHqZbbXin41nh
TUHQbPkZUA4ydQwpgWlzedQRdKj6pzHbUGUupfD/BmAIcJfz5kCU7MiqH5Ra0Y1LsxJUZMMAC1K3
d2dhBpkVUhHmGmodMMfRo5eI4cK1+5CPkk+gfbX7exCgkePDobHgnVJIWff6jy14xij+J46XLK5/
WB6QtdI+gSNDlX1JirnWHXjxQQV9vIoSdlNDRGLwZONzN2PpjJZEyRq91w2N695h9iwDE94o7guL
1Y2kTk7C6eSCsBvHVDUu6r2ndmKEobdMBqio3rKUn2r3PhJw3pev6lItnmAyLlGd9QkQidIyDUIA
07RQHIo4cL7U4s8+RlNaE/vKTOd2V/bemZUEVjUqElmDu8U9dMdiwpcdaO/4O0QrrrGdueCf7pyO
tpk1kYFOQ1YdE+/U6Y/BwNmQPWhHYGHlYgRppmGKc9QvSfs5w21w4J10zUxmRLB1wFn/K0+sMxOs
2+ZUHqHJP95HM0VgUFaRTBk8FplUEm0hbOGkbE+5dUWUJG0lqWGV+UTq5jZqFzIQZOC7uC+u0cKf
ASc+o2FgSPKhlPSs4jjV6lw5b82/G7WsS71poFn3iNa4skx8fObM7Ju6KKFIOC1bqnYBuuJpEfmz
Ngh+B6iKn+XahpCY2G0J3Ur8iYTzlz3iSVv5gwiBUIQSOZpOQP1jkEuL55jBkNza0GVlD5KazeBh
lMFnc1yW7DiaRuQ8oDxCY0tn+JWncaQVsuW24VkZskgPBjgZS2csBixV2Ufj9Y2TRqXbnfh/acKL
eD8QXGcvMa1YnvuEGuwoten7Idp6vxaspmrGbQuJBintqmp/t4ZbVdaAtJDa+4JgEBbyjkL2tpId
Yxj0ecZmp8aLxW/i3YRQEljgQw6TF3lAa7OK9P6PSp9LlUTt/CrWmurrByMAY9aLNv2Z73V8C0uN
TAeGJU1B/bTGgRkHY1Uomy5VKhdj3enGkO/lX7EDgxTrTwPSowT4qn8XIu3vmVn3cC2E7zvdHJHI
it/EUBhwrcrh7/XMTvdM4GScKChUG7PI3CVspwx0ScRIMt2KH7b2MeSuZgkbFQdgzwCenm/fMaU+
79gaw+M5sNXX1tMxdlNj9SblcLQVmvicLQwRZ0f10lgsE53YbHlAKwvYsalgYdGMVHscnaY8MR8n
y50PgNpczMgfqakmwG/FfRWVwk7tRNLK7gns4lv9TmQw1m2w/nt5Nw++RyGwqbLp36JJpa9MmV1Q
n3RbfVkZ0daMGje4sJBT7Fiqo0desTO1xkXppzwK1bV4DaTsh+0XWeJvbPGTh/VvraSA3snKt4Ax
KJqs9Iamgc4D2isvth3QUon1IU2IrMyBauD9MkCk22yyqWvicMeriz8lC2VtJKEXbs25S9hOX8gC
+2hoE1ocMDqT8DIasR0/Iw7h5vxJNqt67kaSPF4H+Y5UIqLC6/hsDp/WMs1QDRZXcjskePztNi0s
NvmZvZSAlgLgZHSpkh2I6p81RRFgZdTJvfpmud06bf3I4zF0eL1EbQNbrtZvG5fiwSFi7Z8uq0/A
fdkhbyVflSs8/GQjz1gaxUVi2yyHQHrK+EkUY/0YvQvy2hMP2LeqLD6pDzqHxWg8mGqfg8xVXbVu
xTm0Ubk8sE56c9phMNgdR3GhtEvu5GeIyp8afVlvoxhYjrmcLU/RhMaHeUo46Z1TqRU3mrQhKCDn
TQksv9bzFJmZ9qNFT5lqGJN8gcaalhgDu7/WhcT0GPtE/SQXf204q/UdhBIbaRvUhZyavUO9X+5k
ZIKrfi49jtkmGKg3mDaQHpm4437xe10cB+juGSwkzYOIHWl5bsODf61HfFODqRpjt8WpmjXwW+IP
H3403endnp3of6Q0ZFmff2n0Udf6NbX+J8CQgf2S/vH922mw3MWIxhIdiPtBRBbtadAR0ZsZgaU1
L4n/ZCYwsW9Yl0jZukdAj/l3cCyR6OKf/kRp+RfD/lM9r4/69wv6GDJu8JX4/2RFGF4xTpchiBfx
aZK6SXtbwLuy6aihftmJgXXj6GOeLQdEyOuxgYoqdb5WvCr/cCvnRWXwV6+xs1Mp8KGDFmWOEmNg
6Nolkwxw/21T6bv98E9KUM1zInbjnNlnb3rNwfJtMCiVUSugxZgNixLClPIIxuy/aKrQiv48hUGk
dbYYb0dzvawmHzlAvFUkqVsFsGvVOhqHgNjBgCJQTGpNtDuJyBg7HIsVMoJzUeJ3yxKWqXvve9ip
XTT/kPqudBtcX4afdYaQrgBrO3LikZgRfFM5kEgWU/l6Up/21y3frY380VTNiPbRV8eJ2AV9Tl21
nvN4SjVtTY32u7JWFZTrjGdO8dttx4Id2KQWVXAwvQ02iHbO1UAM3PTMgh02BLm5kIcxBBymFNh8
eV71TgDRcXU/4yVXSBkWaPgQEaZaYmWIvu3PdfobK/DPTQ0OJUFCVRCVZvC640kyzOqpSdfOwPMR
pYMkZXjbj2ZArw1Ps7XR2FW0jPCkBgsfMwmmdKMmQL2tkt6Aq0DAa3Pz+Zwcf7ZAGiyI3pJ9TFDT
jJ6aL1vBFN/lraFp3/qocg6abWxIOv650dGDrq3938Qvu+CoA/CTZzVyNLUDf15Agpr57EtW13w2
v0zdWtK98MGjJ1LljLBjcHx0NeQZCjTpN6prrUAoXCaRUMm61YF7JEIiL2YMZtY4wvvtfy2C9Os3
V/SSp5WlT53rjAs0w5NU8dIWC4Vk1Q+vuOx4fGKNxAuSnF64QR0l/PEFr5c7fl6OYs2SJppjmEvk
8pUq3FE+C3DmUbktVtULsCzEMkmI8EAanTWORrtwDlwEWX07qhxF+W0qTPn8fQOZRnyrs9ISjJG0
YdZt8h+DCpNEVLbiOvXzG2NvQlObmJY5c66thxxdX4mhatEkjxG/RkvEN5zQuZYILWBIRtxu1b//
oXiJZqQvLzW+icH2wq/auYu1Dy5wdTUY48AQGSqme3vvgS0J6mV0ngporZkxR4eDK9LfC/+yZT48
Utty7qcVkCyOkE78EW085Z5QihvvWyN+FEkHwH3j+v2zmLqGgGG7AbCafobumAuKeZxcVhzjo9tC
rJKpzx6HhZHLcT0bhPJjNBop2pWN1K5Ekvyffx72A4PfvO+MPeL/9eqld9Z47V5ktKKrWQDxXtnN
DFIAIRXAvxs/6MkK7XvlSJ4XUcwHOa8e58xZDyUd2r/MdfTrPpBxmXol/FLxUkQiA2plVGUdGUCy
I3gpEB/TBMzFZjf4Hkr7yU1e8wCLy8Y1TmpyLJtTaCKaqhoDDkHZS0T0UE38+gv5CfzZ4y1HzqW9
9AJauFKQxEUXg8q8m5q/x1glRHCSIefJjQ966W7NCwT0+PwYjHLulJNHZXSdWHOlpfS94BcjwrO5
iCp7Npjv/mmWBTQ+OKZ5Fr8xXw06pCTx3PaeRZwQedrSBB0rdvbr5+ZpjOa2plabUPRPWhv4HHhK
r8LVB++iy4kdkSdw3mMANE8ZaTw9k2CA4QMadLliNeyK0Jm7nJLNT3fU2cC96/Qaw15rVWq7/er9
yp/DtozKVuXdPKCvgSCk5h2EUVS86cCAorLT1rX2XmV9Ge9QTIzgiqmudJRARQCvfnpUi1HEta4t
HKYNVcAjLPfFllBH7WB/lhtU5C4Y+nH4yVE+Ue4Rc/yPIBYqBTBvGPOSXbhLsDYgIBqI2Ig+T3vI
8NJ9p7x9FTVLrwAKphLxWNmwxdjScpAGm/+YPrY+a8wwAdRbCifT8WaY05FS+yqDRzpLORRmntfS
rv+SlLTNtcuv4vysHCgiH03FzO62pkI9KYQ+SRUHfvO7Ib/Ew3iM+1x4lshICwHn2DoRtdMVedsM
tUdhp0eyr04O342v13hkJpj+wrSnaFVEzfrlUs+j/l9e8qqZB39p/7rT41OwdsdoiAeK8qZo9if3
W2zVuMWjGj5aCAJlqLt4z+B1pYl1GTJYwKtaFTDO7WuWXPnNLtmBWnJs1GOUN/pNFarg8YQb/I2/
7DKJXxl1y2Xkn4IeR/YerPkOTUb+rX5VLmEMfEolhr3+9tNYgah1VUCT9MoLfn4nF5bsIm1I7vRH
Hb41wlTyOlZLgdABCjaWKTIk3IOVy1LXCYaU6d71zS93i/VIdfu8Mi5rK0rjBywyICJRos8qbxAp
o8OB2U9B5Ls+B359Oy3bydOeUfwuCT8PBitHxI0lGjeYKvvSbcGnkBrOX/lHX2fYO63RurP46oJn
cbDfDA5qGEYebknl+bs+oE/3u/XVKcv9vvsRcdtLaTyTGa1s7GOH7qJFFshsOkuhQ/wsBD2W33fO
+AntIMMECMCKz9dwI8c/Z2S+auUcKo/fhhSIOj93t4zoTyi2DZzF/XZhQcfFepyucedQEZdNk/Ez
o3jwVsxcp/3x5Uhn/Mqum7WREmEeHW2AwrGejS+bQa4aC8AvY3ApUj3PRvTFrY55y8ToP63WtLuG
3HjF/zATASm9nC5TNHV4yJe1gpUiR9FxDLwcc9pN5eQ+E74O6gw2tJogT5zU2Q2fW//nsmUZFOKs
/F0myrUxzPBH4ptzd0itNmV8kccsaxYc+TIHeNG01SY/+qpjyEQakM1SA3cDt08oG/N+YGO0R4Im
cx2G9/vnUdR/qNo0ELvrPlL03y01CBMfsDVhxDlDozW8XpUlV4IGBJV9CFFMBrA96FR6KLJVlr0X
p5QVS1fEhjnLttFw9MlMsmlToV7Iq7k5vpvTaX+eoZwMGP4n+jxAlzITBVTHCO5+rV6bR1Jt9E70
ZM3zcJkAsn4/e+l+x8+lK/Ti6Cgg06pIv8jpH4Vb6X1Gl96nO/hwY3tAlfmvOkZRT3fL1vyOm8Gt
efoJlNIw8dW+govnrgy6SyhCavj7hLlxHnCTQDc94bfA8sV9KQI/1Q+JXGgFpo41BWU+Loce1sj+
xNPtXdHD/E3KFkuwuynETijGjJ9hmtBf2kVlUG9E1207JIOOvjRDz8X8JCXdTP8vvQDSQ7HkvOPh
HKpxrqJI2AAh/T6Xoi4iQNvb/ZXURYARfvFvbnKGTujUGoPMiWdmSOlGFOL5v6SfVzYiu0S5PJlg
cVmFeLYof0NKYp8iDPkxETOyLvhjyu44WIIuiw1iaagcO5vUImV1z2Au4vVOU5bGMSfhvGxAYA5Z
GbAD58KAkuiDRRNuaRMmH1GUbCSXzSBFvPiSwko2z3v7muWWc3CmWojj/Ksa1GjgVMYhzXcuP/c/
dgN7woioqEqeQn96a08+yCjqaqZIz1NNrThaYTk0AEiRmVnyOP0Kj2kWO1zRRKd9U620yD40xk3w
6ubGDchSjST94UTmfQNQB5qJfmUuJlMQB4Dia34n3D2GNdOCFIG/qNBXnfCE/0LpmjPFPjiIPtvX
Uh9TZP7mnLfYrTSCAjDAgHdVb1MHFNKXNzwCfYcdOaCIdO493vyMC51km1ZPqc1qcAk3Eh5xucy7
lel1yQHwJ0Tj8j35IrsCXvX11f90KDKUPr9gda6Eq5LS7XwMeGouTrlSAJKr2xqYm/OhR8ft4AwC
Xx8X3G/IIe/X6znxS+XTfoscq8rlMg6F/KgiXrs33oR5i+KNTSFBfDvox0XUCR+oTT0BwM+wxZ/M
WtU1DkEjkCUYbuzCcTDZQz3fy+lKoaDqCGeEJ2yDbyw1QD46YP/PxU+dwH1LDsRqLIKgRewFYgvA
7VazC3/0AuppzBW4jPEFI8TVfZbygfFpMoW5QWjTBFwG4oZRxaYN/rqmnJPggHCY2yQirv9iwOLg
i8wdqJrytQj6EI0/SY9KmQLbpeSFCjEMk5sewh3aAEKpxtsgj9FyiowP3OGS7EpXbTJBq0sxna1K
T5VU2JTXdtnpPY6kGpPMlm0ZBA0JkCtLLnQaEc+RUoTVciCOumgZ4671g5/dy8ykFz4IG+o7GnCz
9AKErny/uriPfA63D71LEuS5lzVxmGAgZXgrumlP0T382TA5Ph6Y/4epX7KtHb5S44nEApEQ0Gr7
r58PbxhWSjvmQq2LqXCmV+hRX9Oty0Klgz8iojmgT0NPHDygpLKd8GVe8iQ4U3m0p+Pxhy9H13YV
JzBzFXs+NEaXKa0lC4svCQndbH78MJ6/o6eH2kanSck/W7A3bam/tYR8/NhHBMW+gD49VbgJbHgM
K+P5I2clQCjaA7dFjA14PXroCj2CVv5/oCCopBoAMvydP1vKWQuiu6AjjieuCCP6Q1dzABMZvZXa
QnWqweYlZ0Zhj1T4kGgGCvEFIn5UNqvoZNFpqJgxwUpsCg7CUKYnPy9j4+9m8ALhmvZEhtFGnYPX
LOSEa+eBuFJ0a6RwpHsyRyDU7jVTZaA7lK+piQqzegFQxbszG9qoPFJZrhoQFFmxUzSiqHlyDrRQ
jaFZ3WH8Qmd2pRkMMR0mFHco7YZlq96I6v4hZ8hbv5fkPQ7jp9bngQTSAQ+vjUI2ndzABFpWsMoq
vbqTf4ifB27dxG1sV9hFIl1bpJJUu/FvZHxqJwjISLRRYcsYdjv2hCSVZBjgwjMpJt0VfZZVKvsa
XtozEZQnyHTn6vHafd+Sw1n6nkK05BZMeJQppYzzxdsPc3MRJnqIDksGChL8LAze0HOfUrE2B0xi
GvcaD4s+ZlLnl1H7TmDlGsqHjzTniRIr8cW+WEiKwpXKe1dpelbvS/kIa/iXgbMpW2lhAXQYY1s8
OWPKw4MJU8eB+Kn00F6O2Ce7ybeEpL0GFy1EpX4WFhUpFHaz7Yz//wXbP0G62rGQLz3avBAPD1F3
bYDV5md9PgTpSdH/3ZMzS+AoQf+s88GNDineY3L3em4JSg/jXnxUZ9nVhni3uUXioKj1dFtY/vHw
sW0Nuj+pdm6t8btOCtTTFod/eiWyJUrGv6tMfUg5EGUcVC0SipUIT+n8yHee/sTq8ixwd5GVkVl1
6J42yPMN930OY1RsVOSKV0EH5d/mneUnGWD3lh9nfbECE4cs7X7UbiHmSkpTNfZVzShABsBneLWI
mEc/15e8P1QC2/+0Rj3OfVdNGpMvndjVQiEuCqkk+XUYGHpn3yTD3dm1x+q/BfXi5P079EpBDlJk
b7SerVy9N6QFEEvgnHCdml4zXcGoGH+fnM/XyKIkCYffhZoOXbRQL+0wva8Wy5ZqvBs5+sgFu7PB
7OrrAoZqtnXbNPDW7EfkbhuUCUEjK8iF+OdZxgjXcW3KschNj19ygnBDaVGulLO8m3HkmJ4lK0h7
hnUG2/C3n1vgRnHJVSZK9oeipwPkSjo1uqFu8SFkXggOBlqqCyq3Mrg2Sgb/mkh3KmHSKpDVsxia
aVGnvlBCmxdvb69CZVZt0EU41zfDXXY8+Z3D2E2xt/wl3uYXIBpbzzX6uNiTqQ4+CgI0Ndn+NvLg
8jrC4jZUeChgpf/0Lg+JbFmWSpwPQwTNJUNObB4R8YLIwhCoybpDvBoQ1uSCytlG2CxvqZHkbeXJ
t5BuJvkGeRCLLu/lvkgyW7Yj4g47SXpG9d1kwjwTWGmudhhdxo9XPT26WAq3WbL6C4j0UW0WxiiX
QypX1pyM4VNtbmhJH444/uNjpWf4a0VV3+rl3FQmoWKEw3lunjQWHOxP6Z5R8tn8+6cUELYqHIFx
WXi5aUgOYYcWtEESu89bakhlq5GuQHiEJrFRzgB7u0YP2saIPZYWVMvN94vk7wkQsqhBeyu2vMOp
uU+0BkOfJl/Q5z4NbotBpLHCFLeiR+6qOulrIJUvP6obq6qcohg1xAOhD0ohL1VU0UM8spqedoUC
aP1PJC3Ze7X1gw8o+v8TzZCy/JDAjYWioMbOT6xyvAmFvUfNbGOF0d+UOE0v8WvcX0n9bLV4CASg
NJy85qBDI2OoEvlJHHCuGHmAM/g4yWualGJjq00IMl7pNj8mcx4SYo8YPngkuggGYnzqq8FOzO1V
bXUKUMhzMv/IKsw5er74VldlEd7gBQ9nm73XJGIhNNARBmddh5rWcycV1iO1TiXpSxZGnjrmWX8V
fLFsUtBj6SvGYhlsb2+wDBCybMThqdzS4b4k5ANKq9L+O0PjmvS3SluebjN97PCQbDNvpH1z/wBg
Fn/Ekv+4S0tmrRL1f0Rs+wcAfBbg7Wv4rUSPf93WqY/hjL/0/cLKJwkWgqOzDehp7Q85gTNMn3EO
Aed7mtzJ4T/PYMvA7aqdval/DQBAKhP4/SrS3RJh2z0pwM1zAA710JcQyzJK7LfiYqctUS5jP7Gh
xMvrwFSYeoICXTg2jWk6CTOPMgYYutLAGyQ3K3Sy/yHQpNifda+n6yH1dmcPCwbCwAouIXfyMJDg
ZHhCXRWKZAroCpGBgdVKHabP1LSfo6JqyWadyYCe4dGkdKWwHgGp7SeZBWvLVIFBLGNm+Q7Tdtkg
UlVafgGhoa2YYdTifLAacxSpCShs8yttbEvc4EhOxi1onr5ygqtoEhGb8sz95tzVVymPNxIGFu5F
P4LQiH0/YtlLSGl8+cDfg2Xd2wWhRJPZYlhfRCfBfJOVYVkoVedu3s660/Ta+bMme49zGFCQy/WR
l0/HIPhfsgbhi8ns5gbioeXolWel9aRcJ7RvjV15KzuKeMvMrudtKmqRJ+U0dEQuDdBaEJEp1oVw
23oF/oCUs9E4AZUB+C8kDDfJrOPG8RbYLWBU2ke3V/Mf1zvNbtat23Qqbb7hFWSqIeVv90j+grOp
lkSPYTLKGcaReFNGBv+wgtar7dkCL2d5araNZjfph9yWydrMyeHS+Vcdnk78lxEpeZV89loF04ld
089Z75kxZZipoZ/q6xwGM6BzqqNVM+YLlH8mY0XCp5N2xPgMZRfsv76M8e7yo8J4kqUJ+D0y0j8M
LXj6o/zXoIpRWLJAsE3FDviMN2rQHo8Vm9rfJLGnvugTXYR7nChp2yOszhzBBJ6rAPux5oQmG+iu
rDbFeEdl4nRDzy5MAVRVYFRu8SF9qfhrm+dMEK5tJjtuHb7ty4WNwEZm1TCvT+kuFbD/6P9/1EUE
KQo8vyBHMXUK6El/24lDhvlsGtu3fcxFEIsRzeQ9IRDGU0XKiUxGtoeULTrF1lPNcCoDA0YwXpAW
wqowEH7B8xRuIERYwcSKPmW4BsWFT1kb7iYlrzbJE8BLQLU+rJIsQGQX+OTJvLF+MYHKD+W3zD2W
L+RnzM9UuS+9zNILZ1ndRNKk4itt7oYJN4qN8EdZPTD3ID9Jq8uKE/6BacNrZEyrR3b8uEEMIhUO
tV/S4FcHsGOU4BLRDC0QBoqqKyY66YqvARzL4+NwVc1fgB1ZUMvxPtsPqfc7Tqwr1PVW6nEiNTqj
IrrqAxazr2HaFa+bGyqqyPJThx/Zhx6JVo9dW4yw1s/bOPBc6Pzr5UfXbCmGdn/pLE7Bq6sl2Gxt
Wh6X4ASOW8WKzFtZKcwQ1BflH1PltK/vdlxacAsQfBqBBU966WE4tLexQNUXUe+eH4eRYOpKgfts
dA6WnKtF6mtX8IrIP4G59g7FuzfZK/Aj0YQ483MrlWdF5AEjT+sMJkeSUkhLqzWsac9vlt35v1L7
VUcnlQVqIK0851iv346efA5vx51kGaqV+uk2J5TsgPHGAqbS2iArEddohgCTwUVVe8iZcF+dCQhf
NkTeE1GsTD8/4MDlR7qPSekhMkIFNNYmqBhLgtECMhXeSjI1stTwYJf5gVLsD2FXtXdiiYF7p3ti
laKUuczafZtNzCtTRAjG3F/acyMBm0Trb5vkuZ4WCJRy63ybQUqEZ39O8Ga8aDhgxS+koJwkuUTW
NiFcrAj0KRz5FWFFyccbZSiZi8TYUmdNVm1dhQaT3Od+daYUSg3cvrPDF6v93I50hx/bxkTZVaXy
k4Isq306xzIwBVKPEHX4BuVNSBMeMrEwuohlVSW48oC8HGhAixgjDYe5uFCxNWivvJU1SlW7/xE7
g/ESwjOXMv1NCMTKOQJ7T1qPpqpc5fw3/RTKrovLkTgfBahyg+86axTghImbyDzN982B8eN4G0n0
EB3ggUAyIZQUFTqBhArzOT9jhD4vYy49Z9EDr3J0dTUSWlz1L74j6Mk02sVT4lvVY0BBm/FXz26e
CyXvvgQ74rOfNXT16NjYiY2eE3Sd5YktIYzrIhjVo+lD9yt306o9wyjO72boIz1Eg9nud0mdwM+S
58+lGfNZmJEq3vYotjoUQ1FMvzR5uIZXRIhB8X0g09V2fTLsxPaxQnIFHnliOWZmVnqtnocFcctj
c2u9etrfR/TOpe4QyVDWqgEnjAiQaBHIg03Nv5ScUuFn3WzkygE0ARKlph20xP1KKqdzLaGxeY+p
/luJZyA67GklxmzCr8i+7k+cxUgVUyyHW0i9z9NmPVdVE6BEp7eM/sVK1KJ8A2m1hYuIr1oHv1Ca
QIZVjT8cy86DNtkhHNTSUtydWk8ZdKrfn+A8aYw8Eily63b/1Mnj7ImudEU6oGoRdGBsp0yK31Gu
UmeOqp1h3bwFePzs2fnQqOr3T8Io4Q7DvwjK/C8IOrI7ggUJPWjpYKPnMk7SZV8DpLY788R0rO8v
pWGAMB8716HRJs89nA1Fc/bgu2HX5uRyf9eKXn7cN1mz4unk6gPjtDavmnkGRdZPtn42Uw2P+0Xj
ssE8NPe83LM2r4cq63G0io+7VtFHVk27RB5socqvwOW6UADwrcHCP4goZLmbkrLuJSdP8N7uZzB/
GKiPFwT6RT01unFPHoNlUuJIzXibehNRRLlLfPsx5O6zSb9wQC4Ua5Nu+fA/WmxWkqQ1CKJGG0Pd
ARZm9SYwaotqq6hKwPXfOYUPXPJePJGQVcf9+j+6brKJ907wPNYPfxk4Qe4djc81Ek8p9p6uLNOl
uOWxMXSjN/BCDVoJ0yFvvfxG2XJAGwLFRdG0jrxBYrBmA9nxKRSQuh8pG3vS685JdEqocVKcxH5Z
O1iDDvzu8yBIoMPt+8uRDFdDxVXkXBpcM8VZOfIHnIGvBoReZZtHFZ+PUDDxW5jsX7livIAz6OTg
1W6W0KMwJuZIKb59FsQqI5O2XJ0JEkFMqiyV19RQUMfOUpDejKzN6QSvxLAo+5E9HfqF9QpHb/5k
vbqAOrxlU7jR1Q+n4Zos+mgdwHQRJyiEgdXr9kES2DhcGU0SKT45MjHjeiZKpDVgnZu70Pz4y+pb
sz8fAzfgepCBOEcgU4ru8L7cp3JVYVqPucH+PSfNJ/0XAKwWVwiTAQHKtJumm9wXphOmN51gMyHV
K7fYsNFWiAkglcfomaQVy9McmNfKYWVf6FNsxzOXl0+az7ZlDCA+IMHj3DKQMwLLqkXTpGcecaBD
sMt0KFeiXcOzuFhmKi9t2PnGxnyfrbDwmBoTj81ZI2sMTJXaV52Q2RkW/Ncx7v49lk2ylZxOG9nq
eADAXwk6VSSDZ2PriwBEJ+SZflW+FdX116C2KH1OCoaLWGekz2Y+cXirEnR+VOLqBQ/AYFSixtEr
V2oqW9ogyEBfctuplvry+aUwYVlE8nkOEQCIjx8gW1hLNtcmTNbjB1+7BF8dTiYpXekjygth5sNw
fi4X7TYCTnVQsEAn2u6yRtYQr26PYODeUfMem7qNVXqkzpkUn2gU6ROzXrYdtXOezGomkq9/cHoC
QhCRsiUqCWyj7vGyQ5mEwmN0QtiPBhPlgROAWRwTYCzYkSMzpe6idgkMyU7TQoUTaaZIDkE9RkdQ
ydh0y3fOoGdTVf3+5Qn3FCS1zQuUzQ0B7YYh7aL69a0bC29AgcSgdF+5KxG7I47ndaF5Oi2L5fCh
5zOv6rFvnk66WamqYospL/VzTuC6dU5aem/Vvjb6NxSJcHiKKbREAxM9Ic2IW55l7b+6wtEynOqg
YUkpsi7LXdVelm3doggInMO3bGlg5Fce/m77FsgouNgjtJO1+yraZAs/YarpJVBQ82Tv2vTl6+F1
bpYP4Hql8RjYJKwzk9wyZSXkV+Zo98/cCS0cdNYuf9k/TnqZYotSMgycxTGR1poEvb/9PglemzhZ
R+/8ptlX7n4rTgl1dY4A26k73X57ag9RNRe2UWTsK7Zk6v1vqPlHb75O4SuYTOTW1ExtCLJ/eReL
9X5BfeFsLrbu5+39YWzXnLIEmpSQyAZtf3AT6JIG4BXRFVMafrp0Y539gpMehX5/QF6uyuZ63+/E
VxgSJ39Gggt/6pcbnhRn9KJ8Edl7Z2+8FwvRwhX7iXpr9s3htKKGcO7GnvOUEx6z2L+XS+iGe5Sv
Qfgca9eH/Mz6x5Xl0Wj2afzByGovrw0xJGlrKpnTB6ioxLvnf+azYdKVqzYNtBaQqCg0umKFDDB4
0ENxd3OlJmVpZnCdfCluXkMYPisZ6mJKzkM7SiwUxqIRllQCtKDb14EkDjVzGBbHvPO3hJCc1DUG
B36wH7ef54o1ro425iM3m8LC3/HX4sYZZNc69uY9nGahZXl69e+xwDNHyMMtZEhJ2uIw0Csqryb2
O8Aw/RTdya0KH0477O9sOhr3wEmYSYPIOOQoGz0sNGCuIcr2scPXoEEiZEvzuVCpASh7il+SX7zZ
coW23AMRUVi0NeTNKhrQXiTap2hRtA5gXn8jx4vhyW20bBIRCdFx0TYHl1sOyQ6kgpK7ArUYE6Rh
Na8Q15Ugo+BBvOoqbnRHgfzWAmBp0h1G3IaLdQ/8dvOEFlv3dQ8Sir9BSGRFg6CxLijXJ5t9ToPT
lgvAzVzwbOhzj81TQQ39MmFaCZfuS03OzOTuJix+5zdF4R9FKYlJ5iXUyp2lPo8HCD8MIG/MVBmS
YvLIdY7LRVLnV3yrjGJLpSITaE19oNiJkeNAYim6XLtgMvCUyQjOFv+BmuwQ0ntBqT+PQ33Ro276
labaWY9Jpu5cZ34pRqp3JJbTAmO9uLunlTt9qk1bfwJErkQGQZyHwbDP+bLjk0dL+YEtj+NPcDp4
wYKD/9HuNRmleUmVBOnJu593IniGR1RKiqmIGnJF2aLiCEAqSezb7udqY/d4ci8qBvKsjxgc1IyL
ebD5ulRawX9UGX6UtR/UGklxZojb5hsQ9jrD9ud0d+i0pZ0u49JB6gH5nxTIiwiZz5KuEBEC6FCb
1HXRibqpXzErSAidbFDVRkVKxoCXohcjvKwwYr51/t/d7usZFQJbwKjPvMCLZGfxZTIk4DarUp7p
e2VEVrHjjRpDzgK2Qb/uc2SNdFyWk8Tcm2rkTAmnn5GgUMIpMLq9goOeB0lFmRJl/5pLnWlTlAYO
XYbRaUgr1tAt0plELUOYrrri+M8sKbGYsw8TycKzzlFtZHeDwbLfmS8sFYCOtH2fdxKoiwnv3XNT
TITjiUBL8fvwmXCNbVcEfjRsbFfbT305GFi42FdmFQ2+R7klGj8wnyZfQeVpZX4Da0ahVc5AwuaX
efJnUVB1h5hTJlebndIM9kfZ0jbLUEEpzYxvT5CvX28WdbemQU/fiwnB6Z+5rpky+1Nr3RiiOkZ+
X7JgFIqtzNBkIz4/GAxk96vd4z1eUaBIbUqDaR69wDGISKHWpCIb/blkGgvEnutyOdTJTDvE/D9e
ryeLNy9oX4sONrUku5Qgt+O6VYJRHkBqRBEGySuO/chK/LsoA0/8ZRPQIDU3NzNyROdnCPOZM6ED
0uR7G5NiNhDySOGFdleXfflVgYLuhixuN78A8UV7yPj/r6tkMCB5/wo7L82IDIZ76+Ck7otWvh44
jLx20FXvxHrNlTYOBs/exoT/CHSCkYm4YlFRS4rMiHJq0z4+1vUx9EEkCQHWBfXAerb6SWqibbtZ
4wd1jBL2PujCJnB5hdOyQ3e3SrYO5Nw+3gBheawsHMUb+tJJcutL/qKgM0MdMJn90Fgw9Xz+G81P
H7JNuL5eB7bUv//NSnVll3bLJe5viskUMgh9RrrRhgzV/RhoQDfjeQWAUI18+QgvsF4SUQZ4JEub
xP/sK65qA1n0WxLQOISdSWQ0fSl9KAstNbcMj3lM7gd3Eblwi5FfgM9IRhcHNRJH2Zqwq6xci+qW
ibxSWboeNG4pV11hU4MnzJgha7DG0ZaNHNowaxf6+JlgAblAjfI0zIXDFXY9hH59R/vNyRZzzI1S
M6pMR5h/u7wguludrSEw/OOLA6a48pAqW0sxn35HPnzbWwEDO/1tB+Qn0QuiQ8ywK3jrQJ4mi7UH
nOsDuHqDh/433ENr8KSqCEasJJ02YxWZWbun5HWaUFQVFyp3cpFQJz5sBTDSaP+ilMgVSFxs4ny8
Vth/KNEH+3rIFJVNISHzUkj13PzIXC0xUsNNt0mT8D6ZKyoOIG/XlbZwmKvwr1N/wAzIAsZDCtPq
LIuJpi+FNUHM6eh3PLu+TbW2TH+wHU1z6n/gT0na9ka/4bXW+x5baQnBVsREzQwhlDCxYsij7qae
0X0mhJ+z40L0Rsexeh+gklX7aOwx9atzu5v1a+NoeRt1oajzA1X8uwjP2nwqPb2WTmrDlrvfQanI
9uitF7wqoS5abSdTZ2mQAnmVORtBWne9pIurAkg6y8S+tuCs2jrDS5EpP2wHDZAprsqxEJ+NaEZc
60ed0gsLHFJoVoMVdWG17g1xmncBAJK9qnePo1Q/TbyHWBGdWuUMHc54R5nzhnnvVNsuf/v+8iQa
xA3s7b7xClRFpXFvF+YyWPYkhIEWkQ3DD76rzrnt5x5giv2gJvc4Wz+9wmjmTwcVL/ve13v/bHuc
jy2eyTOr987D5tKPYmZsgHNENREtSYn1bpVSbcaKUg5aGQfQGT6blyLEvDorBdPufz+j/j6nQ0/w
SwFM0vF1DqzwKN/5cNr73H1+L557QQDNafLlqOYhi6uxcGljY8Olp9XXmjR7JaCfEv9lf1+Akcnj
rhhzbnEHGmPgffkqHycUY1iiPApxyYfmgUHJg5xCcrvypioW7XIQOQcuZQ03f8R4fXx0yAj01Mwj
lHbLTJzQXFmraAn2u6+3U/vlXayprirsDlt1NjXjQBoD/nv2NdyeF4cZDQJJGQ65+oPRtDgLhUOI
fqCDcfOi2y21ymsfI9ElyNYq+lH1ro3eQkzPrgmuVEGu9czAW2Fr0DNRpjpeDQg+rWxM69xOsAvN
AzNVFZb6EFmn+mHUns5vZz5HKKiK/WYQ6NLISFh85sDsBapxg7YhUkPnJZ6hMyhKdqHnRAsV8DL9
8oyrh8HtAgqbdQwmPz3qCyfW0vQ4349BW1iXzvtW24OfUFAyP4mvYtnVx1+FgvWwrYnE1vchjX3O
YG12Gg5ujCfSKI5qFPIPGW6HD5EUTRZ/n9lEW7foHYw4+h7goo62RRfhCplHl7ktukiFfASULLBw
e316pqyTCBLGuxNjitMR29TZDrggO0rI7z+QUaxfE3HW1IQDd+EQH4PrqvLegT4Ax6ja1Eo7JTYH
AQCyOYVvE+VQDnhJ/jpZZUNEAUA5y2+n0aZPtnKtaO88dSTG/IkTsOfyTxDby6CUIFcM2Lpm9445
erWONgxLEwl/23OuB7EU+RwCS+JFeEAffIPi5XS3Nikzz/CZ6/uKN0Hs/ZH45EwnGsu61ZCV9WlY
e5ZKKII7Q5ohGgrggKla74AvFgHzAqYeXe7xlhkwMRSIZKLKxwK+jFMDzThS/qiXcj4MQ4PEslax
bFkwvrJ2q0u+EqAFJ2mDTstCewz46+FJAQOMOx2hd2JEvNEBJcXjhKHwenrB9QKgFBiLAFqgEcES
96sx5jo9xJOORug4vjJ0G4PdGYCJLiqTidaPe3+DRNYAOyuuAtiJC6w0gwmyxUIu0vy2ku+k/c1u
9Alpj8GbI5LXI5V+KtE114609BBDC4zGx/upA54R9CvsKMi7BfzuOuHweVlQva3lJbgSsbV+iDxl
xYHl/lQ0GB4qFr0tVPZFHG+8e+RasLFO22/W2zREXVL8+4yToAm1/Kx7if3/WouonmXPVo1hMChR
KG4sg72SmzRG4AjnVPmByhm2vOQyNj5nA77bRC9osGLVD9o2t8SeYFw9ZAGZmoSu8zqDTejwB2Tk
RoesDJ3YKePTkKfV8K8/ZbZguK8LR5RJ7eRvN+yIiVrCV0NU7hJnLKn9yma6rcdHpYXkCqjYjesf
FjAGsEn8GGzBFD9td61uSw3r6emkYxrw9FAfP17aLZyIcz25/F21NwKMzzHkDto1zqNHhvyPQP1b
zyMO/mtGdnCEPY5jBKt3pnQCkmV2ss9dDvAIUPILLubtedz7IePUiMIlp+KOkZGJdryQYFY0mQpW
DWmSg9kSjGuiZxGhQT3QyyDgHZCObGLxy7jLq9zLFrEJjGxJRaDXhAkkG7tE7K6uYLS0ZGwyvC3D
+eoeHK9NnwxvtHuBUF4U8FblHZd/Sg9EKHMoz9E0j/xSVrsK8MMzrYMj3brjcoPVExgFeBFKDHpg
B7d8npK12yiDp08QkxIMcJgrTRQ0BBsmFkVlVxwk7/EaWUDZKud0hLPQ9uP4PyuKuDF/meRF4peT
Xn/weUvfv6yGVF1N0E71cONnZf9bKwIif71k2xBADdidLe8JdvJhET8cC23yOXksHmWCSbXPilGS
+8E8UkZscHHd8SCQKgqul4ePIDJi48pG17QNRSBmWTtsUrjLYBz/iIsnzigLtmt4BSmPA2GppoRE
5UfQN/0v0DhPX6fAjat4DYzrH60GqmKVF7ty3tsensbSyMhR9lF0YZjckHdFvlAyOHQEA2jbPLlC
xvfR7NuixjWphBsTybRsSa3WCtkA03MINkH0gh6xKkcHj8F25Ps5Yri/lmqNqclBJqDCYR3WVqiQ
NSQqchKhsQl/kDfJRGrDTCJteVViBWi4IaepIDNPu9PYVHQ1K1NdhMN4+64SOYJL5p6d8dJXltgR
AXxN6pT040mmy4G1nMM+iTzhj03l+MtliU5ZuT3KWrcLQVABZE7xW37Oj+//mShSl2Cihu8d2rmi
pmxgw0bLQCXOCfUIddfuNab9vc739821OHYzwQ+gFDg7yVfgLgIcyMTbaV2oTu3TLfSaNbO7whMo
VYw5bEMlR6EQmOaWcaVD+Iv6FCvUujHVvy4XrIVLBq2Od+4ihvawslOwS2LyOnxZ19EIUZe3r88t
yZr98Fm1Tj1iR7anwIcs7xgos6SKFlxKT51afY2oMITRmHFZg145NAW8QjiHd0FwdSvPtOeHg9H/
+ent6nomhQuRS3Hq6YZvXi4JvwSPUc+MkINMEtrmNgmcdre4M3DFTwpxyXxxaeBzD5cX8tt0I14Z
0nSjQRNzZx1KBourwpOTp80uM+tYoUbX49+DO14UnTflAC0YianwYwHZ2J+I+KFy0vyD2tcD8Kga
cQyDZV6ZhYbJhdW8TSip2kmkUjS0wbelRb4swDbf9gICpsgaHnAk21MU1uUlX+oL/mJcr1zm8KrZ
IgaEe4gzhbyoJLrgiWgCs/YioDxOI+1vzaOMgkQW2RmXNEI02NOizZC8cMZzOLoSAIkwBUnUtWMC
9mOblnpERW4c8tSxVa7xFI1RZ8u1NKh/vQcCtW2hA3gubLkWT1ZlsxKhkwhdjJ9FbuD1uFSTpf8j
UbHWGpUV5Eqt+eh8GIun9vRCtm6z/I5K0xrqUV3SQOwege/w074e/rYpw7yg61CocEz7oqJv8954
FZYZInNXX3r9lVzDlo0KA3sP2dp72StmottY24yszqpH+9+tCLU8S0TKAQKBz9mTudNyI9SggOav
Wb91Wt3aoRUb0EAafN6zwsYsidET/L3h+hkeKh3ZxW8jnynczx7AeNamZALSFYMFwWtSzNGlJlGG
eIlM+6Zc7INven1Js4jxFn9+eJZ+4RvvRGVFCCrkBWKW37h5KzUcNPM9/uo1Bugi9OMUIRI8HYX1
hu9Afs6hfGxuWvNiJFBY4dnGGpXkeAuXdnQPt2D8A9qWrAmHq4Doz/FrTs618QvFIN49/TTZMHkd
+PdfIv6ik/JkK8byAHBeVMZoA5xqKGrFBe6epmE548q224c9qUA8yWbloekNd97itDHqJzNUHys9
yUOyH63edXCwK3Ao+JbkKbtIZwCfbaW0QuXJoOjus825nF9xlje3HA1O7z5UWhGd7iYwi0OWP976
Y1zQs5Nj1Hw8igEBvochu8PLrfqWo1qIV8JFRQjOeoH41CJi6rfDjtwLmpGo4ERVvwvDRnJ4Qnfb
zSDa5HEobc7iBqEYo9Ru1yYSY0vnFPr6Ue2PHH0pIPAFJoOYE+lPr2xUkQ+X0aUERyy7gWO5atwW
2mjDCDInlt9ZNg7Q11kMybjxK0lM/N4bOFyoVWFmPEhD3cmb3Q+8P67C6sc6S48dc8efLbesc026
glX9puUwo24jQAFHd+C1P1IIUog6sbrnFmDbMx2E3Qn4keWv/YemSSGEphgb8rB3XQ7FRgG6BsAB
ckcTgyV4oGOslxrxVuQz/mDIKDni1VdboCfT12TxTo99BSM3OCi7nnhRM3gLMtAvsSitXsQq6Wv0
pq1GPLAhpsrwaOAGTjCyfg/BLDXgGZM3WZbPD55E1AcHwza3MXIwIk7BBHWR4dxDgRYQeHHk15i8
wAIOJyzKz8Et9jAxCSQe01AnPOrsl2H4PgKbfSQzfWzty9FsJZvP0cE5JG0c57LodJmtd6elFz8+
zaPjXDJ0k4m5cMFxYFzlLLNOZJ24W0Kqt5Jjm4y+fYMYOPqkNXe8hgrJ2ZsA8g0XwH897FoLoaXW
bN+pUeF3Y+cfs/ZR6/c32oPQzGcg6F5mTEiaFRcIEbWtRxsz8CG7dwLLh4eC4r1bPNDin5cRTooh
jc9Fb4MxcWJ84oKup47/hXyCGYbaai4r4Sp+eCI/2C0QdgZi1UwIUqyLn+6itAyfacm1qwBPfVX1
NqKaTBivxbwqqofc42RUnhB+3GDtlk/REj/LRUVzW+G/eeLJ0yqPsA70YZsBwj5B5AHHV5l+lwx4
En4twXj8qNiNdkm1R8rrNeFAr2Ma49Xgeo6Tct/DaSB5IK3MXsPzk0+XuMjhK+nNMp9MJ09EnTVU
s3Y5BhvXrJ9VYI0OkTT64Lm9Nxfl0/m9ZTmEz/QjNBEvUi2bZ0HnNqoFZRjr8J3GWov8XYLS2YDG
3P5dtdx7gtIH4/uSyVvky4kpbkhNjHRgGEiqn/Qb9LPCFfUaIT1yLHvpxzdPH6mqls1vyrrc8S2K
p6GDS2P5q7xUD13jUBhYDYtbDGHdoDDWwrhjI40u476CXhMipAodkUngC9VkQII+kvQydlqHqbpE
7XXVtjBltQ10boQmpxL11gOsYh2ILA6nPFb8RPURZcJkAU9LxzqA5In068OjCPbJdxiLZelmWZVU
L53TXooc40TLPm033d3TCpcETJOfWHAcSu8EoXeLxqu26ya/9ftoboYeeuXCE+3VQ6rr1lZXKF3J
6U6/Qq7DkkMxq/WBxrvZy8pmul/bZxNNvbxlOGQblxiEUQZDWgqBeYDZ8fXFkyk6jLp6G9zJriRc
7qPJoJBFdgNty4X8bHATFATOmmMuiw39UFcoatTYdx2qZiaxiIUsxwEESq/1RRYpeT8Hvw2Nexz7
ofrCXVPW1uo5OMSvvyjW7L8l0h1uByDGuHjkS3JQjvvBRSe6drN8L5KpqMbJ0mRjhgiNG2UV/RaE
crqHJ3VVC9kj/9FMP1TLu6r92D0kwcX5Xn0zXH1ty5ZnfS/5OQj4bZg2ZUxiXXp9JjByvguXzrHE
7ijgqlGdKlHvFC+70peLxf6zmL7vWo82iFqQhYoXeJUWGLG42pLok20SXibknfteHqMuELy7t1wO
jP1s0g/e1IizyGEdstNuETGGFCmVWo9yIjFNaA1yICZhEeqDL3cQzABsRr/FgLPt7qK/+uHG3IQe
P02rxlON+HqLBSSw86kQtt3aLJgkZkSt/tTjn3b57PBE/tp85uSVmd1bLCjx3qT1Pvjfza08oF30
uNSk/D2qK3ZkFcISuoD63J5bG0LFE5iDzxWp4Ga/WYGjqtbbte/jsJJlP0trr/lJX1edDvLP5XES
lGNKpOxLOeAm0jTMKkt/+4mKH623HHAKEhHih9WSYyp5fOV+hr3iCleVpHu3xuLSfjSQcdyaQsHc
Nb/kfXlwYkrGczgjTC5sUXoHRGA+cwAoKyxFDN3q1b0gSHUOGK8BKfOIj+1tCxDM6ArXxv2F/XL+
bHR2m8J8h35TIwYIX+1l7DR39r7yH4IZ51LXqipJCyP0E+c+CWS7LezEQJnHCNeH0Qnrhi3j4pDZ
S6zTAtF449V9bagVyLgS0cA9vf0LSuydJGFxqTqQurSqTEMFvjFD0zp6H5MFy2O6+o7TgKHDWnAX
VYdAaJd+4kebpoGIvPf1Yq+5mFdf15623QJL342ebHIWyllJsQdK2fYgS0OIQlSJW1KupejGH3rl
buXEIoPE06ixJlAUfIBoBpGOjNxGAX8PWZJiEi4eFwEDJ+uU842p7bx3OEk5D7jUrjTT3EV7Uq7f
94tfhd5MPP12KACN9lAUCdO59cfMmAV5er4Grm8J6xrtWwOBvw/Pe+x/2ZaCBzsNZIXPm+9vIHT2
UAPVwdkGaKsIVKZVOJTCRkODX0tdJS+7nvYNetLoWu5tvUGfpJbAb133QRmmK/5Mm3c4l6hsuaKD
KpCFyid3wQV7gmEYMiqYR40dk9XxRiswHBuNSGt7FAA1kHVQbtjvOtDmMathzhpG2YFKUr20/8fK
SpbPSpdviH1DzIc4Osr0TZ7hFpRPzn7WFYaafxBkIYHg6YwhhjxJKvhDTZYSeuJHmOtEz6c3Wkct
4WF83bvDzhnMKaHL8d8L8NczufRW37SYzEgsQuHLBQSC6Jm/YgWPr0u9yOq67pA92OtUn6hMkWKs
xM8f/xrr+RtuQ7YkWcbuuFG2DMXDptgfseXedYGVvMf8S3qi0sI7vTZIilv0hzQT172wEEKylzio
B6x7ZJUJdf3iw/sVsfKDrHN2VlOa0KXo4SezxOngOCH6esdJDxilan0F/gu1Xeu8RCQX4gzMgYNH
NLC8+5c3kqdoDmTm+U0a8Cc6ebW9JgAREBTliG7/3N4A/l3b1xKzDf7L+Lb8jFVI3BViBxC5JTI8
6yzCxuL/t7qcYa8tzdzUUcBbwrWNPI42wygBpipijNJn7SA2wMGV9gMJ1Zi2otx2ShQ/kcOgqjYP
Sy1b2i15eHH/gPIwKPjd11IGaf+olYxBJMXC86TC40ktK2+zRUFBrR7heI5pj0xFJm+ILgGTABBf
m3M8WDmDz32clXQRg0cQGk34hRm0DOYDSBgOj63HW6yQZ92ekxnDv40WwT4kAa7ilsDtQBWBl4tR
O/KjCJh9k521AHnJk5Ovl4yKALAqZZmAqot0quJCaiPq7RJE7gejtlrL6IOCtaDRTKgkHPuBosxh
A57Z8cAbR1u3stqPWdtBG7pCNfQhU2V0w5/Z7JrTPaYhs8tpvthyPWoCJLZLL0OGk4aiAPZ1RLDC
+ts6j6X9vmDf4vl0q86gActKrsrAXHAQHqtVk+iRPGz425OkEozJTizK9feXdyqyA5Bjx3NvMayl
QIaUSj3AubLRg0Cb26kjg70QzXiH8fxmM5xVmIqI4tBUi1e4ttDdmB9/GjNhZHF/5APUc25kciF1
ukcGzmTBp/3BYg8rhUsW+2AxKCaOlITKybRQ8gpUbOMPzox/8n/A6CZ1GWpfThs/3QoveNAzRmR+
75k8j8Us7xPUncRPZzkKjsf8CD/clnbG1qAHCTnbTShDnbnlGHN3AOJ/i6vd0VES9vFLT59MmI+1
LjEpKZKcc/Qh9vfPqdlnY3ugsglTYsLNRw10ZQUi7S9GtuT3ZrcssZsTjsb6CvFQzQ5xt7MHTM7Z
G04eqCdxqjw7/FRRfL4Jg2bnjS8ob7fTSeVtYXx97NWcrHjJAoAcblduooEg+lWB7wk69ammFyd6
rpAoyKsaTLiBTqcD3Za3YYzg03gqYQmvTuUFNEn0UghHPWVTmDmt15UblyGsaQQXgAB+g3IOXAyj
WbtCoKe2/3mP3fTkkO70c0a5SfJIOktgsetWDyL2++KtCq25s6xkYq6pe5lHhYDH1q/AXXBHUrzd
YjqvTTdK0yBu8WTYUx0nSNR+jQIM7uz4H9wwib7MhoYtrH0MSWuzlWLrJdN5j+6fmDkZcc40XdF7
+fuorKqhYrauWf5xWP3QwMPxYOCE346Z6H4kDXXEAARq2L0HhyMoIFL/wLXWu/T4T7ahmx5bG9kR
lqPd7f2xiE811Ao1hzlmwE5Zf95nq14Qq8GJxFmoO8DMEmpvKYMQTMnYaqXG2yrKcWJg8Td/BZX/
yw0EkQCxKYXSUiZAH1nRi1X/v3e+EBVXLN1FrBByhZsemiVRsIsj4RJSeJDCrmd0M2WWvSsBOrAZ
wyWX72Q2DedHJUIWeH5SCvlJOfMsLeJn0ZKRv0EM1XWidA8zFZHs/6X1jECzRW4fCMrgh1/WPxJC
hpJAH2OFjWWY8lWz0l86wtYNRw7dkFjaKbL2M+nuJywdu4Jf3FS2B3TYLX+jmh8A2ygASfORUlN5
ohh8ag34EOn6Op5hUQOfy4qexanXzo8abdiKpcgCfxN+7u0PbiOCrm944sWwnEfYYMSq6WZgQRIf
mmR1NtNI+7ptqZKMdIVoBIJkk6wece9ZdCos8703i82XjeV/gKAdBfSN66+lhx/TucUvDb76mp8q
npE35ZX2riYrogwZzlwgWVl7WPiHzXIxOvvCUt+6qoo1w3iwT6c0tzrxEGA4SrtZCqh/FbWK9o8e
cuLVZpqZezFn8cG3ILko45vipysPEZMjr5ZY33wGaugSeNBCN14Qy+tlzXoWfjb8mkRnxky+uS5L
7ebVTHWvwtHyXn+sHuFaZtiGMXaoS1fpCaDgsN0TVfzEjoa9/SmoTOVC/tqmPIndlOJR7zQKxda2
aPJFk26RX1E8SVPhXfjARt89JJhnJbQrpae056fKGlIZBVgkG6i634OObHKfUUxkESEM/EbxrX1p
iOCoTReU9ex0gVGur/Tf8dOhkXace74O//Mp8cC7eHzFdyI+M8YQwYFEKs6Gyo5qO7UEuwZL0Z+A
6jpBSOytawQo+MTjw4Xh77xk0HYtMXSuIeI+0mWteL9AweocRFisTD0yldUaiVKO2P7X8kBnLGwz
Hxi2KWJTJIHerKa+vpzbXHLM7xZzlpIr23jVIDzPrC8ERG9gGk+VOtKE3NM3Y3n65Dm1NQMU7mx/
TAi3qxwiZT8rAj07WZkH8T/AkcKwT7lHX43XihxslYi1xpmMALfjHWJQjFC1ISzP166lfcRps8Uq
ZMEJhhb2V6HBurXis/2Qw3KiPJQntPzuUyofzK2yK85z8BOKKDDmRFsW6dztwcRDdYgQxsGKOXw1
wuZPt/4/pauuHJ2KnlKY2KlIVll6FSor4qbR8utLvvPNf7cdLoe4eOr5UUQ3rpw7kq9ph9gqR5Go
qhlpntm1Qvet+TYMlz6DM3ZaAQ4GnifmpOsPXXigoKBmuV7lDsuHpMbuY/yp3QrbkicBIjg1MkkS
H5Zj+Vn0zAPou0eKmJMWxVYcFx7ghRXG1uqOWZV2RSrbtTt5F4A9WDE+HEZU6kqYbLfFrail7qdB
wqkt3HAAvNSLT+sQp611rGiKUOXNRMLlDeURbBBOdA9SEwbsYHzLiGieNv2WcBDPRE81qWJ1pwCM
Z1+BElDru7Bpxbn8o+lG1phkurfixfMaLLQAHkDWFiFjyY480Kuq0+5Zdf0Egynvn7uD/b6tzJP1
PIfCA8ZuYdGYLNXaHR9qnl1MpI6nIhH0i3sdMVgRvXW7q0zBdRYgO/2/DRYqn8YFV05Tk8QthoDT
2gMmAtPneBykcgg7euA0ftkJD1/szfNsjTTZ2DkDJIqVrKZIeYuCFhvfND+IwFJK3ITyh81uy5uZ
rIkcdYJqdkAZ1n9T6ehZE3Aqfm1+OAb9Ad+YBGunZG9Srn4+Wxc9OXOM3kC4ueoVYMM9d9l+OY6K
fC0052W7uPQXvwR6hy1L6/tPsGkGWMa7gyF9mq6qbRdPiMpsDUbY5S2/1lC127SFJmn3zXG9L7hd
jFib2OZbb3NI4YUimEFnDxFbzzlFemxBv3iPDkapM8lloT6q5PIQuwKucvkGHx+PlMVC2h324YR6
sdXAohoAeJr5ywJmJirf99GjXdklEXPao2QbU+/dGmIF9k9O/x7K6M+vFAEQ41EmwFS9OYA/qBgj
g+btNBPhwmBf0Ppcdc8n060K0uywnXWZ+pbm2BhfmQs2T0jT5/8Z7EIXzIiYXEl5Z/4HUFXG76bA
YiI7FiHCQXboKQGiAxecLXmPIjxtZkecV5htl/wFB8da2+SJrXeK+pSIflcAghlthpoLea4/Kwcd
+JpCPOrHH2ewAG6rAF0wZ3M34IIAm8rwvQbEnizTKkFJMPY5ownh4e/kTVR35bl/aY5V00d1dSiA
qeGzLoOam2HHUfo6oqOC/Hep4hYOgu40UaVCsHQj0GHKbFplOkPoq13Qcdn3D5ybvrVFHO9nXeyq
QQQEwl1wghKJd/vxSw+QEu0O5CCy8IvAZ2L9wK+fylMRY/KOtWyVy4dXfqb+dwJTJxm3Mbq8s+/e
0hoto8wqaMb2MMxdpaE4FK8dRu75M93kpFHcfBNJdn63qNhU/eh1onss5SQaFq2juHMOrRhYybgt
o0qU60IlFW7vIxgXCSuSTs3AgQwEvxMzAHqMnT9zLtm9dfIt5JJ6mj6DxMDutZfTlIyVOTGIxP5r
khmdomRpT/PSfaR6uj/V8bRqWq+Uc9xZcWEQfOTIOT+UrHgS9vfjLcvs8dk/WnQLbA5G6faCRUmk
/g/ffO0ctL62wDPwYKZxyNZuFGKAUcn6/bJHxGUgJHZIPC8zEpEgBAnNFZHPT5lBTTwrFUWn6mcu
S9GaYOh+Qm0l0t05fQsRfRjXmDCxad93T7+Jj3+4Ft53u+EJd/aB/iCSDbNQp7Cxfze6O0mmdjKe
fxGwbLaGAoSzV0vSW770jzyLNRFtPXFE1CHg8S/TkyZRzg/UqK3wCaoz5BkKWQAVc8TnFEGDlxYq
+syP7EWKBBVdAmwnFWZ46G668Tq1jEcKih3VwgkYZAJS/9QJQi3ivysl4zo02Vv5XaR8hvp1nvXi
RKO1jYLhBYxx0CIvxyLqIgmWvcZIPuCYtCYGS5F8kdqXXURc3G+93sS1Tt/JNVDK/GXk2TVO3tX3
qVnJNmclMT3Eua19PEIGq/n9wBDoPEMKLApDddoSMqF4vUA4sftUKY4cA/pLl+wTLUNZ0Lvm84yD
FdkPTvHKlR28Z+iW5B0FjyOvraGM0suAMXqj+jA4B16K9/qH9HcZFyuP0WEeahKA4b3T33BnYrTk
6OFZeGNbVc5qjOUWg5gpWe+9fBGohctmaI/ITyG+2HH4xotVLZxesoH9+36cpjAHrACOldQc+wp5
fS9PUwspfqgdoPl1s5C73XSaqIManvHq0BV6ZUmGj+KLzDe4NN1KHtXD5H9jE0w4fPenebNJ/dMj
N68jZ3r77VAp79xy6BPGqRRrncRMSEvVoZQbu6XggPXOJUQjeRKqA/x2/p52mHQniZnITxsUuLIi
VQqKhPKYcLOBAERnp6Yl1dguUsLcEt5mFVH/Zodotfpr8zvWLGfnqOF59gGPzVDPzgO0eTN1czVQ
6vAB5wlnV3/F+CWDgQul6I0aCW8TU7vPABc68xt5AiOP8bbabkRTw+d1eR9gzihdL/EzPY6505XV
u+kVIlKdceAnNEggHFQ7AW4wEXhW715si6+uurwFJMRFdr+jY6kgnxtcwzTJysuJB5iyIPHbcNcX
rIU23GQ89P2d43IWLEVcMfsj6aGrhnNdw3xkaJ+KnLmo4fIeAKe1+oDikXmruEvRWGbXeWT/Un08
4cdzd7h+SjPNiiDLjCsY3a40JtyRW9Xj3PcWcTVySHZxNfHJetEhGxYa5Z63oYhawnxmrKcmeAgk
Bq2l/huz3BD9nNPaTu1dqzIDv1dfYjdVN+s5gevWPxKI8f19SMVW7CBY7hZSR9NRcKrCPbO1g26N
swzW2AvKal7ulZXFfTVHYib7HSArLt3eGIYM49dsXE+g53b1cxbBcJp113LbVRiEDeIJAJh2ShZV
gfMh03KbCm5JaghLddOAg9jfOACMvKWvyr/BLGml6efcDVF9/1sLTxSNj4Q0hFal+P8fhw4YYe1u
J7PTkiijBbCLBQFvfNWhFeTMGmpzepPrz5VIyYJmzCBus4meMJrNqnvAaqiS7zLaQKNFb2NolOBM
fV6bsVDOm3bzUbORpVTnRAnZDkIofJhnZOEjP2pKdqIT3XeBPHjMmmRPv2OLvmr4+LgdhCFG5ViK
yoj33H7dW8JALNXcG7p9T7awr8N2difRMHEQA83Uq/edj+hqtoLXqqlNAoHIYfzZfUSDhJFdT0sL
esn6z1FW1nJ8/hov3qI59YzWnvvp+Y/eSyROEn2pNFI0vYh79vHYGZD1XR31Y8CurnZ/fd0LL/gt
f5bQGdOD73gnoagw8Rcihh4wAPZTxZQ4pcjWyVFs5h2zxvCS5+AUoML2GDdtmvgGCzR6RsUolD/h
Pjm44SB1nNNatYacFBzA8atyRQZVRZI9Oh1tOaUft1YclQ8FSUCB+d6wigp3PNSZ9Q42nb+VrDFs
FU3QbC2Y2+endVLsPRIREckFYH0Sji1SF5AOf32ookeJ4ANALBu+9t1rzy0kVuKd9KQNRvdL4QdE
UA7Z/GtZ46/UIaKhYwrmOIP2QfKR0BG15zEB3MQ6a4Yp41dtaJvaaxyKH5WNS40p9SaJq856Buil
hFPAZukAfeBdfuzkoIPH3JyCxZyPzMc0/od8q7PahBSIDQdSu/aE3phQgU1vW6b6ceqREpnth1wB
iUBfTyqmjt8hE2OE5yX+o6Rl8FX1bwnhDSRMVAm8QueTpKKt95DHQ1klUJ39M8+oaIClH38Kw4oK
/d5/Hewe2FZLiocz5LDBaV+B/j0OvhZdS4B6IllKULnhzszLV3ew94KL9REGLkgru2kbh5lqEFrr
AhiBK9VcZwCeFfBXS1bHDsULVXOw4ylrlDhmAnQzqUadqyqq8j1QyE5J5KtGf9DjyNnpV6Mzh03v
+gVVnMYxjVRkGY4X9+mmm4JLteZHBXQ0EXhKblQBJBfRyW9s2KZWOG9DSaM88yMTyk2BTY5T1tfS
px/p+4et+FdufW+3cWF2kolr0VnvYeipyafXm0uqZUPQ10SwMdXEvkX3pRwtlXJt0VjYXD7Tl0iJ
kMsCGp/vFzCbw6g5ZS/3022/afEdJ6a3MZCL+Q0p5u9Tgb4OSZYfr6+Nl4mnoYC9PVK7cdOWkQhs
f3WSPmyoxRFH1c6GASodyGksExGfhAfsq9f+Htj2MwVuE+r8JcuQBk4zr3pH0CcJ2vc4hOJ5VTf0
C9Y5QJZy9SEqNF4N4XHbzMvLtL4YTxMIWEGW4hkr/Yyn+uZEIl1C0b3Jnf4OlSAkE19EE2e4P6FG
peDpDedFzrku4ClA+BoX82K3CtyFiNjQp312wUlPwFZ0T+9+Wn3Fa807GaZRpSoueDH2PiZWEhMg
dy5rnM/7ii0FpIPE5cui8CezDzQBXDxmL3vW40SOmKT8i3tRg9lPzeWtLCZ8ZJKGuyNZMkSr8L08
Lt1PNYtDde3bHXe31YUt4s358YN0d1VZoAFfQoKdrWsdBOtNtFw4SDQDwKTJKyZ+tOFwuELZ8OJK
fmOw3QtVuOz4Lud4UqGigRrFhwjkps8Bm4YH9ciF1AgRCT7H79VKk55+knh8MEz1fbg7tIZNPgnv
UGYmz3Bq0bnqj8S2S52S92QG7qT8dsm63Ydp+kAsTDIWIV9jDVPXmxo3xT9vnS8CKuj8QNM4nD0N
yBayQKkWgH2XnnZVSuJTCA/1ZvxmYNIdZTcHfa9FBsSrdTjXg67ykWCdl9phWSC/gzFiZmfgLQxw
lYAxH6JSS4M7Ymnea/N/NU8qZY7L+Ugu38GmG4aQVCRf7IRb1VP8Gy9DGU/UghgEQVqFQ66Bc224
U/oqa+omkNQVLohKoewnMeZ9HpxBl92AVGTtNTDNz2A1/cs2ysgsC+43FmxEherexMuv57kwylg7
2yk+wD7lSQLLaVA7F0PGAo1aMS/tdQsD+xyn4IgR/K7mbTLMtPjAbMivSTdt8WyXjP71IHaVtuUa
hC9uXmJM+WgASlFe/nBRAVpB0m0Z3MA3Ci2NqHKTVwCTjmbuLafpHHPtdUB1gPlCgC1ZrwAgN2Xt
7lcsNauKHVlltAHwJNQwKxc4FIvD/9py2nynud97bK99jGx1kH0RWnefH5+EsmLqVxS/2HFHTG++
Ci08xazzje6/3gZ1grFfzWOvZS41L14XnXfYW5ydiVd01F+prB5ADlIL6RdjWoNwIBXNmmN/QEzv
FWxwyAFaPXBLth+m5D13F3t0TKss8KVpmClxbY6WXF+Ynvkl52NfVRApMaSd5HoJ2MJyaNXV7i16
Z2fKvHikb5urfhFOMU1SFent0Zw1zr4nmbV1xqC+ysv7llpr7mSqocZCmDkO7a5GYpHNBA/HoUYo
ucQbrtRmDngstGgwwxTbOGWOa01YupFWL4L4IjNrNKocG+pb63dpyx6gxlZTx/KV3CzM0ZZmNuEf
mxhArUMvGwybgR3AnW7H2iGgdii9rv3ZTdiMN+SVqnNR4rI65Bk/xOuRDyOelmJLoMgkHGUbVvO9
yw02FQfA7lZOIPiFm8IcqqFAwYIe5dgZjI8o5insQcADCtVe5jAPXcbmCO2aLsptu6p1IA2wncsw
riIX9NT9PTdg8J8zZ3AlyCpRUP9eG4IJJw0m07Fw4CiBkoXZoeCyXi0+FPvjPlKyFmruECXnQGik
eHGHgx478q0QGw/D/4V/Y089zuhXwfr4iIkzNOFLvEiYznFvPKGcMEKaf3PFAz5ot7A9LZjNTlFp
HiruDQSg6qvEun8MNo2mPYHEAPH3RXi639HglXjM3OMrGlses6QfqsDk7UEu2P5A+zAL2+gL04QM
CnVsfBA/KhPrsOnvjHfRRZDVZNV5B7u+CUh/retkDf3U7o0bVOpsr6wVw0ceuYyeCizMXeRqH5OA
dRo7/Tn9nRjuTCy9CMhw2+KSGuqyvCuaGp/zN82FfA+k5ddNS2SJQ0ACjNUnT4hTfUwIX9BijPWT
BRE9fmMFFLhgGnYhN05Imt8ElfLihpyayXDF2TVbOjTssi/fzIMLHoUwJsKSR06dnOcL2bwxZiY/
sV/dvCnAKKA7jwOWZ1kTDphALD0S1jNwmUO0T2Wl6qiX98PpSEVNPgbEq1Z8pXh0q+goxKa+D0YM
0R3AZeBG1dbZMMSSKGuzrhwN0TUG0im0g7BvKGt1/HBDJnvhKEqmzZzAdNJmyjsman9TkUbXlYJ/
+BQ9/NJ7eI1zzpiT/sheFvtYxnIGzX3GAS1d5BRIfD09MafgOnO4dgEhhjz1qBRzN/VmD8jVIALi
yBHWg3jsRgsHjA/erOA76XZFjEfJ1HJMBczvOCix/E0vJwILilp1pr1j7tfr32CmRi+wr2v+XAQf
5jkSdQtnW/Ve3anQiIZxdg75CbT6bHEG1/uVwxT7R7hPpRwz9cjNlSfL93CddYi+2KXEVmx7OOmD
PRxOajGR5IEs+dYnUiJvT48f3k4ZLi6RscbHFZJ+ix4YORNL9Ldmt/TE1M+5HHdsbf8CZfj8+3JC
uTeyoMOUYBwixocIpBpLNVik3n0U5HuzzAXdKAGd/Wcz/xtVQ0/42bIjcspLJNMy9yDdgQ3QDYT0
OMSQnwxUuCLIBJX9XJews4lZUWu6ki7D1bFdROj3mbW1x1kGUz+UM/X4Bq6qfwsLlwesq19Yf7TF
DDnsvcHVDDCInwCzsHWt4O9dKZON7f9GNyL8BVccrJCduW4jLzl+qebN9BHn8DwyTjXREZiKXOyQ
Tkduv0XBfFldzBc/RqvT6hFYYA7i6gicPjMPYNarxG8aoTvUACILoYbeZVYHjGWf4XNk6sQNlHjw
Z+3zuKQE8FM/OamoLPZfpJk9j49Kku+QNmuWddoMB8QdXbBl9h85IY5l3uHBvBo6PSBPM96vlGjS
3j2YFHjVDI2DWNVN1opvlIIXj8bZFaCdhgokc4Hiil46Mgb5R/3uOxLFv45h5QwN6VPzKoo/x4yR
Y+p2DHhhbyjoqFcFuAIy5w1ptcJUDDw33lfqGcvErGJxUHio17fkxdMyZjKaSCXVNaprqf0N3Rmd
T3SJccmQok4RYOlktWOHhgWWmT7i5CN0wBzGvPSLpDnU8pkxkgS/7iO1DuZjJCQKrWsRhMc6PQNV
afs8XexyIPcQFwvS5wzwRiAyp2ueI5uZC6RwpI9eBy0F25bsRv9d7dD8TlwSYcYcvNnbnL4vSaK7
bun+quqI7LRtJ5twBD5YDD0ey4t4Is2tPh0PCH3swlsumZvaXVBcDeqUMuyU1Y7OygGJ0zgLqrtA
5NajgXTJzjQS4Ne0zbKXLcKVeNbjamWVGRItrJXB8V4R8DUxF51km6ABuNzKR/t5soM9d06yAN2i
N/p61SoAaVNNdHKaXBpvNb4CbSHK5TztbIxPsR1vZ+TXxPf/i6SKIHybf6Rt2VyAxv91UqZfr3kY
cmHYbZ4nzPUNRUsHVcxcOESzkD0fRHAGaPAaBQE0DhrHdOjXXIoLgGA+pzhc6U8n4+QS17jVxl9v
yPdu8vtmpLTEXCu49XT/H83N/aC+8CGUY5sxyTkcwRIV3B4Zq2HKWNDSNr13gkn9EygG0zhqTHw8
SDEY5gtWmwKcyivxMXHu4dzV6s2TgYTkW1CpxiAHquY6ce4G7nfWnjrBA1UG6tdR02vWH8jxNS8A
9m29fFJWRkgHDX8DEYZQR46uoN1PTzweorhdT50ClGR9zpHupNuSoJNxW3VFDOiDnTICz3tn135J
t+I6/yGdZzKke81kv/3p6DYg18V5j8VyxBVBlwbM8D85WkemRIEThbaUYiRZeyMS8F+Nii1Zmsvz
Rnb7y7J6B+sTfThkl7rs2XUTq8bNrrsoTeL6TWpsYcgk2qzGkk2OSiNEDhi5UwVHeUWS7ffD18V7
qY7mRTTY9YNH2BrbnqtMl9u0ZbXB72P7dWZQ4JEGyx7scmzffziZwBLyfY6LBLuABmAOmdvvtmfk
8QxLp91yR551nqqSfQVL+OC2Y2zIwZZscDWVYHXbYgInEFFraMoW3wDgBBvYlbmUCgBkcUExDEse
K7lZsqMoJqzmAPqm+/vRYpbDiTxpmGCXAEWeyHoIab49tQ4HaacfIQuAl4wnrwkxuGIhfaXoPuK5
5rqRXy52XgPbdMVhU2KXTj9jcPvfpVJ9Jq5Mdt8E2imoo9wmRlQhD/+++SjfyuMePW5yHHzJm36M
nEV1p8S9mG4FwNXi7RsTORzNRy/K3ENAGIWUoHaTFsTq3dTpIIWFpjxYw5wYqv2oL7mAij2+CmDY
3gxqhG2xfiNKJ55XFLxP0KRt9yPtNG2oVA2YE63MmX2y7BE+zwr16edn/CvZPeeCZc1e3yjkszPJ
/92EdYxrK477evPG5BGRWXKe450XUSGACk+00wR18FWOTbW2msCc9zS0EJOCT293I4RM1D8BjLfb
Dm+S04typK8TEu3LeqG+8szQSiWH8H6yBy6UhnIghfaCPs+YdSSFoGKqbvHxyxzc7Jnt76s8INYe
6hDAsGPLPX/GLBkMo+HWL+Y3F8DGjbBBiacGV+CDNuuKRkLMKRgOUZl9+eAGX+58Iwjfel/m5rYY
Mh67JF2LAQLbSUAmoTeeAtnT5seb92wctASHFpR1X8LoBoyr5XHkZjJr1FwSXLbunSzcZxyV29yH
BjZEimN2V2UVyMZEl5T7U3O7vdRuFmUBZX9q2J+EwrLAsFnPQ/RkkP7gtbv/E2TTTXJaFKzAAaWO
6gKOGLG+sPgsQLWID3DLQTQ62pBw6Jh5irgzd+QBwMbz9JqPzCxHa7im+2xI9X7tr3nSni3UJgL7
pj773sghIDNtQvJjyHurIieGjDEb6qNV7cCpzAsjAFvymnjOvf3pbTjWV8uk++kTVHrKMkqL0OTl
NSGSUgm3H+eSqHsAyXzXQwUi0XEvUsbwnfxaOV84LRVKzEbLtrBpUdapmhN4gn7URhUiK3umM/+d
ZiTUv/SN1oDWIv1pgMuGDzJ+ZPW/jzgUWdGSmWBdFo8p8WSBI28ddiBSBvu9FU9WWtta1jDpcpbl
HqphR3wrs7gol+tdIMyIXHFyCLjBiKc+1QqqfEviZW6MmrXGys7mapFr4pvbCUIwpNwBrx6yxGlA
IfUAiStBmtb1bNI5rpD6mvsEpxPaRZHaKLagyARF0cPlI9Hhih1GI6Ta5DS1H5Q61qwuJKe8Z3Uy
GijRgkNxJN7e5eTKXZIIThUXHYsOGi2diqe0toBXBAJuB85DwMEIXNkmkThjuAzQTvmay68XdRHl
4njBvW2UgHGkwhobdpg0Hdooi6K+5cTSPe4hK/8+7tAWq843BgM1Ju8L83MKcmG6A71d4WGBU0dj
5YAWbLKLOXeP4zl9VQTo3TBlUfmekm88M9ILZKyNXTIIzNr5Hkv7vH9sk7zrLniFZOONfVuQFLkg
y1w6TVpAX8Q4SXLsJcbDdPsj6t2yyPake25G/Py9ynqLITta8DuDDrVbfqInMp67+eM5Q3OZEbY6
cJmWTCyWgw3MHZTr+X2sW7xeq9yQ/pm9Y1HZOpTvqSid+wjyOnO4JxPIMpU8/OUCuO9kfFoG5A99
w6XXjDBvhKoBab/UBYIpTIamnS7FheOZJuxt1jCsQG2GRMwdzgsTzMD5j+9j9s4G5mHFxWyaFa7u
YBXnbShyMVYbWqmJ9g9rl7FwtZXHrUKhs4eNYPx+RdSRdH+eIaqmDQac/5B94787pMA49vGYuCcw
7LXD/UV90ZfGZAf2Jnyr1B2v+MH7CapBo0SAGHIZcxuXhA5wlkmCZI9zgIhk1N7jly2b3sVxWnGO
j1TCNJ4UBbszVwS1nS63TZ30H5+C1lwSouWXSyW7az9K78J68GJfaGn54Zirdwvl5aws8SCaNc6w
QDrnAn15hIF8uMNxsRpR4zhjyS2192SRTfVf13dT93ODMS3K9hGA/SIt5uSQ8heEqrkpA89LelsE
giBgXuOnojoar/YKrn2QWgggUlP4NJ75HCN3BxH5SaOe6o0Ud+JNsjMxrsIoDt6vOPfLWk+XDeWf
Q0s6sU6v88C0tYBqc8rkCpjFXOg6HofVe/FiE1qGqK6ueZcn8llk6q3wwVy5Rbx3VHyIuxIzP8PY
3SDt9EfijhQYIyukXOq2WiRRK8u/0jWn91aPJq2H+siuRJAC4p9C3BRhscyKwlGhUPIkZe9gLy/4
raL/13yX5EJ2MapeGBwO0oJCZG0JPVSuT8NYMDwHjIAnsi3Jix18usEXeB4nTNqYUGR/0AtnZzE+
b/Qf+QG3Yxrl8DmDy+31dq3xYgJebVzWZTj5SE3lT6L8KYlqfYzR6yPMlF7tEw1xDmxF7Qa1dsIC
3W3hWKY0kKLt/VX8deniIlAjDa2+p6fzv3aALICCnA2PEBCabnf7Qn3rVa6VqFfdjFlqgafSOTpB
2o7fIEvZL4gO3K2SKH/u14ZfsiDq7QUhcS0II/ZITJVx+5mRR+71s57k8hEcqHN/bzKKc90z9+Lt
iy3egELGx6add2SgWBlFOMEfAj39RytSqHgZVanHnbgdAJPx+NJtnqx6uqKLjrexoUIIZEwRyMpA
EgQXE7elyr/NGI2mZlszhOl9AZ7/lpptUVdq1z0LRZJFA+cQlM2p4uRTFfSSkHvDg3cy/xeNo/V+
gZrZLSutRsFn+M8mH7aJ9RG9JtziEAvhfuoaY6DGk8/koUiyfz94XFgEDLIxncMkoHbO3nlIK1XB
IzgZJkPS+ZUPU0mYhI9JV4F9OZKjmOiSym+8yYiYev65fzRSLroW4sP/utdgiNKmV+roTF04gFPe
yhZ/KNeNss0JQFO1pR1os3TABtxU2MoRrjq7/yHwRui3PKjWwJrdhfnbwEHunBVb/AHUcBvHBJ6e
ovYn0Fp/ZlLZNoLGvZU/SkBIIrQmOCu44PojXcUKi2Q22r2Q4yOyKVEKXrczpGOWmv7Kxm7+CpIC
1NExKnN6CRWz4YbG0w8aZmiIYr5BXpjrLvy7Il2Z7hgt56hi3Jj/bhON3BH0m+pCggNx5EAWpVFy
Ate5ySS6h0dA8VUyYVSsoW0AHrnFuWYKgq9S3XGr0E6EHjBZQdzq/qYPbAMYGh7Uv+mSU4+yAgr+
QWkyWk2CZCVBHtJUAnEjxAf8BBHyXYlQudggpbNtHhC8PIoEmAcmmFt/pC68gETBaaE6Wn6Lisx2
jJtlsfvp3DVvhCx4ePVzGGXXELRU1EQyoCbqtnY1dfAbj2A+O2Q/ax4Pa54bq7BIhnD9JtNrQolv
qn2l3x98Qpnd4lE1rIL0yw9/N+g4Os4SLuohd/ZuU0x+jkZRJAGbc8lHdUEXL8DusqTBvclbFeNE
yTIWBQ1qEReOkJdyo7V8fXIZw4FbadEWMIVkjNnFpSjwBp/+3JB37FiFbRiZHBAOfkBq2ZBzliT0
q0DCYezb8DeE5lKSLYO3mxd7bpQxsalUsS9Ro9UWrSEbCpKD3vim+wGBdy19hFqh5qqky+yLxiwj
QNto3ZmUUeRzdRIm/gzS5BuCJyKafniFGztqFWUGrycTovMw/ODtvK/aL5UJ3JM/cnexkKprVGBH
e/FaExuRRRl6H43p3QXJ15m6QfQf1gkv+4pipUnj/KOJWJlOlwT7q70H2fIJDpmgQGjsMlydXvpN
TOkrSd2rHp9IVkZ5mCToLWzIROMR1we/kmLcFY2lUImSGE4F5U8/ET/SHhSUOMIt/ROk94YvMK3v
Vg0v34IcBcToTyYta1By87lmzUGmk9HW6v4nPlo2Tzx43rpZH+p04zWqH/pOsu5Za8ns4iR1xT+L
upSRrX02adP9dspUdEO49QgmlFDyp1VwBwxOpYeB83NPK0PxIFzsFxaWQjR3HxOdAD4DLiXyb88S
gWAzcNN+PV9yOsyuVdBhqIfxxR1MkZXJ3ZpzGPfQqE//V6d/PRnXtLkIYTFqfgtuM42gQjsffSL4
Fgb06mIHxCUjvpgaHzYPihnQseEcBGglIvAZ909ZhhffL+ODotoyGubUkxXfky3QcbEvXMOe3fwY
GdRyoh5JjChqpjVJoVDOpw55TLQbzKFXXeM7BXx/W8bUFBnjzt/1qQoprJOHgDq/qRozBSWBlCBr
YBJfZ/UZwrpaQ/rwp9y97Hd+kRPZgyEUcuP+TJIL5Eh5U1Y3vwOBzM3LIQB2wDAqaDuY+wGFfvUu
aQU2ZqWyQyAW6x24ODim237pVuMHOtXOLjRpqGwqiN19u0RN55XdbRxhGTsWK7Kwt77ldGSRxOcn
r2ylE1ZziQnQ3TDK8c7iumtFxenmuQ0iF22BkCZDxcH1HeWiQXfugNE/9oNhrycZDrdgblYrcKhJ
upAO1EFDklvT1taHexUpw8ntm8yJXu1UeJCTICQ8W7NhKhRPeLkgSp3kvAPfEwxCIArMRlYWzIeG
lXPtjZMLjhAX20GrqIxhsItt2V++RFLLydo0ajuCUcWk2r2KnkSYd37sEh17BCehB3gSaOxtbIFc
r514otaLFM6tFBhWZDnIaMH/2o+So3Yr/VAQVkBAc6tw8Amay7QD8XppWx3IxxMz60Afgbh8pK7p
N+EmIOfwAnUpBKWdTwSXig1+1yb7B8x3Ohg2myAnkRO+pSR3dtMW0iud3h1iVXIZ/mXCeH2/ikkA
34Y477j1Lx/dk/I/V2ceacFYXjMS7XMMlmJV1UvNAmM0GZ1MtpW4XrKHBGOLAXuBB36SzvhKFscq
zSUEgn+usESdTSNnIXX7heUA8cl1ZvFhr2nm4DbUfDex7wbqtOTXi3YnS2MXKgIN5dZ4YdT5jB/P
TOMzudFKJRltEDGjQfvJ1e4x+sN7uNhjtX/PCsE48zdx+NxJ24jJOPI5mNMb29p2jfqliq3EX8fK
DUrVjPV9s1JBsuTsc5LOdQD1/o6BbUr1s6NNDyB9BU87LWjS5tuaPlDFFKbh3rrJaejlaCpalEGp
MMiuY0Gy5ZtIcHh1Y8Hq2d8hLqeOaMKcsVnm7dh0Yb670o58VFm0UAE3vQxShPdkp5I6qytjIY5O
gGNuvS8g7LRgaO3LdQHHE49LPmbPZYbRgVwxNTzeK1sNvZeA9xMYTRr7+R9l82TgW1GRbbqDP//U
MOhdTwFZq/cboWb2fwTcie78020XJyvIPeMYGVIer+O50WR1Dh1Pe++c+d6Gaid1VryOcdZ+NQkj
C8dZKVcsZbZpeFxVSnbgLVfwQwHbmzFYqGIiidVh77ViQcRga58E2JPsGYk58N2V56mdMr2k3yHI
mKmNCC/Ba0Ec4ZPQ1Mv4jGQe8jPO5fWFIPIB6aKIYybVxuWwe2oIofCKxkMvzyAEVDSM3GkeQMcX
D25BW6Yi3QAyQhRv94U5xXQCjbUnBXi69hCzirqLVT6XSPU9jrV7Q11x7Uz91gmLGenqw4e26894
G/0nksFZTPRTvF6ucvEbdm137xuKrYP6y+anrIdLTy05MMjN+CWgSvSoKrgiGJlBHg1gQ1mjmbG9
bzbOKJUJ5RaOuRks4fUM6KbEVwHq1KrbOKmlXxmm22yZIxNFiAqSQpZw5HluxD0VBakE0TxDgY6s
SqUvrkTeunVp/FDqzpAcBCMaXpcWzP96LbiP1YTDcWNollJ7t2tsK4QRQ0ihHc/rQhZHNYtdtq+w
83zMOB1FCdPuiJFHhYU0OrwfLnzBQ6sYtxpC0wB+RiR7auplcz2WtpzbrPUKLYmRpasSb6tDWG7Q
TpCxPjCRH+ET4Dd85p1f+Ep+iKAYq/z5Jmzi95RViy4T+2eIbVBDFIovFJu1/xaHdECrwPamwoq3
h6MmBLsCIL2dy2k3Yb92jZQEJTg+46o0sIU3Kk1LprZ7KpyCuUnfdaFAxRqr31clUCVJxOGIM6eY
FhdUMjcv9btYlf99y0pTijH9+lIKMhyaHmRNzrLCala5IpcD0bYVZv18/aFLAU4Z+bHv+FUwHvg5
pIWKr1/ullYXvSQ4ULo4Rzd3cYE8fFC7sOu2EaXICH9ARfvamYD7TJ8HCGEQlgdhWHSgd3RGnwST
e8nbuQHH93bd7u3qrHco6FjSjQLzqF2WxNLJ8e0Y1Wx6y790qWnugFP0pT5iahpKmLlqVzHxArzm
eUkfGpmMtjZmVT2+1EqkqqbLz8aAlR7JWmlFqRpIePLyi4GYxvVddCGH5KTyQuc9Qayx/AGYa0oq
5LLTjRQ/D6yyzGvJu45rnzPBxTyZaQIm2fedFp0LsDlumCbeQrCi3POmSOyyrQdJHzcGXYAWqMAE
SuubovxK15U13ocChaMF/qp6rqVu3YytOstKL+d9kEYVzs3AT+6hJ5ahn+jqOYKtQxO5Be8ON+0V
fTlrOQSStx4ra5vMc21sCVQkbIm1HhHYKT5+mQvtRGTaPpt6wIWiY2ym1voVQf4pdxoQGBEWuSdc
GlS1vs1RsXJK/R6NUI0OetbexE+faCQRK32xY5jBEDdAayZe9FhZzInzGex2goavmly6mduFcC0q
dwdMwl9SAF1uMzBDjxadYLzw0I6kkBsTWEcoI2N6X4//oOwgGs/fGXTtfG6+tk5D93ttfT0ZTksI
AWlI2hLnn/DOUTB0GWbpFlE4Dd/n7++GYVfkrGYb6j2DsRVql6FyAqccQK8Azl2SNEl2HlM+ukQc
9BG5Va7rLmhYokik5eqciqIubaEoy4QiQPmYaQLh2u/AMJ9LaIFZJVxFM3CtELeB6PPIpZdSHTw1
7VQnnZQX7x498HmICNSYiL4sJqGQO6VpLWuWUI1p1WfjUfiw1X8chUA3plZnh+2CMqVM1HLKF/wB
kXBWnElAQgx+mBqw8qsHeo/Ws0fY2Zu4fZ8MmCHM6R3ch9WN6o+bgjXp+rjMARl/zR1wa+CLdMLX
lwF8yshgx8f+SUpzZY7DRNypmLL8VP+zZBChTICUWUBVZMejDQt5VILoy9xCd1WK0Q3KdkaS1AQx
n5k4okBgExv71KUEN3qZHjAjkuDK2BvUCg4DmzP+U2TZ/e5y0K7/um/nRw3nqxf/8L/DiokpV09b
IbKeHpANLoMSv3kFzlmWBMA4uCJ2EZmozvRlZ7mqNsbkXbvtNC8DX8vk337aK8bggNGR743cl8Qw
uefR2nl1dWCWJG/NDm9ibOpWB9jDgbHoCZC3hrSbaQcBxDHCHrwQcRSJsNUEJ5HgQh20TiEFTddF
d4VMO/vvhYJsjY8cGvgAqprcnRicMQi7kOOuFQAF3bZ79JegwvX813TvH1wjlWTDLfNxO/JDa/ZZ
PXunvoggHu+HttVx/5RgIrTWHJqBURK2UnF7MwPRByTuRvtZ5dt61r5xnlqkqyGyuxvDvOuQbAiZ
1U7+LEUfifr58oOEzY2XwTfXIZyv0RLfKfNiCHk7EImL/q7xatJp/x5uTaJKNpTeR4OeXJ4VqeLK
Gbaa/tu3Xmp0ReOZ30b8h/keM/tgoCpUfAZ5RXDjPcY6URqbI+qN3Q9vYiCDq3zjpVChqcVY0lDk
OnRMFxPHmqY+9aTP3v+AQnOQ6rxQGurIzqGoshDrQgdFXriFjKZTxRcL3GRZvv3D2Z/veSatzno2
TQ19DRm7BXpA5FW7eP/YKxCMoRxnmropl8GsGWLFKEppMHUf9rjT0LGHjU3Ki1i36deQmncIvQE1
dtIRexT/Rmqm7ng0amiR8prnzCRmqcG54yNtStOpm35UZrkqDP0y+McKIUoBy7Gf5f8CXlGrsart
ooFGplllzbBQnwuJT9mf1Ezm2uyG2x7H2DrqAD5e49HZem7R8Yg4wJi1caKBB0ccbr/tKL59YPaE
qSn/t6NRHyf1x1NLdOwpc2jKJqLbv0GSXjRkp6fwL4F24bG1KhHkGsYe1zPzP+xrpC1Er++CLtaC
cwshb9W4mkt5E4dK8Dq84xEPLh3FBrZ2L9Y33Fn0N3YNi3K3M6PXerolxBzrB1z0DDLGpUPRXuUb
0yr8zKj1oSALdRaaCVWlXbkPdcXLbRLmRSLbyEIqEKu6OCCWxWxazoHTUqWJrQqYAsyN6Cn14UvE
qPjl/tm3sB2q+319ICDNQk0wqwUt/dJxrSml7P3QE2UakfobIp2SMyol5sULMW7gZO2gZ/zvSmRL
QIuNszgbRVw8xDZAZulab9c8gHbWuFVKbslru9kvB/XbqkBWjAzKS90KaM1yLZv1eZ3wRqPPIvfx
XpOkyq5oS4XSKCJXSlrA1b1uEUnpSBe0+iSevONXV1crw3kxAWzIMgRrkU4rIdYZSew2I+gHnFNx
lOLngh+gMU0Yq5TKeuRBUL43C4WlY+MnyO41OKVspz3ey/L5CHGUBIBwxZPhOf+I9+LaDxKv1Jft
CnEnWJpEVvGYbcvP6gD0+txq9+DmqwjmDWMj8jb5kdid9k1FOTzvapDCeRl51QD5anA0kWV9WbLZ
rASKon06EvgOSYja3fpgmvgblIxClX9ulvYpu2AUn6j/wCVSKqTdOUMlS/1U59q49rk86uW5KZL0
UpnXQT7dgEymS5TX8o9KzW8lC4K2c3WMlM60saphHb4fCLSeR2d4pYWyQHcQ0WUQ/0x7shStWMk9
WMTUKUgCw/RYCFLpBs38ZCQyjhla8ZrgzPlHvXOJx+ILdJ0APuAddtP15ufPtvpEOZbGscwQ3pKP
mNfkyjlUWtmXBgHeSCqsOc5mEsOA4wdhYiFtUlvpEBX/ENP13aebhrTEqvcKFrAizVDVazNlox0i
0VHDp591URkUXbZJDmQP8ypUlJANB0bjBGS3zYAOz0zEaBGWz62o+48XNPQJInOBGWvlPrbKs2iY
z6V8QrCN8WQS36kpFYznx4JcdSzXjQsCbXrQeR0OKt4NE3ogtRfg/OSV5fSxZkk8TWStuah1vLea
ZoYI/68raZfYQcJlDl/J4jWu02nRP96TNiQoIVSHljSVKBIA8B4yiBot3Qhrb6/nzk4x52nSZUCK
k4ERWtkWY0sKa7f9FuBUYGYfxw1zazlLXT8HpqPvntPKKCxnWZAURP4QF/fL/3MAgwM9QzYYHyKt
2zxeDMHb2c7bUy8YHpu3wgyQNsrtDpXRzixPbiY+krcAPFUX+A+DEuyFa6g5wltqunNdl23PRpMm
bK//McWkw0Bqj1v/ntfq0wEJn/9b9ILBHHaQLjgzFJYOm8upyMAGZ9DfejGTOVlP8jcF7X5FhQMm
oA4vxsqHuB4ARkU7j9zzTQaNxuDJsx9w9bmx+MIKxRX2XRxUS/phNlyBM5AnRngllXzeSBXyerK9
7Q0mT7cTQ6UGjgpDexzJWzoDyRa4HVk8OGJGdz5Sq7UnoxrQ6DkWrOOWixNe64PvpfyA8G3p+CkI
GwNfQ8fvF2y1Im1asXxnjgEhC5w7Yb3XUXJG/WaCOtelTMGa31q4iatzL1rRYMswMGovdSVIITay
YLna4RZqfjrt2AW0IHYWzgB7m3CBzDHiZMdx4kiTZcZs3m83YLhC638EKDuCc6jp40TMAbjyENdT
1kMeE8AAd24uvNv98S0Vt0bFsH/1W14EB9OEITr92xhqdSlIS//HKDprHS3QqTnzZ1A56vWU82pS
9GqsaFelTUsXatC8jqbVyRhu0kFAhgexlDg5G2G0r9jrg0IY/vVNFi6RCxsDHNgPC4e7va2WpW+b
H9/TNzHk4VKdd4k8Cgc57N0QX0c6yU7oIk1smLPmyP7Xr+TQma37J/82p07YbsiWjmMX4ZLaKbcq
bhJK8bh5gOUIElm2LMmAr6/RSHlpj42wLY673H29ACqYWlGt3NESZoiEJwky2ceqaV4232xvLc5+
jyv6G6awJmPWSWJ6j8IOcTeshtpjo3OfRXKZZf9tXdtD0kPBs6ChMV/uATKRaXYPvMGYJC7x1ExH
Dcm9wicWXL6e/BLOBob5Bgy/ZiHFJjOttEiTalSrsb5XhbaULTweCsxR2nl5bvVb9H7IfiX72Cbz
ql6cNIpuSwK7zVxiMoRdAFNbo6wBZB6PWYAuzDk0Flt9Z4LRqXulnQK+ezkB53hYRmMb04TIQelW
WBHsxSybpc5OMeDQVV5r6Ml8rxrGiwuwxeCRk2SidqqWHsTQ/svaFB8vtbIxLhplLJR/CWWnazTH
d+n+/qKfs8D6btUUwmKi9cNmXRGjZRN8656+tQe9GMNmDo3Y1dR7YEEgPEM0bP7+GkOB6mJpDENG
3b9VxfG+pM/qn59jn4bA0og605mqexn7IBgJxeiedM4bJOMp8nkKI8IaUOBr2ucfg3VMD60XiJQH
pheLqmQ+/HNd5xQpkAAZCyDg5geXYvd8MCJGNuEqLiAb1c+JyNUier/hr1H0se3vMJ0MlZSz51c2
ZjifxiuBWDrSR55sP4GSJLWPLc9OTpd5xLiy+oCgKoBKYIhSb64pecAhsFkrSB2I2KfdfZRibNme
gpxOpq7Jh5CffChjXlWKX4ZOkAlURjgMmzv9h8K4M8Ysm4PyB5S/GmBEANGU9ypmdgOE6UWksgFX
RwLyGNaIAcZbQS9YXtLingg4zIH/FtMxynZbwVSeErZzYUTVVzm3cPbyj8hMflDL/vMIsPcazE52
dylGIDNOC5R59SRIH7IsUYxQ53NXhy6PwuoaM8WCNRgKGk3MD81wx9CWXdCW4xDZw1WvV3enW7+u
uKLYeCyYal0G7Y2GZEOnr/n3DEnk81p5PUrG2tYnCHea9DpVvP0Ka0JppAMfjVMGV2xRz3Wak76o
XxrTvKURU6o3uBL/On0d77vt0oA2YqO1MI8h6lOVjQH0V6K84osjB4MUN50YHH9o7fEeP2P077xx
SQvx6yJ9d7ABnZW9JTvl9z8AA8hh4HHbyRRAeu/biOxT6WkkeKXeKC7JwcWBBMdM5lKMRYa3fUyi
HUPpiUDS5jXCU+P+aPuqADOoXrNGG8pZLTgbIMoY8bT0987QHqBC3OxJq6844MJx8D1jhMJhaAKZ
cNMRTGJwYSRifquGqvsHDfSBuslZaE3bKSCxjlxu/68XiwXB4sudzy/m4YpQsTypo1yRfMRdgQkX
66DYa/hjHJgHrhCpcG/izUudxbM1RMbTIcJMOV35eteuGgv9d8IbsGgk+EK8yICaVZJi5T9r2fHG
tfG7ZIDYg9oJjcIaUWkYwlCy0+mnCxLIpKhpWHtX9H09+Nx5ynCXQH0Fg7Um/ri7xLYcX3rSd993
5Mr+rJhFZ2zKFwxwhLkGgPjEPOjZjyYWjXLC4xFWzXXyGUHTyx28FtXn2HWKfzsyQsZ74tYs5Tr/
knhowKPZJJQhHQr0JbgGiJggaPo2fQyomgR6cg2M2vlc9KoSFEoO0pRmFXFNoC7HNk6Jtu6SbE2u
9Yf/zGTaqt9ezOwJzmtjML3zsCvyMfeeLUZ/J3P6bem5ct0mw43NsmtbL3DB2JmkRfeABRO3R48E
CL+9eq7e/oTnGso5pnsnRzbSETA8e2YjxVffRegmo2bGjvf9cdFvBqg9p4bsid0q59qsJW0p0nJ8
iPsHEq54+lAqKb+hKY05phblmMW+Y30zqjr5//lTX71ka0ZjURM+mFQFqKscTWDZ4LqY5qYANxvN
ksJZTw9R9dnih4455rwhrb4OYQxaHBhlLpLq5XGT6F4Bk7UbrZvcmd8kwXztHoaezgBCGgvi9kFu
4gbSWOPDfH2pC9l3rHA0W7j1o+NnKw8p57fUjzAz4e0JleaJRv7PJ9Al0sXcyrziyzzMJ8trQ+Jp
KAYu/ZaGk1tyRsxx711osofFfiC5ZuTzUDwgYoNakDnisN2fyz4XmangMizB0M8upTFE8d0+BG48
IrYQr+g2c0jaU7qQ2ETQwKGst820HoyErrOfoyEJ2LeiEojWQnnrxNmv+XcLH9MLlohMvc/n7pC2
qKqUy5gFfZ8oXbymK4RUdTSYWWxrH2gMXtYN9iAOg1R6BPxc8mJalDgBMSVKQqN3ULcWqda4WWVO
0KMBoezHP2xEPsNpirmeJfocQOhKE7rNNuqMrzGhV4yYp+SQlA95qA2AGa9mqQBzzXtqBSZAB/fp
OamZ2QX8QIA0SX5KB1/QjERklKV8YJoyFW2X8LLBFa28jW9K0w5NgaEsIMehPX7psnnuyC2E/WMw
Iyxi5tgU/WM+lt5tHilvsCzOQ8rGqKHnx7dgzZxLac3ae0gy8TlMEzJ5mMgxrf8ZjLhbltKBoMfs
uW/vDVBNLhMPur/gr4KKGRVChN3abLPLy+Wp0XbBZYOGqw+6TWceC5Y0jqUvz5I3EN8yFcDzRea2
9klzpsP3jFivvrxsvbmVMfsJAB8CB7NOGeIkRrQ+TAd/cfC3egg6UkE+bWqHlM2aJct3ON99m6V6
/0qVh5jfG2LT21mP2GYl6xM7vTDc41VWc9ag251gXHQvdv8yZtGn5WHgBEdPKN+65nel2NRGyihs
vU2diHFoq2b1X6ofaL8xTpd7qiMjHWfYM3p96RDkmxA0bctdVHFO99L4F0TQSzIscUJXXBATwW8y
iXyv7fhylUTifb3laTFgx64mywHHf4jCJqCtdk9bM+X8bR+PC9Jintd0pgsMesWVDx7xZ75w1XnQ
yRrY+jN5vxp5bXmtuv7PedRV3uEZsOiZH7SiPmN3Pm77kgZgEPqpKC2APqq+FswTHoLU9X9W5tcV
8oo+ejK/OVwEEmHUWYGIo8NWMI3pth4XvGY2Gwm8TRUHkYk4Rzhm7L7hSuQVjbwtTXuSpbZL/IGa
LrabhHuYRRdsUWK69bHTdPssvwmjYmG/2HN7HxUqFqP9iupKMlZQSwOdil/zdjwEUxpJphOCT0em
J3NA5rrXfExPV0agaIi/IsP5RLxd1u0F47FSk7WDpM1IhDdSVGBsVB9uAumkZzi0mbtNq24+9xfc
hETW3F8qINDjrtq8NEhi7oOImDHRp18+GXQXytQcRFgZ9EYCDGkAnfY95LXpegOuh8Lq0H3xuebG
ttEku3ibOSG0t0WOpBEvvHfATBO70141b2ukvEomUUmk7iDtY2SVEsKwgkjO1JhSBllsSSXArYgt
LvB56EUtWjRMyu1ynueJ/Hxk89OMtXhG1xvUYamgHDJPg5is+VMyDA3Cn3Hg2t7t6J75jH49Db2n
USyIH4HA3TFloBllEiQPzH78HeCzbB3pHNssNSIyN/0D7zwb2ekL+v2f+4sHwVx/e4Iff8mXJRr6
4bu5BEchWAl2rVLDpOF5TmEQiAVf1SMXoEL90aOiPTwHNqLV4dJI6wSY20DYts3XioHZ/Hjr+t80
o3onl8A5jxvYgSb5tJKcW1XdWy0W/O3Z4jmveUvn31o4jnjnRH5D/kkbYJNBSFJA+GFbugNXhJ8l
QtFzZBKlne0pNI8uIGUnc5QUhhyaSK8rr/5TzXdtumy2rKXuwh2ZbpT3Uy7NTcgRcbP8nOWu1qwD
6mUkf9pJjO06ts4L5GULLlyAu5mPM8lOKTwNtH0p7ppgsKG+Hby83Q8aL5kovlp/Oj9vjWJgckUP
KOwmHptPdaS46m3jftz2VMEs9DgWmK3MPDIEg0inoTPT5+Two/Pj1yVc1kAZJYXQ1tGMzjLDCp+W
oZbDlbzLoiFaWGR9SajSRqphZnpJ/d2Zvkqx8jq495XivG3vkX22VeO4n6pcA+trXNtUJeY+Zy6Y
QzujVq5HlZC/LFFJoeJYEFGRFSNLHbwXwm7H8RE6hE8gUq9W5QXViicDNch6wighbA+Z+Th4xE8r
HWjZU7hlAMrZmy9VskNaas6AHZgu8AmVoAeowY16RqIu6+GWmt3/SckhtH7Mwt33opAnmWN970A+
QvoFzhd+DHvfhSHa95fnqgNgS3RlBpMSyxlQymsKdmbe+x4JaqFALA9YtFs1uGK+/loV/zqfoPs4
6+6jdVTPiqjGLlTRofEsDEHNvXiRv0bytYasAcRdhqd2X6tb4DT47ZFC6InLHOQjFOHH31RaOEUz
pN/4fGnMPykDJN0hoZAexF8jWvPZxSxFbaslWqDm1M1+DT7lC9U5FbxuAEjyDCB5puVUvYF1QZVq
0+q/k7XSwX0GJlBa+SV/x0/Tw+6wJNNRUPEwmabM5ecprAXrp/Tchg3l4pP7pFNt3j3y3wFm1q0o
FiYOyrRpCVGZDpBNz80caHWcQGz/nk8JHvi3uCGtXovB3D0BFa1j2yF3JameHOhKdYmUh2Qsm2oe
rf/+aVbimxzrKSYgVr7Z42JkKXFz+Xv/ujdPB9H2wisf0XX50TCO2PkmaG6+JfG6AWXfQMCLP2Pv
4uwZ97Ye1YN33mZ6cTSQyUKMq3tL1BYhRYzhiSZljpo56Fz2W5o7h+522W1D67z7HIzvbnnDxxZr
/TAtKy8KRreEa23LVn8PEsROySBcqgKBmYtwuhIhcLbU1pzs7n83KLlbtVNh8EK33HYbERlHn0x5
xhGzUp35t7f5/JzSwds2p6FaQKnYmjO7WWK7z10Sn7g68Wo5+RpuKM1rzsYsp+vJfOmdTVgemz7f
AhMJgw0xd9ZV1Qb2Sy+A6gEI/3h/tFEeX2j/1lJWDtZ9lbaPOSXYxrg1JYgWY/aRqAEwaVujr6LQ
+2IjuuKDdkulMXwUqroMh40nOCex+flW9Z3lA3m+P/XepX8j0RYOrkowwTYeWZQlGDEXnkuqd/jI
W36vS3aBgbE4fL9bkbUxGHwnYXPJ8FbW/4Ak5CZD8bEySiJ5PIELTJBD4cI1RJa4Xc+0+YtkMg4a
DR5+RvcydZXXl+PysWmeu8oElL0d8aCI4rkbdJjNqhmT9iB8uD3ouCjfOfdIn/gK6SmiO0ydBoFm
7XojQek8MfjSZyMc8NvY8COcCXdS5duTPmkNDl05PPZNR5KWZWG6vjKTatrQXRXWWGXUAz4CSEqS
5BO780vc7RH0SCvXIwnMMrbXt/TDyOG+9OZRyyfNCmHgMtE38EisEes+Gze4ouFSzvfgrCLbG8iL
oFkLrS8aKtZvJRoHLupXxEzfmy+KzLP4ox/ylELNjPWBUcb7D0foJsLtMiMJotBC75e5O+0wwM7z
3DWAFWsmp6+BJ8mO7MtvfjBlA8xQfhjp86IsjW1uX1gbT2LtFTvrxsPtsT2d0ghPx4swpHYMF+u0
mDrivBJVaKXD+D1yG3culQT+l03XDbzPevQzzuiGC9gdav6Z/7AGPJyC1/LC+yBMpdU1LTsVwJ4b
of55dUIXYKKI0I8ofWX8yx/uLaQiawCpYEXHKEe+a5z5Q8wKsoy5DZQ9ZueT0zbFZVaqGrWce/S+
I9XDM9xi7P3BJTrfTvQMEeJjJvsjzCpGkoUwcQV8/KiYm7QYD8GGuDWmXAKJrVm3pUhrMcDPDMDn
dB4sFfZJ2h4+Qwp2euKKUH6wP1AMbWXK5dYnvsEvX7tAVMdfRuGUMRBWxKWs0b1RCj/YcGDkRf5O
LbOZNQsc+bBofgxJ4SoGBjB4gNzlsTxFX0oP82QGskuRloDs/ea9fxIe/wCIctwP/fGdim6eR6N/
ZDbFap1VxU09lrUdRZMueiGLkXRlqS/oR6wy7gVBAFuL2Q8UcV2TFmdGMA1vQfJbQ4jA/W7JErRr
PrZG3VOQpNEV+CJHDAlvorI3Ye2WpV8SsdiemWGblaG28txVo2MZ14uTXBVwdmEE5RpD1XK6Zyxp
B0Rot97hSvEJDJQVzG/XP4LawnjiKlZBpPdCliH4Vvh/qZrSvYDNNefd3eC5lWnTrSYmWJAJ0BHV
MN5tK3OrqNUhFiUy3KkzYESuvPYznPbPofvIlWY0if8HIQjQzhhzLWtrk7XX69VvkeW4rbhQQMWJ
5bk4oc+Np5uSQz8jrfvERD/3152uYbp2ILXXk3AEhzg4kUppoanKuaQskpn/0b8t/MNOFnu1s6W8
62Ngozq+/KJbfIaR+obb8UtbvwdN9ltPkTyaa0aFCa5MJZK00t5qtYbVUOwfaTC8Yzm3XQ5t5kLn
z40q2zmo0KH2pJO7M8wov4nqZolvp7wgHqht+plkUEWSgvIIiuJCX+DkK52jJEZmqISqBML+Pt7i
f9hpbBbqUdfqVdzgDDuShoSr19cICmMIATiW9XpWNlykSd0pKSqn7kPqiRtmPdKyvV4i7LFAHqLN
PtDIE914W0sEmKFb0R8vOqsqPt6ZSbpumEn+kz9gfzvbkULTMinoxRuToTU3dCqOfXsI0NkeO/Df
xE+UDLO3eqs7dJhRqNFxb2O+UiR3vTill2Z5xjiJgmeWq4fl8jOwcK7vPs6FKS9sihh1/uU4/xTg
gsXLjnAto3o0WdfctIK6a6RrrOcBIjxscst6YtvqCaaoKevvbV7gP4FdYYY67ov4rdSqA7CfJWQj
YXWvjo/R/9aWspSPewB6el1IwLWBs4lwhOtMU/w/D/yC4EeZg8Doqcd4aUm2dZUM5aujzxvpF+An
pCxYk+cpNuy8YaWSZ0um4KWFXn8OdOCSRwRcg2dgNW3FNqYm8VzUf2ahxi6to+7gM/12TbtvGmHL
bRSMdbTv6U8jvWRQmlm4Msq0/7ebZ9isEg+h+pLc424PGN9gnR8daDYMhYuV90oplfXP/7QKTvDN
Hh5r3rrs0gr7avw2CseUWz3g5efDb+/B3PqKep+LotObt8FL+ijgXhG3/xfpdBxRDig3HDCqDueY
BM3VFcNq99ni7qajDdC7dgg1g81YgWB88f/VrjD+i7/rhSuEbnaCgqb/HG/f65kk8XY1mv5bMfxP
vMX1wQwlnHkl24zQIafeYaamp1+2qwE2F9yrn7EkhWGdFBvEbCksMo6/AGgqJ/V1Lud5X2gnou+r
5EQ9UOPqJQ29lAqe9MAop1xryjhXI7Ykjsys4RJYaDc6vn1o3dii0v3OGdMaQuY9p14S+03H3j+J
87mf/MYTMBoZNaGXuK6xSGKYBF31h6EUjQ5Nfi+tqwu9S4ymPL2UAFLxan0rS4eZ+g0JsoH+J3Sf
yaTr/Ms3VsmLNMEm5OXGqGgnLjPYez1bvJmnBoSFY6WQHlkhCt5EIAqao7ZX6USllSTNCVb8UKvC
xjpO9S237vNHXYKUGUmlnAVkgQ2jYeSn8JL3RBsfLUosU5zoizhfC8ZTW9I8jlAReyS00HJbCrYZ
raV+9djwVgZ7kY6qM7ATDBxlszFjpcvkDriY54KadAxneVDjlfQgZj3Ui8pG4j80CvNQU9o/f8uO
dUztExKVp/bF5pgJQeNrVAFk64Y8Jj3MWuutyGT7WmlZgpSVrVLiMII5PAl/tcQ41cnNhjduU2TP
Wp0R7ixLZsRJWs40+KbzXjRwD5gu6NSlivuUMgjF9DiN+FTQTr5zF8qo/GD/UgxOSSqD/LjM51LB
DxYlWrb3V3O3HdjGJSFmyNgvDMQbX0wfSAwge9pKW+W4qw7KtlbTsqJXmwjKiaF/lpNIBjvZUlbH
zSwDjoin3Bh0QS0YgE8ZJ1AB3iOC6bz5QjXW3bZQdysW5hL2ZGJdAI7TiH4IHidvQL7jXqw8KLNf
361rpfl2ezzDtNJnxO/gzpqiX0gEgX7gMxYUQY2ZtyxM9PBHXMXZ+yu2XZpRtPHeT2fjrrB5eFY1
QVziAYeLPB/bbJ16+Lo2NqDf5S+HOMvarLUCWiPsvvXXqO0LQoRPJu3EYemZRcynzhMJZZGBGpXO
XRNhEX9du3R2LqysjINA97c5seKwXZWBcJZITeLzP9GRiSBUGfUgZDvu70bf5mhT75tgow7d1rPZ
eRz3lEeK439H6M7ytSsWVFm36EI9sllnTWw+hRhtYk7q6pJ/LrstT7nSI55ERYU90JsmdZfM1qxC
+6lxlOKKf2Y5eaAFNaja4OIyKimTR0TLCmDCZgwZnah2aut9gudjYBagq1xb6EWTEBODFBhcOuVr
CtHTc9nMAfstnVUSibiVz2rysZmcjugTs+L2kraGcvc185fDD+VRQUCKfkI5/Sc7DlGhJqUTfvcT
Ie/xX5H0zKmIsFG90x7ltwRKmUr9IY7FyFiHKzUopArasUybgy8iq/IIrAbkR/Qv1Iq+7tplTozh
T28N8++yQAigxQiVCKeIk5Kw6ST0pENm/DTclJBbClnbY15DM9ISZ/7HE1DnCNSYElHnlrbZYTXw
MxcJGNvmt/pu+gwNEj5lHJYTVdCJCiv/q3AeHAeOEHU7DJS5qfEhoX9zoSOw2EOKr1TrssWZYUHE
IXyqGWO1TSAtlzMtaD63uY+1FGjEKc5cMXDVSx8Uy6TjyDs80WupFBivP25HAngTQpujFzBs21Is
IpZ6a98Xy3T1opeeOU6CLe6IAQUGdEosOahW0txIEO+fE3HC61kue6Ee9b1c8ZfDRr4arkVXkwHq
JrE6SWOCMAm2lNXBisgVOAnOsLxig0/a4fhqI6RtdbuPELP13gUtOpc1TdvWULf26/HpXxBdRAV3
UD5o8IZ/j+d0g7c879NoazfPymvt7YDY5BolqU1E+L86J9KeO4mqqzJRcS5WIaLQ0GuvmWefucpB
AlxvFRX9LBMvo3KuzKJYtKr1XdUBw8pkjieldHwpScuEWZdcPO56SlCakrN2+/933bqJvE2xW7JC
XiC49D1m7+YCFYlzMXC1bFZD+bS0dI1AvaSv84faM90IRxnAYh7SsQJgsqGPcUFxZZmdGB9jllUu
gIKsFb84s2RmokWPv8cJldHvK8lptppZIyegtCDi4k6XpqsH5ZBY5nIPtDNwGGBxnb56sp8ulf0k
TWve9kBokZh6/bqRLMZyLqfGWTNjgPnVu6UUE9dmWfwWfv7gb4wzD8CkLBweibIcHdimnXxmnHJg
uBypNY2+92vAV9b1bI3cAFsCi5DA/V7fpgFAuBj3hVzN2E23PjnQQ2vcdiU6QyqvoZj+QI6Q8DgA
gSwKJA95y+1FZos2JSyLbWJMrf86MUcMnHVUnAJpbkscEM+00HwBKbhKNW3wBkeKMfAnoFng9rd+
8jFAC+2OWvvDorh82mkZbiIM9dBT/lf4J3cXYyYScn5Dsl58q3A+NmnL3Nys5MP9jGWwOGbQMbsI
v5Yu15cGItO8weoFekAuRaql8RBGI4E+WdUGJc3AUmbTCoucZU5mrJtPcO8WjqItRXK/6WfMEkj2
p0YR8Yq4GdUpbkCL2k804ogWKbQBhnt3nwNxYmAvLtl8t8cnk308vUrnhLWZXUZV98Aoy8ppuWI8
1sr5c8M8wMY7bBymzzmAvFgYSdGRnn6ERHhu5z4a/dbkOMsN1LnssEHK2C93JAYxvngzUqmPstQg
l/jphBytBiQ4e8yQzrNrrAY6djWe7jbMquazvlnWlXFDEKPCCX6W5PCOnKcDK2W2jKtpFQw4suoJ
WsRaCHHipOezAXsgS9aE7IKdUI2QRPU2Jqwz1sRj35UbibPkvMnI4APCEOA8MTShILRxRg1lelYm
r5I4XR5G+APFqgainQEWn/NBHAmPHCPAIuwoBYcQ5CuJ15rtvfaf4KdVi7XS6qscwNVMgRy/X5mi
q4bW8IfTW1T10fKypJQd6dM8ZyduRoJSdBRzBfHnjkg7UTwSClc3OM81W7VkPNF2am82aIoOFS1x
otHicmWwZ23jpVsXcoua65Ohtx3R3VvaL+U8Y5CrOdNUsZQ26HRETn0lrWQYeJ2nsR+hh2LVEHqN
AM/vhsD6yOiChjuhWjY/PTnffE9k2DMmWi7bcYb7LoBvzJX+Ho/Ps738jYATX5C8xAJtbdOnpGl0
WcxJvgoyVkVz8z0sueysN5KMiT8cqtSzZ1ZRvrF1vrfZzsWq0qcN02XFusKwX3dZZyN97xziDx33
seVrfvo11LspiF6zVOnPm3AKgQPJe9yL9rfYMGDX4TNHKhgef+6f9NUJFZ14+8oFjbKfWSk59UyC
L2qkpLS0+IVqiKw9DIwNB2JjodekoL50P1fBIz08faYlveKtsJCvS3NaIO1xk88ZdLFZgtQH19wj
/sjRHsY/llPakVoeEY3oCRrgp5ccycfHnwSy+lqGEsOFCXHVTj78RoMSys27YborcsX6fNq6ObxL
AHXPDL6QUz3xVDV8uv3860V95gVI95XQMWXzblsGzf10j/weY0xgV26CUhSsWjwGj+75QGpbxPi7
3OPfEB2pCHgwuTm4EPIPojux5Ny99rKpuhpCkEv1Sj6WBuD8kEIJyYD9bIfZomrR5ZTjKpYjUjJp
bYaenoHeL2AyEImyS40cOpad0+7XQOhD94KI5pLcDzPG4mNC4DiIq/K+S7Z4Ub6VsyiWZyG4EvMp
GCtfYxWUcpxsCYJhYfwWQpQ7qI7dLKK/qCXQsifkZWmv4qPPoe7vKAwtPJW3Q9oa89nBYi0GGwZp
FYMrfzoCWMyTJZ0V4YILLUooCbiVmJqLIHJiYhCjO2Twz7RnwFhT4zY6zg3RzZsVJYTpFn441jPJ
mqbueVF7g6kB3HWChwM/xru1NCWLBjvlboXzv4jsWJ5gh+Spuyd1QjI9+T05hj5G3UQgYgWF4IP7
V6+y2GO+Zw6JPxPaJDAULVMXYlDSKhlXDkbqaIU/lwimFtZ9cMWWErxKEkId2uTsj7mAuaF447o7
XyH1qHSHXdP8sNjZTfKh1QaMAFk32twt7LWaU/c05EEboUVFtLfButxqakfQoiD1uKnDu93b+/DY
KfAr7droy+o+GAbW2Tkg004v7iZ711O1Zm4cNqvxsz/TN1+9J2qPSOkgmdBTiNrb1HPvRgY9CnDq
M5BspLOafNNHSpadKZw1cO0xivziqmcaECJlqT2xREwbXSSWDcEhb1DKGkC19/D+xZAAMsb5bHAr
KXzdqFTSXaWpl6sYZPGI4uSAjnQwNupc9DLgYtOdj6RzN5pBZ5iEvXhKyJg6u1GtTIedfNK6sh4/
HHkTOMgpe4IubcvwfiWi1LaufcWGpcJXmIyk2uiN+VbVJ2+3yb8tz5ZDeenhGAtlk2o8tL2lZQfI
uLKNi48SkkV1TYhLqJTZ+L4ir2Y2IHJzEsTgB5d+ifHnBrlGA0ZmfImG2KccDMlNrNRUUa8FX+ej
gYG1R0VCqCQgfNIyQHPxd2ab5eOr9V2mwQGiqmjnTOoWHD2o37HTjxJ3uSmgMPLxLW8zhHbSTkSx
oRWylaOsjithd7D7yZNcR/DfEgZGGAaFRD7Ga9kO+IKDqWXvNuNUUvQjXkILGTd+/lXWa4ZwaYqD
9EmYfTbfaMdkwed3hfFzbrKzx/D8koxNF0K/4HYQ+q+IYH8ws5Znw4iv/Ygfme0/drQ00r4h+ILW
J2jjfURX62zIUnkxDb+vwasO+w1q6C/+oFk9ckWZ+JFdaP4jE+SdUVamoxdOCuv47fkenNTpTewE
YdizsagC9Mc+zmDnbX4RHpBcuTGOplGHvmQARI6sVDgr01ymE2lKA1qk93yNTdgdnunCF8kAzPmQ
RTAiyJDp7AmyijVZkJOW47aKZdbuj80OLRDDfTPWvw5McgHLPd8ssogNH0CY+Z7AKgXrMVriY8Wk
l0k89tge7TRixh0x0lEog334KkjtF7LPoHpX48zCxkF1dYbWWK/skGZHeRl+qPrf7PSqtngz/buK
wwMt5qmhLoJ53jMKNhZI8OR9ZMc1YkRbKhLjZttZUXqH3kfhbjt98VvdnUEgKjBlcmvyZBc7CIrg
NszjDGYUq566ZRE14y21sjLjpXb5e0hzDAMy1auG+o2Q4xFIXUOXzXP2j8vux94mHtzuItvS02CS
cRKda3MDXmL3hEyuGJ7T4u2k0CzKHq7C4F3UY/g1FktNnArOH+oMILC+kVpFpmVGh0yDquqJofd8
WCenwtCTEDAXWOHZF460io4W+2GsqWFUfgY1D+B7zeXAlmR7Fqid3/Pf9DGYu1cnWLaunsK+skSe
glVJtj51GOgB992FjvS1EbMaw02rK/J2qCrGT+VffbU58iPGJS/nSEznLBg1afVRnVqUdIMMhHVU
sMMjNTZILGUVq37N7wfIcVAvu1yj885SDj7c9MGlqcIjaUNuUoohb+9XlweEozDfjtGdPHssm135
miGAk0I53pHn1tHz83hWthkMh6D53V4rLW2F749wy2ECeZ1rxFbTxidbwfAIecZ8Da4hepNa8CA8
KCLApYMuYxyk20VtjVGZB2LdQ1DdKst+BE+0NXxXHG2BA2oQQW7hm1QfOLPHkQt3/PyRCgLtYfl/
WK+H2sc4ljdqh6t53cjAAMid5/tx5bmVFC1ptLuoMtcQ70pAPaDQVjtFfXV3oRNuRVzWOz4lbTSE
ffpS2CWEXbGTtX3sOoptZXfKKSXUhT5bLSXvGEPvDmSUOXPQdbArTbQwUGB/Q8hDuPlwFwy9eo10
ND2NkIrGsXa3SQSRm+ZJHpcDCt0HpGppIDgVlXygNkwwsf9hz3Yn3nkahfhWcU37Fgibbebdof4z
hh7SXo6E5aHq++6Fwv+yERuEHV1mT3PXH19/I+pULO40GqowzRUoegkzlTqOctXGOT5e3kaHowpS
TTFUg2/JT9EImazpuG4f4bW4mRl392bnFsD8CUjHohY4vgpOQPjxly1IcWXOVfW0sQ2iUMpQnHhH
f2owm4y529fAepLtz7pMQ4TVKDxCQaBhU8b6/r5sdKyXpbkB+kzknfJThOHPTMwhKWduwDMVouGl
WUY67xrcRymzFc4O5/6xprSu/IQUZxGS7OZRIuIkvoTFtMgmrKmRJlnGPifeBVHVdlbqZpubQRNj
62Acd4rwV5QTfrxihqXUjyl9/VItJAVR3I3wC7pzqfFDEgFMLeB+EV6OH5jjv8OnJsMvaQyF/a9o
z+6GTLaIQOFscnqUrmX0RE3jBHcDrABG6BOMmPEF3zWFVv9JedTXC4JJBWfIHKkvM/ExQnmLdILc
VzlNM0EWQ32Jzkq/JkYuweMDopo8W8NBWvLSXFW/a3yhKTYzQNFbktg+GhaM/5I5fKX212kUHqyN
4scw9l36YptBS8RGBYDn6PeCq5vJ7OFnWr8nuF//FdIaz8LeOuWoFjfCk5v6XNeYWSQ4GjJ50G7L
BpJ1I2IS9jAGOZIUcFy/RcWvraeAhXVU2zxssVx+NN4jT9j4HvTJFdtli5kyheAn3nhAAQK+j32j
RUqyVT5HAblfNgKau+rWYr5ml+KQMjWVHlGkCTQVaFP15B9Zn9THXsyZ4vrUlEMnC4mHGexNHbVp
xUgPjOaPB/wDdSUUQfWD+WfDez2ufo1ZqlAkxEKPDF8TVpjyW0LYgogksfho0M2eK8YJEwhAJdy5
Qnarf3yl6aTiVq7bRUWqGOkCuc3SPxIShpwB2M8Ye6DRynyMDgIQyTFHdVKfWJc9yXJPpadDFXHn
rS4Yzs7Qs5Ivi8cIBX9z2pd7TWIzUc+rOJhTzH8RmWybaQCscyX6AxSsgdSPeOWzagVPPzJCbssi
lvsm9KqsqG0yU+5/uvqvc3wWRXBJxlNr8wRTf0STwuCbjOwJcVireKr6hU5N/s8wstamgJtEgwLk
naxnEBYZUlUAN9tLMF+9xo5YFe6UqItSoalzBK8Vmea4+PSCupZYKO71m/UVidwpO5qkQgaeC0IT
eIFDsCgcFMcJPEnjSwc78qLujIh0WckJXIFD+epFO9JtPQTT/bKAbZiFYv3K1b2vAso0dhk9qa3o
YxDWmSLQSczMxcPwu0Sox1w2wfWbMscE4nV8AlrwX5rgCFbQlXYd+XLihMuXWNDZstTUFQDoNOnK
A/s9WV2VSmBTFJJFKtzO4t60wMG3LDhWzK9uUmil6hGnrwmRX3Fo5VyIjUyrNG0te4LsVjmRSjVv
jRYi2mdT9g6Bw5eTWUL17YR3Rhk5mBjb0AV/Jvr777KZFrlayqVe/wo0s24jSd5uU7B3Ht4/Qg6q
KWXmN5BX/vwxe5VVMJGPsCwR9XyWcpKfHtF7eMEArGwVEu2WKOjO/gBPvmuKW9dsSFCOjw/MMg2k
Zwpqct6U0Xmn4DtxK7NDIByjmI7RafdjWlgE8+BeseHeDdK4KuD+OnF5Mp0SxyVdRlj5DGOXU7Eb
GMhugNkwoakoEwM9N9/befncbq8GUyTjBI0d3K0xKqhOngL2UjF8NA1S7z9kpIVdMqqmAxXXQ7iY
VW3nLkfBBt4rNaqGir+aMQNg9CgtqP1+2Ag7PuhLENAoYSLA4/AkmblNFApEed8S3LweMyTNeEFN
aEYjWh8Cu3z+eIKkO6EWCrxMFa8OYkq71CzHDGxIaTF7fWcJtI7xl4ZSWjOJjZ+XnmVpps3H4cdB
6dpKv+OtA9ic581N+xkuQY087ATiPL0HfurbbzGjDDGO2pCF3vglmUF/w5Lkqsn76fUtO/Fvi8S2
KHAV88K6AhkXIR4a9Jnw+qEhoJWLavAsNkIskESqcazXk210qxLD6H0TQsVdWQJOzh0y4uAEj4F1
DT+CdgeU5lrYaFKt7Z/i9ROYT8Siu5qPrOlbGDtNCC6ACL9Nt2/ddywKiAjo8Mb6V/NVsN7BJagt
fmz1+E1jLtr2mkf1F/tkCtGg8z5BAF+lY8qiEcL6chxisKDAhJLt2Xt3VCdiQ9vr5ES/T0+FbbUG
TzCrUdEdvsiK/kYbhQ+kFghCJI6P6MQWGY+yKjC4l7i1xMfNDBwgmA2kDBctjakZxzICDcJ7ZuKT
1qajeN27MmjyTLWzSNNMuw5nErbeac8TFWwBKkATgt1wkuo2RkeQLoUx+Isohz6ZIMgtZ8XVEgsv
ufz8KK0+dlWxk5ZB3GJVcTOwhIddpjqwV5r09OqtX7Jm0BbURxXwk1B6zAHt1j0viFQKSgtFYHlA
BwgCCxjDgJf7G1WGlN09y+o8Wcr7RVlO6eSCkFX1oGcaxkEPlWUekFAZsu7Kck1+E+C59bff5c00
ktSJUews8/wR6xGaERyiOoPtVyoXbfVD2Wr+oePBjL75jODXgaRXIhT27nZ0YYttkDNmcrc+woHX
O4E7kTGmaZQtiHUbJ/i3ifGkQKvLl1eayF5KiapXTJnq6ykeLoVheObiK4yhYhr0mXRUsp5gzbXl
2AEe8c88gvERGSscz2xd/whyje6W7Vh5RL/dGoU30cG/b0hiWvz6GZQZ1mANkVetclW9L4dsjoHI
DQW2xouwRFPn8CUqkZh8D2Mcge9uxfktgBNZeUWFVqgGw6rkrdN/HZ5I9abJIhXU457iJJK9CZts
QZ2hJlMCUB0KS8/Qd/+vfI/0ClTfFSPyCIn6fHN703Kvm2OGRVEVgrwNhQTACouC1S5tz81Z9QW4
UbelDz33OTLh7O0N4H8owaQRQotjIiPqX9kGxX4i3e27FBhBNq/AJ10TnMRausbBDIeDp/znFBLw
BKKVk7RO6nVTGXRVmiz8e5baKeKESgEhVlSkuzW1goFxv48DN/oBDGcv380RsBgtMq+fpoN9l6fi
XicOHUbx+hAhQ5TWv0VS2/u0pyIQP9n/zap5r6rJMBjlESAFnuR6YXUEiBnpnrdwRSuyjZ18hyk/
QRhZoTIiPlDLf9ZThJAh53tmpTL8ZE/fLpPEhCQr9tpLKGSUTJz4UZKcofvIarBl1JjUOJyVlati
Z3xUfH3mgzlpsLZXZvyQEF2i0I5qemzaYIhIgvE1j8uSjnsVQwilba3RybUmDVZ6NVkGKxHVFCkF
vUvFX1wp+8Q4cWJJa3vmRbpdIxNIrir1aPnTGRPGK+tw++rFHTdJcYWVM0yU7pnlb7fijM5kCbDe
JxLsxYICOdoX/4OLRXN5B1HzHyn7MGRAq9pKJLJi5B1fd/M4X9dOHNAqaE7WRYt+APPIRlpA9zyU
BGFjGOaERj7D8t6HebxyDsShVTrtZo9cdxzTeHtT1fD0Yfl1BEz4QOlAsc4cI3wCBvdWyyEPrIhV
srghRqAHWrtximkSPQZ6Lb+JKPbi/cFt2Dqq5t44BSx/mt3+mNc2+5tFd+JIIJJ0IGBKHJCAeZEo
BJZyMzd//ZGW6PrI5IFM663L0O2FzkgGRYH47Gy1//0vm5iSBHRwh8ARYdLmAUp329QCf7si717H
pRC1R5LUddLRlmXTqwJrpPD5t2jieGT6VI9Qi156Vy4fQNP5lYajSov+YPn1VR1+gx/sS5SmHzlx
bIPq32ZFawsuuNvolJTy3CuK0wjfZVwUtrr830FT8t2XCB/FcrboAms3dh4CidPjCG2xEu5SeeU2
Mld19b5XKOD6UuDW8IZZdigl6mURvNjNXa6rrIlnVf1l8i4iszgI2nIDEoXdZnRJVexVx8a55Guf
YWWFGI5gV1xIc/IkuJzbd5bZI05iSEU5Vl10TED4pS/LuHAaSlIRq5eUlQUUHxJt1jea5XlDtR2E
vGPOby3qkNt6zEVHrih8325YCN8kZZvaitFm8cXlYOVWlzx9J/LesslALplvFNFReJPabcpRwiYx
9nBppn2drOuJ7f6jyTyCCInAWQ0HqzWzAFf8wslVpUYGxAgIwKOy/TKD96OXqlOj/TYJKKxqczzF
YNfn7xmwLSLlt5ewdvfIvSl+5hOEFhG4sucU9FFa0RemqRbn5e54d1+EYI0p8U31GzN8KlzZZI6F
CAdtc4m8Chs4BbfyFE59Hm+/u5UE8Gh+ypzpxMa4y/49JNDz18XEYcGBCKuXUA0mqguMRaQ47V+N
OhixQZyb7jM0c1u0oheBr++w7ZqOpWgRZbU/Y1YoI9NfaP+36+Wn7MrwP6zcnrsOBZvesJkvg2h2
gTpqbzp5NPr9wpd92JKA1LRJ4xOCEbJ1TOJpFB1JsYMkwga3rgU7pSFReZZUuWTlQddxteevcCzN
nINgAixmWuZ2qbAIBZquXndEON4rzlCJ38qkb+A7K1D0nyKxlPWTMYBHlUzxZNdBMWV0RNmNowBB
/j79GE9p3uUYXpVw6v8tsbGZuHg3tg0kpjyH0XnDKohHZ0I2LZYaZQZfCBf/mztY3qejIhCY34+F
KHD3HIILW9OoaNIPUXTnqWXPJ74tO+FjqVyr74ih94c1Jr/dPm9oFwT5IKN9T2BvlzW86R18sOkH
Jwchn6vETRxSby0ajgMeCTomYi/OV/IUaNpmYZZN0VhIKchnE9IXT01jBHLZGXlcLbzEmsb6bgCV
uAXGy8fzpqoXtkCppIKf3+vZtlgymgYmJ1GLsrK1FrgBl4gVF/geHDjVhTnpf9PWbwgnNhlaVzU2
AdDgd0Olm9+w0zJaHDoKQHRK9vozuuAn93QrRvRmJKuLt2L9m683Rgl84Nu2kh1tLk+u8f7Um96Z
KM2seDGDTWUzQsP5ce9apIVj8SJEI7d/wsh815r5fGBWYqryBnZ/VlA9Cfawgc43zgcIJVYvYotZ
/TBDzRbyYgDUBPd5D6ws4igcyuT+0tSaIq6cR4vZ6x6faMlPn4b/4361gUGgvkBkK74WgEUmavDD
7gk9acVWN0yX/tUOG7jCtMV4FUg7gPZt5CT5oJSUYKNl0LvSqaVjckbzcfjH++iTf0pGKpDsFxKI
knhvwa5Cl3AQdYThQ0RDFdGj59znrgTXeHl1+qQJXQNH5jartOoeuLsx6kOGO/LgGcVhlN6zfMpi
JIni+FahV+qDIOWqfq3zFqo+jwnsOPBalU7owe2qnQ1auSlHbLYIqCtedujknBob6FrvmNITk2aa
AyNTA+xBvNupKNbf3ZIsdJXBb/Yz/m0KrjSJFu99aNVnFiFb3Y2j7wjry1dwGPEFY4IoJiWrR1ys
G9DLI4g2pCSVZoDEqrPABvtBtsXGFjkH2+wnRoiA9n+yaJ4STDkKIWuI14OrVRcScPAtBuVkTBsb
sQxdFJChvgD62fMdoVaBH0o6Hq1zKqIUPXKf+05cpIaDXvDnEfqb9Ce6PTLJJCHs6+kaVAsFG+w3
x3aADMMuKQoX2J/1MSN7phqrO3DICyq7PRjoOqV8McICrY71I6ydfeFRof2Y2jIEeYbJJbmGeDsY
RVxMZ9wtHlSZW+XW+xPnFxPje6ITAUqXnmFdD2pkeOHvqoMoWqspvK+OPgJaw5kdPx+CEd19fmkl
ECL1s/hHzvwuR8w5MLZ2ZitKwUFaowMginbB+7LtwFgYvAhSks8xcYZpktwpE2vbVSZwTU1ttuog
r+FITk4ONZMWff0g13+icMazoT8g6tJca7h9xyt26gyGGC5oItwM8cXr/o+543XeSdHqXnYf/xnz
MfCt+HrS9iG1/PWqoirCoX6OmM5hUIbT9erOwRxbqx372z6Ij1vwWY4TCelQl4ToEVc00wMR5gN+
1OT5D8u62Z+vprVylN7gYqjyy3SGvSj+M5NcA9JV/RCNQBXyi/1NzcvbHYQ2tdCbb7wHvC6oIKxI
Dw2UxYoboBLxx1N0LVY0AZNrhmVsaEGONrtqCJU6ymCshwH68SzDHZ7fKr3ulOTQ+l0k8IwpQNHd
M9OfBMXLnl4/LZ3SqnNeUuEzDXIgUN3XmuYBs+egKEeUFdTRxglPrH3BYKlmnCQB0MfnzQ+9N6K6
71rQfuK571j/Pvo0LvCOOS4MtyUTVLOUXsjfoKWK6HwVrcZvWIyYPARbIuj+dui7882sLnVBriYr
mEW5ZHREBYjsgX/nCvJerx51aeGum+rXvzYVG3ExMWR2eXM8haxfxW4ogn8uEH6Y2zMvCHBKtbza
2SqjaUFLK938cyNaVuLg2BLfvlBP8dWRp91CfigPxcmqQ+BbzIR+bgyHIe0WrezfObwLfggBys+W
XnsS7DnD9zoD5AuSQP+TupTn+QLpx0o9I/qMOMlEl3uMNWbSRFgQ3RVHuPWWco53oIzO0KOlo0oj
5BmHjZwluxRA3YnCmaUHa7ZFvZCh6F66BFPtjiRGAs53vA5vNMwm+EGxusxmhoIK7Wa5w4yTe8Nz
7DK8AYHg94Lh1FcV7rvulOVQ/FpyD2NwkuzPmRWXADGQklHm02XGgTd3Pijpb8STt7ywvE/rlMwx
tqVKxQ244KzOy/4Tq8/pkAGoQUNTw4SP1zulyS31tTK/xcBNlZAFgOfK3iBdUCcsi9n8uUajdyQK
fmETIO/ApfUVAmYrqJ2vy7DVii/VRC2DrQXsEbgLdF0WOSJgLzzNqby98Oe0VM/Q3LMiPdNGI7eP
Mh6aCRuOmRlsRInYx960npsmpBUCL3bLRtiaDHj8eB2znw3mLZSpzpmA5ouAxz0vrC/UzB082oYW
+4o0w+UNi5hBv+BKGnnk7UXXUNL0qbr7/4/YmfCtHtofvodVTk9yrMRNU0hMZlJdziPWb0tWr07u
tgfNYGnhqDjCVGq3dM6Xiw3XpjCzdaH2aIUd7CiF0DN0JVMDjRSvtknOttBptsvsmmfHxpQsVGJx
9OtAaONjVVU0hn2c+x7aDFxJ6PVoJltrXqysbd+RFJguQ9x1qfVtCkJbRqYZhTFoBlnADh9jwbqR
6XRJb/226YVNd5fMJuH1iwJJNV9pjlD6fLQEOBZXIDTYIEGBPHM/0RP/4Ltlr8VM/3BPFSrFsOss
it+b27952BTCJWRElJuyEJRl9sy37gNGvrt3+LmdejTMrTN8R72KEotM+sJjkWWLxCm4+8OaM/Ol
abnlT8swZb6YKBwtO7hLAHxgBocbCXn3Htyj7MxrS8APD1/wcI8hhYmre4UFEGtGKTMCdHYRdVQA
Wa7tKZ2MR8Ll4a4MUaz6JTWBaLDtfE47ozfAHtZNbR25T2E/lctpTUcQ5yv6TM9JcavFn5fWCN2v
qgOdGCEctGJblS9lBXfdL8lIfVJ/Yx2f3xXFCUirSr9bkio+G1MlP2iHVwzltLN4ZsMwNc7Hvxot
pO3WRcPm4qOMU44iwwSkJAdarVeF+/29RscvS5ELJfvGU34V23jmhK8FJwKHQbGtVnPd9DwbrQbH
fLaUgE/XU+rg5iVjdzKtBcQoO9/Z4A3fERZuHOXtYkiV4bQEYaq83ZJnX6qyY9gt3+FCiTSSWhT6
Qkj70iLbXrZcWNeGVTTTXamrlR/xzoK2WWV42EcU2T6T5v/DC7JLLu7dRSJ2Vc+cK/DCNO1LzwtN
sx5Re987Mhoqh4owXb+nUxPJKD+EeZXCkgunw9yzOsgZbpIVT78jPVqeHvvdHVyaeLWAfL555+dI
jfa9dih5mByNSxATFBHDjTc8B4ZmgTgIfia7IqjGu1gsRJEmYfp7YAZR+dDsnCroP2Tbj8/4Rr9u
mbEjDvy/c8dayUT5CNlOOXRQWUEoZ+1yehtfYFcVe1/RGvDYzMmJpZfQnVIriHjJC5Qo0tAIEN5t
ZcY9hkU23D3ClWQX4bfceyWGFjpOVuzF6PgT0PpwDy8ymlymP8/EvIl2Pitm3UDOWwCPMB8SHK7A
p7Ogci17fVVsPtQO9GMbzWl6JyCSRPyskABiT7FOXlBO89qxX/f/N+nRzzswLUgpanyQ5O9cEN5o
xDbmsQ6PAgkqSLr1vGymyb8IlxoY+Ti6RaHdHl3BUBy9NU45N2YfBi7Lp4P5l7/aB1S4oTrbF2OD
6qkYvQmuOrtU4HisCV1X/yqeFWaAzG/jWIT8BDl93ogJe+oRtXQNSUBPUiJIiioZnQr322ODwTPu
jIwJYDM5/0MaSPdyJ54xVHrlRQspA1p05fkFz1pYtT0JEtdZ8ogYRAXg3SGh0LXaCHq/Vzhx3k8M
ypYhiVLHguH/AVqk4YSgZhXIdUidrcun3VP281IfwitYsICB5Kd/SA21D/Fu8Wm+kGhZIc4uMcSu
0N9ffGylI80P8KtoGQJXSiZq3A5BWMZG36kia+pWYdPzUOwyWBbDXKM2q4rJtDyYRBaa881sNKeU
137ve7f3q8zk3Q7Cq3m4Pg74hmPya2s6KuhQlmDqNLp3kMAc1JWaGSf8lFVJLeqsp3rVHLaN454f
L7wWBAFOM/HJjWWR/NXwqasQIXvZkmW6L+G38TMI6ed8YX7KeMRvT2lk9fAV9WVwobVof/DSBY2i
w0nAEQ/q8RIKBgjvzfqtSfQD38sFDo8JqnxTSLsv/1/Glf6sWosYE/dZEnhbnu/wm++PTBM5MnUG
5151MG1g1qWM79n1Wrt4+dYXybslSmbTkK+AkQfgUVEv38Eni/2UYFNRdfgQIz9ScFQDIrTAeI1I
ympNjeJogb3pIdsV3xzY9MftxgfR/4V0OMQP/RVLLUv95v7PHIKg18qtptex883sKirRMii1gzED
w+YRBQf1fSFxS/tn1tywK/qoTiTDE87NS8TTk5Sq0DTzo8LbVb6pycaSc+kKA3WwkW45kvAqbNf6
l4Se+3GYeOzRI8+3JvFAwctQjhxTnGS6QVtgdypbiOicKYWAJbhH8v5t0+eR1H4biFaTp2EENBAj
TZMsAU3N6FEdIOjdFf66HAhw+P9Bl7943V98M8x6CA10EuoNQPTZIEicyLdt4ZVtntlZw8zn5b21
EzGE19dGDcyTXylOlVCY80Za2l7Me6c0Y4Qku8272cEbehIfFi8FmB9Z2oaXVtsXr7OYhTiutEEQ
RDL6fwdOw+XsNk0ayV7NWTyhTQ9n+HQYdLBZQsZ5hMQCeIpGSGA/AjnTIBRnHWxQgKGdajuherWd
EfgYsdCyiwl398duAjLZ0z7IJ9N4jmmyZOqkPBpVA6im5FA27yp40KsKv/olcXqTh12lua2LiH6Z
sRo5iWMFfh+RzG/rKBMrVQ5RJBEuRH6RWMZwUIr+/zSbO09ya12ENYTQG2vfFQyp3V/Eo4KgQkhn
Dtxt9vp27rzfRKPaieMPfjrrivUiy0Zcp+RAOGgJtQEfXYs/H1jy/76bD97GYFgq+SBGbsqT2hHI
AJrkL+51W9+2ST0Xq4kEvpN1VzNhg/WjpeHyUgkluaeSp1LDXad2umUVzasH1eiXwIsHkCDNDWsg
9vdOhNms1Xa2w7JcKvWHWSezUFKxdOp9nu5NLkjy2ZDgTN0O/63M1SE3N6mJWlO2pW/cDwOw5qMf
5uigqZN0mdx3PA72uUC0zM0YhII1m8wQGgwUYeuOnwvmzs9RqBIYt/b34f5yuc8tGvyzGMw+qopO
7exwVJDwJ8jV38sasuOlRR7Xw1oDT0PZR27bXAhHvg3YgsswrDjxYWi+0YcMQqFUEt+ZD82/BUgC
sEka72qyqOtLCJxnoa+3SoIE80bx6rG2ja6wleaOJAbZ5f2vrue50k6xWg4oVPJa/b3G3OlhHKYF
WwceyeBMIW7UcpD7UrUo3pJDQCAB5FJySiLSY547aHqCgZe4g1ePup2l4HN7ts48ugCXh268PWyG
gsp4C2DsBOvR/Elsk62VfU7B7K5jupndhTrYjTz/Dn7itkxIbUW4a9NircCNq+3fkijrNJCcv66X
KuakL/C62M8Lp1dRFu/uhh0sHRm1wdefIPJhvSAtIwidVHA8lMiMywxKMq8yMSHN5j/Fp0zPRAb/
dTDSOvrPhPNopfGeph84gjQdOG8iodpi7Rl4xGQK5b5WFI9Zg/Jube75wZ0E1tb2WVL48j+znaNt
w9jHQYYjmMR1FlEzTWmcc47avfE5bOl5ieLLDjbT6DWfbnKZHVoh3yEXalz7fwnICIydvhhWP9xL
M6G3s7kmrzssT0vb7F84hBr2VwqrV1IY6UIYqftFBF7dsbk7zmKfhiYpRXhSzKO036Hw6vTiLdXh
6T8ucHXdLxnKW+X8u+xDmdSeuQ1pNJZVsiAR0+ni/OOoUMhmKl/IC+t+fEi7Va+OCZTaz1musUeA
uJ28nl+5UyJunK5uJoL3qxsDn0MIJMfHj6RpE5pJbXDwZ1rSVndj6+mtsAxW+jQ2h1YhNILQFwL/
ttzdfLCdYSGMFsNrS3nQFyzhjBz7Z8sdFLvo9udwPIa7n09j8cMVgnGK3HFTmcKqDeA9QTXWZG7u
qv60MLAiJJJOzUHnWAiMtpgtS3VjcS/bx+xYtJ8ClMFoJQurnPIxQzxB0wiGoivHtDTGPxrtkPEq
Fe0pGm7DJ7tAzIh9iz6sv/O675qpUnJ1yMeLt2Ha1vIgIFJ9eCG3MrmPx+1ZxPnhM8GNRL2kq+uq
ySjuBHpXnSbmB2rGhmXzSLrEj1jSfbh44/+zh2uVna1TqorTaabO3+vv4ciXmtUgbHdcWQ+hy4cx
z56rsT0ewAkSYAe3MIMT62HHPNIIfkEQBJ/NEuXaT2b5rUveBsumOrZ9UalXEjNJcGFDkSKZgeOO
NbcxOsb64TwgI+tuQ056FUKfHoPjSIku85tdt29N3bGQHxc9XtPzm9vTm59Sk8bnIM9lcRstsR0A
eWwc8KZuWLws9wkxkZPvexH6+R+iLG5FTKHJ8BXa0UrGyE4BVOZtRLS6gwUXgNN3gL/E+tUyzKIu
dbeXoYNwKh6LMR9SaUDwU4T3kG7QwKc37FmFeGvHVZACJiycjpjh8pG3IIrQLwWKaBlhaP2DsH4i
lZqCsX7c9PYlCli4zchXCHlvJKatSKD8g9i4kICAqZPEbs3LlfBJQgP/jzpuj2TOCHzpLmKI7OpO
R8siu/6cDA5WVUtFul8jmXreOyoySyLuG1iZtO+VCL8kTMOxM3SvKSSEyndrcOIDG9iRQNITkyEk
PEmJFl8cA1fnqp20UASrQBOca7FK79t9mG5d8Q3XiHth4IGYmX7ikMCJj6FUCRtgl2gH9GW/ZM00
NJH6C53CipQ8x45wgiagKNK2ZUCLCKdtSt77ujITyHaNZKIrlDVJ6QO1q7xTnMLhVNtmMEoEd9RV
DcLE7eel38OQhTYclC2E7IZ8IfIosysA3KyGy6UBqADOoiR8JXSbrgrWos7VEP2IehsnnSlNakYp
wSOX2IyjNEPHcoldoYa5R9n2prwR6FX2gLG7boc9JtMeuxFMZHUoqWEf2Gs750rme+ZfGPq/XlYZ
r0vRmlfHD4BQq6aDpR8ftbVLn93e0n9qUDLstKoPBHpuO4UqcdeRaRN8X8U/ERPUP5T+PKCjG0mu
ZTwROrGAkO3t8vVrpOGIFdDl1wGZuV5RjIE4D6x4TRdTdd+5gCglJtGROs2IQVQDw+sKYoqm8Ta8
G/dY1qvE/bOfTAFQBd1wcV3mCjb2gB2nlX2qqJT1ZqX6oOhILclGCLnIPwAaAU37mm6yS3Unolos
nMVKZTiFuchMNixQuLWl8G0e5fi54nZijtwuPKputplPZk48dVm9QxAltL+WQuNH2fQzQX1r7BDV
Y8sg2d+9fBviUbfTr9NTs6dfw6Ja8sr5eBJN00ea4KWTLPUUA8onOAA+T/RHHHcogBFtwhlfDgKL
/FzbX3uu3KN10x1AAUxApJhIjSUCuLL2ZB58MI/FCIsI5Zxao3tRYb4oLokcJ5kC8LiVPKJ/hrxi
GpFUJWIeFbN9wKZvQtI/OECBKM3eUvHVLjZH2UzDh5jFlSDCEN8Y2nqOQ8s+NenMp5dTKL0r2LLU
SYez+Rd1wg75f/fVzNOMIAACZdOvOJKr+IjUn3qqV2sTEJAhlPPlKg3jITLTOicHur7M9yWrQn2C
vOgo0z4etG0OF2Uq1UnzVg30Kx70k6Ly0K21F4mH31i2YxTTR1vLoqAh1bMhidiE/d8atJHpx2LB
F/acyk4eetUYQxDoKYBppVeFIUdydYZYgLICgNWJBOu1nmdjoFoJvptew5YIrhpUpoymkJEqZf89
vX6BjerN/TeI+/Ongyg2gqtqCrELT/5Che27RG9nPpjiAw1ynUur5KfXWzbOXa41F0W1ZMN2DjaQ
fxdkVddMgdFUp9uNu6121aqu+Xo2ChxjtggaKAp9jR0ScOIggchH6dJMjzSst9itVySr6Fd592v/
VOxImvDUTNz+z9I0zauF3gUsilhuodbpFx0XfH+bFVYYO2qJc3Kq2JzRf4yFKsRvWSAbvIk0p7DR
y1fHPCb06WQh6g1wB3ffHRWoVoK3+EBkEGzHw0jLMbB3X+soJR5JXHPd1OZBfieEb1Vm4ZFLwEuu
E9T1ivggmq15wGqwcCpWBIMfUtRUztjp1/h3nHDw38vyoQpSTyHsa3URc2GPjyj7MbalqdXU1gvd
ZiBbLTDKOfvRmplTe8MiNNf//Te9gJQWXRlXTrupItUr7BDfJ5Ll1dZ7Wb+pR81vhFZiTmy69l2H
+6p4fP7L34B3VOxbqlPfW/GLKjQ//mRjuFY/14W/tdn5OnRyeEUu7I4sV4zxKAfNIy0ofeOjPO6Y
HUSpBM5UZ2CQ27F5sQCslIHYj/4FGzhRJOkpapCEAZEhHiOdLNSMqop1lMXb9slbcY5vRmX3g4UM
wgka0UGZTwzmJ8MQqxFgsJA9ErdejrTCs/AVmXxZj0zhuDIBb+LQvoyhvf8eSi5+LbncvYn1l78u
vcgEkZDctItGnf9x3gNmFJu2yBWtL9pCB0SNuw5yRBdiTonwPxECl0L/c3NGkuMLJ45//XJyunLD
EGa5hmyj28+XfRqt8ZjlIvvKtHEA5wNwDoJzdvHHRRJmFl/t/lu19Lzmq07dXbPGzHm1ZClSnniJ
Ln57rsJbnQZS637TN83W6wGRwulng7q/hZ1rNEyxYuV0CMtf3398yA4jq6S3VJqOz9GLJK7igI/b
8vKj8c7CJhE4ghXpW/HzRNfCeSD/P2L5c8zk1u0sqZxedSjXPZV2LiwnZfNAhHvjwkUX6YSycQGv
iMj4eoIAafr01aw7b/Rz4mnV6SdAgnulCNfMQdp1pMuejeJLy0uKWeemXGSH67WJ5gNo+aHe4fRY
omw1H4gmFPTT31vG7Xqy585DuoMC5l3og9Yiidd3iSr7iNygVHRNqNbBzAdLqxe/PdiPbt0qjSK1
LCBJwUJoB1EZVwTMfU7wDP/ZyMEEIMbQUDRc9WqfIYelVZSr83zwqBun22akO/PlUD9RPlQLNr8e
TffBgHmKTIJb8qkj/2hhk+H8eFDB/qbicGanyvcJbaHJDLeU9FFcWkaGBycAj07r7sUe5Q4x8NhI
tr4o2eL3VKGWdtghFeKQUPJkeChvy5DoHFvQw+pm/i8YobLRoFEZxDKiEt6+yl8GOR1AfNabhW9L
E8CCTwrr7Ucptvn85KQO7Sbg1qYDvEevB3Wt4mwkY4OfeRfDgz6yS4SuzdQIX0sNlS1XHMHTs60s
uppfANCo9kVgkU8SQInUyZx/k9FrAG5XlO0wJm/UBtDNNOQtp942v7RwDRdT6DqfQ/xcqcG3Uq1E
MA6KyBJETvC0tIf8EFLK4QQIqxFOyWVHelZlrRMZ348WtMUE9bIrAtw2CytB5cXCxkc7oBnBhqgh
sn5FnfO4ct/Ko11pA0N40OUc++LDkwdXoL5JtVCteLFzku+mg978+W3/730kihkBKOUZ1eO/I565
WSe28azxeKt22rbYiic8kcX6lJYalRdG7vfjAoxI/XHTimEeD3y/T2dRp/AmiPaI92KuZMn1uZ56
8PjzV4Ee9YXU14vJooqTUWVpYf8yfwdb257a+NIfo1C8YH2KB8tSaLt/vmDIwthaG7asmeP4PSYz
+qM5sYLoNPNUzixlq7wpRuLv+tzQNwn6WxU2kxa3ql3t+3dgVBVbkX2jDQZLaC8w1YU765SW7aJ+
RPHh8n8aBVbmkk9T2x5I1Wo/xyPBexCWFXzhdQhZhT9MaG/j3GcfAdDW7xWIeyw8nRp9N89zkfQY
UQxjzrv93g2ldBvn7oxHU0plVYAkC9Z1elKRDrUgpNzoQ8WfxDDWT4GKhwth/S2SQbh2Lozxwryo
9UrD/79WQy64u4+rzoynnuOJP9CECJtjeOJHA9QHIn9H4JgfnlzrJq67vBJi3dL1J2Gkym54XRe+
kf2tIyChjTTzb4V9EJ5yDkt7uuKJbO6AqlI+AwhipXkZ06JRcvaYbVUVutPUtnNgb0rrloFDL8k0
RGMxWfRIp/6dvTgMP3vvMSdYG7HjpvScGDAgDw3y2vvA/PbW4qBUNYV0sVZt5tEWfCJLyB+uqu08
MerkaX1ylDghgOOzEMMJHOliqi8LB7tzwU0Ph3wixCaO5pSGNYrSTXWHh2vqQJu9CkbeMEZiIlb0
3qIz/pWLUrWaSxefbrDvCI0Ikonw/I2H6C5PeOyN6w/Ko8brcCp4uS/uQPBnvv9CYO3AESVDOgcL
mXfg6IIJ+yVfIon9jafa8+2KxYXRWDbOhDkhXmpvvee15IR+VoApOfhcrLd4STv+zpUGG1HbnZyR
HXQmDF7/CPEn86XXQ7beUcDFCD+DoobuXqCNThWJmMqq4vZppUZW31D4P/xCyapHIRk6g3z0ZjFu
c97gbW5eSzvliqSgessE83Iqhnt8VoA4wwWwBx2uRbvy/U5w2DefOze/5LfzcZLdczeQ5JdoU0vH
hcR3ZAq/S2bW6BlF6edyyFR/mE+5WZHBD4tgyrOy0vPtmNHz5ak/etVCcQJsBnmkYd8ZssvqkuGl
J2XNhY7OKQqxapLmLfrk4EqBqyllcTjA0CHYJwFF+9NKn73ekDUkNt0SO9pzYpBcPF2uWWyNKfj5
/+qYVvvkGk6hI27xT2yE93u7YxZWvjQzw+nhkPutefl8nvHHr3NfmoHwjnsxsvS71G8hGUHPny0K
GP2y0Fa8EFV6BIvVklJZKFxn0ieCXyPiI3uyR6Dzbo5Y/htbGh/V8YyvmlIWFuXyCEElT2wL84eC
WEoiBF+68zxtWhPUVUBSYzKB573qJmYkH9jDgng6U1KqF3RRgFjWBNEqGM3jIKe2v6V0is7IHPLk
pBAqAHbyUwVq6MPur2soOZrheLlSsvjwVImxvhiS43GoGb9IjthenmXVrCdd3fXeuQ0tx0zZ3coQ
m5o99YRVEcTBXcDfe8UUvmCQMGALkHALPg9u/xq4xAm9JIN9/qqgfHRxzJmL5M+/gIIgSmZ1NEIv
9iytEWncahCrD8C/xnvl4SCCz/VZ4F0GppF6YTudTRvdhzcPbkrKSkaju05o/Sk5IymeyjwNGxwk
2Cu8zDFZEhw8LJcNtd7zC2EWU/WoDRiUG0Tbc2O8q1txvMIc6jh0akrDSVJ3jjgzSiK36L+9U5/4
nYYrRk2eJyRamW/10omODpaEHMih/UmkIUbf1rc+ll+s9bThGcrWcEMXuol89Z6hxPKUWTLJix05
ZFFgGMsSUb8/EToq7E+A5nFPxVQrmYRhgLNmIDgGNH1qkiD3DbGY1tcffUYMttRJq1PnmTx1HdRH
OOOuy5O2fFc1cIemN5/kvutdJvWL3dcL5kfftEKvPuqtTTXvnmpiUbwuSoobybyS+xKuAp/zc9x9
PGaAiOzWPecf+wOfnBrHbs3H7zwQHexgN9D1AUdCr1tPxhMv7eYsz6ElXJYQjnmm6zZ+Vg8TRS0i
evvzEFO7iWpDF4pguxW5BFDE+1FbM9vofNEME7DQuWNYRSpAM7vl8uBFPKXU82MlfrjoDi7Jw4mm
v9z7KS4ITQkdELmHqQCjzGecNdcOKhoEGQwKJbDCngwpVCf5l4vZ0SUtyoKQg+wfUu/eLILeABTz
Jz5YMl8hgl7hRcQc7y8YOgfCoxJVVfS6U2OqbY4fzkFLTf+kE1WFrw3Br7u4r2eFMdr43rPMaPFX
/AgF3mqFrjQyP+29xNCV7wEnmFuFRxTj64KY6ji8xrUHHCW7AklKiPB1N3aCYqpTdKGinD1OxB8K
yaPh1LUhbK/2JgxE5FdiBHCOL66p2Z+pQaqqLUalpwV9hQBp0P1gMuUcCPIumTij0dT5vjDqTOBQ
cbBrf9MiE6Xuh04x2V4FQ2XLv7598V+7F++9Cq3ZriBaIOcmKeC6ZfzAYEkSVddHkI0pcsIVhZXB
EwEnZc48VvAMEHdcwTe7+P1cK1zzSFCtgJAMrgi9DPxcY4VhC7nqUbxPTk01tKt4yiUAlI3TxXuv
7xRfggNCXYtsgiTZrGDFuxCW7XlvM3BHBaxpBdtuupGXxZ/JZUQJ4a8B+ZeKMPKPMPNXP9m2jkG4
ViaTwMS7h/XtGJ44RNSqLJdmsqE608i5Gqha4LgiNe/DKdV8gjuRbxafXHJmWgHZKXj6DwOAz+Ly
gHcPl4dr+1/4d8F4cfs4t2hJItc0jGdGC+rfPzVG/6saYsNyOegdVVurDR8GTqWTQ062gA71mHlG
Su0y5aUwGjfqAG8BmDczXcOKevbLYs7BK2DlfgyrUHFfmgy+8v8gOfIe77lwSnZu3PFs2OHXRJQF
264v2MMdrjylTJPxKFH+pRBYCH6xLjcXFdoL1/oDsHZd150CkQ6/hbv8nL3tQolnMv9xQjZrBQLq
4/SunzwZ6PanEMqgbKwtxBSNmBYN0jWlZK9rGqRRi7tBcFmZFfSHRZ+ZhIu2I47pWULbKPtNptL9
vIEvcMB2CxCuJILHqvpjgjtGo9GWF7YhjmS4Vac6jnjuH6RDBLFveI8vobKDw9TeOMCPJRyJnGwr
846YF/70ySR1UB9YmhBOIQX9K6etce3t9qsLhkDUn20RyYWb6fobyjgjOPDAZf7z1OMZ5zRyRShf
iOHKhnrgy2IbbHaQRYb4EYaeHyEu6230jOJEzecIQYRu68mC7dP5N2G6pl5zbmay9IyxkIscX7Vp
eEBwAkl5p0eTplrQ9epr1jx5cmmvBWCiYCYF2jmg3o0cGPnOpUVcZiL9MDuL5Ne59iumCq7SFJb5
+aaoCS+8Z3g0KtRAby04yiE27w6z9jxgl7YViAoXfaXlOQ8S2uKb2uD0Zap3D59fLJ+v38izKEpQ
AIA+kmcJD1aCbqzZ2DJ3O421X2kOcw7pZ68GaQfwiMoTC0ltDMmDfDEdDjTpcaZ8kyhJ1x0KVhJw
boCBjFDs8/uK49o2V98rWvalr6VIVnR94Bn9ujmgKwAIm6O/ImAMO5IzTa41mnCwJj/3cJsYz9OG
8SXrcdXoGp6jP12BKGaKRCGJn5vKeXQOGa5FYJbsXFqnlZRSKFVDsdtB2DH1Fmf6YZwBx9WxuUTA
WNQzWwhU6pQmmE4a0j0AAlT7k+rWD0lCCs0QWMeWjxTRYZCMqkmaYfUDnL99OHF65IFEBakgFy0p
ez7kbFVrVrn72w23xzJz637J/2gU3fTvA2u5G9XpN7dDjAY8VhYi64IYxhcmvbbpTUNbWhKIV8x8
WM8SvF64IOYk/jUAH37og+WG0OxQZT8QfJID8diI4ilXLmTvIYU7t8bQt3zbwaheV2pGGP3hK4eY
U1W+Ndef6yl2IkZmZsUideQZsIOV2mqnf8D70lwhMNEF5SAXUuIVI+BZ/Zan/6nJFHciMSeUOtV2
gawnprQQMcFMtnlu2JIrfEA5xHbEJ/IKVMS7Cms+4J3aa+hYhvymUVnik52N+aOyWrB9DmKbGCZx
pUro41FVHfwf7qyHBHH66RHqrfVtOe4gcsOXK6rFJpjkZSlLW9A3e3WA4caUXQU+YmoUoOkpMT40
uOv9HpjmGN4zlJVgqyLrb0tWdiLUKkqaqUmXVuv5M7EEOFNVBpiAtjpBHssOd4R6Pp/xTwTesg6y
3oEASI9eCLq7l0skJx25wXU6P5NRNje8OPiahLGPoBmfbMRZMDt8Do8XlQ1sd7VeBebxR9TOfn4Z
iusuufgbk6KL3ub/DH8xGTZMz+ScU8WLEM/B4gxmC+yhttuJEd1+UVxE73w6afWMLZ+OevFELYl6
pL1G85/6PyV88iA/dGFZozBZVZWsLW++GJBF0sX/aNGDyQ7pOvnyH0GHWFqo7ZcQvgD11rQn+PFo
Bsr6UXIDCnDpmftcEC827aD7wCLgZ0EnEBYXDjN/qD4PtNlA6uw23D7KTGncUAdFKgl4n7bLU/gG
BcjOlLUy/ZZFE9DNEemAPKxawNq9lpJ9HPrlZwSDzJt+7WWLOZgteLEc7Fexa/YYsa5ePm9p/iI0
a2YEzRflCHknznypjWxBsdisDRlSGS3feyz4QXqt+uwUP1b+6EfUogpcF9J1jdWglAjg3IBWh5So
zx3P9GkfpZk3nupPcAY9J1WwwL9q+BeTMggGaGMzl0QZbq+e5l3MZXtgLwKBbZg5eM9SaL/zPYcm
9pMof9V7q08JI8aPDB/DLpc7/2yH/5zc8xg7e4B+LoSic8CAAw58vWs9O2crbKSgw05OSml0yNjm
UuXk5tutZE4qLkKqiWdkwCNQx9nNM0MQ1wgUGXMt9VcizUJ5mEqMx2KLSUCKknrMM0035OzyHCeZ
N+84VmSybU3RGG1ErXSv6wwF427TnIFnxAvek467tM+GZneZk5xdezjgdDLuE4Pp//sl6nI0tC48
RWCf0DVCur0wz834xIsA8eC6kQwdiTT6NE9foI37kpNJGs4EBa+1SjZ6BDkSelBLF6vSvFKF9OUX
r+Qox6FPA5auDZB0uXTa/LZsaUrsDIFqKfUVBRJKNeJbke1Z1wq942dgFpBn5+NcMT6+wSCxz18R
MKQ4V25kYe1ZIGunxVLIjAc6LIvDzm2lnHORTCHWJeZaWi0umMTHNJKAs1c8PAu83t/xMEpr5sRj
R2P9iydskdGGVskFP9ZGLQwoLBacpdDn/H0/j/NJK6fVCMNC0fBvA6BQypsFe3Gc9JO6fORJ60+E
9ZrElGZe2SlCop7ZCXCpwdB+VYGQfWu/KaRF5XnY5OFpUOJ65ZgQrwoGSXP6y6bKU6gUWD46Y7LD
A5Xgifw0NEcVQuKeyfzzeHu+IVQju2Oc0+VHif8qfGjTKYvIO8zu/6/K9c5daOSJGOFx+CfmpvNw
M5qPd8d8MKNTxbLgWT+h0ApHH3T84eGWz4Rfk+SxMDhgQq+IhY4+KhANaV0YmLAX3ERhw0S4kpOy
MdUNWBFH8+QhXwJ3F1lcEeYCj6vqTOzSpH1dU22r7BUzAIPG+eX8QwwYNcqsnzTjQvQIQ00adixI
n7MD8lXGQWXIZ9FngJJniW6iUOal53UR04XZ9T28ZyIxCbVUgGKwQYtaAwkLuNSrL4/wRRmNfBJP
Dg905+LkKN4n7vFNdx6eGAawR55onrbNRlpdkOHefbqIMAXl9atPlk/MPOfVexpUuEBWTli3fQ6d
aiFZxXI6/XNpE6XhQwcbg3Pnkl0Zz9WEiIORE2T4h6pnvgAFkalJURnBQIDO7xNbv5J5rAP2FGtv
ydLgoKsGeIGT7EFpu5WEeVdmiJ2NFBRrO9Sb+B8Qyj9g5ju+QjysOk39o7rLHp/y60ysS7IV2Pmx
KVbND2FJ/MgPKus0vRWSegUBbV9cQlOSpkYyvdWEL0CTsM9+L9ajW8P242dOPn98X4/RNZbERZst
LynqdFrKfHl+cU+LrKq/Pn+zf6mSiNYk9dXqwifI9ByzUTS4fB02kgh53C0TMEn92Qw1Iyxln0ti
Z0LVKT1RuQtryelYzF/onWcldzPZL6f7culU//LV1e+lbzR/4hrTl26JvEaiGGiauYL2EA3oi4of
H3XXxXy/2corJW7gyZmYW6n8W9yBVZ5bNguVuJkcchBY8epbgDWKOS8G1T5NBVKP4rcF++cOx2+Q
seNr69m6phdqTj6LEbgDnR4u2aHSHpwl0gPoanPALLVE/ZMUg2x5JrEIpE0Z1FrcFs/G8UH/M174
qBnTH9Bgn7RUiKGGAE1xBYFewr/hS+YgzfRY3oO9wbg7TGL/7l/04CYj6h51yBwgNk+JWd9CUGEG
4Mwgz6/acm9YKJT9eIziFA0jniDz0xUJMK3Q0LBzNJU1Fz6/9UqWKgYM/v9lBAWRq5SLfkCEcgKI
jkXI7C6XuV+rhU7XXh2UuHZAs5K+leNvp45Rhve4pNnYcbMrMZitiPx8pJhJ26KUmX9SK+5/IihJ
yk/4aiOk42LjVT27gfd3FW1tNX5qbCR+Toz0kJDVTB7DlyHEMuDhYlfGsm6PZCflJpnxquGx/VlU
ApzuK2sQAyN9ui1M48RK/b0hhxjyOGBpxCBN6Ge9PGwOANcdcnkf7Gm3LhEwXSta/qU0P0UzsiEV
ZdrdSGG3P8HR57wAyRmeXYQCGNZAlC1+cyasLrEIzXmsM0BM42dB2+7GxeB/qfoRGBoj35TsHpXh
FEVQ4/VukHw1tiQK8JBNSKayI4YCtTfb5+qK1c0cSjPPExs88Rw5+/wjr8zwvkWJKrplfXnB0D2H
HHjAUd2JPN0uP7qcEzfm2PDLHfCPpe/CD4cPCT9GVGLqpXyZg8cmrTOzI+gzvLgR9ch4dNHaeUTe
lRGtbRKTrjp3btQc2j1BRcKcCthpbkrTPmjpySNvC74kqhra8TpkKUHeCa0OJmZpYz0ghyt4LAW8
5C46w1OpkOCBzjtAnK0742a3Az775NjTDXnc203Y5BMrUb3+1NVjGarQ7Bpkzyz9R+KbUGSBvqbT
5cMF9g5o7nqnpZLQTu1PohQ5DVR9945zvIS2ySxxm2kfqyTOSXz4EuUWxdbgfgTVHN40rS1Sh/6X
r93ZHUFeOoVCXoKfOILqYh2z04eBjlTdSTE8LNvFsm5+Eo+HrYAJafFcxIvSNSZlEtADBZF9pwzV
FzHeEsUf6PsOYogCvC9SXy1irv/+nwt61q6S+YPeqi2xY6sHWjLdRTczKZRSGKw9r51oODgosylc
VisomF0kMK2lYu+bH0OVsnMC4yro2m6IZpEFF4mFWQxencrl3hals4TPo73tuTEEBKEHa1wgXrH2
kHC7HU+uAgu9sciOIbL/7ELpUupdR7StnVtqHHWCkaEUAdcV0lL6EOdAB/eGCpbgGFyAMiwGWmcC
g1J0nDplfDUpvJMZK0h4NLH2q+UXkCM7eCRs3c8FtTL3fRrX1s145YJ2lGxNj94bGYfjeJa2TMu5
cmDmMMAlpmSfWVljAlLZ66UpH4t3vuVDF4kZ0oUfrtsbYMbt6LHuqKNEDcgrqgUCzMvs6rTvj93e
3pCyL+DHGRXBPlpgKzRLBbZ5Ma/YH51nkwA7F5QK40IWbua/BCzY70X6XWDkPkJmmST2Rk/RevkX
NE6CgXDjX/YXff/B9quGXHGoAyo9fYULthxSfBxIVy3YWMPlHlHoxb5B8Wb72nWLlf2SB84fj1lh
i2OCsTKSu3bQygtkt5RrgYlKJKsmrS4MWvbVhepMjkgVlrZt7XiGgF8mJOq9Avkoq0vraixEPEOe
IH1pY4LJ20osC08z27PaoK31UOnP4PJSvB7Biwo/bNf/T5Y4yplSx1NgJRBL30j//0VPipKcD1pn
fGr3u0hYi5VrHZcxD3AUzHdYVE/Q06mpNmehl4NBpQM1PPhXSr9tXwfF2niEWHF9b9YW1OxMYQMP
pXyKyQCodXkYglMWjbsbVaeZ/GsFuKvKBwsBSDYXQJ2NcqlyGy4324DRYw1K8jGURb+8VTrVmiYS
3uXrjIbE+ObhodxpDoZqsr6uTPaModeyaR53OEHwp6m8i8s2fCRFz6IfR8cwczltnZPWF5O1N9oH
11D4OcS8q2nxKoHIHSaVrYK7eM4ui2+diuvWmBdMiMl3dptBax8b/3Vvjrich1TzkS8WB8EVjLze
6LP6Z51yqBa3Udj4cdgIkeHOPYqE6fGWxvDoraEdC0wOPeNvQJkK6ap/ZPjLrqnuFXoo2zC7QjOk
LMBe9mE1opwaoDLP3dtAcbyLqnLknhbnxjJaueat4NuHIZa6xwJInF5y4RMEe+z9I6yXGUVFBFkn
ymFkvcfUq4Hnw06mDnqsIUNpgtbbVxPge7sfl59h1R58y1skY7PtRdFXW0O1x2E5iq2HhJHRb5bT
xEghbRfCj8RL1gJQb6entg0L+T851DpbGay6TtRPwgttQIbzbRM6joWil0S3B2WZyMW+iuzgZ/Sx
iPiCTkHdhhwHQRp1wz6i54RqP3jNSDfkXabQURW+EAcLui0P4gZy3UVRPFFs4M996wn9zMtBWV6J
1vEszhWLjFjbBVp+uVKjdLFZL1V/e+h+DJo4skn/uahm0IRdFbYVMwavRZY5Qk3QiRnEFaOLPztf
lfurZ+Rgrsw5XAhxv7OBFc7YKym0ST8Bf0wswlhZa6ADsCmS/OzDHpjKL/Y3DGtwXQIbhGGoHLEa
vxg1eZEkvBLrVjKw540k3FZNN2RxP30JoyfX5ySMPk2OrFXY0SzjjzC/3X+sqMyk+o/fX2QsdvTr
oQ3wcuNMP8o/aqAnZAAnELX1ed/A7rGG3yx47045dCx6iL+I8fcatOv4j9wJPBgb6GiWGSMXTbiS
2lY3eD1m0ZuiuBHZkuAw51K5iljRZkB/C4POhsOXzmNspUMRjivefrjA629Vf0csi2l6MgVJvfAB
dVzoYWnJEGmwi236e99llaC9JjlnkZ2RMfNG8JkbgfIxX4pdZMxcLZ0OoJt27k9jPc1jGm4i3Au3
3Q8ydslGZF5c1EUP+ycrWYPk+oDWKJekHIcU+v9fvG3xYJ8d6SuAFUF8udJfw453IcC7Z2Sr2FdA
NcNWsW2FK3GTK3cKQkAoXTYGdi2XJoJhl+Kf9/86E3DpGmQIuyLBi/b/o1aMIXRKlnEiTDEYdOU9
/BOfARF1Ht93ZDBIds64RaQHmNcf/Lg0dD11OrrwMNjXrp0zbQPAYus5zoOxwfNxw1Q4KgQj2onk
9PA26Gsbr0CZcKaTpQxXyUMa8Cr/eJmR6ltRNej1imfJ9hnwQjyYOP9WFBu3xLUkrxWtzt+wtlDS
DbF+0dAZFUpyZvD5JccE/QLjHSrst+KKf9wrtqkzdWBFL/ajg72Kn5JroELbqlcAURYlbmoC7q+k
UZ+WBb6V9M+6+F6sDQasR5jLuW1YfPL7vc4spqGO9ngkU05ja7SngnVUZSnWEjCgD/WaeDVfuQ6W
OWLQZ/hSWGy2/c4rjZIddZBs0QMWfhnDGTLtzbJDqsKbgFjnW5gLP0J69CPBJ7o3sQrkFbd0QjYQ
6kFDqocu0kxI++Y0dt+TiilEsX7BIUrE/P0xtJ8Q3on1Nxq0tdjSl4f9g01r1LrSsVcPtVkKhCeY
apc50q2O30FzkEl69t8TgshP5ILrK5xoiwdkwhg5rKQuEJlkhiimS5vc2Kc8wttu3n4VlZLYqx6o
4iemaIw470BIQPlNGQJjC7ul7TySem/FrE6feSEBiNjWunjkPTNP9dEitCnHbTpv+MuoAkWfsWT5
lGYZf8VOKgpoqSxTYYkplRK/42gyDAeyl2E4aoknvZn3YTX/2uLHu0tCL/QPYT/2AbF9eS9RR8+Y
Dw/prujmPphPd8xtFhT2vbHdUU1tHThbNSziuOPROAfq7RgRTLCWI9CuUdx8Acx/e1GrSOsNlMv3
7ICdlYV16VHDCXddr913SOqWS6esgtjdtbb0777Fx7oAjz94wPYUBUH39cYFokNlhB2hjbxLYOjK
239BKEyWnlHDa71CJb/m6NRVsmrxim8PLur8vTgItALGp4HHLz8ll2oD3FvtnZxKNFcryNY6X2NW
rs3wko8O8zkj6a1JnqWUjmo8XRF3cmcHqU752A4zJfmJjeDbc05zp0CwmzuHv+nKHCwppGgtKMCJ
OL+Vn6lYpxF8zyn0xfs+9lPgm6dVV0XxYpFKArNxquFzqt5sdO8u4IVvE9FLciaFpQWmO3Evre4D
b0hBNxSUW2wvsjNqUI1FbHET0hf0PDViFD+dlpHdFasrHcgBp93g1iLm7WaCK/5jhjVjhgAVSWkP
JQiKBM1quxMcwSleiwZ93n8qhxtU6utH9xLN61YABFrsQNVkEIJkXhxmk/vucaPGOk/hfPINk7lS
75CB22+rw2xX1TomliFr72Q0HKfv0l/EklJoEjI1NctxNot42asZl3LSk53nLQYdfb545leBOZiU
O7H37JHb6Gvg0fBrTq/kwMMtjTKoQ8dN8DFkIotLOBpFbaK+ykx+xVT4su8YbL0ootlXEABxQNjN
8faEZ5k5UT2TIpglPqNEQGGe++xe7Qiyuipzmd1xIONw5SbV8GytEaROiAobUSCRJgSPOpAG+l0N
qXYPBMKxKzd+DZBk1T8GtGSarYlcjKfkva2Fb6Hln6Fucy0XL15V7A/Fs87Hnh9drlaTwmGughDV
uqUdJc1SVgrdbMtenjc5zR/MUtA13mguYi9HU5Yn33WkBIrby8NveN96Ex95wDLljBO8Q8uDxH5m
XZjB7NawTyWJ8Eu9sfm4t8rWFcXEm5yZhCa/eKKsPTdH7Q0QgAoKncx/pQCg3fm8JenvQ+ssGntH
KpGlIcRui5SS9MCC9+TAjabBwphfXOl6glBDsKNQDLETrPyYBetAzEmpqC69hR8hDEmuk0lJTPyy
hGafD2extLkm1zwHE2tdjZ8jEgmBKoyipYv8h5TkAChPZNyRbi6eDdRfaRTRZY4UDiLScanzY1Bb
ERjVGRpCmnNFM5TBTxjcWk8nDPRs0nizz0zuqs80By4H131hPJJCJgvAaqPNRRF01O390fAeO09R
HpHAy3bdRvylIlYo7NcdcB/AZ3caWMSwcwlgAEY3Zw2IkwBjiHWG4U9WMHc9X6Ip+XlEYSV0fuI7
sI2kzKQ0632ca8nslyVQbnkte6GnPx/b8Ml6Rf+/vHoaH08Krf34T9V5mjJrIsFxIFVdYqtLW98E
mDnkA34sI0TPufqCikHvVlo1M2cQmY7qgfyr85bXLRz9+tZvDs7y3Sk3EBJNwmNy7ndAmVKrWVnN
Cf33DiETOSkCqvpWVL3DWOe4C8gIjZ5sB4T46Sf02P58pYDWR78jJvCTA48ADPotVX6NFInF0AsZ
5VX0YBo8ARVnlAOkptlJHgn3h19QMCZqSGrLkG1cZon2DOxSj30sbUPQyAn3FyLFvzE7BW5B4ZCq
6eooltauer5qbb2RwkhfNeN1TOHHnmM0FInNOWNfToEEHXS7nId2hwUxtoNjg/lTWlcQugHhpJFC
OL+UXAg6yCLa2nGBcFvfvrM8YweXwojr2AMlAd3qYxJQnQK83CvtNvitUGORcFUmzUy2WUP8byJq
It+tQMhBRvVMErxWlcdAjsYCKvdBtFOBqsbCC3dXEnFSK57W/GAXD11d2Cb7MEsOcS/cLnHcrnA1
LcGsqcfFGTwzjdq0H8OZMpmilVqocEM/U3H8DSMMJZ55DoiDZFbXsiGDYPhaGXWMUdy4A2b0oMgG
QygaWwcRB1kTa9X3APWlPQuQBxs5cBijydMObmufaeJQoJyvES6SxYjEzFVGjSduS8HEf4p5AwSD
dMmsf4NrRM75j5bTIH0YUXEWnd1ZUfOqa4Ldh7La+GOsuKE8xDRDzRkR2+gaWTZEG3E9bk6j4nuO
qvYxSWb3BgC7PgIF0CrbZkUHC2mAHIpPd2lQu6o0bY55oQlVlBsmCDIHj9aFwS7Md8DramNdcVVl
EKt6u1uI2T25df3TmsSpP/JthTsMzL1aGz9WOi2SAd/2/V0LF0c+85N/Ntfp+XEuFrUsWzM9ohhy
fWCdr8ISrgHmgIZqfQApRTbBBMbEpmD9k8W7YlOsluisGX9KUPB6r6QGa+5ErK92nzMdCnwVMtZp
HQSxgDdE/Ik0Lrk9LoBbaoOTah7KscGNlY0T9Zsho6DlcQtT2WR38NgcMuZUOKpWep5tEg7nkg7+
pRG3qqCVBHWLfDjlqD/b7jnZkI+ecTWHydqVrxWtkbL8CIPvUOwZh7g4UyVuk3IZVflGGb9MWT04
9jVk8f6VtB4Gqx8oHtkg0owHwD4oKSe619fdN26Qde/G0BTEvrkK9FSESxCPEAjf865YbOOAw3Zf
lKC/jKdFrna48SE+3ykCbh2O2zAdWu5P3HvdtD7JTfAtflt87L62J7HR48aMIOgZjUPJgtM5fqgQ
HMT0+2pEZr3EkddRphxtXMUjp1RafSZnKJslSAQfYYcibfiUEjecIa8+jGzlj8+bQrquO2FPmlTE
WVuozhRB4cqpmKwTw+bKRK8ricVwT8lVPCmyN7Lw2hCJNV8GWiRIWO9n7JS0kq5NhRz0vhdmACSy
bIr+CNKnjvGyM/Aag24AibAecPTu7zR6MjPIt5n5bR1rkVU/68IT/ie/NN6OxMfhQ9eq7P+Sy0VN
FcZaO/CMcP3/WQLyelFErml+g/qS/kzKg/5Kq5SenLXieTuQUNokWKISbqTwhyDa0tjPjUScNx/x
eajGNFMYaHNKrx5un392DIFXisd84pVmBiNiB7pizzWEuqhqDNr8/XoBp4y+54h2jFqDE5ntwkG1
2y+SDPa0XmnbzvW/obiBArZ9lY2zluUDVKA7kbJWTLnYuum7mJQ11ag7iKATRZkipBk/td+nrXDg
cywHT+9FONlwRDWaFrzL01XI9X0nZfPQcIBs8ENvuxFvR8+N3S9YPCh4uzI9XY9mm+PNZoe0x8Rp
/vS1e0+Tqw5Q5/HHiXO7h7vKoY70ZEFRIW3J0owqmuxoiOJPpuPJ1Oj1Kf5lvPQ+kOFx6GNpxI/x
Aj8wPd5WbjvEh+LxN3gdQZ03SW67clk0tJrwRRkpsBd+oaCUh7rEFsiQBh7R9GoqNLRhk/tyVAN5
qAxpKZ1rzYuUWjnuhfW3uV4u3Mlk7x7JJKAbfoy4PGd8E3MCiO2SxE1GWFEIPuxD6+Z/nDkI35+9
Eryn+39kOpOAKm+7KldtPiOLiBVtmF7Cvp/iYHCovswqItAYV9K7KLb5FCMZJPmViGfP37O2hL3a
egkV6pZxF5i8pyX4uoGxJe8SB2Lc+OcpohCzmp6KCHjoM/ArHmMIvZi1y8dV64aeMhBEbCUt0L5I
iFLZoJWo9RjpoxBTJQXpOes8zmTMJy4lIdIy3xHNzW8wh4LuNcwvcCDxOwZge6fYNweELLYZGGAW
NVgq6GJeO9age3mma2FdTGiZZPNdrH7rIQrOO64u+49juWuVbwrSJFlZwLBa9isGsZu9Fh19EDqm
g3traSnb+ObOfrtQJLJ8cACPCjzCERjEA8Xlp6AKXjJGQ5Pl54A30j+ZHdpvIkJGBGjBZt/W6gIM
xtQ+/1w+KK95rO3baJX1n51RqM02GIPWA63/x9+4DJi3qywcJkJh7CVpES5yXADptt8oNktc0cZD
CaHEb8U5D2nxmw8o9Kzs9SbDCe4sY2rbemzGAt0pLxGearJEz7kv7wBRW/IQp8x3wJFcNmd6vu8E
pIglnnrgq0MK77P00V7CSvzr5IY6oA2KGyHSlEKMJRT3jEtFQAxNec7SnW/qlHQWeY05TFsbJ9UF
3U5PsP8UppYZzo9Vwpv7Gqr0rrvoetbFElhDQ1vUM/F/t1NEDZYr86VQP8fdMaMDwmB58VhJuGyp
1g1NlakfBdMNO1cld6DLMqn4dia+pQSHHC16wmW4l4kfC0aAVnfraN1R8mAnxdm5dU4QS4bVkl57
6J28zX1GoL6qWOX8b8P3Tmz78MDlm8oQwS6+t9hyKGRly4yOYLzeaXSLnVtgfxpCsOmQwA8vtTZK
ZPjj7jl7c/snJmyH7wpWxAaPMqE/QdegWusD70aHQ2amSBDpzhNzat8islHnvniTRa7AkYpF9sQQ
n9Yv3J/cN7GR6T2eGWVlTdrRj7cH0Hk+11Ulpi8tXp9MhhG7jS8z0LgrF+mmH2w6pbZdj45hWyRC
8U9nT+QladsngAvUyRD9i5Fmun0VUhLQrP5KoXxpuNMPdmcc9aBcI8W152nEfL/YODPp6ka39RQN
4DBX2tcGaaBKiLnAPKmqZC1J7TgeLhHgAJbcTbQ8r173OGxhmbyalsgdFCCgfldGh6tZUsdRxc0Y
DRMzp1fzb0I8DGumLbDDLlVdcdxEdBB/PIcs9KZYyJQaAmTVIaQtzM/oL/f/OXxvzHjdDbpmYvMB
3Sk4ZWzjqn/R0uEycpOkwcZsjDtLWE7oXnhryJAhJv2AEwctH5hd+R3Bs359CPajCEWJ1TquV++l
HPVxbZAYZ08j07Ypq4a6rbmdS7B+oues2q8TqoCaDcC2z/35sA8cOVxj4jMzo2ARfzi/+2OXujh3
pknicm+I7xIzCBl5IeauVR+xF/xkVXPYppvU/zEMmvu3nvgUVdi4FOzr8JXn+AYwp6pIhxmvLDKo
2S3uhBqHll4Xr1WkufwcSWoCN4zSTeNW9j1kier8KgKzPSZKr2egbWSLtgXpz0z2uF9e7P7S3Kf0
/HvstY4/HOonNtqIzrkRq+USr4J/OEX3tWv/7Zwxe5Iu52RfaUREQpPe1JlIPnkdB9INd2FvktNo
5lj/L3gUbfRGwFZGwySWfgBJSpj/lmNklVqFJcPI2xa+t8Fa44fiDo++4C3OLYlHyPIj0LAtM01N
JXpDQQLxUzs0taRbJqLq1ER97OmQTT8naYA6KJYln1EFBpHRMxs8OdnkvCc3Oqd5BV2Rju6QOHfu
yQz7YIuclOKbwqZpkDkCwJafYfyLR2ssCjrBLY5ofFZGsAhR7LHi2pfYywEUHUer8r65vCV1PKth
eg05S9q0qsHkF3BJGYS45JRDAnKvoXlkuw/e/nHzj6yIXEkUJfs4WfyrJZhhs5dgnjnuWY64QFbD
0ldQ9ZWnDWMNF56X1Xo1Os8TaEiQQBaE4PbO024Bwr3akTEzzN3lEfGXDZTQQWU14lkNg8LjQQT6
tJhF57szMgv//kUNn7B8mD6XOxlEcWjqMFzQx/omPCK/VbNADJ8QZ+7qzmS3y6eA6POPGOLQT+/i
/8YWrV/lE0vEvvDbTrXPADfkXxTcLUTYBJz3Ee//wQD69IZAdLO+oHw7v8XZYoOsYcw9EuM7YQO3
MDxbEuzGMVXxw3hMCE8GyKZNV9dvwhJDfGMA77SkMTa9bl1P4gyfR79ZpdFraUBgJvugL2QCfZTN
V969skR+LucXoIEL4jRGoy874PZfoDZjWs+0ERuRExqayywRgCItCm5rRls3dQezg3v7Yr0FAUEi
MpT97o29b0uWw/01FRV/Z15hxNygB57DsEMeFepSpyK71L3vdFN2N9t4TSEF5kNxkF/LR8MkXCFf
EM5ffo9EiKyXoxcfCN5xN7U0voGzPa2jwTPuTs9qvGYQpIzTMFrBZ09i15ecsfqyzpFALbLovyBN
VytaEiyXk+k66hpbdYGW/FZwGawXvv7zxJhneyh89TcNSZZHF3DaOAaPFdlCYqDkduEwMyC9yIuj
IRzIu0NpPfVDYh8LisubfiznVWJ7QVaAhAi3TGJS2E5TW6QVNUIf/cYG5QSwISoJsKZRlkAv+i4w
wudLMl9OBgwmo3oqjgLd0xqZ5lczIJTaiRb10Lw5rG/1qkLSi+cRrLurWlxJQvwdoJXI7DB7oh+C
muLWmgsNLK0I0dBXM928sWRRtV26MiQMRFQHBJFU/mZVjVsvHuA3hXS/Q2urD4L/hGofVM3S5bDH
KYjrGoL/faf9bU6ObtkFYaxrBZJs/lnv+Rv5lluQdY9nu4/UEVVor410cXDJFG4rk6/O2jsGgfNj
eDbyEthdC3EffGBPFeorSm5nGyympplMwWDc3fV8IslvT6bL3YDw1vYJUNF2qYpkMAvNdSh5aDLI
uxUvzS7Rocj/UF4CH5/5llITKyIN9d16IkAery6mX5CWaV3nWGGI5rG8dbTazb+2PRPgBvqwJXsX
2oO8IuaNXIMCmM/4YpIMO4B2l/xAD/llxMk4/UObN0Be/e37ao9KAUrpTm4ykEULnj4qSKoZgk17
Ej/YHPaSnq41gIqfxITzRfQ+QJTo3LNEJ9V+d6kWiF3qsypx4DkF969lespYioowsvVfotFZglS6
R84sB84Qnqx01Gk8IZnOeogMnvjsoZaiCja8oLxYtHsS/rjWBpnVisGSDgIip7vNzqxuwGLnX8Yv
KNuXz2Q/sQCiUA+Ihnx6kMrcFStbNhK+OIFVlJuz91Hb1k7gLIPwqW2qPhve+oOXOBRIKXBPF3no
RjtF46sJ6oslFJDSksliLL89BSpIdF47dUhxC+BrEdk7FpuFzO67KkoOzMTUzJkRDhBEr0qbVdjK
QuozR7Pp6SgIGMw4Ci/i0Xi9F9KUQET+J+ya3e8a0aJvXPvp5b6PCVvBEmJ1KoLbxGX1UiO6ldQd
8Gklnm7Ktl+5X4dnh/infQgNnuJSh7C1aUwLDJSNTltaFRhUYfItFqXQ1An1MiIpPPymLSVMxRhQ
z/laZHcgtkUgk3xiO7+925h0+DPezplB502qzVSmnmFQvTu1UTuqNrLGSVeckyr86DeAwxVvdYd/
oovytsD+JRbwufZWcZUf56lU8MLCfd68qypLVUBTpgXaSQEU9dbWIaXP689J9At2tYhUEcJZmh/M
Mf/qxUax+QWMat6Pep2I8ZtiwhMv/ZVUP6DdhXxMj8uw9lahOQTVNGF1GkjnrBIzALIm0okMBik7
WBXoraxNzJaoS0YoD4VilEq0XNnAxIbSiJ/8IlBHMNUq3DKUzU6YDMDylbVGga+j79q7moAWRr1x
Gb4HoJqvLG3cjZmlxFsVNr+xa3brpRzMbOwoeZB7foX2nlFkZRP/SBVH4/ftJs86arz8UvN1zIg6
Q85bzrI7fWLr6tDJOs2amdQTyks11+qW/rk4kJAL1SZimReZvJxhjUzf3VYnkY0F/B2cBgXCh/69
AJOo3gHbiHIdUQcbb4pD6BWOCmm/qNZxJx2P7ctfCjh/zIZP76Mt7YVQEs6nbvUPWIWQlf9wSnpB
VZYF230RCakoTzWbB1EiRxTCsAF5wX3HqTI6N1MIeJQp4iqm4RKffZCd6nlVmJjIM7euEh5N0dMj
ae7VZmGzkx7eOFdLUp4CgsHWqYsUZOIcqhRZqRDsjfQRUnjpb+xvuKNHEgARgLWPlNy0oTytt7lR
g/c5zdgALPKlm436+mzq06KuhKA4Casyd7E5cpOFlIvdd+NGPLrkyH4eKH3D3fKoilGNf+eJE1hu
+Y8Rg2QUl/zLdCvX/fIA88jFTxLgcUKS2iYgarGjjdx58pLPJAl+Le5bYdPjKA/5itVcWgiDb/rC
FXThRulzXEHGANBLR+Mi7tRuNRJy3t1F+VK9ztcXuuYxxLYFAQ0a5cBqnqiV7+8DRtEycHznNI63
7oSZLFMctD/NDa+RIKKAruQhDuXR0My5q4uPshaLfr/VteqbnUXqZnXpVFkk7xzGzN2KnYYniO1o
3Kwad6tShbQYHETm/o1049T+NNEgIae3jJ2WYl9rDFU1srpne0YZHajBAhjQ5/n4kcer05qQiqvg
TYhgbuVo+tuFM6a7+kw2CHwwD0LM+17rTMA5VHEYb/d8Ba/Vl4Tpy1RerfFvxJVOOCCTRnYNxbnp
6UQ6xWi3o5oDF4OV7pViLPgzI2/e8NHZAH2xOUWQEkDG4J8vbBfUKFaUCPNxdtFiX/DYQSaN6w6E
+RwMQuAi+Xtp5pgYYLjNxr09TO6dwLmyp1TINpa9b7rUC6gu1uZDndc+WFTwjzMf1WbQ0rnoayyS
Gh3EFP1ee/DhIBPYf9OF6NPabz/URMYirsJGd8dOt/zj4Z1YCYbJ/w0GShivWjNGINIufaJvpIrz
TkQBUdINExpO6g2YqDPjMCTdjYF825cWYoK7OVM3d3wZvnGbG+BvzyBmIc9wXv9hOaJiIr21Yy3L
u1ykky3bFDiwmNI6uU3KlDBkvqdenAdqCpnzjJopZGUTTCR0iFsyGkdbHHh5bSTJ6PTVxTgJsa9H
E3uI7TieYFQb9ADzFmGWLvlGtQGtH4COx2CBeB1pIWvSbNmOsVEQMyBbu5g94Wjl4X8jxVy1aurX
NwGc/3HBXkU03zmvAH/BYPVfN2wH+VjIT6ZhHd1lXbWspBOxGWjPDvKCQIQ9tnN5dFzfkiY2DDaJ
/OdmEuDxAh0VoYkVt3DqODaWz8vZr9odi/lNUbKKSgekMYnd6WCy2eMfkdrcPYN2AIVzZSd4iDlW
xttIsBCzSDGv9e6feN6iJn07G3Zrfim+LPm8DwGImMCI7V9r/0cMY8/EbtzCFMJ3xqqzm1CDvYni
TGRHxExakRaGdfmVhSdu0+nBSUr/ZCxU7lIR4ftnpkWIEmRLPxvij+IOxTl9z1za2y8jCefBfP6J
9j0gPkrFo8k9ML1Uicqe4K0hI/2fPFt5HXz3IM9dO0gGRs8MkTLqlmxvvg6mLsEHQzgoD4UK5/Le
lZFubnYquKSl5WClfYl+/iBCROEvbNA18zNtNHlsgfRBChAA4TRNiEakusE5Q4Rk33Ae8+P8QhMO
nrn2FOjjMUpJp5znj7DR5qef6th7zz2FNYJ4lx6mLcip7vyP4YU3ECXF4iksmaTNP+QbX6YRjAyM
RKInwbBP9oEyJL0K397f4mFYy9UcmqhRgl9jAcCXE919+uznTIyh36QpnW//DcCk1Or8qfAOSioX
2ktvGKavjhTExwRG85HZIH0xu+URwcrLui9w5Mys31cY+5ZgrCIr2KdA2jt67C/YEfK6fj/f3Uqd
VZya1rek4MxMeqIvQFST1BTlVWyKDb+LOhIkGpEXusVl0zfvozfknNHtTn2czClDyYJeNrViweaP
L7VAG0+MP3tnTHarkDzK3vJTDNleLBjH0KK0WEhk3AZU7NH7HzWSU2cF/4d1r/ueITnZcyTwuhiu
O9FjdBJxdaxpTQu3oA2MCMdC1wZsQ7a04UT3tfQTOYFlV4zdvf6hNHhgSkloGXmWF3kdYnIj9hZw
1s/BWzGIR08QHT7NwveVb6YZvSLMUcbrngnjMfBV4QAiOqMRRHtPTDnrLoRkxidp9pzrznMoPUrW
kk4JURgd4EZGtmII6GkIVocZOgjdIIyjRqindfGOAR52LCr11tg7EtNqCKGQtrKcZQTACptf8GN9
iDF4AO+zyHLDQ2JVx02OE/ErCV65z4IBkMgud5Fs+7eZ70RWazjU7q6fIBT8KlpyOlLAkH/4KyeP
ZpbmlI/USd1WKLti+uEhvQln0gqQcxeic0w2RYUKrruYV6Ihb+isrY13kYq+neGvX4utHydfkGd1
yfyrQWP9VjCUTV+WF9KvvWN5W6Ulmw6boOJmG8sknWTUamZsR0lb4c7BmVMJklzlXozsOAcLKa//
7jKI/ZqnlihMqMVWYsc2VD198YatKHGa8kXNbyxHzlYES7v1PI6ZhTWtsbMdtOEulGYvQYt3ZYI7
s/OBBUVHVweSlU5ddFrSWQzKjDkGIKWfJa4FTMll2rXdJg3X/AVIMLEz2JCtvtBd6DV7FfzFMJy5
5EszEM/ZIsZojw69wQKdZ94wQaiL2gQ6LslCdmWd7Wn6Qh+sloVP7IilxKnWFCVjMAOmy5VJd1BX
1Q5YXSfveaMuZdRQChXgyy97f+BFnm0a6DFQWAgFsuMKsZ6GRwD78oSmkcGvX43DjgXG4pn3RY1p
7NIa+b4ICY37YP0J72lJqRWo5nwEkBPASIuG1/p7ljj1bARjrZQvXZL2P+KnkcHsSV5YPIYDGcV7
zg7hijYSko7vu+09z3F50yFsQJIhxTKxIW5t+3cjS4gcIed9Z6AIAeJmlrpH4E3UweT8KpXvk5FN
8GHPqWZvRf62qPObC+nf6BZrGHb4C0CncvvJitqWC/N3DqgyDur2BmWYkWN8WQ4xfi0a8UjmLsnB
++E/8vAU2+KQGmiKguS3TwMuU3qzLNdiiKbL7umHW3nMxKLG9JzHhvSyEfKIYs66JLstC62rJdQe
yMV/VbMGiLPh4FurnzaAbWmEsEg26em1Pt9tr4BnOKVBYCodhngvqaSX4gPbJBWDbpo/e9fkWulH
eZJr6g8xFTgX+RyuwrJcYpa0j3Tk3Y4+VlAHj5r3tp21QLUDN6LhpY8tcx1xfG215hK1hzjtsSxe
ANM0o9vgWeeAh/yA0sJBTiNOj9Mt6bsTs9TAYXDB/0RfbnIHIz1WFdc7YPYoIEYS9AGB6lO02bmX
S9+BVs+yd/eVc6TwNzxDTaTnPsf+vxMYS2Ququ96fqzqPUZnMd69wtK73nvnFVKjcPUjZmJ5hqVp
iA9TDMtRTRE0XHFaLvZnMKjeeErC2Im2zoLY3aQpdI2hQg16h4wxgtM92INIEhXgAGSBWbHu2GNU
uf24RxEwqFXyQ690rL2AFUFwUJs/2j05Zp14k1NhhH8pLaOHdtCZ77QBWOOHOft7Sc9V7Dsnaq2k
1RNLvxb7kvgsO7zkn9yZnAWLyOAHGO2cRUA9E28Pjj073XHlmZqhkc/z8mqYXR4SpyUMjP6525Zm
78EddpPONuBG/0Ief5edYGPfh1ETmipHPbFTuymbBCLtV939CyqWfU2CX6TkeADB/jg55OaszzHe
IJmwUmIwum+Mo1xzNQmdZdFD76CXgiJzbObMtvepbs/nzuhAJcJsbGWpLwyx3gOMwPbkHc65B50X
n8FYuN6uwWAOSw1t9/9JmuamkG+dViWNhmavrlzw/VLAKd+ea8ZeO34IBQ2pdprqF5zQH3VUjQSp
+v2znJYYVb1WpdBgjnS1ea5LKspKD79pk57XGsbAFqZ+grJjit9u2zafM3NJBs/cFSSaUS3sk9ca
P+IQGs8jr2TPQ+zod3WlEXSwsMXNfK9XLt6QvsohmCScJ+F7SCij+fzKIzXZfRgkHsxszDG7VTXP
UUVJBHZKKpSVcTUEGWXDhxruRNXHE/kSpBYqcJ7w7tL/iH8h+QbWzIo6AbaFvpbgpzfyWNaf5ZWQ
h5KFvTFH55UvzRs78NvU3lXyIC5YKKGBMltm+n46xlaDR4BX8VXJYZqwEE7c4lUcD1BwfDEQvTF0
vwxMZGVYdTmR1Z2EHfyAmfRiSFjfQlOHtTWFHN6CFHqCAePqHNARFbPe0Hg76g4Nav9k1SBW5FvK
ejpp5tJNftsppPpHKVNysITqFeXG4ErGKO+XovcKO1hTwwXtVlAFOGsvX6gb1/4gvLQFICdACJrp
46VKMiCARzoYcbF9Fg9lU5824jLVH5ETmyg91nqeri2nylTig2uvvKeHXACf6P4UGjbF7tgUz1Sh
TjaQJs1//Gsoy7ntj7003OjGhoBFAOSpJ7sda8QYulbkJtcIWqgOdpNKSbxcUCe35NYZw0gRFFAN
YSzgu+iwnrsiUulAbOhciG+U5TD6uq6zmm4j1z+BnfKk5ooCMBgiwfiLXHvmODz+Ani3iwK6MTRo
9znTXRhKynzyyhbbd1ZE3dRCfTkHBeOKs+5s8XAohtEDlu8UegwNocpDMPvL/HCRCXf5B5d/YqKf
vFlXqucDVATLkwkFO81GMnC2L/3Yjb2XB8b7L7wzN11afsZqdoK2CrYdLdpftHqNaiFdJbI9DbWr
PpGQfuEyU8yM46yJnFyMKI1XjsWEo5KhDrPPB4BR8q8yUPQh+dZVk5IQDaPOA7J1K7L/jUtbHZ8c
TH08sRTMKHmE2xidTFxBxBhYC2KYWahplbd2HMfBSbhN5v+dbjdHGzpIXQSwK6rMSVW8FJe3bT0o
rygaMpu2QozCA+tRAsmgvsYKhmXCFbuiqSHzuuks1A8Xbx8OxL5l61+hNaxJ+VdZocM7AWWE2pmG
kWwN8pVrYUhS0XnXpONPsXRFrcK3OJb+NFUJrM8lpQ0fRJ4w0jev2PhF98AjmB7FZRTeu44awXGA
QScCa7L5B0zXzjc/gpQiecERbHy9LFYtQDo8HgFbO1SPMrE4OHdnHKR9qtHc/+JUl7BHNQbejo+2
wAV5xuhdY6F08hk4ZeZsxf3+I9cN3LH1nROrtH0R0F94z2HJm68iowVdY01gFK/i+rpYtliyxpMm
NWh/PVw2eEGx7lzYGXiX/4OpRF+YuK+gLGb3y1hhXHAp2duj0ZZu14rldldx3UST09uMlgYeZfBe
/RThqr23adHLupIwAXeZi6hRcPShcCuQrLJ3g/Omkj0pvqKzgvC5bPrwiN1WEf79oYxPMu7WXcHF
Dp5BFiZ9gtOoG6NqCJYrJWe9p0ocTFo7vCjABOewDN9DS2cFKf6KCQkLXbCGYt6tTAaITll56L8s
KEzbNUs6Fki8JkczgUPmDcKYDaV0yDgL4OZjBl+z+WOFL1JCOemxkA4uSbLNUA0haxnteEoSiVlt
l3kyoPc9aqtYiPZu48D98/saNu1YMwKq3UejFAYpKK5raAO36FSPfV1ID00TlmbOh9PJgtC5uF9I
WFTBMq8cB/Q+OUPFvOj0y2yXjUzNcW5n/83u3CQDuD1u6zK96db31HShANJ6To9lG/FQurYXh+BW
5SJzlDjcrFuxAEIm3IJdPpj3rruUSoTfgkteML0esGsbDbDldGsMeOmCt/ENdMQKeVbYLMvQlmLt
tw+WNU8y0+hX0wPWbUTC6YjYOmR5TxmHlPs21lnNVfln0MDsye1C5uUO97FAVW2RCx+I/c+gjM94
lBL3I2QQHpyGj0WNXxS+lRn3EemWGDXFM3DWc4f2Yl7xkBd7J87Qyb39TWHCJd+YkCwUj80xx9un
veBxerocijogd+qeGupfODVN1frFKeu1RjA88J3eoqxMuLk/JcGyfmMu/4folemipi4NdE1s/tbh
78JzpxEpioHgT1PyyeQZB8Lii5B1yjugKSK7bm2Mc2/DyT8UZ+HkyDbqRtta93DnABNZmq9ryGYs
K4+vhIooHtxrB3QAvo0GqmMpd5MmAhP9BssNtitIvqGtD3TZGltAo/H92eQZDIOwbOZQyelEwokg
8oJz/gAsIaYzIlWZZztJk1Z7VCqDvF7W7+Sr3Evra6iKUoz6n+PY8qhovN4N+kWjIFnZ/oDwjfgl
7+mn1wJXVQSJ/LOokWc2JN+c1MV2qbbMknIVDuZAZJnQ/FkrpT6HWlLbxjXivHcYuFXqnO6sOoFH
IcQFJOEL1IpkJMVfhfqKmVT7fIBjFwtn66sIO61upz+3ZRA0qpPVrcLRG2jNhX2ewMYMZVLbq51W
6p8+J6mzQVMtXCN3CiSokW2rulr4uphoBODU+zd81SQKeuQiMOoo4hagycj+hb8MsnlE85YXVtLb
CpUD5lJEQlnvqRPZyxWCRTe6GBzwnd0GzLz/r1MiUkU1ZcOnz6wGQh7tvuVTyJoD8/brVmKANylB
LX0vlucdC7vYzQ9aCZY7jfYqEsgYLX6ElnxvBwBSooTXny4XEHzrePQIIiuPqJ8L5m7MoLp95UoL
ytB5PUVgF98Gg1hmOp1iaICLLxB8JUghNjI7jRpFW2lIB/+mXNsu9qg9nQB59eelsbCM+5Sd2spR
LuXF/nwLPxHxR/L8t+aVqBrCtlqAf+IQn0TRBAsHjswEV7HWkExnjBkMb41DOyf9Rf8xANl9tx2C
dWwNdoy1OkDNW2kbStZbwJ29qqTtv2rG7nVfnVPgDWDdIwQz/MVMwWm+zXlOd1wUIN06gMhqRP68
ap6gjqIDR5FEltWq68xFsMBGBDdYv/mEQvN3BFWacT6ON9ymmDJJWOWIyfeoq2Jh60tOBFzSl0/P
rLsenP7X1983lKQgc2YnysxuHZ4u4G/2GVmrGZY5Ovk4qwqg5G9mkboNJsnzyXXbKgd9qb4OmCyL
y/jzNRMtkdjfqTH3pc2jQC2BHrywrGlF8j4K2UcmT8ZZ8SlyPrtv9cFezJ1cScwATBZL3z5lh2/q
x35EXyeNryvywqnmsryKlVh1G6pm3CHAG+fAYv1wuxv+gs7xnaZDheb5sKFklZpb0bO3KNdt3/YV
0n8aWgvOVr7U3+Jel++nZxEKKI+rlAo1QxXB8EU6LNbgAbvriw7Tt1T5XM/C5uVzO2I5RM4jXMKj
DshDQ1tGuTOeJY3+x5goc10JeisT32DiW+IWPKKedJwOFgN85xaDLCKb6KAzyOIYBTZeLzuWwcSv
DI2zX4Hodjzjl0SWcaGiJZzpdUXueSBuelOPAedU+S1KYFKHz3h3abI3T39mRj4LMCh8SbFXjX0q
9yqpcCZsMggRM11IPex/DwCCkQiA/uQ9WdVZQlgCcJhqHD45zn9B5l3V3WgJJ7IA+wz+1qxUM4ke
YCIXRvQ11a1yiw/QLUaES931qpP0i0OJ5nRGlGJWCWpYI2n0++V+I5vIxd7bExGM840hIgJll9IG
Rz2Zklr0rB/v5TIXt5Xk0yPOrR1tlkWx9SCw0b9xmEK0lY/wtpImDgs0z3/SduhsUEudKzh78OkK
u7Sa2wEG7mK8rQm7JRXUahkqe8Hc1aCuiAGivcvdF0bBsR0OT/UQUfotBUn+L5ZHADW8Elzuk2xP
pOVP9KvLnb9i4BP76ABgJGgC3hAEBSV38HDpX1Cz3KLmZSO9H4dA2hWANiVK4YDaWGL7OTUZcrmr
kKWaBpzNx2Vmdbu8Vzm22th0FjrLkBPQM/5l5bsFkJXMBrCsKakeXYbcq05EfdHBtt/JqlmkqBI1
RXGjDGgTQvECuuZZwPEBzOyZuT1Cao23zhh3hFvPtKLt7CEMBQkaTyDZc+gKJS0wPdCahFkb8AeX
isOk2uMRMCsZ1tz9pmldiUTfT44Dxqs5LgLXtDKrQDlAQvFD58XOwVN4mIEEMJP+1x6kJQ4ZCfwL
PFvZuvZ6+RWey2KwPi2wbBAI4medq6FLZ1wOI0shulibjhyOIwXoH3SBiV107/zozsJJ9J+Ch+ck
UkrRXGeEVZ08qGYIAKKBJ0AOgvTItihayqQa1XEzGTMlgB4el/KSm8WmscgMuW5p698LWhPhCHDC
Z7U8x5HfaIvLxmWBywMaxSVtMQC2QeBmm7oz7fkfjTagR+Hqv06TQwNKzLFFCbcyKi7/fpcdcu6C
F+n1+0FcHCbxvt66zsGIkBTOecYJ+BOIYUOkPVcnJivVUxPSEqrzjw88SxKuebhj1px2uNw69q3v
jhiqjSU26bIG8i424w2mCLLYFbywfD97XD3XaH0+OreVsRmioPMQTlNmYtAjrmGxNgBlI3yqC/+W
E4M5gdC1P6/mki11JoLPzeD636KcNZWaLbx8wq+NpIxJxYNovQp7tomu7QnqhYnvKTPpnOZ3x0eL
s8e5OQSPJ1M7fpxCC8CWLgJms+A49Gfz9D2Qf4SANANP2ioOZcgwkHOUmDZrH9X624RPjB3u908u
7KaCZoMZz95Fk7X9EewImhrimbm8R1JweQdOfA7dXZ/drOxr6KMJ6dRxfIqI6uUTO536qaM1B4i6
QSEv6jy3Ob4y/MzXUA3HGTeCvzSKrY2WSHS4DWuBvF7R6184EToFbylSCWDutWh5UGxarVbXYYip
m6XuBoEwPQ0KpVJT9J/eJF7uob6Sph4mc+VvihqE9p6ToIol8NIA1vwvHnxDimXtx3esrGan1fi9
hNsePKQbEFj9jHExFXZcBrFoW9fp+WDxex6IaneejAUY71OZZATp2mOpAGGd9u/LB2IDwvSkUiTz
CUv1LOpKavA1OSPV+pZcrrkohq3MvCaNgPknrHEtsvzWcZcC1b77ldP/HuyOCfqsv5vFdGI3quFv
ASBwuUxyovxXb3ouebIPWx1+H1iFI/TvNq5lTn5QbP6LvfXMH+J9GLPJB4CMZ79QdMro/jOWvjQJ
/GZGeyYriY/5HCdTCk36HqQwZu/bDp+8jNGZH/zEciuBiXh1erFtBOAd9/LyJrneOAqqTkWrGUXV
I7BooQ/OkWOVNbAfeFu73pMfg6X8PSTMId4sKyig7hfhylSCL/FBKmPZXtA50j6tjgBc8A6SszHD
oJNt8ShEbIe5yG/Gc7OISIDrAS95NqScMKSGTdeuMCrGZb4837gDQk17DsTzx15mfIslWLTxB4Ei
KQhddV5p/BkRKkRikudTj4njy2m8A2F//o291k2Ek1p4bdaEVeVQmT5Z0JrD1SPkxun39pgI4lUH
1utXTp5GOpTmatEfu08TZMcABqtm2moZJBgCc1QFDlg0KRZ8WoJp344xpunRC/FrHq2RKIRk146/
mPl9YyeL3u8dIorBpbVVka43Ue9j2cJbFmMc+OLV5hodELDDNUXZ8xQ7m1IfH/+5pxQSd3upbxPR
QqdvwVfJp0ql0w3YQDUD2o5QNP4ueQfZ/oNpVOKsFz+sNrB/ttQf5SRPBwp3zyueyO/iSLL5/b6W
ws9UQbM7tvHAYrddf6hSPjTjE79B9+/9KKTjleE/KWE1XdN2iPFymlPHpOaO57s0ApMavW9bpp+5
WMn47SmVPpYvoWZdev5RTYWRNQ+5KaxPV+XVNdH+TPcMiaxwM4EGBhRa0kcK9vmKqVCu8cq33Il0
WGbBGJHQ9zZHO1Wz1zeaETeRXEeBcp1mmy+mbgX5QUgaytgYbUil9CesFFrdR2lopuimOkBHav0v
P4pABPPGSbCaf4AkMTVTmOXgESLFDAMSUitbcWpxUJMvUBBtuPU/uaDvy4jXxtSh6yMiz+8kRaHp
ohWF9cy0UdWrS6akaMlytU9Za94SViznma6wPF9SWvzn1OzkaGzDkht8LwepUZsuQhAno2rBMOF6
oKJLeVzOfQ2da0HVW7hEj6syxMdpP0u9+PBbd8KoCZDOk4BL7sQQTxi5eZcZRtqX71G+vNlkMF4G
wf+6EcVoNkSoHvzY8C0Mxr0sEAN6NOFjURZFG7iqt0M/3xn21mE9UTnzXCPcKRqyngohvverH+jV
3L7QroR33xSdrwbS9JW4BDkhHtHEveJ3foDkMJJ/4C86W+4DC5mVcNlXVFqcIWtn/f1D5XFUwKak
BcOuhdIIE3IfHGmd28LXG98nInfmz7pn44xBiPrS0Tr4VQDTGzpbs9h2UhYEPIuxDeJaiAf7wwvo
EAX/lBT+dSFaPE0lEcwvYjwntR8h0Q/ds0GV5VUyBoRkivsuD2+640xIFJEihGi7dvNtFEtVtVlJ
3WZIQziiAn83YLtkqAYggK3JLFKRhP3Th37NMcEFxBxljSyE4mcLat6dGdkhNHBSpTpT6PYxWesf
nLtrhus1u7sroqU1iiKnq6nZ1HZtXoCHjt4P/xAaOQYgDwO6TZ5BFawmieptxFaHdYUs0YE6TQea
xCLL/Z0Xs5oKZp2tNjfXwPoako3p2B1qrOLm3ezeGhdWhEkoRKxYudm7/0+ZmjY00jR7A+GxJAbX
ovdZ71jkajDSHVuVR9EzX3V66t3SLdW45fXwJoqtKAQSBgJ1CsTtBDdbAuibQo9utyWcgD/1S6hM
YRVNaouyJpiEbG8HunacoTUp7GD2vHU9teeRkhoVl0ugIBMnCma9Pg+oxG/a6WrxzlVGeXQK4dBd
cVrKOeO8Ey+aV2tqS6g3toaVskiJZz6UXu7Nj1DztCdEeE2UavO5nCkSvbj8OIcfbj/ViK8hmNoB
qcuI89yzToUb+LBxB0NEBA2Um0uY01GpBXcl0X1wiPstH1HqqMtlecP51Jn5wgVFRqSAZMN4k3Up
DnTdi1ix1TNvU0wpRaixCRZlrjr/d9TKWvTeCVIJ7h+Tyi0Y5U9E3rt77cxFxouqHogyVBa0t22N
/LK6ISAf7RO6tQnUMsxjvckxeGeVtuh7I0AjfzTslrADTJxvTcllLzJPR/Gl9n1EFvz0VWgml2LQ
PSZIZi94s9slmV956n1ClgnJmNThJAth+pHgu4nbah9OWORKcTVBPqUO1DkZ99WZlBN/oYPAueBn
XshZ5ZEKxET2Bi0zZXDKshCI/11vI0mTpZAyI7P3si/UIJHgdqD11/pt3bc4rfEFdMCNeFvQhBsE
/xfbhWYM36RZ8PRMXtXhJ6C/vLXbCIp4umCshBUktnmiAlPMH19ZOP+PhqO5pYx+BqsyHKq6kL+s
8tMLa9KvnuBhaenzbxqeJSvhffQSscR6tso3qDSQZ7wluRqPQhX0khE53iwsDUVcKNJUPc1y84L3
nC6pWa2liGYLwo8qk+E1xr6kaJATaogQgjqYBOVj9hhLTnwmewXadakoP/dbg5loWioBXdM5devo
gPEmFvGbEjBJ+MTujwGB+bKrE0GgUKjjQYNGp1g/vYtKW2igcsE+qSEHWrU6P3loz/3QebYiKgov
J+F4mFIaToRTZkm3zNRB3gvXFN9s9wRrsB4RRHwA+oTJzoP8S6zSk8PoPMZqrlTq59FfdeMU5nCO
f0aRHh+sledgUvnoT38wOvzdCOYBxhicZbeBOqmzg+EpA0Cm0qPg/2h9qDUiwKwEgG1F7CxvNwXA
OTju8JtdSogDYoXdmZFtPKSIU5LUjnaLUvsiQ4u87/kvS9nAJiyl1xj/9uKeekE2YanErx9ZBJTB
JIwEtQkA+MZOb57nnsLtKZjZx2lSsntqG5c3aKnT35MgSePu/n/FOgA64ir0RaPXfnowkN/Y0ygd
sB07wgaolAGJFj8XWNlerUjdLYbWF0RCQL6bn88w09GlEwXGDdo1FaHpmgWSLa4Lru3ASSm572od
emFLMOnjV5cRV6wn7h2B+IhXpvqq/kGk7zJdNDFUAHmI0WUJBVH2d1F0HRDDP5972fqqzRKqYkmA
jNn7uEjIF/vT5Co7x9CwRwtiVIpzaIGMTKGNVdiYRYRaNljZYDEqSTZ3je8srgH3iNQKXLmA8E+P
33tk0JaC+hHKdtBEA0SD2Dvuk1grs+SmorEO+93oOC4xaDPQTNq5vjTkLWv7qlnCGKEOobeygh4G
JfvmXkFOdOzF/Um5bTqXNlVK82bQDOuTZB08MGDvr9t4fcsE6x7U47sizt+KAspYI0bpzohMuCRL
5Rh1tp/e5b8lyZXDJVHIVw3+TxORcyTCtV9rTOCykmUPYO2cyqd4JkPRs4nppqJhzIaZmOM8Na3P
36rFDpErVartqdWJF3rnpMI63Kinvk2K82FN7dJPz8LvlQZ9VstspJn82DfxF7uISUOxfwOhltqY
1QNqp823FOS5AHFTCDQR8gQsFqFIqtlbEIJJ0zxrebjK6ui/JoJwZzNKqW39HM07mB0KsNsic4eC
+ixFdw8QBqQU1PEThWDNRokJ81HEjj2HDtqA2X+B6uv1XVh9p5bEpaJa/MUjXKfGNqFYOvD2Dy43
uojkM5XIzXMZZUtReJTGP5XGtwC/ifvvBzPVfP8cxBmF8gklQx4EoRqL6jbh88R5gwiObUPPRU94
/K/pgGOceZxVJlxR/qph8ISzGCd7G3GvnOjLDh00sjitP/Qnhln+lgwLU/AcUS/uowe+rpSsjcR8
jQUuGTlTJKKGTv3ITnW3evA288VcBUGHuv+OTPJz1UoH6czuq51UMfUc24+4PAt34nyFzpOMdGET
o/MtaFfcTsqocj5b9oMF+CiUj55Y2aq7noOGEVaNWZDQBKsPcjBOARZ12iex2xpvxiRDoG1S0JZO
fqxkcjef8bfY8GztJDtkV8HWKQB3TtF4F8pQ+TOWQNSWw6ZmWI+2vggTGrEPmjQzn0ZXqhdjlt6n
xeJrdtpHC9cF4QiiY/7kuEr2Rl71xMOT9/dcM+a35IihEijbT/KiYt+m4Cay9JOwK5V12/ginVaS
b282YPO02mQ68q9JqzzCtSuYL7f0WFlD91WJhUu0T+V1bvCewkqTyJJdsmlpAWv4pzPenz2qWZq6
jSJ3qFnNzXul2LqimG3k8KrJ/iMYnhl+6/WLYmMvVgqYP7YMshWyAaWiP4cXHjkIjGu3nPg7j+QZ
YCLPwwDaUDB0jSegWT3fKAOmZQ3nTZ376E9hvExSlcyO3JUuI0AvGwCO2bU9YiMS20dxUSbtSoo8
NRihefj2tjTtEQykD5wULZVLEzAyPwHHRYj2097NYyPV/70n9EdmVDvqyuhcvMPwVi77qo4jI59N
e5ez9WDEsbG376s54XiHBNG1KsfBt7ok5+1U2k/tsDJq0c8GriRzc+UCI/bAgeJDQPZJKMZN84xt
gNCw8m/8hfwoCzKvhCPYl+c0vBnknbcGSOv4HHH57qLWzmr4CHvh5iTgxMaBwZXLRJqyId2i7nhH
fPEDrS/66RVh6DRlzxXOs+t1j/CJgsH+ByRV4jhtXm92JW8AcF2rBzR4pLwKmbOBx4liR6e/Iz+6
vNly6H7yScSUuT7pYS8x8mQ2qvSNvZo9A7vPd8Mhra8SregALjy0o97aJEeDM64FGw3P1FsC3aWb
rjtfF83SOtHTPiAhgupfQGSZmK6pjFhnoKqApskTml+mGB6G+HXsKnJukOl6AGZCrYSApyY4Qf3s
woKyL4sS8Y4tSL1z85KoFk37jGiUbObuGafDLVk5H7Y/sRryzIknVEOqfvZIeEI3+0j2tramxb2p
B5HfjNVofiRq4QRegUubLHn3j3uswXPFrFGwhiuG81/S/81/UaQmBc0WUnWy59KQGK4pcv/QQmPT
+gDluRtlRHWjFJ0e4b4NX2uUvPT55NqcDJ4Iymbi0IsKiVwuKwEbo6k19vKkmmP0HMoeCyC/de1U
MQvM7qswzCQQhFRctCbkyXHnjRBUdMVQbe6hGWuQfsHnjab16M7sUpC7A9uZoV+OXVtMSdpQz95c
wfr3FT3tKJAfbK/SveowwTCeOkwsbbLKR0cPDNmH+SOrNiUsvjm8tI2y67ykkTsed6BDLD/vAMWD
qvFhkVYkKykLYFS0U+oZG2VCH4qSblJ0Q+lcZZm/BaXTIqG7we6dDK6TFIJCDdZGCbiUKcHPGVJ1
JhMcxwH8Q0Yo5K2dly/DM1N9uV9fsHY3DiR1KzExyTLSF35g+0UZT8Hi69JceR3rnJh81hVt6qrZ
w60k7xFXk/YxRYWl9B3V9e08ZPhTPWfW0yU00Tpi+Z9nTVTkpab5vBLcnQheDwbFdbAchlkTCt7C
IEu5JqFpIs3d2EcDlvcKb266abI5scEXQxuwbX6/RUyr5c40fjKIQFSpzrRbzOUjcTaHygSDwEO9
Q2gmCteqbYf9/ousNoAQL6m8R5u5c6JWIqAOD/P5bg5AtltU1nS5+O/y4PNZh12X4qRt5lJ4Diai
ekgfI2k12sMCnT/h7n14COhLxBTKcf+DUED1HfPdEjUR/H0oZ0c9ERzYGXDttwa3WlFHfVb4NLHh
jpuYdeSE6cP8/G0nzJ2X9QoWNAWP/129ZqOSY4VbOUYA6Shje2njgG0PejuO6XuSXldAg3bxRwKE
I9suc+yKi98HdjCidaAQQbFmO3DGnYGh4rvroMExlI7RhHS8Zzb1R9Ox4cpiJbkj9p/kmiaSRrHW
ggdwCfTNX0aj1gt54jGEJJP/tO8DriHdsNCkrUeStuCYRM0PD2ABW+gH0jAubIah8xu4a37VNiOa
VIquDdEsKazQkaPXyJIc27jSayTrqeNr/Xq4bzkIhU+6CJXHlEwrTWaAdG9S6ln++qixmozWgxp/
UYjPcrNhSP31i2kKE5zc5UufpDadRALlhK8Nb2SX7J7MX4mtB3ze87TsZxaNXP14jlvmSjJ04ea5
OwIkLLyD35mBNTlQsvHnt6NcDbSXW3wM0E0T/VQwkGWTCdVMUApoA6K2PZD8cE7CsxhPLtVR5MSH
xg1U+IvmWW6Jn8ZB/CPDX2dTPKVDXYsjNutwlofWv5QoK7h9ir1lw1bkQcOWbruaPPgZdBeheRov
WPMjuHh+ni40B3DlLBhMW9ORCcLYL4x1kDrz7Bj4Vd7nzkWHOhxe4a2JfaSkTKI6XqEzQS1MaZiH
DZ60qP+B2On6TNJWBkhlV+cVKRocL4bT+ekeX4Dw1TzyF2v+DQ7Xb83j9P7Lg5vtXcwK2Dn9rGiI
wrqhvESxJ1B7lJEJ/VCDYeJi/6C8WJtj9pS8COg//aSLvxordBaDG4zDounwrN31oU0kgnxzkxWb
OvD9esXeaBIkgDYOxuHdBKqjDLP0C+h9fNaQbxWRKi7cxQXIREUSxk4fh0TjsdeMsadZkpsrhwvE
lBQPZOy4p/m+nVhfhqrTFM0/WroVyybKBEEDHpAUnwADL4TbnvHLkSY+/bXP6SJDb8T+IDbI7r6L
vQOYf/aPNMYODbThf291FkUZcAtTGTCDyWXmBgosSrWnWQBWM8Z6J/NOxDUEolROhhbK2o+5qGin
FzTAf3bexaN+pTgfLCDXyrO1CDDSwwsUmqFh9nyz4aeDim9o/Jr/ZBSJCL7FxepZu5b6PLHNrxZX
3gh/cWjejtzjwUVHzPeIN+m2rnB3/xJjmYzALdE1aohqXLUSoLHMulYd4NFSoJQn96lJCyzHH+rg
P3om+DLf0CJoWntSGH7pxoA4yjG6r+X89ppmfzwKmv0a/IoKaqm4Dvxkf1uOnlZhbAy2XbOlGCQL
K4BGdZKe6s6zQFXzOs47vl0jio2K+MjNyDzhnfV7+j4LK1NGpC62h5zJ59yd1TVnCaJJT31jsqqx
sas9hV3g3kjd9IQmSBhI1lTR2zaMXcDz2ptH/FaoTU1C2xJ+BZTnxC2+F/SymY0r9Uv7nYCUIhON
l2ewkRhUH8XePZTvJcpcaH9Vg17HpzzEYDJczlir6UMLK+82yWsBxlEoJD5zfxKY7PP/iJJj44Bx
nfpOsiChoBAj7wQ6thq3xHLDXrxqmEJxpyBPW+IOvUrcrHgvxTTS8VxWVoNUeJPc0adqYwlMUtd0
C0pTQVV+kEtVMAR7E/tL5BDEgkqLjEOUEtqbiGqedpTr7UWIzpZe9vDFStLZMYI8N2VgI9y/gvl6
T1DyrjgY9Xr4BROlYEMfxiZ2CGMq2IWrfjF/cji4Rbc98LsTCR79DSIYTOrKW1fE2B6JSFbQZAoc
q32XWg9JFFiBSb6b4DskZ5fQe4WSGmwnbTwMtO681/sZWhObY3DjrzQmaIMT13DLd5GN/gYFrYkg
iFN8DQqPHG0EukprWUHETOuDW9cTNphkEkzgDsBFPgdmEPvK3+QDnGkQS4uLwcKsqWyXnjRSYt8N
KNg6/byCLK70cnDHZfX87QPK5Wv27KNLk2Um4zok1hlrIKdzgC4Gj8tgrHsui0AXwW9Ggx4ikMng
ZqEPf5KEFIVmBowQQsfocYDLr6AybmpU0UghaT+3JFDRyHSeqoOOLsmmzLSmuNCXf9zPvfl34Xcq
4pI/DhYULVdnGDT4H/TXYCzN3j6qdx3t8fU4Q0AuIpHwRB54aWsxa8426Rkovpr51pV7clEbch4U
8l1xtu8I5Av8NoCGoiMWmH6Edut7Fju5AeeqILPWps0D6uKR53/6pzYKdc8hX15giJcoG+9UcUO1
K8MNzIpnRhulzQSuS4aMbLzJR9IiFsr9tCeWklLUSSngczHD49IFZjU4QESGNi8dEA9QHiQ046EY
qVicXM1E11ofGB7sm2u4aDw7WuiOydXC4sDL37Pbpo79OrWN0IcYIJpfIVnMNe9J1y+mQzWwVSAi
IhThr8mCy5maqx/OTRZdqPUhU3ZM+fsGw+v/uAGU4JypPBFLgfxnv6FzPMQrxl+s1d2z2hs9kD5z
qeTyhhf9kx6IuDWSAHVcnF6wzpA+LGeWHTpaSdYr6H/GyJLNAsDbSlzm/TGeJc9rfrXxtmwF36Ib
Y3nN7AJhnaOOLNXyFDKvno1t+gJcYZRS3ymsb4TgO7DHgcpeNREt7rnztgKz03EWEemkOyEmD0jB
8YSBwiWh6wCWJbWUKA48/DkeqaX2cHpdwpzdI3w27LciE4EAKyEzlm68rUSE41N9i7gVKx0ZrTH1
jjSr8dMNecGAKSP5zpCfbO1DrsIGsdWD5ZQ5Z73eOZ8l9VfS1ttY8NI4DbxHzqpWzV+Q7+TD9ObZ
6vhoDV+Wlv4YwiVjUSbqr4jg3wJIj9/TrUIE+Bd4jwFzrBi6dCMXqTRzj8cnNtToDHP3ZtGYmpba
oi0S/IO////vGdKhKTvMjb/H1Hyuk3Vdg/31ilplOErKGPNUQltaHq4erIKFfZQmyVbDV1Lw3t25
BYwP/fYh3RAYbQL3tgze6v3gqv6Xnsadc7+wVKAp+TQaY76l/cQ+LUB8CZ1mdyEi0lp30xoiRQUN
onMyT/PC1BiUacis3NZ/llCLkLlbvosM/4AvntVEnVFW2dk8SueI+3E7X+pbSm5TwQ5SR1utarej
DTP4sHUXhO6vlpKovYZ93exS0a3TuRM4Bb41oc+2k14JwVHfGmRvJ0ek5FAl0MOeKKnIMHh01BFo
eU7z+eM91ecyAtggw3PZp+er48/dgLj0Au9ukht8DDqrEjk1RgPyYbjZXJQENeOrkZE9z4495KZ2
JLMO59L7QfVP4Xt1OtTn9mS1RGkis65cR+St3FvobJrMKBrONj8c+Yr8NVluszCKt4Q7TcTD4Zju
+xNFSFeqgQPUN3K/yfCejkTA1uWBnDADOzKlq+56QMHhL85el54OOCO7sZG72HUEAuU8kjnwMaPy
NLnb9dXlyYuYNxAF5gObLbOE4Ei+kqT2nxR0GpOOnveSGcwxiENmvPfUuOm7A6AngysMwhFOsUr8
YEuUlnj1BOGCu5YjgHFH0hMpQlSBeGVRcV2W5SbgvsmFvHLula2FiipYMrc1jJBsdmtSb/HFvh6g
S3LXBOdm3phRhoHq18GAMOd7FJg89BvEj/p6qyKRu+7pdQquF5XK9WUC6Wv584v3amic3R7zQwCT
EEMwlSH/3JLhq0JnQ1cMY+F42lhpeTHxO+XRA1AMge9eFNRo4Vt/KTb/K1Bu76aEGHhomCru7Iys
y9z55vLIhvlc8c8sz9ZYI3MUmYrIzAAeCH4HNxy/Siq2VO96x0eZcZuqiW1tfjaaSszZVpPSKLG1
Wy+ZkI2oXZi6r9YjCV54D7ATOH0Aq1qJLrBpNEjSrRi2DsXIoGlGdMaY2stB1aLWg/CUnzjEqz++
tsJVnbmGG3NkOBX4cSZmQ6Rx/TvBPcF950/R5e1zC3YakZLumqvC+WfvbJIsWeg3/4Qm27HBic0U
MeJUp8lBSwTwQf/gVUXC5dI7mD2DowVJRvz7dozImC7DbmHPq4DBKGoOBxciURT6hZjYri7uNT3B
ciE+YfntMxo/4bOjSJIynLn/rinZBEqpXzWU2W/4HP6ld1SWGIZN5Mxp6qWCSjrb2f0CrxeGf4mF
3zsR/ALDe8R/RK0QLlB4jcWJlM5HXbsM6ukIaa1v1GBzWQfaP2fWUwtqG6RH6TF4jHqIgnveX1j6
n86tMhMoL3Wg3yB0MBgdJz08jq8JIQnBbfW3kScYgg2AJf5Kwn9xuHWxSP+qdLPjQemDxeBIro1H
BRvobr5DRchXse/i0aU7236XJpfEb3MiFORwJFem+5CF5YD9ILlo1o4u2L9K3/iOYhF8DoUyYOdP
F1ZJpHO+qoUBQ8dc/eoGhh3GEr7tp9kYyG49Q+wW/IugWJx3uLcD2nS5yuq2dInayntTltxn8SJN
WXIPwjz2LCTIPhsYT/Egf09Mazt7zq6vOLsWudD6OYJ7XZzcmMozOiBSpT1BnFI3Y+H2deGePh6s
4e6q+xS0HrcIV3x2ZixRKcCI12ZINMAmp9QxCRb+vJYxVzhpwr7zC453XO2gtqvgc4D3vtxdJpMX
HmctXLR6/Hz3GEszN/AghrtrytH8jdC3E85N4DSqfXQAtSR2wIip5x7nQl+peHO1KHZ7I29GwT0U
6oyETKWn97iPf3XNjJ+PVSQ0D9Y1PmcGrwwp+0sFZe2O8wuQhwnMfJrA8+UeDBYqu29P3dIJfVEz
glChUit6g6juZfwzJhQqkf4fXcTH/IF44aZepSkdQZU8QdmRsWSfFwbjWvTbRqjvM/OyJTKRvnFh
tL5QnLaXxUIQ2EAWYWsTnz7SAhWFkQnS3+nwPYgRsfM3hrDRtUx4UR8QdtvpioZiSosi+qyKWv/b
ZDdsTDQiMb3/FAT4LZpNQ1BsPdWp1XavuMHPLvNg4Z+LIRtIhrhPcLTrvxyQwrv1sczJxwiLyoVL
6eGoMzBO2paJ+gR7X+VzPQxEVDsZDyxpGs2ScY1jHvEvsNd0snP2ok16D1FoiczdT507ah2KwQJr
5X4p5p0GVUoaGbys3gEtbIBWKj7nVkys6siNLDJVYPTQ+eGzV9OYISMsL/GNLkUmrShLjLjw0fuI
eoPdocOlUEnm1hdDbWSIj0kvesa/lOINSx33JX3AOdj/sq/LSWF84rAzxi02r9BjdTAd/C3d4aUd
PtENTIKpsMl8jkP4LqPZ4MFcwGSdyEa2mQNf576oIspddis9CkTpfJUXU8ar15RBYKa2Gb9wRaou
qqRexnKR+NL65mcysTPr+Q71zBugw49pZTo5hkHT0/UuVkaS0z9GRNHhXgXwWG1d7S+DV59KmSuk
Q872WpGqseIzkbCepXlnTLUDn1gAXT6U/1+XQx1zsa8HbIS9i0lvVqv7GTRDWxSWfgTdluBgzVDi
wGYPILmY1KQMXwLrlW6Tq8jAsWrPaL/UOB6IrqTYmw1Bc4WagrQ8buIXxV9k+kNQ0VD234fp6y6p
Gja3gqKwe0wGTxOXYQFMdYCfirPafN+gqwhoEoJPXb4cuYwULbj2ZkMHYqve32e6yxQBJVU67qiW
lNkFChqrfNDVTHqbRk3Scmmkx72MAJi0ZsHVZcFiPMdo7sDgUJ4xnVij2dBd+TPS2YAyDrDXZO/n
5YG88pGYf/R5FAKovZn4rxAx8MCyAHzhpa+zU7IHxSb1/M11YBkzsVljGVa5Btv9qwgCHdhN7LaH
Ow4ZzH5w9gcufPDJPHdu9lRyVBX4R2iTNzJpAoAsif2UReQT9FivFK8+MjdlrJi3M0H54IGkOySx
hbQzjTsHV9KHmmgXRRi4jZIisMo/9ol1hcuKfm6sorEI/hq4rO0T49u2i3ZIjIT/B8Und1VS52dg
b8M6Ws3iPG6waMd1+cYKhHJCV7a95oCwV8dIriQnQTiUUy/CRuLbhiMTSZeXWMdQuFVdV0YvURUU
ybmhbnoFF71TK6YtrEVg15kAD8CQ9eZNXZzvo1CeY/S+wgW1Og1m4ZBs1JXpSRWX8MEfwdaj/CGF
JqSFThaIyLT48WWIrmBQdYWb2A2wDCSsKyPo/a/B3R7NVxLcGRAwuE0L2tOd4TXegkr+3vAYy3mz
HlJI2txzIvoBMm9tOq1biEWz+5oKxQxgErlV/7aXcjhcnddDSnsQNsd7R0+73jGzLHFudKjcjtu0
dDJx7Tcj6JDClSCSebKIBNltZ8nclN970ArmXcPjsVUqMC+nHHAYv+iaAUK+L3rEdIQqz75dSdcV
NuzLuIXnmEhLHNlxESKGDPy14jsNSFgRmaOvOsQc6cYZ345O8uvQNmwj0ZpPO2DmZnUdkP+IUMGD
6cIL3ZZQ9JsqgnY4QIY3xfuFDz/la0EdlA6G25nvMiT+ss2Lz2K0Cz7+W+GDbbXvRtyRHYCHup/c
ysVWaMokJWS1jvV6BIV/v4SgWy0DmmsbRP8SdEn959sZn2RZ6fqTSp8uPI1LwtnTPTZyKqT73xGW
4tlXWCrg/SH0NabZXtj6FO1VyzgSRXEZbdDv0m0rrVws6UynPmzSiMAIhB7mubfzhLv1RHxx3n7S
oi3RbAoBgsECW24rfRpsVm/SbRB2w2KgENu7R34qgiCTSilMhxIzPke1VapmcEs4pcl6FJUDHE3e
fV2vibZ63ah7EoPzy/CGddZ6uSY+r7h6FJlW9Tq2OMSBpM9f9Z/ATCP70baagdjBuQXvAS2JJ3N+
IZd8zDgDW/w86T4OOdQe8N0UnPRB5H5B59DKfGzUvp3QhxtpL4+IDII8k8YM9ixeFxI2m5e2qpal
3mYozoG4RRRG5ynE03GJJw0Vc64hfkCFkyT2v8drPomk9rsxM1dDgSuy+F48RlQ3GdM6Z8n8Djjh
l+IE3MzPL/iYqNj71CS3jmEGTvoyqErAIFiVxxVo1bXE5UOdO2giC79fbZVfuiHJ607gJiO9Vc3J
RRJC3wdn0y8vsJuqB50sJJKFSPlbvQ51/T5y8O7qxIiQyMgcVroKwxo2EtD39jv0nCfxpbdCv+VO
wVJjCCBlprbT0yiubCA9+jN7S51qPnNAf4zPNghNH4lbMiVt6ZgqJI2lsaH5GrQT7jWjkIpUqk9j
5iupPIDHFe20hrxc8GI9poUShkeujvwzF5kwKQu6Ugnt07Ozm6HvPPur+CjXQ7eN5gzJwqfHWXhv
eOMhGH24CiOfBpXw3NbbACP7O5s5WvujetDze0LQoLCKiJF8Aw7PlmvjRJR21QYbtDqIE2607pIk
B8H2zE1klI/GSrlAVz9+XArl+fCDQ8Zrg941o1kpseZFoXalmDiktSIjRciX0YR0z/NIpeqE/ayh
Nfwd1Ur20TFquND/bx8dYDadvumzyZsmo4XjczUXMBeihUvtFI9vlUfXQ15uv8c810CWlBRJ8uB4
hKBrKveheEz+02PZ8pAXA4bslhsKx3zhLVTNqIJTolHPeCEZfDJVBdRmf2AiqpRMaKXa2XoW04rw
asu+LXGgH7ieho9p5CB9CNSdfn95Er9SmwwATiM/kXe5VDyS+ZkWdeNy5RqKwWX579dunzOf2WgP
ItxT1KgHHNCoDtSLqAxKsqCWoZj0A643fbPVk/4K9ixm32inlyorrp1h87m6mwfmPm3Pj8ib2RmC
mW2/ht4lqnTWD06WurrpigbgRC5QCWDpsp3nuAxVwX3P1nrO27kKbTleQRj18vlQ8MpGqHyowITc
0dQ79DWQHsQ/NZ4N86WlTxL/P69yUULZQDoIC3htKSq/1HYhuCc3Vvw5PYsy9G4tSnEzT2ZiwY7z
2ER/K0VpwaEDsYbAdVFZV8n8RdwiNSmlO0gCG4EjiQySI6dzJFJSeG5Vi67IbDNqPSkp86EhHjib
PJAYAeNrYjqAk4RhnsvC4wHELKILNRQJZtrk1NPoZ4nYUV6sIXBnqkqBWGkxY+rsXX/0ak49J3Ny
7nS4yxcDfAZlZBI3ne30QfLFncwqKKXf3liVeZmjgq/82VafT8rF2xh7CTHOxUOdnA3vPE2tpKeT
IY/0WOlPYUFvoYhmEXDzEZE7lJsSpVmTcTZdJwlBOp+d3ZiU+P8e5C9x8UmECekw+7H7mITH26ff
w6E8AiYf6pXpSPKHD05+ESpAMEUBftHoWLhJ9jnRt90yZJSqOM27d1jutx2iZ270YCt1dCMt+tLL
4DEdBX+tSqqP8n89r0WJfqKZcstQngS4NrwPrSxnAh4eJtxcGkGEN9nPcGWY6ScjRADvSoGTdD1p
lMK0X977nsobAnR8gCcCF1+KM77cFlxVUBbZaN3ntSmczQNlb0MST6s4OhWMfE13UAd/dPFHwSGP
+7aedUMq0NbiidGg/+KrMVBYn+Ntw0qm5hwgjqN4kTO8D4ZuhAsG8ARtyP2CyCcIwNT+IS0naiWj
jnVbw3G86G68Yar6NZpAvI2wk0wo4Krqa3PLl2bX7VKA/wYmlR3tgXRSftCoiiFdXxalXrhEIu06
aiGhNpCzwGbrWoByVIBBjGsBUnYlhhdRK+NFGUTodZFOkFCcy9zHeVfXQIca+RXbxeBXxc5t6iiT
irFkkj00ZGfl7GVMbhhvPKC5SRW1ooU42F4Fn2xwHwMR5fLowK7bKzBiB7d3kUTgazmRSvn3zevT
H4TUyQQaNivI/5g7BrhiIc67PTZKIOlxppvNK6VjmVBKHjrQtSOxqsmyD0HD5qCKaPNFf0qCgSI2
EhFuN4Rsx0cb/EWyTYzSmT7LdY6CSh+DiQoo2EbF3E8pNO33lH5rpEjvtlPDdrrEdmLI1hF5fPRV
MBfpX2bj+tblAiQE0wQA3HvBB0fe71Rq4ZgrMKbtPh5Fqh6tEI6y+/twcwu4wENqmVgDE+EqcUlS
abZH96PBPH11UQDH5qDm04VWP2Kyx9EY8a7Woy2btoT/zPSwiNcWnDE8J4IQ2p8nxeoWUmuixTPO
PbAzn+ZPGP/muqyZgJwxBLscM7RgpZsHQS4ADxq2NBGSu2pwE7Yx9FByQ0IhK2WaODyEv5InW4Qk
GCZXMn8kKl/fuMmScWcJ2OpwVsEWv3pFwAdV2MrfaHIAW1vvfRmKsUMA5w5j6t+SlBu0oe3k0BEJ
FYHirvDJ9mPsLvzO8mZHUWdkETSs/AgXnLPjmrxtDfIr+CxxFUpNMgT7qM/XNMuuoXneUldYO1x+
+8247EdOqfG6GybfGUPDy9F3F8aRKtEdRjQ5GqsUEM2SEHyXNsFF023Brvc83/cvd0pyJNWA12VC
W9/3kVlXHvZ2Rfo0rm0jxBf2NxDKgb21xFb51T+IehNu7UcMqyacxndqqlMSr+pQjC59CxyGLbl6
WZgtjPT2c66pcerbakRXzqmxll3x853vzh0K3OuuNkPfTK3txxr/v/z7hmXFhnnqWp0SLQE3v8Ke
5BddnfP7FT5h4NhCfnpmrMje3UN++HM6e1Upng4XWJkAosdGeKxzORXXHDOxzW3rs7PfAgup6PNc
ySF14C/PJf7LvFr5tNqGUcaDNOJ0Wivd7aVWmdqdpp6+t5+eGjaC1Rtoln8abha4htReO8LcxfoP
ypVpdAgFv4pM3wiG3UYs/0v7G2NzgG2e3VCcuwG5JG2DTXtuPVKSySgvC03FvA+KeqPZoseh0mfT
+hKW/KV9dLw6+4wkbG2//OeCbznaG3gsjZxPg1UsFhdoC6YiJJjTTkb4epDyUx6k9Rgo70ru/7aJ
jVayRjUAxBqoiXl9FR6TdRazMRKUxBSgj7y9ffk0q6YvP25U52Ja8LOp/uwK9jf6AaOzdaXXP5n6
UvPkoHM+zhWvs1jHWenKyFKM4LvbobMc2rBaCJOPvFcTWZB3+9o0ktH5tt6LfQXkIMPwfRT89P98
TnXZN/3MiU7uPH0fJwHKabCCHUimc5RqTyk8XuwCx+CmYeuVZ+dLPRQtV1fLErd8uD4rFMh8cYHL
uiAI74vLJTFtwBJ7MgakNTdxZDJQ1ykuf/YnocjGeu9kjOOZIqgXlk1nzEkg7VgvXdDFYs8Mcd8h
3CBGj9ZA1BScp93YlfSNg4LIGK5/P5LBPvNt4W22BvaOKZJXaZb1Y9bqAbXC09EDnjT2et6Mb6+Z
aQJurepdRYh/XBqVNgKkK7xTW8JT4jKfNY3IklIvwC8d9DriNzlP0Z4d6PDx2U6jihgQ9hFD9pz1
lVjcVT2Wrxx2l2mD8XvQOVHgL3hnoB9OXybyrIGmMlwCR9Vh0fUMK56oZjEaO3AKdpGCQWvl/ii2
bvxiFZrcaCeuXCi7xxtBqhCE4m8wsEEYdoAjP/A9UP53ADfko7z5/pA5VXYgtTiX6lKKUenRHOJI
+f6vBT5CML0gop6HtqUfkw7EA+D/KaaLruIlUnNJOYblLQEmFmpEdNtDDmbxooiklReSf04rdES1
xtnUpvYbf91J+EJplfaddcpzphelZu1tCT83mWVmvdAJ+RreMl0jQvKXoGCR2HzCr6Vhp/zQpV30
aFJ9wacQiSwySXk8UJq37r83CLF7p7+NuQp8NuamOTODCLadOLki/SmDDMTOMyoSWKyAJgFAUibC
9SRO4cbf01GIl6WsuW2y0sewHqNydJSJDF3Kwx9n2+Si1saeqCGEzYhPkxE3SxBpcSxxiNm6JLOp
nOk+B5nl2Spq+B/YEME3NX9gACXX/ze93BA+i4lj6rp5t+huAQWoxFbShBYT12QpJ6mSq0XjQ/HE
rkTIAHxf13UBBF3Wue5OTaz3SY0zi8tJUy3VwNlIi5C8zbX9B63k1y+E+tGb7qeSu1w4WlyWREPI
+Y7y7rT0M3gqherQRhXN2K8Xd9N6v/5Z0lz72GD0feEbnNjgsd6H3FROqO0oj4auFNG5InLrdCOx
nYXA4yYLew2R4n7AnvDa9m+RaNnowUTyoPFGsWIODB2N4lKGHvsqzU4T4Au9Gd9rM1sg59AtYcWW
JINm+YMZGZuN/hAoauQ1DGVQLBCnwsNRkupxjNra3zUv9oyHrETwgHHdRSv5TT2EPPJWgTzleFhN
8VDha8J8uBbGB2xJsM8hB9OkTGX0ga1NN3L51aITKJ99Fkd041yE+J4KzXD9qO6Y5a28mp+2njbv
PNk0YjDx1dr0Sxt+7QvUtmRTDXA6EvQcIGabqFEjkBGBhDZuO+lhwISmC8h4GD6N24+kLaKxl+ok
byH6saKiyv90xwxztxm/MwZIfVBgCCmV0JDYoiqaiX8gqm0wk+Eh4iHSY8zDDO+nS/2XMayipUBG
zJ85WzOQCEbKHOmqyq3oO0PmYzYAPHZpisA2NXm+QJTw22f1QidgHSgjuBpIVfq1kGitwW4G0ARW
mzfXFS8i/OYy5AqqLr9F0V/txQeyhNvIP5ZaW3rZ7aJjIAi3xV3t/CVNkw/CHXs1bZk/zyMdfJYz
ScsY27vyGaG80wGrueXfLSBPg5U/Hei1iFZ/9lF8/jZpDBTjSf0+T6GS/kQ81KMSuf8KRxZx2ELy
ZEXEfdfYHklSOM1yCRCUJ8wiGkm9+2HhJNgReoQIwIx+4/vaLjjdbEJBBWGZ0WIBhE1VrGXRGHIf
0t0ODmuY+TeEJpZi2XFzx5lfp/+/PcW242D0/eh0GT9reiheM29ekf1R3Z5MDAWj1ugyoE7KDox2
Q75R7SfETQl8aukzmzkDwvkgTFgIlooFplz/wwZg+/cWeUIMXzJ1eYLXYn59yYiBfceKsO5gr2Wh
QzJ53SaViP5J1/Ni1dZQx2Y6GaOmYOFgthoTqqq8P0t8OfxpZXmRu2I+eIwCWQBoAB7AawAiTUxS
Y1b08DySDsazqJzKdpoa6KHKXvsBzcOA+SFCNv4a+M2cFj1OrNGsR434HhzTCh8knKDniQWdxoxv
V0e3eVLpZ+UtXb8FlXr+SjEXEWV5Wi42lsSggGNh39QhAMR6RHAKyJrtqLBvmcuqFTKZluZHW7cg
q0pCBD655uHSWN+X3s1FBDg6gT7l+rZbD89F9pMRYNfx6/wpim+T7ER1Q9IIqILzJLuOBRBf+wwf
rO+aMMVK6sN6CSoe08+SIIZ6t2IwLTEwgIqsDs2xoCZav+5hS0RKBKV3R1KaR/+9ec4g6afhsNOa
OqwAmhhhyohGCT9NdYbNYVSvyC1QU10vGenmxPG52+2p5xGu2CNGupyzyvylM2uqnGDINXenG3N8
ynQWnICN+IZEtSe+0PJS+sy5m6khiwo+cnTz/MZIOyyFnz6yeM/yQB/ZRJmH6hiFzfYUg558hj6z
lnGgasQHiuLYhTlGqVG2lTO18xp8OVTsVmtKJ7L5qWH0/NPQem0xg8meBLj6B05mOSERJsdsXT5s
Ka1l9kPc7H+6KIkD3DJNkzJIwwiVGxJSrCuXCbT+PQ8fO0NdD+V3/rNDDC2tN9wzhzBDplpDykr2
LTkLxTz3c/yw4BTwoPjqoyRJf6gKVSDcdBB7aHDwJMHuggP4apHF/hYK9p+Ar25K3hTqKCRONGqz
nvsQ7pGYjySV90rJuEfOeO4Bt5ZR/LFTMW/oFpjsk6Jdguh4xHgMwPJZRGU3+BlOX8J3oQkrKWOr
WSJ9C1p5CHFcXNDWLDFsjFo8TdHgzdXPhoTPB9/ZEf+ZNI2vC4eWYsOkT/s5lXp5uq0kkvnrCg64
kdieJde22JXMMdy9x5nrVurTk9wNknwdPJUR7yocEYEJeutbs8WOr+jp8YnsMrpEE6tQRnBoFoX5
HZRRw1GX4PVeNRYgEVl6LzlSByGBSvMdXw12kWNeeHydxa2t4mtSn+XS3eu6ZOknWgVWsMHD/Sal
/taT1ycSSM30Mj4teIBJLxaraGixQqQfGdqD0xNgKonlWCkYRBpyo1fKpKuFWVVlFq1dFYhaaZXp
gMrXdnzxgsuyenOp2RNM3vyKFudBKUD0C9ihk32CPFwW7R/oK82fVtMzPyLeOfy24mqyCj/A3RHi
7Zy+d46aukDyKCxA1Pj7X1ICZX89BsjHIVrA95h1976FBJyG3GJQubxxDNylXNxAkBVhT6HSj6EN
4mukmNsvFhzcC7LCe5m9RgN4fWugpLkkFBQG3lilJLphrlLC877rE4zR2R3nXpkFVGTNmLdARea3
6MjK55JqDsiAEt5BvWqC5pxIyOPlXkbRIBFrfe+yjfhD/A23FJsNSjF6Ty8JNok/L5Cex97bFd+T
zAfxmktGwPTHs+a5gWW4L2hya7H57PMEgWBRIF4nSzG3WuhF2K4I1x+hoFTOOy59OWvhgrU3IGMk
K6WXsXCBmUhKfAKs3i34eM2zeMb0K2ik4J8mg3LFWA3iH9kdJUdXoCFi+r3p3fSzJ0Uu2jvan8gd
8NkvtZyV6d9nrTVit1U30KnyMxmpNmaMbgfxuDK7dlY1BQlK1nz4exNrX3zjl3TNswaSGLQ0huTo
XoItNFQ2VKrN5MeB6HsUec/+iAPP9jFTQ8rH7kqpRRSdMueIsMvEcIg5oqLao5lILsafJINHk4Wq
MTtxpvEdfL+EbbiQ6OV3S64Q9ZenOiTkEqq09DDJ+aZMVwFa4gis6SbssjsHXPwtWGiP40Mo9Dg5
T9E51IFDVl6A2Hsvsd6s6yXbZ2+R+QP7o+wsuM6Gp/5OnSiFidvoYMqHG0ClKp4bgysD1Ga44ckx
Ht4VXq5rOGHdB6OvzsoC/BlHJ/aVWL1MI0mybHPL0VMrC9YzAUpRG3F6dL08GDkewPTlJWoyH4pm
LebOumD1ryuH27ZG151eSckWcsJ0A7d8+KxbzBmHR0UKrYnTMINFyqeJjdRBoStz0c7gul/MAJWc
4DnefnVpMaSr+GLv9nlwVMkUZCgEAMx094bgzj3W91n9xVj/KJk8TvTmDmWcGCMlOMkzlXergmHP
v2fAwPFQ/PE4KWxgTLjbM7Wc2xMFc2z0jIDcnzMzFKjt5sHRVmTc+SbywpvUlyp8JKmKdfK1zcqf
/1B1GSnrti4aTfRAne2hzbxhxLix6OhLHaOgj7v8XA4yNEFNmEEPnqWIswdKgm0HFXJpmZ9aIZC6
oxv9c475gfL03JF9/yf01S5fqqsttRm/emDe3k4SAE1Sit2b6QXjN6phGlnuZTGpsMkDQ7FMP7Md
ZtmuH29U3r1KgL8gkn/DUHIZR1buu+eUUTgugIOvSmhmtzY8ANWAUsW7Um9dGJ/iYwyoP3TlMPes
dW0ldQ05Rusxa6AbwMQTdQmjiJw2NYtLUqcq5v9NGXJV5NcIXyonbwtcSLtJlYzmqnIZqdcvVsyf
9MzjDZeRNM2rd+yKyeRsmwMGN1Z11PqLgGCO6W1ijohzP40+ZAb2o1NoTlcVOvGMuqStKL5adrQs
1gUXfjuh2VgtO1YKeaNLXWBMNkMzkN58u0hn2jstChRD5h4AYq+dP+/WssAUsV1j7E5y71ocd/yf
lpi7u8qGbSFbpjP1bhpSopZD5tM+1j9Y4njV/HRbKuqqAqySHBxi8op1QpDNEVkH2Ndjrgh9syOC
R5xMgu1561i/RSSNBhqCp278fTYfsu0RMh6zf70LVmByrHjKfWvpAoRCuMzOnYxdruyzV6LeOwHy
i2s6pexKyuHqQsMUSbLS+tAMW6UGuaKqj0u/m2h2CXxu7rpuwwIRegBGUy6tPS5eC+fC17iK8J1v
Al1H+UdMLezC1KsR7vd2E9YzpfZRe2+ZlhQshnBNKS7a8jzZHEM6M3GsIQuG4DHk5uiMyO3+q/bH
RoaMdmr1I2SYdMCDFAEbriTjn8m35TR/Sq/vvMc42oFPmSgu/D9lLxEm9CBPAtRHUF4weAFOQmKw
P3nTNMk3kYOj3Ivv/4E6m/WlctY8Pt80naUfv7JPkUUgr8vS46XVyOTIJISsbfBFpzGzZKSmyZ3+
9eG1PBBMnXVYLsJDzfnpQImT+Nd4hR+6hjDuSW+xEYSqTvRANGfYygDGdut1xkhDUykC3KrTSQj0
y4+KvgLYd+R+o9FZ6UhxfX/Sw2o8g1czsp1rYYiEpzdtJauFim6b2IPmlDuVWyJpxijKDmWz0aSG
sNfJ/FU1rhMIouxDgkutZS7CFIrGvw5k+c5ThsLj/yIqeOdx/y50w4uvIpGO11nkgC6CAtH3Gum0
5lSrO6zXMFzYNUQhCys5cQ87frcTyRPxHggCPiEzzdZww7OIeT9m1D+pBmrdn72Jrl6ATn644P89
crmOmHPt6+ZJIzGqs9ZhmTYTjL8x81cY/5u/hFtZTgkdFGxg8C6AhCXnmVVPlrOX4dv8EJaEzUyl
L4i1Nbb6ED3rnzppLueJ/SMdUh436EXt3M3D/M0Mndk0jnxN7yTCV9HwIRnYQ31kZ29KNQkC5/G7
nN/nFX1HDEhKfPVvWrKmvRaf/PgSQI/6fCD/OmhW3nZJ3hgBuM0g/RNuiJO2x53uRLkEaRtV3oyb
Rtkj5IHt5dRH0O/354HruWrXIYb40NviLw4O+ZxNx6CKH6Fyh+jvYCzqZGbohGoGVHxelpdh1Htj
5hv+3syjlJ4qeAdnBHC2Mm4jmX/BEvj0x5CRw7lz0vs47vqmtkH3ydDVjzVcgYN15BbL/Fnm4bFw
7BFt7Yrdufz5aGH66MSJfkVT8wvMJOGlSp0QRevtOWTcNXIHeN6hLpFZ0abTSSbWcNp6xw9Zaxtb
SXIzLKe1G7aECjvtWN7aqQJm1ZUuvkMhuHTW2TVbczpbSLcC+KS7TbGDyfkVu0Gi6Fw900KeInVz
crjoBi8UcD3BjK7jKcDb6LT8sWlOBMxhvD3SEqZi1MUQ7MsnFch2Pb3JGY9+sYm04wlhFQvYlkjH
t3/2EeUjGse9wFq9fdBHJ4DST3sZoiefia+tqglmm+8pb5grTMvuZUTZ66b46WMhxdHtM3ucrVLV
VlSRexl9VJOfyBr4nN1tayNKIs6iqjVi9jKfmTb4un4lBQggU7Vx9/tRcMaDBBernIlPued4NQER
McJQcai4zOyBYPtDAVQXP3eoznL0MFT2+QtK1tH52hLmPidOqaI/M/8vkxm4PCRhfGW1LBXUL3E8
eKA+1YGWAgncSrsNv4HGZx9cc0XXTlEbcXt9Drl2O7HqmtNE7ZHSzhI4DhmxkwoA+kp0KMIelf/2
c0RqAaQhf9dGRsWEjTgsmT/VsSq8LWISOKsgeYNvehfZVbovMZkMzeC+NHcsuiXiQLKiLUZMUPKA
kUHmNFEhQwLaS07ea8tp0vop/w21sZTCmyLo/jsr1KnbHa0KWY2P+U/XR5+0kaMYe2eL7U8jS0F6
Bs2S/Ra/AdM/wEbmLW4osWeHDVspzecMuViWhH3waQtWi4TvpC06XkY/YqJBrL0khIpkZMYjUTpt
6QrxGgsarHNEu6IWs2XhNYMFzstAi00rwfMHYWk5/5/VLCJ0Ic+ZU9cEwk7S+pRZabo3DlvO2Gfd
VvfB3EUrG3KpBvvOX/tVDe3PZoX9HmK79ZjpszQuLQehR1ITpbrrLZBy3ZRa75ZkyXQiJODvRehB
fWySmihTgsoVyB6eLzdNHqzyRO5FngP1585jK4y999GfhEz1qv9xUc666e7YJdNVSpOuUvgXXQaj
0gDmdNTA6rhChb+lg+Yq0xg7yLIf0svGrgNt6mF4Yocbdny+98VQjLHbulPY+netB9Yy8oIsWm3n
jwE5Xq7zP0dlNAhLku8O2aHXS52aWlLChisCtYuGuhxIWl0FmSla1VIgd4L8RdvqEM2oMCZGTfZ+
MHO/Phcp/lSp2LgS3Ctp4TQvTBHrSR0cr67AshVmSPAHAeiTBfxYOzRW1WIuyLiBYe31EE24lHK+
sZC2mej965zQv/ANSNHTq2SMQSOmUOtDPO7g8O8g9c7mMrmy6eHY3OFdTuhq6SDNzTEy2SRvzpNA
xC/8W1ITGLt8YDWXphXnDFv9NU0b+GT94m+tuxbvKnTAwVMe9qN5EIgCgbgg7bZNGCR6fNiHUWfe
o9iJp3LsupIONz4+FtDvZj+uTOVw/aTz3m2ie610UJYxXRFiZUlXxLvXFd6LgRQu0ClfT/4ADEz2
iZJDy+nXRDboC02ysuLUV1qb3roj6csQCDXnKUnbK8jH1mq2t1r8L4orOux8TcExegM/11VwRurK
sjCJcKlC+xGr2XEm22gRx0YCKWz2XecUtkfNGhWV4NjutQtAgGIkPQE6uzZuZVQMiPELLWlMtp2K
T8RgS6nzaBU+IIzQFb5fmbeKKkg3JsawwZEf/iwsgAObCjXTe9ozRrVhGaU4X2xwsvM3T6SpDs4v
lYBvQlSWSyylCE8BKyuRiP3DHZAgwJ+SpTj4y2+w/KlJWR57JFvN/0ioBDJcCjalEQyZU4T1JGIb
D5lzGR57vse48AgyNRgMmZgkVmOsYxqoDEczfxG+tGxop1r+I45ID2uS9LYur73lt4VukvK54Qj1
U/E75ErJGdvvLiOaJPo+0yH2zMvVG4lazunoOM1VsZIhIZr5vfsG71Svd/n+10trmnmEML4kkT0c
9DHn8UO+gUEw9+1knFU7+Gwt0gS+JeljHwqjr1KSrfyAVviPPERaytsj9eGXyYHlX9kCd7tTEgh+
a0yfaOn55MpFut/hejQnCNeX31uIQ6wDnqTpKBaNr3b1wRWxDTtId0l4yVC5Jf28jC0wpzSeR2h6
p/Qq9DrTVZdJbBADv+2QOPhZ66d1V63hZpZEiK5T+rZi5Yn7NZ85Zh/1qFVSkoW5ktBguzJIYXXA
riN0ZgfFjbxoo1/btIpQzp8Xk7/D5ZG81i+hJxZmWtF+Cg8x/k+UO5Ty1SqgPFaHTtZEq/V4zD4Q
/Rs6f8Xd9og/+LwS9szIceb3nzfFivdLZpYQehUsro5LfnLA4o1YRffjbiBzPhDQAyWFIrVzJEvh
kvZFq74KRILVGifhhecWDvd2/5bk+EshTHHoP7qrsptCSExcK8cBdmStzsejzlAoOl0Xk2RsrKK/
gjzvqeO0E6+qGmFvicrqEmdfDsdHhcFrPItZPAkfb0MkUCJHnvwy3MpoePhIX1tN1n2CuglhNsws
XxzignXBfb+kTvqlhZSIxwJjqcYPOAUJ/z3mft+azNqFXyIdcIyurjEIALwIbj9qUNwicEP6WjfU
58crV+5tdlctFrujRladH7QtFfJzt3gbZxhRkHHLg+ktnpo2au67XVd4KxeXHKwD+nEW2mcVJ5G8
vPG1QErVmlMmMas7rL7MKlfgukAv8eCHuwKELfNvaOjnQNy62rKcqM9W8uwNASPKRRh38bW6MKr4
F6lcDiIxTz8BmCZsPQbJ9cghaqvaEDWXmuzZmJPPL/lsT6mxqAdjSObgUKDkEuS3yWgcr4qRyTt0
B/xW8PzShDLv3dxCj32+pBcGu+MnZ4SuyIgTFN53D7345dxYLj7hUhkyjStVtnm1RdsmSpXZoDqH
tjdbs1mY1GSlRDUqlf4ToTkDY/IedHrnDprgapjcsr7TLRgj09axCfx3QEjaU4UkbcgcbqgtIJnf
0tdWIWtSfPda7JhpMTCfTa4xYjQSUAD+EmCs0qBqecVpARa5/0nEjUOj96/tauMRRthYlrfuSdRR
xqLT2bF2djlOAYSOTvTlwc/AwnvADtLRPBnUf7hQFKUocCYVmLd9b/w378QFXdltWY5fGxISu0UR
o17yJ0/dMPL+/oJW9iNgwJF+5dvpBfg0lQWa/Jq9YXzZArjlGmHcVKDMo2NZS0lZ0dYp0tPNE/eW
j0aEaHv1eqwMlfO69IYV3pmAJxpHtt76joRJIGbe/z6s4mt1zuZNLXTl18rsInYuxnXtxUN2tRTS
fMQ2SDSgHvd/LLcoNiopl9jyHceeTw7TtrB8YziWmDdeZ3ftTpAzVTL5I4XmX7yO80gWUziXtUQt
D8hl4ZzVtlTfW/DwCFGGQH4nBk/w7OMhgC4tO/n4DMPRyR0/sqtVkV4zCoRdp54rpePersfUd1hz
5Men9PqIR4XmFiJ/iNr9Pa1jDWfbGoBk5bHF+Y5wIJQ+sbfr7RUhcgfwr1MYu/Dtj+26AbkrzFS8
OAUjMlzqIWjwXQy296MiZYts3H1ulZ6L494Rclc5nyhIyJ+SgayQQW9CtyDjkhCQi3qDPIv9nm7J
BU0JB/EXhNIFranI3biSjmg79/GpgAEDkcO8vdXBqnBW4WNfRpkmJjI3vc13Ohl2sQd95C1TBsCV
eL87f/QcNlZrJzjOwaIv
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
