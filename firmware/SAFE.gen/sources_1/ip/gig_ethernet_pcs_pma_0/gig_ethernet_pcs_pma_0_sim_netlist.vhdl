-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2024.1 (lin64) Build 5076996 Wed May 22 18:36:09 MDT 2024
-- Date        : Fri Jul 12 09:13:08 2024
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
-- Command     : write_vhdl -force -mode funcsim
--               /data/cms/ecal/fe/SAFE/firmware/SAFE.gen/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_sim_netlist.vhdl
-- Design      : gig_ethernet_pcs_pma_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  port (
    gtxe2_i_0 : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_2 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    cpll_pd0_i : in STD_LOGIC;
    cpllreset_in : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_8 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_11 : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_GT;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_GT is
  signal gtxe2_i_n_0 : STD_LOGIC;
  signal gtxe2_i_n_10 : STD_LOGIC;
  signal gtxe2_i_n_16 : STD_LOGIC;
  signal gtxe2_i_n_170 : STD_LOGIC;
  signal gtxe2_i_n_171 : STD_LOGIC;
  signal gtxe2_i_n_172 : STD_LOGIC;
  signal gtxe2_i_n_173 : STD_LOGIC;
  signal gtxe2_i_n_174 : STD_LOGIC;
  signal gtxe2_i_n_175 : STD_LOGIC;
  signal gtxe2_i_n_176 : STD_LOGIC;
  signal gtxe2_i_n_177 : STD_LOGIC;
  signal gtxe2_i_n_178 : STD_LOGIC;
  signal gtxe2_i_n_179 : STD_LOGIC;
  signal gtxe2_i_n_180 : STD_LOGIC;
  signal gtxe2_i_n_181 : STD_LOGIC;
  signal gtxe2_i_n_182 : STD_LOGIC;
  signal gtxe2_i_n_183 : STD_LOGIC;
  signal gtxe2_i_n_184 : STD_LOGIC;
  signal gtxe2_i_n_27 : STD_LOGIC;
  signal gtxe2_i_n_3 : STD_LOGIC;
  signal gtxe2_i_n_38 : STD_LOGIC;
  signal gtxe2_i_n_39 : STD_LOGIC;
  signal gtxe2_i_n_4 : STD_LOGIC;
  signal gtxe2_i_n_46 : STD_LOGIC;
  signal gtxe2_i_n_47 : STD_LOGIC;
  signal gtxe2_i_n_48 : STD_LOGIC;
  signal gtxe2_i_n_49 : STD_LOGIC;
  signal gtxe2_i_n_50 : STD_LOGIC;
  signal gtxe2_i_n_51 : STD_LOGIC;
  signal gtxe2_i_n_52 : STD_LOGIC;
  signal gtxe2_i_n_53 : STD_LOGIC;
  signal gtxe2_i_n_54 : STD_LOGIC;
  signal gtxe2_i_n_55 : STD_LOGIC;
  signal gtxe2_i_n_56 : STD_LOGIC;
  signal gtxe2_i_n_57 : STD_LOGIC;
  signal gtxe2_i_n_58 : STD_LOGIC;
  signal gtxe2_i_n_59 : STD_LOGIC;
  signal gtxe2_i_n_60 : STD_LOGIC;
  signal gtxe2_i_n_61 : STD_LOGIC;
  signal gtxe2_i_n_81 : STD_LOGIC;
  signal gtxe2_i_n_83 : STD_LOGIC;
  signal gtxe2_i_n_84 : STD_LOGIC;
  signal gtxe2_i_n_9 : STD_LOGIC;
  signal NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PHYSTATUS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDATAVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXELECIDLE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_RXVALID_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENN_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXQPISENP_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_TXRATEDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHARISK_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXCHBONDO_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXDATA_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 16 );
  signal NLW_gtxe2_i_RXDISPERR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXHEADER_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_gtxe2_i_RXSTATUS_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_gtxe2_i_TSTOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_i : label is "PRIMITIVE";
begin
gtxe2_i: unisim.vcomponents.GTXE2_CHANNEL
    generic map(
      ALIGN_COMMA_DOUBLE => "FALSE",
      ALIGN_COMMA_ENABLE => B"0001111111",
      ALIGN_COMMA_WORD => 2,
      ALIGN_MCOMMA_DET => "TRUE",
      ALIGN_MCOMMA_VALUE => B"1010000011",
      ALIGN_PCOMMA_DET => "TRUE",
      ALIGN_PCOMMA_VALUE => B"0101111100",
      CBCC_DATA_SOURCE_SEL => "DECODED",
      CHAN_BOND_KEEP_ALIGN => "FALSE",
      CHAN_BOND_MAX_SKEW => 1,
      CHAN_BOND_SEQ_1_1 => B"0000000000",
      CHAN_BOND_SEQ_1_2 => B"0000000000",
      CHAN_BOND_SEQ_1_3 => B"0000000000",
      CHAN_BOND_SEQ_1_4 => B"0000000000",
      CHAN_BOND_SEQ_1_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_1 => B"0000000000",
      CHAN_BOND_SEQ_2_2 => B"0000000000",
      CHAN_BOND_SEQ_2_3 => B"0000000000",
      CHAN_BOND_SEQ_2_4 => B"0000000000",
      CHAN_BOND_SEQ_2_ENABLE => B"1111",
      CHAN_BOND_SEQ_2_USE => "FALSE",
      CHAN_BOND_SEQ_LEN => 1,
      CLK_CORRECT_USE => "TRUE",
      CLK_COR_KEEP_IDLE => "FALSE",
      CLK_COR_MAX_LAT => 36,
      CLK_COR_MIN_LAT => 33,
      CLK_COR_PRECEDENCE => "TRUE",
      CLK_COR_REPEAT_WAIT => 0,
      CLK_COR_SEQ_1_1 => B"0110111100",
      CLK_COR_SEQ_1_2 => B"0001010000",
      CLK_COR_SEQ_1_3 => B"0000000000",
      CLK_COR_SEQ_1_4 => B"0000000000",
      CLK_COR_SEQ_1_ENABLE => B"1111",
      CLK_COR_SEQ_2_1 => B"0110111100",
      CLK_COR_SEQ_2_2 => B"0010110101",
      CLK_COR_SEQ_2_3 => B"0000000000",
      CLK_COR_SEQ_2_4 => B"0000000000",
      CLK_COR_SEQ_2_ENABLE => B"1111",
      CLK_COR_SEQ_2_USE => "TRUE",
      CLK_COR_SEQ_LEN => 2,
      CPLL_CFG => X"BC07DC",
      CPLL_FBDIV => 4,
      CPLL_FBDIV_45 => 5,
      CPLL_INIT_CFG => X"00001E",
      CPLL_LOCK_CFG => X"01E8",
      CPLL_REFCLK_DIV => 1,
      DEC_MCOMMA_DETECT => "TRUE",
      DEC_PCOMMA_DETECT => "TRUE",
      DEC_VALID_COMMA_ONLY => "FALSE",
      DMONITOR_CFG => X"000A00",
      ES_CONTROL => B"000000",
      ES_ERRDET_EN => "FALSE",
      ES_EYE_SCAN_EN => "TRUE",
      ES_HORZ_OFFSET => X"000",
      ES_PMA_CFG => B"0000000000",
      ES_PRESCALE => B"00000",
      ES_QUALIFIER => X"00000000000000000000",
      ES_QUAL_MASK => X"00000000000000000000",
      ES_SDATA_MASK => X"00000000000000000000",
      ES_VERT_OFFSET => B"000000000",
      FTS_DESKEW_SEQ_ENABLE => B"1111",
      FTS_LANE_DESKEW_CFG => B"1111",
      FTS_LANE_DESKEW_EN => "FALSE",
      GEARBOX_MODE => B"000",
      IS_CPLLLOCKDETCLK_INVERTED => '0',
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_RXUSRCLK2_INVERTED => '0',
      IS_RXUSRCLK_INVERTED => '0',
      IS_TXPHDLYTSTCLK_INVERTED => '0',
      IS_TXUSRCLK2_INVERTED => '0',
      IS_TXUSRCLK_INVERTED => '0',
      OUTREFCLK_SEL_INV => B"11",
      PCS_PCIE_EN => "FALSE",
      PCS_RSVD_ATTR => X"000000000000",
      PD_TRANS_TIME_FROM_P2 => X"03C",
      PD_TRANS_TIME_NONE_P2 => X"19",
      PD_TRANS_TIME_TO_P2 => X"64",
      PMA_RSV => X"00018480",
      PMA_RSV2 => X"2050",
      PMA_RSV3 => B"00",
      PMA_RSV4 => X"00000000",
      RXBUFRESET_TIME => B"00001",
      RXBUF_ADDR_MODE => "FULL",
      RXBUF_EIDLE_HI_CNT => B"1000",
      RXBUF_EIDLE_LO_CNT => B"0000",
      RXBUF_EN => "TRUE",
      RXBUF_RESET_ON_CB_CHANGE => "TRUE",
      RXBUF_RESET_ON_COMMAALIGN => "FALSE",
      RXBUF_RESET_ON_EIDLE => "FALSE",
      RXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      RXBUF_THRESH_OVFLW => 61,
      RXBUF_THRESH_OVRD => "FALSE",
      RXBUF_THRESH_UNDFLW => 8,
      RXCDRFREQRESET_TIME => B"00001",
      RXCDRPHRESET_TIME => B"00001",
      RXCDR_CFG => X"03000023FF10100020",
      RXCDR_FR_RESET_ON_EIDLE => '0',
      RXCDR_HOLD_DURING_EIDLE => '0',
      RXCDR_LOCK_CFG => B"010101",
      RXCDR_PH_RESET_ON_EIDLE => '0',
      RXDFELPMRESET_TIME => B"0001111",
      RXDLY_CFG => X"001F",
      RXDLY_LCFG => X"030",
      RXDLY_TAP_CFG => X"0000",
      RXGEARBOX_EN => "FALSE",
      RXISCANRESET_TIME => B"00001",
      RXLPM_HF_CFG => B"00000011110000",
      RXLPM_LF_CFG => B"00000011110000",
      RXOOB_CFG => B"0000110",
      RXOUT_DIV => 4,
      RXPCSRESET_TIME => B"00001",
      RXPHDLY_CFG => X"084020",
      RXPH_CFG => X"000000",
      RXPH_MONITOR_SEL => B"00000",
      RXPMARESET_TIME => B"00011",
      RXPRBS_ERR_LOOPBACK => '0',
      RXSLIDE_AUTO_WAIT => 7,
      RXSLIDE_MODE => "OFF",
      RX_BIAS_CFG => B"000000000100",
      RX_BUFFER_CFG => B"000000",
      RX_CLK25_DIV => 5,
      RX_CLKMUX_PD => '1',
      RX_CM_SEL => B"11",
      RX_CM_TRIM => B"010",
      RX_DATA_WIDTH => 20,
      RX_DDI_SEL => B"000000",
      RX_DEBUG_CFG => B"000000000000",
      RX_DEFER_RESET_BUF_EN => "TRUE",
      RX_DFE_GAIN_CFG => X"020FEA",
      RX_DFE_H2_CFG => B"000000000000",
      RX_DFE_H3_CFG => B"000001000000",
      RX_DFE_H4_CFG => B"00011110000",
      RX_DFE_H5_CFG => B"00011100000",
      RX_DFE_KL_CFG => B"0000011111110",
      RX_DFE_KL_CFG2 => X"301148AC",
      RX_DFE_LPM_CFG => X"0904",
      RX_DFE_LPM_HOLD_DURING_EIDLE => '0',
      RX_DFE_UT_CFG => B"10001111000000000",
      RX_DFE_VP_CFG => B"00011111100000011",
      RX_DFE_XYD_CFG => B"0000000000000",
      RX_DISPERR_SEQ_MATCH => "TRUE",
      RX_INT_DATAWIDTH => 0,
      RX_OS_CFG => B"0000010000000",
      RX_SIG_VALID_DLY => 10,
      RX_XCLK_SEL => "RXREC",
      SAS_MAX_COM => 64,
      SAS_MIN_COM => 36,
      SATA_BURST_SEQ_LEN => B"0101",
      SATA_BURST_VAL => B"100",
      SATA_CPLL_CFG => "VCO_3000MHZ",
      SATA_EIDLE_VAL => B"100",
      SATA_MAX_BURST => 8,
      SATA_MAX_INIT => 21,
      SATA_MAX_WAKE => 7,
      SATA_MIN_BURST => 4,
      SATA_MIN_INIT => 12,
      SATA_MIN_WAKE => 4,
      SHOW_REALIGN_COMMA => "TRUE",
      SIM_CPLLREFCLK_SEL => B"001",
      SIM_RECEIVER_DETECT_PASS => "TRUE",
      SIM_RESET_SPEEDUP => "TRUE",
      SIM_TX_EIDLE_DRIVE_LEVEL => "X",
      SIM_VERSION => "4.0",
      TERM_RCAL_CFG => B"10000",
      TERM_RCAL_OVRD => '0',
      TRANS_TIME_RATE => X"0E",
      TST_RSV => X"00000000",
      TXBUF_EN => "TRUE",
      TXBUF_RESET_ON_RATE_CHANGE => "TRUE",
      TXDLY_CFG => X"001F",
      TXDLY_LCFG => X"030",
      TXDLY_TAP_CFG => X"0000",
      TXGEARBOX_EN => "FALSE",
      TXOUT_DIV => 4,
      TXPCSRESET_TIME => B"00001",
      TXPHDLY_CFG => X"084020",
      TXPH_CFG => X"0780",
      TXPH_MONITOR_SEL => B"00000",
      TXPMARESET_TIME => B"00001",
      TX_CLK25_DIV => 5,
      TX_CLKMUX_PD => '1',
      TX_DATA_WIDTH => 20,
      TX_DEEMPH0 => B"00000",
      TX_DEEMPH1 => B"00000",
      TX_DRIVE_MODE => "DIRECT",
      TX_EIDLE_ASSERT_DELAY => B"110",
      TX_EIDLE_DEASSERT_DELAY => B"100",
      TX_INT_DATAWIDTH => 0,
      TX_LOOPBACK_DRIVE_HIZ => "FALSE",
      TX_MAINCURSOR_SEL => '0',
      TX_MARGIN_FULL_0 => B"1001110",
      TX_MARGIN_FULL_1 => B"1001001",
      TX_MARGIN_FULL_2 => B"1000101",
      TX_MARGIN_FULL_3 => B"1000010",
      TX_MARGIN_FULL_4 => B"1000000",
      TX_MARGIN_LOW_0 => B"1000110",
      TX_MARGIN_LOW_1 => B"1000100",
      TX_MARGIN_LOW_2 => B"1000010",
      TX_MARGIN_LOW_3 => B"1000000",
      TX_MARGIN_LOW_4 => B"1000000",
      TX_PREDRIVER_MODE => '0',
      TX_QPI_STATUS_EN => '0',
      TX_RXDETECT_CFG => X"1832",
      TX_RXDETECT_REF => B"100",
      TX_XCLK_SEL => "TXOUT",
      UCODEER_CLR => '0'
    )
        port map (
      CFGRESET => '0',
      CLKRSVD(3 downto 0) => B"0000",
      CPLLFBCLKLOST => gtxe2_i_n_0,
      CPLLLOCK => gtxe2_i_0,
      CPLLLOCKDETCLK => independent_clock_bufg,
      CPLLLOCKEN => '1',
      CPLLPD => cpll_pd0_i,
      CPLLREFCLKLOST => gt0_cpllrefclklost_i,
      CPLLREFCLKSEL(2 downto 0) => B"001",
      CPLLRESET => cpllreset_in,
      DMONITOROUT(7) => gtxe2_i_n_177,
      DMONITOROUT(6) => gtxe2_i_n_178,
      DMONITOROUT(5) => gtxe2_i_n_179,
      DMONITOROUT(4) => gtxe2_i_n_180,
      DMONITOROUT(3) => gtxe2_i_n_181,
      DMONITOROUT(2) => gtxe2_i_n_182,
      DMONITOROUT(1) => gtxe2_i_n_183,
      DMONITOROUT(0) => gtxe2_i_n_184,
      DRPADDR(8 downto 0) => B"000000000",
      DRPCLK => gtrefclk_bufg,
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15) => gtxe2_i_n_46,
      DRPDO(14) => gtxe2_i_n_47,
      DRPDO(13) => gtxe2_i_n_48,
      DRPDO(12) => gtxe2_i_n_49,
      DRPDO(11) => gtxe2_i_n_50,
      DRPDO(10) => gtxe2_i_n_51,
      DRPDO(9) => gtxe2_i_n_52,
      DRPDO(8) => gtxe2_i_n_53,
      DRPDO(7) => gtxe2_i_n_54,
      DRPDO(6) => gtxe2_i_n_55,
      DRPDO(5) => gtxe2_i_n_56,
      DRPDO(4) => gtxe2_i_n_57,
      DRPDO(3) => gtxe2_i_n_58,
      DRPDO(2) => gtxe2_i_n_59,
      DRPDO(1) => gtxe2_i_n_60,
      DRPDO(0) => gtxe2_i_n_61,
      DRPEN => '0',
      DRPRDY => gtxe2_i_n_3,
      DRPWE => '0',
      EYESCANDATAERROR => gtxe2_i_n_4,
      EYESCANMODE => '0',
      EYESCANRESET => '0',
      EYESCANTRIGGER => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTREFCLKMONITOR => NLW_gtxe2_i_GTREFCLKMONITOR_UNCONNECTED,
      GTRESETSEL => '0',
      GTRSVD(15 downto 0) => B"0000000000000000",
      GTRXRESET => SR(0),
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      GTTXRESET => gt0_gttxreset_in0_out,
      GTXRXN => rxn,
      GTXRXP => rxp,
      GTXTXN => txn,
      GTXTXP => txp,
      LOOPBACK(2 downto 0) => B"000",
      PCSRSVDIN(15 downto 0) => B"0000000000000000",
      PCSRSVDIN2(4 downto 0) => B"00000",
      PCSRSVDOUT(15 downto 0) => NLW_gtxe2_i_PCSRSVDOUT_UNCONNECTED(15 downto 0),
      PHYSTATUS => NLW_gtxe2_i_PHYSTATUS_UNCONNECTED,
      PMARSVDIN(4 downto 0) => B"00000",
      PMARSVDIN2(4 downto 0) => B"00000",
      QPLLCLK => gt0_qplloutclk_out,
      QPLLREFCLK => gt0_qplloutrefclk_out,
      RESETOVRD => '0',
      RX8B10BEN => '1',
      RXBUFRESET => '0',
      RXBUFSTATUS(2) => RXBUFSTATUS(0),
      RXBUFSTATUS(1) => gtxe2_i_n_83,
      RXBUFSTATUS(0) => gtxe2_i_n_84,
      RXBYTEISALIGNED => gtxe2_i_n_9,
      RXBYTEREALIGN => gtxe2_i_n_10,
      RXCDRFREQRESET => '0',
      RXCDRHOLD => '0',
      RXCDRLOCK => NLW_gtxe2_i_RXCDRLOCK_UNCONNECTED,
      RXCDROVRDEN => '0',
      RXCDRRESET => '0',
      RXCDRRESETRSV => '0',
      RXCHANBONDSEQ => NLW_gtxe2_i_RXCHANBONDSEQ_UNCONNECTED,
      RXCHANISALIGNED => NLW_gtxe2_i_RXCHANISALIGNED_UNCONNECTED,
      RXCHANREALIGN => NLW_gtxe2_i_RXCHANREALIGN_UNCONNECTED,
      RXCHARISCOMMA(7 downto 2) => NLW_gtxe2_i_RXCHARISCOMMA_UNCONNECTED(7 downto 2),
      RXCHARISCOMMA(1 downto 0) => gtxe2_i_4(1 downto 0),
      RXCHARISK(7 downto 2) => NLW_gtxe2_i_RXCHARISK_UNCONNECTED(7 downto 2),
      RXCHARISK(1 downto 0) => gtxe2_i_5(1 downto 0),
      RXCHBONDEN => '0',
      RXCHBONDI(4 downto 0) => B"00000",
      RXCHBONDLEVEL(2 downto 0) => B"000",
      RXCHBONDMASTER => '0',
      RXCHBONDO(4 downto 0) => NLW_gtxe2_i_RXCHBONDO_UNCONNECTED(4 downto 0),
      RXCHBONDSLAVE => '0',
      RXCLKCORCNT(1 downto 0) => D(1 downto 0),
      RXCOMINITDET => NLW_gtxe2_i_RXCOMINITDET_UNCONNECTED,
      RXCOMMADET => gtxe2_i_n_16,
      RXCOMMADETEN => '1',
      RXCOMSASDET => NLW_gtxe2_i_RXCOMSASDET_UNCONNECTED,
      RXCOMWAKEDET => NLW_gtxe2_i_RXCOMWAKEDET_UNCONNECTED,
      RXDATA(63 downto 16) => NLW_gtxe2_i_RXDATA_UNCONNECTED(63 downto 16),
      RXDATA(15 downto 0) => gtxe2_i_3(15 downto 0),
      RXDATAVALID => NLW_gtxe2_i_RXDATAVALID_UNCONNECTED,
      RXDDIEN => '0',
      RXDFEAGCHOLD => '0',
      RXDFEAGCOVRDEN => '0',
      RXDFECM1EN => '0',
      RXDFELFHOLD => '0',
      RXDFELFOVRDEN => '0',
      RXDFELPMRESET => '0',
      RXDFETAP2HOLD => '0',
      RXDFETAP2OVRDEN => '0',
      RXDFETAP3HOLD => '0',
      RXDFETAP3OVRDEN => '0',
      RXDFETAP4HOLD => '0',
      RXDFETAP4OVRDEN => '0',
      RXDFETAP5HOLD => '0',
      RXDFETAP5OVRDEN => '0',
      RXDFEUTHOLD => '0',
      RXDFEUTOVRDEN => '0',
      RXDFEVPHOLD => '0',
      RXDFEVPOVRDEN => '0',
      RXDFEVSEN => '0',
      RXDFEXYDEN => '1',
      RXDFEXYDHOLD => '0',
      RXDFEXYDOVRDEN => '0',
      RXDISPERR(7 downto 2) => NLW_gtxe2_i_RXDISPERR_UNCONNECTED(7 downto 2),
      RXDISPERR(1 downto 0) => gtxe2_i_6(1 downto 0),
      RXDLYBYPASS => '1',
      RXDLYEN => '0',
      RXDLYOVRDEN => '0',
      RXDLYSRESET => '0',
      RXDLYSRESETDONE => NLW_gtxe2_i_RXDLYSRESETDONE_UNCONNECTED,
      RXELECIDLE => NLW_gtxe2_i_RXELECIDLE_UNCONNECTED,
      RXELECIDLEMODE(1 downto 0) => B"11",
      RXGEARBOXSLIP => '0',
      RXHEADER(2 downto 0) => NLW_gtxe2_i_RXHEADER_UNCONNECTED(2 downto 0),
      RXHEADERVALID => NLW_gtxe2_i_RXHEADERVALID_UNCONNECTED,
      RXLPMEN => '1',
      RXLPMHFHOLD => '0',
      RXLPMHFOVRDEN => '0',
      RXLPMLFHOLD => '0',
      RXLPMLFKLOVRDEN => '0',
      RXMCOMMAALIGNEN => reset_out,
      RXMONITOROUT(6) => gtxe2_i_n_170,
      RXMONITOROUT(5) => gtxe2_i_n_171,
      RXMONITOROUT(4) => gtxe2_i_n_172,
      RXMONITOROUT(3) => gtxe2_i_n_173,
      RXMONITOROUT(2) => gtxe2_i_n_174,
      RXMONITOROUT(1) => gtxe2_i_n_175,
      RXMONITOROUT(0) => gtxe2_i_n_176,
      RXMONITORSEL(1 downto 0) => B"00",
      RXNOTINTABLE(7 downto 2) => NLW_gtxe2_i_RXNOTINTABLE_UNCONNECTED(7 downto 2),
      RXNOTINTABLE(1 downto 0) => gtxe2_i_7(1 downto 0),
      RXOOBRESET => '0',
      RXOSHOLD => '0',
      RXOSOVRDEN => '0',
      RXOUTCLK => rxoutclk,
      RXOUTCLKFABRIC => NLW_gtxe2_i_RXOUTCLKFABRIC_UNCONNECTED,
      RXOUTCLKPCS => NLW_gtxe2_i_RXOUTCLKPCS_UNCONNECTED,
      RXOUTCLKSEL(2 downto 0) => B"010",
      RXPCOMMAALIGNEN => reset_out,
      RXPCSRESET => reset,
      RXPD(1) => RXPD(0),
      RXPD(0) => RXPD(0),
      RXPHALIGN => '0',
      RXPHALIGNDONE => NLW_gtxe2_i_RXPHALIGNDONE_UNCONNECTED,
      RXPHALIGNEN => '0',
      RXPHDLYPD => '0',
      RXPHDLYRESET => '0',
      RXPHMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHMONITOR_UNCONNECTED(4 downto 0),
      RXPHOVRDEN => '0',
      RXPHSLIPMONITOR(4 downto 0) => NLW_gtxe2_i_RXPHSLIPMONITOR_UNCONNECTED(4 downto 0),
      RXPMARESET => '0',
      RXPOLARITY => '0',
      RXPRBSCNTRESET => '0',
      RXPRBSERR => gtxe2_i_n_27,
      RXPRBSSEL(2 downto 0) => B"000",
      RXQPIEN => '0',
      RXQPISENN => NLW_gtxe2_i_RXQPISENN_UNCONNECTED,
      RXQPISENP => NLW_gtxe2_i_RXQPISENP_UNCONNECTED,
      RXRATE(2 downto 0) => B"000",
      RXRATEDONE => NLW_gtxe2_i_RXRATEDONE_UNCONNECTED,
      RXRESETDONE => gtxe2_i_1,
      RXSLIDE => '0',
      RXSTARTOFSEQ => NLW_gtxe2_i_RXSTARTOFSEQ_UNCONNECTED,
      RXSTATUS(2 downto 0) => NLW_gtxe2_i_RXSTATUS_UNCONNECTED(2 downto 0),
      RXSYSCLKSEL(1 downto 0) => B"00",
      RXUSERRDY => gt0_rxuserrdy_t,
      RXUSRCLK => gtxe2_i_8,
      RXUSRCLK2 => gtxe2_i_8,
      RXVALID => NLW_gtxe2_i_RXVALID_UNCONNECTED,
      SETERRSTATUS => '0',
      TSTIN(19 downto 0) => B"11111111111111111111",
      TSTOUT(9 downto 0) => NLW_gtxe2_i_TSTOUT_UNCONNECTED(9 downto 0),
      TX8B10BBYPASS(7 downto 0) => B"00000000",
      TX8B10BEN => '1',
      TXBUFDIFFCTRL(2 downto 0) => B"100",
      TXBUFSTATUS(1) => TXBUFSTATUS(0),
      TXBUFSTATUS(0) => gtxe2_i_n_81,
      TXCHARDISPMODE(7 downto 2) => B"000000",
      TXCHARDISPMODE(1 downto 0) => gtxe2_i_9(1 downto 0),
      TXCHARDISPVAL(7 downto 2) => B"000000",
      TXCHARDISPVAL(1 downto 0) => gtxe2_i_10(1 downto 0),
      TXCHARISK(7 downto 2) => B"000000",
      TXCHARISK(1 downto 0) => gtxe2_i_11(1 downto 0),
      TXCOMFINISH => NLW_gtxe2_i_TXCOMFINISH_UNCONNECTED,
      TXCOMINIT => '0',
      TXCOMSAS => '0',
      TXCOMWAKE => '0',
      TXDATA(63 downto 16) => B"000000000000000000000000000000000000000000000000",
      TXDATA(15 downto 0) => Q(15 downto 0),
      TXDEEMPH => '0',
      TXDETECTRX => '0',
      TXDIFFCTRL(3 downto 0) => B"1000",
      TXDIFFPD => '0',
      TXDLYBYPASS => '1',
      TXDLYEN => '0',
      TXDLYHOLD => '0',
      TXDLYOVRDEN => '0',
      TXDLYSRESET => '0',
      TXDLYSRESETDONE => NLW_gtxe2_i_TXDLYSRESETDONE_UNCONNECTED,
      TXDLYUPDOWN => '0',
      TXELECIDLE => TXPD(0),
      TXGEARBOXREADY => NLW_gtxe2_i_TXGEARBOXREADY_UNCONNECTED,
      TXHEADER(2 downto 0) => B"000",
      TXINHIBIT => '0',
      TXMAINCURSOR(6 downto 0) => B"0000000",
      TXMARGIN(2 downto 0) => B"000",
      TXOUTCLK => txoutclk,
      TXOUTCLKFABRIC => gtxe2_i_n_38,
      TXOUTCLKPCS => gtxe2_i_n_39,
      TXOUTCLKSEL(2 downto 0) => B"100",
      TXPCSRESET => '0',
      TXPD(1) => TXPD(0),
      TXPD(0) => TXPD(0),
      TXPDELECIDLEMODE => '0',
      TXPHALIGN => '0',
      TXPHALIGNDONE => NLW_gtxe2_i_TXPHALIGNDONE_UNCONNECTED,
      TXPHALIGNEN => '0',
      TXPHDLYPD => '0',
      TXPHDLYRESET => '0',
      TXPHDLYTSTCLK => '0',
      TXPHINIT => '0',
      TXPHINITDONE => NLW_gtxe2_i_TXPHINITDONE_UNCONNECTED,
      TXPHOVRDEN => '0',
      TXPISOPD => '0',
      TXPMARESET => '0',
      TXPOLARITY => '0',
      TXPOSTCURSOR(4 downto 0) => B"00000",
      TXPOSTCURSORINV => '0',
      TXPRBSFORCEERR => '0',
      TXPRBSSEL(2 downto 0) => B"000",
      TXPRECURSOR(4 downto 0) => B"00000",
      TXPRECURSORINV => '0',
      TXQPIBIASEN => '0',
      TXQPISENN => NLW_gtxe2_i_TXQPISENN_UNCONNECTED,
      TXQPISENP => NLW_gtxe2_i_TXQPISENP_UNCONNECTED,
      TXQPISTRONGPDOWN => '0',
      TXQPIWEAKPUP => '0',
      TXRATE(2 downto 0) => B"000",
      TXRATEDONE => NLW_gtxe2_i_TXRATEDONE_UNCONNECTED,
      TXRESETDONE => gtxe2_i_2,
      TXSEQUENCE(6 downto 0) => B"0000000",
      TXSTARTSEQ => '0',
      TXSWING => '0',
      TXSYSCLKSEL(1 downto 0) => B"00",
      TXUSERRDY => gt0_txuserrdy_t,
      TXUSRCLK => gtxe2_i_8,
      TXUSRCLK2 => gtxe2_i_8
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_clocking is
  port (
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg : out STD_LOGIC;
    mmcm_locked : out STD_LOGIC;
    userclk : out STD_LOGIC;
    userclk2 : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    txoutclk : in STD_LOGIC;
    mmcm_reset : in STD_LOGIC;
    rxoutclk : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_clocking;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_clocking is
  signal clkfbout : STD_LOGIC;
  signal clkout0 : STD_LOGIC;
  signal clkout1 : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal txoutclk_bufg : STD_LOGIC;
  signal NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of bufg_gtrefclk : label is "PRIMITIVE";
  attribute box_type of bufg_txoutclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk : label is "PRIMITIVE";
  attribute box_type of bufg_userclk2 : label is "PRIMITIVE";
  attribute box_type of ibufds_gtrefclk : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
  attribute box_type of rxrecclkbufg : label is "PRIMITIVE";
begin
  gtrefclk_out <= \^gtrefclk_out\;
bufg_gtrefclk: unisim.vcomponents.BUFG
     port map (
      I => \^gtrefclk_out\,
      O => gtrefclk_bufg
    );
bufg_txoutclk: unisim.vcomponents.BUFG
     port map (
      I => txoutclk,
      O => txoutclk_bufg
    );
bufg_userclk: unisim.vcomponents.BUFG
     port map (
      I => clkout1,
      O => userclk
    );
bufg_userclk2: unisim.vcomponents.BUFG
     port map (
      I => clkout0,
      O => userclk2
    );
ibufds_gtrefclk: unisim.vcomponents.IBUFDS_GTE2
    generic map(
      CLKCM_CFG => true,
      CLKRCV_TRST => true,
      CLKSWING_CFG => B"11"
    )
        port map (
      CEB => '0',
      I => gtrefclk_p,
      IB => gtrefclk_n,
      O => \^gtrefclk_out\,
      ODIV2 => NLW_ibufds_gtrefclk_ODIV2_UNCONNECTED
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 16.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 16.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 8.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 16,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "INTERNAL",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.000000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout,
      CLKFBOUT => clkfbout,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => txoutclk_bufg,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clkout0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clkout1,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => mmcm_locked,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => mmcm_reset
    );
rxrecclkbufg: unisim.vcomponents.BUFG
     port map (
      I => rxoutclk,
      O => rxuserclk2_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_cpll_railing is
  port (
    cpll_pd0_i : out STD_LOGIC;
    cpllreset_in : out STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gt0_cpllreset_t : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_cpll_railing;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_cpll_railing is
  signal cpll_reset_out : STD_LOGIC;
  signal \cpllpd_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllpd_wait_reg[94]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[126]_srl31_n_0\ : STD_LOGIC;
  signal \cpllreset_wait_reg[31]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[63]_srl32_n_1\ : STD_LOGIC;
  signal \cpllreset_wait_reg[95]_srl32_n_1\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\ : STD_LOGIC;
  attribute srl_bus_name : string;
  attribute srl_bus_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name : string;
  attribute srl_name of \cpllpd_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg ";
  attribute srl_name of \cpllpd_wait_reg[94]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllpd_wait_reg[94]_srl31 ";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \cpllpd_wait_reg[95]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[126]_srl31\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[126]_srl31 ";
  attribute equivalent_register_removal of \cpllreset_wait_reg[127]\ : label is "no";
  attribute srl_bus_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[31]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[31]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[63]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[63]_srl32 ";
  attribute srl_bus_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg ";
  attribute srl_name of \cpllreset_wait_reg[95]_srl32\ : label is "U0/\pcs_pma_block_i/transceiver_inst/gtwizard_inst/U0/gtwizard_i/cpll_railing0_i/cpllreset_wait_reg[95]_srl32 ";
begin
\cpllpd_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllpd_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[31]_srl32_n_1\
    );
\cpllpd_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"FFFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllpd_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllpd_wait_reg[63]_srl32_n_1\
    );
\cpllpd_wait_reg[94]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllpd_wait_reg[63]_srl32_n_1\,
      Q => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q31 => \NLW_cpllpd_wait_reg[94]_srl31_Q31_UNCONNECTED\
    );
\cpllpd_wait_reg[95]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllpd_wait_reg[94]_srl31_n_0\,
      Q => cpll_pd0_i,
      R => '0'
    );
\cpllreset_wait_reg[126]_srl31\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11110",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[95]_srl32_n_1\,
      Q => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q31 => \NLW_cpllreset_wait_reg[126]_srl31_Q31_UNCONNECTED\
    );
\cpllreset_wait_reg[127]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtrefclk_bufg,
      CE => '1',
      D => \cpllreset_wait_reg[126]_srl31_n_0\,
      Q => cpll_reset_out,
      R => '0'
    );
\cpllreset_wait_reg[31]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"000000FF"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => '0',
      Q => \NLW_cpllreset_wait_reg[31]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[31]_srl32_n_1\
    );
\cpllreset_wait_reg[63]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[31]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[63]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[63]_srl32_n_1\
    );
\cpllreset_wait_reg[95]_srl32\: unisim.vcomponents.SRLC32E
    generic map(
      INIT => X"00000000"
    )
        port map (
      A(4 downto 0) => B"11111",
      CE => '1',
      CLK => gtrefclk_bufg,
      D => \cpllreset_wait_reg[63]_srl32_n_1\,
      Q => \NLW_cpllreset_wait_reg[95]_srl32_Q_UNCONNECTED\,
      Q31 => \cpllreset_wait_reg[95]_srl32_n_1\
    );
gtxe2_i_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cpll_reset_out,
      I1 => gt0_cpllreset_t,
      O => cpllreset_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_gt_common is
  port (
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end gig_ethernet_pcs_pma_0_gt_common;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_gt_common is
  signal gtxe2_common_i_n_2 : STD_LOGIC;
  signal gtxe2_common_i_n_5 : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED : STD_LOGIC;
  signal NLW_gtxe2_common_i_DRPDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute box_type : string;
  attribute box_type of gtxe2_common_i : label is "PRIMITIVE";
begin
gtxe2_common_i: unisim.vcomponents.GTXE2_COMMON
    generic map(
      BIAS_CFG => X"0000040000001000",
      COMMON_CFG => X"00000000",
      IS_DRPCLK_INVERTED => '0',
      IS_GTGREFCLK_INVERTED => '0',
      IS_QPLLLOCKDETCLK_INVERTED => '0',
      QPLL_CFG => X"06801C1",
      QPLL_CLKOUT_CFG => B"0000",
      QPLL_COARSE_FREQ_OVRD => B"010000",
      QPLL_COARSE_FREQ_OVRD_EN => '0',
      QPLL_CP => B"0000011111",
      QPLL_CP_MONITOR_EN => '0',
      QPLL_DMONITOR_SEL => '0',
      QPLL_FBDIV => B"0000100000",
      QPLL_FBDIV_MONITOR_EN => '0',
      QPLL_FBDIV_RATIO => '1',
      QPLL_INIT_CFG => X"000006",
      QPLL_LOCK_CFG => X"21E8",
      QPLL_LPF => B"1111",
      QPLL_REFCLK_DIV => 1,
      SIM_QPLLREFCLK_SEL => B"001",
      SIM_RESET_SPEEDUP => "FALSE",
      SIM_VERSION => "4.0"
    )
        port map (
      BGBYPASSB => '1',
      BGMONITORENB => '1',
      BGPDB => '1',
      BGRCALOVRD(4 downto 0) => B"11111",
      DRPADDR(7 downto 0) => B"00000000",
      DRPCLK => '0',
      DRPDI(15 downto 0) => B"0000000000000000",
      DRPDO(15 downto 0) => NLW_gtxe2_common_i_DRPDO_UNCONNECTED(15 downto 0),
      DRPEN => '0',
      DRPRDY => NLW_gtxe2_common_i_DRPRDY_UNCONNECTED,
      DRPWE => '0',
      GTGREFCLK => '0',
      GTNORTHREFCLK0 => '0',
      GTNORTHREFCLK1 => '0',
      GTREFCLK0 => gtrefclk_out,
      GTREFCLK1 => '0',
      GTSOUTHREFCLK0 => '0',
      GTSOUTHREFCLK1 => '0',
      PMARSVD(7 downto 0) => B"00000000",
      QPLLDMONITOR(7 downto 0) => NLW_gtxe2_common_i_QPLLDMONITOR_UNCONNECTED(7 downto 0),
      QPLLFBCLKLOST => NLW_gtxe2_common_i_QPLLFBCLKLOST_UNCONNECTED,
      QPLLLOCK => gtxe2_common_i_n_2,
      QPLLLOCKDETCLK => independent_clock_bufg,
      QPLLLOCKEN => '1',
      QPLLOUTCLK => gt0_qplloutclk_out,
      QPLLOUTREFCLK => gt0_qplloutrefclk_out,
      QPLLOUTRESET => '0',
      QPLLPD => '1',
      QPLLREFCLKLOST => gtxe2_common_i_n_5,
      QPLLREFCLKSEL(2 downto 0) => B"001",
      QPLLRESET => \out\(0),
      QPLLRSVD1(15 downto 0) => B"0000000000000000",
      QPLLRSVD2(4 downto 0) => B"11111",
      RCALENB => '1',
      REFCLKOUTMONITOR => NLW_gtxe2_common_i_REFCLKOUTMONITOR_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync is
  port (
    reset_out : out STD_LOGIC;
    CLK : in STD_LOGIC;
    enablealign : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_reset_sync;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => '0',
      PRE => enablealign,
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg1,
      PRE => enablealign,
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg2,
      PRE => enablealign,
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg3,
      PRE => enablealign,
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg4,
      PRE => enablealign,
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync_1 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_reset_sync_1 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end gig_ethernet_pcs_pma_0_reset_sync_1;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync_1 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => SR(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => SR(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => SR(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => SR(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => SR(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_sync_2 is
  port (
    reset_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    reset_sync5_0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_reset_sync_2 : entity is "gig_ethernet_pcs_pma_0_reset_sync";
end gig_ethernet_pcs_pma_0_reset_sync_2;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_sync_2 is
  signal reset_sync_reg1 : STD_LOGIC;
  signal reset_sync_reg2 : STD_LOGIC;
  signal reset_sync_reg3 : STD_LOGIC;
  signal reset_sync_reg4 : STD_LOGIC;
  signal reset_sync_reg5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of reset_sync1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of reset_sync1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of reset_sync1 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of reset_sync1 : label is "VCC:CE";
  attribute box_type : string;
  attribute box_type of reset_sync1 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync2 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync2 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync2 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync2 : label is "VCC:CE";
  attribute box_type of reset_sync2 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync3 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync3 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync3 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync3 : label is "VCC:CE";
  attribute box_type of reset_sync3 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync4 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync4 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync4 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync4 : label is "VCC:CE";
  attribute box_type of reset_sync4 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync5 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync5 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync5 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync5 : label is "VCC:CE";
  attribute box_type of reset_sync5 : label is "PRIMITIVE";
  attribute ASYNC_REG of reset_sync6 : label is std.standard.true;
  attribute SHREG_EXTRACT of reset_sync6 : label is "no";
  attribute XILINX_LEGACY_PRIM of reset_sync6 : label is "FDP";
  attribute XILINX_TRANSFORM_PINMAP of reset_sync6 : label is "VCC:CE";
  attribute box_type of reset_sync6 : label is "PRIMITIVE";
begin
reset_sync1: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg1
    );
reset_sync2: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg1,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg2
    );
reset_sync3: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg2,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg3
    );
reset_sync4: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg3,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg4
    );
reset_sync5: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg4,
      PRE => reset_sync5_0(0),
      Q => reset_sync_reg5
    );
reset_sync6: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset_sync_reg5,
      PRE => '0',
      Q => reset_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_reset_wtd_timer is
  port (
    reset : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_reset_wtd_timer;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_reset_wtd_timer is
  signal \counter_stg1[5]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg1[5]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg1_reg : STD_LOGIC_VECTOR ( 5 to 5 );
  signal \counter_stg1_reg__0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \counter_stg2[0]_i_3_n_0\ : STD_LOGIC;
  signal counter_stg2_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg2_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg2_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal counter_stg30 : STD_LOGIC;
  signal \counter_stg3[0]_i_3_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_4_n_0\ : STD_LOGIC;
  signal \counter_stg3[0]_i_5_n_0\ : STD_LOGIC;
  signal counter_stg3_reg : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \counter_stg3_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \counter_stg3_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal eqOp : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal reset0 : STD_LOGIC;
  signal reset_i_2_n_0 : STD_LOGIC;
  signal reset_i_3_n_0 : STD_LOGIC;
  signal reset_i_4_n_0 : STD_LOGIC;
  signal reset_i_5_n_0 : STD_LOGIC;
  signal reset_i_6_n_0 : STD_LOGIC;
  signal \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \counter_stg1[0]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[1]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \counter_stg1[2]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[3]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \counter_stg1[4]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \counter_stg1[5]_i_3\ : label is "soft_lutpair71";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg2_reg[8]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \counter_stg3_reg[8]_i_1\ : label is 11;
begin
\counter_stg1[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      O => plusOp(0)
    );
\counter_stg1[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \counter_stg1_reg__0\(0),
      I1 => \counter_stg1_reg__0\(1),
      O => plusOp(1)
    );
\counter_stg1[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \counter_stg1_reg__0\(1),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(2),
      O => plusOp(2)
    );
\counter_stg1[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \counter_stg1_reg__0\(2),
      I1 => \counter_stg1_reg__0\(0),
      I2 => \counter_stg1_reg__0\(1),
      I3 => \counter_stg1_reg__0\(3),
      O => plusOp(3)
    );
\counter_stg1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => plusOp(4)
    );
\counter_stg1[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF2000"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      I3 => \counter_stg1[5]_i_3_n_0\,
      I4 => data_out,
      O => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => plusOp(5)
    );
\counter_stg1[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(3),
      I1 => \counter_stg1_reg__0\(1),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(2),
      I4 => \counter_stg1_reg__0\(4),
      O => \counter_stg1[5]_i_3_n_0\
    );
\counter_stg1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(0),
      Q => \counter_stg1_reg__0\(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(1),
      Q => \counter_stg1_reg__0\(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(2),
      Q => \counter_stg1_reg__0\(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(3),
      Q => \counter_stg1_reg__0\(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(4),
      Q => \counter_stg1_reg__0\(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => plusOp(5),
      Q => counter_stg1_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \counter_stg1_reg__0\(4),
      I1 => \counter_stg1_reg__0\(2),
      I2 => \counter_stg1_reg__0\(0),
      I3 => \counter_stg1_reg__0\(1),
      I4 => \counter_stg1_reg__0\(3),
      I5 => counter_stg1_reg(5),
      O => eqOp
    );
\counter_stg2[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg2_reg(0),
      O => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_7\,
      Q => counter_stg2_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg2_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg2_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg2_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg2_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg2_reg[0]_i_2_n_4\,
      O(2) => \counter_stg2_reg[0]_i_2_n_5\,
      O(1) => \counter_stg2_reg[0]_i_2_n_6\,
      O(0) => \counter_stg2_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg2_reg(3 downto 1),
      S(0) => \counter_stg2[0]_i_3_n_0\
    );
\counter_stg2_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_5\,
      Q => counter_stg2_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_4\,
      Q => counter_stg2_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_6\,
      Q => counter_stg2_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_5\,
      Q => counter_stg2_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[0]_i_2_n_4\,
      Q => counter_stg2_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_7\,
      Q => counter_stg2_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg2_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg2_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[4]_i_1_n_4\,
      O(2) => \counter_stg2_reg[4]_i_1_n_5\,
      O(1) => \counter_stg2_reg[4]_i_1_n_6\,
      O(0) => \counter_stg2_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(7 downto 4)
    );
\counter_stg2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_6\,
      Q => counter_stg2_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_5\,
      Q => counter_stg2_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[4]_i_1_n_4\,
      Q => counter_stg2_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_7\,
      Q => counter_stg2_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg2_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg2_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg2_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg2_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg2_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg2_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg2_reg[8]_i_1_n_4\,
      O(2) => \counter_stg2_reg[8]_i_1_n_5\,
      O(1) => \counter_stg2_reg[8]_i_1_n_6\,
      O(0) => \counter_stg2_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg2_reg(11 downto 8)
    );
\counter_stg2_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => eqOp,
      D => \counter_stg2_reg[8]_i_1_n_6\,
      Q => counter_stg2_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \counter_stg3[0]_i_3_n_0\,
      I1 => \counter_stg3[0]_i_4_n_0\,
      I2 => counter_stg2_reg(0),
      I3 => \counter_stg1[5]_i_3_n_0\,
      O => counter_stg30
    );
\counter_stg3[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => \counter_stg3[0]_i_3_n_0\
    );
\counter_stg3[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counter_stg2_reg(9),
      I1 => counter_stg2_reg(10),
      I2 => counter_stg2_reg(7),
      I3 => counter_stg2_reg(8),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => \counter_stg3[0]_i_4_n_0\
    );
\counter_stg3[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_stg3_reg(0),
      O => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_7\,
      Q => counter_stg3_reg(0),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counter_stg3_reg[0]_i_2_n_0\,
      CO(2) => \counter_stg3_reg[0]_i_2_n_1\,
      CO(1) => \counter_stg3_reg[0]_i_2_n_2\,
      CO(0) => \counter_stg3_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counter_stg3_reg[0]_i_2_n_4\,
      O(2) => \counter_stg3_reg[0]_i_2_n_5\,
      O(1) => \counter_stg3_reg[0]_i_2_n_6\,
      O(0) => \counter_stg3_reg[0]_i_2_n_7\,
      S(3 downto 1) => counter_stg3_reg(3 downto 1),
      S(0) => \counter_stg3[0]_i_5_n_0\
    );
\counter_stg3_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_5\,
      Q => counter_stg3_reg(10),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_4\,
      Q => counter_stg3_reg(11),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_6\,
      Q => counter_stg3_reg(1),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_5\,
      Q => counter_stg3_reg(2),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[0]_i_2_n_4\,
      Q => counter_stg3_reg(3),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_7\,
      Q => counter_stg3_reg(4),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[0]_i_2_n_0\,
      CO(3) => \counter_stg3_reg[4]_i_1_n_0\,
      CO(2) => \counter_stg3_reg[4]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[4]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[4]_i_1_n_4\,
      O(2) => \counter_stg3_reg[4]_i_1_n_5\,
      O(1) => \counter_stg3_reg[4]_i_1_n_6\,
      O(0) => \counter_stg3_reg[4]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(7 downto 4)
    );
\counter_stg3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_6\,
      Q => counter_stg3_reg(5),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_5\,
      Q => counter_stg3_reg(6),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[4]_i_1_n_4\,
      Q => counter_stg3_reg(7),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_7\,
      Q => counter_stg3_reg(8),
      R => \counter_stg1[5]_i_1_n_0\
    );
\counter_stg3_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counter_stg3_reg[4]_i_1_n_0\,
      CO(3) => \NLW_counter_stg3_reg[8]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \counter_stg3_reg[8]_i_1_n_1\,
      CO(1) => \counter_stg3_reg[8]_i_1_n_2\,
      CO(0) => \counter_stg3_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counter_stg3_reg[8]_i_1_n_4\,
      O(2) => \counter_stg3_reg[8]_i_1_n_5\,
      O(1) => \counter_stg3_reg[8]_i_1_n_6\,
      O(0) => \counter_stg3_reg[8]_i_1_n_7\,
      S(3 downto 0) => counter_stg3_reg(11 downto 8)
    );
\counter_stg3_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => counter_stg30,
      D => \counter_stg3_reg[8]_i_1_n_6\,
      Q => counter_stg3_reg(9),
      R => \counter_stg1[5]_i_1_n_0\
    );
reset_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => reset_i_2_n_0,
      I1 => counter_stg3_reg(0),
      I2 => reset_i_3_n_0,
      O => reset0
    );
reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001000000000"
    )
        port map (
      I0 => counter_stg3_reg(9),
      I1 => counter_stg3_reg(10),
      I2 => counter_stg3_reg(7),
      I3 => counter_stg3_reg(8),
      I4 => counter_stg2_reg(0),
      I5 => counter_stg3_reg(11),
      O => reset_i_2_n_0
    );
reset_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => reset_i_4_n_0,
      I1 => reset_i_5_n_0,
      I2 => reset_i_6_n_0,
      O => reset_i_3_n_0
    );
reset_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => counter_stg2_reg(3),
      I1 => counter_stg2_reg(4),
      I2 => counter_stg2_reg(1),
      I3 => counter_stg2_reg(2),
      I4 => counter_stg2_reg(6),
      I5 => counter_stg2_reg(5),
      O => reset_i_4_n_0
    );
reset_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => counter_stg2_reg(10),
      I1 => counter_stg2_reg(9),
      I2 => counter_stg2_reg(8),
      I3 => counter_stg2_reg(7),
      I4 => counter_stg1_reg(5),
      I5 => counter_stg2_reg(11),
      O => reset_i_5_n_0
    );
reset_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => counter_stg3_reg(4),
      I1 => counter_stg3_reg(3),
      I2 => counter_stg3_reg(1),
      I3 => counter_stg3_reg(2),
      I4 => counter_stg3_reg(6),
      I5 => counter_stg3_reg(5),
      O => reset_i_6_n_0
    );
reset_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => reset0,
      Q => reset,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_resets is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    reset : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_resets;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_resets is
  signal pma_reset_pipe : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute async_reg : string;
  attribute async_reg of pma_reset_pipe : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \pma_reset_pipe_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[1]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[2]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[2]\ : label is "yes";
  attribute ASYNC_REG_boolean of \pma_reset_pipe_reg[3]\ : label is std.standard.true;
  attribute KEEP of \pma_reset_pipe_reg[3]\ : label is "yes";
begin
  \out\(0) <= pma_reset_pipe(3);
\pma_reset_pipe_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => '0',
      PRE => reset,
      Q => pma_reset_pipe(0)
    );
\pma_reset_pipe_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(0),
      PRE => reset,
      Q => pma_reset_pipe(1)
    );
\pma_reset_pipe_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(1),
      PRE => reset,
      Q => pma_reset_pipe(2)
    );
\pma_reset_pipe_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pma_reset_pipe(2),
      PRE => reset,
      Q => pma_reset_pipe(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_sync_block;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_0 is
  port (
    resetdone : out STD_LOGIC;
    resetdone_0 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_0 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_0;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_0 is
  signal data_out : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
resetdone_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => data_out,
      I1 => resetdone_0,
      O => resetdone
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_10 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_10 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_10;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_10 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_11 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    rxresetdone_s3 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_11 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_11;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_11 is
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"008F0080"
    )
        port map (
      I0 => Q(0),
      I1 => rxresetdone_s3,
      I2 => Q(1),
      I3 => Q(2),
      I4 => cplllock_sync,
      O => \FSM_sequential_rx_state_reg[1]\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_12 is
  port (
    \FSM_sequential_rx_state_reg[1]\ : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg : in STD_LOGIC;
    reset_time_out_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    reset_time_out_reg_2 : in STD_LOGIC;
    data_in : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[1]_0\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_0 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_1 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_1\ : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_2\ : in STD_LOGIC;
    time_out_wait_bypass_s3 : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[3]\ : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_3\ : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_2 : in STD_LOGIC;
    rx_fsm_reset_done_int_reg_3 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_12 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_12;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_12 is
  signal \FSM_sequential_rx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal data_valid_sync : STD_LOGIC;
  signal reset_time_out_i_2_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_3_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_4_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[0]_i_3\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_8\ : label is "soft_lutpair41";
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_rx_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEFEFEF"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_2\,
      I1 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(3),
      O => D(0)
    );
\FSM_sequential_rx_state[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => Q(3),
      I1 => reset_time_out_reg_2,
      I2 => data_valid_sync,
      I3 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[0]_i_3_n_0\
    );
\FSM_sequential_rx_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF24200400"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(3),
      I3 => Q(2),
      I4 => \FSM_sequential_rx_state[1]_i_2_n_0\,
      I5 => \FSM_sequential_rx_state_reg[1]_0\,
      O => D(1)
    );
\FSM_sequential_rx_state[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => data_valid_sync,
      I1 => rx_fsm_reset_done_int_reg_1,
      O => \FSM_sequential_rx_state[1]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]\,
      I1 => \FSM_sequential_rx_state[3]_i_4_n_0\,
      I2 => Q(0),
      I3 => reset_time_out_reg,
      I4 => \FSM_sequential_rx_state[3]_i_6_n_0\,
      I5 => \FSM_sequential_rx_state_reg[0]_0\,
      O => E(0)
    );
\FSM_sequential_rx_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFCCC0C4C4"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => Q(3),
      I2 => Q(1),
      I3 => \FSM_sequential_rx_state[3]_i_8_n_0\,
      I4 => Q(0),
      I5 => \FSM_sequential_rx_state_reg[3]\,
      O => D(2)
    );
\FSM_sequential_rx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAEFEA"
    )
        port map (
      I0 => \FSM_sequential_rx_state[0]_i_3_n_0\,
      I1 => \FSM_sequential_rx_state_reg[0]_1\,
      I2 => Q(2),
      I3 => \FSM_sequential_rx_state_reg[0]_3\,
      I4 => Q(0),
      I5 => Q(1),
      O => \FSM_sequential_rx_state[3]_i_4_n_0\
    );
\FSM_sequential_rx_state[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0CE20CCC"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => Q(3),
      I2 => data_valid_sync,
      I3 => Q(1),
      I4 => Q(0),
      O => \FSM_sequential_rx_state[3]_i_6_n_0\
    );
\FSM_sequential_rx_state[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_1,
      I1 => data_valid_sync,
      I2 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state[3]_i_8_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_out,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_valid_sync,
      R => '0'
    );
\reset_time_out_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEFFFFFEEEF0000"
    )
        port map (
      I0 => reset_time_out_i_2_n_0,
      I1 => reset_time_out_reg,
      I2 => reset_time_out_reg_0,
      I3 => Q(1),
      I4 => reset_time_out_reg_1,
      I5 => reset_time_out_reg_2,
      O => \FSM_sequential_rx_state_reg[1]\
    );
reset_time_out_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FF30E0E0FF30202"
    )
        port map (
      I0 => \FSM_sequential_rx_state_reg[0]_1\,
      I1 => Q(0),
      I2 => Q(1),
      I3 => data_valid_sync,
      I4 => Q(3),
      I5 => mmcm_lock_reclocked,
      O => reset_time_out_i_2_n_0
    );
rx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABA8"
    )
        port map (
      I0 => rx_fsm_reset_done_int,
      I1 => rx_fsm_reset_done_int_i_3_n_0,
      I2 => rx_fsm_reset_done_int_i_4_n_0,
      I3 => data_in,
      O => rx_fsm_reset_done_int_reg
    );
rx_fsm_reset_done_int_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00040000"
    )
        port map (
      I0 => Q(0),
      I1 => data_valid_sync,
      I2 => Q(2),
      I3 => reset_time_out_reg_2,
      I4 => rx_fsm_reset_done_int_reg_2,
      O => rx_fsm_reset_done_int
    );
rx_fsm_reset_done_int_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400040004040400"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_0,
      I1 => Q(3),
      I2 => Q(2),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_1,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_3_n_0
    );
rx_fsm_reset_done_int_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000808080008"
    )
        port map (
      I0 => rx_fsm_reset_done_int_reg_3,
      I1 => Q(1),
      I2 => Q(0),
      I3 => data_valid_sync,
      I4 => rx_fsm_reset_done_int_reg_2,
      I5 => reset_time_out_reg_2,
      O => rx_fsm_reset_done_int_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_13 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_13 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_13;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_13 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_14 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_14 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_14;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_14 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_15 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_15 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_15;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_15 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_16 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_16 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_16;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_16 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_3 is
  port (
    data_out : out STD_LOGIC;
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_3 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_3;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_3 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => status_vector(0),
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_4 is
  port (
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_4 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_4;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_4 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_5 is
  port (
    reset_time_out_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_time_out_reg_0 : in STD_LOGIC;
    reset_time_out : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_1\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_3\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    reset_time_out_reg_1 : in STD_LOGIC;
    mmcm_lock_reclocked : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_4\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_5\ : in STD_LOGIC;
    \FSM_sequential_tx_state_reg[0]_6\ : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_5 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_5;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_5 is
  signal \FSM_sequential_tx_state[3]_i_5_n_0\ : STD_LOGIC;
  signal cplllock_sync : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  signal \reset_time_out_i_3__0_n_0\ : STD_LOGIC;
  signal \reset_time_out_i_4__0_n_0\ : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
\FSM_sequential_tx_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]\,
      I1 => \FSM_sequential_tx_state_reg[0]_0\,
      I2 => \FSM_sequential_tx_state[3]_i_5_n_0\,
      I3 => \FSM_sequential_tx_state_reg[0]_1\,
      I4 => \FSM_sequential_tx_state_reg[0]_2\,
      I5 => \FSM_sequential_tx_state_reg[0]_3\,
      O => E(0)
    );
\FSM_sequential_tx_state[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000F00008"
    )
        port map (
      I0 => \FSM_sequential_tx_state_reg[0]_4\,
      I1 => \FSM_sequential_tx_state_reg[0]_5\,
      I2 => cplllock_sync,
      I3 => Q(2),
      I4 => Q(1),
      I5 => \FSM_sequential_tx_state_reg[0]_6\,
      O => \FSM_sequential_tx_state[3]_i_5_n_0\
    );
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => cplllock_sync,
      R => '0'
    );
reset_time_out_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFE0"
    )
        port map (
      I0 => reset_time_out_reg_0,
      I1 => \reset_time_out_i_3__0_n_0\,
      I2 => \reset_time_out_i_4__0_n_0\,
      I3 => reset_time_out,
      O => reset_time_out_reg
    );
\reset_time_out_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020002000F000200"
    )
        port map (
      I0 => cplllock_sync,
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(0),
      I4 => mmcm_lock_reclocked,
      I5 => Q(1),
      O => \reset_time_out_i_3__0_n_0\
    );
\reset_time_out_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0505FF040505F504"
    )
        port map (
      I0 => Q(1),
      I1 => reset_time_out_reg_1,
      I2 => Q(2),
      I3 => Q(0),
      I4 => Q(3),
      I5 => cplllock_sync,
      O => \reset_time_out_i_4__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_6 is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_out : out STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_6 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_6;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_6 is
  signal \^data_out\ : STD_LOGIC;
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
  data_out <= \^data_out\;
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync_reg1_0,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => \^data_out\,
      R => '0'
    );
\mmcm_lock_count[7]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_out\,
      O => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_7 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg6_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_7 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_7;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_7 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_8 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_8 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_8;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_8 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_sync_block_9 is
  port (
    data_out : out STD_LOGIC;
    data_in : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of gig_ethernet_pcs_pma_0_sync_block_9 : entity is "gig_ethernet_pcs_pma_0_sync_block";
end gig_ethernet_pcs_pma_0_sync_block_9;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_sync_block_9 is
  signal data_sync1 : STD_LOGIC;
  signal data_sync2 : STD_LOGIC;
  signal data_sync3 : STD_LOGIC;
  signal data_sync4 : STD_LOGIC;
  signal data_sync5 : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of data_sync_reg1 : label is std.standard.true;
  attribute SHREG_EXTRACT : string;
  attribute SHREG_EXTRACT of data_sync_reg1 : label is "no";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_sync_reg1 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP : string;
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg1 : label is "VCC:CE GND:R";
  attribute box_type : string;
  attribute box_type of data_sync_reg1 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg2 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg2 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg2 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg2 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg2 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg3 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg3 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg3 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg3 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg3 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg4 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg4 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg4 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg4 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg4 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg5 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg5 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg5 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg5 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg5 : label is "PRIMITIVE";
  attribute ASYNC_REG of data_sync_reg6 : label is std.standard.true;
  attribute SHREG_EXTRACT of data_sync_reg6 : label is "no";
  attribute XILINX_LEGACY_PRIM of data_sync_reg6 : label is "FD";
  attribute XILINX_TRANSFORM_PINMAP of data_sync_reg6 : label is "VCC:CE GND:R";
  attribute box_type of data_sync_reg6 : label is "PRIMITIVE";
begin
data_sync_reg1: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_in,
      Q => data_sync1,
      R => '0'
    );
data_sync_reg2: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync1,
      Q => data_sync2,
      R => '0'
    );
data_sync_reg3: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync2,
      Q => data_sync3,
      R => '0'
    );
data_sync_reg4: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync3,
      Q => data_sync4,
      R => '0'
    );
data_sync_reg5: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync4,
      Q => data_sync5,
      R => '0'
    );
data_sync_reg6: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1_0,
      CE => '1',
      D => data_sync5,
      Q => data_out,
      R => '0'
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
afmoZr9skqIZsiBY7liE8HhN5PT9cSTlE7b7FkfYEnmAz8/sYewHAx0YfvnPtVZDeaUQ1+SA/YSK
81BVyxiW6Q1+w7v5E1h+YJTA02rq79x+8OQbPa0mLIjdXTtNmx+BnF2Qrq3QGTPNoSFginwNNM6R
DHZuBkJnITzBkDYC/to=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
oFhR7iEHB5cYBaPdDcFYa+fK3Gj/00Z1GiQ4ZVkb1MGu+Q6dRi7Lgvhbwd39WT10uvi5m95gOjlX
gGa0fqW2XuRf5YGzXsxqVh0xVmshpf7o2GLRr2CmqpGBtEUVv4LbXJ4WiaEo9wVMVr2rBg6MtuPB
CQBdJcTka6nWZivdNblYGd53J9DObofIqJ9W/NUzFP/DmzVmGbctoa4amp6KUecoKxnO4tESoQzm
lz7s+Zslv3tCv5wVOwV3SAR+Y5ul9hQX8OljyF82Z31ipd+bzojhPg9RhmrvIMbFqzM9eU82pEIT
IZzSVKYMd1P6t1qORfwdgodKT0ZApoyFVxfVeQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
FJ5kCtzgKdagetGF6bdHAPKIu2cq7BcnQFRTWMN8j6rUq4ZaiydIPtRo6ybDhCzgm0ZRPth8Lgd5
+HXk3g/SdsTLZoOUuXnEz+TkfRJdu8kdJ+0y8sJMNK21IEW3pSWsj/n4toksSjLJ1wENuoq4XqDK
9VQo0Gobmx7OOqOYFuY=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
d/VWmC3TdLGZzbtN4vTxv6kYGkcDx9TNbvTz1ALcTDtGFLEUmm0Z0/puE1NDYgBTJquGDZ+j8d2H
/il9HfJ/2n/hTxPKUXm83KvLOZ3eaRdjaEk1wHy+UsjHJ1VztyOuT/nrMaT3sUq1kpP7wkC5I2EK
lQZbp4ngkuiSjWCad10Yoj0TX6DfE58qXjaybvWsINo7H6gMzi4JvYHWbBnCYQS/RkvaQvSsne0i
YvfJuYy63HVFs+Afh6hOO34nbqzXvlsXI7SIicsCUu21AgLUNio4IsL+Uj92NEPjaJnw2wTXrMuH
A1a3i03K7vrZ94AIVwtSXRwfN4q33BWP9bjYdQ==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3xYA7hcw+q7P0QSxtus9heaAij7kiNzMgPuCXFQTIhWPWN0atyWMkDqoxytTvKyCd0IM0n0DheIN
XngnbZmTQpitsZh5f/e8iD/+iWxl+7S7I6wyIivHhUA/yP87+KG/vFGPXu7WexVyPmZuOgSS7sS/
Fnk+QS1l3qltq84DH59g9EBTV7YkcgLEdICkCR0tgP3K8aSVjCptgXzA0onYGrkQESEdw8nPfEvR
g/5lKhHOmmE6kTh/XfO6eQpuQBKOlD7RaVWrPxnTkM9lxrjRZZb3OSfaBLMKhMGrOdEY4RIqfHMG
MBOH8gAUoIRrgyLztGTWc+RsxUUYHq9Hwixqkg==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RyOZOz+qJ5WH/47JnOQuMFPCUAyXetmhoeJiNflULv2xv5JJeXWSOURz6kOeJ5/Jbz2KA/psbOVw
rwZKAlTJoEVMq47lcrgzkIZPdllrnwbNl8hiziEb/YEq2vvpC9EJzY7LlRbQINiQY3hAzP5B/s+N
lYRpAUjOhEst2xZEiFz6cr7zAMz2jn7lHWYuvu75NGUv8JJ7PZ6vUISUeHOPW9ljH0TH4CnOaTpM
Mey1hosrMsp78mwEmy3l4n56Khrq2FOUzGZbi5lhqEebDEU/90LVUpUSzOkhJ03OfvAgBuj9ejqz
WgZRD6DpCk8BVUByKAGJqQZsJ0NcEnE8/btwhQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Q98gl70eQ6J1AYIClxRNKl48Tz7BoxVypGMCmvUy8HqLl3jjFsIYy9amcj+/0Qts3PcCHOutO4e+
ptR4cjDneD+41DiKHcGc2sgfdKq4hG60mda13C9qtjKCuBhg+Zg1jHi3TOh4bVtsLuydPMwaILNj
7ORZo/jzBQ4OUPNFy2tkOMJArdeTOIuydxhUFtpy6FAABlc1SE1Mer24GTLYw0pJtOOJOX61uJuc
ghuBuHaXV8kExCP3IU1qx/oyfIpY9TQiqLKEf0YUrmEzBzV+JXQqn3Jom9PXTay5YJ+PqraK9iFt
ai1YIDay5F0ncLDXiT7OMRFjFG2mL+C96DFEmg==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
DgXXbyNB3XMOH/gmmWb6uBYjn7XQuHgaTP3eav9Y+qTkta/ItNcuqONFgEZl6duKUkQI/YVDFLf5
JwZHmI7U9IACSpdRAsITpVXnabTJhDmcKIxi5anNhIeSW9RerBcxlMYtuoI5FgWJKz6h+0yZmes6
xYMyXTT3gJS5ttPc1n6kBHNZpxPgl/Yv979A0/ZxYkRTfuWrCyaS5T08AajGwOyC+2sH4FjL2xLX
uR9a04wQespOimJ+InC0FAfYhqUcjs5b18x0hSfPhe4G0xKNvmpukNOhIflD4bBgKXl0kqXnpatt
7ejKBpSWKRjk/M7rYOxErj8EahuRFH07pBR9b3XyXF3MfQBwAzV2GIz+A/XzIUDQOcBbojfGwrVA
2HhHfn8sKZB5ktpwF8MBjdlwrtn+kdQ8ux0ZNrgndrvw4nS4TWxZtQeRc+wfb7Q/lIzw6Pc+Ifnn
b7UJlJvNr5ZF2XNxuHGz5L19UYgB0gpH8xWvYpn0Od797z3FfYIiGMNP

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
YW19UgddTwwnJVcciGeEKvH9ZuBWuoo4LgtOhQvaoLIGVBAQAVfk5VY0pzvluZUn2+81fwbTPSLA
O+/RYs39A761jlViEBPYfmojlgjyiOTK9aHXNLsD3WBxfTXGOS3fHhhwgCmiZ0OncriPRHcNAK7N
4cyj3anKb5y4g9gzt9YeJ7T/zkLQuzT4zqnb9ijTisbH6HxpAVXI1ydKtGzVETw+s55Jb0gVXyGi
Q1hVe4oSDBTETiD85J2/A1KF+2DHKkzOJhr53qxvjND2l/LqsA0G/4J35IdLcmuYWAoCJS7cHkTk
wnGOjEnNbP+gboLu5UrXAFwXCo+jsZLVqIzGqg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
58apVjNnALRGpDI2bjJhSWJPQlj57Kat42daGg5JiM8yXDkjxm/BmzBRp9oBmOurwCnZOZcKlj3m
HlexbUqO32l40m0mMLkLzlGYPsduAUijMIb7DMl5HUf9XunE4fC69CyB0KyecsJZI9Ru4XxZrniR
CYqqjLtNjfFEppiRGRN6bXqv2T5Vgbh9qrMDIrkZnrIUB7RBd6xTPZ0xU1V6/f7Oftp0kyRv3C0z
HF7RFTnZ4y90YOsvuCM5CoAyyE/0gTXvBlB4icz61XmHsIlwfUu/xC6yAitX3dikrJ3DWeMC5Soe
vAifs/1pf/61AlzL055eu8EpDBQRH32ikamDmA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GkhzmJz9o93IJx+1EnrQfOwa/OOrgHQT2+XhwLsfJNRk9bxhLz472g/aTSOruRXBYVSa7twJJ6+r
lf0nvm4VQfQ9bG+fbYjOsryrSSKlth3/Bq54UcBz0wLrrf59Jt2KtKCOUQojHAIinG8CLxMBgQJR
wqwzAYtmffGsCYbobk0Y4IPkjQhtQamQd1BTI8ZwvF5zPN8kBYuoFLMFqwet6s+kcH/tQSrB0o/K
lHILn0b8uovehAHNWllP4phB4MeAOgMZbbsniJgqPQEhSd4nOUZs4WnWtYOsf+rCLliHTc9c6HIS
0u5f7mdvO+uC8Pi/nwNFr026nnYUX3YcG4zdBQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 119328)
`protect data_block
2zMXQCRGNp+uZFJJPkiE2aqxfeY9DhdXjfsxhz9PIMgAOcJlBEdR1D/8K8l/cUVGuhT2j6KLKWRa
wBEa1q+YhT4qyfvB0dv3bk2ZpFnE/0EOj9UkKSOfn1uGNrOZslDfmF5IzQkH6XkXT9veLUCXudh7
49p9o7aZgUCD0I7H+amW6kZjGAkt3YYSTLx7rKDtdMJOax0iC+1uGR6iBDwNka2lwDfOi98Jf/ZN
JzlUXjdLdzTUlyyuxjPml0z36fKpVjsQPcFe7dq3aczK4mFT7twzLW3ekSdOJZCRrYc/vPgBdU8z
NjSfpPZ6kWdj/IlEfuY+/5f53bQ26ugnJXuBvSG7Bl2ZL8IyzMh6Imu7I//+M+FHqyormN0W6CVq
KIidzmGQY48+p8MLsRSSlvyrf1hOED4+86uRhSxtPz8b5J9M1c97Y6Xw85+sk65gnvjXYFdEcMdW
2vF6o7liWkABToKlji4uFHB377d9Eh05jGfd3WuFonv6vBW18Ij/WRyrL/EYb4PRU+X87zXICKSa
qIwtFttSd16XVMuqSTGyKqQfk1zeoDJg3MgnF+TvId3CnLTOQkCAjzo5DvM7H+EBOLT/DrvuEarN
NXJb3tupcPfrod+zD/IkLEM7d+JYGhVYX6WgDT/gFtmisL/3AFeq/E+Lq6GtnhYEWVrjiRYIBm/7
M0oWBHRsPH2AeollvpIDPkya1syPMktrM/gkXT84Y9zNs18sC8tW1Cbyha1ofr65GVCAd0IGXhLD
ZpWT25QTckYPRKAqnNSXE1JGMyG56nK8soXRLFBgwVQ0FHwbHD2Y+k34Kc9Te5S649HXIHOnq9su
TpKFV/oYodOpyfDpk/XrpJ8H2i+k1lcUyqg2G2QDx6E7rzD8IoLmwXNFRbSHSevUvbshLiQFlo7Z
EA+cn+Fe8MKGrLoufr+awiWQat9gCzWbDo3XXUuFSkW/K6DpCr2szVI822iuC0Gr/iPpBsbW3Jw6
0GRKOMd2M/81+T9tbDCzdEkwAEpBKbYoNisVzGELaj15MSTzqH43C0kjDdorAVVCWO6VHUP/S5BO
sP55n+fuf8vJrAsXUmR3f/HUgrLqbqwMHB4dlBAnAxt81H3DADi90KlCKlwoMuOroBaQDIp6k7cp
kcia6NQm0rwgZ9Tg1gMngOigWeyKh0zjERgmgIB7WrrMiDZGbyNKwsbQiBnTeiYpC6Ld3qIpbD5a
ZoEMjNF1Gg4uOXgMkITf5Ruvj4svtJMg0Z4GUC2Aa7s42IPQS7gNqYL2QradLHIOFXNLRKWYD+Zc
9A6m6lE5+rNSydAYOYPM8uRJoJv9YDkh7UaKta2v4Di0U6nyylNDO8ZZ+6t+U7MQaei1/utVPx/H
9ZRrDiZl4dVBy6OtVLBjvIs/ujJfPsLD/HrP9XgheHBP+qswY14soWIn3XOxNmF873CmpNcUGCq/
GELi8hj3TSmfuTDPPCLik/dCfxEPJZ1N0rRNEOn1fmTrUOErvrV7BZQcrozIh2Zsm5NFX0jGg6NW
3vdkqfQx0seDUyQj+t3CJx/pHpqnRyB6HggUQaB6hSjvgDWen4QQSX8hv12+GCrN/4oSe5zktv8L
mW/QrHIRJ1BydkTcZaiMXY9OyQOy+YhIwAik17Uo5dwlafx4LbnTJndFeb9Fsfbak7P1RI0BFvOS
FpG2IOxZgoiu+4oAGCC3Dr6HL4o42y8URsYh2ufYDj8JwpFmLlRc4yLCbKOdP+NxeT5FygbKJnjw
rtI6nEcoVOOMuF+NBV0LIm2Uohyt8kSvCYSl+1Kn3+QcChLmG+SwAj2icptsWsoFQ7JEo3eTzNbv
IAy6SvNpHZqhHsaGcKqMzIpfUi4aYN+vihFURrGgUJxofekz9AXXkLMozkhLIIUwQ5Dkfx353njY
QN14kWj7ePgT4oHRRgaphedicmQVn43JvrMmMUfT33CMUlXL3j9JG4+YNKzoOKvWGwv9d4RHXCzj
w9mWv3SOz+CHnuYZqPnHLPKaH9dUxMh0Hqtk5jNqOBL0/6h/Xz1n6x1You3mWEJ+JeTIrSEjm4Ec
w1ebGQm1jE8A4g1CSwrLjbe8i1afyfHl6S3Xs7T14IPVLC554JSHSEav9HUoS7EUzJ7JmSPOX8Wo
luABOvQCZHwS91AJ52JKw67EuWP1S7FTt0pZcWN5rByWmuhIQzbJyQLO5AMh7euWz3MOyeio5/BC
UN6QiJ7gRe8aY9hMpOlHEuAB78QlZU+phu3fnOSmgzHmwufsr3pZnQFo5qx44D5SPWODD00ERtIk
ILx1eAfdD96TcUAbatLOaQCWwZuZH3xZKeip+K62sg2y+J7bLQuXVkDRlC6IvVissxrYKxpRDZTf
Hi97jB5bi4tW+uk3I+kCjc2q8eY08WpX4bbeT9RGb/2xtaphyE5CN5nRKcdn36hITLr13hXYrxgf
gQpqvEqI9ZD1hyFI8hDSofxn6nts5gqidL1xkLeaU7ixfR32HqY7FNX7sYhXJzRKTDSEVaUIurb/
fTA6yIzY5EWfzLXCBgh86i/LQvYI1YbXFU+QY+68BS+3lHP+FG5WXm1xD2xsDR7UlbrIdjfkU/xv
KHYiRMxEJTwqvZepKPokRUK4o+XYth6RlCbmAM0U4i91BlFtJkh//Kvu5vXc+r/2zzxzEkUhfg+e
b/ZuOFuUqjq/lv/27xmj86KIX+xMBsx8cqmRwYvVzI6DKQ6TBCY+CFXy2n2qXwkcf/XCU7yEsD5H
BS3z8x70af0QIH97O/UpTdc+NdJld1Pq7UPoBej899BzbZtrtcy6/1YUEqNTIqdyRkwZT0ILvUxC
LWMgS3W8SKSij7HDvQ2ocSEWhz1g7fJajPL5hDPak/m3HnagBduD3/8wIpsnpbGYmu5x4ZXUUgkj
PGZvHoK7qsuTl0e2fZkC62qo2sUUoK+sUktibAJdWFbvbFlcXc80m35lR5pnLeATlb0jXzYean1B
5UUOCF+fg5ZALePNB+WwyaECsOrRDrArBoSZtdEGqov7Vt7BXrlZYkTamawt8LNHyUPq+V87UmhB
ySAZc4NwJDOdllYaoxZxp9YvgDnUdwK0NcHXf/sdSABLws55ELgfKV0stx5O2CmmRKMOxTlrDfIU
8PUSfCxDYRy0y7Guq+OEx5HefCrO52uKzQcOTYfJigwnLMNfmtVUeop1OlqWscOiWCtTbPzhT6ZW
kn6ekxSk6pNcMD/N+tW6LduhP7v2+z6z8C5u0ixPmZ+2RFgSYYhk346jPt2BS1UpKqHzfU7uKWTp
pSUXWe8zabb1eXKSTPh42OZ23FpHaT1HhgzslsDo8/QFLn/RtOuEbdEwqLOkWZcNdkapdAVTuhJm
DJ9wCV0kSJtNYG9rNdMzOe1+DWfOT1Iun6GOn0xGiqimOCTEalBAncdDGfu2MUjXagBxQPB5aGl+
uMaiAwwdWu9JoluU4lF3X3Mgkcor1x0b5ZiH4fcpLHkTZFcG+khcOoATxozCwg4/8SbqboQAWM7j
38/va/vNRN+gjplGJSZrQYJ3p3Lexy+wlHDrCOSn/LXI2M6uiM0TDKrX+//n9MgK++7B1ozm1aaU
lBv+t7pDuy1lQOpuE1t2EsScBSxPCLkFmOHlJoMg9GcCJ4m44R6FFHsV0cWgq84wAWrKnXzECJZ9
xCX1gDxk6c5PveShpLRF0lSZgyA5rTlFCe6L67NcisXEl3Vnhpfn7EnN5xiB0bKBmwe5t1PFZC2C
z1pEyOwi4IXND/31UdAydH+bcAQaVaS5KJewI6UbVfjTys2AmSNBsV5TW9/r8bzCByIl7aZpYwW+
ctUddEjiMo1zPx9qIPu+pj7f2fHsi+ClB4u3BDg98KESisJgOv+mq/upCLChQXRXjILMtV7VQViu
67zLbBIAiTk7+CTSmThyQOtPJwIR+jpAfcczaKAUIl/HdQXMTukeGK1GIOOgduwDtveSJ6Zy66/u
Thujaxn0Hq4GmcR4BkIxBAFlL2a0Q03awAkwwQY0X8OMTgnOY0cXg16X6cVHM0Q/pfWCDYbQDEPi
5OdIXUMwtsaLSgA5dY75qFfjbPD07w4qf9KmZupgJRVkwsrcPSNg4ylVsyw50cHWAbnircC1rSoH
qWA1/CTC/zxPj7FsOfParsnAmWb9Ra2q1pFzoackonFMuLajKctEku1iUBmiDKG9rtqJ0L7K+XSZ
tKe3nBhn2lsxvLHa5z3m8bVW83mb71VRSzZd645Utu2W0S0+6zuhpJ04WFGEX5/g58Czv2yJfZX7
OZ62is+q67y9RN+GSYFLefBM1GGvqS0nVyBtsvmgFLY5xrxymtXNn+6nuowNW82b/qHfCjqOY/QU
QZ8hHUtRtgvf0s8s6EYX+FgCuNEicpCxMVGLGPREceFbSqryXEF59wycEXqGOff41H+wF2J5pdMP
UnynDZGZShY0nsKZdkbfJYSI3ulIFiKpP0Wl606mCqrTY+cAvuRxvr+a7JFIMKRimJK5Vz1HpSgk
aX+OPGN07kknXmOt7PkSUEhyeiXR1n7VPccxuV/hptGnlkrBcNdz+9ppRCmn7eVPsHZjVpI9C40N
ixIja5y2rBw4wO7WmGzloYbveh5LqEh+WlRc5KhtwICb6K4X4AP5G0LTbzKb2TtD4HrWVq2ZpOiz
hfw6W0jKqzAXVTFEmhJImjDPPMYJ5QUBpMw8SvnJYHTCSNwXqQqad1xE7VIS/h8p8wVwRMXpa7P5
7ZmJXkbbSuYoBp192+F1wRim2P65vfKJbKlTJZEgyC0lVUaQIdgaCIpe03Dg5aNrDaYvNhYBd2LC
yxP4fLB1ZVjLxJt39wBtVRHMHvs8INaMfL5E3N1G1/u2MparQoWkGeBEhUQMHDzsVAcA7Iysvgv/
KBEJjQFhxW/0NxLIfA3/pmN4Bda0AbiookHsPW7KhDkRbuZjgZv6tWbrKhnyOKw5ytVe2byGRLGC
I9hhdh9D+i4mYZXn+W6Cu3YFy+dJeN8RltjP41cTmliygdpvFaOcNieQmEOsCjyyQxUKPs3DaV5J
5ev5bcXMm61aQNrLdnO7viGRqIw/XpgQU8eWDg0Iazsw8WPLrp74Xha+wVoeU/Ph9bv7S2hc6IQw
QNezHjkjP5a4eUdc4+FBdG/I56qtal7KKDQ46KoWgIhGI5jxRIOHd1/tUT1mzF+LyZeXbpTuIqXu
S33Ys7aIFng+cCE2kqJwHdm4sqKaEHCTsDfpb13YPoI2393GJkyEUNMYnTdsrQOdAFe/QmD9ee19
8zqYWZ8i3VGsuYzGR7N0VdBZcqPtkHXpfUY25BPkyUz3BhMUMJWljZgnHda2oXZRvZyMPppJs8qa
WeaXsklCf07j8Dk2DbhMyzWkjujXl1Ksgvou9JI3haY5SrwLB86oTYwoqJKQGKMQ/mnwppzr6MEy
DesQDtAeDqCjIH9I4cwhc8KVt+rduzGY2BFz6+UN2U/sv9INA4FcINvOVZyr4yctTmAi5p2zpy/7
khqwJaeIPG0tkDyxpYUWrQ+jEfEAls13IpzYRmILUmkLRjOd2Xv+wWNztFw0tACdqL0svnegz1e5
8L/jcyRV7e++we5EtPS+OlJKFM+bAGqxOJOzU9JOc2chkSA6v1bboJZzOzFXQ3Rcx5d03KLEhmma
tgA4qwqYmpTH+4hp+I3FXZLzGs4M+bcAJcb/rl+DN7isbWWR2DOLG8TvL2vq0PmGfVB9DKz4PEfo
Y8NBG3SsCNplAixKjt+HUGWSFYn/JjRbwtFeBhkIRKZvQYtsM413qwIjJqb5jrEyusYwqvdnIAZn
Z/l6dq79rsvZKqkrqmEOwyQ/AGMsokU4adZ0qdqObkVIURbXOM805rUK9gNm8hciaXG/shd6T72m
dKgOW+fmZqT1szRHh972kaA1lnGr0spW653z0eI3Nc8EuxvfLR/4jC5zBiXmOJD3idOqwr+fmLUL
GgEPB5hvgsPUB2dV8ko+AQtvN1FG89J6YJgk2CriqjHU/05gljD0b73NJG2TC9xo0z7rI784ITuh
hVb+JmXWQZiB1RnsEnwnDTY6sXTQjbRo0/3sRex3MzxUXoR1akYOTMHM6Rf4Dw+dH+dRHT7Lh7z+
x2l9IMPNOdnAUMhfshQ89hP02BQZ79IOzIF8bneVmHhJ/1gTqyMu68eHTWrg5oj1u9TGEnp92X1K
7V9IxPG3zo78CoDr2+CUvpBKGlUdQVVfhAQBqXbqFIbeDkh+2CydUxscKFWyfl9fmO5DrK5Mf64r
/PNhWsTA6RD9ZVpbJ4L8slkXfmfZsUHwYWcSr6mzTwsFiwoGS/zkxIt6PzyD1L1hTLvNaipg/Ltx
Xi9exF72aMYz3TY+KobnYZDi1PCL/vdgA0lDWpyu1Ay25PzKKmE5MK3GXUF8MhDnFBcKTkwjUQGs
BaECxS+bLzLc+TF/QPR/lvvItGGNM4xt7tyOrJUv8Q6ib88dLULmb308MoSmadpI9Ezd/e2VZXGx
q4hFkW2T0MIRJ+wLbAwWOdj6vDiKJcerOVibxBBnZC5Ww/7oJH/SGFictWRV0bFKY8Htn1NBDEvD
23CfXcutlL7Lozz4jJ4zloW7B76PvUqg8Eww3dgHDq3DM/rT+iQwwV91sgWUL+khm761vDzYFIkv
g0w5MHRQr2yNgi/IOid7NNb+MElMpLIqDaKINIsjmIMbR764+kpM97OTOU2mqJWTAOPf7gvXXRJL
tFvvlWOz7z2NoWRaMcBvG5CiuvICKogJrtXJ0pALop0P1lJraHR4tsYXrLMiLoS2AOZjJyVA1UTC
a2LvANdtkCPkwfzboMc1FQk+OhzFH9273qQrSRpUOoRPpI93FwhXxax4cjnyDq1vFm25Y6OZWZ0n
e4sif/uuDGtUSTp+dWuY78Ib5Vts7Jr/JcaDRO/SF9u9n9lNgcS0XP/qv5tKcL7f+LxAqBsrvXcb
sEfr0DOsdqotyW4XIPxQkE2u5CO58feSHfvFR0IuwOqY6NoOc5FEbPAUlelwqmptahTGh3sR74b5
autiezcmLvk4nWQU1D9jFCvVmLsFNAKGhepc2yhswY38xCRYZypbw13f5pMNaRwNVYgzcf6z6l/I
dWQ3+na8xXj/p3Vxz4p07Kznx7sSsyUw1K5xFMEoNFJdwnGDA6bnwjNhdEKqXT8DWN9GmIa4WxxM
ginPQasMyr5Z/M/v3t5YM8W3RUcg+YWLaiJu7AoEpNstQlh3SDsaHMWAejB4efu7RqmiuIp2JdBU
cBmfBaqdANY88kPWszJ6ZFrTwoTXmR7RxdXGBJwCP15YeNyhv3Hl1/dRB8FutuMRgAvl0x5A7KSf
0q2+z26u81GTF6fo9YZQoNp+e1hGDsmcSYQg22Ckvp9PFJ8YBArOM0PF/d5aMokopBhXaLODegj2
5718DAZBNSQQrVOC1ByJbwfu0Zo/esksDjXCkHzJ92y/nm78FYPyseyQyiDJWb0adbd83Ngy6BCj
lMeaBid0OK3h/BUqlVzePcOFaqxBZpWmwQ4OJ0yX6QkkedmYJqic0Ky/sGqhKgPFhAqB94IHeWDt
ct6+GDSveF/LTvP82b379ceg2ls8zYqzG6w4QC/hnebr8W9p+80Hngbxc87ktwcYkaYJ8sBylpvB
/ZEIsJHXHyPMHI9/HRQYaH3QDf3I7hQlMcP2rHdrd9QL9N1nW9VSM1rkzjlLUh/3qok8Kvnqzzuf
WwvmIMidjMez9dzAH7z4d1LJFhSRO3lzDP2J/8/TI3/deNNDvvK4n2moKBy3ZFU04+jFkbQQQgyZ
Z87yQrwKyKtjcoA6p+exfVU0dYC4lTIPbin6mwTffd9/mzEV6YVgtzEIMgR+CWRXPjQiyZh2B6fm
rvWxjKM1dG22gCRsV1Yl9NKv2qfJ3taCXHi10GcGVjaetcv7kBzedAD/g3HjSU2G0VRaXMVVjDle
GKdrhT+8MTxP6ux8MTF8dZiyz1VihPrbwnAihadwd+ziQv2NPbqWs3MJvCM4e3/iFPaL7lsX/26w
2QuFRiQrcH4NjnN/Ee8U/HhaQbH6Ky/NaOjIvXpDy0dV8HBD2V91u40i715d0BWiwVkJnx8/ChRk
FKispo17cKpHNDSQeSp+M+d3EJwhOacdnVOKHPmoBJzcDsU053pZj/t7NCoY1qrIZDJdfr5oV181
uGsuTuDDC88VuU0wyK5l7A27/pmdZgNAFIie/vKLFKWOolkMuwZsAGkEgD3prtP+eUWdKd72MbrO
qcEOqQaCG6fokdCDyGqNkcqhQNXDFQMtDeU+aoxe9hHeNt1lTOOJuzeSA2Kg96uda2lwx45F67/a
3BCyP7DoBOVdY3SUBb9vblr24EOTq2FejZjUiR/fF7iZ+WMsnwfX9Il9qFzYTZs8r/wTNT9uOvJu
iKcUEhTglb9Z+3tQ0O4pgHxCgH/Qj1KVqfCSZNef2uzc2McyKJX3j4/z5vQc6F1QIHvVjcLSABbP
LivznN+zorDDJt8D1Ah/JlMEjqD2ddKqF1Mol1NJsJGpJwfciOPH85mYhGAXexzpvIdDVd0deSGw
J7kVxQDIZ+Jq1JcaGsoHRqwizSWjZcMZK6bCIBeUEQ1Lvdr31ih7h0tZngC+DrC9G6c5jfPT0gmI
mvjgyVQ2I6Lri7wmo/PDnOEMpcI/dWP0g57LHg180Z5Xd9VqEGwG/PgLywcW6ZubFfxvEcuYO2KT
goke6mn06oF7lx1OcqhnNhTewUkr2rNFJNHKDrVZpGr2LpGCzmOv9Htzy2vaYbexe+NJN7EjXBY/
M4C/DRa5PhTws3ImnyTlSaPJnUIReBdfv7wEICIOefEwQ7SE79GT1faiILzIPuXtRfSJifQmRCzT
rrOSHCwhl/9TwuOFspV3TQzX0IKHRkE6KpoCndpREG7mOs1/yNShS4HBPyhqi4NPULyEXLaevBO/
7RBduGZ9zRqOIORHLnCWUGOlLf9vZIKf5LyX8cOUkrEiDvbLk9nfDDo+zbNKNVmGT8UhhTkTmxLW
UPf9h21uaErn051YtADzpza372aOo4f/PCBFHYNaFXPk3yZGb8z7xLL5WFOTY3nAWf1/iH5IqOzX
exS8E6yIo0xfgRjJgRPjjLAiaH80FWfAbI+TscEGQlq9294eDCyDsyzZWpKmLZKIWeoIldFxtvMn
DdjwBsqVaKSNo/amSAH0oMfOBO9BOcwc3QBTSeIwXs9fOvQ0nbbsyQisbFkH7p08AbPfnUqQAT7k
e/u9c2Wr4n/qpKA9vP5o1qR003E6ULaaja+qoHu9zSEtYOR/bbMiKdCMSEqBVJhYSZmGstfasgsK
WqcvpJAA+idtvWnGSMEQrDRJPi02/YdaEUTT07OvflYIfz1zfvCUpVZVC6pXBpFE90lOYPuownTn
GSqrFaMZeudoGUvXivUqU6TmEjnJTQbes4w8JRd8Iuynh1Vk+uJAJ0Me7DFSnEE1hNrp0uz0y3+l
4nRz0lcMrFfATZYwgTEuAWLT2SYA+JdwP0h9X2AjXjNsJpYSAarQ2HedHzCFQQ869emU1uO3N8Y5
vp/myVSrEdIe9o1SJcNLtKJDTNr9fiwvwlQ5lMSg0Qr69+5XSFFZtWfTFlZjeC6FJJeqCb8Lim4h
cJm9mbA4urU/JBH4kjx7SJsB0fZVwH2LobEJ1QYHRveAbQn2opH8gZRje/NdBVG/7ZVwFR68WkZU
ztp4dCYcqLmrQ5K74au/4sB0Awi9JxotClR7DzVu+Qf9LlfgNo1TN5MHqm0vlt8LtbuHkCYXW5N7
G7UMVcayyRle2gi4jL8yHqhm89aLk18uOeTxCKxeBf9IjbUc02gNf4s3hOWqG28rR5dVeigXrbvK
NqrSsXCZhNgQdG4Mr2Cfx+w13ecKyTwkdRjYtQ9lGd3l5Ds6xXTy8yZXbKlBmUbvvgCEHvIsM//h
UJlydjNofiylJNMvoZ1EFLae78SCaMB7ArNTzRXWyAkYpRuQl/X1mu3VKigN5U4DnnQN89CxKnOC
1fUONYvGKAPzJuZG1pypWvD5irORw9gmU/Dvn3cd2tl+ExVBeUTQ0l7pqhxWmvQUuBoEwOj40Q9W
EC8eb66yYJXhcVxpC2SfLIQSFPZodEAu6arXsJ3nzNRaCFmHBN8p4BaFm33yoJ6HOsqeOzHDYHHT
o61GLpLHuL6xfzY/821dzCvC4XLkPrFpBj+VHinWeHJ0GpI2milcMVnBj2jFyh2NfVWZnNW3skY9
SHHrm9z4ySVX3rKu4cZzMeg5HFIqKJI98F9z/DVLhB0oVxF7/IqB8DxFCsZxEOfQarYHuY/0FBox
u5mKc0Yll818AVOIJ7o91WQyDLBzFny7MIZN/+02FrZ2H2jmTqmqpmOM4cJlW09kmbp2IOdl4oSh
PVvytN6Ta7OArGD7jhTjg/QBUHuP7olW99CBXv69BzHDl/FJNuvhbNrrTM3rZGy7DVIovGuhLtXd
KTiAcbjhNmvPDLDlDtLnRNbSQumlr2ykP5CM2MB3jnU5dprbpb0GKlXKhN1qFk2szzvaoZ6yr916
nxOUDEw7rRmNfKxmaFDuElv/RsuynnahhCRSV2yzRCGXYIy3HYAFtT0KiDC/NOsibElpvuFbQ5Qt
E0uOotoWmpd8bqCWiA9m8vEabuSIbWjCFIfrCj3nvsW68dvpyq7TD7ehzsHdYIqd9ccqWErL1L7i
IqIz1OWAQ0Uc2HmvNhsvEVDvSsMu0iI8M+iEOtkQrpw8VVNIclh0AFpd2ilYn4WwEDuJqDnOrkmU
UDlgFwgKBFV7jMFesbRfhM7YLiGcf2rkyqlOe9JYjcZsOZ3Z6hrZ/EMJ3bawH9V+AWVu2KhjRIEM
7L9Z9BQhVu5FW4zm73xhNXxy4AgtQLV0hsLS2bZWmC1L2FkIXCErC7/jeaNlYMXPLfcmv9JnFa1v
nZG3zpOJVl0RzQXnAXllvjGga1L4lg6nhEBFMr/0wHouVW++WZt66x91iLUEXxQSt5umtdFDC4iv
hd7Vmqx3jpmZfULZgvK9wRHUOwLI7FHMOf1LuGUGaOlZBMcmcCtVRof298M70yAUczzKQYxFEyHM
ENLygqWhiiF5zqxIT/0RhHHoelLal2FOA56l6f6q/d2/ERakzauQ+LhqJ3st0t5ofnHIKil7kJYv
Zc8zQO0bszhbkDgj0yuEq67roOQylJjMykhsSFNUmgP6gqfF1dgY7pBjjws04iRRp5Ej8YWbZ0hX
h6MZTxEdXjYC8Hx6FAWe5zcfILckN+PHsLOtjioe118ySsHyuBHgnoNLKyZCmf0evS1V/JN4/KDR
93IMc9df6+e+3PhuXQHxR5J3MU2dRiTptw6WONAKCC3Wh/u7CXC2GNydNBqeGirRKMGVA88DNrQz
phyyWXwLGVB01iAMsFDqVYNkNfFTL6UI/RKSrThNRNXTX/4OzaEcN8LhThjdIkQsqTx9ucWGE1r5
dBpooyU4aqkS0zfTob00NdJy+x6vKG5N5drk4ZKp0Aaf2hs2H2AchVrClqHr5hJnj0dQNiZ//PQ+
xxYophGQ09O/CZHTauMExLnSpeOukYNUMlXVw90D5YVTtN8EyahApRME+ej7zXZ5y7kST16pbwwU
N4w0rXb0vrr8gdNUO41w/s1df/APvMmedUC2OSoFT1CBgyboaQh69OwXn6i+OonXCEoJD/CS9BOS
NL28wKcABrdtPaE+a4Q+5ANs+DR766U1Mwp+RrqavE58O7iwgEmkL1VIq5CCRSzD3pu7hbHgZsLO
UV0/b3gMAlv2veMZpgQkuNpB1hG4p8ux5ip/q6NpVi8ixQyOBml5LYhNNbyXouvwktaDMHDhSzr0
0TQBAw8uE0f/XeYr8/DSnHEBN9H4mbnGRuOCBGqBn2VZneXdMcSVXy2WHqkhMIVnRGR4Bdc44pN9
86SQMfL/jPxodBdWNGWIqrV/ZMJ4nWc0YcdFgdfFAjrmc4tK3gOSMpD6Ux+ZlEemOn6xKOZzgLBG
ow2H4yzMNaYwpH3KCdtLd8TioNJ9z1eWKazwKBjfJDLzZwcAKnQPpyLRsYL5uoy4OI6/+u8W0+Od
iZk+yyjztqVAT5g+MaQ/V2awNpKwGkwkvZRzIBsIAschSlhX3Iawx7XNLKRI0vdjKc+dlL+AHIhh
OMARDmoToOSWXotVzTeRfwxFV3SV87yCy0lK15P6uBPUVRUgqaFiNLSjENSFR8Up/A/l3O6k0HPK
1E54gG8ZPY3FzBVJVylNJR70rNe4KMW/OYRNU7VSVLr1T5OyE/MnfQUrChTiHZJxLVWIvtr+MuWV
Tp5Yw3YYItQMwTTxhGIRd49EPzNH8NpSqwHGQv0cCxWjG3BL56YNPckj+GCqiY2a9GQuVNdCJmNB
OxRw4aj1dmKY2H4jYvk5fmB/ehvvh2YKD97AJxDYyJ/9bQAXETXCC1KTt9b7W1v19otBtd7tak/w
smk9dDouYOJjdfHo7gsi/2hnedWc2N6FQ/thiecw399opRo3Zz6xZpiRxUPbkkGBgwbUUyFYN9nv
AfQx7HaCidQCmniTHBBMMEJyUn/d9Zxy7pwNY/sGflTF5B9SNNqDZ0U4IDR8dSXMfcKNgS/aVEb+
Nf9LSkTQfCXVpqC2lHUMgBSGIYrwtZ0p1/TBxEJ/pKkoz+GBk+O47IMCqPWr/X7aYMiNurpBpcPr
0oZpnHySI8vtI98IVCejkU2v52a35fRnOeZgmG2gm0BHQ+/ieKWGBkwWxbXGMCmSl29KdUULF7m4
fzfm8QeijB7CmINQORdVL4Lw3fS51lUWs/S7MiRW/x9/lwwjacC9AGP3WSR8oxcLbhla0fB5KxYV
THS/+YInwNxsHOrNxa1RnGqb1NdZXOANfE5MwwNK8MLqoNYx2phF5gNpc/7v0HG3mCdll4RjH+qw
DxW+J4lrbE3ye1eS12cmpUOCtlIpHdi88xbnjkcLqvQBilSRRl2wVFesRHavf6/CmSFmMPiSgD8Z
YY6QPVD5ZlfDJD46FWeLWeQ3udCtQzMcrHrTpLvnbN7+rCgZeGxni4daRFvoZInwq+lWfCmH9D70
XpfMn//aMK/NOXDus74OHiLNJusWslrtr3o9TVbn7cx0yX//3ixsKPl+CuFnIp5ZRd8eaTiN37Gv
lQK0kGcn6BUSYF2q26mm78S3gJEUN362PnH9JxpMRSbmlI/xcw/RWb0bNx85T7lSbouScNpJBmci
XmPvy1JR0HAOu8Ndbk6qpB7T/l39CADgeVsVwo00xdngVRwi9+VC1xae1TqOKc8fn3XBxS6ECKrU
278X8ERuQPacYHFKV6BhcPY5rNP+lwoJYupCgDDq8iRA/UQq1XrvevE9lZgsvQ5uy7wuxr0kXV/l
WPe9B5NeyfXh1sgZ77fOR52g9cZ9kaidrYYCorMcdP/1vKEWPPo25cC1qPV9vj67S3SZ0aWkK4uY
umVrKMwtOLh2HLIPtID+8VuEeDQocvRctGL2MDYeLELB3JY+agvZFVqbM8dK7ak31Zq8VHR2RYGb
kNciCahQA/BptDZn541VSbkN9GUaF/0omyXXNlqlbBjO+/wlQwIw/CAsVp1G5PpLF0asQqQupTT8
t5FvI2MQFmZlsLIzJusrXg+EJCgPMRnXbpFyV6tA1+zkgUNxrLuNjglKz6zIl/0qZiAkfvhcrPVR
4kAxCgOoHSUd90YJzOKrw0VD3nHfAenASqjQ3LwE5bKa9wcrbyEcf3Iv4O8YSah5xWrK+msPgcTG
B+3gfdU+VSwvW9fzJyvl+L+hIKpf1Ur0piZDjFjnwB7CmzcJ5EwXig3kPGnCe27FTeCCunRZXdXs
mQw9aojemQIxuX97pwHW48oXWMIsgtMDfk4CnKGe2NMcxXyCjpf1tCF3eITevP3KrODK9Q2//2gD
V3LQiPqdcVmVA21nXBcJQwnNGZYgRH0N4L+1pOGzbdL/zM2w9YYDfIvywZtSXSsrEEtsE7m9Ft7V
OG3c97LmjSUGzxXz2/5E72PDH1/HBqWjx6m+64XZ+U2BFqGwgtgQb8yCoUNy2mNx1mcQX6ZpskJJ
P0UKo1q5DPGKjx8kcGLFquacnG9PhXp8QO8Kvz5AldYDVbLigG2nbN9okk6i794lNI9RN3FRo0nG
lv4SH+Cmoq7aYDb6CcnHxNWd0I5MpuyFS87Oi6HWt0d0ft1vOVeXVGaVU+b6Lg9KtsBWHmBL0VMd
Y0aWZDit00QGBWC15AWPe4+uHltYtFdRgvhEwZQ2NpQObmeBLVWgpJXM84/tclgQEVstvXk9ZDTz
L/Y3Hm5hI/wkShVdv7UCUubnXyAeOz3hOZZwLrxH3dp04zfxG1iQC6SIu+7ZUJzHGQNOdppoZaY+
q+eYzFJqFEdfdi5/z5+ayEqRkwGRDhSk1mW4r47dKMphB24Kpc4JJOuFSCBnSxeqjKTifvZP8f7c
NCHLK+PSMU5IU4nu+D/YeuxjaoPCdo2znlqPZjVWAmCAlXr9NTJQAP/4zyQKtjxGJX8gTJHqFeNf
pkxNioES4yG/EsYOEtaaVcrSipaJ66S4nKXG0QrQdIRTXnJ+F9DSch9BQLcQnKcVkmGPrDhptWB1
w8GNo4S1O7Df/fmE9SQJnd9nsY+w+15+woHMnxzNODiB3+oeDNW4DQWOa7WMPQN9FDpxQXteVciy
LKcfeUPhb9OUD/CmHHRmEuVH05+qx/wNy+tgEAsipjD1WqfOAwHvNGx8R0c726bkC7B5HOGxHSF3
NV91JXw+HGIo/yDXb0BSP5ZeiZkRvNdM7UsfC7yLnb1k2h+iwuniWU3irx9SQ9E49+xzhLDBiNBZ
9YiHxL5aLoLuxaI47FwXRcgMpVKJ80qKpwFJO2xKdx6H4Sr0yO9OhNLWBRrF7I2dSJynqmRU5IBZ
1nDb6ohm7Of2uDjQYJlDx2qurkz/h7923HPegr058sc4FUxcVYlqDfEaR262SHz4ceYLG9tF1n3T
e6G0U+mF0AqjDRftPT8DnENifNJfvy9c9c5fowJkmBpkaBuU70llEU9Qa9ea34GdKOsAVkA+FuY8
5JrnUa5xW9p6ep6MRNIfzM+eGW5o739G7UawcTKhqlcwEn+m6AuDEknZK6uH7JHRWZgFlE0zEmA1
zc4Cv0F8S2IwrdzmEKS1DLwdwjUmchCM2LvG4EN9A7eS0OeM0XU6Ak3vzU7Mj+s4R1SmoxagpW3X
qrbJAsgbHDt2R2s0RoDfuxMb6SwDZWT6kv8ti7aheUotVGnB2OEguc29N2ijGMuf4P3GX2XJooQc
g7GTQpqoE3cBsQ/LPsXQvgjEYmnJfNDQtTmHi2heI4ofvbMO9s+UnDNOgZTp/csE7lhZ4eJCCPIq
bxxw5MEzYNKCLVspdT9UWbbZFJLJ6nRr7U0vomJ49R8BMGB5Lkmnnbtp7Sb/cPbnM8s2S9iQS7eK
5BZ4XQD/CRKBcLtLoUG1zmhUUits3RSb4VBuA2582osmJsifzr5MA9LQT30Ls6PivfRSQ8PwUj0V
6Tf2GhGc6+7k9+VJUTkVUJE91QOUPfHXigec/uqM7ms3a6heSzQs62atyPJh9oZ3er9zoVa2Z/an
fvaX+tTSQ9ND6O3/9YIH93+Y1MrBnyhYOTcTh8Y17k89lMtkHsA+IWtAMWILvUbmKXp+4Le8T8ks
t+l0oKKyIhq32CtEtGhDyp6j5gRlNxwKcPlIWlWrEB+4ryHh/20B/UH88lYIqBsGEZSJRMBSDn8z
2KvB7kyxP8TcNePizYWYn6ztz3kBNdyv23Ce10cLNpsQ3juRy+RoAN7M2N0vjmI38iGqoT8+2E29
ZumqCSO9MeAXJQY7tgWbKS4hy/hgXOcaYImZfC66B5QXrtOpeuiJmnkxZWiZtViYIOCkGAT80yPX
ecq5hslfBLMXRgagGszXH5WM3mYZFP/xVi92+E1/SLN9d6o7djhMu2wIsR0W/5bJ6rbAZk9EOih9
72EkJzWRXdAJTU6f448rTpcTSQ8iKgJfdubvfWobpgQQyXl0tBqueEwUHtiR/wD7TvmPKBl3pmag
puzx5OUL6e+gP4zahZxX/Mvr9RHI/AXvxpexB5dIZbCso8iQtuAxfeB6gWvEZw3Lu1n9AzQ4ge85
iifiLpQiC0/eOI3pcHa96HpthEkk48TvpOsZeIRJi52Cs0UoURp+dKzK1qeRVOG6rpf42xYwWBsS
ZTNCyMJ3LgKpF3nDSEFC69X6C3zJxmsFfNVQupH38fisyV4k0FsddDHDNHtsc71Qb2uMOIV5fkie
nwtT5yr3uBx33ZK0FSK/AsJhqAe/F8CGjB/lp+SO+mBeAXDieqAHV7dytXckb5dXpOIGCJyBEJ4H
GH2ILgszn0IqICvLFkcNmKXidx1GEq9ZVyGid+/WlXxeRuGvfAlUZK3ORHfNatFPYsojj5D3c9e0
Ja7WcshYW7Czg6J21WDeQ1YIPM07ZvLX1ZSTt/kwesiaVA9+3rKQg+uR6VSwBT2CKbEeptwyCe7a
Kd6M0Pumzc6vWh0fkg4KbBlxuiUAkZxbzPRw8o8jUpElU80EcndxPc3L2Ukg4psxeqpBPu6Ev0W0
bscydyqsxUAi9sVoIRYJ1hwTBCtesCzRhpH25P4orchA6CDPDVcG/4CGrAPqnjvBTzXzeMKdlkst
fWuhBbI8VTs8C95YcOIjigBgbG5rIWu3m0RgBm719MtpMXfAcn6gnGMiPNIiMCznGEG24Nx4zJzt
P+vPyR3AFvOpbd5dK20aJFFmBJFUiA/0VarYWhZBJo1MnNmtX349e3bbSci0a711+Xh1QFxNIMrN
b4htW7peiESIuYrE1/gk3DRPMLmYcie/Q8aQBVrxaQE7cxLur+w+aOXl84kZzEZBi9yTS9aqXd0S
wynbCOakomDSF3fteIE73xZo87YGLYv7F5YrcSa84bxKVGxxdc46i3CtjqKeFOCUKJz7JHicglSX
JF5Ds02KthJ7V9OXSz2m5DVgBfk2FQGaQowEPkPhWS+gW2Teyf9LnIkAz/y9ezOJWkgUMi0YBybU
e1/6I5nG+y9NeqJLK3wo5UTSsw5wOK7Eii1TKRK8bTYQHV8WR7dXhNZQcBHroiQ4kjtUN2kHvW8F
maSpgXtnhTO5Zy46Sviin04UVL+8F3OkopRB/hfhWbSg2Xk2mA7/ljVYtseuufvBEyESyxu+IMsw
d21NYOwqdtjzKOAQwPykTUw0icxNVmfnStiw3GSv/Tzck/sT0zXHfpn7BzRHXdBU8rt0Rb2zJyRJ
gwA28DDkU8MMidBKyoDdnYvxk0CUKXqAD0/WifcbIybzd+IG5PbD9Mhx2PFkWFdqocdPPiiyU3nI
JUbTKL6gvomqYlF0EqLc/aRvY8AqN6Fs8aWwFcSObF+0V90cBwuguZOIYLcz/5tK3jgga4aJYVRp
Ml/3AniITJoDxEujHJMTADvy1L7DRPYwL21CR7RKzV44PseajsU4kN2ItJ89vvN9ZAcz6GNv8v9I
D4oMHUaMkVaau7mQEcAGuf9nKtT10H3V9V+WYYqYKiY/TVmsQwa2wkX/pFTlTVFE2BPQXK9e+uyc
BuG09UlUnqGHjnSrD+Djo4R6ekhUYWlqAJq58zxU44ej/JfZj0/HvIazI3nRXR7B3etd7IquDD76
X8buDxkeNS1iA+DT1j11TB5eg6mlCuOzUXcgPlDqTnbrOnzePP5K5gfP6uJgh5hZl2z6OwHB9eEU
pJq5fDB4BViCJXaAf4BBquaFub0hBeS7q4ysuBBhkRttPdIsjKaejXr8UZiF414kiwUgj4bGgFdM
YK5J8X7drkZzbhqfF8MorrmCDjngjMnGCqF6Bo50JOugfmhF1F4G7Q7xeGlRRc0gQ6JUTBuFlODw
9S1adxoShrDUZMFZneUVzr3FvW2wY6XpcVqzCiVJz80smZfMPwMl2N+n79LT7aYlv5FhfrvKuxRW
AybpmJvJcnAa9TO6bTSYaM4D7mV0aZyhqvlaYYFJxbQZBZ3CChvIaHB7/2HihskooLmE+GrmD5ye
oH90wnvHFOXPmbRxZWYd6UlPQQ07qrDti/zkrdL8fip+XtgjC2W5Tew27n+RJ19vlB6tTvaDAloM
IUQ7PovOox5U9cNNR9rGHQrYyjFhQXyTwsZJMcqJLufLPhZcAW6fwxLQqsY0Y7dSXxk4JFjyK322
6XYyk40Agt8hr/47q9MRU2y6CRANZUM5Hp3QCTfjikeDIbbEFT1wCl+s43qaruItYIpr9jLpYoiY
9mHo25MAtpdE4eoe/j9ClwubgT6RtgVxpcs89VYPTtvz2nQBFFJRM1fzWJVc7PKyb3piq5Tqqo8O
TVDNPstPddSNVSo7YStiK+8+SGBRoTCNR8wJfIxPZfRR5tJ9iYVb5nJsf/HhDAWC+IbjzebKnCv3
Zfgp3Wx3CGIqmKpqIXOVguytSOGg/t80GGrOPFsxwP8MbDGAw+Y7TSRwaF4GeKL4xP0JSI6p/vu6
BItUb14IUynDp4EQanPQSjYqKChpoowM9kCo7PsdzT7uib7fxzB2MX+5JGrpD2Zg9261csAypldM
bheYZ9kmW2DOogJx5vNY3mhPAJrfGuoviQ+C3keAuag+juIUPeOj6d2dQIGHbQJgU1MlLtFozEYG
/yJvg4HGShWojNJ8CUo52ZGTxYZSNlM1b2Yp5q+xoMFMVYJ5wUkc0DstewRbBRAf96rk3PuhPfq7
77cpIBiM0Jj+Q0amFfJIcsPUAIUet2r6kN2SKzE+HiaeXycmiIl2xbQxqtgnDJoK7PeNtEtsp+EV
7jPn/ZbRf3mZV8CvU2a6ToE9eXNBlLr5SF6D8qSrWH1b/9Xn54NVXupznL3yTRJIhrMAWl3NK7ag
u/81aAoCFJPNTGnkpI4pxvyOEr6cnqLAItcUibXp8+/3GPYioEX8IVA+qsiTgTHkx4bVO7WgkEDE
Z9IwnIe9+V8d00pLUyTh3BuDiJgnqqqt4qaWCBF8nPcqUkomzFlhyeWvIQ8GMsVITltkg7s+xrmB
B2B19aCfQWqbZ6dPC/YN9T9im1YxUpsnRzM06Vj4GHAHh7XLN6lcjjxdFP7y07ULJ5pwUtAdpYGx
OYgoZepAtV1pYUlXp3Ge0fbzBKX7jDnFMhUrhkOiR1r5Y5CANp7GhORtccsHN2PF06+NmwGZRrDX
LmPPq3Y2Tw2uXYcRVZdsgNdiUYWknricO7i7jSDHFZcElTgx5Cza2pRPrbWs1yvmN6+yI5GMIQ1g
WnH+ih+KFA9djuiEDplgKoIOwunmyoyzD7BX9tiuiLeXFXe0cfB8CFJZsKhKmryElEUnnG9mzVcm
G/pNOWEU7hqgOU7IHBzIJP0jVte8qCtrnTMtcDVz86izNmLYnSaHskj9pGuSjUGNk5O8+fRXI4kF
XtIWfwVuREqaxuRutvz9vwf0F2+HPvKPdn2trE0xR6LLvTfAaVDov7/7IM1xUN82zugtPq8LpKDu
ba8VuFk8FQmRaguHxUSoh6fgY2c/U6b8qerF33ds5nEbXZE5/d4uF51iyQzHI97LxV6lds0yQlxh
k6lxrta8JPAoKb8heDxYe6ow8WEbN5yqnwOs6oCEB380Q6oSi4x1Q8guz3Myr54zjV/n+S8eTKOM
IBkIEPlKOSn5lhqWYOzw0aGje0nIBpoj5sM89e/jHZ10+IUm3DM65IYUT+3hF+Se5Bz73jLju5jL
vUd+jE73i3CP/uxQr55jGoMwillEmvE6JaFdKJ4TjYv6tbPzxNyT1/JxtqWG7h83d3j5H//A4g4N
2tHW1fyaabPCDdXfeS7TsoLDxgbQYKQxtHpQhBTfn6U14B8uEDtcghcYKkLK1+LviprRkT6Kr2KT
DxctPPHi3tMOltVCkaixDkj+JB/XC4ri4rgPmB/Ru1SpK7qBrux2/ptPbLDVqlY0hw8sghS9x7Aa
kYsGM4jpst7aZBnwXi679OAWeWv5QVz3EUHcAHjIdpl66h4YTC/VpkGxxNL5O3qn9/zH90xfgdUn
CWTSQfQiWBKhzir3eEQmBvr0gQEnsHrdK6/X7qPJyJfo22+w29imVojM7YtbjKcZmuRIaeDn0sJp
NzFpcjFUmITuFBZSYjfSbJ28RC9SUpxQS17lDkX8CraSfgbit00CSPV4F3i0bC20WsRbXBzRJNAy
3Wx8H9V+zhhdTuERid8IwiUGw7CBdMwjXNnWpbIzdweecb0SmG7myKYiAF7vc2nyyMUyDOHtJMms
bL2eI9JDnJVtvKv80W6EeCGBGw2h71E4LAAtntIQwRBblEt0lClCgm/VltCXghxScugnrEQOiTy5
7WSGlx/gonky1O/j5uCk3Z4ivTIVLcjo1D2M32YpoXxv1GBVElWp8JNr0A05hboykhB4C+iklOjA
emaNWOfSV+oXdyjSoe7Dc+WWiZAUncpZsG402KNb1gnxpzIHirzLRUoxNbuT52Rr7R7SsasQBS3E
t8fm4UWGQYTCZYG+AphxPc0lJ4Hehr/U/5gvnM2vdvcBkreCmRiKAw5NxwESSVxWzCrucKXoPOK5
cohdjW6EGyN0Fo2BJFJH5cKv5/arqNb3u6cUVO1NLhp0+iTeox0ygOfKEcNCx60n86f8tb/8cQq9
mdmPQJ0lD6adCZAiNVNYI1pKyUi9TkVm++4lbso/Gnf9U8Ejj2IVW3W/7EW5v0Wtysjtwl+iVNHt
sMQtcEGpdf3Yya/YmWQNsdiT55fuAIbz3qgPIWkLsao4uHpzUsuxxZtukvhvttuEMlwflRvkM20R
lX0v91tD71lyxFstQTx98TRiUOwkq6Zj6pMKmSd4qZbrDTFwQBvvYMgUqeMiH95b5Jfnkb8B9x8L
layTnvRDutZfF5aERwLfmlLHN4KDdG7SZ1YLBcTY1PI/oMnKB5p5hmQSi06gClLdPCtPKeAARBJJ
Eg5ZpBt5X+M+EYaGumfjxqKdOJARTUqSHb6UoxtQDcYAvvS4XCxztwWIvmoDmLJG76e/xVyoEhQC
35043LQohpYJAxE0xHtCq/yPLgRf5i8ufCktLSPqDhujwtb7Bal32FDhnGFBpf+bPtKMgQwuIq8P
FfdV5sh9RI6uUuzmH5Wsmf5cSQBD+o3Hsl7iR+o58Of0PLgdVjpwWIkVSr4whqcjU4l8XhPQ8I8R
irISrCdvU7UyXF8e5QAO45JDbbceWEhaKDrdi2neu1BsIXVU+ofnqBwt9iEO+sKkZTWVeC7FYOHw
rwyELrBnDXxgpXeONdNr7PoanjR8/JRhkb/1tap6xpHCD/UYCTNMAiuY/sX+pJr5kRxtc+XMrpi5
r0c1i9wmAn0su9dvdlESHj08w0c4w25mW4APHlPYB5e8J7jl7C9Se4cmh9T1uXN5j8b+qffNMzH4
BgIXJn80E5F/aGRDB71F87lLAPhXbzq4CzjrxpnBBP3UwUFU5jYdyn8/2AOrDliPGq1HBKfKxezC
/kSH20gLtJEtmWBI9LFUKKevVmUXbgrU8hcTudDhl0ZeYb4X2erGY1rPgAMlzTn1NP+ZGbLLsiPd
2DUkiN3GJDFv6XBhSxHxHODXOw4ramrTKdNHAsD1D8Rs7OvU0IZpcbo1C7ayUFNRtf/98l4ZNFCm
WChMhMa2i0aSh6nQnfFqO+gh5Y5b7fQ8F6GSgVphwgT6l2z+Y9s32QVIEHyksaBZhGr1rfh+oPdp
pOLDjQTq3M4aBmAdQDn3xfW2p4jb4ItJ8J2jDol47xvaIYmxYZA6a34EQnGWELPRAfmxL/KZ7zvP
EQU+Q4tzgpQYdJm7Xe3BmhHyZnnD6QMTS4r7bSUfXBi6keXo1upPrU+P5olupCCi9+nSH37z+sr1
ZZWcDnHl9hRLfEkmnJ7OCOWV+lLupQ3prru9qLl+gZnDK99z/+Pep6Qu3n1RjrJJGTtOYbxUS1mk
EfJhTyD28roFGzMev51glm7vUbBRRaYsSEc25i5EVxOaOG/wTs4A/ttak4ISPPmLLc/AnnmPs9UX
SAK/WMIJ4xegFxxzO9krPB6oS+g3xgHbmDRlUEL0Uj+YWWIsOBb+WNMH4uCAod9nyoHCxOpAVM+7
wTohtCvnDgWIIGdxJ31wSupju4cv9E3rB3RYlZU+a08K+iZN66Kz50degQiQcITU/2Jk68UFLIad
Iv2H5E8hfrGOVupHjj0BRnEuU7A3bfmPbQkDCT8axP5UyhRPYabI05QoGDs3Ugdzjf8w6ubxqMmR
dKizwVz1bZlHzFAeeRludsMcCitKEV2nJ7rCe1gznUVlZFLxW72euyWSPULkNwBDbbI9oXUu0Ge7
Rm7+p23d1GyzR+DAb/3YtywXA8CU7DJl+Xyfv+NiYm71l7G/IP4GcQLww2DLva/NoMe7uy8tzZT8
igEVonjyutACRKu0yblXwjFhZd1bzyd6wEBM8A+oHkaBhuK5lRGUiCzcMIofE62dkaA+pc0jXlCC
gDoqN7bn058jtwnawvLiDqeQ6VaTlIrtZxJvHRU1WdJsY1iQjfF3kBaCOzyrOpBhc8oXWXjNQbrW
y1nlBcI0Z/y8kKBQD8JkDY3l6WZWMQXk1GSfNGMxx7T75ddxW3ReyFbnKYIepOngN05w2NUIumCN
8HskjqDi7IC28SEGeFds+2QTm5rM2qXlTR9qTTVKGfvxX51yZgpeII7aLIst0b0D9w3xQdmFkekN
8IfTu1Upe3mPGUDnuOmMOT7e78gq1Wrm0Ry1KV2zpp0+7LCmg6zjjJkePojkvoTAas2zvDgr+vMC
/iqElutBqrBuIpc9AcLSoX7nmcnimhVDxT9TIkTCYoVfraBNPq/nwJoH7ly58MdgiFDZNzlOXi2z
5OOoP6XPA6qOuG5K+Rkc5Cj2qtfOjBXvfsETuC9TmFqdAzMXpJsPr1I54JxGM8OTGkq2rwib/1GQ
am6l2aHCcge09Nh+PumgB+pc1XEwCSaqNGeajnnfYWYrffT2O7A4aG2meLPN63dkkjuouQvBKu7E
ujIRdjFg/PrDd3WM3lzLcNWV2RWS8dZWcwRkQWDRohOkFMPz3y1+ncp8M2jNr9lBrd/IWjv4qFj6
rqttcC8mKnlE2aK6JuQrbaLJ8xVXvC2lsBVpArpKkwpZs+i7AXJnyCfPyVTtZ/BQya78dxscD6b2
DevpgNM7mNx2aSwq2laQI+ZBaF8bHF9FdlUhxiTVAIPSjKIg1Cdgcheh6TUUtxpgJc0YWM8vQBwj
8spKiKAhfkIEg38Y4qS/1BwJtJtlkA5vLYZTmDygDIFb+HDFJAwrPK+cOVUOkBv767F/fdYJVI75
NuGcsYQ1OaqU+fCsDEGHeyFehvKEjvRFd3APMByDdDCFJ/bK5/dMpkMqCijtZUus6M1gx2WMG/GU
pBnHmermrXvo1mxXnAQFZ9rZGpUPR0B01poUkySWejazSp89wpY3OVRb4aAbv7mRL9J5HciaQCR2
rg+cLjdbl1SIOTKBN6ONGxLzwMFgbt8M87oV9iuOWP+rjyxr5tmuqKG3BGmkKv+T9enBMpwVYPRH
y9BaiU/8disu482QLcVpKrKeCJfgMpo6SkrFoIf4wok7jKrhlEkdxNzCP9ixYksAwzw5Ffy9HKAD
6s+NXlM4Zx7IwNqCrgjMBWIpJ3CPtN+6y5HYZNrTyrjVRTa0FVJifyFBrLtXCHBhBR0wwgN6Kx23
Yu1uhSHkTezO7M8/vjBhI8iq6SfORxXiYPRhsmWjJbmNNS33ukin6YUJI7dLiraRhxxNG4d5m4Op
PSIMNdiZb7tf6/Ir5PfuR5zfmVQ4sYwlRZtrypMGDMLyLC+J6sI5N75wkE90+ijWuOFISRAsszqJ
E3+2SbneCt7Z/nFTs4MNi04/ffVk+10veo6eaElAgH8JHzkEu4s07dikKdeUaL7LTTNVhuFDgG19
Ni+Gs6ohqK5e7HLjg2EQquHa5wbzTKBJMCWFfm2LLt0UlskOPhxOfW/Ms6z4S82rWJvp04cnkis9
7h5+Pr/GNdK6iYzeCGo4QvUK803cllvSXETJMcpFzEnIFwoWAxon1YqgzxWtbGjVY7x2ucHJfTxL
k05v9zI5qkuaZl002iOVP4tkthXA9IvMZr20eqmkcA8FEk0CHfqejRQi30H0dVrisklUApdfOl6t
KrSe61wMCZNrWkoyGiD/BK9MRwjfiNrmz5BJMjTtlqhlnR00bl/G2Do1PHTIjYX5lCSuC6x6C1N1
kMt1p/PpVtiTFbNn7DEN3bJRtkmp7m3dlkUW1//93Q6o/r9nvmk9PWctRvJGICQeUhzTy88Drdmc
lwRK12Tzs7/c4lW26NVTgrjBwDV8hHLYDKKAHpLha7iVVzZ0E6quKD86So91zOwRRT9Ev1oXRjdP
V9+fTTP17OcyEKlbhR6dThmaFo64HxGDvmqRvva5cKKn4XDfQvZRNDhS74g/P6t+9GqwuEu1z4zm
tIo+yNtP52ZzZY3+6GwFMuh/qINYBbWJslrMhkeKhwvYWGb2W1aRD8nkd+k4hFik8OBO/cDwGrdB
AmFq3J2+ITMLJHyrS0SVailyIgfAstMC9tQBESjpVBz1p0bQfnxXjjOL2ftGd+NXolqUl+rEZmAq
CDax7IYfvISj/mGR8x1ANpEAa5Gzk/wg8efqdUuuoOHBO3KzAlaA08TsK9/OGYbAwLDiHxWK/L5z
YEpmUCmYjQqr/R+IwiUOAuEm6ffCWKl9L1eUC4bPSnwCSwT1r7LzeaEJOZ/9bOY+Xy8uhrTkftlO
rSN5Q3PkzqmPxuO5oYzKzA+ZSVhWN4eyVcRPlVgWZGU8O7daY6ZCjORpQjEgWhKwTRIODeEf8iCj
DVZY+KiilxeFldqEP9zstrrN4gKKZA5b6EE6BIWd4lO4KJQS2ABotVVXZEGPwOob8dQbPOwuogWd
NM4hD84TlLaJ1j8vEidojY1NwFapl2MMbYvbgFj0tcvuYF4tGQWEfv55sawWtE7+a/OhpQgtrhmM
7NFIfSWGqosExCYrWpIEQQY6FXODpp6c+6567d+p0jTC4K5ADrUi2VJ6HMWa7xErPrT07FuWg/nL
Gmspg2r+y0E91ui5/+beV1ivSrFx+F0/hd4df5/gfwU7pXLLtwr8R9gbhv/V7Zk3l0/bi05dYXVf
qzFizpEP9yTZBoUGryOoszcPYKWFseqtuqr8vkWMneQAj5KfDp8OcaNvpAilq53tgVLUYR/M0RWB
KC6TumoR5A/S41lBkt2HCsUyWUW7u4NAuNRcVGwin4vObg2xQqRt5FjvZ/MOvNGmoo9aQiObYvZT
4Ec42FlKN+6cJ3Cbh0l9hEA3ukjtDxDLdY+dBjZZKea+05kRbznEMERNKxgx42Me4c0J6Cjr+RpU
nFW1gwTWij6o9vqHs7bH0lCGArWLrjyH4mWvqPMgs5rAFwFtIZUhNkDVoE4x+nO02T+iQ12wXbsc
7MbKFKDrS1G/FEZrDcKARIItivYYMyLlo4SKPOSaZEDZMqxfIgtGoHEnQH3B8zf7IvXgmjKFed+i
ctAShisIUmv3MbqWszbVzJbmKmHegYZELtQaMnFrsRBgGA4DAh1thWbYQVYamLNjKNyH9D9u5CwK
Ihsm4vhHIaRvK9XCiv+FFounWKzwcH8IqPXmrb3b1Asqr4GcOgNBRIrGsPm55ZAO9YGuFoxqRlHy
pqICzfp24MVGcUTUC/Sxw137nvGfRO8SqZB/QgVP9yca0SVJjOrOW64unP+INmd2+Nsl5Jj7vh3s
kNYCrYyRmvi6D+op9325snI4+xSqn9a4KYSx/LFHqvkY/QNvTHXBlYzP/AQr1ap53A91x5CpsLoU
hzhipMNRyRMennNL3Vt9g8iYCKpum3RE6L0LfQvtGs3DlnO8vIaI7Zh8n3iEltSCJLV11ziJdA1Z
K+Q+G/vWK4wLgvRTp+EuLeE7lbmI0mx2dGuxqArfeUf4NsemrNC3t+W+jP+mzU267RdQy/iQNvUT
yZaRAW5vKhUN6wtpaSvEwhry1aFCTHxxjMCMlnPEJhrmDE7FTaIH+7iBAphDIeSqvBMJ9/E8JLo+
3+it7grteQBa7rrbqDRaJr5px30HL3b3jNcGFASZGAUoHLfXjQeCu4F5D247hJ/hKJieu5QCcDMQ
ZcCfQF8gD0myOKfeVugpoML6DuLTUai54VH1BrU9Ubz5S4CeCcupXSWecYfDAjnEKD6+O6IRvCOi
9gRSasZdp3eD6TurhHkwUN7udxIvMsg1f1SDOttqgFbwrP83Ms1olAXo3UJytbqoHbV7agHU4Ler
IstewX5G2CxARVDRvWe6WYH+VSxa5a0Uc5hmx5FDNcORZWC1XpX3K4lum/Hzva1l1vMmmrDIm4hi
ZTpDoGhLLqH3CWrixnZetnOdJMk0QRfJg3qX//UcnaXl8a9zTt05GTmWtmF0G9p3XiDAj7377qN9
QkHP3WYZQ51G5+3hpVFEogurywVPF0rZmsZo1IbLRde48obCDOUs7t/VaFLDNUdSalTB6RFUb1VN
iQHztE3UFut96shrinHGJMx8TGsr8vUUUhTIwNSiaPD8e3wgSwSRfzJNCRvhxUUaQwzZP4l0YCLo
fwZSI7QIicaJ3wVTi/9L8mdN85LlP5f+rorzEhehUMTB/szonNgpiH7W98QTaukwfpSWKut4BC4g
kP5TOSQ0BsxpP+3NfIhzHy8qLrB8GOkgDj1cY9DVepJJ86q4tKhOPlPFIOegecX0i5XVyesGwjWP
Tu2WDrPhjnRs1SdOXtQTp0FIOlpkHxk1kW61PpMgdZdCYz1VglQYQ/b3XZ62ZOaxwd6onnYzdCyG
Li3xjxkTvTbpeIHT2cAjIi1U30j60YBg9PoRYzDqQH/1+gsQed10QdlP3vIeI/Gs1ZWfeSozcHWd
ZJGE2kcuUZDRYLqquJ4AY/cYDdSEZNuSHOVuvTPe6dDPrbGnMQgp99Uhv00XHGLtvMrymqXze/ji
8PYGrLw3SJlH0ip6NalbC1Y3oIbALqM5ZMr9qPkCZa7gn92AQg1R9WDhgie6D7TBu33F9vlMY/S3
EFLj0ggwpctVu+eyT6KNqbO7QRfbG0+8uCd4hlLv9Hc44VBTsD00nbw1PZiQhhKCpr4V3Kgquce2
CsdyLz3EDuGu9AN8a5RZOKypYVKvZ+GT/syb/1wJEQnDpmnj6zlyNbJIoIYiE/ek1nH0eMC22L3T
Qb3yY75xqyqnSw3Cd0sbLWGL1wu3zKZ12jwgVqcamqXKXdwp9zYzEPtKQ49nAQsiNzh4TBvUMODV
67vnxYy/FAnl2yzbZARNbBbTLW7ULfCD+1DJXv4OaiBY8fxw9xBhjCL9+3zBOyyFLjZSo1omULEK
W0ztdrBZCS4EG5XXCUNg/N+NfKTNxCcClp02v/Mz4QqxCV/mCA0IyRplDGjAXU11J4UqIiw84Fqu
KWkKKn+UifCqHUtk8G3YU5oajQDdIx94mo2dSkJcqFnfQqtSJYykF8odAipDWXVCaLHRTmTrvA/W
IpNOVKqsYh06glj45G+adf/qcBrN2pEIQ6/HoB8rjuo0kk5Sur+Q07oyFP+nLe7DsfkRDYs3DSdr
0Oj/BNLTrHYxU0mzwRD2e1XwocXbbF6It/VkD0v/BHjItmH7kiHcqGqevxQkVuKwicWcrtL+iYRp
xKaV+LJdRdg7syzHP0hSqfOt+G/lV7wjIftf3gspkPo72fThTJB4BW0HQtWEb5WKfiuPTJxNht4b
k9bIh/7Oe5Nff7ifJwB7zecQkbO0eycCFveknUOteV5If1h8fr5l58mh/DgxSaZvaQJjmxjnglFH
A+IRAnEGozM9A3EMRJlnrcgpyRwG92BfDF2JpUTjBOte1nAV+KRKzy8h/egQqMtgOArpohZluE14
FLVBDtaI/RoaRqd1DkC3dGIEPPTrj9rvOwQ/UzSpaHf6HTCAyvkKLjtxeRmvsP6VapxolpKnOLXY
rSHHmcYpnorkqfQE3fBpiQmouH7HtZpf5TBfimTl/KgFemRYyWwD4KDBR1+DAIE1gPWv2Cc7pcz3
NBYwSObzlwUBfEebYna6TcoAeVbXUBBWTVK6Fc89OaaJ1zaaZ39K2NIohvWUoiPkVm4dF9MgEJaF
gG2LmXOtmpZHirUTcvsIZqWtTEEeUZweD777WhesBA+KmSthmj2j4Oiq+fUzhHpCap3JRScMUfGb
XzsdQm0cB9MHyGhgZSd38Ls7zC7QF++XOrw2xVk+8f4kKnKULciIlDFUFy2otAKKuvQVQY87SQzw
D58IEHafrhRE1WfV5ZstwXfIRI3VEI/tKhBp7aLm5yMq3zcJrFQuvHcO2duobZdvcPqoUiVbLKFc
Nzlm3y4RuTGAIyAx0Vzw7YrvMTQi0cYMDbU9hHrK8EKQuV4vUd2Ya5qKCR6GvPaTobPDw/GriqxJ
cKDTVrkBLK81/JG0hDJBv7yQy0pddrfRgMIU0F5WFG/cK9+fUhdL8buh81ca+zvt5TCkUzxfn6rp
lAdYbKUvs2kxdbib7M5G2uQOUaSQE7l37LNAXVxNbh4P00DUNSR0v2d8EhLt3AgrwSgRgypSFIJD
71XVidgvy4qqDS5ArDK72cgQJ38TU7x0NxmxyJEbHuKCy/khZia/P6D0CCnwotUcWh3FJRobAnA3
8hEe0aL88bWybp1uK+NOiyv9sR3l2jv4qAjtiH/sP/t1IqGO8p0iCWrSuh4pJ4QfUdBJ1nZcMuzj
BlJBq/ecu/v40RCV6IGLyFNo4PaLkn/jLNPJcYB6PWi2XGy+Dp0rEHVJRGJTpGZD0xc17m7hP8A3
YH5Gh6BchM4zRD3/b0oNKgEjah3DN3JgdV27R1X3PbAtq4QWQwM7TczHhY9E8BT3pE/6NsUybPs4
I20rQUgsACbWb4xhfn4nE/qo5lma+7aGGhw5i2/gbe6Q4/1r1mxSaOnQhFyrZXuqxA1HRoXfP4cM
CiH0fx9Fx4nOxcCmw4FernxljFA4amYKYPMUvDSHbNis7rDzXw+KMXmETzVHzoPhtRL4tOlDRBIR
HDOw/Ej5phK5d+mG2XyVOVzgYiucvQqsUFQM7QVlJUj0e+fa2QwX+SzNQ4j3z5WF3gAfzJGEs6gn
nddxMsvrEhnQlqcXE/AO0cGB7J2tNPejUJjfto8vPCftsPbyxEO4m4cpHBsM/2LZvRxcMdOp/Pp2
9BZVUz45c80sbGMUcVDj8vIcHz8qy8FDt0qy9okcwlPO7PcR6BLfCuKxAXndVgyXUrU6mmT8cNb1
C/Ft8XzLuOIl2C3XjkWues/QdMaG/EYxDr43i2ByQWhEqz64cXoi3OSfan4uTUsMN0QFw6Ztlr8z
pZqXh5xUvKZUoLhe9DVguMMt3Tmja+BnNU+fLwxkaFDTMnFVAnhooKr1uK+5fJkrAbVtNLyDY6ju
pMXq8JWxb0MTdvsN/NFlzJ4F8TtpEKBYI6Rwu8n6gbJNurrs4e2tS96tcx9uiKlwQSigQmPuv3ye
ErkbFwgLb0Muq4ykjPc8YH691yeGty0csMyy5NorkQ0gxhW1j0rfpiQQmcClg5RKfd30OgesZH0D
jS8Y4Wtny30oOclel90rlOQ4AW4qtpqEKCxTGdrTeFyud+wjw2gMWtpGhRT4kzJmwrVWZ3UJPA2V
PN4K1c4mj7OGxsdV4HBQcfwnGw0FKQCNT5Jfk/DWvcPXJtbaXOzDPPudonpE1wQJqqpBiAOPTSHD
/A4c2rjdWE0xZ6HXorzEoJzW0503rQAxmNfIVi8yfs/nT8ddcQsfMAMUkurD7vhiTOoJsFu1LIUl
rtx6C5m195LB9e+8tUmTqHOwZu6F1XJJvTxe9kLM8uwp4EOeWcOjpDr2Atz/CT/+6QXb2B7hhhP2
0Lzzup+cT+88RFQvQzNGyD6BUpXRt2gdCcX3zjX3vPyMQxMCPZlNyg3QwKz5afC5RjVrsoQM6zVC
f9xeX7F0+0Odx+1YnOr8MMgpY10EzHYzkq47jGFx4yEgKZ+YnD66kqtfrwzx8yqfKsLLlNYvztH9
5wGzE2zZquwt3LEFLA/Upifkq3neCx6ytMrH0oM3Nzq1+mPjrGm6BFDGb7/dWG2kw1+cgQmZEkc1
JEJDSOA4YjBDcFdFrVtWEGr6k3dBu2t5LvQ8/O0LqiWc0NJLyLrM3Zw4JdUg5Va0mutsvNK9fiW3
8yY8RHIKDASAW9HZnXWn6a5S43RrpQPjutUNsKK8YvsmIujycmMh65YOSBsBzNkQiIYivzOA0KZ7
MxH1aqkzmIcTwd0TvCUCt4+Lhf3Enux9noc8qAO/X59wuRNk7aziw7cA222adscrj5HNg6+/2/kY
1yF2sfj3P5vPgxD7OKkX/24hlk3D0tK4+8z0w3edB/DW4LNnJPeki5IKG7p9L4xlnmwFfolt3wSk
Lywte8rEPgRfl8J3t/wxp0eBD+fQZG/iNc5z/KowOqqUDiSudG4h893mChCpY5iTZ2bzFCcVk/Aa
hLGCeIAd+DmvHPVkB0zc7lBv/MFCykNfTzmWmPxAH/0KMP18z6YpFprlMAj9qy0E2NKp7EAsaDxP
aESaTRJ6o8UNE+kMS4tOBVn+e5+y720Q9NWcnot90PQJNJPtoT92gFN/65vraTAkPsIYS1ghgyfv
aIp9LQ31i6N8+8ovfo/I2PVNRHDSo0TeLYLKf1HIdZB7qzwRsnoTe2hoviZZRox6aS0OFthp/oN9
3KZukYQbkhsS1pTvOpyu0J4Rif6PxBo82xqRYBAe9vq/4IKR/6mlIgTFrjsbKB/1hEGW5NEYTdlU
rjBCtFr1tNzN9kzl9Uq9ybEweYr1NeabhQV/uTiJP6u8pOWGLO7+kkI20PdbMC+f+N/Q4VfNHJCR
L3EUiXzhMJeEez/aMwRAb/OKkw0eVPYXGUty6sQA4Ah0Hq5nYKqU83bheeT/ssWyZ5Zi4cnspsxW
09tCHIQP7pilZFGnA/SPtN/cpT8P74mFRG8Da935nlIsNd8Egi6AyKQNp8JAlaVyAfgGlnfCTttI
aS2wK72N9/N0bIQaPPC3vjxA4uUCdHEJCcrZd14PkU5aV9jjAwByXpvuHxzhPHH8/KFWAduNwM9X
hx/RTczlo4Uyh9TPSi20egpSYI3NfpAt97BUx5WyPMpHE6/0uBkfzWhSrsiC2xNR5k07sLb1MsQH
No2qgDIlHfP6k1WDJUPhdXbTuG3QNLKtB+8RzxkGQqzidOa0fPkgidufrxJv76v55AGWG/LUoD4y
DQ6BO5J2W++h0Ph2XqtfhZ0YFsFkXvvCfEI2YABjzvW8cpr5HRPT1t4gH23Zi8rI+eZn9ADI3jGm
T+33/bf+UrbDv4F7+hAlmGq/wBSePjdkOsKlYtGKbAYP87nHAaelAkPX+w36gmlyt5iz86koIF7v
uY423j6qYFlbY+6nZycTqmzLOZwCjm/rqRg/vLh4AkZVlH5dV9FkJjreBDcnOYeIUa7jQp2+wwQE
I2JYizT4thVM892VMIduCuTKbRhjgImP+u/De+V+nyaHc6AI2E9XMuomciLwef+cVIAviTv2Pk+M
RNERxrSKHxtxjnAY5ANenKWrCHVqpD/ZevhvJenxEvAkGaH9X3p96mBdqwVXsCJPM7m7Rs67LTNY
rdcM74HdCebnDA/E4XznWb4+YtmGwT4zACgoPl1Y5ScebKfkjWGHvWYKXoz6fG+MUjt0EGcKGiSK
C4bkRX7NBs/rA2mIyMKbP+492AYOPj29fZGHovblfFEuVDwnA71efafljAQ678fm/t+TAToKgFZ8
e2p5zmT8PPAMv4LE4Ku7DYL5/JbN16/1CEJ5XXIfHFKlBtgyAigtDNt2Q7QpDi66ECTft/Qp+Epy
SertBYpaQvR8m6HhsfpcQyafqfy8GAjv74IpWekbVkCWJANFrjNEL+n3enstcHmo3va7ltCzbU0r
B8XiLulVMHhg38BJ+GolRPcBBkAopTpldt+wMYUSOzpfZk4acSatrpSGkyu0dmtbhHiTLPwkCJxO
dVfBVM8X1BeoYr84rUoM7c35cPs+7NbQ4msP5bGDK2xuexbxX4yhYwBWDyPNRhJVJe66DySr/xfT
4OQWyHklOkzs1U9sbzNbzrIndlclD8o3la6+V8zYFefDInYd6JJ2wFy8u+NzOvkgPs6bwft6Z2Di
gKLDhStoKaLTS/jOvQXY61yMMwzqijXuNi9+5+DmiMKIkCqeixsSdELLNWSXfkNMsF4DdOMJNt1F
SxqrZ4dJ71stknvrc7C4jK7Yu9FPmXwkSpvyCfOc91yuQXbdelU8FaGBhcAyXQIXnUC54WFNO1gP
bRGr7+f5lXCch2EYa6mtIePUoM27IruNAEltH+dRG5s5Eesay6lw6pARg0XIwRNHHs2LGqhhNcFp
7LRtevVPs62GWCNismT0HkqUpS5tBt0Sd7DPGd3Qrjp/joovgedyV2Y9eFVLycXqUzlK8Mb0dIFf
BwEk1xXYUe5QOgFiYr+dMc8fQhK07w72ipgQwA8pOlXV4t+yJQrjIZn8WBhYl45JgMWY4V8P5SUn
ZjjRieUp7hpRt8r6RaeatouonHbEhHGE4MRMSWf+yDIJYJFFdEs2+rs8LHjFPwQP4PU4CttnhGUx
546xOo15gPGi4dnZQRFi280HXbpMqJ4C+jP9462hn4VOVTHSbdFwbXty2vZlgiuN6+hRQy5hMjG+
leADtlM1nUCsXu6hf2LqTw0gA2fP+inNrwrA+SSvm7BJh2tlN+TDkheqhbIMmSQaoJIZWRRI0VuO
KceyhCW8GEvlJBO9F+//qHhJDk8WTqVmGo+PzrBwOw7t7YmgZdHCjYtaGSHDPvi8P82NYqMUipo7
YZf+c/Srh4+c/FKGF19gx4e4b9qaewGbxz14zOQSRFG2zw8jXYhkmCByaPTHbTk3GC/zrjSWgk3P
R6WwmE0pE1PzNek/seCp0fTbDx7L0bDyMoK2GbBBF0vmxcqZdlvHPSlNxnYQE5nfreU/vHeui+/V
lziNKPuib3EWT8dsogDNYYpnNko/mykvUg3DEOnV2OtVsOnoepjZBH/IvZUsFzGhv6YxIS7CpeXJ
2iFBtxIPxKTayowSV6VRGzROkHgqoezR+l6uyub+WfIXNR3xlFOxtyLqvsiBk7RQyYMxmrN992E1
qBjIlGDiOYFkhGg7iZk0B1jgFiUkNFIqD/JdYKthwTLHDMa7yR831rdr6c+8aJ6nC6K/4SuwCohC
BZph2WnCjSSaMMlh28oJ8GPttZQlUnyTnJL9z2O8ZY2ROsT5Dx/jwP9QP51plND8UteR2sYcmroo
7ca4kmgW/JRXDnAxY38t1Np647feb+r7ex41l+GDZPoktl2J6txRqTl5mACHkLMwo4/iFyP0WtIo
8NA0OaeYqzKmbj4YVqvP97LlLUy0HlDkn6rVn7feFhPR1WEz1Ul7VibSyLp9vWLJrvBniuiGqvS6
iq6Vie04B1vHkp4Vr824WTsESi3XJ7hxr0aKE0ZOS86FyOxKK7DhwntVCi6S2zTROfYi7leOqcBF
y4/Ve8e0hEvAmPsVceU48pt8YCQ3RLBcuFnlXnHDz3ckAKAjafpmPV1iG6w/gTjnOs8KbNyfs8FA
JuRcV/H4+k9W6Pe8volT8C2/blXOvpBt0Wke8f/IIky15Bt0ERDVDExAQl/PSuTvTGNkgqT7/kl3
3g8uiv0gQCmEt8PKLYcGXJ2oCostlIotUA0CacnD6oWKFyyMrG1jtojW97++03CxapuKwePDVDEv
8FTE3gKmmRAqXo9Pv2+lbnoIuOxTlXs1rxqerFAKsk/bmrn72eDJN/aroflzjUa8GADQKcY9d1Qx
ZEA1piXpnw7QNHFgRzUXbNbKgspbF5JNgaC/swHzA9uwvJoMM073u+FgDP/0TZ2rs7WIlC1Q+jDq
bkmzcKiPy47wWyXO4ULHPY/05IvJ9oaZ0oMkmBvqTyso/OMrAJ7QQp03Fo2hMd5IK3RwtovcdJjg
Vlpxpl5tsmtPVJFlp1fs8MWWjIt1Q5Httcb9aNswdu3bIir9CrgDoLUD2t1Xy0BdcQE2AkH72IqV
crBvIGdDk7fGC4yu251KC0yLS0BjfrSQcIjV6rmPZHhnpFtQ0khylBKMKWZ8VNCb+igVy5B2+s7T
Ejh6E8yLugL1DFK8ZdJrg6z59eHfKK7ORndQsA/Y6RXyhIkK6JIsspHfKytnxV1+CNzIvtQES93+
V0WyZJhU/BNTGD3aJHMfxk8x23L03DUa1YdQtupqQ9svvtkm9IfHpj4Q6vYSuBIiwLungyADtD7T
ATDCuX/Qto0LRkk1wf+nLhckR1iBMNPcEiL8aW7H9GiPywR1JdaCCJ1gdK4KOjR81XFpRg4tPoN6
Fg1ujfTT+0mf4hbXLYEv3x/dLp8vyAPiGNYJmWPwr6s3kD5XG0LyKYiAhx7Q/vyOH6Bs9XsjUCE8
RPBZzpqKmYeE6Veygt8vZ5lYPPg1D7hInNTz8mOwrlwnykO857dzCGx5riSJBN5/fn86vTZuukmC
V7KFRURSj3jV1PZW94In5Ufz4dED967LAstc8u1++psRkrlqPoePNAj7PPMgvEs4vU9C0vdQIhqs
ceEcCT5bXV8XqfGCtnFzKJLf1jJsPytesr97hpj0q8rDUFZyR3qPB63d17RH3x+5ttTHebpyRoOK
h8TNmJYL2ujrgQ44McWWO6JxR5C2+dF50zTVMhBfLG/ILoxGm9/e77Ww1I81GSc5JceAGEnWFvZA
LuJ1/pLztiV18bvAqfUCp/fESsdKh9Gnq5Xd5XxF/I2kApsP8+9IRfSUmST18HlKDjc3aHWD/vX/
XgJNmgpDOI6MYDaF8tj80VTAPycJgizdk3wFKJ52P0Jgh57b1NJHB56wpCA7eqFxRQk4yl96qWA6
gEy02/RRksiaHs3RuqG3cWwEj8QwVh28X7vpzHEu6f4qRTMU6y1lZ81EuyTVzvE5F+rfADxYS6DL
T8w1Xnxc1efkP/dZmxisCFz1ff0v6x1uUyQxvinyL2S4Fmh74r6Sy4pJwvddXIwJI7TPlvOb+Zyn
q3o2QjOHF9InIytqA+6sDVOxYxc2LI4ZnRP54rUp0+1cW2+BRKrX/wq53yYY/3MyU2NeBh2PBhU8
xtMdVuJ9oIT32qduitMMKpKnnUN7LpHLEMchcnGXn7ARaV/gBeSKQvxzVGpejinIkf8jmv7quRrV
F6EMCePA++mnM8cOkoM6s/S4nGYqMqTkesKqTws3eo4XMToUC+/P34aUeWYDEyqNbQKVLW7oBY2f
ldwAuW89jIpY1gc0ksWuEcmM7DbVNiWSdRbtbbM4+E99LKpzD3DHqdHNIvIOmgoRJvkrSqx4cNxE
+yYrEtwVEs6zzZhB80K9kaew6ytsUf331E899CQ3P8F1ouhrx8ywlWELUN91A+iAD+SidfaIz9bw
K40ANcogxdGyCISdA6rPPEZT+XTTCQnUUloIv17Fw61PlL6MrnBKBtcRiAYeOPrcWVTWjYUPiMSh
w92QPwDFgh3B6PvFFSN/fAqSUvrIidMEN4QcAHJggsyMLDcus8q5gsCFyoBNjMFw/JkqXFe+UUb+
g353NJ78MyJWduvAKXeOarAjhPL8MVsvmNUyTp8TCqtvpDKT9E6xkycX0Hgj4uy3VNF/CaWhhy0X
egx8KTBCfXRqlCzEYsFBqihmqMp2eKOYOz3aQfD+tO2hvCB2aK2rCW86P0+zMxcnIC2T4dK8xQua
HUWgBrGYe+JedXAaMgcJ5hoPY68OwclyihmTOQ2yZo9NQr8NCv0OJYCnGeo/md6NZUkBjG7Eh4Zc
871RY1TIp5ltE75XGB892Wjo2iRPe5D/bEFq0GPZlmf2Bn4Fa8YII/sTDDkB3iX3kM2X8NVEWRrF
5OdKO+sa5XE7zrgH4w6S4SPBWeht0AIOw6cPo26J6d9DECc/o2sHZkrZkKWW1r0zTdYTv2adCYEE
tBFB+Ne4h5w4rA8tUpld7UaYKG6wScgQRh5RvWBDlPXLgHUowBRry4pcVplBhnq1osOPEK9kBG7+
3J7uyYVKYy2GtcQDWEaQNPtKbrLiPvfPkmtYXmgrfjc7o1N6hPhY5h36Zw1/Rz8OEDf2xPnf6Qkc
DBPoqhHlH834CbvffJIZrYhHkZxqYEWi3zHEyDnZrhsqr6h8pW0DVcs0z+MZZo32LBxL2C+7E3mb
u7vg5voBX6oJTsBZJgj5o9d6PtxrxKj8Z9WnEeTeuYi11hGjoorxbwt91ulYd907KeW1bfWjoXhD
d7slk5uMxjdfmHTbgYi47ZKMOQGtSO4BWiBXAaQCouzr8BI7ZIFHDcq9bnI83X28tX18Nwgd+3sk
g1AyrDIUJ7bCOddSotJEFuYkxevOv0tVrlNNY7921rC1oY343ATMhtPyq7QLAiyepU2vUZXEDLRd
Gi92LepYNWAnXlBG7H9P0EFd5wb6gSv1Pcrk7CqbXssi+bSv/1dCaVk5i9DysPmGSSv/YGbL9dLC
Tq07NtmNHZr8999WKSLSNYsTG6Te54HT5R/9U0jo/ra4wKS8YfA8h6/ea7oSkKNXwJybvDsF07zi
GcUNmBfWNGee6sq5o1nb90VaOsL/3lDCuzR6ZkJBnIHZWZ/q6NKbi4US6PgC9wq8s0P05k1q6kvF
+B/x0HeLjsH6p9bEqFiyLTC05NnSm136yYx3rbKUAv/VPnGTqbpwpLOkznBwFDGbR5I6yDKMgZMh
OfMxcjWeJGXYSiXXmH69dZbjpbY7/tLSav5jUm8LwIqPv2bv95utoqlfFjN6a1oZtxj/Ljd6JF52
iQzWf/W7PU509vPzdBGau37U0Se+03a3DErUaqONH/1cbBEUoS8+GYtnuAV7n5V2g1Kf0K/G7jlY
ljXW3350T6YL3ySUiQ2NkJmKJM5m2c39D1DnXjlxJ4TUiSokmuDJUfczlNR2BI81cFYghUnbHz0S
ZpHwaCQ7Keh+gmBLEDs1vLV3hIdFNt3jM+eXhRy5s3Qtxt0eP+TgG80qwMj+dawTIqwD5wAVYCxN
1AGurz1MVfqFzwbLz7S3bdym6Rz6hSBBWiEKoHL9I8DMAZcURw3JBxTkQxW4QWPnX98A0+WYjaus
J+Qjxhn2BUhuTeHONbhnHBjGGNUc5nuIAlxc16rfe3DXeUTZBoDMihvMZPcjZabRXinu5n+P+Abt
Ye1etBApB/90f5weFbTxMxLDU2AuKeK/JRh27XK8Y1xZX+YpTcBIduGDf/5DTHHdw6cYq53d3SAC
YbG/NEHkm+NNxl8sU9wc55YXVWZisRAMAISd9c/yaq/OOeITTjojmM7OzuK8uruBa3FjHYdYKU0x
iA5zLE9Iv6EtbtMrg/pxqQ4EPViaH8F/M5B6RG99LIPP8vieAEjwb/VVYEceLfX5zvc74j8jQxWX
G5CwCPHoUWwZdwbW3rMbdgSHUEWc9hH11AofKJTa9l44X4B3p/MmeOLsE711t6xkNOuP7nwopgXn
PB8bEC1fsTdcdt1UKuiu0rTeIMZx2KBwdCAJ5BVeX2fjwMsDosOJIq6AADMOJSTwH9zid/wU7pc+
b2YejA/EiEgNUAM2YL9HXPCreriiOw6Z2+D9VE9qRhHJJH29yuzhX7Jc3htSy9xCLd7LfzqVys4T
zocHPte+zI6ltorsBd24r788QE0RInnqm28WOmoXKMMSurexXbFOzqG4O841QtafdaCJLMGzZWDg
8A7L4/o9FtcXKae/LJOptmpN++hVfncYdv6ZjOrUSH5Ja+A2uB7xZ/KHeJK7UKm5gzXS8HBZVTtA
jhCX9lMDMhpZGRJzvLh3ic+LaRHcEQ35fBFSuWcRK+YZ99Y+RJeYqDwDl1ahpOK/0p0nWhNa3VVA
2uRlLp/XNUc4iq5hkjwB78oMHQFzPntwLPnR7yaV59mWOIwhGgiPT5ssIdcTriUy+bVVBz96oMhC
CeJ+WVpnzgAYs/EKMnQS8IcZXOOBdD7PdMdM7Y2vrtnp+q/e5QE3zJHyEO8vEkgds5dk0bABi0Ve
GxdDtdXxZp6u6EKuJqIspr9e4qMuq8mBH9ZPFxW5z88u7Xngn+izYBuZUHBIGBbUZE+6c9pwJE2v
pBCeKzDLSPuQogKWDBV2/hfSIYsg3Em1xUkeCV2Zgyx0cqvgkxfzBIh3E7/+kwe/7wJD4c10ucnn
oDi/CHHoNPCN9LWJv6xgDt6SRjyyUya+2MZD3lhIx1xolKDudjey+V3exiONeLZuFjg+Doo6Zu70
8JRvXhdzvLwoehMvnJpg/Ai+lF9wflav5OdqF11l8F3XygIR3F1vmwKJ5hYhwhbupo9dXUYxrIyX
Sn4DH05IfEbXN+qrcy1QxM967+0cO8ucLqdzFHjo1rpf3XURKFgEEsQrrpi/WxvgDxHsVmueBjql
XYJdh2oE58ixwchHG2gqazBHzONh5nBji5UAjtRka+3RVJeAQOyJDpcmaEDXqzaj09MU6wU0JPYI
/EbrPZB18BGS29oUj03hiorHkj1zvHdkCmiBPpNFv6dYGo19Iwm+c+MLCBPMhS9ZA1i5jOM39XqL
ehO9w6v6y4ROhKnKEYGWV+6TT78e5m2je5/YttBnymP9CYBedYTo6Xp03d/YAvr11S7JVcxJHwqr
pFR4XFZKKtRL/p6LjEthQ+PXuEMuSXRG/nlfsav4AIRZrsMWrACqWBotM02xLwC9rVrx7dJALuV+
8lTLW+mv7HTo4qwQYiXNNBqyzP8tmF6xvUKsJgTtrCQkHgvl6D6SE4ArPvL2yEELhHK8q6GKJCZW
W5h4qOwApbbnojAkV8TfnkJF7CxZuCtydRKiXwFGJ4wi/vpwJp0oLdJv2rZfXa0w9v4ZLoKxXLlH
PLUWzcsRqIqZ8G859waaTpX2F+7FaouNspOj+zbjYPyu6jZsK9I4z+1GRJiKTZAWzHcnSlzdfz2C
Xyoq3h/8+4Eiz4Z4ybVW/wgK09OK+bT9r/3CQzGSgAiESgMUtPpEqzOZ6YTUITMqlOu4Tb1QnUrc
fyDgHGCnZoG8aLcANzy5Qb+lqcODQkfeb/LfIzE7PPD/g2ouH4Lg9LUAcEk1gdLAV1ZybZFhdmPr
q+qLdbKGY0+pinUwWMU2EoMIXX/kFmst4FLg2fNR7fEASeGnTcXKXnwnVqbatdLqt6sfkULCW1wb
9g6pkqIvVcKDFZZ/jChbRFS5+IEpNIrD9d7tb3TMoVaTe0fknt4/dAmvbQTn/uyIF8izh+2NoRCD
rUd07A1z5ajUEFUwYlU3hARvgaY8hO70tOYfwfoxlcZflH+gW6EeEAjSKgzV9BDbo6qhA9ft+BEy
Wf4T43P9Ozk0pLH2UNFXYt+qzzQVAAbjgbInyqwJwxi/Nm69UtplipaytfYZA/bO1CPsVT+JuMGA
mMRvscC4MWnexqL/WMlHDFFb/pSH4AFxQQEbxsPPgIF1xf6vv/6Y8bVlYlxanNXkUc2U1TWAUzu7
8OA2T56F7AjGtS9GeSrcd9wMk1XYyOUAy+Ha0vc7Tp8a+PZccksW16wcpCv7yM3NkMGYr3crz8wL
XWTaSFR8SM+kA4QgmVcDzGzwKbpiDHgnJnWZER55LOvtpj7AqbKRoAid4vLRr3L6dtS9x7g0QbnU
akjYExki3yENy5AKe6W7wIoQ7GtWkumnEma8obTA+SGLbHPXTotxwZber8rLH2czqHZlypfKH4Sd
tbB0lz6XzJP3idrbsNfvgWU5Vu596eaxXieZwUndvamthQTWkpAQXkUgsmVy6Ag7v6TgkVfmG4Ln
2UgyR/fR6qkBPcdkMjI5A2af4A+swACX4G0W1qMAWhFeJG+boaEc0xIPKhz8exfsbN3uTdOchMfE
6xwkmnfaoXjuhKFkNI0+yOAPUHc/KalB83NeYC7D0HDU9vRZT8U57M7IBs7kkfry7bzBVcwg5dFP
dX5j04VA+Er9nTmRy1MXCAEEZGyWVuYBE0epLN94DInoeVcib9Zs2nzNkIr0KxMTdHk9t+/DQHgf
GEp2V568mn6oRpjPkV8LmUx72WdcRLxtGLnHeajUoHIapzGxhulWz15hDZwyZBMiUg28EuhtPnct
y9p2bsZ2axwsTjBq9tBZM8eLssrJwsgL3eljEToK87bLKnMMCrkukJXEF55L0OdUCW0IOZ2W2QFU
599V1vT0SjcuJo9NvilPtuAwknJtQLtvX8DPH6+2SxOnVkhheBM+p3uTLKtpfyfrZY/ZBk2intc1
OHjvI2GhPoNozOImxmDtD5QRudAHK33r61UkYNFGDzpgMY9OJYB87kafbNzsP3YODbiSAbL1gIC+
sJySeknf9uozipt1keLYIXmXBvufXpIGS/EjFnPepQXQV9O8qD7YfNGQAvHCLampHLLU3MRAGyGp
M8HQIRQkRIaIvgY9G5vTobxVWbDtwX7H40VLJCApvOXRehqJKDv0ilvwBn8vX0tjNY7kfIikowpv
B6G+kexGs+S4TYkJPpeandtw9LihXkq04uQMIdkvdmNaMIu3NXIlZN511lTV0diQd0N6CAGYXyWO
sF7Hn8q82bnw+Q6xVKdSjQIrzme2YpMAn6l137a1NrgN6GkqW75GvIA+YFJYX2yE5zeH5Eq90Xbb
8XB0F6upg1K/8KhjHntyjW/YN3ZEXz6uEqCcHGuxf1USQvA0gTRhC8lHK+2x/vMeGpPM4fA4xrJA
PMBIPTbXJtFERSig597F40XExGof1eUT65G6yHm9UsqRB6kNmmQUlGvdyVK6j49XtaJQ2gUe0tDj
ku96/sqfrovfi1qaEDXH4In/H2B1jMUiw90f+mM1n0GgMvuQOZVbY474yX2era1L3tc1L4HMhNmD
QCg9YANVCjmAq79VScNbpNdHOZeamz/cpog9Mid2niDdZGkc17Xa2RArZt4DWYO0/arzRxgdm10t
1PFFV73vJjdSsao8QCoVJEJr4HqyUuHc6KkjELmKO0drXoWnSxVT9feBegkYUQAVlKpOnhGK5TOd
9qqkuOFeftj8cPFUrBvKsRIrkkHiPvZUz2X55adGv4tK/yK+EAKs8hr6N1ANG215ggWKkNCtKiGN
awuETKg1jGSwtkIBwPh7XDFsjYsP9u4I7Ql0Z+UWxTELV3gEM/EZLLmjQpdLEYwXoMCLPcqlYncV
Z6yZQYpbVw9Mk2ycsyTL0ChEr0dQWwFJP7lDBcel8fHQ/750S4NDxM4V2qXNblzCCJjKAf+yhDvi
xDqp/jVrIBHVDc1SfygyAA4C3UrupurNXoVVSeMjAoPtFt890VdhkHtsq3bAq9zlzI7nRj+vIQss
cX8Lo4g8vJvjlfp0Gk5uff0DhtxTMe4e5YTMT+bjbaoBaYezcm2yRVhqBEhwRfW9kG1ZGDoMy9X0
0nYC0vLrzmKoA2ATc6FS0KmyBI7lATBvb2jc36NpNQ5vs7/4wZJO2QxDy8mE9SWGdUSCrUgKbyNW
UjfERdd5x79/o2w+jwmXhLAVpBTVQqwbJ0AtRrudjBGDoRDUIaPAdcwb0Sj31NpkbvP3MbRqIWcQ
udA7LCjuHNwmIVG6k0JX+Z3YKdd8KbVrbfPIAB8hgw51ADiafp+DKyJCWooIP0uryiA1gnVs8LOR
53VxmvahqTl5KJTCa/SD6q36KHDtoF86e3AjzSzZLkoj62lvmdownO8cTmwRkgk3dx8OHfMxhWbf
X0jV/ZjIGrpa5Kj33vggEZQw6S2xVj8Jc29Ug8Wih3c+eUs/scQA6dUjUaJ+bOHBiJsAV999bxXq
mBNurX5X+Qae0m3xVjYgNSSCgyBiUqnqAfLjtEprWPCw6OAlzA0mkYPhZ0xkre8PLwcxlRpL3Q5S
5ixbQW5EKo4vTvLQN4nGCuHVjjA4z72fbVVibc3cmOfPJNKg+MjjGByH4sblsBpVOfU6izqSMwVg
0SjLc8bXSwLK7si0qw90HPQzYcGOmcF4nyrVqA1AzK78U6heV4USEnNO0/Cs3ho/xS1selwKAPBG
rkvwsn/VDD0+8i3rWlzw1ac1KUW1aNhFC2FLCBHx1AkDZamWHzCnqYfcVEJ9J/0XrUrsgUFKlbrO
BHLnxVlg9JZHnXs8KXDLSXGMlFbtBrJSyCdvnEXt2oGnQ2vrazP95/W1AHgE14Wdl5qSm2H2NLYJ
Ldp6iFc1YH3FzLp2IbM/MrqfWCwqPlanrpKM5jjaDuzUIcFGggY17JwGAd2dHUPp1PI6ehyqLa0G
VAqdjtg4LHmOheDkZVplbFtqxXoH6RdAWOlxRj9fa/ylqqhzfPa31AgZT6ycdA4Ch3l01PL158IM
YGqX5opDp8Z6yNJHx9ysMlHAZzq0xvEQB7yyY/UDTaHrs/99R7WXksu8GCqBYt/0o07OIm10puRO
qotO6LziRk+J7Gf/yAMYU5q9IPPj2nDD17wSdWnnm3mHofzye1kZNuyr/jTxUa+RC1LS7OQsonYh
mbSjf4vzophT4TXxqPNnq+RRGJbYkzeJfukmk/5cAo1Em8JeoApwtFD5jwezUvCQGE9zF0guCH5F
z2jyevgVZ3Eeh4VPFpcALJUcsU+n4Rs7UzX5rJoiiLTH/nc3fNLtjedpQ2Gl7oz6LuTGIuCUzEiX
WV0vL+aw5sLO6W+Zzp/qfQdSjBftSz/NshXAKT+9OjmKUH2ZmxrHiJ6hvpLFsOIibqyGtjoGW+YF
tfNKbSxnBK7jfN4RrdhQT8alM+mz8BimNlRE7iqtfPp9ROXVnS2k5qhunUZMKSKxEXrepMDvEBXc
3VUDysT1SNY0vIZFBIHnPyJjTF7K3/UeZ5hIgYg4b9YiIKC4rD9YaxyJhep3qKhE7L+EEpNbB5Lb
SHtJZtlQWiPw/FAc7j32Zii8iCD5UAm8zPCYMw9PEnl42Z8dzr4aMv1iHIR06JZsoK6tQrBA9oWK
M7ZlNklV4zfjMXx1DVgPR3vAynIsPG8GQqWDwQ3QjxOD117F2vjpX6AkUGyiNMjT2lZ9Lpse0JC6
OGE9XMm58Ii9ws6CtP3zfVJnzCzqlrRfjNffwDzJdxiRwZpCqWcVFYJ9QDcuP2xpJiO3hZv1Kdlv
aUTda/NCNTkY6HgOlwlQuShRx7M/c7eqzGiHKt0WZ4x4jAM9hDfyeyLJMc0OrLMujfdOXr8krFhp
zbpz5T6or83S3oSUHXEyt07XHg/tVgQFI7Dnd2UvUYaAulNhVC6YwFCXal7WQy4MI8egcSTd4yH6
qiz+Phtf4cmF106WsX+yZFrInLo8aA6NtYK3nuy9UNJSJ4fppj/YCxAPA9H+e8RwjyOxm4+RirQ4
mLsri0MOTb9/XAaiWhSltM6PYox9GkkKu+IzBztL/kfh4R24ttcYZWE7rzgWuQmGKjVd4bLpaRxi
BtPBx8aK9YWRtZ+sS3fqhJALOWn4hqV+Hj8/BDfYXnI7Ef+ZSal38CWiP2laPPXO+zDLiYoFVzlE
G44/rV8n0tLEke+C4rlDlIIBANFJcVE5K+oBAN2/o/lkems1kt1NNDecfqFUjR0IdOJNv93Ohc21
/TeS+lsZ/jX5lT0KK+PUlHxLL+S6X3SQcOQqBfEDaRpldtaVtbo5s95/Ho1MDEzk28Vjj9+fdsMi
/mK08rcd95zTJzyupDx6H2ja02XiMko2mLSU8J30xI4PMPGXd0ea5RzlP4VcK7edl0AuKE1vMNmq
aidPDvcJ8XKuZ7diaAArpMrzwS3slS1/arEzdLUzVOFY/ANjfI9lXrrCfcmyWl1urBccpK/s1ypj
GYtz9T5aPGx9f/htV3dpUE7JfwuBnymmOjp0WhGkMMvUrVL/8IdNuYC/8kjp1e0lTiD7b/b86Pmo
0Pm5Y7HN3jPTLZQZ8qNthBNV+Bg7owFdLIsKxPxyrC8MIoMEkLJAP7kydOr97m9TTPzuiHmtKp2Z
HZJfD1AJxX+71/1F95e0QtEFg/aNdE5Gv03Igz0ZTZPztO9vQD8m0krJO7pXwHM7tvnCuBe8AYXJ
KV3niQQ15KN+CZvr3qdPNPsX1t67jqfckxmEUF3KXLBDhlwI4GP8mZU2yVIU0YW1o1Nq4bgPauKi
GwdUQQoXF3r+mqI+4yhbvuLfnleSwDR2RRunIouAAWpL7Dh3WvSCj9Aq9MxIu7vOA8GEI/2I9P02
3VTeBd3uNdgSRZIYgyfXJuP2AvovkpAnAecSGpF8Tj7hEV71H3OJAKw/cnwWMkkfypyamlE5Ko9Y
npMT3nU2QThBLbxXa8u2QNzEFdH0//6u00bNuMkqflKTx69Faa7i6u1trVavXopAFgm6Uq4Nb/Rd
1AYWKJOY+aX57uV3zoVtdrpYSXOdXEfgisNmI6IQT++Ev4K4aQ+EHYtduVjIJldPI1cqGUcMZNEt
Ne1CJOss6FClZVZByjQv50BSBLlokCqvQ8Kii20fqEOGoKvC/MGhn5bcOM1LCrzQ5F8HgCACKAVD
XUp5nXguzPR3pAryi7kUll5vqEH5OKa0Nus700BmzHMGAGjrTS3F5NWgtFFZ4EuBgq346upOi6K6
Z/u0HXVFaEg0wJxHVdQcwwnJB1FB0bAuJtR7c6zx5EhxDY1TxgkuAlsIycSwcRIr/RCw96ZyWxLu
ZXE1UKWMk7q3/xe8vXnTQ+gQ2/lKhR/dLlhqA1FBuLbpSqONBXp38M2F584EokGmft6Nq1tct62c
qlqJ7xB8MbC1u4rLjmC8pY+Z3oY0aIkjaSq+06fqyPJ2jch1WJrcnjWNpOg6BC1b90z2iDFivIQZ
59RqPX56At4kMqgSCY3EOIDDXpIps4RRSntFwKLUP9DG09DvCuM2n2khy4QW+u5OSU309ZAj22/V
aep1+rcy/Da+NRDypFdNb2NvskGeSt0xMyFe1Oxinfj6XNT2CGqOZYKsw1+SXm+JvYbSwsl7dp5m
J7wG3bxjjoFJ5r9sny9JmeJVKBBhW2QD9mxOfFbzZRcxOokvJX7zeqqtoWwvxVblUTa/sq6hj0DM
QFZE672DzgGWTDeLJPAa2rECjDj8KnDuWzD5SVkPWrEPLFsNmI2IZ41/++zOyvJzgwuUdxkzfqXk
hF+Wn5v6nyx/8EoRDp22ecPw/w5K6TWUttQ2ANa2dpmII0QSsTAko33Fx5fX8mDsYnm2lSeW2B4C
7bdxwnrbH3UMsM8+QYPE1qwm4IZBleLhr1y7rXE2PyKeGPcJ+TOxNQsboCXXux9cSUQbQl7xW5/R
1WDKEQPlGmthadOXWmlya1KZqE4QJbwsvDf7jQ80ErOoItzrmLMd9lL0DWyZqNKl8wIBuaH4nCLD
7pG8OELlM/lZnwQDfLRubKcUSctibCeCJY3+uoidrsgiw4aEcLeVpNL/GBskASviATCGTulMdQqD
3T3W6BepR5LNbm4cIg3OacUt2auDfOXoNiDnogMOySBBPxrz8F10GLBnnwxGYola6fiG1AvuIEXG
zG+GNqA/+7ikDgOvcFtpoZ3fMKIjenV3LTaxVrJ5w2n+8zeIaxqa3vN56ohNGOcVRjAfB1klV9a3
6e8XLaiz9HUUubLP1geiHLyvDijBqsrltHj5e++4e/uQamjHpQtx8/eQl6zIE3nfgxR8A2KlrL4J
HyGUrp9S/O755baty1eR59Lz839IjRWNhZHqgu5KP7v1e3Kn3nOlkk0rfLH4AlANz+wgMD7NXW2J
wEmlDkPillxPsEPRFbEdlhw5fRBqp/5bhM5KQASeJVfT0Rr4JQJJJrAdyPM/KwNNcmVKAZpewqds
90ElbnHJsnEqNg40zZKHqCCeaGoAPdNPELfoh/Qws/CvmBPJHh4laPTHy6/B/giqCAr9z3rSH6V0
vWW3r6dvxPrcCRebl0m93owsH5BV2TjxZQJoGPW2i3oxO7IKy3K6952P/EMkbyWxBUaPGTBNQ30b
rFkBzW4OiCd6XCeyOsCFlr9VKAl9qZLUPN+doIvOoizBWyO9FuIzo0OkdoyoYzq3+OXMFXIcWcwv
C8gNgwL2LCKkQJoxj+/jrZEGYm7zeZdAZ4pKJlyTtQZaqdWLcwh0qMYqP6AjgXl8uCL5xaixxaBQ
inBmP4G8PmxTDKjyGJqq4+0BW3ahAWrDgobNHwdS1EyRpgK1jVBtZKN5R79UF6/QkYunBTvV0iEd
yl03kwJ9i7O1NJqStJu1gd9ubi32GP9sXbwM74RSrTFfeiUfs+0TtTEhhqOn82dm5+cuH6kQt4mo
M0mPIXNVaeXXDvmrCGDY1EQv1eisDwZBMhb+djxCfFatU+R+wPI+g5I1Jg7gXRGdh8zQoa4GVLs6
4nQoV+56QuMNHPBYR8Wo7qVasuekNIYSImCj8BNGvmhqHkvFbNrNgIJV71xQyetVtr/VqiR/kpFT
qgfB2OdGo494+S7GD7BRUTotZ7T9GP+xGFyV6hTdZ8JIuQBiUkysszJkoRHvywsw8SXwjuJ8eMQk
djs0WUMDIDdZiiel3D0JgkxxG8cVdOTnFoBQ5eCYIJ9UbMbf0Zi2Vn7G4dR88aMe7eIfj/5hjBjo
+AsZw5HYcZ9D0AQzKSHi7LXIiIGndDgBa2WUYxd0ChC6soFoHIJI1EQFDg1DxIOSZov927cw8BGw
GFHxp37/LLQVXB/z1YtDunJfFSwoYmcKltpGimU1ClqtSRgYT+MdDoUiq3vHdTo2A17Y3rtOurox
308y5hUyZ+6leAid2WbDjkVdImdJ4i9xdhhiPnJFJT7PIwZZM0RsnmEGCL3lX4Yiu6GA9tBCNakl
WZ8cFLSUpU9aLtREmc7f88Xf/JFVufX8lZPYkpqevr7T791qVI88Pm9tQWTeMevXAm/edcsq84Hq
Nno9iGjZiX1EMMDYXn/yVqNkZv5Xd7W/8pTZ6fVq0PFH+kSkYvXT80X1UMibWqUbH2gJqqO3Plwf
baZ6UZIZfQ+YAF98gE+3B4zpKagsRWF0p7aUp0auiMapAMHzttH+eBcIw+LN8n/YEVj2u4a5nbft
cRvFgcHaOXX6Dj0b9jwDH/ZFNIAYDJqAtaj4pOvuTJspkwHi7bVUDkU0k5ynQ7PqJbJVCmDBqVJr
S7N19ufyC1wS77Byziq+zaZqyH/UOoEEd+my0st+CDXbKnuAyNsjIVfPtV/MLbFp0ftVKl6Ix1eI
/+fhmS4Gn5gfPvVKV82Hy0iij0ZVxj+n4fXXVC54qmYdeK2LWsbgSW1y2rVf82TKcmbUWPW7CfYk
/apfobrbbEIQp1QnPWj/tfvGxaCVT4pPhStO8bSdCcK9vguipTYjXsknruhqd49OGZGVM6H1MG2d
I8jgwYKutY35/gSM39vAZ6O0g5bPWqLIfd3vqEnZkVOhPqRcbCrnHka7U6pQpTHTWUVhLJ5Rn2Z8
7s1sUE83c1Iu7dDKWqQ3fLk6dDOUzvN6RwGBUG1LrSt9P9tCqsoOQF5wYPJafPRsKmqrKdCg1P6r
wY1T+YQPFxNijyA5t17FeRppvds5YSTVfW56iKGI0pZ91i/Ph4hSl+tFUisCcgcubIlmet9ReOz2
KibsCUwuxWTLlyrno6boWybjrzs5V4b0aE2Ozd6c629cymlc4cz8JgSBis9Ck84E5epRAdK9CCcP
W7h2iFgH+PxneX/AbTtlcdfOrXyFP6BiV74JerM42DK1/70hoF0Af3ivJUC2bH/DjvA/ITwwAVxn
V9NKQf09UQAvMkac48F1nPI2KnocZp1hZIGz4js/AWc+QGp7ch3FSkyYB08PoHxT9P84vDOouf0f
NJQPnnKSDvj8e93bZQ7InyzR2LlCINxM18cQXf3jQJ0tBV2jOnqiFowN1OimpyfWBLqxCDZjUFHn
Msxg+oFk21/9Ya9EDfBbLpeMmTn8Yz31bFDRZBxaK4QpQhOXkgQL3TqbriKpJdPURfgP7q+Fb8A+
mAMW5IEdDvzt3XmqfbSGWEwH1RtvqHH4oqK4kVpUrpPObUWRESne6t34FbwD1JxUlan4AmbqZVYo
IZ7zhcPoJv/bffgPcJCZ8dAsHcgu70xHWoA3UDYOWUArVsFIQmY4Sx5XL9hG5JkQlwrQAwXzUx+0
Dyb7lPwlnmPwL9pANCqDm/6PPVpLmtR3ehGjvCOYTKblEp3EglaeHolW2qYAyQWnqhp43eMSGP1u
t8ETikD4CsdS6R9D0oCKflYC7HVa/1vB9a/0AYHwJAuy3zKGpLbZ/8qvZOD+mHeIFcHoIyfBEcLF
w+cKV0GRETXygYdmSwDnPt9FQq9XTgbTueHdk5ru8BjbkpCxpJhmMENAdFKTQ5vwmw4kEVn81aDR
yq40LCqvh0GTor+hqrJQjZNz4lL3T8yh94kFM4oBbIwVDOQ4ITr5BFAOcaPLzt3pouRpRyYew0Om
c1jJopDsi3qTdwqCYYOCZx/ZcCMXg5XL8tA68cXmZBV1bbbFs92phJfJAdYi+zU6PFk2zoti1cb2
xyB5AcvrCtHyrnCQbHGUE7IVh7IBDraQUaxsvXGWqAq7aPTg19gvdgRRkFXBBcWhdMEIAaTDAslU
trUqw2FuV3Yz8vdNyh6UTCo5EXPeHMx5neOFcvgG9xr7SLsl3EO6/vLPsCFrEAXZmI4/7j5p+6BB
FHnHLXtAnBN1UdaB4bKfMyU0a6WrjNlUC5buj7sVrxrXFkcEGB3Kvyf4UjuMJkPYsk+KEtFYia8o
VONzOkYhMR4HMPLK5dbd2FIs1iLSS5oXGP73Z1Y3rqNduCmOR3sIWISyFtyL5E1iIiDicpLMsTG9
QQGL9WvujlxQU8eEPuHIP2bb484Fxikod1i/1iGPEFzEJ6hoz5jPTYRNrct+MbTl0XxoZXh6Ic8v
cIi9itddrK9RIJVazYL7CEwCCtjVH67HQkdrOIbrRC3rPA4hkCeSTVY6HkmpKFeLZXokwE+ktmfO
p3nvkLHBwV9XDcF2Npgz0AF56Cu6XJu3Wzd5/uL3sjpUoGPnJR2P5J/CQJQ9oH3YNsLUoWSDHUw5
XdunGbmgu+jlTsEIKod/6bUB1wpo1RpbpFr6A5oiwk2LTbdF7/9qC6tgSUVryRi/wsNq/Cb5s55G
TRiHh11aTHrSLXUQ9Jtig1dkubWcUsIgUsRNtmEXl8XQca7V8a2wFMLrYRSRbBy/4UasWpIp+anp
+nqCTvc/sXiJl+4Hl6Ifx734XGVXMsjbY62cCTaPrYXsSjJz/Q1LxBoaTpfkkmvH1QvZ74KpMZmd
5Xo1sLc+p9czd2nnHmpV9mZnEeBKu5qxp1oHo3ImmvTjnoefc5LvuQS+aAo9ADwJUhK2jb3gklue
oSyBHyjSLFqB+UtVGeS9/c0zjqQ9zzDltv530HZmhEQGO2gRD0zPGfVXWrEKcOzlSnLEIY4oxxUs
SYzDVp1UoCc729WC+xDKjTsoY1lsrLWIkd+19RGd7iCxnf7CgO1I/qCqlGN+jlaRFq8f+vKGA0bi
K8VKgaI76on1nfuchnnuJ+Hw8nPrsbGtaMiytqYTgrDFMUbfj5lvt4QoPgMkGcTtu/feBjBjimcF
LVmd5RvQ1L+XolL8Jy21/Ii9OumFU1SeEV1TXLPm4YrVSMW5tAsrwfDv/jur0PUuz07Fhvw5teLn
rEJYrpBEKfGXM86q9DthpCQNsUnfCAEqw6jtMHzjVZ2lblI+/HJFvq1WP4RK1TMsTK83uB3hoa9j
cK12TYQiZbeUV9rMWRAJXSy3Ft2tmMkuNy0qprLfbUNtR3ZmtDag0TebfCgL23TZEx1C9icMCZMk
+ChEaDqT+DfiWXsyKsm1eMZLD4Q/yKwteDqSgvcgWvqbkCiTp8pqSGnFbyrC+orj06DITtMExWgq
0g/ozoOaL2yjIhZTg+elTLFTZZ2T/QKk/Gc08Ousu6HFJ6XS739usR6c/chiPwKKVdSoDC4fkZpN
uLPbnwea7lxRzo/udwVv2+NBz+CDxdAx22zVgdRQwWao/kB+49yeBWnoTTqEF89AyGCiFn87w9ly
wQJF+ea12dbWwd0dPGMhRrzlzpb5R1KfRLh+b2mE7HA8PYtVD1GNmtSuSo4itWQCU3z2pTFyAgix
GynOiRVuRAftHQzVL7OyKHmknwaWOdXG3Oxix+soSplB4hrft4IIWDFlkYRhpXDzEeABjITss5E9
XVDFxx9cheMQ4oUXUye5ED+LSCoj/9nHDAlLJ1XD6W/rHCt/mAWDqrwXvXg4IBFFq8MAePFZ7OAY
Rqd26Nidzov3ljwluWFXkjrQQpfNTG4kfuDJe57XmG51TFn/kTA+ASRf4o3YIZXVpVJcZII2L+kJ
lBC9OtJsyFdVzQgrJL0cLo86WOMDBVv98Vjr1XeYJ0UyluuwGHDkJtdsqiSZ+Kc+JpDDW7cKkr0H
6flhWDQbn7Od1QFjZZLnd9DJSwU+czG6Zw0Swasi2p/SegnCZ1KLjCdDCuYM8d3ag+kmy9Muzdk3
6uV6tWCjIQz8GWP3yUqJdWscwlBsQMTX7hDlhAAn3KV2WS5VSrw0vRogFghIv7ng6ItikJE0+O6P
JYkWHRgO0iw89qc1H9v2OYuHTJxvlZT5ZC2dMa/+Wfp4Ck/Z7qhk3UD9CC9MOIFtvjwiIdkWCGx9
YT0ZXuB3hdXUuhtOGFkVKU6toT884zXnrw8a2Zex2EKJnJXIqIwFCEVMOJi3dTYOxDoIxL7HPjjK
LhVcRuihuqsZAe/wPD09JeGvJvTddB7dr9xG79u2rYlbkoAwyHudEDemnslhzIQisAKKoeSD45/0
31D1LCz4aJM9bFLq/Y6pPja4yILkCv0AyANazq9VZATKC/2lePW22W7S1r005S0k5jyncj0u3AXB
v2B0bljFf/x96NTSYvUCMzSQrf69Cd8QGbQfur9KlJAdtgNfJSipTvgJL4qdzgmVpGr7TthVee+r
mFAkedEIrmACqLtwVXeAvEmR/OpBE/7RMVov4ti5AvLV3pmOlQgNCcg1nLC7s8tRPrlEvhnfAudH
OfrcB0u1pD6/kJy3fdgX4R1QCXwbtxJ69agwbsGIIMGs5gmeBgfxtDf360f2zqZc6NgPXQubV7yk
xP8bTK8ODD4/LpWAtU73cYvhNR8kZhRofHsEWMMYtMtPVeOUAfu3ctXpf7n/A/LQFW3yQr9ADsiD
VAO5ILbCWQYE2xnLaZU8MOIm0wT86oxJ9Vkuw16wKITOVc1M/36U/sd8ujsUT6zqgAO4LpR1/5OU
7Bvx56woL2pYthGhMsOrBVB4Y7Ya1pSYugFyaMez2J4AoTdtmVq5mQlAptomM0rntDAmxR17diPG
0ZES/hD49mIk6i/aeREUqdgovFqyeA2n2DpKlBvhTViSlrA5SxRY+js3XpQd5utXCPesjQeJxESH
1LLfPEQiAzXKHiDqeUVWCSG5OLGCNYmi3ZRDjSXb7wficCtCzBbSkMEkF+ZN6+sytb1Xz6geZZbN
gmJga09M0DXiIyGHshHewTp3TBhEV99yx3Mp49voqFZy5okxDxEGs4AH04QiHt8Rmns4mVoE59mU
5cGXYXH5lh2JdMu8VW1gBU0EIQt0B9IIJPipn1u55ymEJZLoDBwBF82PJynSMot5yamCubTL+ggC
z4xL70OWQCfBgE9svl4Mu3TFqTOgc2etncQeJEOdw39aUZJVKy6UBy/Xhm45ssYXr3GKIGT2cn3S
x4WeZAfu34Uihy/ZwTNQu9h+sVZJs04aYk5heoL0IOGHN18VCfFScyu9iojAAR/bjFKNq5A4tIK5
vk7Y0k20XD1qZBsv8ZxhV2fM8LpdOFf96y75efQo+WIFQn3MhGqa+xI0hfWRm6eTLv/ipj9TmOcO
RlqX4wfg+mM3jAqJc6+mXmGoL9wAmfntAEj4/M6iUcj8tYVtdMa1dXfAU4kxugDHkLOS62haMjaE
P/70VcTPmPDEcLCe+lHfWOeWC/o4K/10pSc56onyUAX9dVvh/Lx3oIqzYjqaIuv1Ou9BANtTGTOJ
LMIkvfgdQGk3/mY02dfxZYG8wvOoazvc36xW7kcsdy/U+LL7J54iftZ9L6rO/dAd4SU+IKM0FDYi
jAZMq/v+rDGkAWTc2DCvGVII+AiF7z3Hd252U6Z4joucuDNsQix1p6lCkE5T7rmSn2sai0HoZ7cr
Q0IhISkq4UzgirOYWSA+ODkCStQqDC2mPtuAEBFcDwWFUxvgHurwtMncOefq5OUPQAd28TDiLHxV
Z5CEk/6dUmYr3q4uuy/DZUM/KwQFKcl6XAUk/iga7jPUrT72yjR9lD0dofUyfsz86o7yuM79x17M
/riW98MdN4KF81sXGqhlQUe0SqfqU/n7EGv+Rz9V0NUNlwd97FPXbMi4Eioe9u3zjsTyPnreWLfN
jHiAZsHwwOf/kHDqPPa0m0E9RXseB4hxZ3qN7T55NTAqkgkhycvzB8XrR7TBT9y58iXHy1VyVWmL
Oln2i2/b5PkAV9GA7r3VyrVF6GZ9j7Y79Vh5XQ6ntq5bOAATZz5uFugZpx8JaoGYohlG4AmExnD4
u8HR+T//hFx4VhZ/Ea5zUAGU1Qyvld+/4TQnmMLB8ZMDCz25iKZeY6QUn5flhdFdifhx7kCuCFcw
O6spoQarid1v1pVf7w1k60LGTyrjsT55+cOT5OGIgrOjmCExoTlxN1fxHgCjkLfZEGUV3nt/vrRd
FBqz4DBJrwX+8N+PUaY0SBzI6RBBjcnBaWRshtvoAIpA0xCr4QRigWZjp6ref0zLNQ2cUauf4XZL
CIYcDQIhfpMdMSCYebHp7yLa0SN074k8dbzibZ9b6KRkHLeXjlV45re1MuKNJOW46ihSVpUZ28ri
aHAsSbWqAcJQUA55ZfD5nmaGUGoRW4E8lASPsXE1D4A8H3yTF71o8veJ+4Iw+/SzynahFdmUrzkg
+I4BC9odwQn04+VVsjasqOE46gER4/SCeP0AgP0XA1peTJPkUjEOTlrBrBvUBDiGi2EsO6cn/tfx
uUcmJIBSXmN0LMEporZZbmJOmvuRcFIkGzPpyCYNW9wTB9X3fxVYrlQieKfeluz2M23lUjqI4i3s
6WgAlpzQ14CMZaPlT6lYbPdhM4oOLl8aHz4CnKfDjY5VwSaTu5ATte4Rawv1jt8QLfNXfgqi1kGx
apolY67WzpHl0SW1poeLOB7EMG4tc1SyNP7+VyXmiLgRJkw4ldRkHaAC6w5iqXGbVQS8wZmdbyX5
lp/HZBrqMvvOTOC3TOZyqheWTPWHS4AlVfKKANXUgip6YL4pLrUdblZFaTD12aZtzezjCgrfxOYn
eh0QC0ACTKP9UiF9oHA9+7CD0DjIyX5oCUFgiM1UsRrz72GWso4bJDhYuR9y+y9inPdMX9ObxvY7
3qOqtfnJzHfJ4b49bVOQSYMZnhXdn56shQbSxODgiInAOLUVJ0KKIxJPN/E8lOV2Ix5S2mg5SW0Q
ADoYOrQ7lJFcJv05ig/rpPxfHJfeVhcSUC125Kkyx9lVSoMMa2ttIErAoa3VNoQvzmZTpUQWmOOU
RHNKp7K5HSu3SNJ5Hpnb6GARLMlPuMVnNDHtYruKODgXp9Cr7/PEcV496UrJ2msAX+ZfL8NxWQN3
hvxHgf8nSH90Ed+bpHsddkjtf5nk73nyOZT/eEYWfhQF5LevhkX76v58xng1RMgPY7Sz8BORIltX
cBUXtmmB6e+MKXsSCrc1nwVVvcFi50DJdnK+OqGWxD5w2QC2iXHU3hFnHhcDqdnCBRWn+YekKWp4
QzthGdiImdvteM3acWJ9RP+BJFJbSs6Jl5wmOcxBAwc8rFNSLtIWHc4yFLuu7XhLdkI78VhbSzDT
CEaF+Aq3nvZ3b3d1Y4jF/fj3nbPQ5OmF991PiKUi1DXu/xhwRkQ7Q/5YzgRVPuz26HgIYF+nBxsY
OMFgUx0As+ZbLQtc//hzWcjcJQqZdOmpMP/G1NQdQKOQvopYJpa8IoVA683n+tvwPU9l3BTrey2L
S3/Z0iQfecsleX2p7qheiM/w/vw+JWyfMxrWHhbybrTDJ4JIO2FogM6LXonsacF5ykgZnqe+Qgyp
5CAZ2w+t0QjeaVkhCcEKfGOXLlbkPUEWvsTR+CyMIOek/o0LbN5lJIKI//qVPqHRnFlrSbVOz9+5
Q+0T+Kjzk2lYV/518qc9n9HTVwp8NlqgCVK34Pu3wqZg02iFTaMjvaWRwxAAIjaYKgNMdhR7B2Mr
T9ONI9BN4lXN7oeXIfdRu7zVj2ZmNwsKKHqrYNelsIMVL5uHEYlQGC5cvcyvmM1vWQwHWNEo5ZJG
VNEuAmaaJuObdTAlOyExICdwOUJC6KUjeGj9JJfAGzefgUdWmzIdJPS0f9KR7jTsvBsHwdvRXaOJ
qBOweDO6NM2aE6fDqZEZ2XhWEJpzjyoF00s6ZDDyjhBfhQ+ZegYjCLCdHJAF+VAJRo1cOooho3cD
/wHU9PRyAHN73E95W61hCRzNuD4l2pyH+N+uzweI+vE+uFZuCt9QKkwwEZv6uo+z5aBOOiIi5hMu
kp5aCrTOIvf/gdIGIymL0TgMt3sjJYVOH+QHI6qLRXpWc1oia2E/Ibn00D2vxBgumps6/bWB+ApG
rN0mhGgWmFmbaTspgH5iI1ChUjQ7n4jqT9Tm+ZHyT4ivTbURJPZMnbHlY0ikxi2UB0kmc3oz8ZbX
WXzNF/w5+1zzeG1mMdpw78R3YcXH1p+1uV6F1cIDBQqdzqYD5pjkhjcKA0xlU7fW6cIvIkh/6+P+
ZYKOXdO0NyEk5+wOus9FSEP4/GTfg8esJABF2gUIJrZp579BY3luh6ZsS2tvh5i/FfaIv7B8WYm8
wGMYkgRfpDHY4tT4nHkSqJsnLHo76Md8zqaP7Eef24c1+WLc2U8TTCDrMLqQUFwBO+mXTSHAKPOp
lAn8dHpXhs7tRVC5XDrB0kKF8sBwaouLAAspRNa3+xzLxqTfvVpE7f9Ua1NuceueM+jRAd3UcGVo
jX5D3QphSnDy9s5CGZLiYdtjN8wIrmzauUB5xZDK/Zhf4B6e+2eZvmRRwyS9X+6ou2pIeTJrPrHY
ORq/QeyvbMmo8euItKPbAkGXZ9ThfwrKki31V2bHCi8xNENnW7nNHXIbhqCVTNJsy0O4vbm1/5L7
ziqPwV7Sj9rsnu99caPyJO1hEQTPq/Egfw1/9jjTkHIJ1OksZsraCVVBJy/wPQnNNR4bwAPSSUiW
NRjQlvIiUWmEEjUII7uhER2PnN3Vko/SbNXDrQrDWrCxExdPdZ6WiOQjNgAKBv4eoA3IqF4NQ2Cm
JegOHeBPVerU1O4HfJWBNq8HhSul7Es90SYqX+pY5x/CxxgZy1nC3fPhcH+1s7lucqXfIP2Dv1Dk
YKbShupa+jboLq1VGUUgK2WBakd7SCI34lT4KY1WiJt2PW/m/TBRX675Sz3dgWw6fCD9A45VErzv
5MmSyLRYv65ikGtrn7uqoTC99IJ148S1NEUqRvTMmos7j4XaXz8XuiBL71Ttpfe+eBgy8tsza37d
VMQPNncin3p6gZKO58PjfFx89KiWt8cT0yBdksNEYfGNU1SSe4Jdctwn937wwNShlXA2w+SJFwgS
7a2jul4FJIRU71ACVmxPymCoSYiETIqX7aV1A6E2m5jP2dldfQqoxogauILrWvVlT+eaZknKXv3y
MZfNIYKjM9phGy1Z1T35IE9cMnVIM7J28EJ2ZyFD5E3buTui9f0dKrgcfxUXSeSGrsgkjgS6F4zO
wWpUM6NlCOXkiVtYqmK0gGmuOz+//Z+i70/bB516ovHLq1gf0hpzwR5Awq6y2dovAHtwP4Rw4BKH
S8ay4bpEg9HdYs0Rz+6AC7bCnFecRJrO/uDsdkRbZFZSX5JYz3VdsqfITRGjArXjeix9nvb6oITn
RsiMKVYpRMsebxy2n1mftQ0+1UOCQgafFqYT2Ww5DzNE3v5orY9HK9dpZ8tM8yJcBpOtvjGYfu55
4M0mPq77i6wdcWP/Vc7LianesjeHRFei3Qnaxl8l+SOxPCUL0EQTyS0irc0TCR1nL8W1DkAYQ/FM
9pouRuG2zpfpV1wfzLf5g9KDxU/xBoeHP7W9vRt2GVNNx+Ei/bLGDbpbmLf1obTmBXCjtZP3AhZB
oa7NvzBv/Fd+aBtzXlOplI+2DMvG0xyVNP1T6cdMTCmuCpPG2a+anmpodxf8tjj6OmE1P5Zmuvnt
9AiFcGz9bbs92d8ch53HWVGiGOe9AIIy0aYdZWMx4DoNUQ/HN0mIVVgLKTe9fxPMTeH7OAsnViVT
djC72BuuFO3DO3ZsmWzkWzpiOHDnGFXJW1FYpMdazwiT0f3S6/uLJuFaTCRdbsJEW0UrCQt91RIg
8GlGSPE7TaDb2pu47fmjPKk1GCN85dYi1yrCbGKPEBoVbZNBrtUoF4IEGW2qi+TxzDUvjAPtYK+R
gT0An2de4DPDd/vdHInGnmnM3KdGTCn9gbCEVMk/HUMAiLLwfzNHu+kaGaGGvXQ+MqytB5r8Bzud
hm6YtEDxFaNtX3qRuHGTxFsrZ+nbm178sEqLVnnnx4CJnbOTbUMx7vjHy0Uyjreu8S2qKKTmjbFE
3eFyP9ob3TIRJwMKUTHgHMw/gCLDUmTZV7IWGyBuxm/U4735HB5x9JpJ9rXrex1DQL0i6bRXXG5L
ofY6fwWtUCRG0sVJ6F6CNTxwjswpTPqJWzbiNpTaYQcKPejx1M8EDjGmE66fAVE91mjhHtiS4CLH
KbkCm8W8QzNZFTE0EaAMzfBVMKn5cuztMcHeFSlR5dPAWbjEsq6Vugk2wmtHxXKh/xH4klhXpqD5
ULqGVygdtRtktBk7qVQJODpevCZCLn8wKJGtyRsSwvjmB+wgk2vhXm0Q1N0rV1RJ28SQwiT7iC7B
2TzEy5a1CQ9ZJfm4rD3Q22b4OWBmGT1luOxaH9aWBCr5LguhwaiPI1sV9nKZ6OFadLaTFeBqTd1U
Lju3zA/qfRrqPtaVse9ver/Cas29dRxFXFzc+mRBlz4Yo5nqD1G43nktNu9YlUOSsp5N6voXhZdA
WPGLx5lQ32r/kfb4kceB4TLpq1Ei4ITDc4iTG0HA85vYJuDY/+EQM7aqtZtCppkqTUMC/iNj4Sto
rAskH0I12XYFQ2SReY7K5gvICGybdmoe7FKTK+8CY7hal9+ryXqsZSr4WoZ+r9r7JfQUqd+HTqTZ
xEqGPJZlChpBE8xS5lwaQHH7Pgj0nBkJz+EqwDi+JYA2Ef2vyFZXImTamX8oEE4kI70mqHMBjnK0
rPyEEX+bsnVuvf13qg+PZUjRNZEC0CvvQmFB4T7OfBlP4ZAMF+S2hg9lYeMsHtcdyr/9k0k/mbgI
GVbOTranlnJYYE3IOiIIpHu0MY12DrPN+NmufWeMcNQw6yLHTvdIlHCLVTwmtRQehc3D6x0LP3Jn
zBrYsPaf6q4dEuknPXzoLy5e5zPv6XyAqtLTz38u8XaaM4v7t4fWzSLfojiRipHVedEdRR79QfiW
hi1IGo9uHbPQgFVxLPw7Aut+t7bpm8rbBY5sulkcZP8jl9asHnxXeFQfT8UBZlRwNmWOM16jiXcG
dcqQskdOy4/GDE5E+A3pwgsWoPTwQIHjSr4AxOtTeL/bdo9WbD/OfEyFpwb1rFtLe6JHKZzbVImd
omHVyYZbOElV0FxtVtrdCX14OLfxhl5IsSjNBHfqGbHs9YIlhlQYvHlTGXLDUOCyFZ7qQT9qTSfK
qSEtpEt7+KFGhlVvnybFk//9EindI7/SmcimtchzMU1PLlF84b7UO6uAfdf4lR+W2M1xesmic0on
rv9f1w7sOeVRfI6dvjlVFNvMlEVmPI50txAaCBDi1Xk8y4Wm5PC0UzkUw9LaC3MsgXEAHhIxGJyh
ku5s5VKPLuqCCcYd6LfTpTZxU1PF/Sfvkq6iVLwMF5Uifo7FIYlHsSWzKQxq8+st1UmGFNNWRXhD
HNSusDWjo1S0Rm2pLLKrtKuQ5Z/desVHiBqjd+KVuR5KS8zpjtzpOV71Y6eVsjGtcTonOWO7+Nrf
1kbeJYY8SAw460CIuVMyjYN9W0hG8rgSLowOyl5/zenYb01bIpRaXXLbJbEKvISxMoT008qtU3r0
j+nxkFPtRH61ak64DIcSO+svCBQ09cBJl9hNB06C7pWjn4pFTWIUMyMc/cMTTu/2gAozRuekJcRg
o0FxMOIfO+e1S1HWarj6h1Hg6sZ1h9+pNH2evMSJFAy7UnUGL4YADvO0UaFsM1WJQbwwEWBPW2VB
C+90jlfiYI9gRVBv9sqphvByZ/uAH0PMVz1w2Js5GrZ51WzwFGYK0qwwcpiMs7GX1WIFPUynKnsN
cwBNXKLT1tOl40gaihjMWyHX15Iv/a75nTI9fKLaT6u/cMeYH63uFOKixp2wcaBWwaYSgiUmViL8
eJB2B7w44vsTgoWXNzid1nhpz84np19OMp8pLczl9cYWhcHJ3nkxmrTfDV5bm7I1yEI1r6a8MNfP
8vp1VzIjb4sM3kiLbVsl9Qh1p0adlWlnWuGj6lSC9yWAUszNvi2Khl/ZvQPtOPPOnkGDFuiyTdne
KB+gjZA2Tb58svTtNxcR0GdntP1X2SgVIL7Ax+pEZ2xtRF1wkzCZza3KjPmwtRwMP880B4UJ2tEj
4GSE/ARm09ZbRcj/TnpDvJtHVrJG44TfZtKJCbC9Qu0rZNyZZXAWjNJqxSANptD7DgaNKutG/gXy
ME+RExqT0eycNAYq4R1Eq8k1NAImGqvPRREilpJGJMT+B78OUZ0khfA+p2to7HlQ0Aui92NWGvnZ
vaPpRXVjRw4kdB2MuwkfknnEzbcTOW2dqvNC+kxYoze4zpDDvWaPL4pBmf1CF8uN71gIPCR2TovI
edtfjzHyBZqB6L3UZChN6SVerAS2QQIydPSs0C/Q6Rz1sWP4B/oxFLEpQ2nXy3Wn4LLY794hmISS
/zhg5919QuE0KT+rnrBFi/L98waAp/uWYufK1ia6GQ9QIjdWdingXRQ75/1OsEbtw8ibRZUuHi+H
hiAI836ONPHwO9jB2oLM9xNQ3gZWJmhn5NR/Vzo2u5d7T02OiwcCI/TIJL9RqTYIk9UIhTLOYF6I
l9+Gk7O1QFJHViGNgfd2bOMHubDThBqv72C7SI79mlCXzd3KsvnoGwiFsZfTfvE7p6Q53v5V044g
ID0cSK/9TQYCVuKLhuWkgxe63hIaBCS+vNMz7J7NUpPzfsXNZSB688VxTYWxG9YyAWP5ZU/d6Q6O
EKl4UyvufYnGsDyopVvw/c+FXjazLIlkQFQYgA3lPMDqNLkiss4L/2a6Ejh6ZYSrnckvZUdtDL+y
47z1ms3Tjdy7whoryLPJP9a1060aWL7hqLf3HShv498C0s1ifJ9aQTFLXmTw+0cuAAsxUGNlowih
gwDdpWr1/lPzBDLGlpPlQZFQZ+O6/omBnxhOTzID30RImaI6/eIo2fPzqpEpuSt0+eBurL1rotPz
/vofMj99oCEMR1Si42bq2HuOrszfMZ2Pnhhh7qzyQwl3vSVhkuDC3wagjaDU1ZyUUEJCG35ajEYw
Biu9IvEvOnOsEWOmiOf9Fg++gLx2DEU2ryADe6qwCZ6dELCZWbjPVvZ3RYwSj1ndOYxRb10JaUzd
ZHJnA1YJ9DBBqWKE/uP1hG0/gAgeLIxKCIlBtXmrCyBpmIAnEDsKMTsQuGbiv64x3MjGrX2ph/K+
jxzeFz4u5wCq5E8od61oMLBprFO+5bqZx8Lr7kXN1LUVfEbsBAkiRmxhIpmMsCGKtqlwZFdCnNoU
8q3e0GbtJo4IGs3Sib0DsY3W/L6AryhianhwuOXTFLNsJWxHmNCNSLtWAwnEH3UwNEZLybdeXrCE
5u3G6b4DTDF1POQ6WsvpXR7kz9URBlLe+lNV05/X1I47xmhcGxGJFPWShUs+/ryi34pZBV/YuFdw
fOpOYSkPzka/FW5NvlL6icru524ZBtE9MsIjSBKFOhcrYnYV1WwHyhHoqI5MFk4S4Ty3d0dEOeGB
F9I2Lew8fM64I4AvXef1Ebz60NKyTBeQrhhvoRoYt+f9ufOBtH34mjbpkQsKflqRzPFTGKrwlUL5
Mt9y1Zdukmaa73p1UGMHHbE6pKFrz791c9AeMx23LJHrru+8hs/Ipw+GsE2RkPkkFPfYdHNYmgEh
NUK1Pq4kvuSyw+WNNHveuur5VkMBw326PHBoTo4zM/LXEsHVnr/BPJoizaUYo62MgkxqTVvlZFMj
F3tnx3iBpKTLBE9UwKYp8mODCHul4Q1/Sdot/XqoFyLkdidBf65xACLFwDULRkzzkzRT+QOwL/bY
df9mCNABt6SaKKzEltkwSFeQw+b03xZGqjBG3+YvCDC5pBjXxHHQMS0pDrNFC74oaElU/DaF1xvn
u2x1N4n+x/9M3Rn8HqTSGvrUAjxxR8O6z+x+x8aKx3ajW7w15klEObqEn+FKGnQEEwbRGSyVNU0F
OckukChgVHIdSZHW8V/oOMRJ6uqwjn5o9H0Cm1ECzS5uw4nBKKNfsUs5KCF13EXrLpOmMhrWoy1J
juYEqJpbYi//L5lIWhu5tspjnj6uwYQ+sjJw6E+9137DLsArefzfz5o1B3xZL6tCgMG+qkZhWpN9
zEwelSd5IwapuKQ51Hxfxn4vvHG8tQp9bFABKi0MJQ2KtVo2JZsbNs39MQm+L4HoU7lisEPkxEy3
t1GpXJqdb2qvthIL0tkIC6mBpflkceJGyd3bsG09mnoKJt9YMVp+AzHvVHqAsMuBw6xjUGwi31Dj
ca/Vr+mJp2Gel45vAPad/QWHMZ13poZMt3IOEw9Mq/fL+KSHBlxyoGeFepf3lyVa6cbYcWtwCCsY
4SUff6spqdvqF0BENasYvgyDhhydwNEyIMFDdeNRBgNFCE/HHBEn5Hy+qcnVf/7aeHY5HbECjZRS
lfNh+hXENqfLd650bq92hDKAVq/BasZ0cII0bRXKpJshSSKWlhqmfVj7M4Glxr6/JA9EHsKPg988
MkW9AGL/kXWNWAmDX091yWyhX5vvOMj3Q6SZKmPn/QKp4K4d4ZtRcSywowLm8MQ7b9jBN8za2E4Q
ft+rtfLvsR+4NpJvkUmtB3mkZ8j2X+f4JN/uFmS9bAvuTS/WXje/CsM4rxN5/hPCUzTd6/r9Gsox
VGpHED7Rt0oEUWGCbPyUwwVX2PIeJNSguLRGGYx3otHKFEqhmCYs1zoINJAdQ5rV2Cez5g5+YhSj
0CS99h1fDoDu7SwLOPMi/SHM7WAQj8+d7voT1pQWMq3Ndt+pjFMQBUHzPH8XJIsacV/Tgb7mYDQv
fOQVsuPtSpauUnDc6mJfd9JM6bo2XsI0isqgN5DqrRjfI1sXCdrxqIxKULxGeNN9lSktlkz36ty7
BGYOaVIPC81FGmEyuvjytxqQ3VnC/fiqzOkOevEde3X1mJc92CMgzP55470bkS8dhKmJUw5wAtGX
viU7S07RrfRcS1D4lt1GAq18Wq3YoG5tBM42MHw0b1s+sv13nT33o9Vh8NvRJBjUWvr61Hq04HZb
JdyA6Y/NDB+x5cuk+Ki5s4YLWiclXXv9ZLrKVhrmSpuERilZRAH4keij0/fZARDs4YX60csw8ScZ
UoDnIbwxCac/Atma7Pk6Y0lFbpKAnG2NKHv64kkWhqja53G+V2afkhuMFO7BAfttLiQuwsgBzU01
lc/TbWZvUC3PY+scvD2ZslnrHsFTiA3WZ3Rct3ssxeNtt7JEUHtrgX6ujEKBmJMmvSzYY07oZaAq
M3xH+sFQhPYaUKJIs168pBQRr1yppeJms6j5phltwZv/qNjTzH/hug9Q5DmtFhq9FAeZE3fsFz3C
loRt/+ufBrRaBIytuoCUkfBSvzzcvt4Y1RTMPpFzcSC77WwwRr7EvrWXWs75+a/18ts2qXvbGj0r
brqfTeDmK+cyvP8X45bGZoroXgQjvDVnhyImNZ0upNVWDBxc9JMXqXfMIxfzgzXjNx7+7luV8q+a
I+krd7DVMn6WBtmtGjUu0S7OZ4I+SKWcC8CBpjS8OCFHvdGDT2eiLAwtTCAtwoP02gu72QPYhGAj
OtP0tUxUBIjco6qB5mQFi82MG3/6yZIHdOUNEHG6FFbkqXA/DOULprogd8ROIJ/stoPufh8F9GCo
JO/MkRdBSVRXhiXWi9zyP6JlYJgk53NB/V3ywv/gP2qAhBrwyoPnc1ZOPbOOnOH7HYhusgbJwg2u
TsYK+w+pJtosn2caPnBaHABajO1oI9/w/kYnUvL9Asdmo206BC4rs9GTGlbfeVnO1zh9bs/AIt1Z
vjxZYK666YSEyVm7nnOJNgzPG1X93dCvzbXaqzQxVJ2ok9mp1GI+eX8Z54RbOgd/i25Invp5y95z
7mc1oT61LdV5xLJUxzGo/5Uja4WMtYJiPVff2FJFzdT4LK+Z+MJWS6eD1e6QXzKUcs31WgB97NLZ
1lAf7cqPKmkHnoGZDujin7tAHwykS3OeInmLk32taOa+XXlPJNhzNeSWGeWR4uQ9vEbCTvVUVbOL
u08AJAh2gOVSCT8zKHLEL922CntgB09v+eIcjdcF3UKUFKOy9D0NVAFan2cL2HuWhTA5J1iEdmWQ
TEwFNH+WsywNIg3qHwl5r7b/fFTNL3maJrLnsNq4om6LAeniVh8n0NGqA+0EvrhTb0om2CLhSNXN
qJLzDpxWKhFK5ARrbN3YC5QVhvbvtfzsJsYufh3UTkjz8cuZhVSbYfiTmYTs0UCSboTPol+SZmVS
PT71c/fJnuNWKQb+SCxIaVd91acDy2lURmCqls2LFtL7i+Attt/V1pxOr/opfSZ1AqTi9C4gGjI3
LwhgoG9tmtebteOkB+VbPIHSOqmF7N+8sf+tDhwWwj1y9mkVh6JxTZg1dl75ABgDHbkzolc+Q8ZZ
0oSUwgNmorOiAK8o85PfWQbaR1ucL7AV4UpQeCzfJpJdsCY49umVJCpjr1tBNQyf3PwsN6CSaL5p
TVAHnPN9NO0dSSF7ZpCa0itbnTieRpt3cWUGSVPPOSgm+NDuq80vWC7I2RAm3QQGGt41iPrUPjMm
GpqA50LCigKUcYpkKdPmBgV8dS/VHAKLTa1TD4lXN8GfVdKK47K/TGhUJJevR3A3QtzU9MLOsF60
xPMEqIzSFNmKDvEzPZO3Ik0/HobWxoq8zJQiLYIVJR2Mz/OrcpMuArsaSRlFXkEz+LfAYnrpMQ4U
YoEw+F5Dqqqy+h7/ELymhFZm9bv0OccZhxZKikUKw9m8gserj6Ugv28PrgaN6c8VcbNSxQraZypw
PD4FYhnEMvmuSG+VvF8mjlZd/Vy3uZ2fRS9EMIRy0AiHpI6sTczBdX8LFKSeWMmyfkK27TIYFsWf
HRpIPYzRrUzANxqrHoauQrS/YCpMU58BcEaxw0nJ/TWYzfwg8wS9MNLEj6hO4r6kZM3KwL7uB9g2
3vBYbXMMJsUbgURnGyvhiTuA3pZBjurWMV77BA7vW+ZbfbntjBjASYp1qSIJHCsmEah0aRu8hnpa
xxKJwzQl3x8YY9kC28UjPSqDHWD5heJ/jeuqN3uBFxHgSJ3TH3YMitYGP8zqha5IOpQzwuxCHpSC
E9JKZHEWR/5BmwuVpeOWP0h9NiEi/izulWdtWmSm8djl+kWYdUOGn9F34VBblcj1S/Mp6FnGl8wr
QQ1UAMrN4tDLdEIlxFkTSwUJ+xnMZDGZbeCfRW35DSAbabroEiF2TBE20TMf4LaHoJKV8OFOPg4i
cNz07mvDgBn1x2En2/cpDjSM9YpMItop9frCSJITtwQ04Mo2B+QaCz5Neu6oHyF4NBbuMOfr/0c6
dTPBkjOEw9duUDA8X72Bmy7GMg1CoVIijMKhKE8tfEGBMxd0jFQkTb03mOVheSkkWZxxwi0iAZNs
JCXEraJkp+lW3Gpl+z4Rg9sSuYOdPNFwV3eGKE4Zl6TOACddR8lBNxzWeA5Dz9KICe0LxSI7W8qQ
8Vg1LH/qomQAeyAGy+3zaOd/0+GCwwS4l0Dgfj08EJnm90attTmoVR6Qwu3GYKjw6NBdPN34Hbqt
n3S9TOAcV6qJiy8YZ50eKimOObSJPSrh90gzUGsQfjG2OrYqIzgxK9/aQG/k8OyvbYAVtwng/O8q
1wUJ/+IEVOdpOJKaG6VMed/KOi8lGh4Lk2ofAr10z+nxco/i26OlnmGBbl5ft7birCd/nOXdPqVg
YHxFr7lP9fYZqaDOu9aHKUoNnutj4nNJBs1l5yulMCwyQtnxltvR+1bO4wTRMzgcL4wlHyZkms7a
om91nZqQn/wWusKAwHdzYzjCWtn0ejHMhkkG3miFF0HQ28Gt/XY8Yk7sB8AbRQcKehf+prerVfY2
SlAByrpZrPb5RC1X5+zyvzT/mlHRPYFbA428Gkm4bKVdU91VMxAgctrrAXT/DBLQbXzEo+yg36wx
TpmLGAq8/yn+hvyteFR2XOAOB+qoslDSOUGgEcS3Dn/bChHWp+FUNqfGJJ2RyqJ/yBLJg5OX7SZd
UBzduMpfdSDykPyfA74LfVPwi+1U3ws6v2S1wXTUwFfvrN1xw5zuvEy4idRokMN4RWYtPpgWiPP4
7yXnkSuEkggdakUK9wb17qinBaDrlDNpU189J4L5YZbCpSbVLxH0+QMfWR8DGM+bb+LBvkUj/ztY
HUbONJWApgrWtnJIMK0IxdzY8oreIKpMV8APEMuu4r/E+Fk2U/pfbi2f87uREK4k1jzk8ki1Ry94
3ohOGswTgIrUbJi3gDuHYMGnGGrjbrISW8m0TZHd099scqLaH7LULEHQYpmRGXlVbQUrWFcFPncx
CoWHZVpWALQxI+jiTuN6z4oHyxXaWWgHS8vxCW7iBlDeBjBklYg3AVun4/B5/ITRdJX0mp5tdyj1
Uf9wZA7EpHaD3b7R2bE46aQleDYKITpGItQJHw9oE3lRchAq902nUzjQ9pRld8cbExB/jpO90JsT
2ffcFIHswV0hAIvg+cn7RM7AQiihXRHd5P3Ka2a3T9gyZazsHo/UoVXOuK0tQrZhs3yVP+LlbojZ
EOQ1sWRdWux9u4R2VXYkUo4jp9TezUs5mtliAGy2rqq3UMT/DL+XbmMqXF4MYpYOxwLKrrSAHrm5
3hkLHRn7icqoJFIzut5TrkOUrMMsUvTpqca0DNJwIygX47wpctau+qF1Hvcjo7X0pFV9OvdSyr+A
23YlRGcIscUagOjWPdqO+S/a8CFbcCRE0BFR+QK3Z/AKCmvyPnVobfNHT7NS5H8ZcAZlagfgP8ER
tyEuibzoKy/Vv04f0QsCsQn4urZ6tLdIoJ6sxgt+UOblYI/vFfLvjXd0DEnT6MYKBoaHl5cptI0T
VUPHSbs9jvKvGNUabjlQjg9wkIXFdY7F9oLRJ8qQLDyOTKKF0t2YVd4yJc1d8k3g8ecdOeAvLx+f
60OAL2I2J2xcCFcOC/P7J7K4DuiEEj/nBK0OT+wu/I/D6YG3JIITEkIwD2pyfVLoGo7yoa/RYINF
98k+zCgfognxDVb170amzxfw+7V5WRrgqKHMsUXjEZwAc9ZU5LsXaehhyypspTE0LJqL0lMLr9m9
8lm51ziGuEOvqygAMvzQMxsiOgCdk8r8QashuRRgyYPTRkeKopgF17rBx8M6vm4ZpPpvCGB0ofAL
S3BQsQEXoRWDlYNE2I7hyFXOVAkPsVApwk/rzAKKQ89LwokvbsEI3EW4em6VeARq9/DQCKJ2nObI
hwXLoF1ElTJZT1Zbrbsw+9sY0oOvG8facsRY0l4Io5IjX1qc52iF/tx2IlrdOwD+ohtcoFjn5Qro
jVuuz4LVR3E9b1l3fFkq+TUR4qENNfMZIkCFvPu2oR54k3salG9xCyizLyV4SJt4HlBawYNXv8Wz
Y84L7FBC21aiYIekMgTTMDMpKQJkXut8+BZvWG6pxQ+kHkPrWQwTiOK8Z3itZklGo1u9PC6xkWCO
n4RAVqQPkglaGceNDPEn95xPVRXWg15Nu1S6V8c0EFxZXcRjzLqLkVhqX5kWxUwD6d4T8PTvbBUa
vaxoXqlDY4OEUmTTIaXlQlp003q8XHFsy61upWgGy2fWBAgVg6QL1EFfqJbX1jrCTFmL3obHtulE
ZJyvS296em+jhwpwYQ47TPdisZJNqDAPeJgg3F4lN85BOmaV7ZJK76P9t+CszEYCsfv1h1P9AfGt
Nu4qjX6qwA7dt3zPkXl7E8pVSm1dO38g35ISjEXPsf78xRKlHij4dCDiEUqlQUsz2KYuDzzbqFwP
rG6q+FZ4jfylJP/GqTza5IWdwFjEzohePisleLrqW52XwJQ3moitTX+xJCdVHW+kzoPhksEWrc1b
yVLdQ+u2tu9s8SfKGUwdTYb1gLSrj+Ku760i367dmLGqz/Rf/O/M6irsYRoZrzlb3M+aIUVKRSi6
5acP0OmuKPbYcpyB8gKz8CbIYEIHNWchUdUEJjHIvYxzrhP12TvNoYY4ua26dLzc/3CLXImH6Otg
59PCqUqw+0strSYwUlKthukkYnL2EQf6cQb2FzTqeqPCHjK+FHtX2WouvvNNvbvkr+WCHMqce5c4
djV1ZATWe67DH3fMLKGbd2PY0BtHaABCmSnT7dCRMumxj53ECMa+AyckIdggBu75js4cVK2og5wb
e2uApwUmdWlNIqasZZ1GUgbxmfkLtNB9DoFOyYnS0FH/vZ73YH/JZinFC1HS3WZAhewUld0fRQ2m
jEiBAweF2yTrBopZKslRdNO80ESMTLwfrx1pu/iD0cKABdwtZZJZScrwfpdIy4gi4vetXZS+6PYE
SVoOr/80UcplCqbe84ES/TGwjevcUpWlhd+jedtlMw8Smo/jIyxZzj4ZHMPk3zphBUou1iuh0mED
Rn+V39EcvQlBsIIBNeafU1c1Xaydu0EoeGJuCrF+q38sIIHk02A1PzrGkOvG1Pr5dzlPxMGg/P9S
mfbo5lRJ6I1DrgmLO79qjSSzUESdoybtlQXEejLmrh2ddErp+bRuBIr7oF5Hu/abmS5pNdcVwhTl
oMJ0cNqxhxrnS0RUkp+FHr/Fud4Zdu2Upikx7qnvkjEEInz7wchcHaK3E0U1F5KoRLBSAv7sP6u0
YdbXP861P+IyDW+1Pw+CCjRvfTcmJBDeMcRKmj1vlcqK7MvbFsNoafoCX64+bYaH3JM9VUt0mLGN
UaJ+nQH6W4snLjfQNfDOfoM8n///I1jQLm6imxjg7lmtw0A4TRrZwD7N5nmZPC71LUfNIU+DNWyz
IEW1HtPEqvYZVM4hs0a3ieJfgEAX8I4bZxEplNItZXtjt3CU8ucFQOj6fF3iDuXDKMuLY+fBsDDU
lEWO7jew2kkA/Su23HFFz6SiY5XlSBnLUjUlEp0zWqXsDV/gaTx3rbX6g3qhnRHajr5gU4GQ1ucN
G7lD3NMc8SJm7QPwkc5XAoIkW5d15fYzAu0wk/vG2BRlDO3MwqjooL+3vWNVgZi/j+EleY6nALDz
s7Gf+UNgiuj0Elvj7Q1TA9yPLRsZlMxOgcjUUMWwG5k4viE1xY6Xs+51hbEb0My8OapaupbAUBvG
Svk1tJMzvUsPm0YcJs9YzZFOT3TYGfBoPDgl1xGUe/6Q/bSvRTFloDzk9Jr/afzC9aRiOrijEiwW
QhiOsxX7i1fWO95SZt1A+riUHPJGyq4giXV7DcPHfO/7jXMV0XSOvQJv+yJcBpoXN8ay91ut4Mdg
HpI7GP0+y1qODQ1Ek6dPPJVrX4C/cgl3RN+QsC+AhcY/W8Sbxcj92TRI15YIQYm4KBoyGYWO39YM
XdzFn09xkNH8jM3gOslxatg981vXWKlL46UHvuzUWzMUswKdgALp0sscHYn9e+62Tgnv9TnLNigM
tU8+e2s6sSfkM5bnQsAiC05r8ZuyvgnpjJSvZLRMuKiDIPtGKgAm0CrAbEMj6jHjm2EjT5iKmQ+5
vRIwXqjv4fb+1BSqIWXNtYYHDsDSs14SY5NGpW+AMpchTsOaZnTGYb0yf3s96FOpmZJ9tI79QL/B
/0wds0r/WH0BH8tcsF1UjDaUn8NEb0iug77eYYpUjjWdv015VaOHMIMKH0JO+Xl1ciErfLnXSQsr
H13tz8O5hFafZnGuyuI7O6SxKq0YiX7hiorrWFkbgFVOfCD6rKeTBdFMrDQcZZHgYR8pNp7eXDdO
RyFbzeQZueHuC1vI59WBGDKgBglAsOLtverEh2i5ncd/aBJjWlgBEbK1AxQTbJZUpry6ckyl9FrO
uvqMY1P0eJuV73usf9YW9CY3Dg/bo7R6JfrlNNsm8Bo/NNeHzBI4cVtrgtvDimWIahQuRby/5l5t
Jxqh6DD3jMFckU58MAeznjmV+Tjmt6cydy1OudJjKiO8YY004i+HRwEXPrVKxtQkZSL+3Yrp2VM9
bj3UJzn8SobrJHswtM3oFrE+w26NK+DjjJS74atUvTo9Vv25x1/H0qyb4zNNz2aGnRXq7vsdLwF/
vs+kdmgw5FPTuVhyC7n9n54hTfQQ3g4hJnBi9botRI5obFQ3I8xRACcYRReefjwPaohHk1VBTIwQ
v/H8LLRX/CFJhfQ8UaxWaEUp7W5MbUe2jcjjwTIIuHek1/jr6qEWlBQnDC1iTaCYmI1U07vYEkFL
irweyP5dAmzOjUQxP5OHd5AxA5rDdJmRZRPmqbxqHTumEanGSJuUcSUeV7anSFAtnHY5IsF1fsj1
pwcvQXB3thHMV13GdhBIfkxjGaqEr8Rxg7HJd3vFEF2N8QBC5pwi/py9/mCfhSvKbH69LyiLlAzW
Wn+lgBEHzshF6+xq16AkSuV1YsYnO4c2Gy+MskoVdLwm2MKCVA9gtBaTycrwsiAX1fSq2dq8ZNs5
C1Rrq5O9Jh1ZHB3JcOWooYg9cLu6iZKue6SgU8leXrP3EK884JjXOQxDb0Mhvl5P8zEZ1++JRVJj
z6KQm8b3fox0WrrvfBTmpvm7GeEywIPvPGigTYM/GRO5j0hEJnRdkltes6NlSQg7RlHdQvGHuzDb
cxCaSGAREghfSkup03exk1+novVqO94AJ7A031pM2QxdFsQh8i0ZkAuiGVWjLTIg+ySxa5KNftvm
EpBRxAy+J6AK1KnOia1wnp+lXYDkfZ/IzCJPLu288YYbi1ypafzYEcM0Op5h7bvHRFQVFn2A+sSX
Onc8TkNh4TANFDvMj38u08Pc1qdZLvqgmu+bG3su3dlMTbBG5sdR+h2qhFufSDQ20rDGVr8qOuc1
3oLycoJypwkiW/udq7xjI5IrmlZT8WisD/opshuqg+4SUbHs/M+fepliZBuEmcUhVAyTo0WBVL3w
APkvP04Dv7l91RzqeRAXN+OqXzhltD0QS21ZW5PXOGcY6DHwBihMIFX60AEgjsZZpRvEN/ae9tEj
Oaq5pCDzAjByd99phHQl4XVAY40LDFQ6qmohvdz/NsaG4QB0rQ/pWGYDzhCVRl+N3WT3Vo+PVAue
Brdrt5nblZVWoT6kncRVFHnB5dx1OzdM3ug5z047f3dPvi2iRrjpAyOkyCrg441TaVEjTA53xtFY
ekeUv/6DgRVsBvIcPp36XyoSQLUh4FOFotqw2sQ3NF4EJAvmK4lAugRb7gMio6xf7cYJIK7NbWpO
qA+jn4qa38uEk0H+HEM5emR4PwZiAgOIrzmUmi9XlwzE0E+aSKhU9g9KVvvMc/b5tI6eo+sDD2XT
4/FLTnVvq0Q3GOO/zneLHlAUK3AIhERCTm0X1Bv0RQ7CpGoOUCreJD6FLDZHd+4mrgRfAm6hHb2W
iSLrX9JsRu3Hb6q3clpemSwnMD+5oyCfEdPvqcD6p+QgyMcRmE39OxY94JTxowPBjRPFC1/FpM5L
SLCTC/UypqDucEVpQk6UbnBNmn5AVvRThnh+WCYvu6TOE01FMceomHzGs2nxjYlhuqtkJcHmBsO9
FvUBTlqWZC4WVEI8mRqM0oc6lCspFk3y6wad6qJgc8RQunpqLA6qHMoBGDNMsSxompu4KJPUXZ3r
J+NBsTFQAuyjU26NGx3CCnf9/cv2o3qCac5O3SDjmKD9O9NRkmAjrPzUGnK5pbEgsz85ZUD2AJcE
x5i5xPdqI29HfkgdvqarL5mCl2Jx+2fFZDjCZvQyZqBYvDXNjPz4IRJLJQzgig+X3yFpnEI216//
PlcVC1bcXZrpoT7Mm29WFc8M9yGkyEuXrMcijq23UT791thuBSC1RRO455rrKnuWy5prFqsLhzMZ
Kg0gZ27uK9G518p6jDeFGI7KtyRh9ubOW18Hrlhz2n2fSEJRrMoVBcrWWTWwVADrDWJAjSTgOhQi
tL2b0gq2dzwSzJP2Sk3YHx51o7Z7Dp+dBd12R6PI0N7qAb5COLExNt2oOJWN0iRgyWvetfpB/R09
JjGFeuHKncQWOQRhdtmWB36cPwzNgXlnKcQIJz+6aj9LEB0S1wq6l/R/URdQ3wrDKhZGycKDAq1q
6dOw5VKgIYlgUYDEWxv1VVxXwJUYGH4p9L9rcKp0Yt3/al+Oa7Njnm9tn5J533fOihw1MKi99lno
XWvKypUOtHCnL+PI2nL8izJblxcdtHg2R1ncZJOs+0SXuyJWp6mqmYLErEJSZNfqZl8Q9QciS9kQ
RF+yvQ6U7FJjwShXCt1FAuUbHjlDim0TKf3FC16QGNOLzR9IigikjoLdx25L9aq9TrduRvnwrKop
cxcTMfZRufK/qwIpAu2T6QUCFxCjhTzNDk46qNeaARMGCP2pEkVfRHOVRtQFNcifZ6QLQRrfuvaG
gwLtJVlLjeFVpc9pz8Hep4UHVcJ7PlnmIEfpqdzO/I9Xu/WQEH86+n2qy3PMWHIt2xvYAUHSApcy
k25RrAsGXMtuHzV1G+wPvAKpe7eTz1zPf63SDTc2qPq+DNt1l9OwsiKIISKtnBZfx+23pwbAsknu
ZirmWT8PAtqemXY2TJ+E/+9emt6GMOJx5UmyZJSNvYxzeRvn8CYz42ELg1Ej15r96JSKdChtjPOG
90hkEYtjW40thUVQ6zVuFt9fF43uu0jdw6QqY1UYZh93WJb874xqRyru5J7Hj3M+dE1GYk1IBlHv
8WH/lZ1jAGebhq0W8Chw6yfN/la8TZX6yrj6P1Jnf+PMw+I6m9DO8MkMHXmVHNtcB1ntRx9wNR9A
8bRrEG4kIGkfiGhs+olDkJaS2R/QFV0cd6/N+j6VzigfVqLegnrcjuI9vqPoz00TCNzFNSp2vC2D
RXMc80x6lcRpVsMvZ4MvsNeRevQmHhNj/ihT0RNn3PrPW9dejPxHLpRdY57KTaXXPhBF6Q1fCa6r
d8Op2XzMHqcLh0QTsnfldFfziaY4MMRNGjKWeDqzPFxNLDTka7sqYmRXFOuzvbgGJZFmFFi//xho
NOQFTflBRf2YZLT+n0jzTi3cxN9LUzejJesBbgEl3yStf3QjT8PC8bjn2Eu7g3jP6MgNSKHzS9Ru
9WEQepahOZHPZWB4791Jf0qrD0sM7ivWTjDxL1Z/RlSovXtX5WEXiVkTcVnT4JGHCP9uiSIhm+cR
vAHBYvs02n146un7AY/PlkNH4A7s49kEkRH9r3h/tPVje02l8GMlm6gLL+bwOO4CU445kXn3wvQ3
uCKQAdIn05cfJVNxqPR5rJdeqN+0XTcfG3mNcpTJnBhvbkWSZwqcsxMWN/8hSJR6GAEUOYuSRZ/Y
cV8XsNa81b/kiEzC4fM2oZGY+L20JS9r4skAHEeg62o377DwaNbeC7+7bIW1Vb38PgGW7C2SpnKX
4/L+7h1KhbxXSFS3qg2v7DK4LCiM+KZ/JTIxEWOwO+JoYPWicWvOjj3b2Oksb/C3pNqzJfRoOmDh
IZODx41kg2D8zgNvbHRIlYGjzGQKYMp+Ntl8QzZW4YK/T/s/GBrx7TeWZKY7jNPEd/lXQRofsY5i
4NthkDMb1bc3OTpFV/Dq8fxPOqPLmUR9UJxbvKCrLmMbLlXdhRwPezIv6kKW0N8a42ZJ78JVAu+K
6kAmqIPXGH0XGhDQ0S2i/UASOvu872rzmJoaIP47QBs6vv4F7s9XrN41sE/LP1rbadw3RiDvi5cM
FpjojwVl5ofaBcoAZm8ks5b5mqldf5X7CzJZdeIHrI6jzL86b4zAGeORW2+R71FUjV9LRQ8iWSLY
+K/XZUDgz54ETwG26/ig7vGQgXl4bpweYV537Au8guCOdNF8qwwo0WJ6DVb+MHgiKSAATljQYboP
BzMl4DW/LJPD3VzALe9Tp5K9WUE4MqVLXZL/5pwo5thhj3vzYAhPiYOr1avVu8W/liQ584LcDbzC
qeWzxkBZ/WYUvLD4LGULL/f1AhC/cV7qSr0gGNGMyIhgQplGRSqpi7JCrjT93fe4rzxKPN3Z67yL
R/szh7joDV+Ds+St4yjkMT/0/nDtUP/JEJ7qO6EqAePLF4PIWKlKfYbugIubLk6DtvkfkYXizULy
RGsfY2jUac4KtxJdV2fgml+uCqxY/uScrizPrm2CLGYtTANe7jUTQt+seYl1F04lEVGyc/axj/Tk
asKTXHMQaWr/Kwcu0Z64R4RzR8asB6PXM+hEfx5BxLpsR6x1TLVRg4g3HEE+FV6u5ycd28+kVJs+
CiJAc4OAowqB0vzXR3PbCiVxAj5Ik85vQcCQZMR08L+/CH5lxk0NyEkXH1srlExdSafBjTrfgz0b
TIOTDTzlIRKkxMFVtqYcoNaIha8KvTN0pwYUZTeaJ1cK4UzC91EzwVotquEy4WlmTW54rHVl7lFH
WjclNZTaXwR0oNFo3EznFKILfMmYnnF9W1RQZbP6WnsrAFIf84vdIuBRMswNq9Truq9qrJYwKdMY
oRwU/fvgHhmYnC35Vlua7De5lkxi46Eug748+vBECGNVPhF9u7QynpYoqawG9LBOFn7HuPIYHVI1
HfeyGBEWLYokgfo4syYvTPzOIp7usz5guW3x067HpxhVYiIvPCSKkM+JvyfnnThaznKskRirzytw
4p1GZo+op+vn9DSMANkeAT/YP88l7D/6vmuO2ZnfVVdCECA6hGbuP2jsSK77mINZmZ4TNyHB/skh
pYqaxBetB9dUAPzMVX0l+HMcmGaMypa6ccCIAtnX/uwImz6OLz4Q7o7y2G2RppW/xTTvOkB3Ht+7
wCVoBm78TyJ/LfWZA+BW+MnHqxhAhvQpK3p1oUA5xj/HQ59H3Q6fpIRsXcJicmxSX3VCOBP5zYEJ
PRpmcN1sW4PCstrynkDeGPiC+5A/Z7swgloMoZ8vuV2FEZwnYnEFi5n5LP8wp4Y1s+1gCm4H+YN2
dL1w3ZEWxr4wJkBuN2CVyR7Wo3R4JuQ3aKN20eDQGVX1an5IUWf8AZqI6WNmB3yKt530IoyTBdkG
T4vSebfkVTCm3Jv4hYBs7i5jjISTCaZD16oxNjPn6psAFXNMI5M7ETmEOVeExwro1ZRnvFHVwh0h
dN/2GPcQSeTo4yb1EMFXmr7X5vss+CIijvU9OwTxy30m2Ff+kW2PdFOAGTvBw96dC4YwUFPuN9Zb
rZViQX4YxF3SKQBXBiF3zBL3EE70/h2Mm7Xtb7oTuxBN/y2g+vUqpCdDqrDbnVOWqjZDg02rENHN
/gxAIWSmBgJ21OmcFM4surx4JJUFMMa1eeFDFYV0xoxeMt+qicr/weabOESxnPMyh9DYtBgFu007
CW0OpCCMO1BehVezSlmGNdnuNXTWjfDrWzsUwFZ1BEnefve1gJyEMR9JYi3ioGA8BtmZi08gE+0g
g9pdWFiR2kcGYO6uxGaV7DCFVTq2lPA64BqT5N/0vubHV/48Cq5s5l8uSHKVRdng3Lx2rEusczRP
LU7ZVLQSATZv/LJ4jf+yYkIGS2h+CEbscDpWD6vrw9PzyLIxEaHjRP0GNJYhukurxme1cGReryhZ
S+ZUj/cklGyZjOPTYWYNekCmnk6SPSP9oJz74rP4J2HPUM1Jj+WSDmdvvZoxZAwdmkL0VgXQvn/f
T36pkEmq5vpDYiinYLipVZwWRQSDFucPuQswkkSpokMI6y6DpHaeeTCbeGImZaadW7WcXo+Cez0R
6jLUkqXTwTJ8LPwvdRvqRin+qJDqYggdTb2DH2gBB2tEidjqLYrZb7u95dRc8h6fFJDSa3Ab84Fv
jhgfSPQySjDSY6qybZHusNSCYWr2iyP1zmC+BOu5E5XQ/W8SVeuohdQreAN/y5GusiRpZMro4/Rx
4V4N9S12L8L/jwKFA+uWgeXlDHdPPVdAk8v7EPM9SbVmlmC8VqJ3DUcD/XvtCpWdC6T8dzPfhWMH
cvWmLLuI1+gi5morY8+oxvOoYXpOjoF+amZLYm/kpYyNPNZwEr9chZQW/275/pQqXRlSed/2q3iO
tZsqjhAMP/6aJyhKoC6f/J8YUsTJdz6ce36DhUWDpUByy58ZQvctJATGyzk94t4cKz5IXQ49Gyqr
wDWGyqbfbQDsEs7a0NHGpBseeFwyYoo0oeV/rhqE91hdK5E6/7Gqqj4+fjPEyqKJNz7aMIq+jKWS
tHyOKMlW2krJtZM47Cw4Cj1B758Jaa3syazRy7C6kkYrGsz60AsbYIGu46VaazijUlQCDvsyAz/3
IBOsXxsRqWRXiaArblq7f2l8cyXcSvKFQvvFm8yESNIdtsgIzWelZAwf7o/uTuYt9mJhzjIONi0L
QCfYkeFOdnXuPVOi1/L60OEmizmiSGjapO9BmTY8UfNRM1VjwZ7TCehJNL7/FABMYfKO6+Tfey71
24JLLp9K7Mh0tzI+WJSY3P2BN3MCe4HaltX1hB8DotjQN2MruBgw0nZDL9eNvLEpM+lgnuPYuUt7
8cstDZHNejWfGlkAgCEADqOm6ZQe8vHV3Nu0GgnI7O98ymefnmc3cELRfd8iBRdO+6OHaq5mI8nK
rrVXQR21/5klQpBzGyIwwy21LFQLI4ag97nCIsRuUF6tbMecOmumcRjlDUx+TeTeEip+GnhnDYgH
F8ABpnTyoDBOY61kK7sKfMxhNYsLZcgDaKlLFqKWvqahowngU69+/cRNI82g1GAQNpkiiMblUfK+
XM9LZ4R106BDa+Jy2G2v8hQ3FMvg7+KKxGnjTXWM+B3K0M5mrA7RknUkkVu7NW+LU4MxArbxs5A6
xH+0kprNFSMYeASn0qjbtDfFjsIwu05UjnznIhiUqfh60/9Qwr35pez7zkwHM+Ylou742gEC+8mG
/CtNfL83wY/PPleVLE8LSInDuLrgKAcPiGzad6eXyB6QUdq0nhiWRR41kouF92LQvgjKQ4NhOTh+
ixtaLLMdofnitbUK1F7MrrIQYiO8h749MpfDuM+IHXDV0LUsJVfU2XXvNg2wr0ByQwl/qBQnwK+Z
WjlIpT2LsKgBFZoOSyb24r8Mo5GcXiVISfY/xnvUFb5Eydq3POQe9O9YT432mziClaW+evWZEqO5
sVJnheSBu8TbpdJQRrG5bcxevg34SvNG8B1hWZXyU6ifs/QxEok/7zbSl7iveHeXCeQUU+MjrmY3
y6KRlxuc9vfeOe7SujQIZgJCRNaCl4y/YtFwSlC8zjwpDrmsWIs+ju7GfHq/0UKbfQoj48UzqKLS
uMHz9dbtkAoxp02A32oWLR9Tyy7ZETUnHlPot/nb5PRZbefaLg7dyOviI2uwCBFWdy22ZJNwrm7O
CrfYwpT0bjhIWcbsy1QgPm7FdIoAijxc3hLJVuUGTmUFKofhARMZbWsdyh/cr/dBCITzf2MF6Txz
ndPeBuaqW/SUf7RehlNugAAsD8JtEn6tfl6qWwr15KK26wSBLevl8iMsxgjx6Gj+OSxdtJ2UEan2
WTgHRdcQropn6ffKiwDMYjk91StU11RNP4WNsWg8Zm6EZZshEsvo1xurVq6xfft/IhM1O8Vnuopi
XCl+eQ5F9V69T90v1JREnxzNG9v0mbneAGnb43D4WYIeF9VHetdDUEobZS6H7ExKQmLoakon8pnR
AfKhJBwQmj9r9xV87Ea3piNsbBWd3AcZzmYVUEiqmx8y/Nx02q/Eh/o9B3FtYSTB6FeP1drPddfd
WFhKPjuhVByQhij6UPsrEKk5wpUK0RKfp+SHuXjhgjpRbbM65+JTxPUTGl1aEYmB1x212tcG9kbf
cXhvVdLOIpargQjbgpJQpP8FC4KuyulkGCjeCjNcHPj4WS3RViaHCW8t4uETXIqgwLMGQnSZeaZP
eNpdSFPxwswyMg/z2Jj/kvP9GH3j175DqTDWRxp3qCoo9v1xWy0uLltvhZESOTEH3M9H3BlmmLeX
QHcyzMh4tNOBV+MOVDrFOOZTHrUVjjlw/1pMoApKp1u2b2+t4qtMXwp9YG/PqKpuL6w4thHG64wA
D1WhTvwaZLQ7yeKuY1phMrdZL04ofeCRocz+bUiZKr7LCHxlmRdLTwrz9mHXvwr1jqWmtCx3NY8e
c3FVHoRrXwgBwL8lTvHHBtWnkwF+C7AlNZK02u1cxt4B0Gd41vyHYyECUlpBUFICddd0KoLV2J9y
WkrZwmCsWToxuthsuB7bemRkF3T9Yoe/hae51zb7wPn9RlfjX3AnzeLp/e0U3QNlR+YND0r1+U1f
YX8mwSv1pi8c5HBenciF1IawEKGxBKOSlOjbQTPjv+5/O46EWL7IxHodUSikwZPTxh0chPG41PHA
eY/CatJUVEd2LoPD5scQpVbinKIq80pG9pz5NitvQ1A6rmXkMKAxT4TZ5VXqZzchMAXr6H4pxGa2
gV1rcLCUqqlcJxFBDqxhfFiPfcdyrADTYJYr70K8IMI0pw0QqpJCVho/EZA9vRcTJnoxJPwq/dW0
iRH0kws7ZI2vWxnfB6NVYjaBrM6OjZ95po0tbFGwRkyXhk6AyxOgPh+4NRP+iTCp3qKeHoo0nxLo
4/ELuSal0Xyszcss7j0vEh+EAyXGxqgL89k232NNGe1RABR7Cs5npz2YTzvCY0XPly00YPiOAqzt
Kj2QxQfd6jHdM1fN5OmpPottLjYIfssaRqbCiqhojCZ1+NFL2YkJc4J/4dKtdh24AQY/rr5oASLj
e7eaZH1eLxaI25rCMhpAHw1A2VpLGKr8EY5MpGJciP6ya0520sDco5Jm123mOb445kM6JP1g6MaI
FBFT+NA+ClzDqAkQTv8aXbrJ5NtSe+TZfox5y0zOaT173RTSoX+HXylRDW2ii+g/PkhqDev0KeDC
NSUMjU6887I+OfuZEPdwfwzeXmMahU2G5exaVLbLLWI3Ok1u0BGSNRE4wddm87mkfPFfGIDTGkxi
/sbDvJnRDo7PfqfMq/Q9Owv9A1pVyieIyB7gClp/+jSmJZOUiW/7l2q+LFvFvaxM/aehMQdIMUj5
oW7SPf2Z/atjCwM3deXSoBrxN29b/oV54ilWCxGHxDx7AJyOqQfS1WEvceXptyrWLHT9jt24fUP5
9sTw3R/Uz1S4/wHARnk+enfHTcplfgtTtWMC2uAUKnFr+LLrDTFvdvdBLwgsjfuQEvsjISLphrtx
GRuTzVvbySzamiN50dehYZjpFPmIdDcSJFc9Tk/4Scuq+tWFLsSYSrfVWy2IIzNmloQhWXeyuUrf
/0q6kVXcyK78MO9/W8MPPetWX+lqTwbScJriyl+ZM3f0M/VbJZ0ryLIb73AeUQvoXFRe+LLyo69K
0CTze2Aoil3W/aUz5l6orhXrVwkb7EqYZE3jxCVmSQNPVR2f6MIC55SV5nvFHuIsByVChC3OGIlg
Qg/C+1COD7m7V+Y6hO36skS0L79VLQoU5dQ4+Yi5wSeBfMQXrgK/fJ1cqzYrqg87Og9psizrT4vP
FHM3KUvQTEHeGTp78ZM9R0KOZfiJ0Ec/iE+4qUynyC5kNdEkatRRFAIfL/X5pcn1zWJSdnxOOJez
fyap6DJFcLiTWHRMJg81azU1CALW00YPG82mVALkZgbwEbJI4JdwrE2WUhxAgapApg7KVDhF8afY
wesBxsPnFS5MU736TiM9kEfIh/lsx8ovBzL3WssFrMxqcZqNCgbUTu3MUPT4aq5Tv+ri7kXGG4SI
jT0SiQbBfYADjPTYUs2jWZnzGgFyVbr1BM04vcImw4Tgu2+O+e7ADXRH5bkNWvNo380bAjlVYTTC
em3wPDzzUmm4s2LO++m0QiiT8o474T3/K1Q7IyIQm4kjZ1iNokvqGdH50PF89MFV9GLy1BrtM1lN
zouXQMSlvvyK1Q35qKkDWAXNg3LMWfDBHzJwK7ufssx/PcGT2OnOelPgTjjqPtycJOoc6IV1CJf5
bgWVw9PGZyrg1py6PldXjGHQr8NwZcGKSEtiJO185rveNC67kV6lEOW5vCVY25crHHLBqNVESKLW
ko98NzGSXB1CpkBvLCN6ZLskQx3/y//bQahYn4HyAwiW3xRmuLPzXohtgC33JQ5Kdib2w4asPsJP
kS12/IGrAO7VNWNRc8GPxr2xXuRW3XIPVjwkc0gM+laaknjV7LyabAIu9LH02Ey8/zeLigM5xoEY
Yf1cL7jqgbsqKv3cf6k5hXvNroWoACXfB3r2hfIG4WKbVopBK3+acoY8ou/plQkdDjF0q9VhTOHZ
6sHKQJOl5tGqppJMt6zCQWUeIp+5QZemILqfHyRkmylkIOE7PfzyTE9Snv/A2TTvUVPkjDmagOGQ
H3wMnw73r8vawYC62oSs18/ceAE7WrgM0PdT6Gwr/geRPHNSNv5OVIkB85mFCyef4U9+OGqZSur5
W0WKkAHjowtq3sPikeAzq9Qqkovpvw175RQIldu1gvS1qFuviOSY2ffhaBv1EQpY5Z9pLAhEZ1Qq
79sGNvptckZbcIKD5B2zXFBQ/GetBheoNml6Yl0MaA5SvwYE4WC6ynCQdDtYEAgVMn9wVCgPeD25
7kNAVxa2k1FMWEbMmQJjA9+XaCpSqi24RD+eQK14DgonrVnQAKCW/zIJq7wVUZoCVSY1Z/S6VmYo
20+R6zvvgPJ4DLiRUiXt6MpE+hGANbOMQNn3wss9xr7OW14ZPWCW3/S4gp1+PituZ/44OFfJH8JZ
tvPPo/OhyDLht69p17ZMZC2xZ0SyVMG+9Cu+iybNPgt7nKSytn9qG5NaA7EKK/GSZaRmvE4hyrMT
49tPMItWZOs12bdyr7JiDSLGpSgPV8gnGwEjk+Rmyz/VKdR22fcdCZ5vgnZY6deKaaGxapJWZGOD
6gKyVvYSFisYoenFF99Hz7LwqOHTW32djy4pkk0gb3LjPvBGZkU4UVkHct8mMFNa+SZh6A979cC0
UFKUDbYqXInIX1YWXMvHSOUc+x4H2+LNSv2oBLeSQVVpkx5QNKfYQ7q+dgWN1BfKxrazUCSaDGJC
wLUk7rRWORqdV42DJv+mHce3xB26Cha7i0pOOib6Yc7vaI2yNFc2MFG5p0SEgnV1IUN8nHA/NDL2
pAnu7rVxAPCHQTlT9FTQA9Z9qbmeHSxXygG+b0kbEoFGqW6KYeW40mr8JzQtNxmARYMLGNCppRa2
aFszF66YY8ISUzP6a6eJjIwVzGPS4CVKiRshAGSuxGJUr+5YA2V6IaHjQg6xqBbOKZPCeqti7aI7
4YGc9NipOPUEd5EmUhvhUABjKuIHrS3YI90lGQTYVa95hnk6n2BL9rx97KfmnuZtah1l+bYiNCjW
IcFmc7nqolItuqxc4J7fKULwnIwEnKCGS7dDGlMAFtPcC+L6BkdV4ES5WuAWEzri724kQYCn4Rhs
BpTPhPTV97pgjySG82gHIxxT1XZ2W/l8uY7KsKgs/t3Cj1RBJM99P4pHAqv0asebwbME7321FtrF
0dNSDd3NsAvZmcSakCnbxWc0b2kLEBPD+5acJYjFWaOntmIg9+TXQW10WGUr4G9/X9CSconY1Oip
67vzarDDGlYxEYdhJkj+H6cE7w+UI6kQCHE0TndP7xM+lgRdoI2dpm9r3/lg8tbX1C+/eOhaJhf/
hq7SHDJpspPYHoVcbDfpdA5hwtB9XZesthh64TB9QU1IE1wJqvt3Br6jxlZQQFeEfTckL/e7kJwL
WDjdBAarhbV2W7obdK062PUBJkQJzLMswiztNWnRqGWwDc02Y3rXFwG3cMRN3pXQmKsw/auwoPOB
jDFWKLNWNYhldGtJtQW7LcBTr6wf3QcX6X4z9O/8WXZ/RmvDaHOZfnN0m8bg5RXBHjEY4FH4Iqfh
JMXfHwnXxQ0ywDpFpM6XM3Ig4Hj9oIUPMEbmHrhnHpngTVckYM9/DyGwXSlpwZoZHTYJs44LN+pj
HYWKXI9/xOsD0I/PswLiOEFkaPH7yamkHtJQAbzuyYGsnml1RTJBart2q7pJKoiktJBEJs1KogEl
X7nVfgU2R8K1LolqQLoWNqJ5JFD8g8RBpNOhxqXh0uKzLCGBXdL6JNIafnO83dh7tdZ8vZFX5uiJ
1GkoCD8yGYim5TMjzdJAShodCdQFYwgYPzjY6BmlD4wcArJCjXI2WR9JYbg5GO+OZ8GshJ5kohKo
ao4uM++qzeIXT2UgGV0TN8eDE/u+Uw2IvTvs8Ov8NbMXoF65spS1dAPk8futEr4R8s/8+cUKfYx6
vvgTUa3qDk442IMwD83C580fgdR77TG8QPepWXIIAI6B9Ge+12ymEIUDEhMnwkhfKggw0A5TZRpH
jwrX2HOf8XtlfDPZB4NtqhXYNJQ4DnHAsTLFhsGS7UKjUXCsH0vrgmjdKA1jhpCEcBUb6VB36DRh
MUuD+78+J5xLM9SQg/DMta4l42+Louc1SGZUSxHNEM8LphK9kNThw16G8XiUp4CnRKxd4uiZKv9z
HWU5MYUK5XfPi1MAD294tdhl3vN2nMX11lAF3jOAfrkyp2xxjVKXn+IjRUIb9n47NVyIsYOLUjp6
cy/Mo1VkYlEUprOdRq0cR/BP/q5eoe/HFORBNIPhR6oejwswZqIsNTBr7pGTYXmyhBkqdb3hW42z
nHYRPBFBAFlG3Xs9r0H0tC8g9NNtAZ8VrVVfNcWfqZMamq3jXWu7NGzYYbdqy6huT2efP5xceuzf
TAqxOCzbt1gE/efyRut98ytqLtDUJ8BFKHBGomIMQUJF2fbaUzGI4w9KPxsZEC85bcQ1DF6B+Wsi
XCtHw6Isad1bftrRWCOwvA8f9gVGuMp4dIDZ+woF4jTMgwyJFmpzqpdxEnnoHn6ByqP9RJYb3D0B
XAW4PZDg28tCgdQpG5hFJ/cuKfsbnzLtf1Tdb+tpR3Aw8CN869L6JTiJFxcv60LBOKfaJf2XAo5b
F/jcDTZucYuLNYShsGomnbnr5YTBpxp9xk4bLigi3USlnytDsvcYBuyrrl8qG90moOjYlKcHiyCX
EdpTmTY+KBjsCcFRMYpCNZmpjEIPMfDwEeoyv41XLRNkpIMztW0wCr4T8du1dbYjPGRCyTI2Rkt6
U8w8kMuJHPbhJAfg3IVuRWYXdU8W4JCYfdWnuedhIOrDa3ejEcglwKkj1+BAZJ0x5cVNWKXeg87E
yTYIl9CZQA4QTpLVNvPS9m2FanvRpIzpaNUdWaFHKXJgGQxeULx4zNcU3NbXpeJFAkHmIuhWuUCm
8shXEv9/VuovqE3tq8gkXOzDsIUn+NIMsDDztijvPqwqkC/tvK1XKZODY684Wd8KfoDxuCJSMwHj
+/bBYmuuNCzP/cCuQEynUGKjjS9uMwdWpzolNQuKYREGcxpqOckLpUPKOmTC38lQVR9tU39IRR97
CjRzcC3KEP0HcbDx6HJzoDTOso9nXZjN8Rpt6SSNpJQpcFzRjIhZ1Nm0FZp1lsZ1LBci5VIpKUiB
k8bVOA8saD3NcRToR5WZjQJFYXnctw+glE4ShouD2L7zrryKNMbrxFp1t4NhJDl+Y+EFlGSkeCiy
HSqrA+ObHl+Rx4Hfi6bTMi7Kii1pph1jaQo8vK1zdUFAMq1z9VVlillYXYt1M2R+sdnD/5XgjLRE
PTm1mXvO31hX04vH6iR2EXymQaqepRHk5l7DUeRDVCqoBYCLRPQAHka6K+L9kCLM9CxjZHMb7tN/
5e2mO27aNH/nfZTheq3m0rkgED5RheETEINlsLg21Us3fXY9WJXRlLmfuYtKbx0GPE+FGYT+JYH+
aP+s94y81C15SdoPpjfICQqbiy8StpdsC2a4zTQqfesz9wSWimmU8lpBgyE2T6on4bxA1tSVMQan
XIlRxWzWLDBO8TwNF/nLGpr8bkN0RqND+VkrCXp3pzDiE6v7GLhHqvOmNK8w1pI2EbcTYY5pwhGp
lMZ09LW9bceVgAgYif7vAW3r8tJx2B74o7GQAH1J5jhKOHW8zTZA30esUW/hK0L2miLfCx70SfQy
ujE6sdA/xjS+2iiYw1NCde4zsrMZuTr6HUUAI7zLKu5gbOfSN0FwagkA1ji78h0HG/xQlua7yggb
WdQkOLf/ATDctPdG6irhfDKz83RmCk/INbBHdMHZEfhINKturPPdheVN2qkLj9VdTITpeuLmr7+j
T5RTOkML5hmL2zQGRylzJBdRJXBiu+8CWkVIc9HcwTURc+zXUHtH2TFT6PuKYvACZqwYViakNGCE
fjw60gC/4hIuJTcAmV/eyTiWIGgvFKfXA3phi8sfRO5Pcj1+ZevQ6UUfc3892Omf/x7r8qV03g3b
AOYarhwjgOcqqhz1Gkqvlk1E+i1kD7sZlV9ZfC3agJE+FsThbscM9MwLctVexV/OE7vzqVD4q9B/
ozNdg6M241nlQt/tp03Wcs5PAU41jinkWvlg0e4bjOLx/XmaRWZajcf5pnjyZuh7kl5IPnUIta/g
DAuf8W1DoZD1oXccs31Bn8uA7FW7ghh5g1jkJ+5QSBLVz+26goYcVk+s0hHuW2wen4ZRUnI0TsNj
QrDkTJxNAMGfzK084ZZabMu6yqF2e8CfV8g1L5OjmNR2zNQDgohPV7fiIOUCc+zpQfAU4hK1TkRZ
QU2Lukjl8p2EyMWW2BXtMi9OScslDfCOnLEI1Q3nMv0G+XGhPFe15j2jpsYYPxK6xqPq1eFX6h3h
uNKOsIOvuqMU8dBXi0t3rI9CXkxGUeO4uvScWQ/dWT6uH61syzlqYOhxbLxNdnZwRuKEBEi6lwY9
1OJ7RyGylDANdO/sU4JTIvunzOJgZZTvlBEl1kfLTcaEd0T+Ud3bqy46H0emUKwSHjivQQK4CUlx
GHQ5tr0SZYh3H7rdBsAwFvbv1n0RFCKMHFR2hI74W/4Zr2+HEDb2TjD2InkdJ8RVKsVM+PHdUi40
9t7TRC0ZMGyxbVkfPANhttJOy+pMzDQ64J7K4OQSX1Y9eIfVb9KkqF98hDHO+HlMZkEWJXmUTEhk
ByCCGnGGUK+E/SnTy/uRl/XUMO+SPixAeZlfvi+w+syYDjKnNLeGkM8SjXrbj3LY310a/CRPiGtC
CN7oS3cefcm8Qypi4/MWIM326SulQnINr7kyDO0N0QHN7JkMl1sDEPTCgN3lTl9SwYaGaNx+e/jv
GbeBIj7RJdZ2pNhnryskzAit9+zX4OqMmWzY3qTU4Ydztyg1xnw3KMMgdNBLU6Xpm9F2Fdw/9WBc
T4OvC8m09F/Fz/EeZ/X8UEVw+NA5eod5ZomNN0CMjkeU9qXQnZRay1uN3rXfd50nsrvhRKrty/Ak
iHDAm895cmDPZoh6M+vxn3xx5IveqHB0HepSshYoNaJdFpiPbSxwX1PpmbMRJ/kt/v1GUbeN0mgU
99HUQV437GoGXWXukkud313Z3kQcL6X+A4mm8Odf5Mv9K9Mviq73/CXLidnmN9A1YtiVwKvdJm7c
Ll4HVd9GJXRMWw99tyh4/z8jOUp+i3SoKPU8ioXXQjWKLMl2fRdAHzkisy7YlOc3WAqHjAYCRD75
y7nUZb/JkPPJ7RWhxpB4wPyCp9CH9VX1XAf07F6CYix66GnRLTqAKTLzV/cSlxSz8XREfFfO4DRX
uvUx3ZQL5Ec+Umjtzp5m+fjgow0/wyDFQXTnQ2T1GLq96P+mdPq7blgMy5k7kWcyOcbKB5f8xBPl
UcdMlPchXj3pafbBfiXUmAlLFNQg1dFeCUyke4Ym04gLIzK4+jLQzR/0pBumnVxJREoiLJUTbpFx
MxGFsM7mge94jTN5W06wf8bWbsGylf9VK8w5mBjlOkjYZx+IEVH0hX/Wf5YxTyLdjpU0HwH45wlc
f3k8IdRYln1gH8t8o92aYezDxZfHLziSCx5rcM5aOLg6SYMA9ou/ReJlEml7pRtlUwD92CwFoP4o
enf7FaS4kTxpqIZ2d4BxaTGF0tYV5QoS2nBukK15Q/Gb6paNBdR2wAlZWVi9OZw+FL8OT3NTI636
+gDMYfd+t7JUho5xJXM48VavLfcmjzdGFKHyZdsL/OKHr7jwHxQKjkAjqPLi2kb0Rulw4fzCT7wV
S3XRG35sHvOiYooiprOrj416C10+VgTxtGodyEN6vWLMcbmJTlJaPCfle3XUTQcG2G/fdJZCMDgC
8RmA+eTh5tGwrygfF6XGZd8sf8/DJjAavhX9xx3OzDxqfSRn3+iLpj3vts6rCz0d2559HgnaeELg
TXFPy1/tscmx1Z7UFx7eGMO/7nUXu+4V0CftUEUwYMz2DD5rP4c6zI4WDjZhvkrifnCuSVbj6R9j
nhxVYjgP8eZceTa2at/B3CiWHTje4QjhO3Wo/hnwYP/2stfvHik8fx5nUWHR+DmHTZDkJejW/Mx6
YUtcpvPKpJ54FHRFU18fjvmgZpH5vNhWbP/IZ3Y0XXw9njvhPD42z3i1QcTCNE+VD3QnIsJaUFNT
6y5iv6jUE+mqP1dfLRZLXR2XrSn9BONoUzR6j0h0tIzSK0Sc2eZZxPXiO+qApyHSgT6tYJexKf3s
gOUS9bCOsuTFZSW8OBRHlsPf1bzfeyWFxbqVsC2W5n5WeNkxwyijV4JdDa+B5hsxKXNrQHVmpDw/
AIbzY5JIwqJAkaRyc7u/jLmPTnxIlk522KI9IEJ7hq2k8Vn8ZnKfa98ogWsylbRSm70+WNK/j+H9
axc9XOYKNMJkosnaYEkd5slJKnHzp5fQb/4W1O/BQQWHuETJ0DvWy851CsS5xltEFSAcZcaMKAaB
oxsoiaDQs+AyHqCgDd31h3Vqm6ElIMyeJ3uRZbuB8CkSbn5qlyvsbIV5gWamWNCZV4u1gHsMqE94
zvpwLZRQ3tp9u3jPQF7yiKXLhig7dJMhIqHoS1bIHNaHdUppxqjo2fgbdPYiTu1CIlYBF91A3wZz
9UzWDEaynkyCL7diGZKRXOG6jtGGfi5OYxpJpWM+YiELWxCgndw61MBOu9EGx2KfV6luZ1EqvWxD
AhNXLOijGKsAIrNdL+d6gSd2HL9zWehVmOBTuoJkXdp3HDPYWeR+yCHA/E1QQM/di7SeSJTh58ll
XbOUQ4SE5kebgxeD+AdyqHNe6hYFK/sifA2+GupgkO8dZ9dFl122FRR+aGMC2ch9wIFqhc7XWtZC
chgLhmsBr9elylxUxerSLv/pUKCoyGEpkyjcAafP1nxaSi7gRMNf8ySRhcbBz8N2GIe7mWy1hLVW
KDly88e051QCxp4H3ZVIwDc/4CTTFVB/r0B3VxWUn2ITMnAWYOFSusx7xWsP/sQvlztj4pxGkarz
XTvFZsBEZktnfOZ5hiToE8WlTXHuIU5Pk39aPvDEU5RU9TQJwKZ3aSRkzhTUn9wc3aEi7skuA4a3
t9Pkl8pKSH/J4lo8OcDJuiuUuVrSdpKgqRBTD8pXBismkCKjxI/DOnyOyqWJQ3GXKHM3RjCafY3S
fPPhTUjrGjcUAz3B4lANn2cQmW3J7xAel/v945c6Em1j53dAGsCIjrRPhDaW1/Cqo/k+VFx/rClF
74jxxSWsXGJwJCVBC/IQ/Jiem+AUfS1Dao8jFjpqCRGX6kkitLzm+sJxUBslVOZo5wqq0gOsNiuz
0Eeu/+sQpaHR0BFPVQrVtzHu/s0Lf1FFV++zRydDVoZHdS7P6XmYff9odnTvf5XxViFmuS8X1CZu
AWcJcHnT32tBIfGIoWN05TwiYJJxaUEF6hecotAqndWKS2GJ935o3LJQBCIxNs3bP/+x3H4GHxdn
Cs7gnrSedpKw6pIaaAfWCXfqmNWo/nXOtDkzuvzQegiXN590YCP4HYryAC9OjqToCczPAvRZFGEn
UFMS0J4sigjBpPzv5LurCayKkIS++onBjDqGVsl5u4gyGViBf4z3OMb/mrb7++Ln7tFnvaPj2l6E
X9IufYw8fKCppRQGBwsxjtbBz6QvXk0gvcNWY8hXzW6ilSCZaLBJ5PhWDwkkg/xNIjVyaglUnmHC
Zb8GnoESLKmrS6JCizSJ1Av4qV5cJjvVN18w76ukT9Acwm2UAso2UXvTHRm6+e6Pq3aUY91MFCxf
yGScTz5y2UVQeAuJse+1XWj+TzUnWnrvbwReu4XDtquTc9D+9ygDbji4sRdvDeEIg6MZAXG77miq
OfhbYZi7KIYRCpR7GIjbtIB7kQQYoKDkq88UaCkJMf9T9WSDNx+3uUX8LCh+AlXSzKXfo2OSNbQ+
9miTxACCBfWwPmMx7XKATE6eAvta/ccz2IwVYw7nmGW5nKryPAgELZxLKeXEk5BKCQcKJBxvGUxN
nIn0Z0dgjSppJy9NDYrcZV4V4AMgxjomjLolZhd+ky+sNchq40x0UMT046X5HijZbT3mpMd+0HFf
Mp9XFuUtrZjQnRJm7mpg0oF2btNPV2xhj/IZQPwv4XiWeDtXSt4KSom6K/3acUWPXzSzvzcta3JF
BQAZq1/pYu+Ya1hB9DHdGqUXLrBkIeqgSE/JemwfHfEGC8qePqMZ7iw6oPwOTiKxo6ujbTDFDXEd
ErDt0xWHTp4jjd7si+aFUT/fB3B4e8GX51XBA/ImqKZ6BPeL3ZHvMv1oa0fbFz0P6S4hjmpnNbAY
efKvk2Q7XkP0kNSEmYbJuCb1M6lfYuhc9iZeCZMg4n3S+PI/nqdzCE+j+0tL1om+my6Dy4vI/NsI
+MazGP+FDfi/Y5ae+ve3Gx1ZNsudAqPvyxtgLVKSv3gcMZtEJMk2+2d+Azh9JX7g/nl5OdBzuc+M
Yh5VBQJ3+IFRpQjIFYj+FIjGjoaYDnHk1YfkaX5EZ3Ih4dhk8i/tt5DeJWH5gOxg1p6Xu+acJn90
9rtdf4eHxOr1KOa+kLoLawfzrdJBD+hGERjwIdIqZKEFPlGKPya/faYe7kEFbYcAGxt+tG6Eib8D
WnNzh95GLUOIlJyPEPaxoj3G7ODorfojSQElJctIszwxppWV5grPuAWLVsZVgpJqHwtA5ro6YCUB
8UlRJF1jCakE1fE4XQmfXk+L1Kk641dL/onGqzb4sZYEsn2G9iSk+HhGQC/Fi/DRO90KeNaj2WnY
LsAszt8b4N15k91IoY1t7GWwhr32mWdLcZuU2md4rG277U+guO3jMxq9h+uRu+YFvuAt0E4JrQl1
s0tkH8lWS8wesir8h15HQcOEjcq5cxR6R038o9Jg6i3kvORB3kA1NCJ3Tj6zgGRhdroaIkShpwt6
u4IPYKNELp2oFVGDgsYYxgFdQK/6GkuyzzU7FPsdufHlzMzDaltzDw+eQTBGth+GyMno6BS3yj7/
JoYAUVmc4spgX1BwgpOgtgmHMwhZxCeiCteDWSCg/RzgLLKhFHVZwcy5w5Qu5aXVr/zRLpS+m00P
VQvRcryuzmTxjlonQLq0shGeQJ9lQUxLNkthRoPdbAGOryRTGpTMz8uqB45VBzoflbLg4vD1pDKc
VvOZSNgjzoSw6fe5icWhs1rN2lfbLltoLTaNpGn1yE3a1BY9mWC6jemhSvB3y/5bhx6T165rWvJc
/u21jQKjf0soJb/eKsRfaXw82gDmJ23nCo0c/xRyFTYxipJC2JUO9ec/P6rz0LoyUBZYzoA/6Hbg
G2YHiif3L91wx4t1OcCFYdQZYzR2etJGc+TKwnXMXcKpeamOHjWS2Lt4fRfXfB7KB1gDMq6YeYXc
l+IDB7oKOzR4ACScU6BhfKudUNuAaMFkPphZzG72ldsZgbiCxM7aO4pBD+NL1aW2AxyGRwWFFrIk
nfMQ4DTLGCC3NhMVz1XI9URAwrk64kw6EPxINHJTYJc6jIg+7yabJXc/fGSA/t0k3YaCL9i3bYWe
8UFznKHYjcYbeX2aVzHkmx2j0oZjvOjDuyACwLUYEBqzx2dRPWRSsnQ4L6cbuyVSjLdn72g6OIcS
3owu3VgKsK1PP0GAIwnANqBM/a1eC8E75tvy659Lh/bcEj/fG3gQIcDa6Fhm9U34sOOySlLz0Ul5
XHGbQL/5WhCBE1iauCd9eiDeroJjZgkhOhE504hOS1cLqRlcwZuF1rkOKSNs3RA6+Gn7u65wATEz
wLhTfRJjzUJfhLXBO22q5hNvLqD+N41HpY+5BPIafZY5gtSJJ/vinFRM7X52hsL+JAhC2aqkfDO/
Nhh8+hv/ZBfpZYVdV8qMK11MrYo5w87gYRCtKfyRA2V1BXMTKRw/ox+HMTYiULMs5N3I+ZyAjcnt
GZVbqsKW/gsuXoHs9LY72jK9Y29eS3m9Vw3SNKW639/ESAPDO1m6hRSRJIwAgaptd/6JRMjW8DK9
T6so5/o3a4zGW0YX+zwmQ65gXyiDji7CwCVSAqQbegthYzCqFdWqR5VbHACOPeOljtCtknvuiTir
t3VGaJPnqLNPKD5oFSANzkjeRqowhrILh11hyg/s3pialutuZjkMhDhwCPhXHt3ag3P74261pp81
mkNsD7MEjzMvUiHvoot3/dU7U85OmalMgRcaPKBEvfRVaZu7uwfVLpcZc29uyqOnRajk2szj3e0d
Jb474yKVG5pij6ea+2P6JFwTd24dmx3H37t3qwabtrVRKT2BX6iIgueaIfMLwd1ys6cDrh4fibic
6z1RWT+WAv0jpg5RyD+6NAvMD0KbwrMu+7qzifqzPVW81YZQKWOnhBhA7aj4tT3/GTL1pPoHVGy8
+LvCcaQY4QtbEE2kPYzATiHBkZwDwvRkptLSAe6i4qTiIRKrL4isz5Xj0TaIQ0qNaT91v4eRXWGf
73G1bXww4vcJ+ILx6RviLWK0yYZyTjf6Dik20y85qPHQI98bojRJrH/Q3+K+2qaHHD+xewQWXgxb
91ppwTWbEETjy1qFdayqxcZs0SuJiz1zqmVIH+5vV4y7iH69vO138mRP1X9a0cFR07rlWgg9le3h
dksUNc3x+FZeRCY1bA/WEr6hmrnkrkXpY105zX+D45qd9piZdIqBoneJXMZ9xVss6ufDnxMsmtQS
8UTl1HGoE/OSANnqDJu8YpRVUjdEg/klVeEjovKzt2obG1J2vgAwYknkrIc2oV0ww/26qFjji2yM
IpctySRrBFrpRXr8Qu5wWCzW2fPBw4X09uP/V8YMwVbNqbVc/xOFiDfHSChWaChmEmKrAyix0Vj/
r/slrOKH/BFL+obGspHSlX4nljnAJxCyx8IMtYX3Ko4Ur/dHVRPLpQLrsZt8LVeYUHzUh4biRkcK
5+bRZ3xNOWgl0QpOc5b9YRdEUXZpEoZqtxbNkGJMH0UPSbAZFT9VLaUzA+7oTNhob5s//HxhJOLy
gM7OZE056pcno/ZbxbT5eFixP/e/eEc/QxbeO/qAKwuuwfXlhYLFIAFDWpmXGH2DLVnAchb9le7T
Wx8KKYJcUIkPOtAW8pK6WtbWtgIarVtWpKlUbBR4FoBFSoZpb+5ZBRnWQuFAnAXG/p/9kccXUErB
G4erFaMhM5Z2rHeaCsDasvBhN3HevJeARh95r5Sm/k9OVL8Dmur0Beyjle/ylBWc2vMny04+vZx6
aSjevg+/+pP+jlsVm9WEunobuU3sXkIHxowXsgAKc7cVPnCOTILsgDqncvoccbu1Jw3eYpuDkVKg
1+EeNStu6qc9QwhVQqqPdFVR9GqUX9DrFxCcBnEeFUu4wmeRX1AGQu2jpII04Wqi39ZVJmVda9BA
CONYbyp7auiTmqiajaBoStGdjl5BJRrz6zkUaSl1W6Ddn4yTpXe2GqL3HZqZnKaxOn/0AKZlMg9V
diTPhSIYDcg/Kr7T6nYp+x9d7qPOr40ug8mFaQ9Svc8QXzEERBJ3Z0FAIEfOI1IiCbuWaaoo2Uta
PF3jYmihpi9U4fhqt6TA3UCbTTv/SnKjb9tS0rQsL5xLSJdvSIf7bU/WGM66qEEXpCdZGaLIzS2c
SPBDLLqJ8MhiswhtfA0QoUJltww9OzhxES19Ptbj46oVGvPaxRGC6fthWmm+cJwdZaNwzVmFJn9T
gf3/6G5EjKusGd3k9o3Vr4Nz7KbI9/IiTo+XBMQbAsh+rP0vqPpyXJlqULHIKZxOPL30f+4yeagm
ykBTHNJaL0JtRh5D7ONAaYM62CKFwtY9io1cTCnPZeDMoLrqpoc933q0FXtYtsUFDUiNzNVlPlTo
Bygy22txdWjCLKlg4bLlYLKUbCCimvQpWefR9mk3mKi6XpC4YDgcx9seMjzk8PPEBJ+QfIgGY3Jd
kBIB7amfsKcCuMX3eLTSOlSkXuF4QNT0/tNBM4UahoPGIIWJaBp0emWHdYNI0fWPHNNKihBftnDz
dViIBGvVmadst4yPXospqWfq6mI0zc0lHPlRT+DjxUI2R60Ut+E1ibGg/3x8v9XzQhUfxdG/hQhR
Hvk5e0DT1bi6b70Z0VaU++lBE3C+qxOyF8+4aig7OmtVt3A9zrJ+ipkXCnjnzqhGcITdcDUsSmXc
5r0q7zR3xohnUe9H+2O3RBC1QxE36L425eunuYbic6tHtu4ENWP7j2BInbrAMLYQrDLHbNc485o0
8+MdjMZrsGkIFG+TtisGqU2lnV0pES+VmAavsbTIS7tfYwEpYOHoPeKD+JDnaS4MamOpBckrR7MT
5BOGO9SXnuJkvdshBEojRSSz0R88oU0cK6vNY+lxoQj37/0QW4ytJajegPuh58ejjPwh+poSh+KT
Is/JwHX1pi95k/NrIovpY6sRC1EKPtSLN0Mr+fWIVf2RK7EKaxbhHdDZZP394TtYSSubqz7uNFKX
X195rDXsXdGOQdhhFAiJOF6gbtvC9S1raC7DsW2sKdSH6YOO8Jsuem64OydIyJhbFY3WKv+nHH/9
+T/aI1IhpwuL1uonwziwJpnM9BzYyTaWPdJHA4Z3kvGptk9nQqGMNlQrIPK+ghKkv0BHXw78IiNp
DotpnpUhsZuYf8h1rJayLFn9nA2VLXexbK1xR+16B7xINugR34aIgyH/xm2//iGKcYmKyiU2HI2E
20FwtldMRUfnPbkOzbOopPaL2nePGLhO8n9iTCjCI43UWFb+4Mlhp9o46xfCf7hISPo0Vhs+Ax3s
9mOr4uAJ+G17+jb1mCdDyl7jnJY7+K4IcUFE8wrupKV0R0DSSxxx4sdv+hwiyNYev5Sqvyifyl1C
rQEngeJfNE/M4wSmC2hUm4UQsoX+q5M3SmoDREnmy+VoKv7thvEu+E+eWbPbS7iCm4RY8DmGBIzT
TXE5IC5KHMdGS8Rj40GVepn21sh+VjrYPWCBl2bWsauhWgkgz11jmCq2j1Is9sjGVFpiVUS/+EiE
kZnTeW7MWwWvuOmoXYHiPOoZWWiP8NYaUho7mo3qXGBqG6OzKvpeJZzhSfboYu247yiZGRLdU76B
cBA6SuvYbwpOeRyBPvrrH/wHzeVGc0f+qgfwk0uR67zB3q959drwR8XK9Jf0NVhPhmZC2ihY6Txx
53+wHlH0KykrMTTeZ7Ph98GNl6gxv1R2MA+onGclxzaPnqNqrUXbZm4Gox6r5hVbDTO8u/qgckcx
IAQxd5W+IsSI4KXk3kPTfykDu025w6OxlzxJQAMr4Ga+ZDcczda4AXG9masgOlo0iSTLcgw1JCtK
Me7Udjj7jfJ42wenrz/A5WDDJogeZnjm4IHY8LtwM1D7dO1mLVMDsuyhNNnFxPZuIJv2fEV5dWk7
8HnCcORFRhrrFdeMDxcfRvdCNm8pqbBsO25AFIHTjQ871YfhBWPT7x3+NNxXzJWCTs6kaxRwkVd3
RZTHq0/dNnjGeTVkY2cihsCOTk2N1GkcI3km6WADE8DnxqoL7DbtKJREk5Tg449axeZVNo+tamaL
TH96UHx97VM0oaqC8QQCbRgctF2YIpALxK5M7/LPoJLwgLG17LONXYQcDkraWJYWENRLbQI4Newa
LKG6B0HhVmoBUV8bdu9v9FIlQgnLgNrY0Gnsyuw2NtL8u6ioSt1kDEwsURv9xO+l68BMH7N3Rzu2
Fl7UeBEr3jiDHGrMdBr4lFHDWBaJcyDrJMk52BBnINr4cyRx0AnV9NFQJAeIzGGxP+VnKDpzlDp9
N0JPpE0l2ap1wjQhGHVqsTZ7lPmu/Y1rnhmQBGlS+HUE8ntMXZovUTnCgZsaXiJgchEAfv0y5oKl
EGfdku4YYR/wEiqLuStFVLPrr5mKfsXnJ2YeKciXwdV40BNFht1P8FE80b0wkD9x4cahwS90V8G/
sNgQc3kBvF4npQLGy/oIi/xvC5i8KFcPujDRLQSAFmCoX6c6r7M+V4/meguKErQfaVJ3nb0xkLNY
dnoLK8SmYr3oyTI0942oHFuL2o56E8Nu9eUY9rQhbiFNxMAG0LBnGaeurwxqkxWhq/cqE9nC+Ifl
x4ALoxuMNsSBl8CpdOxbZlO/dehJzJNf4IwJsZ6oWT4Ftxp453Uu+WnxIbxTS1rJ68Kujcc58HcQ
5936/KN1m79T+nrTYO+uKbE7UkYwqDiqDggUoj5/loD+6nfcugBzBgUVK1MbLFoxnmdLxIFdblxD
gKwrcxIkBWueibiCDePBOWCpM+J3dWl+Te6JA6iJEFLQ9vnSBr5zVNtTPrLP1MksMDULrRZQTeYB
gjU5W4bNbvdwQRbHZB2/M5utIqNM0lEQGketcvOq26mjrSHMAKyaRt+9S6fki4y907l3/nBTMjVV
KzhIW4rg8l1xUc10EOlA1W5A3llIglB5wwXU6xxb+NzB5IDjPwQ0jRgt/3xoXz2vKGoolPMvnC5E
p6feWveJtxptw8Mdq0A5IxEMQdOPbBRfiHy7GzlS5EMmrBfQZ9lFK28ZR3ktEUynJKFkODNJ0sO1
oJ1uRXsHgnJK1zdKuom+fALcwJ9IdzDLolxkkB6fNuDCGE5urDmL295TdC9DcafADaCQa/86yPYo
e7T1fviwLJurH9kEv4YUqXESkPgKWlnL6ZN/2l4JzPBRsCvuWLH6if/6zgwPqn0VnPRzv7EiL8hw
nhVQBfWawleEOfW2ZePE3/vCb882PW1teCRJAyPAI9VPuljch3biCsM+IdVkmCV5J+hFsc1wj8zb
ZBtx6B0vBAKBiybPrwq/+PimIwX+YPEj1eBIQ1r6XKnQk6GR6jhcZP0Yy+Qmb0WqBppUqUQLHLx/
wiDK6ovguQdg3nawogYsz2JjkBGoTKVXqywXyvlFaIiPdyu3eEyHyODXMniYBOpyeY/F22N4zn4+
pnYAd2UtHrkAWF5VtVEE4tVnaQFx+SvFAE3QYI+sr10LLDPRrzYE7V/rgcU5FO/I09KCkw3cemUZ
0LVo6Q0P/3JzAwGkxsN1YZa51dNYg1ffd66VE9+tTi7nMrSw4J5e7DlqBS0cH8+o2UKP9y4lHO9U
8niWuK8SIy3D2mYoPeccGztPw7X8P1QdV8ZbDVmvJ/v3PgFYzLh2Gq+CwI4NTbvJEz9G7iVODY61
F6+C/zn0YLK/DtgUjCccoy+bCWBfFAIAiHOO0JJ710JVMuFTugPybbP3q4GO99p6FM4Y7+nSAxd3
8j8MMQnftG1IR1bhofAimPw76MAfILHyYS1YCN/Hfb7gQ6YPRvdsFNFzXW7xmad/iYU/5cvTPr7x
KsgMmb5H+Cw1eyqCfEvbYY3qCmyLz6b/EBzfOieVODqSRJmKul/o7PAbVHzOw9Aaby7yvf9gheL8
IxLTOMTxMJADlYuIvbbhLe7WIiFOs5Qm0zqgVugvEnODNDYrKaRLqV/HHE2qjBQlioNhVaAD7mDA
IDZ/We5J26pe8QloWUZlfel5eW0aQLcFjHOWlzpdl+Pedrx4Ubf4v4Lww0yQtj/JA5JxBROk4MTA
oOfDLh/TD/0oafkxjGnMl8G4I6phtYUDQ6dv1Hq8pAEMea+R1/s2fRl7zN5HbzwMhROCKllzBxOU
hpPcXulUEIaEKIKGY4aBZXJogT900Hwnod0xOAWMOf3ru2mMHwV0g8ZYQ4M9xrllkBFQQqyLpNlI
IMMr14zp70OxEnwqQmnVxSnidI3+Iym2/SM8l9YatTRz5RcVyEkj6z7RqubZueLMmYFMO3qEgJjH
9uwWXksEToJ+WqvZMMon3iMrPUStJPi8VvrW7zidNtb9EUkVhLBSvKMLcHuDGVwsMVsyycsyv6N5
VyuqI7KVYF4brM8QhpoxZxtNeDf/PIBlmE5oHjmRs25wL911RvyP6gn3UbziGb4HVCOaNCPM0+VZ
u9lbVyAqf0tNBXhI2luNwcv+vKVOpom7IyZkQbrSN+0K37mB1DgBK26uuZja1xnqks1yGUQyy8MR
S5w6XijHwxULGfB2253RaA0omZNRnAKWb4kAw54hH/Oz3SnIe8sAfP/bRv6XSK6+j/QFdVXv4QdF
0C3k2dzLR8MR4Q53SMPgnuAhttG+2z3V2B9XxNoOkepFS3s/lSpmOYs7faWD14dm1YAWhxvkb0TM
VZ6ifp6uFkT/qGIDnSVldvK3gOrmc01ppX4E+an4rcFk/NDBn/OAVqHWpn6pMd5truU3UHVpe3uA
svPEIejowWBudK/e0BX/yPlcs6/FsmVgAPDAkfGjLkbmYUdMS7iGzXnyst+GF+IWmFpj+yS6MHnN
3YYudEnUMQB2Hc0kVFqfVLUFtC2VLHLAwMUuY/f2vo0/wyBcxxknBmO8M5ZEW0sR1Z9YA0LAFTgr
s33X64zWpefV9vOF3FJSxA75/erOUSpKyVqA59/LpnxuML1ejkr0tJlqKDCl7Rgzmdi4IxQUMoBF
SwUkY+IXh6vskHOOwY0HFM0QMY47UhdGuyiofCvDCCJO/LoS+lHLUKkC7xN9fwmbyVvmiqPHVaNC
XiKCAC94gzuXQNVGgp8we8ur2iDt1+826RzbrgyoCC9mD0Niie+LSUnPHyvHkdAAZH7CEa4xoXwa
AgZ5hil247o6tK81QyDLa2GdSsc52GmzjpNxwUw87584r+KoLo1aYTaZqilImepjoLosyC4TZB/2
qm0AEoq8J+Sj2KrdHjWRo18V6SH75ttq4fxseLExZfzPqhwWYrAz1bbxxXbaI7Qx9+EkXllog9sp
cEjJHQHB+vkWrWA0d7vGOLYXLIntEcVnljIZDaW0bDQgxd+rwyOvRybky8Q6ip28Peoqa7Jh6Hh+
ikY7jPl4LWKtzl35Qvi0Ugvh/bsKl7Vu1FpxIjTY9+e3F0v0AGnlr+uCWZU7alSOBXtMBuDTsoyR
50hovvGJpArHE+AoWMclaLNBAxo40KF4oh1t3eVEzDVaOkxu5WZMw+ew5GOLN3WBj1obVWrEU3ys
Vcs6e510lifBT5yDgcdMVAXrPHn4mSpjwG4X06rEiDFMA0QX9C8zvxjTA8VLl80lOacYFSpSvaQc
qtYKQI9fCF7Afmkfa1moOU+OsQ24NzedhiAKG5txrvJEsu2rq4dELhTma68BsgMp8Xgxgv1khT5y
DkdlKoy4MJ+22bT0vaVB6EljfwZGVss2EUfWjObk7UeazlQC3mrCim+akseHd0yLcDxs0c+3zDaE
y7pGanWpAvl4XImsfvWwZVj0fRUOx/jF3PZil3l8X21cIx6z3hBHljTiYN9z45cpQGLmfLnNFGZJ
hHXF5hEgj/+Cah+fCdUtsVvTygZ5O7u2EyXYnf8TN9TOYu4x0M89EUUMwNn54XYJ7O8aj2LIKCSe
WhXTPFnVA0FrAU5Bx/veQV0yfe7/pUAAelN9WSVwV3ZTtGBxBistwVEWYXfsRBEn8VjFX4LYecAs
hUSIAPbua0sd8gH52j80icXjUJvynvJ7FWFCSLciM8UxmZ1DHxX+Zp0fD05fPLFD0ow33aJPu2H2
lr+D73fdvsiMRrXwB5YuSyOtov01zKIw7DKs/0T+6NUf7L31JoRiJhhmxrATuZkMREwsQKhc2KMs
K8SH9/PP2VY8me3s6ZNlwlBFpwyFb7B31Blx3cqHaBvnIhp2GC9fg5UTPT8TsqnG6TSQJ789kruv
QHSLKBR9Cd1DlvygDXqmq6eE33tv9/I/gvO5MqEwSiG7T6KM/usfBqP75SI/El6nKSq9AFk6DbpV
uWYdkaROzMuxsntuhnfZ0sTPzIEYa6Hptbj7h4pl23//M/ZSiLO/3dawclOs81xYus/LVYLJZvZT
qk6K216KUv24H5aE10OO4KrF9rW6UEgiIJTNCzmJ16fqG3WFPJ6syV49T4GXzfGm+mXCcOxPMmRk
78ggBjDlyqK3kHGv32Nok+k9P1OfohdjfQ2Tcau2DgDPpn4yYwCOI6MdgKwT/u+0PPvqFsmBJhmN
ycXXgUh9jMVkY3LURMpT+6B+itZAtRmqyqfz2AliFyhEKGtIgv8gmBqtjRYPSzP4uLSl68Kthizr
ibR2poWmyttMvkeexqDB9UpuB+Qp0yrv5YpAh7Ue5/oTi4orzBBjb+ySai8v/QSXkdMGV4rMxL8S
zI6X4Cq8kB6gh/KgmwE+cDanHsdaM8j/L0X0stih3rIX4o0eyhOMMDIf1CaWsqbdCkE3na1Xkm+W
gX8X6Zl4Q/8bsGRuygfg7ObsSfFH5Dyo13Bgwk/aMFpFj25iZQbkYfLppbVGgvJnnhpsSTWTpXJi
j9iVN1FUjGZMqHvxVenyZElOMl/zcWQ3kyxhm2LSidG2+R43O2Mxv04hoDfJlahcHw4Z79Mqbkf5
dhuTt1252Lggw37JFhBrDYClBfcrfdtLD1pvP1SDT6JfI7p63h0/GfPV0XJf22kzGUGiw/DaCIDd
ZKiBjHhv5K+kQJkP6j+cEzJZgmZinUZiGwglbNU7HedndAHQ4JeOvu3Ez93dRSDcxQPyMpLVaHu2
DQ02hZ6Rwghi/W1B0rORIDelqOrdDJ+1wo4/dmcqY/AUyYKlYzpfaXM+FBIZ67gt3q7GgRBWeeYa
WFu4RJR93QxSOWmArCVQ44mEf0QxoR9dNFP6JhmMLc6Vq0WwgmoAsbsMFSCsrBUdZ9zrCJrUg1a+
fwA5m4gQ35SjewD1Rv/rZGwLRQGCKVaG2TVpR1cZfxzecgoD+uzK3uAZFMEVY3lOf1BZqFPe/xiZ
mFV6coKFMiTk2BPYFbaiCl6YDxDccZsT8HcJt3x9eQExOuFSuemFMXWvTHFGZV8aN+2UfZW9VXyn
Gzi1a94Zi8YiWyC0MniuexECiuJoGmMXkH62S2++JjFz+Nkto+GII4+0wsBTGNw42sKyMWy2i2qV
ajUtA55k0GYzRXvK1ie1qEMkNQYh68/vvA+Bb8upf1UfGoKdw9X5YbgWr7AUe4a/vBh5rQjbkTWr
M+ICMa4e5JwAdhrsjuXB8tcaqIErYTEtFs+NGAQo2OyL/SPERvgH1TJHds97QgSs10XPkYHYb5VV
R7JGG8ew5OvlrC0wxkbM3VsZ09J7NGAMRfonBsgS6cGwEigSAJ6Vq8KWYlWjd9eYFXFVjIeeN4cX
wbRLzDohDZSXz6H8XhYHxWKenm+ELrnNF03Kx3O85oVBjZuxmweJN3bVfKjQiu3RBPbZUveox6YX
XI/+QVPjMwxjNCw54ro7IYF5mvUbjACKNzitEqDyvJACoXdNnyUJfcYv9eZouGWbkl9fLlzCwyAP
mNGl81v9XfHQG3YiL6OS8KQymCyXTVJS3mW104iV+4XnQ6D3FpQynHINBvNXNBDhmAVd/5LA+47O
xgZaPc0UwLBWptddYsFou3Hfhs+2AtMVhIasyHkm2U7JbeQZOzDHDkIn3Ku2IRowiv8kv9kwd+6o
G62qwX423qb6yMB3fcdr/avwgwGWz0YXTpxBxz0p3tDZARyscj3ur7/ZrITV0aFbsr7whODqKcXD
TLpjwfbRoDiJLJFXf9XdrLW34RulbXDu1TrmeDHJTK7p+UDk+4JkGyGCxaUv4KqAyXonVrBw/Xlc
gPTf27SA6X/Gec2uNR1TfQZ8Se6rdQHZ5ct2yJoiqtuW6tamC43jGAN8S1gOvJ7eB8/T/CGB05ro
in6WxQ/B72dju/cm1B2M8fLAPXACofV3He+metlZr4PGL1cLirfSjGmg52wyOkx4oZ9VexOY4inR
OJZ8/AKVKSvs9XXv9mtEgW+eLi/I/SaA+KeXzDXQV9biacTZmCNcSgk/AUWHilLSWLKLDQCIKz2/
ZHpBPmTC388nAbmlblRivq8jRZU1d4aW9Bu/vZpeLFME+KACyUEmP4MRgzUabRNRoSw+OCuXuDaM
d7aJPdHOBMGTmTd7dXxLsbbZJ4H/5g8lczzgoPJGpWPfJMtKyBeQpNe+ElUcB5RR/4CC+72C5ccW
zMC2paoLZv2bvZSPss9GlSfTLVz0LJLy8kw+kXi+MiGleMByoGjGMM9u0MuqzT4S/qMTQ2oQKtVD
Eka0SK8Nw60JvZLEsWeilhp2hCj1H2pkoWZtMbgGf6zeX+tEW4PZYV6/ezCnCdhLL29bEKruyF1G
4LzpYwGs06IRt9cokP0dAXW4fTta9M9ChjkKhIaehncoBWDffClAe6BaglyPpSg1skk6rVuJP56L
4Y1mTYlKeZN479pmuadxzA66VMrrvEeLzkA51GslUPbqDkGJ9b1lGlyJaLmaKyKTv472q6dBc+YV
J/sM+8t5rqF2hQUD+KR29JhQKSXdFVNXnUN4n216YmNP553jY/2gfeF1CW60j1c1nNfG6yHeYFJg
tgEDtyIt5MERl1hK9JU+SIP9UCzN5mrwKnWDMBP88UjI0JtIX2na57OtOuB61bK7lD8ewkLqWTkT
wJ7ozicWOGaa1jTcobZt14OFgBTJxMVsrdTfjTPKcrMv3Y/qj2H86xDm2ryJIbilhLCGMtFqQZP9
a9PU3yE/bm/N/HN69PSNlhJ//Td3SKz1/a8fkAo28SYzxe3d3gEiHabyeyzWzLaivRVg6k9x8tgQ
warv7fc21vc/MPpYL8Qr+dEpOg0W5o9faY6hVdi5Z/PfXifnwzAqUTLy1syrs58AnoRg+G8xFSGE
E4qWHLNi2i9MsOMrFH+yOePBrgY5H39qYrcsSQOjllKzTHPre4WFjGLFDvTyHZ0TnJI5KL1L55N5
gbivWFFdB3gb7vkdyFv28K/K2C/N4mpKYuHLwvM4cabweIxRVIIs7Vw8z3kosnKQ2nuTJa+3p7Kn
xrp8s12X82oaSk5reuSE9ix21xG7FQsyNZVbwFw4rS+N3nYq1hxBxXaixBA+InbrqoLc/BOglypW
/dYFh38HNKF9qLDzt2WXSO1y4/2CC1PLyTOvWfxudcLg9KdnCNiKiCJIpmSs9cyX8m6n/tcHqSGU
4DWlMuCm3RFeRZlpQUQfW46R4EXhtS0WNQF+L+ElAZUEy3ZGu3lSTdMZpw4cbAnDaDWmILWaesuQ
xCU5vOJnhkP6qViXpjP02kfjrO8OIgBxslgCghXYUPNSd6+pmQQC4H7btI4wtuPF+voCtlNS4y5d
ETRPz0zB3hzHSmdoJ0ebF9eUpRU1N2Z1PzgWnE0c9RCq1Bik3M67o1kuLAAZAHYNKpamI14H2T4F
vAITXxK4M2QBcnEHYnWhxbyZXd9UFSrdoPtuvQjhVkp7wqqqW/HDKJahUa2ePX2FIqbrDQvFN2u0
tQ3BwWXiMx4XprFx/BaBR5iSSTlrCuUdIDPV5n74JFQVJLU4u0x78TBE0Tqj6WszjNLzwStf1SeA
LJGQvGNU0FdmxNP6adFcwbkyBxr3Z1F2CdY8Ih/U59X5PQNCVwndDyPm8dkpOmA5iuOEu+TQoVRq
S9d67H+KndEpS+QBLgFsPe/Tr2gtLEnPH+qyd9nmfkFBZtD3agPiCspFd3FhB58BYPudEPr4M3Cg
haKQBPBGkHs4nAZpVFj05NPoiBIQzY4GxBnpfLpPDt68zjqQxDX2MdEhmlVXchkYohML+P0a2xkG
UsSPadijKbDPStrkPAJhFmeW5F38vJddCqB9ogJUZoKjZFVAozbjhOdKryIj4kUHyoKJjaBKOJX7
6sR5MTfAKov9Ck+Ot+ikVWy5fSthUgE9JIsRlsKHQueAm09tPldK+0tPkn0lDyeN1rvMbZ5tDCd4
BCABrQ1X4TU6EbTBHrk54mHWxavZ5UXGy4K0NPIEbMrhsVWF3SxFw2MSyEDTqn/j0qK1V267gWdc
Rx3i1Qb8i2s3VYuHTfBCy+GdArNmITPZrndK+MOvPkSZ4DMukPOp1i+GMS5ck8mspmy5rpU+uUF9
Fc/6OR+bY1gcj9GgJb9bi3SgZQYMxf2loj8DQMed1+yguBZgnI349juaC8bjypwiaBkG97Zj6UrH
VerCIcjiBxWbXSPL9vzABAZupYzCn6EyFUwtwGMynYAL2splNne9vBjw3MFoMsupbFuU1WcJA382
V57UmN9R7i93bzgwBpgW8n/siWtxgNbW4SIL65EqDN15kvyBahE8Ihx7Pyjjh6v7N8dHplPLoT0h
WsP+8qCQ8ITQu2mavYNKaMug3iuYJJHVRVB9ICPXIfZFq/fNj212q06C4ko/6yt6vWTAFx9CAuAU
NZ+DN/OuhZ452XWbs3lD2GnkQDUipe1TVoB/d/axRfQV1gBK0mxFgWCp3rzQyLAeABSFCf8+CHdJ
lZUamgjOaUgP9R03vv0Zg5sPcE1k2Rla1DvcwXxqyfpnnuu7uAJdOWy0Ay4i8qobTssyoO0HLwdR
IZhvTGeXcjlu8CeSUYmhU0GTjM3mBePg8/RZmqTKH1zDJD3W/atitqfRVMsfp7d28iw5xDTrEEVg
MdKm5hrUOyEslAy3tgCHB4S+Bu5AYIO9e5BRq2MKUT9aebHbzzfZdAy4aGdOwnGVDATqEUptCLMw
n9zbPQqtgVlfyIMpeCEAJbCG6EMmpZbkMCTShINHjnnJFGJpscpTTQteWoq5O/6aqST80z9bF7Kz
c1GWz+QUMsmro9CeiQKZsUTaf2PHkRdOZgZP2yNXVL31JFZ/slw8x+GpvX8wLY2SawOgqDD3k+xN
GVEAxbjrq0T4cWLvLxcaHWbwqfS6KHVHdDYXDgMwkMgarXnIRcrdRUZ4akpotnZiIXEKRInO4EJK
3RGauLmWwcv7GNm+jhnOTirirICgE1AHtDoqAvNx6/FVvJD7a4aW2xWr1bt2B3knM3LNTDHe0TCp
0WO/B81dzuJlgKo3NjO9aRr4ZvvABI4sIK3hyjNJGUAdLxHI8dvSwnaEoN2rRQW+9eyqfm16IkaA
6nRU8wD0YLILrXzvq7Y2zjTMNNTkT3izSzo4m/AW3iWcoW1HJYSB9bYsQyWBVIbOfpLIEevblQvN
jeGRPnsS79VH/yxsCq+yuZdCVpbSHNGKeQp0lT64vs0uyWGlwbx7vziRABtuVsPpjxt2PQriGDrN
oXQQ/W9cI/UJ7FxTbp3J8TDJmh7cVGivlyp43ss711sKfUWGyRM2DTpbuwtmRqiaT1P3BLoR/STx
E1O1DEQYRu+tntwwlEQJkfRtVFMFM+1WxSRE+TdHG1h2+fXYkBeHAVDAEI6Zx4e+lf+CnSAOjNMv
RCiYL9iyCqt3ydysI2u1KaCIC6HD9KavDZ73l7zziBEKYIUKealzaVKB634ERIrJFDuoI2Hvo/Zk
7DMVBbZZ3RZvMGcieXskgYMNuZOnb230fTrD5AoxduCDJhlKgVHMB5XOt8p5boFvfUFhasZW1ajE
0nm62mGfNBYaeQdL/53MUDnmSM27UBiNuMzs+RvQzfDG74npCIqM6TkeuEW3zM2eq44LO9J9Udyf
8LsOzmrrwigeKesz1gVQSWj0DAijKp83HvwXwi6YFETWOK8p2AQ2PDGkCwqoA50fspHu+mpxaAlR
f2WXI7RL4ydRL3D3xAIzz1S7cBpkAkXYtvm5VT/B47uNux8FtPu6v+XAS7iBI/2by+4zpXtf3pZ7
kmVkWBuEzapcGYbckbNvxV3w0HRLMFIZBJGbiN4vEOO7/OQrtvfv6mbibVvnYNUw50LFSVXEO+mj
nG3hKfO0Vw7mTnyli1Qlu2SY9Xmu0rIWZ8RUAM4WDzCtopv1lvqiX9B+tW9lTxFbuec6YiXccQeW
gN8k1O7QtYaUcmleXGhoXIIZ3R+YC67+pUd/J8BNXYqctE2nxgrvvzkuhRNciEiu69tww1LeS4z5
qN2NlCue5EXsINn9XtmjIARPgu6PrqsiORLgc3/yHezjYaAY43fIy4xMK3YvU7sl7KZ0MVIg/Di2
Muj7Ew+gtcVTQ/SRk0CilYkLYL3MP+qr9PE1HFVMNRuLbvkIhPQTBvO7LcDs92yf7JLt4Mbqi/lG
5vEnaZg38CVdnb/1VSBG+EfqQRA1VnVhCdankad+r1w2kM57Gsa7MN5pvy+U9XBvrIovstfebgJZ
f0fbSg+bZ5cM741CZb1XIYGQq+uZUsllmsGPykJAovJyvl95eql5f23nFi0gdpCccrOgvFlYMIQq
pIQ4Lhbmv5x8VtKhhpo1WxWG8ucIjTEeKAs55V4kwSjyd8PfvYjdSMJ/4huxfw9oeWiuxFZZQR8+
BvfzCv8IVCdk2ClSW/vZEI8+FkSGpurk1ue8gW0FNzXq7JuP8PPHy0LyAQtbpi79RCVzUZFuNzQt
a7rEpkE03ELclxo5wAyTvlUfZYVHso8W5akZAbYdhA/4AmUNLLy32H4gKmFgEAvfKHDx3wTNE+js
r/VnxmqVOscKWZHwV3MggLJhI4ZLGncw+Ms2Bak0PaT0cnNAqnbsMdI/lJBy468wNgjPUXt7EM//
O0cwaWA6fTajBZR0Xk+spnNm8WGLKY0d6276bVtJxrSg/xDBKC3eMvH/6EM8lnaVTRH1hknC6Eve
okc7fnp1uBETtLpac678h2+OvshANFFTFjkzqoC1h1EmXA8coRxw17V9HTnEghk4IO0c0e+roPlG
y8bIhAKx12hlsuKVnH6wNCPQVvlKz+2yrmBP6Gbxemi29DcSLJEA5tAg+hfsjEJWFMZUTBfREuAz
6QSCLCWKwf7nOXWzh8JOTdsxeOheIx4df4Nx6noPquS9Zh/HuD97H/do/kNEWpjPJ380jq2ZfFNK
k32Ul3fe3zZii3ccZJ+8AEe2uDx0GY39lYTdA2gSL1fPEa1MbRbjVysvAcQOlHrxAYNSTGQ9UIeq
+oJmbGr+Y8B8Jw3fSRpaGafA8kHGiCDMO+gbX5uZ60OGUl1AnYe+EyJlKDUFvOebd9KYKU6rdgF1
4Ig2wHxW4NFjAC+tF88zFrnqQVkrsd2HiNQaVq2RO69zlixivGNf2xN0NeLbuCXhjTC9JAx3t8Aj
MTFOpMNqEspNprIBOIxDEwyffLWiAhubTmbWwo7DxyF+Gj5izpEzPGLftcmLkq9DN4NVDGjZWX7z
HKM+AM1HnPcuddYr2J9qUA4XnP/ACEH8bB59HszFpnbLqhjfADdUFJJSXeTdXSYh9hq8EDX6kRnn
VoVIqFccwHXf/vkA8r5LMvtt93p+6HddLtMFkn6DefsvbIQTSHOtPqqyWYym/9OyJHTOZxvHpTMo
AhYStAvBgLbiKNpSrGoDphKVFnHVZHPw7m6GzEjWK8simpIq+Iyz8XuinJsU4h9ZJ78hdb7cQD1k
ouk25+hCJqv25BDni/N8aOx/HQrCFmiJO2MMoMYd2dUx2wCWkdinrFDdlbWdkaGGxx60lhhbHDFs
HRrxdJQbVUdOFd7UtUTo+drJUjMBySkKCZj7wFovPo7kZfnLXB8T956/zsmwtuJXnHa3NtOzW/TO
JMvLUQirNzrOjuDuHXauQgZj3FTjMcElbwwC5k47ClJzxPh2FGG4ifS0OAbdatiwgM7CKO0AhKCO
3HmlFTcQgXm9rT0alyCQ2QEg/chSeFSZTjjmJgrkKSg8aDSUxzB/hjJGSMNMlpSCeBhNEy0pPYYx
VNexD1MtErXexwELjWqTeNbDI3WwEs8lJXa3xnyx86BVKxyhr/QKvmh+SIfAyqUHpn5IdHY5Xloo
hc3yB+sVSjzp7WOwHJ9/lWEOVNgGY2g8ulZ62azWW3UwJCNAnPk88RjtbCwxCg2IYy9pSqJc+8Tj
+/f518UzHMI79NbzD3EWKvppp1sdkGW+axhLNXsRAMzBNtT3btnCkyeqpgyRIMo/yPv8m35wZ05Y
/CKVyeuaZ1y61yN+N01VDXZdpadiDTPAHotSDQrQOVHAMXjI1uy1d4y4yshs1YtJRXWV48eMxYQJ
rvrigatFyUjfMtagZEarjhGfk16Zu4d6RFx346tWrtBF+2YdPRiDYHAuSzDtD2sCh8rka0e2bwzC
aUhdG0Y8WCaU5lJw95fRV+9dpDNfT+dSYeg+RUny4TmZdiosp4qXmYVJwh2SA3Wc4T+uLyqjel6/
M6BwjAIWCdi76+tiLFYdG40uN3yEBYfAbm47M3vpGRtIz9hyaout0AKz4RW13a9h8ElA8Lf4m+1C
qGWgEe+0V9ybTPy8zq29HDJsBO13vAvn3lXxRVRZ3QnVVGtCUYEgXXI0WveoYYQA58viFBGqcDr2
BiWk47cl0I0UaLMQWDoR7sw2lsICzCiVQqTaVTH6oAf5Q3naupB0ecXAoCXrYlCuf2T79NOh4COn
g2TGtrDcnsZ0J0BHWPxgdseVF8zFsEzuk+cDchEfGHny1lK1ojbzeNp+dX474OIOU8eP4RyakpkT
XOs2RDL5QDlccWH2JjdpOCf7yjbZEBD10/4LyMd8RhrWxMl4qM7k72NUPZZhE9Fg7+4TZtBnG4Lp
OyNQsjKVFYoYA/SVXha9W1L6nOJ5S9MZivqAYdJgQ73UEt08vGFvtcmcdLC7ZKVytHRLkWUlTgHr
7j4mtXMLe31Pb0G2ZYVjm+mXwcctSB/gpaiWKi05FwXR9xdUtwLgYOtXAqryNDhosdlPUx32VZ3H
27bMtet9ctZEyOp/6R+sFq2wiiIC4TDCXJJZQOa4Pr8fSX495CV4Pzxpu/yr/lsUESXr+TS3UswV
guAfo55vM+qzV5z61W2OP3vjvQYKsHbEGRqwICcAv12apYcPQtU9hTgg6satKIjIcfhr2ah8HGL6
PVS+uSauf0DE8KCMloXYrxTa32Nc2zSTn6uTRJXExp89dabv+ky0vPCM2UTenHfyz+c/79M4WoCH
PhTbdbnFxC9SodZvlXl8mF4XobSjcuRZ/R9ONQSrE9APEMjpm2M449g7M03VoZ7C8ffytEOL7T4e
eFYootuIrx5fmiylq+SHzk9vN8x3Z46lLtDZRAkpc+YuGrNCUfuPdxPIPk5cCd8S80vYLZ9YRtio
JywKtaGXNljaTGP0RHi/GCS9g/9Ce5KcGtbrZ5Bv8m1BT1y9q1q6VHUGyQnOHpyhUoQgukAg9FWf
lI0kqsRKc+TgPZDfCGbh6BDwzQ9fHQasE9UPMHFVjv3FuLdM7T6cNw8qlOhGdno3CG+iG8Z9FU3k
YItFH4JNf/NMX0Rn1kG+yOekmgEMSfWDz+2pKCv13DfoCXkkVO26zRf4rofIYMV9bnplp9lwi39/
Twpjp2PJkdLS5oWzOphZf84EIEDicMRY8C3XOj3td7HCXcrZf8wA5euAt/saVmeb+qWqryURan/7
KSs1LSBB6QsMPUiJZW+yko4Xjx+QUVcplKL4zRcPnWypfHkLG4ggi/Q0H0VzuBQJN9dza9my/o/q
XJlfVxhTvGYw83z9ccEg7uFZ/OQcyt5K0wpPgb0cak6m7MDLu5OpfKbFZawXkgI73tZUNXBGle3q
mOXVdaTHMNqvIOXpoLwHmbPebm7zP2iyFHff2eTm+I8jdZT5212W/za6prh+T6q7YnmSNXHCqY+L
FhHg180INs+1IG7A+cMbvmx/NjgwkzOquZD3A+sybgk45xJ2ExtnT2JROJTnkoqJ2W89oHvEL9lC
9OcUmMZ8Ecq0QB7oRb+TSlwQDnR4Yw1oaf/1xevgTQCXzdsKzh2uiuMbxPEBJntNQgRsp1eCrkQn
6SCvsIbUsbtL7q08bCl3CG46H4ZNyizXYvrtdp5VqbsIdb6MHv/QLAWXOJzWH/FCk8wcCmgzdb4Q
BWNqEqJcLpw2yCWUTdvFcuXbbK8Wc//QyBiXWvxYyqU7qLLHZsfotO1VKx4aW5oheEQc7Ajxjh4p
cYjmlGTBq066prhBjS44iUXQBMkTVAb5odMRYsaXiH41F6KAldo8m5G0JevQwpMt5kzHORpW9Wq/
inv2UpdcAgMGC97JhxH33FaksfOhdTO83vyQzDSCKk5vzEr2JcXIyVH+WhGUT3xnYeSIISoLwot4
5RwXnvTXzHpbfMBLtJF5LC24Id9A4VlltnQLvPVEHJl6yX4psq0xg7NfW7v8oIkkGCD/2sL8NeLR
Z5x/gbZgYR7FsVQpQWFacO+15Dm07Q03P6O5fDXvuaWPfDdn5ekvtvCzyv/i8+dDY3EtJQRdgm3t
RsLKC93dO9XCl/JaPjvvd2XJfMVYvgbzlZnxhqVpExHZMp393ubl1GlqbBH+ignmgLJZO8ykg9l/
r1mhgsIymhz+n7pi5vxiOcBIkcNUvsFgQZp/xO2N7gr0CqxttuEg8UukXPDLOxB4bZRJEmAE+WE4
/go+LlnPEgwH3f2VkKovjC4fbXgKqrFgl9OfNrfIKQR9/GdypnyvK2hSt77kvU47y3U/9aWlLwR8
lrQpgAA7BSVdxsK4R+MHLi9UkhBmUqKCayHTWw1Fw4pxYnO4U1A6HXqw1JqYK9uvX7kVSrD0zxZ1
fUBdEBW/O8YMTiOIS6q/hLbRr+39FBMOKI01vc+aruaTI6evN/FvuTcq7jWKhh7V4ixUlo0UAdlU
tRwdQMW7sxjaGki9rFqO0e4/htLs8c7DTITx2vxc9S87YMEalcYjShDcqhsWN0UoqcsMP56ZIAHV
27gTDtojzlTZKEIxau/kyUKrU0KBzLK5KXdJgAYlRAaSA2JyZPo1+jcjoKQID8Hl08gQimT8LfQ8
xrOyZ+958nwOjTJczV8avBwGTQQHKy25nniLx+/nVAvT6caOpNm1z/nVwQWn3h0vo8K3oV8u4dC+
oTNlWINE6r5QKfGEzQRycTYqgG70CwaHOcE2ZRC7F4TZqxG+tm/lSvlOdkcKQPKr/IvU9EeSl6i+
Jbj+BY+wZj6+21TlTR7ldsiHxKxJfJvBvMy/eT7paUp0f3Fxis1ahBj1clM+PLWDBTt0gqedStRd
1Pc9rNZtG1YH2idAKMni3diGWYDEs38tH9778UC/z13M06+EPvLT6GRffrQyG9XtKBQELTahrSYd
XttTWXtBnqN6AEbytXgr3UCVx9hQfjRQ4ymwy06Kl+TtG8JaBTAq/Kq6poRs3Yi9wHzrFJJAu5VE
2AywDozRx1SbanZm3SXtsvl7Q1sxWvWtzicE3NxBLpdJmIhJKUZ/L8mdk9AhE3i7WZxnHl/v2g0a
CVXn8S0eWjuwiPH8p23TFhay1Hrw189uGYosmYNmPHY4bsoJ1pXT3Z2nc1RfCxjAoSgneEk4fwgz
Hbu2+GXcMTvBBJqfO/yZn6thnk6DZ9kCjDIzKozpNimV13zXWgIZ5KnM4reF6KxR/oHLkqVnHnw8
ZtCcTddBiobRyCpP2iGew8/FdKdNmmJ6fU9GfboxPm0P5V/eJiqZQ1MyunLO/Fhsxl4kQguR/XMo
DPtrvZnSG/oh0er/5oOUbeGfp7rZIRM2t2cm9GNubmRbG7z5yTjaXPCJ1Lr0kJD2oxCzON6WwWdf
ZMHU595aT1DfL9nJUf8170UtXagfXBgpUy2+VIEFd5TmC17+pZrAfKWknn9XrozZGAcuZ27Gc3JP
t17Cp2+S7DgrCqQe0M7ORM8/M3UwwtULe4rHy8/1zzLxq+kHviLU+GsJ5wZ+7p2ZhQvzdFU7H0Ss
WTZuO/ipzsuPub1uMsF/3clDXE3IPyvzr29iZfE1qnNiYqqATLbU+m6eye/piyRXxcNavkk2gEfl
FNmhNTlFJWsRpPIa8Y971ac56GGrLSQmO3OfqaVcQXOGmZVpAMSKU6k1x+s938yKWJ4u71oKEV8E
QdtSCj7pQvt15VJppirOcbCKyeTCFwK4JBf8X1s3fXweR3jeChtO7ALVqMbfmdjDfW7OJvugLjFR
FJzBoP0miznxEeSOSMx8BIyB3OteA/D5vIaT3E+nXtjTs8E2LifUlJeqZsT3oQqRInmf6E1ukBrd
kXzs/YlS2n56I2CwISuviWSm2MQ+0xpvvizGsTy7c/SrIErFwG1jvI0jtOAYjfbwzmRrConbTGs8
JeoVC79jFSu4VdoS5YvlkcU28iaZMxrsKfnKiAksODz3eAXRRq/ZvzIWB7dZXYuuHQQ/ohiWZmXS
YAAFZ1hqHWcTPgyOO3e9hvg6kO6MBu0Qap9XxJxZJFRBRSbZiInQRmkcvcgXcHDH5yyrRx9L+DmI
torHsSQk8KJfkiEFWeL6lVfjcUMTJeJQDvPijlJOrUPO7mgN/cZ5n+GGyurSOVvRrKeiYRqFArwc
HFKDBpHqSwTpKRPpDxSeD+OHRQE4pvd6ZKEBPSsHrMvJQe0cLVeK2DFBjBzh3sWiUbQRgRavFMwS
VxI06Z4mgv5XrenbHq6Kx4Ye9CblOZ8GBUc5JQKR1KdNRIQH4m+SX5OLevQeDobTuuVTN7abissH
gXxjprYa4YoOKEDcLvU96kNb6gzcywBjWoE++bfTHM7s5fR20C/0JSu+T67gcK9vYrPXNlLKCPRe
XvpcWsTL9ot1YDbAi8Z91NKxYWCcMoGZTfHif10ny8Q0isJJx9dDV79Xc9i5ycS5wUXDEumZ3Vtm
sE4RKN+iHHbHk58waqxRUTK+EO7/SAtXdDw9mWSwgqejWSeYy3EB7AuOMgTU32aW7z30v1gu4C05
WX6UOUZbEm4yETbx58BX3djHzVRmBiOc5eTDsmeTm1hL/ZvDh8iwZfbjoNo0jFbS6VNbo2HkxvQX
JwS4U17jc/BVMTaxtJH0w3pESd46BEXDdWhXWERIoO2AjRyyywM2RbCnzSaN7gQ6Wj8RSM1vSBJg
9HL+SBNBSZTAcbfV5TpKBZNZcGfdFBkNkbmzoBVLzgBEkZk/1JL9e27Ds5dt95v1sPFH7mjC9foI
/0Toti7Wzf4u3q2OniA5zL5FNlvrZXnIyeqzd9Fz5yvPt4DK5kjNqVpjtYG96gbchHIDXgw+HFh9
Nv9jba/fYCtVfG1wZKCyeRoXYTRWJ0UDYhIjUzy5j+nzI44Ku52Gc+G6H6n61VYIYyK8kdur+Q40
Ur8bLZfqZtDPsEolE2G8Z3DLCvlXEsNHmyDgn762obxn0syei97NZYw1l3BISppKFrIr/mB7pOMB
65lFYkx2yILl4dVrv7CvNN5IuQVJvfFHndRF8CFQGSBaD2yRKLT5xFvGg2lsMchHQBtY+X42OMTl
WForwv2XSMPT0BfMZrtOfbt5bAzVdlJ42UfQCFtEwlk6uAUtq/WhWGCSrl9Z4Li7nw1mNt+dsdQf
CEpqrBRy84PqYGAR4r/HU7O+eT+NiZ1Az2iiS/IZXsDVkmkse3xUDykYzRUBXGEKf8igTblFrElh
1booeHJF1blcAvnu1EAfEynPqQ8iFOQANBlsy7X85j48on20hWEGUbVQ8CdRc02LihrQnuqxvseQ
EoGzR3t0+f6gmz1xzZWsO7xfm9XtDFk7NyD+Im1wrGRa51gU20uyiqSX45XGWN1ezzmCILf9jb+X
yFjPaaxKkA4QX36MJ3mz2AsiY0tqRx9QXvkNdJZTC4dJteBNmpkN/AUUf0AAUC7fPr+PET1xqfB5
0BE1jtWJK739BZx4lvmMianEUEt3Iggon1GlafMVgk/8INUQVOGvwaiW9knaFVkAfyI1sOZHxPTD
Mhlvz5Y+FxgSrW1UiIJTDZMME9kIX90SzFqzZ/+YQhL1+ApotuSqGAjtR+nZekW6my1hnjgbf3PX
JhZqaxf+iTGzUxZlvY4GO9XoWp0oO1arHenT37fVM31gZr/fW1L5kvZEgEmUCF0M4iT7VXd9xRSQ
VJLzh3SqPZC93bJe26Y5iXNXLUgq4gkFJlrpuNOPYIYvR+kJVjOeg2ZqjNrQpZDaxQ4lTvPXgwGk
R0wC/ctkrQwAMwK2MwVISj8pJ2oL4+EDL25cfaTTmsEbauMrm2rLkBycDKL7aGDwLLTriGcB8hkQ
04NK4JlKTur8TZp3guNKtr17QgTJlFeFNyMRQiSRsI2TJuBBFWa1S1Pb2HK1g5d0G0YBJ/ObWb1o
cFPeEQR7+p3IXBzFDDyO77+ZJciAp1enO7egcX4ArLmkGcjU/G1pPtjk6GoeFG+UZv5LEv6UDOXa
AX5rH9N2oRMh+fE+rJsvb3rcICFj34PDbkCItoE3svGgEODdb82I1KngGID7VOvEoLJYkAxKa6GQ
84qqK+r/YrbhtEb2f33R4GPOfDmzOOBO6nfpEMtMtdcNwiuLuxsQVv+fsEJIYeLyblKTk/WT7aMo
cbdpYgN8YQDIkVUE+KJlz0RsNxABKL8ORuDLHT0TWa8OXxL0x1Azp6gBL89O5yb7FJFab9tlAw2w
a5SiTSnvlFFnI1xetLrJbnKB7PlgxUa0PK10R2/hDcAyvxknEp0TCpP4FBBYzHYT5rQOfD+fuBws
V/3+mXKCCjJIIQd59l93PcaCozwtz9cJxmcy9eOt/b2S4Gxu771srOOTShuoh139Gaox6ULG6qtU
1/a4gj2U/f3tvDeME4CRtPTvSlTtSZF0mB9kPlMX5/h8boSn5N23MscsurF6t/nBDBxaiBpO/X9G
TmFmbpuO1FKNKCukrAfM6VwOCkKuNmMuJ2+XiP9Bdnr3J40cd35+lKiIdlMTzJ4LLTvCjqNnt37T
pfhHyxuo3LE4Qr5cVMGp3XKZQzZxYIXA0lLpyKy0iBnrka0ERirAaQ9dRqo5AAfwbWfksQnMO4Hl
IEdk4tcrq+UhBaK5DqbgZI3A0snBASa6J8XZ2Q1qO8fQaQtnd83hCZ/5yP6WSidLG6Vj6fMqMeQ0
fOQnCxKPWmQgWxNLFHu0qTj/Ih9uM9T6zzoKudk3l+QYHMoQGcBzAAScWg8BOZfT/ed5CWQ6/yD5
aWIMc/v7CPHUD68lpnWFZ3KVsSDd7rlZDVL/prbovhgM7ZZy/d+zAtzBiB7m1xJZxlODdXslSirZ
Iz88LLIUaO3wbIkkQvSCin7E0jZL//kA42F1YICumD5iNDP4Te+PhhZcMgFb2sAR6qvtWEtiJqac
K56G8JplKZx20Olc+7zjQIThwpne8DhMUioN9M0O8LpAYHG99iEJtgGZdC6K0uolZ6VGmvpAfAZO
d5ZWXzN9dwDpDizKX3e7pKb05CeWDSrV5pBcXache4HLIJJY3JTrbzYwTQY7RQvbAmi4arEv3Lis
BucYv7gPltTh4MltUJGrlSEyPni/4C7a5pdT+ozQEXvjSadppxQLG6kR+zwCrqyu2BS7UQchyy00
LjtLbbfTpX1/qZ1Hk6QeOIxjSLB7yMbV2EXI8Pz+uocM1Y6KoYXi8Es9Mxqf9dc4YE/nzWVllBZy
0hU5qouG7NTCnsrpIGxNJ8UXxoHoX6AcOtxXeBZPWe9Dcg/WBJs/wzYDRI1myTWqwPfq7wzq/39R
O2oKYUsdZ4g9g1JgfHBNoG6ukVN/INREzjtI8KYghmKMamrN3tzPwVKvyaCuTauO00BWa+DNmbZw
B6R/wPio34CTsmD31MvvDOIakbXAIjHuISImuQpY1RNQCMwULgvHD8DhfPLx0z3AWJm5E4reUBq2
+aQVwOnTbhviCi44DhcOer6lmsuvK+Wiex6enMTNS+jRnvx4I95Jz9C9hWA2h5wx3sUmjZE1b9IM
Wa7i1cw44f4soBFMyYAyNmQbaGnmebEP0yDsCfaNd3ip80xVNm7C3AA1HYIUPkZxeYES9de6Ip7C
3VuXZfDwWxMPHCAKwp8S7mCzPJ3kjkF4vmqvvNAUbTxEPMFfHmExx5XU1n4VbpUZ+ymoHS8GgXXi
I43v4NXe55oVyv7TLpx3i5wjpaR4Fo7p2TBCymB3Aa824aQ/VpDcFUQY8MBcx1ccKkc+yoiAdoRf
YB0mHkOADDusYNvOH4NeJDjb+CHjmAWAfUqNuUdilUgYt+o4+D5ZsWDso9f92exkk6DQUDNm5U/V
hOcqzy8PcGLn5k7M/KxLs6/ePr2bt6Lz3JM2dkyhdx0REwWK/YJxgdzbei2AQdGi4brL8HB8A+Tz
OcgpbKpm5EUvEKd+Cvii6EcvExZSQiSNaGpkVCez0SWHlJXyX/6PPrkvp1visjD1ql8OvoLCwM2e
0h3Xgbhv1u9smtcQ2t9kxZ8/PhIj3HEfHV0xTOlk++bk4JjK+ZH6Hzy2mhyG+3KJjADeOd+9mln+
VfNF8e9khwmKKAGPxbKtsmcqoDSdnQOJspIvRpMdeYqxiekCBMU0qCXwADFKixH5dVCsdPcfgl/u
OZ57Vd0AzxSgasgs0KbrmF5xBNrEaBtv4rLpUc3gYzaLrggZhz3Vz5nM1Nt1x0NQlROdJ704xkWc
rkDJkTqLpdAzOKWz/2luKLT084SzaSAGeyQOQix/BvxkE58Hm3pXdtSNDyc3ZWcNbNAd2Z+EfXYW
uGiSuXcTl/b3o2TalGKLDaQyJ+vDgluvw5YVzq1Q/NqY8A80hkkhHlZFvaqQO8INUXT7vy2/xThv
pMNGEiGDgYBRvOHfqjnKKeFD16oHAIXBb4ndWwb1hg+Q6du+xCc/O3hS4aY5RbjBUvOKTM5oO77R
L6lNl5jiAaPfRdfoIuEseVSLXq3Y2Rj9Pa0ijPmi6LSAmuYVTTl5/vsOTYCky3Ue6MwbsTbk5t3t
TmqEzLam9QgZLafqWseqZi9T/6+cr9upGmYx4pPiPgDMRqcPNY2mA9Ac4duNT2bCm+OlpUepKYyx
2zpdE60CJSs7TDJlGmltExx0vr10ca3QTBXL3HLpT2kH0h/4NfczrgtnbXm0vD8AVOtBS33DfLiP
LHUL1YwumqfxPuukcXNB9jv1ng8oA0ks2t/O5WBqDqXuiJqB7E+lFJ2PIS/itp3hwlyARcsG5L1/
JC663zQoTdOxOX0u1F9EpUcuebjd1EKgyMVZcr/YpqxJTIu6/azZLTNLQC/9ieqsiE8kHGmNdKdi
BKATQ6ABkk2Gjo4Suzp33uOb++09b0xRjhrJt3cKNMQnRT9JlTrWERu3HB/WJFxUPm17Ys+Z/6oE
/fxBYIob203CvCcuAQwfpkkP9JQELobl+VWNFd/SIxOjLVP9QAc4P0pgN7zdVsT84n1nidsIYkaH
cOMOuFev+xGIvXwpcmJJfKMxLAf2kc7lPz8CYCsmdr89T5Sv1tIXLCSHlQCkQOV+AaZKrvBapGUz
ZDzirkwUZXt+6ghsKt9wbys8J2RwC4q4EADRN+LWQ1sZbcqDXrUkquO3xs+jQOqQj/hHFrcCetce
QXbEqOYke1J93OIHLU1ye+txc62Ha7I1UXJzoj7NyYUDuT4DRawQ+p1NkQki/UckUUQUt9ZY/kZE
W2I0P6RM6QdCxtTPkpXjTr6t6SbyiQ66jfqj8CRwusUnaVcsJsYI+tT9ALXeB+24uY90CfcO+jUq
TJ6AEIBEnKXsEOBORnszSDwQwutYDLZw0RN3ghUiMCfulFBq16rmBHBhRpqBKiXbK3Ibb5Jyz2Y+
NIRUsH/7V6g3myrzAf+JR6Kx3x2t0Ri/qAH7//VT1rIHQpNASnFw9Hc5Sd8oHU05OdlNlVzPnjZ1
yQvb2Kd+gGukC1CDdSBoVLkA/YMvpQ0sowGOjhUPAIj3Q6KMns38E4CUV8IRVue+vF8O03zc0WOR
q2I0BCD1XslRPScr0leOnD2aq4x759oxvlvizn5X9ytZsG0jpvaOCog3ARaNIIbC2Bc+NA4QoGyC
SS604w9RE6Vna9XD20w2Q+Mo7j8hVyL1H2b6LUc/VSlTjYT85au64pQp4BSnS6YWrjVbxedWOQDa
mZhRe61zM2XoLD6pizj7O52l9N/YKoQQLIZckUMx1RGTpFwafbaB9VkpfdzPT3vu7McTxfIrc7mU
YGdvPdDb+5utdEj1DNtfQBHN9x0oM8r09L3MJLLHEECEQP4YYQ0WZx0bA1NrfKzC5qtl6DfTFpmp
za3pHxDSPredBBTxd0uuP/hHcxXj/DO4dsX2yzpwmtcO/aVSw1Gs5VBBGXEUy9sodDq2bXHYnhYd
Z9vL3X3Vax0SOaEmecmmAxoEhGt1v1TyHh0QTMSusgNC6dS/yb1Ye/GRvb6IrmCMXkNesvZriwwO
ctP4/5jgetfY5dQO0wZzHZ0zJcBZLGKoxomhE8m4Ys7VCgR1y+YrV2vP4W7UUz/L4sGTt5Z/OUpG
vMlWipjagQ99T0vqovKaXYYQBhzc9P9XWkO31cUIUEl/xwUJupit5JUCju7XrIQYob7rjqNOUbeI
fQ/yzLjR2hVWbLjoF80aNJeXIHzaLwjU5vE0XqrjNvm3UnEiSx6ZkhcKAIKCR1iqboN8ekHtO7xA
iQrfo/WU+c+3fxyZzfbcASeiire9WWd0mJjUF6YTR0XnvQPJVAVy9Cchr0054FKbXlylHokN8SAp
gpCY3UqlAmOPPRLimuz4dwadwb6jEizUC254aKiU7JWs1samv7ogxq0CFlaZdxFZF01UN5AZdbTc
6c5w79flsncMnyAc54gncLtxGjJFNi6NB2Sb+LeOikv9jCsntikdoKeq1rpU2UbjoRDu2YhzyP5o
7CnBHUpo7FxEgpummReU+Ky0pPE93zAtvSZFE0UCNqy/nNm/YB0ynEjTF/QQbKi4jhat9wC7B/Mc
l4rW87IuaGtER0OS1Ppw/e1rxEdUE5H/hqI+VBzdS2tIK5crJRJjwDMDRrp4e4bK+WkDMchY16JW
Ah+nLtMqGpT++aRGMVqk97M5FkV9FIupJ75hc7UTOEtyi6ljzEBVzTbmBfTEtDccVek3SHr6T5zc
8xwQKb9UTVvl+IVVSyUjxiVP0ABYl4cR5O7ZAIZRSZI9oTFnTbBAbPz4YXAnO/kziQLL5cagSkqJ
q0RtotD4e3CdDV4+3AA1qcZsEE/1ZGETqThJTyHMlUUyQk86J654Ci699e10YBCkQ4lf0xlPphCF
+vTNhDZF6U6qV5aClWYrwrDbxn8HIPMmEIKGOXW+24va/wMgJaz7PmkUBOM4OqfJCzf0zytTs/t+
2GhBMLGNG012Ya3eqlInLyoegMALJrNcmD4beN32fDArxw/dMX+PgpaMf4v6aONtShFr4RqFWgGY
ESlKXuSqZqlNRdhDLFYYhM1T8bB0y6xSlUnKY+9ot9sxkReq6zCdM4oTOQq7JsDk+QK8bG4tdWo9
ZArcV8pkyyZGRn2xZD7TbFUchwx0SUF2TGd6wGh709ZFIIZadCj8RCgLiyAHcHxu2u05+kj8mh5/
JooPo4EfTeszb+Pc7aCVvOQvyZYyKKfw5RsBmh4gVWz2V4vZKVOuXHFnG5wW5uy6Un8ZQNe23j5F
bX8y3KNEwCipnWUd0rW53CfiKEu7s9rUHewKApPsmGXHlz18+AnwlVcsHYaRSvHtV3qRZ6rnp5lI
SpbX60p2vJibm6iNA/QQA+3/Qd65bjQNFY8iKC2d3wpHNiDtuP4W6jtrgpjDVWTFrh5wG1LCBb1z
kFu23NfJQsbUqRPqlovJOozT9k6BjG9Rmk6rsasLXycTyl22m7++7pQ0yssU/1Nz6oJk8CcFhbES
jgP74a+keMPBMH/Rppu0bruw45LHDFpBex+WRjjSAPf7eQSqZ7mAhw/vO7I+Zr1TnGOsTT+2SxFB
8kqN8oaNy+FuwqQw0kvAgJ93bdRZWpKZbAR7JuUp0lofIwHMarbAtvW11zflyOjt4MWME056GuOE
rXgRuOwZAHvx94E3xjWRMD9xZ1MLEbGfaXProQiHkJGfb4eM0GnGa1Z5J6se65s6UBXcWQ02AN3R
54pRJL3lodVrXA3/e9iN4uskBwgUqoK+z8nUhd3R+u9byT/EzCGkSC/+sYLCMN/Yn1pCBorgIWGI
ePivVU2k0BENdnMV1/0Yb9wXaE4r9xfwAQIyYUGVaxmm06+0A3S8d4DPW0JezbGS9oaX6HDhCHPu
b+n4nVTq/u7RWSodiXLyHoMAp2SizbgBrHjd/M5+rw/neRvnC2U2I8kf7dzph4SF9q0tXGfK/806
8DQrqX9MJ6JUFY3UBY+tf6lhJ+RTzxWLqLX1Xwf8JqnYWb4NW/TWprEGKFxdEg210yuCKhKzYk2r
9d6y70wCTa/FWBUiI2NgTrQvO1iK5j9TphPpdSRbYb0Pz8/kY7tTRbMntb/ELMLHE+RcHKLTz2Jc
BircKJBGxrr47XHJ4SK6pxLKszTOcLyls0qSRcFkizsWr/Y3vVnEqSvwcxqQr5XQMPsgCsjS4sQm
52YZT912mh9LzYd7ZWOKzIJ4T+V+raQcunNMOPvlxKcLEm/aSj0FZVLF2dFSx3p1T8HTYiLO2RCS
jE5+5Fqm401GQ0vKOgi8GmyMxpAWB2DJA0uOGYtY6A9tQ/pfBg8D3FjSigXU2o16e7LVnrni2wZX
LKAGZitJR6bU0YrfHskVKuuAq9+BTaD1qMA39ruzSg/qMZkcyrB+OfCBF3GUU4vfvjxbpXnlQaQR
94Gbso57Da1chNyES5LcUVz/gQKVWD6bGqMwz9c7kl6gEoOkUEzgDgiBPO5CpdqLzD3TfNcR39jh
Lw9tLArRq1DekVyLuWsjAqjGW9B8kPKcAlE30T4JHTC8Uhh5OoJkyxOndt8r8pMGBe09nZucv+rK
7rGDSf6lk3u9LD+bd5hZXigIXjimkU24wfBXaG0XmjwvPdo+OX7BlZAQXjWXkm8J2gs96TwMlNEr
yGJ0dEusMtUDUVxwsIUocVaKC3mVSGmakI6sDY0aHus5pxLwz+PX3MBuCptegxKoDyuA9vaiLuaz
Wcq5IWNR0NurIWts6TNADQ9qX5VzuxneIyW1uqvcOiWgP6n2GV0fffg1CMj1GGSkQLLaKxkVEfhd
yOc8seNt5xYZhvNBrCSQ47VvHr5a/trr4aIb3zpcGDaHXYGt4MYvb/xq71pHu5X12CTk2P2Rthts
KxbT+48tIsPQA0aM2UOgOYHlcad8+ovSbPuwcMWDCcn2Gv4dHoUqn67nGixh27QDCTTUkg8gu3rd
1FrpR0XB1f3PCrb2j9KbrzO+EedvTeJqABp5zxjjLjuqX6Sz0jYK2ddCcd+w28+UMHiQvjAC1DiP
hGolgQB/PVf5OuTR0GRISfZwe2yJzjIMK7uxICHOyfBom+8TOuj0IXiiTtKrmFbaInP7cKg0WSyP
vKVhuJzI+uoaQhLw97ghmGpejsLoZETW+wUq3YF4xIo675bdpFiRyRPub6an3G/OLZBw6ugnaUoT
ujBru0oGVoWhC0skqgj71BJJNWAAAjkkfnhZAPU39YpuFrPbvba3rJRGtouPlAP98uOGpqq4sJel
qVSFRUcPOpS9Od02Ejel+3kfXLeGGJ+6gFIWFoUwDw0KSzdC5AQGS5OR7+IKDvjKoIFuNQl8il38
xCDumaw78MESoOcm7ljEhwF0iDDahA/+x3pbXBekLSgy2+URDHEZrOsn/GXssOb75tAY0qmw/pY4
AaJn2cyVLEfRLK3nONe4L7iv7gxRDz00FBo+TuIwc938VnBugwjSrV7RJdXvL8IW1uljTdNlBwmL
6N49lB6ejg/kWoyRVnA4wbaePx6U7iXFCREhDQuxOwiP9ruzzRW3kOJpDi7ef6q2iVEtJhb1GuRf
7gi6UZ7ObOXUjClQTNiRsBeA+6DWI3HQn8BH+3GY33xIfsmqk3YWKpfOaxVb6YecxUAw3TpkMJOZ
D0C0phqHnoI/1S/C2tHOC9rjKCmy0fEiozFgsHrJNtQSj1mFhASxePIftXpuDUiC7GIEe9uwU69K
MtaXMYf9/ma1df/SwTIgFGMj+CdLMtcnxafDdyqfvWfTrGztQ7+X/1IoSTiRUUdrxecxMRqyQfDW
v7TGc4JtOikc8fgt/+D9NDKzvIVhQJ0oPMIu9yUTM4I4gzYucRi9ku1vV4NM09mbVz//THlTbDJ5
g40pWbgwkDBTqSz1/F/mn//wF8KE2Rey18och+HK9gc+1fumDcEPSNVhzavmt7rZU8lE95/mbj1W
rsfJp9plD9jFRB8S0T/srNP6WXhInw8AIQshpdRobOuMcYtj9AfilTHW24JtXE+eo1ziY/B22XPn
uFCBtzqK8Yx+FXfWNfVBT+APhEImcHGRkX4XlfwJSDLR6v2lOIs9i67o6ZvRBTv9wBj+a0k5Zr7g
EjMup1/lIQdWU9/ez4raiNiDUsbnc533cY5/IANd7tfr5M3AR9waRVznXOG/R2P+YSyM66ib5Aot
Q7qJ1E1ydVqG3PR3BbFo/QukT/6gIEJcDo5UYph5BjsdYTyr8A2AIvDdidBDegGficONTrtM++jN
ty9slhJIjLt8o22lIC+iFKW3h0fj+KqIEma9lxr2VJe9V4S/0dvETN6T/EXYEctkuBvqDb+iAhvL
9kwVbAZ0UrGr64XtyrSXrUiOa53uRrv5NhL/KGxbca0CyzKX159VbaZ2Q4HL9OOzs/DG5mF6uSw0
uZRCo8ar+cH/J8ouz1ktYFaOkyo7NDt6r3Afeu2hwlhS6EcTCKkstRdNkrQ/zg40y45/SkDYnGWq
5yu3EtmQk9ITRRpJY/3twkIcotLlRKVjKgEiZSzAqmt3z6JMav80tL6LrntEvrU0+F5DaaTKhFQo
9XSfQawg9tcP4Q+gLOIN0vlRFQUoipKt0UJ61VkyDaPzcLFtPb0fBPRme7Bz83wXgXPCfucmTk8N
duXe5b+fEdv/lLC8dZcMNmoeA+1F4fxW1syGTgkVZkmUhIN9QSLni9i5E/HyG7QLAYmSeSS6JA2q
JlNLrUtyjCg+/+x6meYfWIwlmnJzakp9Qls0ywRTiDJFdIf2Rs9N+gravK3gHw59L//ZEplO2Dkp
OlU1mIZvzG1rX/MxxkYcnsQ+t6RhXpx+Y60HZNOxpuzGKkrhDPvukpaHQSfOQAjc42Vm8Xm5Dq+e
lkzEq/s6rY2YcOc606j0kt3Z7cPltz5WjlsrgzoJ7Ln9cikwzUbflvVS7tcYp78QrU8S7mlJNwkb
4N8SwhPC5uBLNRG2MQT8eZgjrSsaEbmnCFF+mtNTMkEvB/rwxpHffb9NL8gA1DZvo9tvIvYwXXCn
5oCxLLos6V1lOdvRzCkcub4MnPIXcQHBMVpfrQjzzVAVaC3KRJug1PvhqyVzJsQPzQuLOKinaJ+f
pSJWSjohpQ/333+FBqBYWs699rMxcVY0qIh22EVI6P/ukg4hlTHzNbm6flsEaGsqQnyJLhEwkx/+
+jSOCJjczjeHkIsTy1yAcW1hRHLDHDUlYGvXkC2wxzGP8oYvOQqekhWfHjpnhpPtOgHwuVZmPZ48
7IJpUF1y4GN9B5/BIaVfET+uBP43hSeQcmP06W3uY4RqDfVchkjZvT6TnQSfLrr+LPTeAHJgK+mN
QCo70O/UMJ/agIYqjlLWIEqej6cAfTdaW0RxgwsCxRqbqWuvcABzUyEO4eUybxeEEdwS+nQXuC8O
1bWBJAafpU9FozXKwYnRKbi8W+hG8dUeTbJnFTmFfcTwzQvFzopY2ng7dXrZSZWNTyUrjfP1g/wm
azNB0K6CzPJL54s0Ukdd/unqUwnscCnwrljDil0t5uxtRhVdhMifrhUfj0usj6rrxyU0Yg9cYbLL
qhVoIDZRG/WNk9NCYgqqYpiXhWpbmywhtVlCqI/gQVwyQkikR5gXHKKf4g9scKBHEH1d4AIQSKmS
4ZInOi3ZEbG5jIgB6SKn1wbBsULLUfnkpuhoBmnnr6+Pjsi/yB+aquOiMsvAmoYA/3+plvAhcL/q
KteiKRPI1Xy339vuEzVamfwsIyb9J52cZ2d/aUo+iZpPSEXHu6+djyLPM90T8ygtNh8uVrpjozgr
mUwfX23CGJ7KQTfdAAZXDyqiXL+GCPjGZBPQQsnf5OZtue+e9eCjPHt1UhoKUIcU5KUOOGS5zQlv
BPUp5dCZNXVmnB+QKSVJQ1QmCn5Uph6GemE1vc3euCRrHtbtSLECf6sx44QmDKajAQnsapQc9jT0
0sOMQ04llizefY70N4PuXeDwjig411SWeox8de4gE12kuBPd4sQAOUpOnlRQQAgMWOQLqtD61l4u
prrTb7PMMCEJ8ZLem7R6qeYAeHvF64hBW+FsXefyNcnZAyBkyI1enlKXh6hicAYxb6cHKKatQr7/
pUzWK7k/8bY4952gSm7SplIQryxLWfmZaJVf4ghbHYcpbVVDx1LVen/UxrersNC3+aryWJqyLfzj
izispmNIEaK2+evnaw0iXSS1ibk8sfGUXSHGyLxbRhpqsjbE60Zg9hHU+ABAd0oLNbVs9N3cuqtH
IXd7qT2Gqz/wtuQk3dY9iQ+lD02dro7Fo9fKflRGBMKWM+RGtWMGOgzJd25if8urttVfTwtMzkpM
GKaDgHNzAmzHlC9nYJfJgAbSzWapK06O9gl+aE2vxhOsoyWqK67hgPLp1pZpT1QDT5wUbzx4vI3q
CmN1NEaE/TBJ449KYXMikgJdfLe/eMZkjyEOG86VZmdCbq69HZaMtqNUgdCtOglrbhdo73Viga12
0c5vQ/eeDpT5UD0WCmcBK3kbX6kK2NHjEKLED4Y3TrdujHGnfV4M5pI+Md5D7T4Xf4n2zvhlu1Z9
C3w3LEUjrcHRzzqmvPBu6IfwKOpn0os/gKGi9+1D3/PJYIlryNhSh/u2oaiJ+wgNtshthTPxQr/b
pvF4/xMuGs3jOJcTXiYR7b4+l21rs5DBNj8EyWqhikUDg/bz2v3RFLJHJd+p6Ws2QDjlPgH/uWOr
K7eir1h+GIJuTPhwHfTH+aERv5WbwrjJ4+mxsJDVuCBZmRnnMvb7uolsgHNmX/UIJv9gxPvBpg+U
R25Iaukmkek0+r7VMXvYNzzWlu0rpYRgGZP1EYGgs2BNkZ8AvFOQP9pAd0pXqZJbmCSFAqASV41H
xdijOxzcuQgIpkSbZoZodHkPgJbx1pR/BrjC+YjCp/ps8iiMRuNOqzGRZYQKsM1Nm6oH7GhcsnCO
0ElwHeqcCSevUiTCkLE9KMcNsMNkaRK/6H7QVHeWTh3CyB2WuepqBYIP8PuS1ISxJdfZ4uVJYluu
CfZOJp2sy1lintr7Vvr9BbJAr+eGWe9BxykfG71LF5n8QuLKYF0NKwMPxOu72GeKe46+xYrtDk7U
pgvDas3jUW4BoPpwqUEKjpQ5NGe83duB/32JNB6jV2KUfpPt85gi0yd3wL7Vu/663lfuIEbabj/O
8xi6vGJaIVGDN+4eBg5SML63bIZJZdWMAmSLT9wMLvYAU04y9nYiVmbHBqJrt4FaFz5hqaMBRxY5
cbBz6ViidKQiYYrSpzMo/EcCypIZgwAk53pHgQYun3+noFxD26csRd9u4LL/PtNuKEJgoLHaUDbp
2TeOIQ8iA/o1St6+yhTjwS6VdX/SNsGLuzJy0J2GhmmwdqRv180TI7p7o9P+d4F9ccMUmYxV2vFn
TnQ9cVOC+wyhSUn0DLZGdPRI8X3uxfumrPdww4HMiF2+vgj2z7MT08EZ1L9SfEnuUvvMCpHNeDUj
7bZWd7P5jTvScYQaNoGrPZk7U5jTIr0dRXeaFMlVe12TFEKZMut54ihkCwwHYO1vOgSY9ZJU2lSi
TAqOZUPcbJeXjJfGuJ3A752ddFZ9ZmYn7WOwKTfLalFDYQXr12eVPTaQ3ID/l3/ADVzqCM1dforZ
73J+VnLRnpEJB6gAiqxsmuj3MNL9Im2FwpEjrt8nYlIpXeUeA41o9IFF5RDrxD0wWSccAU9W2ddL
BPyzM5b4D2NY4XV/gznpb37dlDu5gzQFvAEXk7hXR2nZ2lfUEnuuEWIr33gPbl1BOAeq1+aeK64Y
VKDzQZolAfGifbwUXjcdDJO8r9WTFcpcUkcmrqf8Z6Pmv/phYXTMlhN0BzwbFrlZgipNvo5Qg8wl
rs1tqGRqDxQtnFqKWkkJkltrCkYzrZJaXgdC3HnO1kYjY+v7wkClsWqmRW/FyVUauu6QtgzDSJC8
VX5cVmwgM7myJlGuhlEGZogCn3ZJX4u9gahg7cGYbq0jTeG1xQ7UrS2JFhnuDtJvJ1fcquUMnXE1
jZirdq35NQQlo2isyhiCJXSsQjduCj/cE7ei079sKGgo+08XL3ZRhM5tI23XVeTskQzWRhEmC20u
zv1DAXq5Y+SUmAjzDsdJtL9QDrDDKiMv14hI3cUMKiiYJ3S8r+gYeCQAfr3f7YumLR35wKiol6kd
dERd3V48mj6PXvOK+IApgc7mH/dL8mEg1P3QhFDaLCUSvpzgqnfAf/1kCaVFrU2Qq12DN2fU/Er6
bDlyGvjDdmRl2mskBMWykC34ZBoWkEsXFKazOCaAFCYZkf8pS9OnD1YWpZUMnlPB0Hf0OjV402Ng
2aZUBe+ZHrUJMZETEpfp5wdxJJVzgECzXjiXB4Edm7HbF/pjWHIzyenr0JplAuYX7ZTnk9L+fsoa
puE0g99Av9lKLn2jfasScJISOJnd1LUwsfCT0oqVy51AeT3iq8sodbxIMzhWy4od+51knXohj60K
KjH84WP3qU03lBIhKxnocPUg4lPCr4KxX620DCsit+bsHgbxjzFkkM9hQuBX8J7bVxWg3esJD30u
SW8ADN5iQ/8J+WbvwqOmEmXHAYD1JewZz8M0v21IFzJOM93kZcejJL0EiJsa0z907A8h02k5SOA+
SUN1SRVPCPZ9F0D6gQYtFWzAd5yLDeMTZDC9zK0zetmhKj4HCk62rc8aFk+6g8qREXh/CKo1cgSK
1kShkVraPLxGkvMg1lpvkbKeZkdXZTK9c+S8dSl37X8bVm/sX2cpHTQPLVLqDA3HTLXJWMHxRw0u
mKnbJX0biLB3yfGG99YG8y3j774AlIMA1/ZC/SlWHGkfl+7mc2/b3bzE162nlDp/g546W1enrkVK
G4JyML0U3wPTsJv/+Yg7oPoZ9UjKBrX8DsKSwfjXF76AbOxYb+yRjeps5qXcx6whrZPF4j+6M9CQ
B4+so525Z2QVxPNLoJqdcgeG4uPwAj2KiFRvDNlbOJmjEUDM8ncYznX2pbtuP9L0DUTEwnl/kjT9
l3K0kAIZ2mrUZ9h7LJ5bszPdRYkFdbcxTSEFfxxSsXZx7sYnieoOYnwl29Rf5ivUe6Wh75WAbj4Y
TDiHygQOPCQRouDnIKVo7BlaBsa7ycqxe06XvvYznUA7pmAFlVINeKrqSU1rRnaLlRo9/jCvenRf
buH7KM0wS0oCtwX+U4RX1wnFDHGUEZXkeKkxn918NU+1w5NOQkmvGjEhcVV/2FXPVE8SsLiII0zN
sB1YJ038zvJ/r/9gYaCb6xtrieM6JrnFniKItVDWoKlKHRay46bCAg/FFs+37eAMIlwNy3DMZEzG
iUxe2roAZa921AJRO1OLM8vR0knPKHVfVIgwLuveaadh60F6i4E4/XElLvgO0ci1lmuvBCJI3q0+
BNbRA84BHIV8BhtBkrEGM/Ir2xqRJhLLf76CpSlDz32xn68TCovlIcdpmL94LADE6R/W7Eo/B1Gs
kYLPX87lj27iOXsJDsRfg0/FaKPvqxeJx/40a+/Mn7dpStANeFkPdgvbWpF/FIuZkX1ZKOy6IfhM
Fpg6Vb0BRb2tQpUsQdhuWihBTlLFpdHqq7zLl94kmO9WtTc5iLZk5K/NIXo+tjfUj37/ANZqxp2a
4ts0uI503gilkR0vkS2+E45PUjb1ywyrksFeDP4JR7RhgmzDEFIijjhK58GQOzzLFfyovpFs75FB
QSVakvuqmi5jvNK582Nrvj4WJE3idXS90lQ8FfO20VkLt4c7FYVWGUpn28QXo4jffcPp2sTje0eZ
3zlZJziurPIMig9WnB0wusrGZNpmLM4fJxxUFFjYYo5WA2gbHE/MkCXwoI8zihWwsA5V2P46Pq2j
k7dBeeegMcoBcIOFfl8EurlQPm6c/Mh6fdg7Q9LGGaJKsZjOinMNP+H2Ip/9+8AOAgNYbAhXL0xi
II01eXqjAE0BSv6iCFTNyW8H6JZdD+7i3Ch/7DMThO/jpWNS3eaRzrpAmNrJTsZb+e+FNg3Z68pH
pFFfbHcttnDlfvxAf7NZTsy0Hja/9qa8ORyh8KOW99DZMfFgudTaHFKXLxtqDPEFrksyAUieiR2+
FJUw5s9XR7PoI49GFBq3XQFF2luft7tPaSXcNmJ0+MJw1w54ISQVTmQ+i1YFZD9FT9ufVn4T3GQu
lEhWBHUFPdLTaXhN1d9XKOT0SCk5R7x0SMdA6OkFeuvb5FqTq7s/YlRxYlUvramNXsOrJQZncc9V
j8Gc/v9KZMa9mqSR3slM1V/iG5dEg/784p/4BTIuI+Dfi5UWRXbZadrbjj2LFksIXHejHQH5PPzY
jqaQbM3ri+PFCTGzv5F3Eo1CIbgoA6VRsr0MrNKGBi9DJjvbDNdYUpAAPYbroZEJOTiYxEbdcIL9
fTFSHaVuRpBrTRvgACnADUBbZR25KJ9cWRDkUxAtaOqr272rBDUSrAq78o3q7Sa6EVYVDB6nqAMe
ace0bAwtsGzUzurho8b+7sgFvfqFct+pekYYWDcka+fSApIefY/Guv/YccWrexko+qrj0NP920iw
Vg6s2Goip+g5RRvX6EJ8Wc26eJtIQbvwl0RPHww8lxEjby78QHRSKtF8wnTXLv/qjDTZ42L5guCi
MmiLjjapK6chpb43ozmldCxSBug3SKUCi1deVy02sc2h9Q35Lwi5fGwp2G3dwYcDnQOTNKR35OM6
PP35dIhKFcQgtxZVvVFeKiBxCiinaIhO3H6O+fLuGBwVbcQ6cPAHH0KQ05cdhQD1BMMxh4QG11JV
hrdJOoSGdLOsotcKQJObg9NUxdDlBMwewTXZX9MydY0I527RYakJewyBbOvgeeG+I8OQ1kgstTUd
wyijvwu6SN37l4Dei+B+4kKZW1FZWd6MAma13wKjGDQ+Dzw3ikaFGkB7JZpDzrcfTUr3e374VYqz
3qtwZYvUS0PeskCiwsBW1T2QQa/+N5BsHwK+bgyFW7DM6UF3M80Qwer/jtZCgGQYlKw9QelvFBjT
/3EDAuxOjoEzxPFd04oCBX5OrKd+4R9mNbqRBdOR6+P6t7V/+YhDHq1A3wy7cj0nTi2RruEkNsC/
c7jRmiojHRG22GQJc6/sr/oHEpH0S7S/UI1NppbV7RuNG7lkHp9paJ5o1tqp9eGq8xDGtjWPoRYM
SXXCMrM1BSieWyygT4kbXXBrHDgQeA/gdSGKSdXHnZ8IaACfx2lcf+dN1iObYVeho1mV3EovQ1Ra
+WfQbk1Knxf/40RpL9Qf9CsBZ/FTXVSwncbRjaLvnHd7ytoCpVKqISsE4rNI0A3Zoy4OotV++WNa
XpbLQQqT22prqyNB6bjMnwKL9I0L853fJ5Klwt9L0SEEPfyL/yMgR+8NzwkS1ZXWd+A98YxIP67b
NcyFV1dGrDJELgOfaoRCQgOONqeH+HW+XNJNItOtTq/1swmkkNS1zknZPukBzdwUcLIkf8NTex+p
uuV5K5MmZrcpaJBiwNtTx8feNEtoTLjqtbr3Zissa6fJTaPOmKGiHxx9sJ1/s0PDoqv/qnlPRK17
JL9/QUvDrIecJ79fHjgbEVCh5Mk7xIZgjW+WVbyeX1ei2V5AsLFujhKIBxSjPyXUtJfj4GB98N9Y
GvdMJlFY/2+NQi00G7dGgM8EGYvhXRBVXSe/j/28o+Tf8hj/Q1rvS8lIIxAW7rer1yhxWghGZhpY
RYxnnz3oNhXPbqs2mAJAz8SNAETa1e0sXrFzUwk0aeEScmG47F1b1LOTWyRWL/dBbrB8QFkblXBL
1QAuZ6zx3ayStcYbpzov3RsF17aE72UPfMikUtWVI23l2UJlza1d7tC+pVd1V8TLxpm7fSxhIoQ6
cisrPQ6wz4rtNZqODLU1l7n87MItvkpYqYre5pzFNgGu7SeAFKifVWg4+1I+WgOFBhdrwo+ZQ45f
Xd36yfJybcYFcJo7ydNiQMU49KXqlFKudcIn8BHj0DvmFvgD/bUi4lE6meDwJCg5/fi1KMQStgtJ
Fy85IUz/YZEseNI193XQtYuWippV/Qbb7FsX3oRxaB06SL06/v7ZMogi+M+CHKv76vVg9Dw1p3wU
NSFGIC4jvPHE0uCI9V2lGZZVyKHXSmZD5pigvatQn3wrkqc67Ud4jS7HnqILzgR1jWupjY22I4wW
xxUF0le122oZ6YV+8R05hnNgsxgZ7Z8mF1B1afcEyxtQdI58uI6F3xuaaagbbS3dApUjMATbM6I2
Yrh0KL9LLk0N6MMqMBAtbLgNhNkQoPUGoF6wK5SlgS6M6N/91iWQ3C9tAf3jyK2ZuUPQtpHD8U+D
8LeGaX1mDQvs8rZgxReFkXzHOqMsoVqY2uQ8opk1g73ttbGy+bHNoxdxuJ1hUUqonU0GzQgqStoK
lhHtFpnEBsFa2nVDJkep/JaJsae/Hq+L0YhlpYfr7vU3CSTC46WnIReUm4Mg/LMJjV+ZCUTOvQCi
Kd71NPITTGYxrEqJIZB6GcmczMBKMt0UhfK3reqrFW3X3b2qfRhZT48NbSK5Uf7wW4jow7tnCvN+
j/cnNCoCT6AFU/K3aB4RnEqPpHfWO/M8ZzsdpMIYwviVHSE71+5V1fVDikcqI78cDuuEAudLwrP3
Qn8VJV38nnoZCR6gUNn66YiTYU0wWfmYiRGby7qj+lf5wMX8Cz9ALSCe7OmsKOkmeu38T8x9zC9k
zliQEMr0OtXEpqWlbDN6zB/g7aVK5nw0k+5hubIsWFLt1EpkC1MP1lGrIo2M3je9FfdWUjKjc79Y
DXdd1KdHk729InoDfa05ED8hdDntH47i0rfvOrk8S39yBwaeb16Q4zZQyEeXjxnRyMaTenFbRwfn
euV3rJO0Gf8S7BfPA6H0UYBS7ZibMn6eazLUvdwgLyx+ls5A3OslFDzU7p+3pMtg8h6BRznn7G0V
k2bf6iaCFBQgCttGqd+tG7A8vYYbZjZji//PxtA6aGWY/5fbKrSlrbfGJXrq2D50Us3QW3bgXGO2
edOgsvRtPP4SbhPOBbcKYG1mwtN8md9BFDz7Dzbtej5PRa1OHjhXC4YeASzhm4Rw1YU1o5oMc/0G
PCDua49ik6nSFgP3/AHGW33stkd8wbkXydqUrfQ/McH9gkNpr8Co/VUzolnU1oKDAGSTZkqECKX8
PtLqFN3qx2oh/yTlu5z7Kpj4xjsXE1A9YoT5m960ByCoUIJP0+gpSiM4ozx+wwjhhCtcBvIqK2cK
GC29oi3HYSRDSmfRdsle9E4UUvsu0RL5F+5wtTF6kO8aFGnTKHrSkbx0mo7ci6rlmsfukbMzhuQ8
/fDbboC2nceceWXQPa0sxo1O5keZrEQsLCEEWzZlZo2NwQAvlw8CXqpcXEIMCgfZrsqsRNaX5OhT
8txJrG8FJDP1Zlu7KVRmT0XTznZwMVrKx9psN6CloyhHKGbGPgXe5ynnS3okzp1LRX7RPFrTU5tV
XVxWvPrY+Ckyv82cx88OaNrlJsm0c4TmpVt269aj0zGmvswQi465Jour9BU2iAN4JW3wELFWH5wo
GZ3TuQ+peCWEZ8c70IB488ITFXuww0YxEmY+MrZAjY5o0D7RwRSHdSLzRHIO8im2h+cVxfMKDL7n
0R0QkAdW6BtGEauukwnYzulQOkryvUzXdrmb0cdpCf/PafjgutqVXUFfIip+2PMyUIYvsBqxDFmo
n06on+4R4iWvt2ewL6VXC8Offbe09M1VGiwuP1MSTrseHuT2gfRqFzIOg+j5xZfFhKan+srkTzEG
tnDeMcVpyrgIFJUmpFzi8bTj2oa93YiBDvvcAVree1j6Ub2nyvEHH39xDRYczmsaJ3yaCNywhaX4
Nv/jalJTsgkJhqSdfq5CkT6CDXcGloADjqMDdc3v2rn5iBTHdDuiLMMAQzgmpxMTKLIFSzU1DekF
qtT+v2Dx6JiAx+5JgO3R2khbqT8/bps3T8pcux4BX50/C5f6B5ntG34+b+Ddd78y5Fo7D3Q8yZt4
ouOHk4EcITWMKy/jErMBDDE31TB/gkn8L10053iLTUpxwFB+AZCG8QKupBqfjv0pAuwyukpBbgfB
VeiWjld23C81Vt/kE/+EkL38HmniH5mwUw5J83gO6mK5MVFSKxnpyaoauKm3K50bwxYs77l9akTO
QbV3qOpr3fPRDpHcfxZ1cVy6//R+5WC01cwmQFhtHujb27JkZhNM9XjC5/sNaNP1V1y9TZb8QBTI
fNPduS8CMf9nzIWiKZq5/EO4KDNURdUa0ByhWAwIkQ0u1D40EgMpJ2gK2EpW4otrBRQmwb/Dft5Y
MsUI8CLDE1WuiAm5+Qin8E1dou+3EpktmmMHqIxdbnaFh52C+EUfTCVrlUvVWwUsy93e1ttwgKVi
dBGZbdaIkDalF34qNzbKKSLqXaLFGY/grs6IE2T8islgdMdiJuNQvz3rXRAZRWbxPuSsYKw5KMVZ
KvKdvrd6JAQ/Pf+DUphDjK86eRIy4W7zHhZGt9La24zJY76GOD7KBPrQGCH3YaOhsPq62NQN73On
qZt+rZ6H9PlQNubBGTn1ckowsM0Q6/NbQdl89VQOS4qFPJcYliQJqe5TKRSZQxhafNjWxFdoMB7K
He8T1AnuBVRV4PwpMZLgrzA718f+gndOJEabxEkceL/56oK9aD9er6BSNTuaLHpqQ6xJfrdh5szQ
Nr571BEmUjxdrBTPkO0cgfHBZ/X7bLt2rHnoRCJkeIWdgXW1LGtekL1H7GV8ipIpgppNqdRHImkm
DwW55+8jOAfh/pIBLOQlKjIpIg5bcDDKYXdaDQaQomdhxTwuDVoy0S0OAJmsw4M9pDZ3VxNNHXGL
BSoJSusKdKp3si7O82DRAyNSBEPKM25FANr859c57cDCCFzuR/5oKq6PfzzplN4YEx+5Gu1Y6Gtw
IeSHlcX45c/Nu0rtPhqz9rYyzdQsk2+f948Cgk4RznYelUSLJrSv/sj+LbLRX8cvPAXu93GgOUIc
+/0/CTu+faZNjtXX44nAxfAqgDIK2bqqNsenP8UBFW3cKxZplC5KR4FInUGtTtQxUljqq+gVttws
WQR31TkyEoqve90SyA7O29Gg67kS4RVHjbquOoWgtWbXYIHlfxuOUMnbQv+8woQ1iHunQj8vV8Ot
J99cLGT2pW4Fwg7dExawkS7+rtby3vFr9YCdDFtQMLMivlCz/legPJt0PF2pWc2BYI2RtVgmbMTI
OoO++bWksW/WUyZhw0s++ztZIBMrwL+3OtOOTbS/LzI25K34MPFdKBYOxHpdGYycjpLaFJ7oobu6
K65e+yYPjVbxmlNn3gYUs0uNyWbM5N8pvw+1O8v8tJsvReNlLDO9kAAUHah5v76LWTUGFdmBGXWk
5cPZrijgwEzDBGQY/No/zJDwuPGZHzcp0723C5tPL+1+FdFWPLj9h6QVglqSfADHvkyKskNlRgdI
IXWn4ZHoptK9ieC5H9rAa6/wCz4Mutxf5ZY3FX/82KTqJ+PzmDk/Ayz/vn6hc1Zt/hlgJCwyQJBt
maCFtqSBsBo1WyF9EM8fFQTXXXlwRBQdFpu0xMt4RtxMeLOfUUqd0ELSEfvsUAblg//Z9vdgGhwx
/8I5OYM/B3EGCrK4hUzAr0idXeTWh900MFA2RtQI2ijoy2UNCvGS4ozfoIiUyy0MmyDuGJFpHGEW
s0pej6mXttAxrqnj563Vl+aCFtQoDB/oc9DWoM7wAEQguar4x+njd20gKxNic2A75dnYzdo9MJP7
LqhGDdRf+y7Hlqu09W06SvjtGMM+mY4+A5eJ75UE4QTPWTPp3vAUwhLYYgYZqimejh9k4F8CbDuC
uSIqObt7h/vgf2+mspsmsGAM9cYwRvtVwACWo/UnDSNh/OuDpbtI1NP+04fdnCoj3+OA5Hj/OvOn
bv/HC7lJ97Ae+4DCYH9g+L9piEUv/1q6OC3InqP/8sykdNkZ6+ZHaRW8Jfr7ViOHDi5W5kUdPvdN
PKk4gIqnJdQnQ7nVulgkx9LBX+jouLpELx79+I92xKa7a/5e4pivGo6ka3MeCe6FXgQ4SXX6KP7y
gHJQJSX/coFbxZz9ncYt8mZA4Jqiw3hVaUAByJRutG2cbWJd9mlyTBcuj5WPHj0ccaGBSeryytGo
tRXW0hiEG66Nk+aqj/whMRMDdFq2FzJ0VFTCsAkxLB+gSyyP6Bi0C/u7lyQEPncaapz56g7J7mkS
Jc4PlLzVqZJXMQSZ8w+zUUCxWNDX0s/V7R8/YjXohCk9IbhA1etwYZeAluRBVNkUu8EhnGXQnReb
mL2b4bbwDiSYZDbQ/GUnsONQ4cnJ9NckVy1JAMjzl/KWWSktRHaOgY/DFUnSYw6UjG0OD+OIgHwX
PY2mgrxkWutPhGc8GFKiC6PFFCcRhBSdkavUxzX/q49Fxein/AVCaEzyDz3F7llktI8PSPhzbZuy
FcqI9bndAZOQB+9i+oAF9864dSXfBuuDjm60tUHWeNfcq+VDuJ3zwPDuvCz741KWFz0d0lXTulww
c/MWL58Vm6YSyx0wYwFAD7VLLNQErmBaBbDiTNRqcmvdpgtjRTO8AAWsTonq2Iu9+aWPbN9Pj76r
OjuF7uX5yVGnxeeMP21L8dWnJVMsO/AEBkB3c9ej3XYdzsLrmGLLvMtbGYCgtii3NKgGeJUD9a8S
kMRIDJx0gxcMF9+5vr3W2k7KTCoCEJKnuE1/MPBwXmcWNwwh4CIKGJjNWHJVHAJQaPf0TUCnOpz0
5gFZZxB0G2crmZokTCJ1Z0wFueC0vPe+kLCl885tpV0ylsdK9m40CAWukDHqCoiju9mcajiiO4vX
0SvvadI+KK09WQzmFP5WDGDZDT+db2ju0YSoxWFptMb8C3FlvPjkq2l8BH2GAn/2ers+J3+H971e
Ikr9+6QiQkUxGwDAHV73U2XWAYq/M3VD1xqrJfTwlb6XboQ+QelxCDYGD3/bRY4KsahyhR/yyrDG
ldP32B41y9aKkHT3wJk5/4Wi06gJpffLvT/03tmgzt5F/fJK8Q+KhHRMnVm8FQeqp1MUw0NWEoR6
3p38DaScOVfSCteES0dFZMVcFTj7OMk9/g1Y8j1hjIjS/Dw70eZo0Q1NdFhjhUl+viXtaswtgGQX
k6mVxxAI2lkysgmmDCqtLJVqXpT13d/y1T4Fbf2G8v2FygflcPiT0d2cxTVO6FgjpFV06Qe5ZTYJ
j5scw+NiH5wdIjMbaDtE1OHTQxkiNZZQ9Rme9btrH2BVtF+/Jlmv8MIw6X8ZtlnCNfUQCMqt3+Vl
MfmgSZKWcl8eIyP1m1zd4Z4tJ8v4ucrhIiK4+FUVKdLcHJF9AL+3gyluHwamdZisjVDFk7BG/Cu9
ReZ1s8ae8rmM89N8YI//Iau1qeO8GtMFJk4yXGLbon/g0H7N3y3gNIq8Gr3s0Y2JTtu86coTiDXn
aBREUKFDSsjeJzQlhZOfWlG1CLdEC8KyB11ctrUsQuZaxJEtZnqfYIj2ufK8dih5cXT2qW9Z8xDk
0Z9UTk9RByOQtmOGxjIVB2hFFajkO3c7lSjVGYo7UwgjbcSTKlb/W5zJcMpHAoIoGwjqYq5qgkrj
X355tvBaCCpZ/OGOxS3H8kcHl6bqD9x9Ov7Ud2TgMx9m6VWK3lKSi8hhy6wsSBWBXxVBdOghC79Y
tky87DcyXN30tE1OfwNOAfT1/7xGS6USeFRki3BVjZFPPGQzx5FDRMwdT+bZ68Iv39+F1WnX7aCa
gLelw1yBI13EuUhT2dBtV4PijViir7HatjjMhsLI427tgeObyvlMjuH7DhsSdAvCuRZgSey9Gb7a
KU7tj9ls3gNWBi8cjuqms7Hmu8Cq50nHHihQCU61Jq7GtSBWR8TN6Zuz98eJ4qwHIpq5WKBDm+hO
fRvRiR6CO/O03o1B8GeAnc1cApPJ2S65YbEDtD9dJZF4GLWL2YdmfOy+q2GsMYFgIz2+BJK4i78e
+vYEUaBvz9iGNKhs+Vgp7JihZfC7heBA663tgUZVTtJPehQTUrYUObpEu3jKYhJfDX88HqSm09Q8
jHjbGsocX0B9JyWrQFuizFZfGuBGcy+a8nn/QpqS7d1FUCJ+ZMMSgrTIsTZasr8eou1qDz2XlUjt
57O2U5tQr1tzNXWUS92p8rzKTJeM6NTz/TCuwurZuijpkYgsXjrjwewUtHen25UgO6egf23/cgtP
hPP8iv30iqpagSkCrpuATynksMXbCLGGQDVb73scBGT5DBXTujqS9W1vIKwoqIomP4mns7FGd6ga
yQ7dwd94pHzhvbgEBrOpNgaSXoYUTul1DrbV6DcicDlCfwQy1sAbZfUGAdN6S4O+/nzk2cor5BMO
NaEjrSvk/P23jX9x/R5Sjpf5dprKGCoCgD0GoL0O4WJAslxkkNFHxRD5bqhrl0lfDZzJposLqCgI
/XdsthxkXw3IECEpqwbSuTUuhJUyKn22JbgAVi8pPo40hLjnTPOAP4LBk5dAxw5vO60n4X9dJCgq
+xpcDn7Bl275Yh71tw/KV0zmnk3hcG/HINTb1z+8yJOXiX9YCEdAuuEaBYIpjkZDakLzoMlxCJEH
eTVx3PmT4b0E++b/9QkL3I0vIzWtTebyYaUjgDCAoQXMWZrn48RK1VcFiUIgBoo4aF/YmhnnLfrW
mU9SPp9Ly8YO7eElrqnKjZtpUE8BmQdZOByA3ParHg2kPDmcnVJwj8sPw27ALUyQBzm1vYe8LsFn
4ZkwXGsnRopvJc0bPANAZNQsWzOqGXdssQLll4Sl2+XpY3ezbqR4N/qHx9JhzDr3w5+6o1TjmMw8
IHH+SRWRZU0Gd2MOjMD98IRnmCi23SFeMmQgBf9PBSyN+YgzwNsSd6CV66d+RUADK0leFGXACmob
Vlm8GyZOdoMYjtoaD9CeoJg5CkG5sUUfQTaNpXB8vCAgueQnkgDd+bwbVMmcvTaGn4OWXF+mQ4f3
mSQ6JAJs1yqpafQc3lSd3106gSyEnO8sjP8428ja2eJUhzo5UxCaILXkODazVlUWYYMAQaAS8ONN
3NyST2RFLt33Fc8y5mWQ4T3qQJmhM2nalk3ljyOn2bRxrTD0bWexIvuXnfQ7X3mfzPfNw0m8NZXv
3EiB3Cqi2Uo+RragzaC4sREIem2Lt/Zg84z33UcBeGUPc1u+JvS/K486YiyxDJ9CPEvJt7+kx6Ul
zjhDs/gHroKPO/s1wy3V17WU2qeYnOQ64Kr7/zGv6/KyJ7XYbdiq+ll/N+7GOvrOYbRwTobYrlUH
FaEfkXiaK648DPuVrrj3N/nazYKQ3o5I7rPFHtrauNecLjlW7zrwNYJSqLmFzdEIWTpHQ0vGXL6M
Vd5iILjPF3QUF5q6OfbCwHS3s1KqH+L5PXMacIFCtuZwqcbmKTip6U0vqMQmGkAmQUEY0r9g/jCb
40Pd0wXoGLEP9QU0401lfxaqueZ9OLIOLl/4QzFtjFYcvOWoOouYn6PAy0K77ueZHcUV5IA+4hX+
W1CBlJgShr6PDWlwgwMA+TRZ+YEFpDlox+UkNTD6raQkhHMnVXBFQYjInbws1kMxqIbKNXqot2Xo
j5mU/PBPZ7+JTVxwwhQ/6dYb9IcMSWwj5xXT96U/K/ctnkeO0k2Qbuc4oIk66ee2lnGUJo2wctAr
iyM/zGo+v1mYrm0O83I7BW/Z1gjkA9kEvW2bGD3USkqPNRBnYVtlSIT8VH1klIgQAxylLeC3071H
i4SlGJ/iwaMh/hgo4IzAsdNmZFMc+uV/znrO61evaD61bY6JKdTw5Sgoc4+K5basb+lsiVzyn7wD
lD/72cdbsLECmhp217knZVkQIfXuXhwUJpdM2Y7xx2j1WUo4xaq0nNWxWnyfh51sGq7NF6tlXKes
iTlUAxlf+iUILc5AAuSSE5XYk/UGWjU7jAI+5vHRchQebH8Sd5h5BfCAz7ceRmWBz2yDgPpE+pPe
3brEDT8zwqK5vBM3lUhQ0wnRFLh6g2NCA/SooQkliqjxkEl8iQFQ65FiQ+cp29fnns/dydfS8ghr
jeL5UNVqnCs7ZsPknU77n4lfTpBuITUg5j2BBLTH9LetKW/VLxvF1Yw/ssOCXGF3ZnxRYgac3VbQ
JhOpD6HwcUBOlb9iSIt5/hwJ0UjGRoR9BZvlmU7C/JUAWd8sNPGL1E8mQDEC4IXcZkMT6oL9yHOg
cN45v43yjiTTLmOWbqkQXjbG1hXNJthniREF8CagAfDzpva5Hxx1gWpuQwZoP4jbI/vvuWN1fCbB
IRV45yWrF5+JBbutoNJpEhWj5dpR8yW2qDrosYPOe48VP+pbnx9PXbB7jj6LOScQAWPkaysNkcP3
vrgJB+SjdYr58N45/wc8IkaubYe4NI1N0xdM1mWjfjr70tTi+I6TkaXBOjVPEiIPJ7AlmZO3S2hx
lfvjRwmQC6/j5P28VyAy6EX+OyFILELSTfa8XK7yE/Zda0cfllF6q77GH6ahLZnuIehM21TXUAvw
+Wi/XdaMvmXcuNuUiK2u59nD2uwdvj/CjMhSsoRkP6oRnFUl3IYw5V4IXw13bMqvE/5x4our5b/h
CxqpNVuzMEFyws0D7cQ34CLmtEvJ74TOtZNviYyGaWFL/jY32jhCdWq6VfqrzCNnYLKFwoCrJgc3
+2/F2Bc2ObPYxMoKR3pm3HsxcZKvtWeSzriV7qB/fVaGaIuSiCb0Mtr9/wjEZX7odnMwMiJ6ZGcx
T/3DyQd2n8WIS05xb8Fw69jwPeuYyo3vW97M3BU6ZeMj+ieCD+dQvl9HYxE2w5CYycflWW0lGbXE
XN2VO2IrM8rdBj79Zm+4ENV2n0LoRA8FJjaa/QZLw69IA2DaO5LmwCN8prGpTdz79HrZG8rOI0X1
pZuEs69Pnb+rjVQq1P1yi5t2nX5GZhHiugI9brz6/YIdYllNAlP/J3/8tUMGgpmeJCmMAQg/DJuz
8bkldxN3dbDa6Opi1xyYh3mK3J2x92qN9G0GZJU2NdYHFeA0wViDc8V7xYu4kEvMw/V6ZbWqC6+f
oSuF3r9dSkZjXkUwBVay6hUYQmoQlTxRoUYKKQKLYkCQ5o8WpHxvoFU2V/eszEG1nqsG/cqWbVVq
S4NEN9n2x4C8NScli2c4mLJbq1TrQxuAJfGqP+1HHEPWAsxYLE1oy147UK+5o+0LzGgDbDoS7wgU
XM5fpI8Z5xl61dFmME26X+ko3NVFQlkLeoXj4msjM3tdGFDX+DUjyojjF1h90GdHz2SSvG4kU8RV
8UUdSzw9uj6syExvXkhh0TmOpqpzYCpGvbH0ySv1mZNvXIQ9i4GzwW+xjiUbkz8eBLzWzJ2uj8Sk
A6ZUVqZQICaMdeSaaS9i7lu85782ufzTsLWXQCM/4ID/6bp+cuih/nytE/1sLYnSn9hq6Puejx/G
QZu+iCNVgl2v+qQSrJSeatcVhmY3FTNENf3wG7L8ReIN/lqSRluBcuZiZ3pNvdqB46toCvx5qhwe
Kp7PawXUwMMf5XxI14ha5DoBtVHNgYSRm5gSVTmeiAVARVnMBAlOGUO2pgdS6oY/6QvwZ1fsS3tN
nCmdGXyy+HGfTGK8lbSGKG6kSOAs9myb9hxr+FLtxVabWWMfRIGrMKbcUFY6HiexgUl8EN/McYC4
uYsUgxVaH2ousCjcoBn01nWMyZ6XiL4TScFnZw+C9y3hdMQKP6yj7sRgpmjpDZ6LdM+90es0BI9t
EjmUz3CwxeDThF137UJTBNG2NEImTHpMFF68kO2OHzDNvqI4YSAfzHdc2vEW70UuNYpgv9vcsKXk
V5s59/dxXG0u6joaD2HGgmF+cuQKRD/EqfzLLSvDEF3NNPaN2oQGOYOacJSuQtAusveZliQl/IB1
6RVkUb/v+1ANUMygUcynChYBNZQwwgiu0ieOaT8JueMIiBXtZgbHKpYk1tjBJctvobsocYU6il+W
ylGanvUQWVTdma/E3UCWTDl5dt2LaPySoLmZ2gQoVvqncqurJ1Jc3viHum5a7vSb0Hrk5gdjM4AG
JbkuOrcAYQTdhoeLs6Jvxm0BARrX1Q5poTvQGKYhAiXb2C15ADRrvExcxEf3w1y3mmgEpAWW6DFC
+5jRfTR3n1/oJKuW4sQN8Yr9wpg0wKEoIxvsQQnqyHhIj/SSAlVskvkO/oxy02dix4tEI8mLxNdJ
fc3D+eSgF5ZXbaQu4D5PGev5REH4VDT1Ihu6fTVNjNhRYztZFcL47GG4/YrWBj6upNlJcflLV3he
Mp15GY8WSyoftAsTPKc4p5Fnpt1mZ/gSvGZB96oJ4F5qd8yVIYDQdmI6uQzAtwq1gxnWtqOiK472
9Xak06kp0ymfEU0E8T9qQBH7Wd+4tUcObNZH7i2+117Yibn68PgdTSosfu6n8+lAWEMgh2oijzJC
2Ru0SYWp9TOe3dQ+IU/wjXwv2W2e1wlZAHoT3Pwb5LB0xfnMPdmbvxbpWZdZeeIN7GnOOMlm3/hk
EsxpygHJwqp43gZe39AGi7Gd9TJ5T6wvVkxuOK49KqwhbcTe5nsoPvq3MCcSDMMGTxOD4xJv7P3k
jPDlofDNS81Khkc9iADxiCyPfxOXASfmR/Nv9jOOwo8WCx/8iBvx5U3W4aNTZcUhPBNh2z8YD3Si
AvNeBk33UAxwDixvlQfcmOxv6boOH7VkPp/xrFhPA/sL5QwtrafolNlmF46WhEf1oyyVNO2TFtKI
2J14qF5IqsiAGsYAXjyQRYwSKxo2S9oFGMHV6uuU6ZccEUNnC/3viY/XE4THM6FehkHhoHBAxpXF
poWEMIcGX/duzclf1TZ0zH4kPbFrGN8m6kVaLBr+rkY5XfQjFufTx3kR6QCYz9N3Kh8ZcytBX5qr
6wTLsns8qeXMHcN45flrsRfmzAjimPnTI/KV6oPw3ugLjA60Wn8K30Qrw6T+oOlehr7fFxgqJPOp
21+xjlMcVCtK/MvsbN3Y0wimSg0+cccW0rMxYS9UZiuQ/OnT6Qd28R3A5ZbmPPJB68DnyWsRwdwV
qKoYFLA8vpZzVeURT0Uo5TAqmFK5+138OPAVIbATFCorzOao2eODEh7pNMndcSrrvxwJCeeK0pUE
bshJJFEOhbItJjGjpJmJmzJuFDjyokGQK03TBUxdlqTjG11JsOtqAWus3EmzJJmZxf8FfP3rwZPk
DCFxYQ4cU38fXuFBcQvyLdxN7YpUxKgakK2lxt5jhv2eCqi3Loyy7tQbu0Tx+5BpGwFFCq7SYQ1z
wJwfADTx1cJotRaLZefZFLX6ENE5xRuG6vpsmsfZyLPSs816VdcLTOlvhRCM0YA/gYlk+R/Xkyxn
8WmWAli1IyepO9ZSgrQgJm1aD7GduXuvK+dXYy0PAzJtNCGaK8v5ycylAVz7PhGAbe1xxPh5MKzS
jToeV+/1KekELyU0nr/PP1HOxvchU+fRLJiX2I57klT//7WI6HIEIJ7jVLTUDA3NyMjQVIE/41+s
jdEmu9wvMevh3ceV/MzRKxA4NPg/4koHj3qzm84S01pqnRhmcELDdENCRPU6Dqs4fA/Esj59pZf/
yXbX2Gs9ovMSvp6TiWlrTYG064n/ni1aWQNguoCyQdnDN9w8CwyBb7Q4P1fGU2LHq9W/dyJ8wE1C
MACc7ibeAF8Mfsmrv7ZuMAYA67N9sRqnYz2m180BhIMAFBlKjosGCM8QVXpV56QPzoRw83S12Kzu
2lsijNAR9QU/Vpy3AgX6p2o6q42KA0r+7QFEcYmvCyyc75dPQwd0MB9hHyOnm4EY80jEtK3/KgoG
wWdLUWissWK69lwYd1HyJIWtZyV5QLQDuFg/PV1qka4ERh1TR4Ba3w2zI915whfLBktpSBwo2rVV
BiWziWmiIrSaNs7ow0yhr6v/cEsFcREgJ5yRjR0EObZnPu/3yE/tQzV7gDJiccZ1B0+fMqHjf8KI
B/u4H8AgdYP3qBP9pBODAwmP3ORLVSSPJZGyv0djn4Sw/mj+bfGKprc9/NgS/0kZkDVnBAe/rX4W
WVQIUAAO1p7R09E8KICXnVQb/C+0cKTBUT8aTpayAWSAs6fdXU79mdruCO16whha2MYofSfzZ8aT
TZ0hNdEKpVJsHYf12D1no7Ek8u1sTvxO9agsu+3grj7x5HxNZH5zv0Ig+m2AsxgDfNT3Uae8Stqn
okN8YuSB6SZ2jvZD24f8UnrsPGYcVEbY+Kggxgt5MUL83XSKVMZbyEP8wphqGr56hqTW9TwPCQYS
BvRfPgR+ReybGTskaqIgxhNWzPEK2qxbRU1bGTbJcP7oFd0Ev352MG81dsBjO5pccW+CBjcAyrSq
XjvButmJmKR64QmF5lt4NY2JLGn3HZzsbOa1nxl5YNYfq0PdbpcoUHyxEI7AZThVLVNOtqlxXPCy
ZXsk9BFLjhxcFdThcnafPEbHqYWv8IhdbojWZrBaqT2WwXZKuL6qUxlZ6vk7fbOavjjYQkqKTjS1
Jl25+hMCnrb4WQI3HxS1C9lRv08mG705T5/tKfVCR8g7as8UAds28ESLChlK5oeBqfelXgrqNzlh
inmx3hZCPzxj9Tt0W83ELMS16y3r8uxe8xNP8f9lkbsdNRXEnyaNlGaVlvCHA+FqdReTOl6vIEvw
n4KJLA+39zxPQrANBIBdl6ds58EXI3gAxLVu7JPGDpvragalUEE70qW4xqBDv6Hm3BiApyihZILJ
JD7rGRT+U4f/ME9qujXuf5rrjZGwhsroIMQCLXkyJXH20knWCtLbdNR5DDim5aizBxyYQGRHej0a
Gfysxg/qsnYSMbuSpi/PeVybFG2rrWRngkfKiSMpp3nz1wabkuGn1vyKaOKy0smeypqd19Kphnri
n60pOgI5p202orL4QvFgi1DngTXZ9CCp85TDDgBX/TAovfe+RdIJpymGNhpD0Fwl/0Dy64aZEk95
OyJBISIYJsFqQ04CZXv9qnXNL8A4mjZr7jC7HWiGKBqP9l8wmGqLjzZRQiduDZyfjKZccdoOXzva
euiB49tYOE+w8VOoL8mEdpXgprkXzD8Fl6T+zVnWcONqOh/oTlm+YJh0b2gdBWMjVi2Y2RfUPBOC
pXGlpUYROv2NLHcv/xO2KXKKU7B0WcO5d/NynyAkDaTraw/Ib/iRToL9e6SrdOhM4CBN2t+FIFEV
t5M2hB2UWjVmL0kFuw2VS/skU7bWNHz4VZomxj2jRZ0JI24enk2dqHZm04hqph7+bSe2wrKe1xlo
m6iPRY9Ja6LIcfdXyRg+0qd9FwT+KU5GvQ9w1Cnzu9vtXnl+RmdSoWYkAZWDeqW5BPUiFxH2aJm0
x1cOceM6uoeSP048tSPh1JvEz+FnOIRhNLGVm88wQ2ZtYwf6Bv2Q74Iytu9twwDM5W2rNc6zGxW1
X4R8KrG6o+9NNDAkNqOQ9nMguOkZ0W8O9A9YOpOYz4SjwBl6EgaycVlwV90l/CfNX0FEKRIfCOy0
VvZGqOszVOUaQ2xVzOGnjWeLaKs8Ki8fMdtq0MK4HOTtC/uDMCzoORUVTHWAQsUdZ9wfT6d1lVQf
+ZEBz1zFi7cJCTGMiF1SGhDDYScp2grbd4nYr8pV8wFfymPBhhMSpq75dqXkpN9aL6swZgldw8JQ
z26goptv2K9dpmx8YtBBgeH0wyc4zSfv3hm8RnoEEwqljT8ZN+okrGJQlrTS7X2HnOo+0J/EQ0ig
MoyMRqOFnK3L9JBWUhffrIgldTqEY30k1wWu14GrTtIMrNt+v/Yne//00I6Pzk+/7uU4a29t3JYx
uNww56GgvAcXcu/TOkAeq+/0XYjpMxJA+alkYqj/1AGOlbO3ASClsjNiOnPD7qgkjQWPqTigGNYM
U0yca97xQKcwP90nVCl1Ln/gxtmg5PBs7ylwK4znukK8k82p6RMmJDMw4crPSuuEwe/dpiGOfR/X
brvBziZ0mC37FTMA62PU3cyu8QhHSnt80ho54Yznf/UiNnGC1xlLFfqvi3qYjhhqBjIRaledqcRd
HbYIRboH+ShuAfUCR6uLcriua5y3pn1YpbRZtILxKb03iiHsAkHMJoGmQrtPUXtHfvO847Jph/UZ
qMxgKdZl1SZJnoqwre2d+nP5J6/v0la9xaFBvmCmJW+cBPceipg4Fi3TU9hG1iBhZno3Yj2zZyQh
t4b87b5pl80QNW1zwOVGtT0Pe29ZYgdj28DRvuV9hSgvppkoDI8WMt5NMzPwDyYRxs4I8qB2Yo8v
luuR4/rhgpAouW168Jtvn8dqj3F7fwgCpGPw3f4RopBoiK+5FxFGu27UNZM+Cx/5MQ6+/2MbKzrG
GcF0QqAkjGVKTNX7ZYrkWUidvG9T2tr9U7hmwdSAjfGjoX77STV2lPIEkXb3TnmRvC4XiuLDwPAo
Z6dVpZRLl222KQGb1tF7KXLq2JdciDtlFt7GA224boROywydiVsLUh858sAG40M72+891pMUoWwO
WzhBPWDxdTwIe7zhbKTFllfBNJNyl5T13jSrGyQMK/tue8xFzbvWfiyJMAWlcsotIUuocfLok/LC
6q6QzAF0sSa5y12SfucYwjGTbjYMNABEUNwhxq9TXSEOatGuQs3A7Tf03zZ3FS1xnM2/cxyVFO3b
lR1n/b602M22Goq7/ZyaE5DhaX4LepPrheex7gPUPJHRuZiYwnA6Rom8c/H/1DzJQWwLJheHHqPM
PtCa5Vj6w24/bcFoSEZ0Uh1CBZzf4l+1hV7SgCwuQV+S68R8IHTmvhliJIhCeJKtGLtTswzbugvq
fjJpNv9EhqWgyNf1jA5vvkxy3L0oO6KYJ0sc00/wt9MC6yX9Tw6sU/vlKfVxgNBYuBpWR1NiaUfz
1rwUtLSayFaiRv6M1fXEqIxSdzU3PHyVP/S19TIm5njHCwKevngnMhOOmx86MlGl1h3d8YlXciW4
pJmCsNvwml2frdnqD0j/q8T76nFf1FBilFAZnd75yACIACM4DYGDPB3/aDsC+8Om5vzEPYkSStVU
D9bkRUK46BI2fUZ+1fH86pG8k9MAqzSkKtFUa9aRo20LPY8E+VQjdHLmDnI9V623KYd/LgzvttTk
ij+3PO9IZU5XLjbFMDGd7J+r1zjRXgwG5j7aAdOYpooc0Ab2Xt0wu94Znkbe1gwP62o7AeTmYCpV
ZW+jRQ4Nog1hwWHBVbuRAvsaY4PWZqXhqwNmZFP/njvJhVaPpngoKcdl1waYxgqfZRcNWCYNeoNf
B/JNKfwAE/XJx/niiQJYf+Em9Mo5o79hEHPgZJSa4iJhrX1X6qbe0+AdJ+wEa02BGTz9nJvmT4Kc
EjEMEQePpjM+boa1sdrVg/0G13M74HkWkmbP9hgfkyypYk+DVwxGkQyqv+03GxZedoUwam6gPwBf
SEInz5wOgAG4QEqRKJvX0nDTVTKJKNQZGcgQ15IOhnEa/123wYRrwNxKcyrkk/i+N6lLEc/cM5aa
HaPaKoZsRy+z/lnklocvuSIjjvzQ6cu3jbsED3ivYkCIxBysIX5MbysVNISDvJbYWj9aqDFWXOjj
lCbCbW9NpGuHbV7KifvNzE96y2Y9WILJBXDlI3N2jsV15cBbD6YUev8m8m8IsMMa58GbIVh1ow0h
tVjImBQui1qtcCbCeafXUjXVIKIbZ/E3E2b1GWYB4gW4q4HS8hKeAwS1ON82AAqLQHtNyqZlzNlK
s5402iLinIYC0X41PiHQ33pRqKQsTncMVBDp42SUNQJSb94UX+YYdPdnLMJ8k99jeIO4hVP13XXQ
eRrWBB9zeZd1ZVHRO2VwLpBx4salwYPzhTdVgjPj1DwkdU5ou4Xoob1rqUEkZwZT2N0MUIyt05tc
iBgYMiEPCGopHaCEcrPiLpwpUC9XAcw7uWlrzQdLbcx55LIujTNwwILisC4q3NGPH1CoDy2wjYBN
6E1UDhw4JPfXOF8r9bXNR4cZMccf4eMHeC+O11Ou3klDSB7ArLjXsSC34zIDYvA+GS/ePUAxA2G3
nV8709hoQCZxSdiNaBSFVn3aTE+GeZkcg4OhRArz2WsKnfezxpXJ82NrUCkr1UUUjTPH+uTNUYPj
6RRJ9c4yUqo7vEOuoz6e1WXeNw1UJ9U9QoqOgpDIdlIOHUrZaheBqqdIZIY8uwMBDd1Bav64uIjk
cLqiVBOBH0nPpxKMDm6Xp0jAp0ZPGz+7CwDgZ7uaxwt3xfpSkSyY73t4qyCYMYKshJKjukWwW0pd
AhZBJHhtoPykjVuuyNZJAeXykYS7BZ77jfD1DElWiHmiza6BjdeSX+hFovhZkWjARRdkcl8hwAGk
Uzy5XVRLKCJngoul/mwxHjogkGR7LQ7LYP1ILRYQeY4kGPYwisMyifDX4BeW5dKk8GBPYEwro+ME
mKmFLPX7/PEB3PhG0qwLupPZSAeZsSZLy7C/jPndxH0d4C6TMcdN9HsGLfVv9msq90nkGapq5vAm
ZhJJawua7dEgwrLO65bdzi4QxKMr+mRZHt1wiKFx5gx1mHf5/XItScj86C0MfDc/Ky84p6pFGYJQ
dhT2JWfqJY376wFIjOLxBdzj7sJD/skrG3giKOMLauW/tGfqDl1kRfR+iLInw0jeZzKs+zHbH7un
1qK1VfadMUCPv7z3J2Wh1vEGtqpC42U89Q55kZGOMbUbv2Vv6aRveu3MmYei2GKBLoNWvIok+8ov
iU7f0ILjTfv55daYp1cX1j4wIZJpsdiJ/xURlAiVRPSkGbFktG2UDkm+IpEce0otitB4t9+VBnh2
6ulWcFw0Rfux6rmzVaQwjk/AwgQ/6quZePo3hKcJlXS65B1yBtrU465qa8OLWsMpw/Kg9+ByvSQL
uqWMxkSCDGDdCsL9C3+Sqe37aAaC80N4OF8h2cVZ9Q1V7+9lLOi5d4f82vlG3TWiV+ZMBP0zjdJT
kbZjFNE6WboucqjdszSEpEQJgY3nwCgR0Vv8OThuBDwDwlt4wQF+3oYkW7QhQd2cJ1eInGKIjxWz
wJau4OCs43KyyTNW5K0T8d78AsQZk0q9pZeQDJ5lQOxAKySNGN5G1j4XtALMqAmHYfDLNKD+Csqy
5nhJ9ppBdpt/zLhR0w/s64/hwxvFgN8F5kk4f8FWdjUpqRCw8kyocS4U/ZHi3ozjVt3ukyKreVX0
V2ibNgRJLzWXi5dYIQ7X1FskDqaRpplCC2ikDEoKxfgnMCtA5YtV7j2ou+2jnnIpyYpVa862tDPT
qJW1X5sLRHLfsUcztX8+fNn5GdBmizdjmBlPSiUpt0WpG7r4wZ/ig84HzjZp8HYkbtwv67+xMNCO
uySiDqrwH36YAb59WAIIE7sgwr6qQ+trHvxsZMfI7XaEutQwSEO5SvB6y8KsE424UYt17rkP9838
9voc+UFdGqNBTSrdLiC9VQZV4oQVOtJSS57NDJr05stzLvrAmp9HCug+Dm5W9YO1f3gSVSI8TcNL
oVD/1/O8KlNFTK0EDsqMqnyYEYxi/83FTFO+MHR9ArJbf4VgTHYzceZGSL1Zyf/VqRdNgX467Cf8
bo1l6PLIHL5C9w88RvYbf9sOcAOAlc770f5Q9mDscfK3JtPcmwilbv+F3j56a/ZnvwwzggWz58B9
OlrK0kBkIsxYRDnpEdEJvPW+YIhEB2sGk0qkwjB/RMVDzpXYLB0SoHmDALv/B/e4j38Xi1GhM/Y1
NLu+3b9n/k/xlM3ew/5M6r4B8WsgQuRCKQBY0Vmca/IWdli/MFyLjlVUZlS7HDlNEpu6GMIst2Cf
/AQ/U+EVVWo3aPEIUZ8SJSe5Ztcb13ywmCrc4ujZD04n9Sn2VH6UuqQnlr4FvTNQUQAgjxfTxHS0
5TwrSC9/Xm4Uy780MLmY1yMW8yrn0TUKSJTrMEuQgOh6GKlMV2/poFTLkQUnRpzGsZGk8FSVVXxw
faRJuWhX61WaGUl4/KFj18ZpAXZh//U/6hpIe1APZw5SeCCzavwBj8LrDoVRqt6J3LTnQ4UDmqF2
UcWQp6BOc4CF0E3xzHml1Hsx67cEvBqLzp14oIG8792vW8VqhlGqhvPWtwVZteWXYSyrdZGLtunb
G4AKqZ4/rFpCn9y3pEgwXOvoLwiuLqRjeO7A0xWfb5GOEXU/+umDnOUVE5j4jZGVVudmQHnztdQ2
0WxJ5EJaGXeXpk6JOPjzANB+YvrURTGqJVwS4CYlh/kMEztbPUETTOyXA3ss/wAqvw3sjQclZVmH
wSWFTYhCWuy0+pIBKzVUEuhyqCuc6EX/kHKw0SKWm2i0EnLDXPAsDBlP/u6Lk2hOrhdt2zdfmNGr
cWYaVb67E49VthgB8MLHG8eWIg4dgn+RoJcQsDCFBIhHchKeGCLs1AW415/6yYNU2CvlvoFO7L6u
jbfqd0BjmjPzaEcdV9IokFxwFxzAYLv/ouhcAAQ5HqAv6by5/ciZOP3LzChrwa0c6UKTcvNOJhuJ
+/c0eJptBZqfDnH4jDybr6tzaim6S5CedIVqAB3aog/QjFjxdbB33uDQBRhrEmXc9z55w1Ef3XD6
jThZEgVJZC7sy5r6MWPDHT3J/OmmAMJtSCb/OOTcpQSemYSFspqiziGWLDYK0xqiAcUa+iAlqEEs
sZXPcWLxrtTdH0kVWpp0/OxyGrCabB+2L97XLY7w8DTnepFnNeou5HAhhIZG66mXKJxI+6KWbWP9
+nnERhlKDbofUShxMSU5XST9hri8Nmc0HXJF1meY6O7IuIqgttOKdYkldE4f0h279tTtrNtWMwJM
okhHQq/eqqOz3k0jzeXUcq58V/aKIhQbI5odhVautNzN6dHff2BJ5ZsA5C9LMuBos3c5BTtzjTyh
0Gc0OaqWdZ3T9H/fzVc2uLKVI6EIzmdIKs2eyDwlD/btf6PpWHBRk4EQFZj9nR+x7oqkb9MvaQow
oxs+prEBRs0UMQB7TF2ajioXf5tfrnwDiQj7Ywrkt18o7FbbJB2jDBD/XY3kQWPvBuhYuGs96fzR
qK8dKf2Spqjqghkawfp+b4og77Q3Hu6cPpgb1MRTqyTKzyDWBMl3t6CaZjCrTyYVvv8EZM9yBUE1
YwF/S03R1P69lBwX9CmsrmQpxvD690P6+yvVcyTlqXGc8eDqgwsCbTqGLXLFKPOKBZhlqaf8ly8n
35ZnkXZ4f7HYvRDN4LBQn7SDYpSe/6e82anj1W8TLReO+rPXkcwFWrDyiyIR+R2fi9f8YdZaI5Tg
vUN87lEvRXW9wny84wkeKh2SYi2Z2DfYOU/fvvOfaoF8dj4gN5LUQ40gD/K4/BvbKGihUPu7rZhy
eCvWMueEtqmhAhf8Ee7hiKIWafS1KmwXShvGCS1fosaEJQqlDPboPvp8BWUIMPjBh7r2A7tKAUGH
M3OELiY5P/947bcXGSXmnVyxWkVZLs747hVpt1OecdWdragm7VeWFNs63fj079O+s1kgj/KbDWxg
skjnVITCmmq3zmvJH4vtRTq0w+eCo199mATsQ4kgfUyvSpipKmmcDCKV/xhzdKn0R6daWwPLtFyL
CQUEAVkKBtLEFyDmfDGiGoKsF9HqYNDWBbXiKQFc+newzwuq2cCcqBD94PGxQ4zX3Djx1yoB8TVO
PaQw5MWPXE8p8H3oB8Q/qksrUa0g5Q3kUsMmVyh8vqRBCScr8A+TADrvQdikay6zvA5B4ULLfnvm
fdIj8Bz1s5DQJv8fZMIEsj1ggcVIenk1WPmyrDeSV7Xo8urTvxl1x2IPG+qYtGEEAYmIdZP4iFjK
VehSbfzBcjF8suAf6pvbtIh8RMnUIhJXc5ETlFU0xaNLTaYfbLUjQ37peGGbeIZjTOkCpFnEZapv
B58829UElHueZbb0CK5KGEuqcnYMefc/bNhY9aGmucb86DeydUtzFV1vn4sn3GNhRq6Sz337F3H8
G5JzMMVRestpf+5EXR7E5h3RAQiW5iMDY3fleOpUxhWkcDLKhdMo53qOZ2v2tWlNtBENrZQvuMFM
h4ugrYANExdoW5vPAobuT7BfGr0M+Li9x7kKOLLcYbLN8b4lJ46tHWht5WyTjRAvEyASTGll/HT4
dLPSWVblwSpT1ARQfgiQpgk9wqqvUxXWFmWHn2U7n0QKT8Glyo0mI+sTyMclbSH/eXVzhqUZxA8G
kkLOcoWaHkCVsjic2UKbo9dMmlETX6/EiYdCSzbOQ2TVrz4TXAN4Eh5S9tzFaODWnainCRGBdXf2
+ydMxKbkP7S4AF3vCkb+oP3bGDYLql3nA8KB7AXzRsFYZugaQ9mjZ/DGC3pFBNwWSJgJ2qm6H+Js
+fDySJrnECAWvwoMQR1DeQCATa646CySUyUHTuTTQH5k3ynXH4T1m858EkiOOYypEAzDw9ckT6z0
CiHuvYRIsOqAM9rrVb9oRLRxwTae/tBz6AlDMmyn7PgW89kpVj8oEZ554g1CdZ3uGD4TpqrRlptl
hsTklWRnWuWigmlR8+bJiN1tSn3iDg8CN4jeKGfrkj7+1G5JyIPjN8SyCiB3Bt6oIpvZ8s5ZxaHW
SCYYLsbMZH+A6Pzh+OHa1CQXn7uEjJ1xcQUiwvM+kE1az3V92hWRnVe4y+CQyQmN4Ny27lBui4JG
S7q1OUlvtyi3NbMkYHp65xTLnF1cly3BsCQE+rBhdSSxNfelP5+gSjPaxM2EksDGm28ab/SRjuoE
T3j5tO7bTUHDg0N0GyhTBqLWOlIIDqmBGF5y4qSWbS3YKa0le5Vtcf8C67P9yvUwLvewL3alA0Xu
prEWF5/7Ajjqe2Cv/xXVdv9htxeuXipRiGmcCqLumA0M9kQVd2ghHO8nBXFMMBbGXTtJ2W+pF6Ox
mDTqxQ9fuTB/bK6F8hzvHH24odUG8QDswRsXUZsZv61j8WpGiRFoIq658gFNVnxkOMKC7kjDMDSw
gK9Vxti5EfCjoRpCnDt3rk7eKoXrLJICQYMAn/Eau6/wDZTBSSqeusIpEkC/lyGgvlG7dZ8GN1r8
NOG2GSoVTJgWtsPXpWCEN3cArNFNHXCv59bVNfLRSYC7sn8bD+Yz0YJGYi9SgKMokWyboBgXRI22
ALVUZ6ohK5UEuvMJUxwOkBybW++ewq5OhUmrIHzLcLYrAE9E5UvzVJXTL4BCpVUYyKy/aEHpcFgf
1eA0cPwzwIKgtnqaybHZShS4a1S2R2fxwq+2Ps2AfVwzvB5sSGdFkfgpwVhAjZYuefiRPoP1/IcD
bvgZJTinJHWOHO0Ffrx1NCjlpw8WTiqLnZC6dZOkGLxQpn5x7j3EI1L8/TM6iKYSIN0AP82Jfzu2
IrmcPofzL4IdCBAd2+Gzfy4GzJr5jmNa7uPytu2/u/59pM5VRH6ANuypAxnmkwKulR0FN1cln/9l
Pl8EBybQ9jTF6EzC28PQg+HyJ0nEr1IzOdxcroAS+rEBE9RevsCzIeXOpbeMgEbCjH9RCARLSAoz
gn+wbUJgQzC57T/F+ePI1pBnNUu57Dyp05/U1MUBILE/uDC/zeaEsMja9kc8SZEgoxGkDlHCUGRZ
jHS7Ty8l84wqSrKA41ScP9RjVuQIlGbHRzB3XW8+Vu0yuJPgz7FA1/QBa10VC5A4/N44UFsOoYfx
SwcHP5KaAwuL6FeUkEWGmi1O0cOpTZuktxRXCfJD+eypN13ATI8WHXvFfX+BokrSlkk5+iojS3QW
2sMB1uW/Mjnr3GFjgNvvEUdev0cxrOR3LG1IAFW/l3On3Q6zIOILvkqXmLE8NVLbe2rDl6JG+1W+
8zs66AhBxFKmO1bZKVkwmoqNF2DVYv7jrnLBiLobSF2matBmQcH6B9JC4IRaW68eFfZPLsEzLwEp
UrYqbXtA+c5a+LwBWhzRU1WIi199XmemRP5w9IaqylHvsgToDHBCdrtZ0Vv5iybk2wHkrlXgUcX/
dMwA3chkOVACnxSKP/ddl9gsz7ZNB05i5IcdB92oJImoc2uW2JsV68sx+5Idxo5sHy7o9qN25ylC
2QZs7gTgtLSCWLk2kMdTmBdjdwSFvw6c8VNAy4yhMpQwI/chrcRIrx4DJXiHwEc6eiYyiOGWSkT9
3mcY/2QxB8XjRtNMSfN4PzuTRJgFxcZMvuDI/RACkrA40ox08uec6ZLT5uzFik1v6/CTA8cXqljC
42eC2zDkV7pJTLPgW2JAyNrB6l2eY1NJ9VNBrVV3G70nOh7TUqZ5M5LwvSeo1jgIBT0nt8s75bfx
PQUQqOdDr+akRPlyUBkbQLp+fRaAB4T/LiWDxGDvfw5Cvw2NpQdLTneHadmBIvUSH8NUNIXvQE19
D5EfRZAL2cT6ZTXsX94/ENY4tbyfb8Gx861qo3UQRY/2xG1Sk25OJvx5T6CeM0to1I7nHJ+0LwG1
RDbaW3mhy6TyUoqZ9VzkOFBQYjnZU6PpEmrAc0TM/sBjUU+tcP+E/sYAkG99vJEU5IjbszhrLxKJ
z02hYnjS9Jt1QBwsaCweAErNr1+r9M1dGg9zgX6kblEVSnMqcqXfyb3jV5pnd+0V8N7IsGS8HsHX
DIpmLYrazl4zysNXCwRpzXkzTLoz3YynEY+6MxeFaUxOARG12LvSWnj/a5KPy4Gu4eE5h8xEfBFg
LArw9SIadJm3ILnA3qXAuZmNh6Jx2nLHRDt6YWOw9WlJhnsS2wE5uH+HOhkwy7qqodduwnSRh9UE
1m8HjH0DO+ExXjh6NDER+Uk0pIXU3crjkRC9a+pRjcFdAd1C+8rU6DuEMWMI+PPXeqRqZOalYvoQ
5/1JHkFA2UagMiZW5MYijFWYznDXp4Av5JlGaeIrNo92+Ycr3NBWSS+YDQwp7w4a+7hGO6Jxr13z
hHFkNVsQho4KKN6b+6zqW+bg6688hqYRbbGfyN2VqzMkBaxwqDvj7SkaA9Tqdbf9n5UE+nsVfNXn
A8X9ag7mjdZZ4lUof4/vL8KsH8VJC7DBzsK1BAhI/ctVvKil5QMyfM8S9lyFsaIwhvYKYw3B8k0v
k90YrJcMeuYdSJgXfKg/5NvMRhDP68osrQpsCX0fg+e4AbK8JwBa737fmkV0AxgtiYeYGBHCtYIa
OHKO2lkbFW1Wf27cb9/Je8clD18Sdoz0KSv986ONHjPQVnZZAOx9EaEI30BS7rlUQ6OdRoDggOLb
jB/tQEAexwOF/GgEz1cYJaJ4iWW8XFqZdTAPfxq6Z7AQ/9UGU+lMrzbRABtZFo8D3s6922Dydviy
MhYJiSVQkxzYmW6MGLXPifb5KU9Q9FLn+dO62RGAeRWHmq7yicKbHwNvWb7nnnGp5OEPW7gzjoUD
qE+RumgAnB6MJfrcgD8MDUu/chRUB9hu4QdNdUQlQ5VEDenU1SL8n9rVbC6DWNyRgWnpfPeTZisv
9jY3soYkl5L63bBR/+NIAg2dTgw8K9A9KaFBeSdqjNbAMYjJUtAD7GWuvsuLJyZTIJZJtgYDs5B1
MZCld4+H/OMuQY48bcbfCOiYFW9zSoROEtr60CZWp9b1f7PUhSrAshERcttQgqcUb3ssF2CkmASf
cUMmARFGMWeUjldSu3+poIvoI/vx8C3IDgRSlSyJzvEuVti8t1Irt98Id6Hy6UM9lqzZOSRTtJsW
ohkM7yCvtVnIaaw6MJX0bvbHhq1oXv0NapOkhKgP9dvOidsGtrx4IoyvST8EMxtNQZ8a0cJXlWJJ
ST4n1uN1tMr7ci9BI94gqT7xQmJEO2AVODMO+QXEDtoYFIsb6R3mEMisPZBD7rI2zm8CdEnwMEjD
/X5QL2Xs0Q3Wljbp2Go3rvajlErmtXuyOjH6PJmCMYlu09oFs6uwenYFUnGSAa1hS5lMSJEGgbZS
Sn7sj58j5nj9TcEVXMGa8U84ObiqlfkqGfBpNE6DRsh1pbPQBGxI5jHnevxkn8B1nFofXyLr+fzt
aPP1iSRaLAzggMSlb9SQcPJhyaD4BHJ34mHcQyezW7KXRtCmr8nzBilP7z3sn2nFM62lrJMUUm5K
c+qpIP4QgMt4dytDc5Ftqnhb45Mo7wU+2oHJc204Hznt1qT2R8TB5j0D1ChdeAd3kfIcmvOCwZsS
GgpFwzS8+qIq0ZyHfbadhDWvXtS2f1myzPdB29YGJxSGT3Kf/E2KPjQ2be0L5xqcilNLrid0Rxtg
OnexlMHXOJh213K8dS5xQN+ossYeXTSllj5M/weJMEQtgDBrZI50jcxXzfVr9Xs4dO5VRGHWhTM+
CnpiGO2d8P6CxynVUGlS6aWx0KI7pi9Vu5f/z+UnMzdYkOIAPd5cimD/Znb5xkro7imHFk096kWf
Q3siIVu042vID2jKdXqJPVq+6bKAQVn9F9Ri49Sqn5vS1AAXN1gEnibnxA/C2UQrE4SBMX8hEuJa
eS+vd6zpZrwFpogGw9q/pzFN31GCXh2l8qOT6xlWW61kuqht9DyowA0AlGz4/THAcLGTb/7UH02d
FaoR1o3dLaNRWaKvB22jj+rFpELcCn0Oj2PugKAB0+QB5D0l6n6wsv91cjhQ6vOoJD87Q3aRTkWZ
xcM/lXDZsqvzLh4LK6GF1Z4NpZfA0uUBMdMpWAhienI5jwFmZPfI6JETbMioPgbWi45aK+hW+AaN
jGmTj7k+ijhtTpKIDheGj/37PJgYPAeoCDK+V2NsOehuRvYa3yTXIGNDDMvK4FyEFnZSFdhG+dFI
QSahTD9xEwW6ILDeLr99005q0wPEOdo4U7jSGgRcq5SKADu47hwAfoFs3xqovMKCET42yiPUlIkG
a3L9WOg/gmyukxqfuiNIwDdJf0J0uufwdauCT0Zj7KG2gOuqIs3ciezJDFMU8AW4/VdKF38O4nI7
xYHHSfUFa2pPuh7VA27s79nTYAaap50b3PHsNHKA96dDl2LG30QOjEM7hQPkMJV29Qo0j1KWQcj/
JOrD3U+0pWsMZYIU85BoH6Kg6xMRDOYkLbho/8cwUhtMqjNxOCtcDK401Ov5XO0X8VQtJMa0D2zf
W40/fZMbNpL5Vj6ZZ0zTVu0aklAcIt/InjhxT9iCnXxkpYYmc2ca+MOfmggHQU2o4u7JGg5vg5AP
DWLKmhbqhCpFlbqTFR1FvYDLyxFftvjZ8tZQYzRwX/8MBdpx1quta1EmFARxuws/0PvYpChZI4I1
gxzHsfoRj/8IvnG7RZ44/ve2+kkhXH8g4lWosnrArh3OgyA8LanqhyCcVATb7cKrnbK0DE3G5Ctf
EkQZKL7b/0FvEL2M0oei4SazowmRZuovi8L4b3OtmuqQnLmU0AMldVBdckO0slADFHQX4GZi4NLa
aosoLngNrRLGUG/yaKf2BWPPefCfBHlUhFeJet9pG2XTMC53kZyItBtLBXkcdxkF0Mr90mxwXsx4
Yw5ApIAb0mFhrtsOMQo0vKo/4dZlj6+1YcerWo6DLNfsr2JjLc+jqozIBuqJw6KJA/ZTPBpkGapP
bUMQ+c6J6i0do7WRbZq5iA5MQNJHdiQvhjxU0v2ADQA/D4Zs2mYBV8t4Y2A30juaqTJ4Vr0ORuWm
Bu5B/k0BMHK62Ko4iJxQbEVTQaPyRaVuw2kLCvCkOW9Nl/AFxG9tjw2PaEVuGiUAdVpy47Q8QkHg
Q/GEfGFs+fn8rj0lw44vwgKeCMLxcKhbfgJFJJ4MVoT8e1Y4946vqRm1ssy42upuIR46IjCenIy2
PI23kNzZTs59vFPGwACc8T8jfsmi+CnS/xZVrwCR2OPRbngTnKslekSpRoUnr894w/ZOwxd51t/I
oIdH3nPsx/H+po2ejdkFlX+ZerrE6mcSxTLNjGZQ+MkTVQeCVMLjkgn1FeeJ4MMfxTDYjt1Yp5CT
wZSM6AEsOM3zYRvzU27kzBof0UB70UGzJJ+KS+oJ/hrQe/mQRckm6oVQFXrh1HmZv4zr+H7GE+g4
GVJ3c3sSzEikXXsKFP/0Flc/qFGst+ldKbyyqAtmjc2Qyws4/2YNmLhqospd1VuO2ucRGr709V9Z
QwCMa7lBdegBGB3+QTY6pJNwJJ0PjjpK81Nl6hZcp2aSJhWKkiwWD1lQZxSxXz89UrQbhBD9tbLV
3EYFTyl2drvkevGzp+WrJ/DMGlV0RfsyIpz+dGmdqKHAmeS+FYSGJFLtttXOfei/ruIveO6KTLhY
lw3Gg6JnQFN1ImfEmENM/HmdUpJQfKJZcAOFqYF8RvOHcfcFMvSHGsqYxut4pzN7TPcW4sDOLmBV
ucSdSfOcOLJGwi00ukm+U8cFAV+P07jNTmrJlo9j5YeDH6Z/A69seeiN7VDmVatpB88loOPqP891
iW5mWWXVJD0ZdbB+K3c3FkJ3PJb8bxpF9lVBIXTQd1mscWR+6RphHsB3z0qc+wMK+hUYvNcBNdUF
mX8+WbiaDlI/TXZwBbhfae8BOMeqbajANfXm0IGLX4xOOTwk3a/4LrIE6f+0ZQ9+4Mm3wiZiLYmN
B2xVRrTESIySVBjX54ny6E+yR0yKeN7ErkGaB0suz98RUkc07KZ5vofX4kt6EPb/+jQOxe729rJx
eD2OdeoYJFoBm0tJKrnFaPGMxxzkUo7aXUb7rm/W3UmKC7C6e0ZjQt5wMzT9o3iKZMA3PX9Czm0b
sOR5vaQxdUZAW11Z16XvcXeYpx59/3qeNSxzRBDiZ+ob+1OCXkfp5ezv52ocmpeAMgShYxyLc0fY
EHjZ+SqL+ng6PCBDsRc8WprN55CpAHTJjLBpzk0SWPBXMzBzrIyjqA12NlHbMl9Ptg8C4R/CmbUL
aW6hhq31/8y7pMTZqR1WFvcCK0olkcARmXWvLzEugHcDxUcKS5pzkPlETYwV3KlyblcRnh6LvOWb
nGG656OxNP1FV4tcw7AkmLxeXSDZLHFwVXizNTR5IO0M8C7vG6IEfPzvav82+zSaxtf3aEhiifV1
wFk7F9C7cF+4sMzoqOVhp9DpFmCpTSqC64c7tpbeJOAq6SxHBjrSqlhM3GeBtW99qAJpKDfE6Wql
fOktKgyr4z7S5I6e75uMVJy4dXHQ1OaFviqbsOw+m6MCdnSDlikcnDyAAOmos4REYRJA9B99vLVi
4fFIbEJ+pDnYGfORKxiaz57Wf4JMdsZf9s4M0duu4tv6wkrIIHnR2N/y+NjHrTIvo0gubeBEKYMC
Ro3zbaHuBttm1D2w4dBfYeCJTdoZaHV/be/cCqFCXlLPE76Qd2mzPKRJJ6e30/wjGjixgKi/a5WF
RmQBNOTDCveZNs7X70ObZGDuiREmGvb5z72KpbqLn9RH8a3crpRLW7j2UMaAyFKIzwiEj2eNNJUp
6HbnGLLXalXV5uMnUjGC8UrYZk1aVo77r+L0Y9TnIIlUjfPv8DmlrhHb4RZOJlNjmuz+98B+/B1T
z18ya5Yd/O9r/i65Gjj7Kr4QBD/OfiYp9oxfM3fO0Z38i2UZeYI1wzYPtfrlWc26jNqM4Bx78AiS
oKK6H9Gv/RILDbVMwCWKL3QYaah9s01FZArsJjoDL5UkOome3HmDyvKzpwS+RfmlxOcmtOP3jnkc
VZo9/OVvnn7PbTprk6084Vj+DtalAyozRWYw7IYGmHBgfYUOGcWX9bdiM4bzZ87SquL7K+z7meTk
lIrf94Fb2g10e8VGju3I+p/Lx9RcNRWHUM/i+EJSNnWamKV8MWHnswm4f6pmKi5LVIm3v7pwTll8
ZGYsRZR3NT561vGwOsjP9b0swllWGTT0kIhqY0KdsXLBvU2J4nVoeR1kEZ8YX+b72sLLKQOq9Y3v
eENsTLjLiDHdcSKCR1b9zKnKrF1KV/7bLLkgPwJmQZJ/1IvsOQrFI2EMDs0SjWOb3FLQn3Jc5/8z
25FrBeOjGUu1TPMJ9rkotbRMortXKmREWNjHcD7YwsCXfKORXCaMbIwnps9jRIJZycx2t0rgCVwu
ONX3srBuhCZ07DfGhXOm2yrxF+3e3J3BaIXRDuX3o0uVCIH+l5x3GxjG/K/P/7rApiVp2SrnsXgp
83a9NaHZTVY9E79sg3fEwGDDCj7pm3vadlZWHCUfQEqUJN9fHDHmcplsqLpwkmRVyBhEEzZLEgw1
thzXZrsGeMTIxVWw7FaKtnEd/AXo4re/he4AEBSmWXKQItSEH+i6FUvQ8JAajFvOmUBmazB04X8K
gtDozC9M30hUL823C7TI11OUj7hRtnHphOFQNLhsze4shdCGHijgs0DpU7iqrYy2FRG6azE3QD/D
tt7qA6Degfv/Xwz24pN/z/ZlMeL9IKkeMaxfb8r5GtMNYTTPGsdD4Old5UUwOQayVCmz24slh8Bi
1KGtRjOBlaEPiu1mxJhBj/Q75He1IOHFnaTtPCO/XKf1sHKyozRoGJNgrB9Q8l84jyTUYH3bpags
TgLSfNBdym1uvbOuuA4ELRJs+DoM81CsMTQ5YMZQrgS/Ver8FGwi2WqRNpFi350rO8zSKyH9KLRT
g0zKcraJh45lpPBP1cAJzRVpws/2cvpvJG9N7X79c8ILGXrxzUz9nJNCzeW9G8hlDlgV1hHEJhik
Nu1SvwB5+Qxr1JMgZrPjKKGVZtz83NRe8ISlsxw/2KN+uUesybwOhtu5qb+1yiIUxAGTf+DoM4MU
O3dveFZW9kH9Pp74Brz+rFbSNhNhIsEBzNubBruqVjOp903wXtzMNgWN5+CkhzUftLRyeqQ8jO0L
mYeXZE3URvnwritiaCkknESGiojKk5NzqxnYObUxirInTiE8ATZXyBTVt8W/BAJwmWy8MtfH0Cqh
pE1kAsXBpsSzKHO0nMGbHe7Q0l8Cwv0nrOqIkk3PHJUDweZsBZrsckEwCRIIpMbOop2MsBtPXGYd
AMAmvtSbS7xZod65lpe2wtSip/l326mXrPD5DkZrdok6WpYh1L2of4GJ1jMggndrzsLSL0pUUqmj
iHeonQ5hWXx4XKwbo6jz9DeELGKAkybNEv538iMO4T3Kct5LEYw1Wtd0QxZH79iM9a4kOdvT+Fne
nHHoLVHDAXlLnKs4kNIoFFtLiOBOP/wwsFB//J2vT7ndKEoJq/V8HFzKo9X3RhzGE+MvWLp4PXlZ
piNzTSe7LmQk6SwspjbVWJq7XnR+HPFOr6Nj6V2256oHrnpa5OQLkubzLmz9s47mxEYPdPwwFUDw
gwDK2OWP1751Tj2jqSd05U+35A9AUhnGiA18lcnX4nk3d6CRzyVel1H2/wcadffxwaIRLoqRyAjf
3+DeKJhb7XT9jgfaFgYJDAGTgoSrXwOK1yKdrWomuorNcoyQOwE3GT9E3MbZnJFIqorB3WvBhH+9
ckVZQ6Q8Ql6oRTnVndaMeLBBHMD7tuY87m4vZQdWy6s+83q/Ls02BLauPTMYxlywXVfHZ1e4gVj6
/zj0G7wm39rXMe43lse6dpnLpVMYdI8y35Uz6YWlSKDmYJxTyCFMgyKSgk/MNFalv7jHe6vzckeP
nGYnxj6fCq5jPowv4dOFIvwJ0INPSBrpCLYDYQyb8q6PrRUbdrYQDX3ZefQKxwCL/FgT+CktIkH4
S/akekfEVv47gd+hgp8rEfk1o96FMJFMpIi6WY8lPNVILRtoxgdafoChrCo3h2WFpnebNywHHkcB
HXZpJnwe/Rh82fd/Ax08Qz7AAZfCE57FabTOnMEGPuUwrdTdul+zQqimU1PX0O/L2cvZv0VZT4ZM
p9vWNqAYt/o4OXwuYxHRaVMJvWmoG3PVrBcmFnbLvsTFPWgBtj97AemBT+huQBQAR6aCD3dSHWsr
g87kJl8mQe5ZQX/112RBLz77fdAnWUU6adGzxsWusUjQdNtTgylm9CM1xQnl4TCtT/dqwL2B7qP5
bUptByH48KAMTtMHr+WLiWQRumuFvo4CrWHfVrHiFSkCInCty17Z7faxYZat3BmDmzDuYOQ1/9Lf
1MAcC3iCkgW7qpUC2Ed7E5X/qCgAc9Lmv8P8HMth9KArd/2nBkshnP0Cozvnd46Z29f1LVH7mPpb
JszlkuMq1IE7wu2prEpFiZxzeG5BEpEsXCHl9Tx1iTU2Z8FmJwSt8csEwFIpc7mrYFgOy1jMISlb
dUYVHPrugI1KV9jv9K4WgQ1+au5MI8qpVKwTBE9rj8YvA0ZpSakaFmLvsaUETug5bhbj7YnEpQhM
B63YBBBSER6evcch66NWu+JFxLRWQtLVhhhdhelcpigh/IAaMOV1ouBBFQGeFoQjNW/icv1C/AoY
T2EmikgFCHyXibD5gql9Xs2/khO2YwFfkRlQC9wuYtZSxhfOdf7neuk6fzbxgFh57qGs5OiOaYyh
j/ybd5I1//vSDWkJ0LTDaUrV0eloheYJ0oUWjZcYhVz1k6aFMs8E+bAbF8WhgFy2c8bL+gKTA9hJ
N2W6cLn5dnBvmm0tLlqqX4tgchaz7nxfQ5AbzpwWxkl028S7nghSD5McjHmP0PwwNFkbzBX7uncY
2lRXzfLwgwVsOOFU80YBA4DQ391eWfe+Ag/rqaBJnmrC7EZvqSxS9AioSKbuhITb+A7bzNT1IhP1
MpmQmy4uB9hKnsimEr2hnbEOlxH1nJtUD585rAKqF+z7o4Cnlce4vJrpMTai2DZ6vpEFsZ1LvQ+L
e4g2UfLruNSiOZ5Tt8kARQ9vW0OR9hDe2uzwfz2HwQjyoh9bqH0Y4eg4UuVMrkRkw2f9wmOQA6qG
e3c1hozZD8LLpRv9UIvLBwxln/TGz4d7dJz8h/ZhX1ONus/IOqA7Wf5EVz1OiE8WU7rT6Up/V0IK
j8fZD+lRm3Ue5MUMHo5nomzNflcVYOYd9VfJ/k0m7/KhpQIakBGyVPcsEggtzl2WzjQc45nXMOYs
sl78t71PHrnIlz4KTfHOFj15XWIjZ1UQLOQFJ3yfvwSzy6UJUtcv9e26Rzc0LFKj5QENJD8ZUCaQ
jSdUB2tQ6Le5uQoehXUvGj0bZfhWmEOsH+/LzpD/mIyhWLTp0SUBvYO51hIgMzeY+xFhfTVobgir
bQ5Lxr6L7JvKs49L6WHnqe6IJw/8vwCONQZUyTIkYGqpHHyu6G8mLRIx6idfODEBOuuJC16JtY6K
bt1Pq1XrLASs3KdTxSaAPBwfr1ZC62ifE+7anjd9N2+uRNBk6BusQ1freyvHGa/3hvLfQORyHWPy
+LyK/mhk3sGSJ9DZ/QQIUADNbJwVlxNvzk5cdJAmx76h3YYkZA9BbWuvmzNEiw+NsPid2WjWnmMF
mhksJJXo/u8djriYWzCaQOls+CSJfHF2fMWswZ7M/tREA5tsB/fijK1EZWyhZj4uxo6p+ItLz8Se
906qRskSO1r9A2hIoDWxv2BI4+Y37KjqYkSx6B9R6KWQJED4uq9iNq3gfNBLL7a+Eu+RuuAFDlpJ
o+SsLfXH1zsAdrUJ/Taz2jHCZnzicsemmMVdlvaTFjsxnRDLwuiAPPwCNfdMi3XNytpmJLQhYlLg
HpZcn/QPDD/0UHfxz2SNTtLugT6YrY6gjUtwIa6TXNfHvd+Kzv5il38aSdZHQ5+tTPEx2esb+4+j
0gEmKqYlIPwNKgslqz7m4nbpjfa9YFq2ywHY+q/K9GMhNbhxvVDo8ZAuf9Fqc6ChpuluuVJse5kL
Doljk6vDQl6E7wivECAe45nZsKF7E2T5jsqcQYoExtlcLX+hakYI42cP7eropmxv0nWmnmdYi4Ss
aRXxNplYOKBu0iy5kBKlvnMTQB05ZJAArHnvLiqI0D6il1jmyFhIex6oR0ZHwksVQ4x2xniwATLa
DF1wj3ls842v6FYePFHEzsOpVbkCJD4tAZ4H7yYcIti4LNJJvoeE5Xy2RhSZ/3QLSsZ7F/joWx+X
KE3Gz+9qyfDdrdyCsSdEohP5JsXCOPGECoTEpUK0TShsvlrU0b8J6mORixg2smDuy1qPQgzo/FHo
CR+9/K/AcijW9NvEzPLcjIXMT19/ZXHckSpExtfHp9JawDmnbpEXbDvEitJt9Qa594XKl/18iDKC
Q5hawRMcsgv1M9iT9gmbFKrhYyIev9ivNKnRHCZGNZQy92Pxe5scf+JwObjrZLO1zJptXfEgH4sy
/1+Q88XBs9aCY7Kaw5tYjgrwQ+kNItFnIZcuyFjkAD9f8IxTfE97vKMm4TvUGy7BIuqv3WU5qQ1M
YFh7+zG2v5fSOkbSGlcKdVU7rYaKfvCdbzuiydXURe9S7CgGg+eBhZ3HUJHze8U9SDuF646rNNBP
KnEX2BCFL3+KldmY6ugpABg4FTwZBVa+blVxTGypTj98XA7W4KiIMzrXw0qjqjDq5O8Fa7AlPx1V
EnBvQxvXCVb1bnfNSaDYsBH4nSRXzRk4QQWhXPKvvzMdK4PzYBun9nmbxidWfGJHD1T/2pSf81H1
Ehqn2rIFYVoVlKob25M4YDJamlgJuaBqjR7j44tvl4nmPPQzxGFKS4Tw4tVDBw8umQBQcxosohB4
EgYkwVnhMnRjoIMbgJ+Z0j6gL5atYaBOC/VoZxDN1p3XqcPRxeX8i0I3nuqvXlzG9y2tv8ENiJdo
haiK9svl/IyM1UTjVIkPpHaLvzKOGt6vCv8x7rhcFmdSPedSKz3R+tr2teDZUm/yjeC0PZP+LAFK
/upOlFPr6bBr99qr9Oi/1GI6LEVNszIFEoVpOlUVimGi0H8NF+gUZ4P9PTqnhIJbB9oqM9V/bCGT
bue+YKTvgtom1YiuqIoYG2DxdxjBumDRyoRiUp8u20OzI/iF+icGL0yZJuO2GK24p4j4GBZhQIms
sCqHgNMNkgf2TptxIEt6IzHEikt7e/JEazwGSeKLGnlMeGd8v2272brgxfyVO2j5zKaSrNiD5GdA
SgmfVEgDSQLCXG0EtE95UUiISz34vVgaqfxbHvA1sBrnEdYDNJYNWhvV4yYHU38g75of2+nb2E9Y
uGQ9sfoEI6DDRuEAOEs4EP1H59FTq3pqDmhyv7hd9pwX5sIXJ7PNM9JH6HM2k/mry4WtH8txipow
OdtUtDNOp9mejQhlixhS9u62xR31l3XFpOyHo4H3RZhqNe1niD4Ac/RV6s2ODxK70StjxayL7gdI
6QaAcYISa64Rad+pExEFD4SEeXAqohc6k9+yFBVNvHk3Pi6UpBjkkZraaBbWe2r3Zq4nJbcxj9j9
jusVPKYmjT2n0kG4wA2Pg1vbWMOV6yUYD6qgSTur9n7K3Ok5lxtLwqtl9NDWk+NAiEHI1HbTmCVs
C7+1UiPfds623VB3iLXYWZcGoQOyBNHtNjcaL1Pqk3EZOkFoakbHF2NhXpFlWk8JxPGOSgLa1/7k
3CBsiyI3w2ux0sRjrIed571aNHbhKQuJFzKeKZjUf9Pk9BjKqMLDbjb5ZKL438DvbmmLD3pWsYVe
HUU9Srs4AD0I45awr5JA4o8uuB7AMxxz7qFOEdDhiz1MsUAFLdpqGZD9Th0/KXuQsoaGajnc/qoS
nErbe8TXQEYvDv6kGF/JukyW2gILHJ3I3K6gG6SpMgRk2s/dJuqJnbLDpExRg1zzMxwV600InEsa
cVotv28dstLYrABLx4EHMOPwsbf2xzwsNxaIpxyl1MwEKR9eR5B+xchZ9x59R9MXzbS9fyaG5uL+
xs7Y8DRZ7NrfWrLyczvJTEBjoDjAKPdfkf/ojQFj8HiwyPgZtny3M/E2rxR7uZh3DIrV8bJ4BihO
ifX3RANwiUEUo/W6JvHbXvJIGetQuZTAcz95
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  port (
    gtxe2_i : out STD_LOGIC;
    gt0_cpllrefclklost_i : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    gtxe2_i_0 : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    gtxe2_i_1 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_4 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_5 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_gttxreset_in0_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gt0_rxuserrdy_t : in STD_LOGIC;
    gtxe2_i_7 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_txuserrdy_t : in STD_LOGIC;
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_8 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_10 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gt0_cpllreset_t : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt is
  signal cpll_pd0_i : STD_LOGIC;
  signal cpllreset_in : STD_LOGIC;
begin
cpll_railing0_i: entity work.gig_ethernet_pcs_pma_0_cpll_railing
     port map (
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gtrefclk_bufg => gtrefclk_bufg
    );
gt0_GTWIZARD_i: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_GT
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => SR(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      cpll_pd0_i => cpll_pd0_i,
      cpllreset_in => cpllreset_in,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i_0 => gtxe2_i,
      gtxe2_i_1 => gtxe2_i_0,
      gtxe2_i_10(1 downto 0) => gtxe2_i_9(1 downto 0),
      gtxe2_i_11(1 downto 0) => gtxe2_i_10(1 downto 0),
      gtxe2_i_2 => gtxe2_i_1,
      gtxe2_i_3(15 downto 0) => gtxe2_i_2(15 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_4(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_8 => gtxe2_i_7,
      gtxe2_i_9(1 downto 0) => gtxe2_i_8(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  port (
    data_in : out STD_LOGIC;
    gt0_rxuserrdy_t : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg6 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    \FSM_sequential_rx_state_reg[0]_0\ : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_out : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_RX_STARTUP_FSM;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_RX_STARTUP_FSM is
  signal \FSM_sequential_rx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_rx_state[3]_i_9_n_0\ : STD_LOGIC;
  signal GTRXRESET : STD_LOGIC;
  signal RXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_i_1_n_0 : STD_LOGIC;
  signal check_tlock_max_reg_n_0 : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out_0 : STD_LOGIC;
  signal \^gt0_rxuserrdy_t\ : STD_LOGIC;
  signal gtrxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3__0_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \init_wait_done_i_1__0_n_0\ : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3__0_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal \mmcm_lock_reclocked_i_2__0_n_0\ : STD_LOGIC;
  signal \p_0_in__2\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__3\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal reset_time_out_i_3_n_0 : STD_LOGIC;
  signal reset_time_out_i_4_n_0 : STD_LOGIC;
  signal reset_time_out_reg_n_0 : STD_LOGIC;
  signal \run_phase_alignment_int_i_1__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3_reg_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_5_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_i_6_n_0 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal rx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal rx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \rx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rxresetdone_s2 : STD_LOGIC;
  signal rxresetdone_s3 : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_data_valid_n_0 : STD_LOGIC;
  signal sync_data_valid_n_1 : STD_LOGIC;
  signal sync_data_valid_n_5 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_100us_i_1_n_0 : STD_LOGIC;
  signal time_out_100us_i_2_n_0 : STD_LOGIC;
  signal time_out_100us_i_3_n_0 : STD_LOGIC;
  signal time_out_100us_reg_n_0 : STD_LOGIC;
  signal time_out_1us_i_1_n_0 : STD_LOGIC;
  signal time_out_1us_i_2_n_0 : STD_LOGIC;
  signal time_out_1us_i_3_n_0 : STD_LOGIC;
  signal time_out_1us_reg_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal time_out_2ms_i_2_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_3__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_4_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal \time_out_wait_bypass_i_2__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_wait_bypass_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max : STD_LOGIC;
  signal time_tlock_max1 : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_1\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_2\ : STD_LOGIC;
  signal \time_tlock_max1_carry__0_n_3\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \time_tlock_max1_carry__1_n_3\ : STD_LOGIC;
  signal time_tlock_max1_carry_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_5_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_6_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_7_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_i_8_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_0 : STD_LOGIC;
  signal time_tlock_max1_carry_n_1 : STD_LOGIC;
  signal time_tlock_max1_carry_n_2 : STD_LOGIC;
  signal time_tlock_max1_carry_n_3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \wait_time_cnt0__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \wait_time_cnt[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3__0_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_time_tlock_max1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[2]_i_2\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_10\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \FSM_sequential_rx_state[3]_i_9\ : label is "soft_lutpair43";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[0]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[1]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[2]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_rx_state_reg[3]\ : label is "release_pll_reset:0011,verify_recclk_stable:0100,wait_for_pll_lock:0010,fsm_done:1010,assert_all_resets:0001,init:0000,wait_reset_done:0111,monitor_data_valid:1001,wait_for_rxusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of check_tlock_max_i_1 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1__0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2__0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3__0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1__0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3__0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of reset_time_out_i_3 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of reset_time_out_i_4 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_5 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of rx_fsm_reset_done_int_i_6 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of time_out_1us_i_2 : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of time_out_2ms_i_1 : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \time_out_2ms_i_3__0\ : label is "soft_lutpair51";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of time_tlock_max1_carry : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__0\ : label is 11;
  attribute COMPARATOR_THRESHOLD of \time_tlock_max1_carry__1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_1 : label is "soft_lutpair52";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1__0\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1__0\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1__0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1__0\ : label is "soft_lutpair45";
begin
  data_in <= \^data_in\;
  gt0_rxuserrdy_t <= \^gt0_rxuserrdy_t\;
\FSM_sequential_rx_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222AAAA00000C00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(3),
      I3 => time_tlock_max,
      I4 => reset_time_out_reg_n_0,
      I5 => rx_state(1),
      O => \FSM_sequential_rx_state[0]_i_2_n_0\
    );
\FSM_sequential_rx_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AABF000F0000"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      I2 => rx_state(2),
      I3 => rx_state(3),
      I4 => rx_state(1),
      I5 => rx_state(0),
      O => \FSM_sequential_rx_state[1]_i_3_n_0\
    );
\FSM_sequential_rx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050FF2200"
    )
        port map (
      I0 => rx_state(1),
      I1 => time_out_2ms_reg_n_0,
      I2 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I3 => rx_state(0),
      I4 => rx_state(2),
      I5 => rx_state(3),
      O => \rx_state__0\(2)
    );
\FSM_sequential_rx_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_tlock_max,
      O => \FSM_sequential_rx_state[2]_i_2_n_0\
    );
\FSM_sequential_rx_state[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out_reg_n_0,
      I1 => time_out_2ms_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_10_n_0\
    );
\FSM_sequential_rx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050005300"
    )
        port map (
      I0 => \FSM_sequential_rx_state[3]_i_10_n_0\,
      I1 => \wait_time_cnt[6]_i_4__0_n_0\,
      I2 => rx_state(0),
      I3 => rx_state(1),
      I4 => wait_time_cnt_reg(6),
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_3_n_0\
    );
\FSM_sequential_rx_state[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000023002F00"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => rx_state(2),
      I2 => rx_state(1),
      I3 => rx_state(0),
      I4 => \FSM_sequential_rx_state[2]_i_2_n_0\,
      I5 => rx_state(3),
      O => \FSM_sequential_rx_state[3]_i_7_n_0\
    );
\FSM_sequential_rx_state[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80800080"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => time_out_2ms_reg_n_0,
      I4 => reset_time_out_reg_n_0,
      O => \FSM_sequential_rx_state[3]_i_9_n_0\
    );
\FSM_sequential_rx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(0),
      Q => rx_state(0),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(1),
      Q => rx_state(1),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(2),
      Q => rx_state(2),
      R => \out\(0)
    );
\FSM_sequential_rx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_data_valid_n_5,
      D => \rx_state__0\(3),
      Q => rx_state(3),
      R => \out\(0)
    );
RXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB4000"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(0),
      I2 => rx_state(2),
      I3 => rx_state(1),
      I4 => \^gt0_rxuserrdy_t\,
      O => RXUSERRDY_i_1_n_0
    );
RXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => RXUSERRDY_i_1_n_0,
      Q => \^gt0_rxuserrdy_t\,
      R => \out\(0)
    );
check_tlock_max_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(0),
      I2 => rx_state(1),
      I3 => rx_state(3),
      I4 => check_tlock_max_reg_n_0,
      O => check_tlock_max_i_1_n_0
    );
check_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => check_tlock_max_i_1_n_0,
      Q => check_tlock_max_reg_n_0,
      R => \out\(0)
    );
gtrxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(1),
      I2 => rx_state(2),
      I3 => rx_state(0),
      I4 => GTRXRESET,
      O => gtrxreset_i_i_1_n_0
    );
gtrxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gtrxreset_i_i_1_n_0,
      Q => GTRXRESET,
      R => \out\(0)
    );
gtxe2_i_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTRXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => SR(0)
    );
\init_wait_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1__0_n_0\
    );
\init_wait_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__2\(1)
    );
\init_wait_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__2\(2)
    );
\init_wait_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__2\(3)
    );
\init_wait_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__2\(4)
    );
\init_wait_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__2\(5)
    );
\init_wait_count[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1__0_n_0\
    );
\init_wait_count[6]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__2\(6)
    );
\init_wait_count[6]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3__0_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1__0_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1__0_n_0\,
      CLR => \out\(0),
      D => \p_0_in__2\(6),
      Q => init_wait_count_reg(6)
    );
\init_wait_done_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3__0_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => \init_wait_done_i_1__0_n_0\
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => \init_wait_done_i_1__0_n_0\,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__3\(0)
    );
\mmcm_lock_count[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__3\(1)
    );
\mmcm_lock_count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1__0_n_0\
    );
\mmcm_lock_count[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1__0_n_0\
    );
\mmcm_lock_count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1__0_n_0\
    );
\mmcm_lock_count[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1__0_n_0\
    );
\mmcm_lock_count[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1__0_n_0\
    );
\mmcm_lock_count[7]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2__0_n_0\
    );
\mmcm_lock_count[7]_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3__0_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \p_0_in__3\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[2]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[3]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[4]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[5]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[6]_i_1__0_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2__0_n_0\,
      D => \mmcm_lock_count[7]_i_3__0_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => \mmcm_lock_reclocked_i_2__0_n_0\,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
\mmcm_lock_reclocked_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_reclocked_i_2__0_n_0\
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
reset_time_out_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      O => reset_time_out_i_3_n_0
    );
reset_time_out_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"34347674"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(3),
      I2 => rx_state(0),
      I3 => \FSM_sequential_rx_state_reg[0]_0\,
      I4 => rx_state(1),
      O => reset_time_out_i_4_n_0
    );
reset_time_out_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_0,
      Q => reset_time_out_reg_n_0,
      S => \out\(0)
    );
\run_phase_alignment_int_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFF0010"
    )
        port map (
      I0 => rx_state(2),
      I1 => rx_state(1),
      I2 => rx_state(3),
      I3 => rx_state(0),
      I4 => run_phase_alignment_int_reg_n_0,
      O => \run_phase_alignment_int_i_1__0_n_0\
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => \run_phase_alignment_int_i_1__0_n_0\,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => data_out_0,
      Q => run_phase_alignment_int_s3_reg_n_0,
      R => '0'
    );
rx_fsm_reset_done_int_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => rx_state(1),
      I1 => rx_state(0),
      O => rx_fsm_reset_done_int_i_5_n_0
    );
rx_fsm_reset_done_int_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => rx_state(3),
      I1 => rx_state(2),
      O => rx_fsm_reset_done_int_i_6_n_0
    );
rx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_data_valid_n_1,
      Q => \^data_in\,
      R => \out\(0)
    );
rx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => rx_fsm_reset_done_int_s2,
      Q => rx_fsm_reset_done_int_s3,
      R => '0'
    );
rxresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => rxresetdone_s2,
      Q => rxresetdone_s3,
      R => '0'
    );
sync_RXRESETDONE: entity work.gig_ethernet_pcs_pma_0_sync_block_10
     port map (
      data_out => rxresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.gig_ethernet_pcs_pma_0_sync_block_11
     port map (
      \FSM_sequential_rx_state_reg[1]\ => sync_cplllock_n_0,
      Q(2 downto 0) => rx_state(3 downto 1),
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg,
      rxresetdone_s3 => rxresetdone_s3
    );
sync_data_valid: entity work.gig_ethernet_pcs_pma_0_sync_block_12
     port map (
      D(2) => \rx_state__0\(3),
      D(1 downto 0) => \rx_state__0\(1 downto 0),
      E(0) => sync_data_valid_n_5,
      \FSM_sequential_rx_state_reg[0]\ => \FSM_sequential_rx_state[3]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[0]_0\ => \FSM_sequential_rx_state[3]_i_7_n_0\,
      \FSM_sequential_rx_state_reg[0]_1\ => \FSM_sequential_rx_state_reg[0]_0\,
      \FSM_sequential_rx_state_reg[0]_2\ => \FSM_sequential_rx_state[0]_i_2_n_0\,
      \FSM_sequential_rx_state_reg[0]_3\ => init_wait_done_reg_n_0,
      \FSM_sequential_rx_state_reg[1]\ => sync_data_valid_n_0,
      \FSM_sequential_rx_state_reg[1]_0\ => \FSM_sequential_rx_state[1]_i_3_n_0\,
      \FSM_sequential_rx_state_reg[3]\ => \FSM_sequential_rx_state[3]_i_9_n_0\,
      Q(3 downto 0) => rx_state(3 downto 0),
      data_in => \^data_in\,
      data_out => data_out,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => reset_time_out_i_3_n_0,
      reset_time_out_reg_1 => reset_time_out_i_4_n_0,
      reset_time_out_reg_2 => reset_time_out_reg_n_0,
      rx_fsm_reset_done_int_reg => sync_data_valid_n_1,
      rx_fsm_reset_done_int_reg_0 => rx_fsm_reset_done_int_i_5_n_0,
      rx_fsm_reset_done_int_reg_1 => time_out_100us_reg_n_0,
      rx_fsm_reset_done_int_reg_2 => time_out_1us_reg_n_0,
      rx_fsm_reset_done_int_reg_3 => rx_fsm_reset_done_int_i_6_n_0,
      time_out_wait_bypass_s3 => time_out_wait_bypass_s3
    );
sync_mmcm_lock_reclocked: entity work.gig_ethernet_pcs_pma_0_sync_block_13
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.gig_ethernet_pcs_pma_0_sync_block_14
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out_0,
      data_sync_reg1_0 => data_sync_reg6
    );
sync_time_out_wait_bypass: entity work.gig_ethernet_pcs_pma_0_sync_block_15
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.gig_ethernet_pcs_pma_0_sync_block_16
     port map (
      data_in => \^data_in\,
      data_out => rx_fsm_reset_done_int_s2,
      data_sync_reg6_0 => data_sync_reg6
    );
time_out_100us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000100"
    )
        port map (
      I0 => time_out_2ms_i_4_n_0,
      I1 => time_out_counter_reg(17),
      I2 => time_out_counter_reg(16),
      I3 => time_out_100us_i_2_n_0,
      I4 => time_out_100us_i_3_n_0,
      I5 => time_out_100us_reg_n_0,
      O => time_out_100us_i_1_n_0
    );
time_out_100us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(14),
      O => time_out_100us_i_2_n_0
    );
time_out_100us_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(0),
      I2 => time_out_counter_reg(1),
      I3 => time_out_counter_reg(15),
      I4 => time_out_counter_reg(13),
      O => time_out_100us_i_3_n_0
    );
time_out_100us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_100us_i_1_n_0,
      Q => time_out_100us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_1us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00100000"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => time_out_1us_i_2_n_0,
      I2 => time_out_counter_reg(3),
      I3 => time_out_counter_reg(2),
      I4 => time_out_1us_i_3_n_0,
      I5 => time_out_1us_reg_n_0,
      O => time_out_1us_i_1_n_0
    );
time_out_1us_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => time_out_1us_i_2_n_0
    );
time_out_1us_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(11),
      I2 => time_out_counter_reg(6),
      I3 => time_out_counter_reg(8),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(12),
      O => time_out_1us_i_3_n_0
    );
time_out_1us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_1us_i_1_n_0,
      Q => time_out_1us_reg_n_0,
      R => reset_time_out_reg_n_0
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF01"
    )
        port map (
      I0 => time_out_2ms_i_2_n_0,
      I1 => \time_out_2ms_i_3__0_n_0\,
      I2 => time_out_2ms_i_4_n_0,
      I3 => time_out_2ms_reg_n_0,
      O => time_out_2ms_i_1_n_0
    );
time_out_2ms_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(14),
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(7),
      I4 => time_out_100us_i_3_n_0,
      O => time_out_2ms_i_2_n_0
    );
\time_out_2ms_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(18),
      I3 => time_out_counter_reg(17),
      O => \time_out_2ms_i_3__0_n_0\
    );
time_out_2ms_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFFFFFF"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(9),
      I4 => time_out_counter_reg(11),
      I5 => time_out_counter_reg(6),
      O => time_out_2ms_i_4_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => reset_time_out_reg_n_0
    );
\time_out_counter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(16),
      I3 => time_out_counter_reg(12),
      I4 => time_out_2ms_i_2_n_0,
      I5 => time_out_2ms_i_4_n_0,
      O => time_out_counter
    );
\time_out_counter[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2__0_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2__0_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2__0_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2__0_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2__0_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_3_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1__0_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[16]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1__0_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1__0_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1__0_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2__0_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2__0_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1__0_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out_reg_n_0
    );
\time_out_counter_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1__0_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1__0_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1__0_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1__0_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1__0_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1__0_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1__0_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1__0_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out_reg_n_0
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => rx_fsm_reset_done_int_s3,
      I2 => \time_out_wait_bypass_i_2__0_n_0\,
      I3 => run_phase_alignment_int_s3_reg_n_0,
      O => time_out_wait_bypass_i_1_n_0
    );
\time_out_wait_bypass_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \time_out_wait_bypass_i_3__0_n_0\,
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(11),
      I3 => wait_bypass_count_reg(0),
      I4 => \time_out_wait_bypass_i_4__0_n_0\,
      O => \time_out_wait_bypass_i_2__0_n_0\
    );
\time_out_wait_bypass_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(9),
      I1 => wait_bypass_count_reg(4),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(2),
      O => \time_out_wait_bypass_i_3__0_n_0\
    );
\time_out_wait_bypass_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => wait_bypass_count_reg(5),
      I1 => wait_bypass_count_reg(7),
      I2 => wait_bypass_count_reg(3),
      I3 => wait_bypass_count_reg(6),
      I4 => wait_bypass_count_reg(10),
      I5 => wait_bypass_count_reg(8),
      O => \time_out_wait_bypass_i_4__0_n_0\
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => time_tlock_max1_carry_n_0,
      CO(2) => time_tlock_max1_carry_n_1,
      CO(1) => time_tlock_max1_carry_n_2,
      CO(0) => time_tlock_max1_carry_n_3,
      CYINIT => '0',
      DI(3) => time_tlock_max1_carry_i_1_n_0,
      DI(2) => time_tlock_max1_carry_i_2_n_0,
      DI(1) => time_tlock_max1_carry_i_3_n_0,
      DI(0) => time_tlock_max1_carry_i_4_n_0,
      O(3 downto 0) => NLW_time_tlock_max1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => time_tlock_max1_carry_i_5_n_0,
      S(2) => time_tlock_max1_carry_i_6_n_0,
      S(1) => time_tlock_max1_carry_i_7_n_0,
      S(0) => time_tlock_max1_carry_i_8_n_0
    );
\time_tlock_max1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => time_tlock_max1_carry_n_0,
      CO(3) => \time_tlock_max1_carry__0_n_0\,
      CO(2) => \time_tlock_max1_carry__0_n_1\,
      CO(1) => \time_tlock_max1_carry__0_n_2\,
      CO(0) => \time_tlock_max1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => time_out_counter_reg(15),
      DI(2) => \time_tlock_max1_carry__0_i_1_n_0\,
      DI(1) => '0',
      DI(0) => \time_tlock_max1_carry__0_i_2_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \time_tlock_max1_carry__0_i_3_n_0\,
      S(2) => \time_tlock_max1_carry__0_i_4_n_0\,
      S(1) => \time_tlock_max1_carry__0_i_5_n_0\,
      S(0) => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(12),
      I1 => time_out_counter_reg(13),
      O => \time_tlock_max1_carry__0_i_1_n_0\
    );
\time_tlock_max1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(8),
      I1 => time_out_counter_reg(9),
      O => \time_tlock_max1_carry__0_i_2_n_0\
    );
\time_tlock_max1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(15),
      O => \time_tlock_max1_carry__0_i_3_n_0\
    );
\time_tlock_max1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(13),
      I1 => time_out_counter_reg(12),
      O => \time_tlock_max1_carry__0_i_4_n_0\
    );
\time_tlock_max1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(10),
      I1 => time_out_counter_reg(11),
      O => \time_tlock_max1_carry__0_i_5_n_0\
    );
\time_tlock_max1_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(9),
      I1 => time_out_counter_reg(8),
      O => \time_tlock_max1_carry__0_i_6_n_0\
    );
\time_tlock_max1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_tlock_max1_carry__0_n_0\,
      CO(3 downto 2) => \NLW_time_tlock_max1_carry__1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => time_tlock_max1,
      CO(0) => \time_tlock_max1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => time_out_counter_reg(18),
      DI(0) => \time_tlock_max1_carry__1_i_1_n_0\,
      O(3 downto 0) => \NLW_time_tlock_max1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \time_tlock_max1_carry__1_i_2_n_0\,
      S(0) => \time_tlock_max1_carry__1_i_3_n_0\
    );
\time_tlock_max1_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(17),
      O => \time_tlock_max1_carry__1_i_1_n_0\
    );
\time_tlock_max1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(18),
      O => \time_tlock_max1_carry__1_i_2_n_0\
    );
\time_tlock_max1_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(16),
      O => \time_tlock_max1_carry__1_i_3_n_0\
    );
time_tlock_max1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(6),
      I1 => time_out_counter_reg(7),
      O => time_tlock_max1_carry_i_1_n_0
    );
time_tlock_max1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(5),
      O => time_tlock_max1_carry_i_2_n_0
    );
time_tlock_max1_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(2),
      I1 => time_out_counter_reg(3),
      O => time_tlock_max1_carry_i_3_n_0
    );
time_tlock_max1_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(1),
      O => time_tlock_max1_carry_i_4_n_0
    );
time_tlock_max1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(6),
      O => time_tlock_max1_carry_i_5_n_0
    );
time_tlock_max1_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_counter_reg(5),
      I1 => time_out_counter_reg(4),
      O => time_tlock_max1_carry_i_6_n_0
    );
time_tlock_max1_carry_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(3),
      I1 => time_out_counter_reg(2),
      O => time_tlock_max1_carry_i_7_n_0
    );
time_tlock_max1_carry_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(1),
      I1 => time_out_counter_reg(0),
      O => time_tlock_max1_carry_i_8_n_0
    );
time_tlock_max_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => check_tlock_max_reg_n_0,
      I1 => time_tlock_max1,
      I2 => time_tlock_max,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max,
      R => reset_time_out_reg_n_0
    );
\wait_bypass_count[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3_reg_n_0,
      O => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count[0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \time_out_wait_bypass_i_2__0_n_0\,
      I1 => rx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2__0_n_0\
    );
\wait_bypass_count[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      Q => wait_bypass_count_reg(0),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[0]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3__0_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3__0_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3__0_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(10),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(11),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(12),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[12]_i_1__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(12)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_6\,
      Q => wait_bypass_count_reg(1),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_5\,
      Q => wait_bypass_count_reg(2),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[0]_i_3__0_n_4\,
      Q => wait_bypass_count_reg(3),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(4),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3__0_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(5),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_5\,
      Q => wait_bypass_count_reg(6),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[4]_i_1__0_n_4\,
      Q => wait_bypass_count_reg(7),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      Q => wait_bypass_count_reg(8),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_bypass_count_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1__0_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1__0_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1__0_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1__0_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1__0_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1__0_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg6,
      CE => \wait_bypass_count[0]_i_2__0_n_0\,
      D => \wait_bypass_count_reg[8]_i_1__0_n_6\,
      Q => wait_bypass_count_reg(9),
      R => \wait_bypass_count[0]_i_1__0_n_0\
    );
\wait_time_cnt[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => \wait_time_cnt0__0\(0)
    );
\wait_time_cnt[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1__0_n_0\
    );
\wait_time_cnt[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1__0_n_0\
    );
\wait_time_cnt[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1__0_n_0\
    );
\wait_time_cnt[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1__0_n_0\
    );
\wait_time_cnt[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1__0_n_0\
    );
\wait_time_cnt[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => rx_state(0),
      I1 => rx_state(1),
      I2 => rx_state(3),
      O => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt[6]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_2__0_n_0\
    );
\wait_time_cnt[6]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4__0_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3__0_n_0\
    );
\wait_time_cnt[6]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4__0_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt0__0\(0),
      Q => wait_time_cnt_reg(0),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[1]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(1),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[2]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(2),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[3]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(3),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[4]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(4),
      R => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[5]_i_1__0_n_0\,
      Q => wait_time_cnt_reg(5),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \wait_time_cnt[6]_i_2__0_n_0\,
      D => \wait_time_cnt[6]_i_3__0_n_0\,
      Q => wait_time_cnt_reg(6),
      S => \wait_time_cnt[6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  port (
    mmcm_reset : out STD_LOGIC;
    gt0_cpllreset_t : out STD_LOGIC;
    data_in : out STD_LOGIC;
    gt0_txuserrdy_t : out STD_LOGIC;
    gt0_gttxreset_in0_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : in STD_LOGIC;
    gt0_cpllrefclklost_i : in STD_LOGIC;
    data_sync_reg1_0 : in STD_LOGIC;
    data_sync_reg1_1 : in STD_LOGIC;
    data_sync_reg1_2 : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_TX_STARTUP_FSM;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_TX_STARTUP_FSM is
  signal CPLL_RESET_i_1_n_0 : STD_LOGIC;
  signal CPLL_RESET_i_2_n_0 : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_tx_state[3]_i_8_n_0\ : STD_LOGIC;
  signal GTTXRESET : STD_LOGIC;
  signal MMCM_RESET_i_1_n_0 : STD_LOGIC;
  signal TXUSERRDY_i_1_n_0 : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal \^data_in\ : STD_LOGIC;
  signal data_out : STD_LOGIC;
  signal \^gt0_cpllreset_t\ : STD_LOGIC;
  signal \^gt0_txuserrdy_t\ : STD_LOGIC;
  signal gttxreset_i_i_1_n_0 : STD_LOGIC;
  signal \init_wait_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \init_wait_count[6]_i_3_n_0\ : STD_LOGIC;
  signal init_wait_count_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal init_wait_done_i_1_n_0 : STD_LOGIC;
  signal init_wait_done_reg_n_0 : STD_LOGIC;
  signal \mmcm_lock_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[3]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[4]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[5]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[6]_i_1_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_2_n_0\ : STD_LOGIC;
  signal \mmcm_lock_count[7]_i_3_n_0\ : STD_LOGIC;
  signal mmcm_lock_count_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal mmcm_lock_i : STD_LOGIC;
  signal mmcm_lock_reclocked : STD_LOGIC;
  signal mmcm_lock_reclocked_i_1_n_0 : STD_LOGIC;
  signal mmcm_lock_reclocked_i_2_n_0 : STD_LOGIC;
  signal \^mmcm_reset\ : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \p_0_in__1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pll_reset_asserted_i_1_n_0 : STD_LOGIC;
  signal pll_reset_asserted_i_2_n_0 : STD_LOGIC;
  signal pll_reset_asserted_reg_n_0 : STD_LOGIC;
  signal refclk_stable_count : STD_LOGIC;
  signal \refclk_stable_count[0]_i_3_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_4_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_5_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_6_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_7_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_8_n_0\ : STD_LOGIC;
  signal \refclk_stable_count[0]_i_9_n_0\ : STD_LOGIC;
  signal refclk_stable_count_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \refclk_stable_count_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \refclk_stable_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal refclk_stable_i_1_n_0 : STD_LOGIC;
  signal refclk_stable_i_2_n_0 : STD_LOGIC;
  signal refclk_stable_i_3_n_0 : STD_LOGIC;
  signal refclk_stable_i_4_n_0 : STD_LOGIC;
  signal refclk_stable_i_5_n_0 : STD_LOGIC;
  signal refclk_stable_i_6_n_0 : STD_LOGIC;
  signal refclk_stable_reg_n_0 : STD_LOGIC;
  signal reset_time_out : STD_LOGIC;
  signal \reset_time_out_i_2__0_n_0\ : STD_LOGIC;
  signal run_phase_alignment_int_i_1_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_reg_n_0 : STD_LOGIC;
  signal run_phase_alignment_int_s3 : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal sync_cplllock_n_0 : STD_LOGIC;
  signal sync_cplllock_n_1 : STD_LOGIC;
  signal sync_mmcm_lock_reclocked_n_0 : STD_LOGIC;
  signal time_out_2ms_i_1_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_2__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_3_n_0 : STD_LOGIC;
  signal \time_out_2ms_i_4__0_n_0\ : STD_LOGIC;
  signal time_out_2ms_i_5_n_0 : STD_LOGIC;
  signal time_out_2ms_reg_n_0 : STD_LOGIC;
  signal time_out_500us_i_1_n_0 : STD_LOGIC;
  signal time_out_500us_i_2_n_0 : STD_LOGIC;
  signal time_out_500us_reg_n_0 : STD_LOGIC;
  signal time_out_counter : STD_LOGIC;
  signal \time_out_counter[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \time_out_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal time_out_counter_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \time_out_counter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \time_out_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal time_out_wait_bypass_i_1_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_2_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_3_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_4_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_i_5_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_reg_n_0 : STD_LOGIC;
  signal time_out_wait_bypass_s2 : STD_LOGIC;
  signal time_out_wait_bypass_s3 : STD_LOGIC;
  signal time_tlock_max_i_1_n_0 : STD_LOGIC;
  signal time_tlock_max_i_2_n_0 : STD_LOGIC;
  signal time_tlock_max_i_3_n_0 : STD_LOGIC;
  signal time_tlock_max_i_4_n_0 : STD_LOGIC;
  signal time_tlock_max_reg_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_i_1_n_0 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s2 : STD_LOGIC;
  signal tx_fsm_reset_done_int_s3 : STD_LOGIC;
  signal tx_state : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \tx_state__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal txresetdone_s2 : STD_LOGIC;
  signal txresetdone_s3 : STD_LOGIC;
  signal \wait_bypass_count[0]_i_2_n_0\ : STD_LOGIC;
  signal \wait_bypass_count[0]_i_4__0_n_0\ : STD_LOGIC;
  signal wait_bypass_count_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \wait_bypass_count_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wait_bypass_count_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal wait_time_cnt0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal wait_time_cnt0_0 : STD_LOGIC;
  signal \wait_time_cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_3_n_0\ : STD_LOGIC;
  signal \wait_time_cnt[6]_i_4_n_0\ : STD_LOGIC;
  signal wait_time_cnt_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of CPLL_RESET_i_2 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[0]_i_3\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[1]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_2\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_7\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \FSM_sequential_tx_state[3]_i_8\ : label is "soft_lutpair59";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[0]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[1]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[2]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_tx_state_reg[3]\ : label is "wait_for_txoutclk:0100,release_pll_reset:0011,wait_for_pll_lock:0010,assert_all_resets:0001,init:0000,wait_reset_done:0111,reset_fsm_done:1001,wait_for_txusrclk:0110,do_phase_alignment:1000,release_mmcm_reset:0101";
  attribute SOFT_HLUTNM of MMCM_RESET_i_1 : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of TXUSERRDY_i_1 : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of gttxreset_i_i_1 : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \init_wait_count[1]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[2]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \init_wait_count[3]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[4]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_2\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \init_wait_count[6]_i_3\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \mmcm_lock_count[1]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[2]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \mmcm_lock_count[3]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[4]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \mmcm_lock_count[6]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \mmcm_lock_count[7]_i_3\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of pll_reset_asserted_i_2 : label is "soft_lutpair60";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[20]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[24]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[28]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \refclk_stable_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of run_phase_alignment_int_i_1 : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \time_out_2ms_i_4__0\ : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \time_out_counter_reg[0]_i_2\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \time_out_counter_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of time_tlock_max_i_4 : label is "soft_lutpair56";
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[0]_i_3\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \wait_bypass_count_reg[8]_i_1\ : label is 11;
  attribute SOFT_HLUTNM of \wait_time_cnt[0]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[1]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \wait_time_cnt[3]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \wait_time_cnt[4]_i_1\ : label is "soft_lutpair63";
begin
  data_in <= \^data_in\;
  gt0_cpllreset_t <= \^gt0_cpllreset_t\;
  gt0_txuserrdy_t <= \^gt0_txuserrdy_t\;
  mmcm_reset <= \^mmcm_reset\;
CPLL_RESET_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF1F0000001F"
    )
        port map (
      I0 => pll_reset_asserted_reg_n_0,
      I1 => gt0_cpllrefclklost_i,
      I2 => refclk_stable_reg_n_0,
      I3 => CPLL_RESET_i_2_n_0,
      I4 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I5 => \^gt0_cpllreset_t\,
      O => CPLL_RESET_i_1_n_0
    );
CPLL_RESET_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      O => CPLL_RESET_i_2_n_0
    );
CPLL_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => CPLL_RESET_i_1_n_0,
      Q => \^gt0_cpllreset_t\,
      R => \out\(0)
    );
\FSM_sequential_tx_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F3FFF3F0F5F0F5F0"
    )
        port map (
      I0 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I1 => \FSM_sequential_tx_state[0]_i_2_n_0\,
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => tx_state(2),
      I4 => time_out_2ms_reg_n_0,
      I5 => tx_state(1),
      O => \tx_state__0\(0)
    );
\FSM_sequential_tx_state[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => reset_time_out,
      I1 => time_out_500us_reg_n_0,
      O => \FSM_sequential_tx_state[0]_i_2_n_0\
    );
\FSM_sequential_tx_state[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      O => \FSM_sequential_tx_state[0]_i_3_n_0\
    );
\FSM_sequential_tx_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"005A001A"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      O => \tx_state__0\(1)
    );
\FSM_sequential_tx_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04000C0C06020C0C"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => \FSM_sequential_tx_state[2]_i_2_n_0\,
      I4 => tx_state(0),
      I5 => time_out_2ms_reg_n_0,
      O => \tx_state__0\(2)
    );
\FSM_sequential_tx_state[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => reset_time_out,
      I2 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[2]_i_2_n_0\
    );
\FSM_sequential_tx_state[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4FF4444"
    )
        port map (
      I0 => time_out_wait_bypass_s3,
      I1 => tx_state(3),
      I2 => reset_time_out,
      I3 => time_out_500us_reg_n_0,
      I4 => \FSM_sequential_tx_state[3]_i_8_n_0\,
      O => \tx_state__0\(3)
    );
\FSM_sequential_tx_state[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00BA000000000000"
    )
        port map (
      I0 => txresetdone_s3,
      I1 => reset_time_out,
      I2 => time_out_500us_reg_n_0,
      I3 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I4 => tx_state(2),
      I5 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_3_n_0\
    );
\FSM_sequential_tx_state[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000300FF00AA"
    )
        port map (
      I0 => init_wait_done_reg_n_0,
      I1 => \wait_time_cnt[6]_i_4_n_0\,
      I2 => wait_time_cnt_reg(6),
      I3 => tx_state(0),
      I4 => tx_state(3),
      I5 => CPLL_RESET_i_2_n_0,
      O => \FSM_sequential_tx_state[3]_i_4_n_0\
    );
\FSM_sequential_tx_state[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040400040000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => \FSM_sequential_tx_state[0]_i_3_n_0\,
      I3 => reset_time_out,
      I4 => time_tlock_max_reg_n_0,
      I5 => mmcm_lock_reclocked,
      O => \FSM_sequential_tx_state[3]_i_6_n_0\
    );
\FSM_sequential_tx_state[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(3),
      I2 => tx_state(0),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_7_n_0\
    );
\FSM_sequential_tx_state[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      O => \FSM_sequential_tx_state[3]_i_8_n_0\
    );
\FSM_sequential_tx_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(0),
      Q => tx_state(0),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(1),
      Q => tx_state(1),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(2),
      Q => tx_state(2),
      R => \out\(0)
    );
\FSM_sequential_tx_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sync_cplllock_n_1,
      D => \tx_state__0\(3),
      Q => tx_state(3),
      R => \out\(0)
    );
MMCM_RESET_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF70004"
    )
        port map (
      I0 => tx_state(2),
      I1 => tx_state(0),
      I2 => tx_state(3),
      I3 => tx_state(1),
      I4 => \^mmcm_reset\,
      O => MMCM_RESET_i_1_n_0
    );
MMCM_RESET_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => MMCM_RESET_i_1_n_0,
      Q => \^mmcm_reset\,
      R => \out\(0)
    );
TXUSERRDY_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFD2000"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => \^gt0_txuserrdy_t\,
      O => TXUSERRDY_i_1_n_0
    );
TXUSERRDY_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => TXUSERRDY_i_1_n_0,
      Q => \^gt0_txuserrdy_t\,
      R => \out\(0)
    );
gttxreset_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0100"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(1),
      I2 => tx_state(2),
      I3 => tx_state(0),
      I4 => GTTXRESET,
      O => gttxreset_i_i_1_n_0
    );
gttxreset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gttxreset_i_i_1_n_0,
      Q => GTTXRESET,
      R => \out\(0)
    );
gtxe2_i_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => GTTXRESET,
      I1 => \^data_in\,
      I2 => gtxe2_i,
      O => gt0_gttxreset_in0_out
    );
\init_wait_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => init_wait_count_reg(0),
      O => \init_wait_count[0]_i_1_n_0\
    );
\init_wait_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      O => \p_0_in__0\(1)
    );
\init_wait_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => init_wait_count_reg(0),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      O => \p_0_in__0\(2)
    );
\init_wait_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => init_wait_count_reg(1),
      I1 => init_wait_count_reg(2),
      I2 => init_wait_count_reg(0),
      I3 => init_wait_count_reg(3),
      O => \p_0_in__0\(3)
    );
\init_wait_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      O => \p_0_in__0\(4)
    );
\init_wait_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => init_wait_count_reg(2),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(3),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_count_reg(4),
      I5 => init_wait_count_reg(5),
      O => \p_0_in__0\(5)
    );
\init_wait_count[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \init_wait_count[6]_i_1_n_0\
    );
\init_wait_count[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(0),
      I2 => init_wait_count_reg(4),
      I3 => init_wait_count_reg(6),
      O => \p_0_in__0\(6)
    );
\init_wait_count[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => init_wait_count_reg(3),
      I1 => init_wait_count_reg(1),
      I2 => init_wait_count_reg(2),
      I3 => init_wait_count_reg(5),
      O => \init_wait_count[6]_i_3_n_0\
    );
\init_wait_count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \init_wait_count[0]_i_1_n_0\,
      Q => init_wait_count_reg(0)
    );
\init_wait_count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(1),
      Q => init_wait_count_reg(1)
    );
\init_wait_count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(2),
      Q => init_wait_count_reg(2)
    );
\init_wait_count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(3),
      Q => init_wait_count_reg(3)
    );
\init_wait_count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(4),
      Q => init_wait_count_reg(4)
    );
\init_wait_count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(5),
      Q => init_wait_count_reg(5)
    );
\init_wait_count_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \init_wait_count[6]_i_1_n_0\,
      CLR => \out\(0),
      D => \p_0_in__0\(6),
      Q => init_wait_count_reg(6)
    );
init_wait_done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0010"
    )
        port map (
      I0 => \init_wait_count[6]_i_3_n_0\,
      I1 => init_wait_count_reg(4),
      I2 => init_wait_count_reg(6),
      I3 => init_wait_count_reg(0),
      I4 => init_wait_done_reg_n_0,
      O => init_wait_done_i_1_n_0
    );
init_wait_done_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      CLR => \out\(0),
      D => init_wait_done_i_1_n_0,
      Q => init_wait_done_reg_n_0
    );
\mmcm_lock_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      O => \p_0_in__1\(0)
    );
\mmcm_lock_count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mmcm_lock_count_reg(0),
      I1 => mmcm_lock_count_reg(1),
      O => \p_0_in__1\(1)
    );
\mmcm_lock_count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => mmcm_lock_count_reg(1),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(2),
      O => \mmcm_lock_count[2]_i_1_n_0\
    );
\mmcm_lock_count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => mmcm_lock_count_reg(2),
      I1 => mmcm_lock_count_reg(0),
      I2 => mmcm_lock_count_reg(1),
      I3 => mmcm_lock_count_reg(3),
      O => \mmcm_lock_count[3]_i_1_n_0\
    );
\mmcm_lock_count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => mmcm_lock_count_reg(3),
      I1 => mmcm_lock_count_reg(1),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(2),
      I4 => mmcm_lock_count_reg(4),
      O => \mmcm_lock_count[4]_i_1_n_0\
    );
\mmcm_lock_count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => \mmcm_lock_count[5]_i_1_n_0\
    );
\mmcm_lock_count[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      O => \mmcm_lock_count[6]_i_1_n_0\
    );
\mmcm_lock_count[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => mmcm_lock_reclocked_i_2_n_0,
      I1 => mmcm_lock_count_reg(6),
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_2_n_0\
    );
\mmcm_lock_count[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => mmcm_lock_count_reg(6),
      I1 => mmcm_lock_reclocked_i_2_n_0,
      I2 => mmcm_lock_count_reg(7),
      O => \mmcm_lock_count[7]_i_3_n_0\
    );
\mmcm_lock_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(0),
      Q => mmcm_lock_count_reg(0),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \p_0_in__1\(1),
      Q => mmcm_lock_count_reg(1),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[2]_i_1_n_0\,
      Q => mmcm_lock_count_reg(2),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[3]_i_1_n_0\,
      Q => mmcm_lock_count_reg(3),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[4]_i_1_n_0\,
      Q => mmcm_lock_count_reg(4),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[5]_i_1_n_0\,
      Q => mmcm_lock_count_reg(5),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[6]_i_1_n_0\,
      Q => mmcm_lock_count_reg(6),
      R => sync_mmcm_lock_reclocked_n_0
    );
\mmcm_lock_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => \mmcm_lock_count[7]_i_2_n_0\,
      D => \mmcm_lock_count[7]_i_3_n_0\,
      Q => mmcm_lock_count_reg(7),
      R => sync_mmcm_lock_reclocked_n_0
    );
mmcm_lock_reclocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEA0000"
    )
        port map (
      I0 => mmcm_lock_reclocked,
      I1 => mmcm_lock_count_reg(7),
      I2 => mmcm_lock_count_reg(6),
      I3 => mmcm_lock_reclocked_i_2_n_0,
      I4 => mmcm_lock_i,
      O => mmcm_lock_reclocked_i_1_n_0
    );
mmcm_lock_reclocked_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mmcm_lock_count_reg(4),
      I1 => mmcm_lock_count_reg(2),
      I2 => mmcm_lock_count_reg(0),
      I3 => mmcm_lock_count_reg(1),
      I4 => mmcm_lock_count_reg(3),
      I5 => mmcm_lock_count_reg(5),
      O => mmcm_lock_reclocked_i_2_n_0
    );
mmcm_lock_reclocked_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => mmcm_lock_reclocked_i_1_n_0,
      Q => mmcm_lock_reclocked,
      R => '0'
    );
pll_reset_asserted_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000CD55CCCCCCCC"
    )
        port map (
      I0 => tx_state(3),
      I1 => pll_reset_asserted_reg_n_0,
      I2 => gt0_cpllrefclklost_i,
      I3 => refclk_stable_reg_n_0,
      I4 => tx_state(1),
      I5 => pll_reset_asserted_i_2_n_0,
      O => pll_reset_asserted_i_1_n_0
    );
pll_reset_asserted_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => tx_state(0),
      I1 => tx_state(3),
      I2 => tx_state(2),
      O => pll_reset_asserted_i_2_n_0
    );
pll_reset_asserted_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => pll_reset_asserted_i_1_n_0,
      Q => pll_reset_asserted_reg_n_0,
      R => \out\(0)
    );
\refclk_stable_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_3_n_0\,
      I1 => \refclk_stable_count[0]_i_4_n_0\,
      I2 => \refclk_stable_count[0]_i_5_n_0\,
      I3 => \refclk_stable_count[0]_i_6_n_0\,
      I4 => \refclk_stable_count[0]_i_7_n_0\,
      I5 => \refclk_stable_count[0]_i_8_n_0\,
      O => refclk_stable_count
    );
\refclk_stable_count[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => refclk_stable_count_reg(13),
      I1 => refclk_stable_count_reg(12),
      I2 => refclk_stable_count_reg(10),
      I3 => refclk_stable_count_reg(11),
      I4 => refclk_stable_count_reg(9),
      I5 => refclk_stable_count_reg(8),
      O => \refclk_stable_count[0]_i_3_n_0\
    );
\refclk_stable_count[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFDF"
    )
        port map (
      I0 => refclk_stable_count_reg(19),
      I1 => refclk_stable_count_reg(18),
      I2 => refclk_stable_count_reg(16),
      I3 => refclk_stable_count_reg(17),
      I4 => refclk_stable_count_reg(15),
      I5 => refclk_stable_count_reg(14),
      O => \refclk_stable_count[0]_i_4_n_0\
    );
\refclk_stable_count[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(30),
      I1 => refclk_stable_count_reg(31),
      I2 => refclk_stable_count_reg(28),
      I3 => refclk_stable_count_reg(29),
      I4 => refclk_stable_count_reg(27),
      I5 => refclk_stable_count_reg(26),
      O => \refclk_stable_count[0]_i_5_n_0\
    );
\refclk_stable_count[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => refclk_stable_count_reg(24),
      I1 => refclk_stable_count_reg(25),
      I2 => refclk_stable_count_reg(22),
      I3 => refclk_stable_count_reg(23),
      I4 => refclk_stable_count_reg(21),
      I5 => refclk_stable_count_reg(20),
      O => \refclk_stable_count[0]_i_6_n_0\
    );
\refclk_stable_count[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      I1 => refclk_stable_count_reg(1),
      O => \refclk_stable_count[0]_i_7_n_0\
    );
\refclk_stable_count[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF7"
    )
        port map (
      I0 => refclk_stable_count_reg(6),
      I1 => refclk_stable_count_reg(7),
      I2 => refclk_stable_count_reg(4),
      I3 => refclk_stable_count_reg(5),
      I4 => refclk_stable_count_reg(3),
      I5 => refclk_stable_count_reg(2),
      O => \refclk_stable_count[0]_i_8_n_0\
    );
\refclk_stable_count[0]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => refclk_stable_count_reg(0),
      O => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_7\,
      Q => refclk_stable_count_reg(0),
      R => '0'
    );
\refclk_stable_count_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(2) => \refclk_stable_count_reg[0]_i_2_n_1\,
      CO(1) => \refclk_stable_count_reg[0]_i_2_n_2\,
      CO(0) => \refclk_stable_count_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \refclk_stable_count_reg[0]_i_2_n_4\,
      O(2) => \refclk_stable_count_reg[0]_i_2_n_5\,
      O(1) => \refclk_stable_count_reg[0]_i_2_n_6\,
      O(0) => \refclk_stable_count_reg[0]_i_2_n_7\,
      S(3 downto 1) => refclk_stable_count_reg(3 downto 1),
      S(0) => \refclk_stable_count[0]_i_9_n_0\
    );
\refclk_stable_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_5\,
      Q => refclk_stable_count_reg(10),
      R => '0'
    );
\refclk_stable_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_4\,
      Q => refclk_stable_count_reg(11),
      R => '0'
    );
\refclk_stable_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_7\,
      Q => refclk_stable_count_reg(12),
      R => '0'
    );
\refclk_stable_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[12]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[12]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[12]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[12]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[12]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(15 downto 12)
    );
\refclk_stable_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_6\,
      Q => refclk_stable_count_reg(13),
      R => '0'
    );
\refclk_stable_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_5\,
      Q => refclk_stable_count_reg(14),
      R => '0'
    );
\refclk_stable_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[12]_i_1_n_4\,
      Q => refclk_stable_count_reg(15),
      R => '0'
    );
\refclk_stable_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_7\,
      Q => refclk_stable_count_reg(16),
      R => '0'
    );
\refclk_stable_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[12]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[16]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[16]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[16]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[16]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[16]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[16]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(19 downto 16)
    );
\refclk_stable_count_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_6\,
      Q => refclk_stable_count_reg(17),
      R => '0'
    );
\refclk_stable_count_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_5\,
      Q => refclk_stable_count_reg(18),
      R => '0'
    );
\refclk_stable_count_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[16]_i_1_n_4\,
      Q => refclk_stable_count_reg(19),
      R => '0'
    );
\refclk_stable_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_6\,
      Q => refclk_stable_count_reg(1),
      R => '0'
    );
\refclk_stable_count_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_7\,
      Q => refclk_stable_count_reg(20),
      R => '0'
    );
\refclk_stable_count_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[16]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[20]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[20]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[20]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[20]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[20]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[20]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(23 downto 20)
    );
\refclk_stable_count_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_6\,
      Q => refclk_stable_count_reg(21),
      R => '0'
    );
\refclk_stable_count_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_5\,
      Q => refclk_stable_count_reg(22),
      R => '0'
    );
\refclk_stable_count_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[20]_i_1_n_4\,
      Q => refclk_stable_count_reg(23),
      R => '0'
    );
\refclk_stable_count_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_7\,
      Q => refclk_stable_count_reg(24),
      R => '0'
    );
\refclk_stable_count_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[20]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[24]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[24]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[24]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[24]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[24]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[24]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(27 downto 24)
    );
\refclk_stable_count_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_6\,
      Q => refclk_stable_count_reg(25),
      R => '0'
    );
\refclk_stable_count_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_5\,
      Q => refclk_stable_count_reg(26),
      R => '0'
    );
\refclk_stable_count_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[24]_i_1_n_4\,
      Q => refclk_stable_count_reg(27),
      R => '0'
    );
\refclk_stable_count_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_7\,
      Q => refclk_stable_count_reg(28),
      R => '0'
    );
\refclk_stable_count_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[24]_i_1_n_0\,
      CO(3) => \NLW_refclk_stable_count_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \refclk_stable_count_reg[28]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[28]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[28]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[28]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[28]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[28]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(31 downto 28)
    );
\refclk_stable_count_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_6\,
      Q => refclk_stable_count_reg(29),
      R => '0'
    );
\refclk_stable_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_5\,
      Q => refclk_stable_count_reg(2),
      R => '0'
    );
\refclk_stable_count_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_5\,
      Q => refclk_stable_count_reg(30),
      R => '0'
    );
\refclk_stable_count_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[28]_i_1_n_4\,
      Q => refclk_stable_count_reg(31),
      R => '0'
    );
\refclk_stable_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[0]_i_2_n_4\,
      Q => refclk_stable_count_reg(3),
      R => '0'
    );
\refclk_stable_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_7\,
      Q => refclk_stable_count_reg(4),
      R => '0'
    );
\refclk_stable_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[0]_i_2_n_0\,
      CO(3) => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[4]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[4]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[4]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[4]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[4]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(7 downto 4)
    );
\refclk_stable_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_6\,
      Q => refclk_stable_count_reg(5),
      R => '0'
    );
\refclk_stable_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_5\,
      Q => refclk_stable_count_reg(6),
      R => '0'
    );
\refclk_stable_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[4]_i_1_n_4\,
      Q => refclk_stable_count_reg(7),
      R => '0'
    );
\refclk_stable_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_7\,
      Q => refclk_stable_count_reg(8),
      R => '0'
    );
\refclk_stable_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \refclk_stable_count_reg[4]_i_1_n_0\,
      CO(3) => \refclk_stable_count_reg[8]_i_1_n_0\,
      CO(2) => \refclk_stable_count_reg[8]_i_1_n_1\,
      CO(1) => \refclk_stable_count_reg[8]_i_1_n_2\,
      CO(0) => \refclk_stable_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \refclk_stable_count_reg[8]_i_1_n_4\,
      O(2) => \refclk_stable_count_reg[8]_i_1_n_5\,
      O(1) => \refclk_stable_count_reg[8]_i_1_n_6\,
      O(0) => \refclk_stable_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => refclk_stable_count_reg(11 downto 8)
    );
\refclk_stable_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => refclk_stable_count,
      D => \refclk_stable_count_reg[8]_i_1_n_6\,
      Q => refclk_stable_count_reg(9),
      R => '0'
    );
refclk_stable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \refclk_stable_count[0]_i_7_n_0\,
      I1 => refclk_stable_i_2_n_0,
      I2 => refclk_stable_i_3_n_0,
      I3 => refclk_stable_i_4_n_0,
      I4 => refclk_stable_i_5_n_0,
      I5 => refclk_stable_i_6_n_0,
      O => refclk_stable_i_1_n_0
    );
refclk_stable_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(4),
      I1 => refclk_stable_count_reg(5),
      I2 => refclk_stable_count_reg(2),
      I3 => refclk_stable_count_reg(3),
      I4 => refclk_stable_count_reg(7),
      I5 => refclk_stable_count_reg(6),
      O => refclk_stable_i_2_n_0
    );
refclk_stable_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200000000000"
    )
        port map (
      I0 => refclk_stable_count_reg(10),
      I1 => refclk_stable_count_reg(11),
      I2 => refclk_stable_count_reg(8),
      I3 => refclk_stable_count_reg(9),
      I4 => refclk_stable_count_reg(12),
      I5 => refclk_stable_count_reg(13),
      O => refclk_stable_i_3_n_0
    );
refclk_stable_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => refclk_stable_count_reg(16),
      I1 => refclk_stable_count_reg(17),
      I2 => refclk_stable_count_reg(14),
      I3 => refclk_stable_count_reg(15),
      I4 => refclk_stable_count_reg(18),
      I5 => refclk_stable_count_reg(19),
      O => refclk_stable_i_4_n_0
    );
refclk_stable_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(22),
      I1 => refclk_stable_count_reg(23),
      I2 => refclk_stable_count_reg(20),
      I3 => refclk_stable_count_reg(21),
      I4 => refclk_stable_count_reg(25),
      I5 => refclk_stable_count_reg(24),
      O => refclk_stable_i_5_n_0
    );
refclk_stable_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => refclk_stable_count_reg(28),
      I1 => refclk_stable_count_reg(29),
      I2 => refclk_stable_count_reg(26),
      I3 => refclk_stable_count_reg(27),
      I4 => refclk_stable_count_reg(31),
      I5 => refclk_stable_count_reg(30),
      O => refclk_stable_i_6_n_0
    );
refclk_stable_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => refclk_stable_i_1_n_0,
      Q => refclk_stable_reg_n_0,
      R => '0'
    );
\reset_time_out_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"440000FF50505050"
    )
        port map (
      I0 => tx_state(3),
      I1 => txresetdone_s3,
      I2 => init_wait_done_reg_n_0,
      I3 => tx_state(1),
      I4 => tx_state(2),
      I5 => tx_state(0),
      O => \reset_time_out_i_2__0_n_0\
    );
reset_time_out_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => sync_cplllock_n_0,
      Q => reset_time_out,
      R => \out\(0)
    );
run_phase_alignment_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0002"
    )
        port map (
      I0 => tx_state(3),
      I1 => tx_state(0),
      I2 => tx_state(2),
      I3 => tx_state(1),
      I4 => run_phase_alignment_int_reg_n_0,
      O => run_phase_alignment_int_i_1_n_0
    );
run_phase_alignment_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => run_phase_alignment_int_i_1_n_0,
      Q => run_phase_alignment_int_reg_n_0,
      R => \out\(0)
    );
run_phase_alignment_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => data_out,
      Q => run_phase_alignment_int_s3,
      R => '0'
    );
sync_TXRESETDONE: entity work.gig_ethernet_pcs_pma_0_sync_block_4
     port map (
      data_out => txresetdone_s2,
      data_sync_reg1_0 => data_sync_reg1_0,
      independent_clock_bufg => independent_clock_bufg
    );
sync_cplllock: entity work.gig_ethernet_pcs_pma_0_sync_block_5
     port map (
      E(0) => sync_cplllock_n_1,
      \FSM_sequential_tx_state_reg[0]\ => \FSM_sequential_tx_state[3]_i_3_n_0\,
      \FSM_sequential_tx_state_reg[0]_0\ => \FSM_sequential_tx_state[3]_i_4_n_0\,
      \FSM_sequential_tx_state_reg[0]_1\ => \FSM_sequential_tx_state[3]_i_6_n_0\,
      \FSM_sequential_tx_state_reg[0]_2\ => time_out_2ms_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_3\ => \FSM_sequential_tx_state[3]_i_7_n_0\,
      \FSM_sequential_tx_state_reg[0]_4\ => pll_reset_asserted_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_5\ => refclk_stable_reg_n_0,
      \FSM_sequential_tx_state_reg[0]_6\ => \FSM_sequential_tx_state[0]_i_3_n_0\,
      Q(3 downto 0) => tx_state(3 downto 0),
      data_sync_reg1_0 => data_sync_reg1_2,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_lock_reclocked => mmcm_lock_reclocked,
      reset_time_out => reset_time_out,
      reset_time_out_reg => sync_cplllock_n_0,
      reset_time_out_reg_0 => \reset_time_out_i_2__0_n_0\,
      reset_time_out_reg_1 => init_wait_done_reg_n_0
    );
sync_mmcm_lock_reclocked: entity work.gig_ethernet_pcs_pma_0_sync_block_6
     port map (
      SR(0) => sync_mmcm_lock_reclocked_n_0,
      data_out => mmcm_lock_i,
      data_sync_reg1_0 => data_sync_reg1_1,
      independent_clock_bufg => independent_clock_bufg
    );
sync_run_phase_alignment_int: entity work.gig_ethernet_pcs_pma_0_sync_block_7
     port map (
      data_in => run_phase_alignment_int_reg_n_0,
      data_out => data_out,
      data_sync_reg6_0 => data_sync_reg1
    );
sync_time_out_wait_bypass: entity work.gig_ethernet_pcs_pma_0_sync_block_8
     port map (
      data_in => time_out_wait_bypass_reg_n_0,
      data_out => time_out_wait_bypass_s2,
      independent_clock_bufg => independent_clock_bufg
    );
sync_tx_fsm_reset_done_int: entity work.gig_ethernet_pcs_pma_0_sync_block_9
     port map (
      data_in => \^data_in\,
      data_out => tx_fsm_reset_done_int_s2,
      data_sync_reg1_0 => data_sync_reg1
    );
time_out_2ms_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AE"
    )
        port map (
      I0 => time_out_2ms_reg_n_0,
      I1 => \time_out_2ms_i_2__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => reset_time_out,
      O => time_out_2ms_i_1_n_0
    );
\time_out_2ms_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(5),
      I5 => time_tlock_max_i_3_n_0,
      O => \time_out_2ms_i_2__0_n_0\
    );
time_out_2ms_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => time_out_counter_reg(7),
      I1 => time_out_counter_reg(14),
      I2 => \time_out_2ms_i_4__0_n_0\,
      I3 => time_out_2ms_i_5_n_0,
      O => time_out_2ms_i_3_n_0
    );
\time_out_2ms_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => time_out_counter_reg(4),
      I1 => time_out_counter_reg(3),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(6),
      O => \time_out_2ms_i_4__0_n_0\
    );
time_out_2ms_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => time_out_counter_reg(0),
      I1 => time_out_counter_reg(13),
      I2 => time_out_counter_reg(9),
      I3 => time_out_counter_reg(2),
      I4 => time_out_counter_reg(1),
      O => time_out_2ms_i_5_n_0
    );
time_out_2ms_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_2ms_i_1_n_0,
      Q => time_out_2ms_reg_n_0,
      R => '0'
    );
time_out_500us_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAEAAA"
    )
        port map (
      I0 => time_out_500us_reg_n_0,
      I1 => time_out_500us_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_out_counter_reg(10),
      I4 => time_out_2ms_i_3_n_0,
      I5 => reset_time_out,
      O => time_out_500us_i_1_n_0
    );
time_out_500us_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => time_out_counter_reg(15),
      I1 => time_out_counter_reg(16),
      I2 => time_out_counter_reg(11),
      I3 => time_out_counter_reg(12),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_out_500us_i_2_n_0
    );
time_out_500us_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_500us_i_1_n_0,
      Q => time_out_500us_reg_n_0,
      R => '0'
    );
\time_out_counter[0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBFFFF"
    )
        port map (
      I0 => time_tlock_max_i_3_n_0,
      I1 => \time_out_counter[0]_i_3__0_n_0\,
      I2 => time_out_2ms_i_3_n_0,
      I3 => time_out_counter_reg(10),
      I4 => time_out_counter_reg(12),
      I5 => time_out_counter_reg(5),
      O => time_out_counter
    );
\time_out_counter[0]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => time_out_counter_reg(17),
      I1 => time_out_counter_reg(18),
      O => \time_out_counter[0]_i_3__0_n_0\
    );
\time_out_counter[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => time_out_counter_reg(0),
      O => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_7\,
      Q => time_out_counter_reg(0),
      R => reset_time_out
    );
\time_out_counter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \time_out_counter_reg[0]_i_2_n_0\,
      CO(2) => \time_out_counter_reg[0]_i_2_n_1\,
      CO(1) => \time_out_counter_reg[0]_i_2_n_2\,
      CO(0) => \time_out_counter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \time_out_counter_reg[0]_i_2_n_4\,
      O(2) => \time_out_counter_reg[0]_i_2_n_5\,
      O(1) => \time_out_counter_reg[0]_i_2_n_6\,
      O(0) => \time_out_counter_reg[0]_i_2_n_7\,
      S(3 downto 1) => time_out_counter_reg(3 downto 1),
      S(0) => \time_out_counter[0]_i_4_n_0\
    );
\time_out_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_5\,
      Q => time_out_counter_reg(10),
      R => reset_time_out
    );
\time_out_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_4\,
      Q => time_out_counter_reg(11),
      R => reset_time_out
    );
\time_out_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_7\,
      Q => time_out_counter_reg(12),
      R => reset_time_out
    );
\time_out_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[8]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[12]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[12]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[12]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[12]_i_1_n_4\,
      O(2) => \time_out_counter_reg[12]_i_1_n_5\,
      O(1) => \time_out_counter_reg[12]_i_1_n_6\,
      O(0) => \time_out_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(15 downto 12)
    );
\time_out_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_6\,
      Q => time_out_counter_reg(13),
      R => reset_time_out
    );
\time_out_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_5\,
      Q => time_out_counter_reg(14),
      R => reset_time_out
    );
\time_out_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[12]_i_1_n_4\,
      Q => time_out_counter_reg(15),
      R => reset_time_out
    );
\time_out_counter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_7\,
      Q => time_out_counter_reg(16),
      R => reset_time_out
    );
\time_out_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \time_out_counter_reg[16]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED\(3),
      O(2) => \time_out_counter_reg[16]_i_1_n_5\,
      O(1) => \time_out_counter_reg[16]_i_1_n_6\,
      O(0) => \time_out_counter_reg[16]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 0) => time_out_counter_reg(18 downto 16)
    );
\time_out_counter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_6\,
      Q => time_out_counter_reg(17),
      R => reset_time_out
    );
\time_out_counter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[16]_i_1_n_5\,
      Q => time_out_counter_reg(18),
      R => reset_time_out
    );
\time_out_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_6\,
      Q => time_out_counter_reg(1),
      R => reset_time_out
    );
\time_out_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_5\,
      Q => time_out_counter_reg(2),
      R => reset_time_out
    );
\time_out_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[0]_i_2_n_4\,
      Q => time_out_counter_reg(3),
      R => reset_time_out
    );
\time_out_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_7\,
      Q => time_out_counter_reg(4),
      R => reset_time_out
    );
\time_out_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[0]_i_2_n_0\,
      CO(3) => \time_out_counter_reg[4]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[4]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[4]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[4]_i_1_n_4\,
      O(2) => \time_out_counter_reg[4]_i_1_n_5\,
      O(1) => \time_out_counter_reg[4]_i_1_n_6\,
      O(0) => \time_out_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(7 downto 4)
    );
\time_out_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_6\,
      Q => time_out_counter_reg(5),
      R => reset_time_out
    );
\time_out_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_5\,
      Q => time_out_counter_reg(6),
      R => reset_time_out
    );
\time_out_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[4]_i_1_n_4\,
      Q => time_out_counter_reg(7),
      R => reset_time_out
    );
\time_out_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_7\,
      Q => time_out_counter_reg(8),
      R => reset_time_out
    );
\time_out_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \time_out_counter_reg[4]_i_1_n_0\,
      CO(3) => \time_out_counter_reg[8]_i_1_n_0\,
      CO(2) => \time_out_counter_reg[8]_i_1_n_1\,
      CO(1) => \time_out_counter_reg[8]_i_1_n_2\,
      CO(0) => \time_out_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \time_out_counter_reg[8]_i_1_n_4\,
      O(2) => \time_out_counter_reg[8]_i_1_n_5\,
      O(1) => \time_out_counter_reg[8]_i_1_n_6\,
      O(0) => \time_out_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => time_out_counter_reg(11 downto 8)
    );
\time_out_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => time_out_counter,
      D => \time_out_counter_reg[8]_i_1_n_6\,
      Q => time_out_counter_reg(9),
      R => reset_time_out
    );
time_out_wait_bypass_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AB00"
    )
        port map (
      I0 => time_out_wait_bypass_reg_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      I2 => time_out_wait_bypass_i_2_n_0,
      I3 => run_phase_alignment_int_s3,
      O => time_out_wait_bypass_i_1_n_0
    );
time_out_wait_bypass_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFFF"
    )
        port map (
      I0 => time_out_wait_bypass_i_3_n_0,
      I1 => time_out_wait_bypass_i_4_n_0,
      I2 => wait_bypass_count_reg(5),
      I3 => wait_bypass_count_reg(13),
      I4 => wait_bypass_count_reg(11),
      I5 => time_out_wait_bypass_i_5_n_0,
      O => time_out_wait_bypass_i_2_n_0
    );
time_out_wait_bypass_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => wait_bypass_count_reg(16),
      I1 => wait_bypass_count_reg(9),
      I2 => wait_bypass_count_reg(12),
      I3 => wait_bypass_count_reg(10),
      O => time_out_wait_bypass_i_3_n_0
    );
time_out_wait_bypass_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => wait_bypass_count_reg(4),
      I1 => wait_bypass_count_reg(15),
      I2 => wait_bypass_count_reg(6),
      I3 => wait_bypass_count_reg(0),
      O => time_out_wait_bypass_i_4_n_0
    );
time_out_wait_bypass_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => wait_bypass_count_reg(8),
      I1 => wait_bypass_count_reg(1),
      I2 => wait_bypass_count_reg(7),
      I3 => wait_bypass_count_reg(14),
      I4 => wait_bypass_count_reg(2),
      I5 => wait_bypass_count_reg(3),
      O => time_out_wait_bypass_i_5_n_0
    );
time_out_wait_bypass_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => time_out_wait_bypass_i_1_n_0,
      Q => time_out_wait_bypass_reg_n_0,
      R => '0'
    );
time_out_wait_bypass_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_out_wait_bypass_s2,
      Q => time_out_wait_bypass_s3,
      R => '0'
    );
time_tlock_max_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAAAEA"
    )
        port map (
      I0 => time_tlock_max_reg_n_0,
      I1 => time_tlock_max_i_2_n_0,
      I2 => time_out_counter_reg(5),
      I3 => time_tlock_max_i_3_n_0,
      I4 => time_tlock_max_i_4_n_0,
      I5 => reset_time_out,
      O => time_tlock_max_i_1_n_0
    );
time_tlock_max_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => time_out_counter_reg(14),
      I1 => time_out_counter_reg(12),
      I2 => time_out_counter_reg(10),
      I3 => time_out_counter_reg(7),
      I4 => time_out_counter_reg(18),
      I5 => time_out_counter_reg(17),
      O => time_tlock_max_i_2_n_0
    );
time_tlock_max_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => time_out_counter_reg(16),
      I1 => time_out_counter_reg(15),
      I2 => time_out_counter_reg(11),
      O => time_tlock_max_i_3_n_0
    );
time_tlock_max_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_out_2ms_i_5_n_0,
      I1 => time_out_counter_reg(6),
      I2 => time_out_counter_reg(8),
      I3 => time_out_counter_reg(3),
      I4 => time_out_counter_reg(4),
      O => time_tlock_max_i_4_n_0
    );
time_tlock_max_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => time_tlock_max_i_1_n_0,
      Q => time_tlock_max_reg_n_0,
      R => '0'
    );
tx_fsm_reset_done_int_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF1000"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(0),
      I3 => tx_state(3),
      I4 => \^data_in\,
      O => tx_fsm_reset_done_int_i_1_n_0
    );
tx_fsm_reset_done_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => tx_fsm_reset_done_int_i_1_n_0,
      Q => \^data_in\,
      R => \out\(0)
    );
tx_fsm_reset_done_int_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => '1',
      D => tx_fsm_reset_done_int_s2,
      Q => tx_fsm_reset_done_int_s3,
      R => '0'
    );
txresetdone_s3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => txresetdone_s2,
      Q => txresetdone_s3,
      R => '0'
    );
\wait_bypass_count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => run_phase_alignment_int_s3,
      O => clear
    );
\wait_bypass_count[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => time_out_wait_bypass_i_2_n_0,
      I1 => tx_fsm_reset_done_int_s3,
      O => \wait_bypass_count[0]_i_2_n_0\
    );
\wait_bypass_count[0]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_bypass_count_reg(0),
      O => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_7\,
      Q => wait_bypass_count_reg(0),
      R => clear
    );
\wait_bypass_count_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(2) => \wait_bypass_count_reg[0]_i_3_n_1\,
      CO(1) => \wait_bypass_count_reg[0]_i_3_n_2\,
      CO(0) => \wait_bypass_count_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wait_bypass_count_reg[0]_i_3_n_4\,
      O(2) => \wait_bypass_count_reg[0]_i_3_n_5\,
      O(1) => \wait_bypass_count_reg[0]_i_3_n_6\,
      O(0) => \wait_bypass_count_reg[0]_i_3_n_7\,
      S(3 downto 1) => wait_bypass_count_reg(3 downto 1),
      S(0) => \wait_bypass_count[0]_i_4__0_n_0\
    );
\wait_bypass_count_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_5\,
      Q => wait_bypass_count_reg(10),
      R => clear
    );
\wait_bypass_count_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_4\,
      Q => wait_bypass_count_reg(11),
      R => clear
    );
\wait_bypass_count_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_7\,
      Q => wait_bypass_count_reg(12),
      R => clear
    );
\wait_bypass_count_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[12]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[12]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[12]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[12]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[12]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[12]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(15 downto 12)
    );
\wait_bypass_count_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_6\,
      Q => wait_bypass_count_reg(13),
      R => clear
    );
\wait_bypass_count_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_5\,
      Q => wait_bypass_count_reg(14),
      R => clear
    );
\wait_bypass_count_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[12]_i_1_n_4\,
      Q => wait_bypass_count_reg(15),
      R => clear
    );
\wait_bypass_count_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[16]_i_1_n_7\,
      Q => wait_bypass_count_reg(16),
      R => clear
    );
\wait_bypass_count_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_wait_bypass_count_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_wait_bypass_count_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \wait_bypass_count_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => wait_bypass_count_reg(16)
    );
\wait_bypass_count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_6\,
      Q => wait_bypass_count_reg(1),
      R => clear
    );
\wait_bypass_count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_5\,
      Q => wait_bypass_count_reg(2),
      R => clear
    );
\wait_bypass_count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[0]_i_3_n_4\,
      Q => wait_bypass_count_reg(3),
      R => clear
    );
\wait_bypass_count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_7\,
      Q => wait_bypass_count_reg(4),
      R => clear
    );
\wait_bypass_count_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[0]_i_3_n_0\,
      CO(3) => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[4]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[4]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[4]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[4]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[4]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[4]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(7 downto 4)
    );
\wait_bypass_count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_6\,
      Q => wait_bypass_count_reg(5),
      R => clear
    );
\wait_bypass_count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_5\,
      Q => wait_bypass_count_reg(6),
      R => clear
    );
\wait_bypass_count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[4]_i_1_n_4\,
      Q => wait_bypass_count_reg(7),
      R => clear
    );
\wait_bypass_count_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_7\,
      Q => wait_bypass_count_reg(8),
      R => clear
    );
\wait_bypass_count_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wait_bypass_count_reg[4]_i_1_n_0\,
      CO(3) => \wait_bypass_count_reg[8]_i_1_n_0\,
      CO(2) => \wait_bypass_count_reg[8]_i_1_n_1\,
      CO(1) => \wait_bypass_count_reg[8]_i_1_n_2\,
      CO(0) => \wait_bypass_count_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wait_bypass_count_reg[8]_i_1_n_4\,
      O(2) => \wait_bypass_count_reg[8]_i_1_n_5\,
      O(1) => \wait_bypass_count_reg[8]_i_1_n_6\,
      O(0) => \wait_bypass_count_reg[8]_i_1_n_7\,
      S(3 downto 0) => wait_bypass_count_reg(11 downto 8)
    );
\wait_bypass_count_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_sync_reg1,
      CE => \wait_bypass_count[0]_i_2_n_0\,
      D => \wait_bypass_count_reg[8]_i_1_n_6\,
      Q => wait_bypass_count_reg(9),
      R => clear
    );
\wait_time_cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      O => wait_time_cnt0(0)
    );
\wait_time_cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => wait_time_cnt_reg(0),
      I1 => wait_time_cnt_reg(1),
      O => \wait_time_cnt[1]_i_1_n_0\
    );
\wait_time_cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => wait_time_cnt_reg(1),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(2),
      O => \wait_time_cnt[2]_i_1_n_0\
    );
\wait_time_cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => wait_time_cnt_reg(2),
      I1 => wait_time_cnt_reg(0),
      I2 => wait_time_cnt_reg(1),
      I3 => wait_time_cnt_reg(3),
      O => \wait_time_cnt[3]_i_1_n_0\
    );
\wait_time_cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => wait_time_cnt_reg(3),
      I1 => wait_time_cnt_reg(1),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(2),
      I4 => wait_time_cnt_reg(4),
      O => \wait_time_cnt[4]_i_1_n_0\
    );
\wait_time_cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000001"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[5]_i_1_n_0\
    );
\wait_time_cnt[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0700"
    )
        port map (
      I0 => tx_state(1),
      I1 => tx_state(2),
      I2 => tx_state(3),
      I3 => tx_state(0),
      O => wait_time_cnt0_0
    );
\wait_time_cnt[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => sel
    );
\wait_time_cnt[6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \wait_time_cnt[6]_i_4_n_0\,
      I1 => wait_time_cnt_reg(6),
      O => \wait_time_cnt[6]_i_3_n_0\
    );
\wait_time_cnt[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => wait_time_cnt_reg(4),
      I1 => wait_time_cnt_reg(2),
      I2 => wait_time_cnt_reg(0),
      I3 => wait_time_cnt_reg(1),
      I4 => wait_time_cnt_reg(3),
      I5 => wait_time_cnt_reg(5),
      O => \wait_time_cnt[6]_i_4_n_0\
    );
\wait_time_cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => wait_time_cnt0(0),
      Q => wait_time_cnt_reg(0),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[1]_i_1_n_0\,
      Q => wait_time_cnt_reg(1),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[2]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[2]_i_1_n_0\,
      Q => wait_time_cnt_reg(2),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[3]_i_1_n_0\,
      Q => wait_time_cnt_reg(3),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[4]_i_1_n_0\,
      Q => wait_time_cnt_reg(4),
      R => wait_time_cnt0_0
    );
\wait_time_cnt_reg[5]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[5]_i_1_n_0\,
      Q => wait_time_cnt_reg(5),
      S => wait_time_cnt0_0
    );
\wait_time_cnt_reg[6]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => sel,
      D => \wait_time_cnt[6]_i_3_n_0\,
      Q => wait_time_cnt_reg(6),
      S => wait_time_cnt0_0
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
afmoZr9skqIZsiBY7liE8HhN5PT9cSTlE7b7FkfYEnmAz8/sYewHAx0YfvnPtVZDeaUQ1+SA/YSK
81BVyxiW6Q1+w7v5E1h+YJTA02rq79x+8OQbPa0mLIjdXTtNmx+BnF2Qrq3QGTPNoSFginwNNM6R
DHZuBkJnITzBkDYC/to=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
oFhR7iEHB5cYBaPdDcFYa+fK3Gj/00Z1GiQ4ZVkb1MGu+Q6dRi7Lgvhbwd39WT10uvi5m95gOjlX
gGa0fqW2XuRf5YGzXsxqVh0xVmshpf7o2GLRr2CmqpGBtEUVv4LbXJ4WiaEo9wVMVr2rBg6MtuPB
CQBdJcTka6nWZivdNblYGd53J9DObofIqJ9W/NUzFP/DmzVmGbctoa4amp6KUecoKxnO4tESoQzm
lz7s+Zslv3tCv5wVOwV3SAR+Y5ul9hQX8OljyF82Z31ipd+bzojhPg9RhmrvIMbFqzM9eU82pEIT
IZzSVKYMd1P6t1qORfwdgodKT0ZApoyFVxfVeQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
FJ5kCtzgKdagetGF6bdHAPKIu2cq7BcnQFRTWMN8j6rUq4ZaiydIPtRo6ybDhCzgm0ZRPth8Lgd5
+HXk3g/SdsTLZoOUuXnEz+TkfRJdu8kdJ+0y8sJMNK21IEW3pSWsj/n4toksSjLJ1wENuoq4XqDK
9VQo0Gobmx7OOqOYFuY=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
d/VWmC3TdLGZzbtN4vTxv6kYGkcDx9TNbvTz1ALcTDtGFLEUmm0Z0/puE1NDYgBTJquGDZ+j8d2H
/il9HfJ/2n/hTxPKUXm83KvLOZ3eaRdjaEk1wHy+UsjHJ1VztyOuT/nrMaT3sUq1kpP7wkC5I2EK
lQZbp4ngkuiSjWCad10Yoj0TX6DfE58qXjaybvWsINo7H6gMzi4JvYHWbBnCYQS/RkvaQvSsne0i
YvfJuYy63HVFs+Afh6hOO34nbqzXvlsXI7SIicsCUu21AgLUNio4IsL+Uj92NEPjaJnw2wTXrMuH
A1a3i03K7vrZ94AIVwtSXRwfN4q33BWP9bjYdQ==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3xYA7hcw+q7P0QSxtus9heaAij7kiNzMgPuCXFQTIhWPWN0atyWMkDqoxytTvKyCd0IM0n0DheIN
XngnbZmTQpitsZh5f/e8iD/+iWxl+7S7I6wyIivHhUA/yP87+KG/vFGPXu7WexVyPmZuOgSS7sS/
Fnk+QS1l3qltq84DH59g9EBTV7YkcgLEdICkCR0tgP3K8aSVjCptgXzA0onYGrkQESEdw8nPfEvR
g/5lKhHOmmE6kTh/XfO6eQpuQBKOlD7RaVWrPxnTkM9lxrjRZZb3OSfaBLMKhMGrOdEY4RIqfHMG
MBOH8gAUoIRrgyLztGTWc+RsxUUYHq9Hwixqkg==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RyOZOz+qJ5WH/47JnOQuMFPCUAyXetmhoeJiNflULv2xv5JJeXWSOURz6kOeJ5/Jbz2KA/psbOVw
rwZKAlTJoEVMq47lcrgzkIZPdllrnwbNl8hiziEb/YEq2vvpC9EJzY7LlRbQINiQY3hAzP5B/s+N
lYRpAUjOhEst2xZEiFz6cr7zAMz2jn7lHWYuvu75NGUv8JJ7PZ6vUISUeHOPW9ljH0TH4CnOaTpM
Mey1hosrMsp78mwEmy3l4n56Khrq2FOUzGZbi5lhqEebDEU/90LVUpUSzOkhJ03OfvAgBuj9ejqz
WgZRD6DpCk8BVUByKAGJqQZsJ0NcEnE8/btwhQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Q98gl70eQ6J1AYIClxRNKl48Tz7BoxVypGMCmvUy8HqLl3jjFsIYy9amcj+/0Qts3PcCHOutO4e+
ptR4cjDneD+41DiKHcGc2sgfdKq4hG60mda13C9qtjKCuBhg+Zg1jHi3TOh4bVtsLuydPMwaILNj
7ORZo/jzBQ4OUPNFy2tkOMJArdeTOIuydxhUFtpy6FAABlc1SE1Mer24GTLYw0pJtOOJOX61uJuc
ghuBuHaXV8kExCP3IU1qx/oyfIpY9TQiqLKEf0YUrmEzBzV+JXQqn3Jom9PXTay5YJ+PqraK9iFt
ai1YIDay5F0ncLDXiT7OMRFjFG2mL+C96DFEmg==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
DgXXbyNB3XMOH/gmmWb6uBYjn7XQuHgaTP3eav9Y+qTkta/ItNcuqONFgEZl6duKUkQI/YVDFLf5
JwZHmI7U9IACSpdRAsITpVXnabTJhDmcKIxi5anNhIeSW9RerBcxlMYtuoI5FgWJKz6h+0yZmes6
xYMyXTT3gJS5ttPc1n6kBHNZpxPgl/Yv979A0/ZxYkRTfuWrCyaS5T08AajGwOyC+2sH4FjL2xLX
uR9a04wQespOimJ+InC0FAfYhqUcjs5b18x0hSfPhe4G0xKNvmpukNOhIflD4bBgKXl0kqXnpatt
7ejKBpSWKRjk/M7rYOxErj8EahuRFH07pBR9b3XyXF3MfQBwAzV2GIz+A/XzIUDQOcBbojfGwrVA
2HhHfn8sKZB5ktpwF8MBjdlwrtn+kdQ8ux0ZNrgndrvw4nS4TWxZtQeRc+wfb7Q/lIzw6Pc+Ifnn
b7UJlJvNr5ZF2XNxuHGz5L19UYgB0gpH8xWvYpn0Od797z3FfYIiGMNP

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
YW19UgddTwwnJVcciGeEKvH9ZuBWuoo4LgtOhQvaoLIGVBAQAVfk5VY0pzvluZUn2+81fwbTPSLA
O+/RYs39A761jlViEBPYfmojlgjyiOTK9aHXNLsD3WBxfTXGOS3fHhhwgCmiZ0OncriPRHcNAK7N
4cyj3anKb5y4g9gzt9YeJ7T/zkLQuzT4zqnb9ijTisbH6HxpAVXI1ydKtGzVETw+s55Jb0gVXyGi
Q1hVe4oSDBTETiD85J2/A1KF+2DHKkzOJhr53qxvjND2l/LqsA0G/4J35IdLcmuYWAoCJS7cHkTk
wnGOjEnNbP+gboLu5UrXAFwXCo+jsZLVqIzGqg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
58apVjNnALRGpDI2bjJhSWJPQlj57Kat42daGg5JiM8yXDkjxm/BmzBRp9oBmOurwCnZOZcKlj3m
HlexbUqO32l40m0mMLkLzlGYPsduAUijMIb7DMl5HUf9XunE4fC69CyB0KyecsJZI9Ru4XxZrniR
CYqqjLtNjfFEppiRGRN6bXqv2T5Vgbh9qrMDIrkZnrIUB7RBd6xTPZ0xU1V6/f7Oftp0kyRv3C0z
HF7RFTnZ4y90YOsvuCM5CoAyyE/0gTXvBlB4icz61XmHsIlwfUu/xC6yAitX3dikrJ3DWeMC5Soe
vAifs/1pf/61AlzL055eu8EpDBQRH32ikamDmA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GkhzmJz9o93IJx+1EnrQfOwa/OOrgHQT2+XhwLsfJNRk9bxhLz472g/aTSOruRXBYVSa7twJJ6+r
lf0nvm4VQfQ9bG+fbYjOsryrSSKlth3/Bq54UcBz0wLrrf59Jt2KtKCOUQojHAIinG8CLxMBgQJR
wqwzAYtmffGsCYbobk0Y4IPkjQhtQamQd1BTI8ZwvF5zPN8kBYuoFLMFqwet6s+kcH/tQSrB0o/K
lHILn0b8uovehAHNWllP4phB4MeAOgMZbbsniJgqPQEhSd4nOUZs4WnWtYOsf+rCLliHTc9c6HIS
0u5f7mdvO+uC8Pi/nwNFr026nnYUX3YcG4zdBQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 59328)
`protect data_block
2zMXQCRGNp+uZFJJPkiE2aqxfeY9DhdXjfsxhz9PIMgAOcJlBEdR1D/8K8l/cUVGuhT2j6KLKWRa
wBEa1q+YhT4qyfvB0dv3bk2ZpFnE/0EOj9UkKSOfn1uGNrOZslDfmF5IzQkH6XkXT9veLUCXudh7
49p9o7aZgUCD0I7H+amvAmt/zG4YIPNZVW80UyTyVPYFKabv7z6hpvrvjGjDKDcKVMxeR6PBI/Lp
UkInzIrlL1dfLLQ8MKpVr5CjGDnlXbbP3Wx4sHWbjne3zZJlWgmvBNdW3mPCk/Ms9fbYnTcztRSt
WdqPflaxd0ByoSkUK0y1rxSnpdRBCoX+LNbNJWCDSRRpWeZp7hNGwe/4z2iEkamyZTHE9q8Rns5P
KHYlJvX2wP2M+XgIFpB/Cdi0xs9DN2+0VYoWLLDNcgGdpPWxKoIgetbkU2PcMvzXVKcj00K5yC+l
l967ZeDfhgUEQ6dH5Gp4k1Dc7r1ESEippFVdpZK8nM4wB3f1ko/cGKMheaRH+cMI+XOpS4UGHfab
mQBO18pkBmmC/CCF/rLIpLu+MNxxxoqIKQUDTRgxFAOqv+aQeZxiWhecQ+WdSoEjBUPlgRZdJnd1
ceWYTpN52vgS36+poNMKrsP0iJufLWQRkkv8SrTpIJTriim7PC/MUXaXhPhJ7C0NAaBqJgrSWPNg
x/h10xt7P7W91HAfm4CqrpQJzy9FRYU2ynJ8PCwP+U7QuMuMft0BA9cX9w2AT+J0QW+st4LiToM9
9nh2G4Tg484YOtBoLiAkcMU1TRPsXlOI9iWxxAGUsC6BsrMlpf7ScoPuLSXjBXCkywdp8TeJ9SvZ
mLt196ou0n06yo+gqqjD5EpblkfFoMR1hXgOyl1ndf5kJYAokLnE+0pT/hf4NGLVGfyrUCJ6SL6I
iSWyWyZSSFCB7baTNOeHQWnXbbKzWh/UHAPIcGiPOJciRSFDjEoVBCnG4Hd7ehFuQXNfIaVWeb1T
IXwi60syGQgb6D5g+4qCFqf15nduVIE5IISs2A1Y8fcRvB+kM3MLb4PRvhlYhLa5flQ4aptyu8if
KFx4krc/zBP2exWmVpH2b5SQnehiyal36lWMpnBA/iOPihGv82q8ZJEOqtWXPt01+Rk80jhCbyX5
PPbyYOsrIIIlZfy2aDqSLqylCD1W6IpyRYCRV8yGRzuaZNR4YZ4QF2+GKjoHQG9ifKRK/xbTlksa
nq7sORgpyKSoyHf1yedr++qVRqyjvgFoI2gg45XyG5wwfZEPvRGZQgqF/HFUijF8hgxz4E4fwGlz
GvKOHm5CZIK97VQ3qRSWDK+jIzF+PICnMbh1ooefPDtNwwHWG0EO40NNNTW4yNzOjIn8tutn8TDy
WsQlw8Pc602QTumJMPtUd25G+CmJ6KRbYzEwaX563AVvRiDUEb0+gfTItODu2vTt3cV96XUPReSF
7cgZGjOi4jCSR+lDsaV6JmRyt2lhdVx+Tm3q7rcbl/oa31NsG721csTOaHiJLMKsU53S7s9VKo+u
B+lOibuBIyIhy+399dlLjiVW05x52gPIYadAr8SYKa6QMlDjiHFqjKHXLA2olPQ11gH5ppHi3zei
nSI4fU6PbDwZLTYFGPgGaGww3i71mbFXHVim0GdkfJ6NYse5IjEgf4le/8KdDgw9AS8qGjHECdAl
fd3hZ9ZVZtIEc55kZkla7ThUq7HwF+vI2qDEKUVidyhyxKfmcGde2OPJRwWHbGp3MfZMT5gHsDcm
EUdEN74AkCpyHTLioRpRecJ1kvzymwZUJiC4ZYzhLsvliVU0WU1VQXaMFRIE44hqbJ3P9E2YwQHE
eQc48tGdFN4lstKfKsTymu33scz3yhWwoaxBcz26YkE0IRgeF+SMft+mDDPFT5dCxCjPOaJ1+f0S
FcTfN7sudpLNIkZUa/T/WmNGR2ah2OXmSVIKEtOsZed6cG12wVB058CLV3TjX5PDbaRhicXMJ+q4
jTHc0lUby1FLYGVwJjopi9FnYqsykGVhw48nubwdaIF3pfbRBDzkhPZBmo2cthH6ZTq5ZpB3ZDks
29xxj42CUpYDzqEXbZBFnU9SsXlUCUvi6yjXlgDLzdmaFy8nPIf23Ul1HdZKbJwMjP0761OfICkq
DJ+RYSNDDqSSqbBPWlFkcKIp6LJWnzylYELLOY8vU0dL4NIAqzTBDVOnz5HddJ+8tOQUHr3kUXoM
sbMH+jIs1q9iGFwqZ+QrXv8Jwx+d9IWvT3kz4IqJdqXSAnu6rhyg3rshJrUxgectSiPap35Sv8Pe
yRJwHx+9dEEhs2Ppzx0XWwH5OAKlpc9gobh7ToexVEbdAI1j72RnY8Fe3hfCGz5H8gbZWLqp4TtR
gT87YyudC4ZXo/CvI5zfx9v6fSr8wVvH1XA1AJwr+qBbvpDyxnHxSI4jaUk6TKfWZ6ith4n8qFNv
RShV/vRM+NR1Tl9iFoCq/a0ywf7jqphmYuPnH+voWTiLZeRIZrZ6EJEx3OHQYOlDQEuwRIYvgr/n
aq2TcXBwJhuQswWDUrnoKoUahqqwTKvkFGq3aoxnR6Leoy4+7NptboB2z80YOUmRNAzgC0ZqLp07
JFBz11nq/rSbrimW7m9Nvk1Sv/MLr9DxD/y9+/2IVZqPjOWDtkHq58VckRp5jNB38H4MPtZr3cOT
qlbofUsQ8ld5OyJYKqL39BspU/Vgw1F5er8bekwruGeezi4Dbi5Ibgq4lb8hvgLufbLUIGiB0ZdY
FYSCM1eiuNw0Vhvkr6RIIR6BUfSFGZUyoLmHy85NGe7jaV+C+LoVC2HlrD02anREjRCIH14FtAOd
dDQKHFLoKdvNsg3xvu6V9u2BaR4GZ/EX13HqM/QM7IPijlXjixg0/ivTi2N4GvDbTTiVGGVyuBpD
WN6DwsIHe1DoVMf3GOXFncQFV5BRJEoW1uygd0Q2yIIaobWqWiyLBDsn+WWOkrl8PbpkXVBd3B51
7HqnFGtsWDL4mXV7CQVcUWoU/6ul4OAIm/4D+Sv8NzgXIvGMufubuAW6aLo5/mYmkDboLT98eBcs
2BbBvosj1Mk7ZLlA4xyTSTF7s/UYwugIvXfLKM7jLEDIhCY/cmI96Vy7Bedk6UZg0/FCiFGCQxVv
Y9EYn451+qQhuQ14rLwQyEk0c6ytM8Nlr3wM7Ff1CuiJTTaWa/9hTSlje02HL//6F6Kre0ipmHgh
OdNOyzwgJbmrk7rA4M+qpvx18NyV6M04UnWh73SgZOIdi5ExpozcPbN+SQbJJMvgN3e/0Qo2dzG+
/165U3TcvsThGxaqpa4YD76Zh03+AQLNTXoKp3E7SBRTTCGeLSMh7qyRccDU+wBCamhEszvilHye
HDRILTdt256ela2aDZta7+Hi6izBFsrNnC38FWrJgPHAsiXHMQAEkrexSRAFm72Uu+DMlKYF5imS
hDslxGJyVQR5i+ox+KgOXCIGgNGBRbja2nrpj06hd4vsYxD2Exw5I3txhUrAapt3CaHAZzD8m4ZT
G7j+vGyxNPPgB1CpoYUuuXGD6aujPlwsRpW6VhKG9xcV2Zi6lXQFH+mIS5ytFZvSbhbp6ZF2wYvH
OxMbah0lJy76lZIH8OicMnakROwfAfqwBLKyeKb7Ph5NzbGmJvHBarGobH8lPdN/x8eY/Guti/Sz
UtsfWQ63RwGjNINhBrda8TF8SNEK6L4LCJbtESkcbpzgXk2Lj5lVWwwROxtXjzHZwPfYKs8X+CBw
5M0XJcJeOZq+MFgvFkL7rEOGjpaMNL1/aE485AXBn2ZZ/ve7hHNmv6EDsey9z/pOM2+QrEUdMnuw
1DLLcCrnMI23THXsdGt1evuxtoNczx/7zGu1JnCZQDSytYHA2pYl9jBvDVjpHr7lOGNYJQGnRhRP
Ge3sB57ydXxZBeb4qZZ7+vby4amY2lqn6qikB+fCFrYdTveAD1g7HapE/L7HgwDbxnP+WMTznLh2
srGSeNW9q0l4FAt/yjtv6YWWC6DKzgRORqWBIPxq3iA821lwJ6G1x2fbIOH+GzpjBJ7WS8EVrywV
9Dq7aE39m8WoZklegMA1Vjre63/dnKUC4Efozx1ztO+/M872JViToHqulgGBpxdgzLNbv/skpLgR
vIpWM82Rs/j5a11yd5sKZHvzsEBcCexM1RhLO/2EXZvvmOQBugFgGhSFnEvznYK9p3FIYxQkjOvo
v7keUlae6WGl3nmohRjmDW/zG2HSpetObfWqbe0ah6SpfFWt7wAE3dHmH2Rjo2Ao6T8vxMl95Vqa
srMv+o+7TjniE2OQv1cj417gtYL+6cr0LaqBOnlvdCqiKI4zMbq65QpvZ7WWPJ4Nx46f213XRChL
sh5VQ5j5g4eL42PHdPNGzsX9RUnq1bvbYRrUJ/UnRoPT2TJsvu2br+awWoLubOdvgmmi6o/qPY6u
1JWYlugT0ipPl+5JXrSzFGsK/Zpd1gzJa3l/JfQ6RiC8hCKJS/qCZpU7f7iKz8K1F9edZYa93SiE
vt6W/grLypaCoh8rLz2ZsCEUyvFH0x5LFplg/PMjNgtq4LpcB/NtGxBi+uU8C+EPa0yzBqUk0zOa
ldNQrCVOb3ezlhZm+p81JEHzGPgnv76PG3Iy0QiB2KGzIK1GV492+aSUrLHuQY2Ptk175gbmAsKT
sZ6BX0OFsS21FTtf+2cwMbmdHwMFnYdz63LJ/yb/gU/6zf1E3uFzmm6/roDz6EXQwB4Zt9AU0595
OCMnXJFyMkBhNDVWcFcWp1LNIflIJYm3HQyp4K+aOBZyQpLOxyEdrZGILLhoggRUylckT2hidUr3
lP3niLiRlyycYRSuAKuFFyPGMuREZDE6pWF++EWxGM+s0fA2CiWkkvYdpSjBXR78NeWLfitK3Lra
5heo8i3Dnq2vySAnkWacO/hSf7mwGDu5vVTtHYLwg7Rx9TBSDQn8t+SbHmYqbxEz+fJr6IxVyDRA
6W384DIW7LkRbdstPrGrf+EWxGniNUs5jsRLkSKdNiAs8zTU3YdjH3qnko4N+cp2R5QWn4x13svQ
gWDcyvsdJKmoQsKoGC8029CmVovt07snGFXbJylA+fNzwnn8OG82UBzoyakrzn/2hikcVc3eIiXJ
bmaQ6hEK2rJ5KNxRiIncsug4Sjw6FuAw2fy2EhRihNB5H3jjWvjhXwrTTARA9Iq+7lP+cL00Ds5I
GdbQhRCc/17+LDo5oYX5HZqbjlLzX1DhBLov20oV2sT77or54YY2H0bjOHtbHXIm/fWHhXw5dY4L
JLDpDdrpOpR24CfLl4TZpDMlLxdo3oA7av+Qv6N4xrPRZ8DGZLKyXP/Gw+YN4QtC6qr4XPp9GlZ0
hXgTOLMGufFWrrYsXjiH7vvBAjivUIauJvtjjHtB+Co70fAfGonU7k96/SVVcpPjQmy/xj4xg+Mb
a/oI5osXM1X+WVtyKZg8Mcv+12iJG6C1A6i48gY8P9+3Cp+KhEPFgpkjG5sOODTmPrhg8iDmG3+5
m1mwDWWGtuQWOliR2d0UTBid5lI1h1QVQHC3OlunTkf1z7TF2D4srG9yrseLtJJoW6zrb0H79SAq
wRUD8seZNsgpRozxRyWbWlUjcQHoeRaiIcym5hZwhneeJKvj0PBCgIXrO4C1nAPT1lHszyRog9/k
mdMRkWgKTwBoLMFRf5WB7IskdsjUA28HTelGOiJ08Cb3Df63DZ6IDBDTyrhqvRgA5rgpvB8KDWEE
9SKNiUrx2DqdPJliCGXvozINGh7pvqT/xcnBudEywxzSLWOh5j9CYQEaqXEDEkGWYbO4eHIm/pYc
ZkQOE1GnS7x8X3DZtAYLfFL7DeXnSYfXQ5bgc/HUosGcSR4cWm6J3lhnO3ng14OkE12tEuaLqJ03
xzS0wQeIOmMQ0ZsSoP043M6DjSNGPy7VhsHhOindloMMrFmrAF9nW5zeYAiplwIQ/KD4D5Utn+UK
RVqMrpapuNFKHX04cHb5TBrgz0WAoL7J2Zi4PY9wK7Samr6+Bec50xhCZ8P/64zP3axSyh+lDKh1
VMIgn3LXYdCxUgn/4ZqqRDh/9i3GWaLuJ8sk2+mKRMMqvPKC71jjPlGXVFW+tRA3rhb3bH1BlJoW
i3MxVmA567a9LbwDGsQo1V6BvEgkq9dhPpjXPgM92oUU5kcdSI1MXE9EyQ5veYT1lNplXD6xs/r0
6H0zfbuXb7sbSVFE19nHst22sSI1EgBhh69plo0Bs/8j3QJ1nwT1D3SKW/x3LiZ12xBN+b5j7YCr
vNpJ66yN6KtsirCuoq99jYpt9/zTk/9hEe7Oh1fygORbGC15eqkmnNJmPvHQaoHMkb1HyKucTQOn
NP2e6R+jTW0VUlzdRElcaaIVN3JT8FxPU4t39AMemJOLYeK87sBKI4w5Ipd5dIuvevwX1/x+TZWB
Kz95516ZfieYNB6SBBqRq+Ftu+5/sPTtZz7bASoYvbbrSZHyUxSsWEC7Iwmvar6IqwsIaaByK/Gf
RWZy659XEtbtbJU8uzJC+zwX/Wc0SfJ4r/ooUOsSoUApjq8a1g222AeZof1YyCzyj95XsCjEmGKT
ZffNjXqsL+lbXavAZNyNOQajS95EoWNJG17IztTVA8y3C2gmSJZQZ+zcQCogSNoRsH1OwXhQhTXV
HzBTaJ8lihwOtW9caRqiAO50Xv/q1uxmpZd1HWUPQFkUefb6djShb5FAyYd3WNsnLlYFWrOm7U7Z
GUprTNt+7Ex9LpFDqMDQpCr5viTmcpL/BO3pI5L4wGRvctUTShm85tunvNNF9W9fCR7yiTVKIQLN
at8CAkwiu9Hy6HewONsoqSip1XeQpLmQU/eYWRv0pdKwZdEagSjwD1Z1TneCmoYtwHBHDwvMPvAZ
v+s4gtDvVsD9MHBvufuXxswFRtGOdJX1L0b/6CmSSnYGlTxQVSKCYHvOJcAU4sfQgo7AGiRJoNBF
YkKaGUS7uJE/zICqK69G4RUdZhoG37Ng+8K3jDadZpjaDNHi9Axg3lwsS9/uZn2E7Xg1R/WMyMVy
adQ2g0U3FGdGLs+h+0AO31rlm7iHXAkzlOKSX0ucQ4TMFMCUyXUrxP+yRpGWzBfMwRR9w5WaWDCG
+bIgiFtvf+tX0x1BkdWtuDL8ccMu9w+cEJe/aIoqKBGpWBuq+fxpRMR/lUCVwYOjdIqHitHiGzSJ
hH9Hcj9OkaewF4Jue8JLPlmiisi2unNvtOkGz3gOj/ibC2mu+S8pE1DXcDA22l5C1djq7yTkKCO/
M/l72ZWUh//L0BOvlsI5Symd76arqIf6ovLhFZmNL06sJFwsmQfIkBhIbOaja7Rfi3LODNEGbVct
hEpeMkODyO4tAYmlZBFtp+A06S+3zDCyh1lI9G7cRT5yc/XAVacCpp2bZuXRBRC0o+YN+g1yx8KN
glQOMUCJPykVMqt1N/6xH/cEhGpXfhoBwl3dwULs66cxImpUei9BYinQ6tK25PEEioU/xwRWZa/A
io799D9dOpTFMsj1yxLS8t3SSI7GIpAdX21Wvdhhog08BjnoJcvlpojvUi3DEoYa2DcdTnLMRbGz
XcO+NEIQTEksxZCjP69ncZKZz2kNUuyYeyWmHzmJVItPXOGqDjPLNbeHdD8LactecKuaW4KjrLgP
ct1RkYQEOm6Ie21YMrnSfVAd7U3iddov3S361WRdcJZ0RN43NB4tN75GcSs5OH4sf3x1kcM3R8nb
Ae6ROjrzKEuI3dqSuA7YvjrCmHuSVLvsY0lWFCREJs9lykzR4AY05+7WJnmG/H9XXjpVFIpWb7Jb
O8+JY4g1s8fT6nn62PuU5cFek1Z4bYQc6HCeRCSfxVLFgbQibzDAmJ4vmMlP1WGuMs3Ti7ZpwWoH
yEYx6vypQ2nltl9HuGUPA5rOuNgmHVgsUvkD7ntQObY5u6OShp+sXhLDPs2roU+QEGOq9rQ5Wtp2
boA0A23F3flK0I9/+UkrD7i6674oMkl5zqE9JY1mgu6Ym1ezvYPoqDeWPaOIPIN9Ug+2/hE5Tv1h
rS3WurmmSuvE9k27kd/uusiUunDZkk7CzSTIdpKOR8h7i3TH7JmN4eszwyZA+MiL5oHXHMO/4rqb
HsNqEXhe7l9ajqIZx2vmNIEzfw7yUPYDenzS0//J4P/sGJTFbRjhHo93+tples2CMvytmCA4Cg7K
yVdwzHdlTh5g3d1IRF/4JvAh8Qf3F2s+pYnhlTmQAHJn0KGLkcs6jLf2G3RREAKPfj6qPO2P3mD9
6Xl030E6siVTBIQIQZsMtFMYLiussrVWwBkRr5ehmS7Tl7v28CHU8Ng3BbeDQGL4VDw2BcplGYYH
RtwprYHex1GyE0v/zBhdzCGr/MAqAGsFIotYgpOFBx/A3bmiiemh9cM/2/J/QV0Yx0AU3+EjGFS3
Z+LF7rQPoSLs5vojXlXo578eiDNFe32xCD2Pw07TN07+JqNxrmer3WiMLtm3tOqUsQ8bFy8T6qPj
QYLXfAMO4w7z+dt8ZakXw5lRQhO+K+KFUDNshPRDIYVEYgpHozhglBFsH7CB6YpWLFyXpNMyXZyU
EcVqnxyT3bze6ZhgT7OdIzWeIapuyWsrQHmCbWm6lNQoZZFqJWRXq3kdzTGeKwfmPQOR0FDyipON
W1qzrGZqTpBLgbWzfxJkksZfU94cSymXbHvD+8NrVAkGvV5EPDsmWNN9yn925jUSVV6VTB3Aij2w
nYwMRkz7kJTdNAxHG9JdWNK/j7Yv8B6+jPNUuKLG07PBGOf5lqodlyCbw+NLWkEaY0siXQGg12sw
2u1q8u37zmW131wSeGnhpp/d8m/PyPR9+sU4r7NGhvVD8s/r2pDVtZeKZzkyZnSyoZlut5pOd+U/
+krjpxj8KqtEXymOvqUPylxhKTzvnaDQtQSnKZl8JoePxxdLdBHBvMZ2JJRoAhjlvqhEHfLonB0l
4NquYiUkHXW2Fu1BrtT3VyroyIzZ476AoI3bYx5qc5/GSV7CDxzYdmXwbXRQ2uOV8v0D4i4OW+9f
wOkk1qmFXUsdATqSpLrr+ZFRP6BsYE25iUlXQNkssB/XzndQU1XsrdEKUN5NjYAD6sRs0EbiXqtS
qxF+imrLsTM69vidJGrIbT+G17ItHy9m+xmENWwaxibsg2baV2kTge9Q71rja2MG3NVLS40mDDb0
T9tVEeM6/UrfB5eEP0g22zET++aoMM23c/EC+Md7t0WJCXRJn6uI6bc/1KqUYGnKTRvjohQ9flXY
RV0xl6ssvTMkYaJDDSnOZFvpnG+H1tZ5IPeVqaoR0kZOVtwdJ1Uv9lJpCrOvs1HKz7TqCA2OCA/j
7Jam0wafnC5iOoNBVp8RaiAT+dyzGg8r3TkT1riFn6iuKRmLSliQ47W4jMKLMgYOfs8eEKzRh8IO
LfdmBcQBUHDzORs3ytCHS7tTrJzgih9P4XGUfPu1/q7TFsdrkcfMCQKlnE1WKC94nI8h7R/NQABk
vY+t9ghFAtyIAeBSsM2wyE4FpwbrCwPDeW11upfOLwrbVD4gkzKafRYhcV/Vr+F2EHfDzv3DRyuN
mYmuoCEp5mnzTBFTLjhWgQGH/PQtsTLRLpCJp4Uh3q2WKQAt/ARe8L4gQJgOsinepp1H/WMHDhj4
+EJ5dsyHanoEbBqO3fj/OvTiVLaya97C3Ri7FjzRpjqt8cqP+FFN6NbCRcbQYJwswLoZC7ExPnba
ghdQCxgq6eNuThexXSAugWHEtnwGFxVC5iindo22FtPO45daWkmWnApgIQuZZ0GxWha054416Lna
stmC4Jll17lKscb154aEZhGklCEUDbd64B0K1xK6lbsV4po5ptIIWrYtY+7yIg19aFoH+gDT68eV
Lu9B02pIoxnZ2RfmENMwBZht0wcYKZtYO/SmMqk0L9ho/sC5m+oJvCzIENDXtpyCCbigoIHX+h9U
tnRTh/rGkFVNiGmvlan2xoeoweO16ETVcSJSTrYSLxmf4WLQuLkzsHFxi8KFPaERzZ57VXhrsnzd
rPPOBb7d0PPHdNph2hZheuSscF+uyeiWwo5HK7H2Rspbs5r5S9Do4E5cAMN2SLGcLPR7Wr8bMP+y
PpjEI7cjDoSZT+izeWAUJU/lfZwdMjeHRBaNaYs+9b50kWcNKWHo20I6XxVVTjHAhtpAk2L1sLZn
qH+1hvxEufzmIlSCp2HKS6ifg6EisThwKdUDTWaGspjFfJQTdFfxamMnP58wLvk3nmqdNlRaZBI2
lLDu6apkjXl3zP68xAWgi93lRBxB4iKQ+CcSRSoByFwueRTyR2Rdob7qrW159rSuxCXqJ1vJHfNT
sGCXrkAKDsTtyDfLx2LY6Y18E2tHQq4musMUHszfxpHyesc3WmHVYgaPDIO/Z4E26RoGtZoODCkC
S2SVNhYWDpKyAEyKYuD7wz5kte3XgcIoCz/FZfvxSNMxzO7+jMivelQnuyu7IzXJnW12RroQ1Sn4
CLX3ZUgMXZc2Egud3x1yy8uBqxNPWCA33OXgo/K5jWG+/l0lS2tAtmrOdy/rQdI6wlnrMHaGJ9qQ
ein2jospYFnKqFG1zEA1GFbISmIsAPXEPoDBEe8dWI2jsnX3+7f1AWCeDZAIAA+iJrazfKjjMMJw
zbqHdCr2SNvgYHnlVkyuVo9Rp4MzbdktMRWXkoFDAC+jGLf4mgS3EOX6JyWcvLrUn1sVWjE2DsnX
QLFmhTPBSxxUkdnO8M9j5ZOCvry5ihgUpKMaaMMSwBOgNg8wEdPjS3mIwfNAmUcmVDTSDqM180pc
Q8k+ZKWE72VFRk2o35lyIXQTuJmxTgmIOY9Pi44UtP4oW6Cq4Liw9NqNK4dDIgxqz97Vi7hPsKwT
z3rnAf1IJJ3a+xsES40yYJQo+yPk6xah1ha8fQVe/V2pHi4ofBVDSE+AfE1KjMiHQQu7xCQWQJnt
bG6T3gbHgyU9DTpZrRp7k/QNxX1pR8OQyHp2T9bojB7QeLG/MN3J9r1Wy3lNo/Y47Ru/evTrt78S
cM3cLw5zYeOpEYQwcfhMXttb0E8/zct9nm/F183t7YW1fP3SdqpGrM0s3ABp/ysLug8nDNvqPhAh
GeSavl+64W93X1OvDGsiwlL2mvmKdDSBJ2dRd8bLHRgUhl1V/9f/BruewMqbwI1cJqtl65M0wcQ5
3zJm2MLDiD9UxdH1/Sh+O3htsTI+dqpuvi/+gAWLdJPyKTO4L1dZRm1O3dlyZol4yaVpbK66f7gP
PhBZ/CePBOUEkhugvsbSrl3G43gwvSslynriXOdnT/EUNmbNmOXzrQEHWMtFzlgOGMrN1ymDnpnW
OFsNDvtajbG0/IjIkjGN5YCB7NC8EhwXb+Ys88up9aLwg02Ind/GJ6Eb/rVkTdbO+lxtAMN0tTyv
DIgEzikOeAEpG1v2V70FWNJapChy9wPLSy/YJd8b2vQiJIMlzqVfoWHsR2RXM5SEBhzNKvY6BF3m
CzRttwQ+J1XV8d8I2gTJP9lRj2mWjEbhIsBnlyvua5uTfhuCMKrzx7pICp2WZZyBK+8VG1zbnhGH
sk2RVV8iYYKj92KeyskEcYIfToKFVj8NCGzinGbwclZxLRmr7TkeWGLkhv8z10G81ym7x/0kmnmj
I7BZwIgid0KR0y4nC+BJXzFP+sP0NG6t9J1mSz9/NbvYF2aNtuTsJzRTBXbx+udTAdaHxUWiX69Q
5CxdZktFOSOcDGqWt0EZdCjPRDlCyV/2W2SNd3Z+Lnccb8fcQtP49n9WmJTDGzBBkRZSKRlIo4X/
Crj9kR7iyxJa5pBrcf+wGLxWrufpuz/aQVpxALabhcyV8W732+qKdDHFSdhoj9Cv4OCOvapiP4o/
9GspYdaTkAXQWG3WUS6wmdrBc0mO060y9Hi/Q1CBCU9QjaZhn3XUaK/ymy5kE/uQidoErpk6zo8C
aJjw2zIKVSOjwW2K0dTLE5evvyEI/FFkQGga70xxIotkFhRSWhUSPQH3eHwPOG6NSorj8RkSjRRE
gtnZlAJ4Y+wTcsjq1xBi2szz95tnHiaXUhJQipwCEDy+eWfesZwj8mOfAEW8ouSNze+50KLbyX2G
Lzx2FaN+Af2iCbwjctheBKWV2TDOFAe68Dpj+4RnTgsMsiB1hBqKpHpZc+bMZ83bKzrtXeaIGIqk
DKvRDgwyrvZYdqr2zt8mUaO/JQdG5gkfFVNGt/VVo4zkf7ResRg3PkemZlphu8Mhu2VwmnfwR+MH
DONdS7lOa2skYfVM/PMphJqBqObuk7D9eXs4qrG7FXuJ8YUE4jAx0EyRUrzTfw3Epxh0EH+JuAgc
FSwG4Idj/k2M18tI53Z0SHgB0gwo63sl2hv4VQSsRIjufD9HKrj6nEyTdGzoGlpbgRVkh+6PpBCX
2hQjRhsBi1Vq00ItxA8qVdmTkv/myHaSR3tp13crH065Lk4+4HJbRqnnfCbmdJUfZ5QDMHUSONB8
suibLdcAz9ZOEi03gjh4APekK+wi2PZ3Bivui1lZhIDlUOZ+/O1BOdeDMpVnQAG8kcGEfJV208MS
XzZwB3xDIfCgaQZwxBA4JE2GFisoy2iR3g/mtuyUD5Zio9VFGmRIAe2rvAhnTXJCnsYxtnE0DinN
AXauOMgCGLZs9zH+1+6sZyZ/VPjjDS41jBqqOdPBz180leiThmfYLoo9AK/HgiflZ/jkqTLYQM66
PmyIWrVWk9siNKF38bO00mRDUrAw0yVgnv82ETzoVK+IiQGZ09tRy8GoY+WG0qByB5SUK4LyyiLq
7OqN5sBokEmZoTzcX7yVK4LA/AeXAck+wxH9jM7QrSqwBO4Ldpx0Ob1R4iUV8abmceyoQZtEKX00
FOBezpHZ6ysOtHiCXlE8VQ7YMFb2zV6UVo0Pk5++KBzodR5UDc05+giQ4OVDVNTBO0+OsN/rYnCb
DhtYUnnBNowAjVfSDR4QF7aVLLXkWapzRZQEEPqwqAize0iOtzlZaCBM8urS04IoLilo7ia+Uk2h
Cv2sjMw5rpIIfRz6gnAhqBkG2ALBcqbI9HvZ1B32iY7KNV7U7nyG6orPWjkXR3fNRtgRk6dToB4S
fhJ3H5gWQFQR4UfRWxFCtp06F/01W7vjwMuHClZvXMUJsqAvETA+PxhdP7KDePaou9+sw+5M/epQ
mbUU/4jvCsodnAwVkftq/lKoZfb2df27CqhWLAoabO7KyAh5frHhcC3w1qMlH50ef7rzyKlQHvjh
rYpdOk0faguHBDBxkdMRmUwevKrHmLy5VaC955JrXp4NXHDTg+Brq0Vihlpo/4VcFSG1G0keuH6P
N9H07yu/ly6WmDY11np7S819+/Zf1Hq/WS294/XlnFkxy8uULwkZXmwKtM0dDp/uZTrN2mZqxAbt
ND3FJg71lPtqKCb33Zg2mjiXTNz8doR+XpO2ui9X0+lnTUmYWajeakmAeCsfEyu2W4lf3KcUtgxh
5TJwkwIGfgursYh0joXrrN/J9FK8U/d+srnRtrqU8+WRUOd59nwoxgTES1RgEae6cT1OmbtX2VFx
XMmDznDTVcDCQSO78q3FVjnhhKLQsKqi1EWKpgMYexBaimD+eGY1QSlc6gfk9xEWsn7AbDjE8HSq
pYjZ48aYgWDtHzj3sDXKCBqRlRXDZTijQVDxfy/0CPiDCFZwNC/Ht3NGyQ6ODZbboWsKQHwyxwW7
Fwy5Vsc3+t23MH2BtCoDImAcuSIbKJA903TVgcJjTDhQVsmLO+lvYTyS42siuSN9F5+28ObzVfy6
lzOTH0r5oq1RnO92Uq7mSSQQAo7x7KiOCOr9g9qEoR0NykFCM9V05o8BDI5aNhibCKtYhGe3/mYJ
qIv5Aw8bxDyNurK2KZECCCKYzpegbqcsOrczbhcci+MJoQLUHiVw8Fg6W4rLxzaStYgXEs28gsAl
pK8IqiFhmuvXePqLJlat7yIs9MtTo3oQ/zKd2XFK/CTILgUgWfnr9RE2b4VFkIYxTcYQ2/0geb6y
n3u+cRW7D31DC94dchOGakt3dXTF6vAtqTVgKcXG7HZrNWchhcJiF7/v/72AAOA6mJxD5B1eDrqU
d1NbWN1brBj3M5lQB+65O5sH5dXy4sLBdxfHAgZGeaDd2Na3SwyEsnkO6HbzQJHJ+uLuTS5Nvuct
saJIboIR1DueDOGcsA9Xb4CHYwcNr7hC5ld7mBPbj5RF5zUtlkmjRs4SHJlPW/D/1vlr3XpjZWhw
jaFEWdRG5hW85W/L270yLvwVoeSga6OxoYhms+VFSDDDd1U29Lz0PZTpjbNZt3RU3TMUwpLmKGpC
/0kFEMDXo19z6v8pbH0R8JOdj1oMUXkt3Kx1jLIOx914Mn0VWG4gBpN4lqUwOmd9hPOnbYKb66QX
5R1P+I7ETjy/jMWr9oXRHPRtz9GewCh5ALxtmhUNWWxIPmDAi6bF8xdYM5s/QEqfM0hjSFnRVVZb
wP+9LVxA5whsUVEO87bgfvJLLhwwBbsKhAJCnDaEFBq1juAZRKRohZ22RKmglHnIMM2zQAD0WYEO
NEBXVG48jWrHba5LYqgtWRN+FWxXA99cmblE7jGP2gTDF9PJNTohrbexVXHgA1fl400ewF20MxEG
yD1baJvtgVxgRLEKuYX8PjXsTstLvACTG32coc7eetUKSTdfvch8dvE008aaMJII7Nu8cs8Upa7r
SpSz7uby9nNeK8kwTq3Mm6EvOyXXCh31C6B2/UmJcConzsyMvgb1LofeLeROO15pDx/E67cKiN+e
6sIDm64Gs7+39H4wmvTJ7eWL6qiZhPi3oTnkIMExtvp2y8HAbnCYYKLPWvIP8HV/wdTSUXteKnqc
9WV9wpQQnLOm0h09vSctpyeajVfHSBTgdyYHTEWSEEFCdjN6+nHDpgLUYsP/s34N56Syg6Ek+0wH
filhqvZVbXyuT2ZwPm+v9x/AqgXu5cRK3BZbDYiloPvPtqo/PF8aooCnO85/3diqDwgSbnlbywh0
7Z/FAT+EnCaSrSKxvoqNoOWf3Bdg6tv6nxmY3VXbODSOYqvs9DGuea1bYLuGzK79lus+0XWeUY7V
gI+maTGpXLOst4vft/M9Sxr8L6ubgytprVG8OHGY4iUPIMC6WBOO02FyOBYttXu8AVLT5ckGYFqx
wSZym/aO+TLKwePqBKXL9cZTa/N9o1oKJoo8HQB6ijeeKaBYq3MsA6P5X2X3+6yu3DNmAQWVXQ9P
6v6EoQeW3EFvp21qrOtEzjw5IuoMdQDeNs8SI0Fy5o2x+IvRwFZkyWBiW+hjFttKVIxy4t9UJi2J
h3/g42taUiMwfYErJpiQLJ+c3hFLIb693PMqW52y75A1KyU7/Jlxlg0I0yFO/O673f5xns+KFmbx
2ouxh53ScPEn60XRAPmgi9CsAR+l9SpC+EJChNHgt+Q84jBpI+ye6LwOR+Itv03Xj2xz0CpneHxM
728aVA2ZHA+hTxZgDJ508LbMyVgP5My1pWHEYC2gbSYTSHbk7uHjYiCbXdX2vOB2SFR3nqCXaeZM
gnk+4LEBWBYjglJUZOnkLTA2+1MaXV47qPucADxqU1fgclae+tIBvIrbB9pnJX4aSXgR5dHKElXc
k2sIAdVwYHuM6q7pzzYH40Jlu5la8L+apUmuncZKTOnRUxjkxh/LqBm0jlfjUdOCGFbHkUa9kVwv
LNO4m39F4I6wmGeVn0Ty72KCBhjGnT33iP7qTbI8wyqEscmnWjUCzZABWqY56OSpUKdvsCs75e1z
asaL//OnsUoGT812Lg8qSgabF6EcRAYmYDBQGtYolmuK1PtqmqbfUsY37GCzRNKvK2gPlXMePv19
i7VmlP9QF+aVtMd6jZgPkwqUa6UG9i44ep26pTIl5LLR+HxEPRwP52HdBR+NCQmQOiu9Z8/4Xl76
VD0mKnxaRwex+BByZS2Xy89aqFtftx9nyciLO4fDQ5B/MwsjcH46yuLuKhEsgolPiMqvkneNa7X0
OEeTY9rqvoj9DfgkYJ3TpKcRJsCCQQxUKtf6+GMfC6jppxP5eHBi958Y+XXp/8oJV9U1oXV4VjF5
sBD4UUCT5tMag3eKFWUBJAowq6vCDCg0+SocTbJqazYyel6Mz95QHoyjEuqXoCbn2BeSiP3YyF+L
gbWSERBPT90q6aAFinaJ19vvDB2ggRVcHT7+NeQqmIQQKZO7ua6xPbEd9JlohT8eUMMmdfPYqSpa
Rspznbi9ANfXoVQvTUYmHYRn43P1N9aP/opsl/82GUAfcRn+beMCwkSZ/rBE+dLsRnMKHrKfdHot
ng0F3kioLRVLIBd7Ca7E5sZWPNh+MQZaYZqghbe8rXwA6Q2vl9V1pxt1vogfBeM6tAaw2zQyX6ZL
lqPpEHsdDylaUkZE1oQHcZRLZi1n27CqawfNdOHhlHja/vPRqDxbBssPCgvUATE60zfRw8Puv8Uj
O9Vf1rD8NnOfB83G2Hd0UkWQDDCfAH8DWXESwpV45OtTFq571QwJ3EJ29QSm6o6M6hLekIer0eTA
LiGuDFNQr5TOduqEqtdrtEtNOBRseDRtzXEr5xxB4T7bd+FUVSHNNYdWFUOjyq3/2mbgEAuYVSjn
7fzInMe+jb9er0FjLhU5p46Odf/x2Lw0oogJYkyck+hZR0r4geGCDvbqb39TmWj6jak4fCHCwanQ
hs5ezdihTiat4Y2d7Zwp3b0nnzk83eR0du5FDRggxHcsdWhe4nwh4CfXHZYwVP7KZmSoaViwSY4N
P5/lsSIxqDP1qGtI0YCZrqzF5I53ExtwEx7czD2NZ9WTR41POfW8pb4qtvsCnhsT5W1mNEKCqReI
pwFYeyhiwyxXDm9pi/XBU8Zr3WEYOqoBQitDEhYZBvvwQigzwSkIli/Lb30GVC4NXViPACTEqbg2
yWey+evfnrwUZnTIOvgspIeI/9ja3i2whk7DvnkhzDb/NTaNPp+1C4+mdM1ve76Gaj6v9qw+xHpE
AgvC63msRwah/c+z4RraOOiqeRodN/WVbXAX0K1wwKXAZwyEwivKGgLjPvjfhnNc5qRnQTASZtzV
XECxnB1yoVWa7FyuJ53hBntTv4KZj6HKvAjllCsq3YyeDzQtJm1IPSZiNIhQai+h2pgkzjhgnUhr
fjc4LsA302P08RFTT6utlIgMvdnk+Xi808JzOr90DBXQ0HJOn+KdT4cgdyDoQUiSH1XU6sn7FRdN
JzJvULeN4MR/ETBxmZ9ZJ6lS48qHpnihq+0SGBDpeIX4l1bmhnOD4OBrABxLexebhvhnhBHBgcfr
7xv2ln0ZP1CzE+2XQQy3uPXcKXVdejyTGRoM19ndlJIFt9zub7xRZi3Eej81+06iLWb/sHTL3khu
1cfKqNR6oGTgFXVijuvTAJQc4l+JevI6+Cha7e7j1GcPD4cIzBFmDdrUEP3K4T8tYV1p1glDSmCx
CE9hwPkIy3oq4Ktm352RVCn0xRUqIB+L73vFtK+6YcmSoHIU8XLA8wos+QzJbQuElyUbxlsr6MuR
2RW+W3cy3MZbdOLw/t3x97/dOXuRzkZydypoHrnOpYFbj/J3diqVSW9kRDdoOVxyiBbw1tBL0Yfk
GHrabJkfg7IH8XqWSFnXJRf14ndIqYJQDTZ9b7WJ5eBA/mIZ8NyiuIUXeMLEPdPm29XsCbmA73Hi
lid6ObNQLpvWxEonvL6ru58fccVBhTa1QtqYPH6u8blWMgdcHMk3xiEhQYNucR4pUQqMfdhRMjXG
Ecg3TjxSLxO/yhPQMjuTWAN45AY6PHqQqGC7stiz1nV3O3PzTHFtVEQ/OmVE0BAz1cbsa5kexSbZ
/HrUY2wjZAQ+s6N8AczniEZWzoaFFsDJEWm4CwNIA2w1nVd6VWEx098ydF88oOFQ4dJQB0Nlzaia
bbfl//WVQZVNi3kNw4we6rsZ6jtxxV93e+eLSr5vSyfexnd6rugCyLa3GXalP2RgANUWV5EMI1iJ
j2hPRvGoCqmTHBtpbAPEibRFuIF/g9CBDJAOGNkyYU2DIkLfVUOwZV4q+V4/IOPgfpiqDfUIULNE
bM1QuHJ4EdX6VfAfEudxl2yM5LUX4ipHBocS7+AFxWXJybfiTYUU0C2WqPloYgav0avAKtxybWPH
PAzBqMT9P2cV3GwDqMTRJ9VUPfS1oKTkLfasoU2ltU7VpmM3s86ziZmmWaz4Vj2ISLlIkkUI2gjm
9ANvuG2GN957Ox6shrN42iHUCLMOTx2ZUpUaNT4n/JJvE92ODTIyiFxXpCl6ejTI6P3KC7aFUgOf
lQUNEihQuV/TdI/4zqrcIzIcYuEIz4qHxHlvYfHSbBL8SUMjEQ4kzHCvxCmn/E65K/jeUPmOH6Os
54772USRIiwTPTGkxHOC18lVmo4+rk6S88yQiOSFPc85DMuER2Yh59n159G1xseIYCXxmN45Vna7
AZq+X2lCT1bCmHDYQaivLUnkqMCSku2hwlHSy4Nz78MqjVrJ2ShsAyTMXDgcRe5yuj5/60xqFy5K
7foew+6lz6vg/A59o0TwveG+wzw4V1lYlMJBkvWEi4TSZJ0fNLXBTJVwQVdC7Emaz+Z6H4bfLbYq
k9Lekcp8E7AhGtDXHQYnz6j6cElCW5Y7EHCbFEiubwJWL3Sx8IDm+a4rlBCPf91mrXHeX02tz7/t
0zoYwFXvCuPuRWfemt1EsRNfNfDOGs3e2UHNlgMfXUgc/RLLQxCBUXRop7iylp7ZcDpxzbenjk31
pCSgUbnNflsY6ZdmTI+/NDNv6y2WATn7qgTVnyh2CeV3n/YgIFXrp5qwq/lPapxYhvL02Q61wRBp
Lu2VbEGPo2uqLWJzu6e+vd0c4+AAQzZ9orRlKu5CUryZjd0eIfud55WKmI/RwV68wbYPqiBbMxFN
olgx2PEAVN5aA5LQtOCwxJKec7nY+kCvUAbi9wsuxosQiuJkiZi1SVMvXMiTCTcoiCOcJr8N7noR
pMvb2tR7ykcz9R0UK4PyLTeE4WugAKjXrxSO32ioFamXC/x5L4v8/fToLOhyBsW9IOuPqy7cTMbj
nwR6MCtOhkzFnsV3Mfj8Fp7ugSYX2M4NnOQAddSwsUGJPAiXCxCidSm+FXRU4mGT4GHLaovcY91/
LhuYHSk75JHryA6oAW+gwSqSQOKtToWOOuNN/4OkWpMn0YDuND1IqWpX7bF3D+CNWUOJDmKc7pT7
Nq45ae+BXpCBT3aeXTV6zF3YWwAydRQkGNml7yaWk6QQ+7BviTnrJoUnZi9sYn/hQZm0qdc1p2XX
t0LFSb7E1fKY0Z7y3EGOotlMuW8xUlUxOnedISSFssH+qQaPGwHyTRx/VeRfUX+zwUnqIcyt+en7
TMZBDz1QxX8RbhQeQIjwhpszfKHI7zIlE+KCBQhO8Jn355suGF097Phh9NqKIsnugyxQ6t2V09G9
QZKAs0jzDX7Akh96A5vHqVhBY63oX2ZuEco0/mxO2SfWIwGwksHKvvsUOoZrgJoP+AaGt9PcDfwX
UKSWkyKbjcseW416449mOVXmUvvqgCDm8tLkJmbeEnmV9boGXxHFyS724Sn+2QDgkCkGbR/TbsIG
Htkix8W8egAZduMu53inxyokzX0XteJ6FSd+DDKgPmoGyw4BhC40nWOnTVf1uYAroJjyo9m0mYz3
Yiav21e4DC05X4FQGhoe9XlO6UfF04fi5xKx+S/uLKYjS/3OKcTZnaZtHrafZmlXsfldGkxRDERv
NwvwsVm19qGYI4KHtk+vY4+iLfnEmsIIyWum5TVR+/b1MUc+ViANCoUh7Bfj3oiuGM5/ekP5Lri4
SR0Xc5Z70NKAtcx/tzHxmoWwmLe14KDZLPLD/rNzRZ/dP7xgg1sSTamUYVtYFu0ZZmlcnpNe6hUU
LlNvaZNgt7mAHUjF8xikg9s5wV/UklDomwMS1TyRLveFETkQPMr075QonPevRlr3Bj5USFHiKqqn
hN5nu3yd+v3j1VYhiMFr7kcugonRyEDW8rkG4XiR/0Quam/5auqx/LOaYslonnH7U6TOvLsA//dd
Hg/llVGkD6QRFkPVfTcy5/D4xsLICuHk2xQQWkEweeT/W1h04zL27qxz+AN3TG2MDqtDxd2oGXAt
m9zraUVu5IPvTA64pgfIWdatP2D25wJ25Nvs07TClA5WNPHG4EAX1zC1Gk4BMcPQYdLANwPaveDl
MvUEJMMBZkldkio/VcVffCK+VCxSruzCDvNP/CXCYYst9Rbme585xH/KehOaBZY0scqS3ciT6yyM
vuQpkXivuXO0S+GvQcHrsZYTQo4zLOWdzOFPRNbfCrj+cjb54P84DDnN5ASpWwhpPiwf9/HETw6z
YkoYWCoPlyUWwtfCRsPsd4So0KeRu+cULaT3RO1jXISwvzJBP0EdntS5Nfmj/OSUatMsffWRliid
fzpxslYFWreJNmVucPzaq08pW6/1iBoX47KCPeBvNTDTdvdvQ446omRpTpwF9WapoOFX3qVQAJxr
wPfo0URfrKNb4oI80u/jkVqXuV8bCWTQeFnnbCtJ1UifpSbJTAX1ZtdXywi8ZMmhZq+0ZyRxKPdc
CvfKTMUFk4NIpMaPmn4GkKcgJAm9MOe9D6XmAQmadrVG0Y8LKo3q9Fudtov7t5YybT7GLNhyxdpf
JA7oGHXH48td7DnEmuRZFgebSMypNpgm3oSK6biPbP4vCNOrXL1NY5MX8fCLr+QjEBEJGPmYrvfz
6O4HdsJXVp94GqfwDyc4f5qDmNhBqA5fXt55fgsb9WUQWSdEGha6I7vAAiSSbaBXMB9bkE4YqiRU
4+nY7FV3H1WxkwvNuRy+cV42yWRJA2sPEOU/gO/7rhyG2nvUOWlFRYZ83MoH6/1oHnLAKb/zB5YZ
+tvT4shMhoJLNkzlez3DwQSuw0c3JQzA6cqmgQ7fISbpH6WhUNbP5y8jfdoX4E9rZ1k5mfa+KP8E
ydyelqtwJgyJj2Paf/QQ+gXo8iMhd54COmHl7POavaO9oCbBeOoScYUrRhxsZE/qvM9q3zRp/Ca7
+vKmtZogBypFTWNKBcZn/gRBgLUQVtbTCKVqp2qea1+Zy+E2E7AEdi1VOAwpBc5Z6Ti6nqgOiZ28
HvPGDfi5GOWge5Yi4e+gMTYrOr1BTnKmUfqOmx8T97So1ejmonHa0KLXQ4ScNIc0l85YZW/nGy0r
845ExQhUkmJaQsI7ydortzV/6aY85maIUHke0y9U4d/NunPSPV1V3dvOwgljVo3MnxD5SewDki42
z6vQMlqspHt/fM9cpzUucBDlHKDDF4ws1Mqlrsj+OnrDERzL/uh6k9xZfTrsKBZEIcz06uMdyX9T
UJvNpdf4AER8od2FpCS77/CU4+diK2JNmFySZq0WnvUZtxEFnScKZQ0Fium4kKO0cUiTu9R499/n
Cp9uiEz5qY2Ih9yy8r672tVqFRnYS558srtBjBM0GrSAQGGGztjj/SBzrJWw2fdYZ8uy3ctb28MQ
8VyJXzZMN8Irht9HhkRRCIJfIOjl5vFYHpaeqTV6832teW2cQK9c3EW8+xUj3F1A/RAVpbkE3unj
rBJ1aXNUX0oALNDWn7MS30XkZH2/NpTvsHumS3nLgOyoK6UOJOF+D2vEnSALmesbsn9YftKSu1Ll
Y0t3Skl+P0BM/R5cg3YO46q+JPE8n7snCWyjVbSz6mJMx90SbqjRu1Oi5Rz0bChw49SoP76Ik1WO
Iz93jA9JRJpmPf0wp633Ed7/meRZM3pFdd6HyrvNXsU81C7hHpzuvRu1YnzVuEaj8dpSb0Yv+trk
jLZTraLz8JysbCdRYRZu8OP2RnW6Uds3PB/TGZg9L0J8GxBkeyrBUau5WBLZ1kQ+Im6v7sWTnYPt
WHArcydGu2Zyx61yKkjlGSqkOcexzvJn+1Fh8UCWGtK6y4mWjcAOk0bBQIfLObYnIzPVCsjZ2ZDq
3YZVMYJmarHmnoCxjlS7z+HwtTHU9r5ZR53Q6otJmXWJbAG/WUuySxF7GOoUHG/u2jOLgUabHO7s
c2zCTf/f/n5orHmfXraF0CmVbrVBYnCflwS6U8+oVSQg1uqk/Gee2iSekQxDbFG+MjtEe2VWy7d+
0nDTJOM1LqLppp5X+vVpjbChLENv6Ut5o5nb5mLVXeewj2gFnLSvvu8YhUGps+Q3zjqWt4ajKYhF
4/SDEeurYBFVpJLBsY96rcnsk1c6vry0iBY4NtOpEFj3nxOJoaVFNy9gAwOl8M0PDQhNJ9dj8iHd
FHC0B9O53PK9MLktRGFFKqCwW/9XRqGp7S7w+IXXUsnaNbljeQxyz2x2iUrLyHg0BhlYPgazmrjY
BCTI5GtKO1J0aFdIi5Ms30o0kiS4eNH+Xo0mjSx0Dv9dl5AZXYWaJhWDadfoRBzRRKZuE0otQk/w
W6jepNoFXKYMzdmvr0UjVYYqOmBVbusShQTHJcQZBF6V1kluKga+wWFfB87YfA+rBRqTwocirAA+
KnwvgzCk0MC1R526JCwg51ZaFk2QHVmCvZqgG7jp1cySIx6eu30QhMAMdd1uGSv9JFC7CsImmKhE
8Ag8aei1fSjeRTAHlt5JAQLlBQ5vN/ZLZQNDxHInhhqSR3C8SDRpIGwBm3E+KMwhI+oF058BfoyE
rLlBSDPau+FLY7zPVoeNFEvTMAlZkiqUVwwLJExStNXUA/Mi+B6AHNxyFBqk1FixZNDuNVQtbu7u
x0dBUJf/mUyb9q8K56sROhlfU6f9ypUkBMMgfTM5NujXqLsMPhaBSmJZJoZNyRX7NlEdHUIc4/RG
vweido8Cj/XN64Dk98zmcIwU6aRa1ml+xcaw2JHadljl+g1QsO2DV6y/tOSqVHPMTeqxpGjGkmBn
5anb4uI1g0yPJj04q2PUS5pW9IAvT1bZkMAuiI1ge8oQy+kce4r3PMtFqhO5GK3LFonJGDIWBObz
cX6I0xwkOylX1m9risP0+c30F/DtZNZxTxfV2sDhBbVPH1pGTYTYfDz7SwSlGvlAtXHlV/feiRDz
rl10xu8hLokcwGdUbySll0cbfKfwTSykpVZ3REF0V0WCLQuLoxO/ie5Qg3JJmRfs80g0Kuma+H13
xqJChDpOCeihK8jGT7ax4Nks7/78+y2qzzQjSlfGsdQPu7/OtU+jDLIUaTwMrpkG+Mb2LVfw2AfJ
574tZHgZL3skIn5U9nMTfhH5f6ZGtK809ycbxnMSC/GAWbwMyz5udgyx+8ToeERADFGVeZV/EXUx
YDOpL1/N/0tjsmF0ShJkb7h4dqir2flj2LOe3v9qtwLjkBOVoN61reXYXll7u9+HHuvTu+k5uYZM
mOIOl48dqhUhLek2ISLI7EEXMGp6B1UcSR2WXvANl/04xf2HCYWp3fOobuHy5QulcZBgyep9xq0F
gRkx7NcL8SaKwYQN18Bzt0mtuBCETFEeIi7i84fg79sTAdypLrQBmbR0XwGGLUfgmDFIsj1+oD19
EkZ2WbiC7BIcUuyJDeU7ySvHHV8fCu0tCBPv7WjmwP/kaIGbYQYRm2XBNz+TC0pdCWUIVL5ZOOLA
gxbNPTVPnUNfYK9YJpeqtoiIk5jVddoL70k2tV9ITu6Wcvmyt4+dAkGYC5kweg69tdq5N3tP9dgH
LSiz5byOiJvi356zfivpJ51h8FOT6eznkcLcg+ZPw9q6oD0El0KMOxDYGXrGbczKNtbw0Q4JC4Z/
WEETTEUp1fi6rysK83qyrWgROwhSeo7AC8LNlUyXP1NhmLXECza/t1PNle7SjRfVZ/zdvsiLHIRy
35TpCjIP7l8rzyNqYgdVUz2u+mRdwk9c/sbTfaCe9+pANpRsQWJkxScmjDn2F4q1rrB+9EQw+jBX
dAEsIgj4y5EGFc0Jikcp5JqtPSwiyMHPJorjDUgOaIc3F3x2U+skjEuLHriL334UCSjhlCOIWzY4
yeBt7+/eZQznmw7yZkKH397WUgiip/4bSf+cwNlOJCTj22r2K0XzxS91WMxwkkoETZjrDN7WtCtz
o6TnIjYkbHwimDZFftm1IsqMOvVQA7WhxuEGGogs/nsf+k0CjcfUZjy9wVdKmRr9+VXlDJDnQX7V
/6MtIFdLkSNOwmSZBz/M3B+SaN4CLqadZJlqCm6g9naWxNAKrgmlN8af5yND0E0gBvrn7zSVkuC6
3oSV6vJ2x8mSOpWbOB1u3wQIriHvl1B4YZoYvGJcsWYvUVaGbuA4fI5sr2/Tn8+dV4nCq7CbY4+E
gvLaRrnX73BvoWWzTxVV1RVIiI6AAtIl/aJ0Yfu9fi1N194PWqpBKFqgoPa2pv7kSZiJHkeGIc2q
XbnkZOY2tSSXEa2sY7ue7P6V3A252YJAivhk5enEr1LzgoNcb1udKDANqQtl14VuxF/FyZJdLJqU
RLeVPCClgjQiVods2MdM5Q8BHcTVVd+bTrZ1kNQ1li/SUm2j+TADzV37s6v2cxAU+j277x5sPVLt
D+cvhXceK/jV3fYbYwoul/nuP8hgTOUeJeMM8GXItK9NdmGs1ls4gROrqoFiOd6nFsT+HYGvjXm2
OeZS19E9wo2bo8xa3sM6nx42wSfEBk0BTRZNFMySNi/JDTPyy6NlLf2MzmOmZJ2lFcZfLIJV2teU
E9ipevpRO3TLdpnnFsL9U4k63oJk78v5QtKCpUWEuaE2q/dIN+JUBTSUz4mfLTQcpj5sn5gOcSTz
62LE3+Orn7aMaoXZ+gJxg+nJvgGmqgHsNn4vDCtSCWMMJuucbP9VY4XIoKRu0NGzGuplu0XjtTqT
BDbKdyju44g1uQXc4WClat7x79LJz0zJ5SjCuP4vPTIKH81gO6aTDzRatvefeoGLFtYuLB7zS4pr
wMgvJK6Pcj6ELXjLXD8bqqikLEOgk/+qpNPyBZmsW4j4hQyQ+O5tQv/VQp+Zc0TGqnNkUYG56qbL
wKUqQWlNCs+q1r0l98whboFIK0BdxZuMUHQsLxVlu5Dcir5BD3R3+ua7rLLtfBArCGVPeY7DtVa5
XaMHENqh7WgtXgp5ut/mWi+eR/Cyo645DGqftFHc9JsOdm9X/Oj4EKCyJdv7owYirjbuAnBySpcF
zmW/WggYIoxzLyG3HwFBoP79qa/iR1vORLnKkeZlHdzbdCwbwGVKvi9nrlRbvnbFWqUn3Xnqzr36
MJlGahis/fIRyOhNj0Bnd8kvCm6ftDi0WEbNYQBlOJl/3mFxfp/Lm2jZmRKI8QpBAyb76V+o2KL+
O4BYV2g1ITHIZEU/z39rGK2yZQILY4+rch7p0NfRGf7qW36AGrZLIMyALfKC24b7IdJfOliXxTUC
BEP03lSbwLeTBnOb9s5Onh5P3gzMfcLQqrDSWp9NmvizC6jWImJo91M3Z8CFVdV60DQzsOcPDRbF
JQlnnrDNQ1YJRhS4dzZWTXnja+M+IG9zzDynlrqU5P/iMdniBcPQyWb4WIJVJ0A5fS9hWTjc+8H0
1l3BxIcezNMD0ColHL7Jb7Y7yuAGQdlM8MvYYWSp+p/IF0uekpI20xWa5IMqDTrMOm9FGgw1X1+v
CdBSQOD3gu8XcgP+PtG6NR8ucuFGfFdNYDvn5GTeDWXsu3PBIkmuqfi0mZe1VRfTOGYcWRZ2sRua
K5B6YwfNWXQCQozkgcjf1FZZJig7ak3lqzbJ/OwGTWw+Vl7o9NktQmApol+AkiKhIKsxEpBhcPYp
behskTk0u4z5zpI9gh30WlANl+kfwtxgrYJ7/flegP3GeGysFnlpsVeBwdaYqMIvI+PeAKReJC14
3YjGjDibJ//90UyMbN56uaM11JK65ZybAKG9+erzIAHyI99hIsfB/en9jKYDHiGIcK0uj8YQwjHu
YFKbcP2ha4wYMWxBTcRjpDk3+06PV+GHspBThcrzKYrwe1zDExb3t4MnsNfeLAHHFna3kL1C9Roo
Xj6Jh/nsFIHbAh52pipuLFhWKjI61I0CBw6dT9v70zFAdHtA0ajktiiHUJkTUa6N1vTwD2IPxZG0
6rkUM/HWpU38Iu9XWdkQCkWyAF0at2aW0OnkgitaP8agiTQrIGLGBti8dBuzKwJm2+C1vK/+shye
XqRUsCz+JwUbXDXWRstwACgpglk7nTSXuu1qROp3vcKIbh2HHZRWXmWnY2KSLIHKB293aEPP6svH
hkf27XdR1OwWgLD04IOCFa9HMOEOh7NDuLR46K/iTPG8JYx3pE0RJotVSoj/eeQhGDWPmgZNMhab
V+z4EOz27YYLXECtQMrbuQM/2WPtuPm6fuig1AXHkhYPVCstJ9eixqgli04YF/97YzskAvekX5dd
MgzTF/bR1tB5wCpN+kuRXVpTBWpWSl5olxEErbY/lncCVFbkXxYJGfXYMz53LH5E8/K3I94UOgHy
CViS8g3069h7MRbgdFXtBPBCi8Cw4vGT5HLTMWzsn70MTZd/+cvVZVmdDO38pPW96g927ZYNroTY
9SgimkWVAQ37DWic7voVoiP/LtYS2KbpPWiIiW5HSMyZagr26sguL7e4wx1JgdqUDrSsOPN0dYQf
PGQXI2gJrLf9ZEANmexLUJzmKJklMC/5g459kMKN1FjCsFmwgtRLNTb26W8ABSq4t94ANZaqck8Q
p8IDpDCaquxIb1eGOxDZrIVU0OCfPKhW+dhDX6rcg2RRyQudxHTpdCmbUn2aEBn41Tcr9A70FCTk
WXZlS3nwLl1OMmwtktEKxkjH7/D5rtqqdoe7jmVW71QAOcOq5oDCG7UeLtVvlNyKeqjV1gnsoG0A
rW7ybBnoibRzOmpDTgQWMdVWbS7Nr/7sD5VuQbpYyj2KQkq9DD9NjWcM+fAeF8hiA7sTqrUrFn2b
CsBnK5cfg7DYXuXCkbFFne94pE20VF3CF3+/Eqjwxzi3wFDlVCKRTbFRdlU04hp2fYDCP+qmS3rV
EJZbIs5tHjTFAV3bheRxezxUPTuSc+PVHCTt26iza7IRaXNdi50+vQsVoisRBmjwfUH8lOs0IUOc
ztFckfY1/3LgkSb8HudhOYxrMpvsTAVsF05fvg8IZkhdxkl5gVHTtjUdQWNFIgWe327uXqEyoe+6
zewjy2gRy95jGluFI+1nBaHI0c9PnJSI6FyST5uMK/jWXlQmpTB6z3q/1hlhiHeyoZd96UqtW62O
0xWvkV7Yo4lrfcXDBgR06N57/aOXmazVzC0ZWTe9+c43RRXmsVXS620PHhkMiEr+V0wpRslVZv/j
MgjAkrGCOCkIhcgLM0lRgp2SiMkzpM/n6pJmSakblDyPQOLi2v8IVtXWpCgugY0ioC3Z1Tj+rrFe
NN+iyFU+Hno1x/P/oaNVlT+UAg10nllBl0mjeWGR2DgYCO8hpMoHt4p3C+d7fCCFfIcczFnZyHiN
W4EJptDKZcdj0wJbdsd+I7HrPRltWdhbs1M5q2Sqi5lMblKYHIF+zSeaINEaDdcMEnERWV62mJyp
DJNwE9PumcFEwOCwt3G2cZRolvmelJfLlZessRt4nLPUN4nKOqO+F+/cKEWApruEFt9Ao9j/I1zX
RavExtTiKAhZXDLtn2I/2XSVGN35Nz05FBRRlhAwurQhXEvvSHxnE6nii9baPzYJgmbFTVApkzX/
NGhmSBipslRwMW3GEc33JspgQX4UYVb0LO9kQeQGcpN6rIOP0jDdEsoa/moraSQXLl4zrtHx3ehR
u4jmAtWH0Ez1arxZj4GdoJ8tp+Y+w+DFgu904haWUhksiCnrbnKVQZh0oYRqGdIkGM1YF9tEJd0L
s/FZArSFKvfX6nP9kZ5QlEtJF9J7eqEXhDh9akEdH0qAbLbQUDvOVuD+6YsW3JeVGIcM6ya05ffG
Iu8x4qX3gpIYmMQ04BawEoVuOVk7REAg0MCNlByHp5QNTC/ha84huieEf5flzopdtQ3xeaGwQFch
WJ4+Guc5ao+PKpLrSPHOeiSpDVtgwESUBuONhUGIf52uFOtMPJJKCdXHno94u7NFKU1M/ugXrcTY
DArGMm4PR32mtksThv0tju7m3kTe7JzMDztKUWRYI0msyngyXWeqa+TkLsYfxcJAuqSEC+wlkzMw
l/ct7ZeHSdDbbJi+3edMmQhyNcB5okNSl+aXPWovll5L9goWwz6ycs7dEh5kWF9rcy7Krj2uifxm
k/BStyVbWedZfvw4q87Xkj/NkM+CxWQRYwJMKb3NrqYB9njsmkIRhi+aQweQWIRHMY3hRD/OHfD+
dUVXPqIIy/1pdUR4Qkqogh8TfgZ8zG/IY6LUjCp87PfGBg5e243G+KO7EQW1juZ5iRGWA7QPqLfh
Guqcnax8z7NicawZtJ2vqO/EUY9BQQSGzO3P27GrtfSH5eIavnrH2Ct0AnoV4vpc7G41V2AVoXhy
SOqsb3//gHuwWJKGoKsoMFDs1aw1cZ0D07OrUalzAj5prrmMyuQ1w5m1jasI9fgye6sfHB/h7EF9
ae3Vmq/H+5f6DuB4Lw4wWgfdDAqLjd+PVerx0DM7UTXuY75FYMzRGkh52PKwzf+hhhag9JL8RAZJ
gteufZ6GqYLJHRNLvIBwTksYq11kSZkNpjXtHXTb87xcbE20wC5Z6/SPN0OHvIUZj1H/UGi1nvtI
eso2gpVuCXwSHqi0WG/007yCzMy9pXbWFxTsvz3qx29r2uMJmf8m0Xz1FRV7Fg1JngOmI0nDyl2U
TlUrIZAfpx3Kr/iU7GYnDRFgiFEcdB/eaQ0Evsf4iju9ywhVK69E9opoH4qDuOFeV/Hn6TiHQWnG
xovBNfKSHZNdeJJa4UWt7vMAlTkqoAGreGRugAAdG6ZI+tHeNGxcOiBJsV6zmrcnG2B8RIaxtyEs
yzzFT/WMso4whoifjivTIIT9YaGLc2hSDzIyFjXY0cu5mWJvLhbJhzelLE2PRghP4pwGGURjR4TU
PlxzEbJvursDmkeDt0yAT1crW+eaNZlQxxU5OoorcpOqxENEgz/evechB33GOaBWcPi8ZpdOe6PZ
bj4r49PwW7W3WURYLxzwDmqcaDmPqLXcJgqhIIGpynB9OplnImbivr5MlK/OWDKZv6zbPjBJ2IUS
9McSzCIJ45bm6IArVHUwvAe0s0x5zFECI6DXahrbtVKv4Ro4JPprB+2EZrOETGJT5CFlWO+5d3x+
WZuRZo+MmUAY0H+tb0goR1zlxRFtmTxk1OSmjZOuNSH9ILBAvB6dHmvdFdic9NbBFsjGVcMc5xn9
OMUIdrsoynTXsoU1zYWsWhtHYe30sGFjT4NSxXocypN4Gy6VgKGbFyWEhlR0zxLjhxwS1oR9VH/D
9R1ruL5EQ7G2oiH3Xb2SnQoj05oEwjAX8X2kXEqfFjJ5at5DyYzWmEHGA4UhTIq5CeXmTvlrPiEp
RXzeLpzD8GbP3O35TXne0g6Pi2h4QU0XpF8G5m4xAA68NOsMVB1NVYGBejkPcxJKz/hd65qJV6EM
lJu3jdo5JRhcpBUOBrB7SE/vtePocmkav/+SfiXgS2C7MV4Wg7XgPzIqX3mTQQuTdIMq6VXmBBld
z+L9ui6RuMCulSNSc0VBjEKH5QmPPo/Koy2rLmjGuQGnLhDIKIbHMSIYavhM5bf8BKKURsql+dtS
i5XJnFkBfqroMG+U5wOW6t1i4HIy/X2KTD8mo/1FJUL7N0AXcSpY7tHUNF3Vgxmj1Dyn9xQzzUNu
qMoDSwD9ZPuIfCpcEGUKZ9hx8OEOyAyOYjI4SX23DyOlZ5N3DkHp5Ec81Bj1e3C/NhqbMaEm2Ttb
Cjdifos+crTIEINgWw4C1DUKyk5YjwAl0cJ5/3Yv2bFX2ophEHDiu6/H0l0ysDIEJd1DgGQuDd2D
0XyPX6kIDasrIp/EkIGThSZ3sUtxz9qntZkLl9iyVlCWenjfrG0xFInqOTTMgx1MFj8En8r8uumc
0Sqv4wNh42TijEJpiwj41IZ4TTv0H+VZ4m4ufGHKH04zTY/AFirM1wYbnKnODjn8HQ+jh3XS00L5
sp4iaJleEyIgTYOZZ0O72DHx4fMOdRWVvxcsOaQM3jwO6iXr1BmHqGUn1jYXuq1SHQR3+aKxndfA
W10eEKiESIX50R2Tr3+Mh+2FJdVqEuyclzNZHqLRogWdKADkJuZ8aRFbqh/Q/fgGpbpQOoU3/j/D
mT6cbxWI2LRhW5Vt5Ynjahb+6svfQwH8+A8cmuQiC0E58CECp+aKXlHG9ByLy7OFPR4FaoeNcVT7
xdbNMF5sm8l4cyK0x/JLONBSmB9bZEnNSeuO85eICS0YpmxE+a3l7vsF94T3KbW0vpsBLi1fqTer
7w6a3WXBLiI5sngm1w22Jjqi0ZJS+Bz/T9hOsikOz9laPnmmHApXUCzZohLnnbmaQ1fCK50mYv48
vfTzOQ+qcOkAgo5xZ1W8V9Xx+eBcxzOqK+H6B86uGsQXQbWlttmz+PMsjqHLRSjCDb3AdkouHjcA
GbZ2jtDNNDpEeQAvG1W3WggIz6Hwnal8/HotXG4uLV+p82XXeMuaifRA5KpQlqwaRcq7b/Iofl8B
kPy5tFasnP3Vp5bJS1uUxRfrVzyUW9ZNjKd0BReGYhn5tW5wtZiDht4c29gpWbTMIZiKnFRuddqr
3ljiDzfHzmIhLxmJAvJJt9VI4Yek2YKGbXn3kfmynkpbskZaahu+6PYntNxgNnfHvOvIhSGXtaSz
NyarEndoglVrHqUYti978VdHPhoAAnAb0VjyKwfzYvoOMXX9YfyiC626wOQwopFJOZvwCvAf1vBx
aqXp/xZhgAs6ch9/lV9SLoBUg67OKOPVl6ooaIUe5rqaELjapFkTUKYWC/GVF+nS+gDnvONqjcPo
J0RpA3qcBkfzeUvQej5w+Ihe669Ju64oJLAedmAQkPcMa95EHIV1t2zGAsBTm/NJpw+KcwT/cfox
PccfYcjn0+Z3lJlMacmJY+cuwwK6OSisf/L9vkqz9i8Gak1vOwFfDdxAf8MA3k5wpwPqcJFf7cVP
V4SrSK65TqGYWW/557LRHY2LFroIEssD2bsz7dpvN/BF8DhNSELfTN1HaVuO0ueKGcczswpsvvsS
e3nONde7fW9MGY4rOwhNn6uHC4NO3ex81loZ+PbAg38yYJ1UgJ/+9DcmrewRRNnPeK2NYnzT77B5
3/74fAkgfsX9Yx7BU4IogTKjADKu2UzBxYKdZULb2elOjDPKOrXtZ9jU0tL+OK33TYx+Maz9ZfjK
aDR1CNNDRVp5b6qV6bh+JwJnePeIc7jbQfbVTHuVfp6lcBXkMkzZVzodPU/vMxdT+pmFBrMasNJM
XgxIs+lE5WXodI/5Dinf8cs9hH/TKk1EA4baOpEwBayB/LmDAqr5bMh6c87KkvTrwXVS2u5liB66
v6fogu6OmOAxwrG2/1ahS4Ft+zMKPIGQxdHsImW2d/FBLJPcMn0ASxp7aLvT2IgaZs0fAj7jYN9S
8mcKW2VX2aRxPmxqDnrXAomU1RQlHC79xU6QzKnQs/Op76uIit6kCjwSsWH18B/KTD8qP1NRAeo/
ax5z9WNpt1UNYW1VXwiT7W1fjNO4mvkXNUWrl5NZhRUckzu4BB5YJIMfbUSIm0i2QZNWmvwtcnJu
ZZVbBXy3qrLHX97xwpCX9Q1is3OChiMlqtBrdUFdtSEO6Pa2Gb467N9yMi7nWedVymRY3X/XgZC4
Tj3BK2Jq0ATvYilsz/dJb98X7wspshQPMaGwBkNFSYY88fU77sK1Jlyq7Mzf3Qtci+mJoX5WF2XW
8k7W0uMjHUUwT8yVJgDLeJq+BRSHpI7KUbHkV+tyyazCW4VVc3MIr/mLcCGEQzxxeWfuwF9wOe3L
DKQjbGD6okhoRY9cYJNl1NxZL6h9+wQfsFVDIKP0lALqCDFGeLesKZLI+xjhpBm3moZ+DLZN3vcm
hy+0u9HolaHnx4s24pyM+W8RJ1qLBO36AJkQkUDocEzqxN+MWFwieJ0ZxBYtF7Jhd4X522ty3YvF
MY04+O+fOueo6EcXzsfhRzjmzHvoS41ZSDrv8cLQUpH2tLEDjuF6REuYqWUN2BKQCxBYMSlwXEgI
59iFlTo64rt1MZPze5dLkW4GOJaUXapyIhENtWLg0vBNvzZUvEFtYDPdyCzVM5xZosdpu40F7R+m
Vi86Wky2C4i7vWc6XO8vZH8YTw8loY2XsGhqO4cxv8qg+Y07Dp+wg9t3G9NDpm09xkYfkS204yKB
LPHc07sT0MD3oacv+CZGRBcl1p4OPRnG5cRwk7ZfZ2aElN3HOq1On37ThEDMp7y8sfxajGQuXkne
sE8ypOs4YHxkrU7vFrRNWTID66jVzit9cywzo55SEtA8HqkBNVBuSfu2Tra/bJcshshxqackzaE3
ry2uicQnEPgaOFheqzscrFrYyOPEX+KZzg6DlnyztTmxVLlfnEYCp+CmCh8sHt9KFLoZvvfw/lQM
pFlaCJimaEEgiU2aCjUIjG2RfuOWeK7TqyPdNRIsI9h7fO0X8gQfXxj6KnsNKh06wQDZdldtrP0R
G05KKegkYJCNIPY0UP6MBKJC+N1HtOKcPkULJlN56nXyF5vzvY9WNBIXc1xTFDJRZqh39jUJVHDs
uhmCHXtcBGI8Vdl3ckxld/6JhEsLewhIMenI24BNE5bAeogk1o6rEvl8ERoLP7rOCOtS7P8B5ncl
wjrxHIhHfiHEQQUkUM1GgKWSFAPS5glCeGIQv5xj7bb2FLIm+n42CPAEpz2qa/yECbccY2hSiYgA
3TnNFM3HSUFddZfNd7GzaL5aiiwg2uj1irOyYRzncn1gIMstBYAVZ9GrP802RmiD0ckKvdmfyulO
6wTFhfQ8p1G6MAaPoO5KQQyZYk0lmM7saC1Q60zU8cr4D6dMq38YxRkwOEBB5kOAK2nXPxiIGJgv
pF6hgjA05MPvXlhypXqK1KQ6AF0d3azlSjy9ps4YCkgz/205LS0jU47xk1oFxnxitC1gQGMpQAYq
6jMXsOiJf+N7V9c0dNwURIEh6sPFWEcW1Z4KepWJIogKOwJnzDtkyslktMBZH60uhJ0PyibL3dvW
faXQ4ZtpVIqEtunGJ87rwQseAEHn7T3otAfKohbTJ1s70WCHIag+jn+gmEbU0GK/Ke1bPxwB98HT
MbYVLsEEbEnLZMW7qlD5ew/BLOS4uCgMfZ4y9r2NwDDRx22mUhTzUdoNy9cx3RLDj9vlq10/a8EV
/OkHDn9cb9qMlgKpT19Es6kzEJAnAVgqu16Iv9391WAfYrBi4laMM0YWmGyy0gUkuk/lpujWoZnM
M+pztxdv8Ubb9nal9vvP/ZrOvw5fWp1KWdf4cHiDwxe84elolFXwcZJ5Y0bNvphrnYqQQpUyPo4Z
2OKvXYAn3iFD+9EgetSdLAUrHTUBTivR5uis56EOcEEX1l/KwnnRYvud09k6V77VWLQTUnsDJZAh
cq7QUCg1zMVMLKSC+XZHQlNjVehowtzoL+1Yt72mToCT68g2pDmVDKAYR3pYnPU6PrO1ZwXSKlPn
I8W77Jv8Nu/1G5pFfTQnWDpQqR8vvYyVFXbZ7tUY6KUyYGc9Zp0PvORTYRVbXttUSnzTM3Mepi4/
s1J5HBZgmxYR79gZMfUO7fQeMgd+8VNzETAbawqRVcngLS/fiekspHQfsG5vSm7UGJ4pELcaFPjT
GnFLVgAjRvIAGx2b5R2yAExxbMPCFSq4/w76xsvKjcpOpATb1pxbUBMEiu/7UiP+ZHqrEyCehtSX
N/dbiV/nkBTgIYZ2Or4q2p4tdKbX2nUh7qj2Q5cmQwAC82ZXGR152MO7kYosFy+8rEgNzoE1G2ha
wFOajYDUZaovFIYII3ahGgA3EZRqdxOFaBoLtE2bJ0YJtFYlg8Bft1F5WRtHEGAS/H8ZM2KzZYZ6
T6h7hGfbM91OuTCWTRlB5jVRu/E39g2G4LoCRlNpSI+y90qE++UWJrftLAi62yQQizbLR8tL77Vb
MEipJ3yORi/MTvb4XtMwrA5sAzloy9KO3hQB+4ir01FQXxv1Al/z2nMXGiHGC2pG8DzuU7mosmAy
CivPSh1ZisFfpvNEgTMw1im7Liiu4n/Bm/OKcMc8WebjzQsp7btZYiB188Thnj8VHPkI/RWntAgw
IBtkLS2dCUDZnNsU7HS8NwxpaHb4srbWoRo1efgW3hBBBh8z+tsaD7TR0/T56Wo+AvzSx1+yvXLD
jGKKa7beCNOYaOE4g+qitnSgQP/lNPsCMLsbiNmUHDWg13aJ3zTE1A/qaaNZ3pbIgh5tt5ZrWvWO
/vp1PNyZyVl9gYlbqWZY1aP73vpie56T++Wv3hVbQdYYu0SJCv8bE3Ajcuw6d3E4nvZX4gBEWgki
mUJ0T07liM+t9v6ry8RlYLKIUGjJFsqjY2S7SHe7fbUxBZU+swDrEOgkmoeaWBTJ75+/nMoiUJMd
eAqxNA4aZMvbLx5Gv7sv3mTWVB040rAar+ctAynr1h6Fxv85R3HhCTK494crytoUOJ9ehHWkg16z
6+08gOncyAkj38tndW5ufSoLKBgNRLJRgUxMw1schvhtbCCc/0eLUDYCJ27J5HT6qHW75UkWq69l
/CgqhVqErkq9drFhKKaENHk9swSrSKBbJdHZ8ZJXIyG4k06ObH9NXn6FI18lSO2Pqv3WDET730Dp
KWLaxjHD9xNe3CMAnIGzD/l2HnmsTRnBGpV1X08ZMejIz+oT36+15ap1uVJmQlmvGPmApxXBeVhz
KK3gS5f69J90vpDXqVVW4JrQvyGjh/H9Ur7dvuuqcsezuVcNl14qJH1AGn6p04DvCxMkRG4YMnry
hIbNmdQEpYwLMxHWY3dE64n+kb33+0Jk4jU+cirf1N5gwVsoqPX4RAX/Qr/MglSYCg9sEdJCwXWp
GbtNJFJI2dugVPJJzv0MUD35lNeekwJM6UnzR5eFe8NlksK4SDHrmTb5UlS50w1r5vFW55HvBo/I
cDNCw2/ShLpM/Ke73iwtSZdhELrSon83GdElR0HufPSnOJUX5XzrwHq6c3wd4E+kR0B0a8/K30fy
CCjIGz8cXt7bODr1qRsiZhBMCvc3kYyFdLFXRoaO1BwHTpwt2ZfSuVpc0lG3PsY5lIPFNFaz26uk
rx/bQMM5wYTNw5gv2hWrfLySuJho9Tbn9ShJQ4xgET2dEmaf9NVqAG1SWVp3pbdyvt2Hn6aBcHGR
8IFvIoYUn5TMGXoCWqyhtz2J1/M8ou1+sO8aYXL8vMq7Yx2ORJdQXY00sod3TpXZmyfbR4he/nkt
e0hl2+MwxHUVDcQxUtngVlpEFUGwl3z83AWbB76fi/GrD+kuh+UAlq9J9HpH6VUd1HBO2RhGzxxj
fZBudSl/Y0SQvcvVlpk/NFjkKKgVhIzwS7HNgkUOu52Sej6HgwfTRDTJ89bGJFYZLxLbaJxzYmbb
7qpcuQZaRfLHgXU1y8Lt5QwNbKWLHeb2npr9V42/N6YNIfBIxR+SWt9JfxlZ92PVx8dCX0JyP3gp
kC0f6AdARHl5nf/c8JgFkSkxDmliqqQ+YUCPTR546yxriuVzFilUO/AXOHmGpcjz67Tv0jVosyui
N45I+jKd5j3l3FHWqc2vEgQwn5mTIIEh024FrAa++qo+jaS6RJb0lJMpyH1qzb0lAl0SPNdSKFXE
BC4cayAmZRPukRqsHOj89CBdzdtknVP0hj9FCqCFDtvINYf9u6lv3Dxum5CMnim6s7fxVKc2XhOJ
CrEFstQ+odxW00aYR1o+tSw1d2SVVILxQkpKRuGp/eQHm87/OQAUXe0GY3eppNOUnMzmBO139qU0
DW/SBQNTtH59dk149+iFQ6sJP5Av/iJLMLYySn0CvANIoMCM5JByfuiaGOroTK9LXYj0Wm9T/DkM
nnoopVPgKtouZMOOgZYswsyNtDTQVG923D/E2DD5siwRs0NQNEk6TmuVuVfmrXEARAF9h6stdchm
vFDH0dKgExuZCPLyXaA2o21gpNEpNDLKe6pm2edmWOumf5/m8/PXMAtmNea3yf7jbSMtrm3b6dDw
ZTddWvBIKzLtFJQ9ZFqvKSwf0XitMpnGwimW9BY+00hnl//C3IXRGLKMlWIGyKDieYU1EJi2jJ/b
J6QDNh/hkd8xZGMJRqdF2/+2Bz/HCU8Gs7F0ZPc+8CplBTc/oYOGCNEMGuV/h5rwnhfmNCczfHgA
Ni1axe3WJeA1mzEFlE04hM0FBL49c0MMFC1J8F6nmPkPYyWiauJZKaNmLWzFnYJQI9Mn++xOnzy7
d4MfOOw4wkVTZNGJ/hgl3wX/9V4rcDa6DZCoQDG0q0fwcUQsMLqhePTXWtseMKzMrLBHFju7GKv3
FDb9/G2Sbz/52VDKNHSwd1Hn9ZmIRAmTH6Qv9JDKc8isHFC7Qxto5A3OwX8oqbi/cjSSCtbQAWy8
mFqDDzu2cts9pSC4fX5zHIncemhe8pwf+AZo/upSp5ms10qytY4qwJRxliiu2Rs0M1/wrrcQNvHB
F8A3PGg0grIL/EynTVS6L7WAjlm1rBtN8sSNDB1nqJE5boQeUXO4Ou/G1t5I/JP3uMb4z4IltYRH
R5Cw4mK0uuKcCXO6zZ/3hw5hgT6/BZfEKzybqiXBoL9xOqRa1oK8haNDGI729LaUmVHJdDR1z2k0
QuSGgkNW3aCSECRRp8UoD2Ni1Oq7RMSWO3d0eY79WwPJzFW4URVfZ3PQQJafaUsTNQ96JYTKEuyG
OokxKWt2IFEKL/UgOOdFkro5AbVm1eNe4IBVfp68uD8pnyoKImUg5kR4X0tTJqbRD0GRggIImDbE
pBjx1bY9anLuiVy187ZV6AjsM+rW1vamjiCgS3cLQHdBr4RwX58JiK5/C85mcPwrJ9JWvLRA7o6i
mrh5vuSFdiEl0wuY+Ac4GLUzbVm0t3oAw7cQvpDPWfsJypCEBvVeMDclxt+ZdRi6i1cTg7TxcIO/
Ou5S/vgO8B1dSPdgtaflDWwUjE54ruzjldl55tLIVU7pT7MjNyF8mPq+ymJWf5PRTME+gh2j3CG8
qInOTxPA3wvXhGbqlyV6hdV8oi1ODN7Zz5Ah9i71oSh7MIqx2j3BisaO7iKlKxvFR5lOsf+D/n/P
z3gLT9fYHT6qSYvq+uxI+Caaty20xWgjVdmSZnEzc2j3tofcs+jUziIde/pAb0ZomhQ2OQYznhIc
eM5n406oZyuPt8tyqSUi5gSkokpNHM6kkKaj7gek6P20AVEPt2IC6fvuszKTiJmBgUKO15U9/aXM
hfg3XgNmjoP/RV4BZcK3Wtiw8MUGd0Xocci3Wz7FHk0Zj88tFEEm6axHFOdOHAwIP3glTL7uOigK
YRHdFUY9dsIV0ABYw37pP43VKLXZmJe1Du1tbNLM+jnlnRAW2duMj5bQPzxSq2CfMa6pGSQ96rDO
0IQBuvAldTFrKsXN/pPS1HPclxEfeJmnng9BwWLGIJamOcoVL3cseM+XCbbYo0pp+uk4axrtwnbE
ky5dTo1Wwuf8f67gQNBEEmrsb0bhwFMEoL8wZLYxgwC1viY7YR5QMxpr1BrEgS1Om480qIYOnu+1
+GyP1DQcS5B6ucs/OlBpUuqcuo6rIk/nkMzVRS0xwWdCt5VWoa9dAYMYalacRHGiLkU28/OqVoUQ
wF3SW5TtiB1RqOkqkB8isqN/43w0sdmOSFwl5iM30UYKy+leah9BmqFE3uzUbR/4FPrmIJo9/nLp
izF5L6uMseGQH+S9fGClJCJNsHMU3MT68G2bB5UWlc64eDjCu1wGeTKJ3unFBtFGsKOwh5Atr/xQ
SNdgoM2pbEf0xNKAQmFYxaBc6d6HflifRJWuqj5xev/Q/m+5nF5DWjBqFm8Rndc3hAtxxBXlE8E6
mxgXq4KNT814jFAhK9JTTcZfuP2qfvj7VjLTEs76NjqwrnB4lsjeGi/yEeV2bVDn5kf2vy+t1Z5z
EI75NxUJjBBc1K7rrTzS+/KJld0phwUqtH9G2vDhu6jBRUqTd+1eX4hWBD390PxQNe43mZov8UiJ
z7/O30hC9Kti4Pu37WfEmr5zPNS2DG02UjrMnU0yFGps1DezlsAtuugDe2Cos4nxIAL3DawxlwSv
XtXCTclCam7CXEOgIkOIgT9PMp4YUZf6vNRh+6bwZeu7+kQu30ThErcJ7wyiGUfvi9kjTLRWj8Md
9/yW4/s0ekKn+aIdMoVj8S1iFBj9gelLGXl+ojVkp4w6pTaWVDsYfr04rQqjdspCKNnyrTgE6sxA
TZSzxlVrgqFsgH41bY9REr1bBqWnCZ0ek7whG0SJB8JZCpZof2nXXpxkbTP+rxDWf5HsHa1qHokF
sn2/Ek1H3dkbpnjF+fYghJ6OmsCwdUpTBiZLMW2ZyayL9FCI15H3vCStKI8StKhp9/jqf1VotDik
mPChAnk0uOeeSxLqDXJ0supzO0DdLhg2NNXNZoZWhnp63sVANAPam8Cx+5d54nRIxPJKK3qnkEc5
owhQi/8wljRVrH2zAv87ksTwBRL3XJOvXDNMp1oP4+8379sSQ+YFgA03T5HeNrCvUibDfsXfTTmx
hnBAzyUU/E1jl1HmadDjn2Hl0sgnJ86sHEtnbbUdiMKgPUrB//hUQiMvioYJiwqjbz5kU/zNLKFB
JgKl+DZ2+JbKxcrvl4nc/UWe6bCLE7VhI2zDFmQFwFtV8LBqSD5WENqgFc0lKf9xJxVQXztPfZ6Q
zOaVhg9F45PjXu+yQfeOWMXdZ76d7SuvYBXH9sPbJWZTEJ5XBy4080gJsS5pqa55IiMR8PSjgfJS
eG19W4C7tG0XbAFJ18OKVbuJVxYMkau671MzbA/zTFpUSIDzlZTlPq+ZVg1nZcC4k5x72s09x+xb
gueAAixyKbFeihgwRHFfSTR6NluLcGeyo/A/hPMYTFpjUprEfkzfFEKiP0wBh6TCPPFau5hiSivE
JqAZIDsDjOu82oEwMewAcO74DBn9gxsSq/xI7RlQDbHOvFEmjBiBzls7u/EYZP8zWp9uCOF4vBEG
5e2vprQKNB+1ejjnZsSYQJwfkM6NUapcVWXbIYwpBJmFdc9oq6cenCZITizYz7k4ZQnJTBXWJEcy
1yo9pVyjBdGROsTo4Nkg3Bm/6xNKYQmBoqzx/u5Ixj2vjPCIDDvw34O7NfX3N29DqOIrqHpwOIQG
YBG+lkSFYeaOOvCDeIdffZv895Kajdopz5obB0Bt7RRv1+AEbMwBiCRYrEUhX87NxLPl/Re7xyH2
Xd0/60vDh7IN3M99HBshJUK2jU1QrK/uBzVRaodSQseUJaMgYzmuqAfEaTKLkv0UOv2l17MWPAbi
cs4fOlIdtXzOgMmLEJmu9DSpXcHCO1HwTn9ENNKYPGPodc8SaDGTHOlX1jmIjMk0MsMKNwE+is9n
47gmhlnlxENG/Al1Q8w62i3mTUwgBRqhwRGJH13GJeyM6C7//ZrEQ0vef39TaHcfEa+KxSrePXlN
dBzxcNuwpXK2tTzYqAAmg80/A/uLBcaFSzD+t7/VZRFcpyRYqSqusR8J5IpxnvRWOl+czrQCcMyL
ATHmhtaiZw8EbIcik87mrMAVvjaQaRf/Ado1HWodoIf5KOzJuveY8k1pY49sLVwKOktAWBmu7gm8
qU+So6a7H7jYB4Py8LxrQfCPjqSD3hwY85CZwCV8fG9Mum5CyfLRPAzv+QrwW21jHT7Akv5dDvgb
t8ksgX1xpwYrdpWlQGGXhGV7fKm0dDRRnaT5/OQso3BmJ1Q4qUazM97sjXD4yaFn96f8PcUHWE2k
L5lye7U9Hxti4jLZBsOnbcNcRf7cdohIxHwlZYfXAyFo6Mc+5bCIvPUrc2iJNcTPRBfl1mpPzvVO
H/A3KplAv4pLWvhXK1WvQYJJJuHOj48ho52QYOnWiJq8K4KratjKWvWaSXqpF3j62Gvl+LPLgJvT
x++By0uKdEYrZ3AwWXwLJaKwYuPHIB40W2dQyPSIIFWnFPWKRAbgyriyXt9xvtpL9pjfsyg1ullp
4nok0LM3QNwVqRJVfWPhcxLZx8LE7dm+1iBX97CKbj8iVsQ2SlwUAqX3bjiNvUVPWhDqP42SHvjN
lW9MJIFY8AXk97aZA1os6fz5cD95DMK+1kr8Ax0hURafJWpclURe4WohyOzFFuy0KGpuaJoeBazc
RP5ZfwwIRHLLHxqkwJXUW0pIYjijFXCTgsAKuOq0O+gCXT1k+qXJLQdGDI+wUnyDOolHHIKB+CYn
8A0NwW+skmCZkzdbpULfZ8dDSBjHV9MZmqp4Y0bW/limm3rCOM7wDzrOZwmLvX71Wrf2a8oYcLth
+OWocJUGjakrWI+Cd3iaC3joauoPSHVNUX9CHl3MMZsFC0t1oWJVC0W/ES9i+hNQikv9YC3FJJjO
vIOJa7Ic5M3ZMid8UXqXIqpqINN1k1Y+u19vXTZIzU6dSzUPi3rFtLPU0p6BC51Uny8RJxkMNSA8
Qc8+nTk79hek5z8wZYM5AQjnK7OhMBV6WIQq5jmQLnqyBOADS56oZs4wMjwf5xmb7zCfFWHpizWW
L+2Vv5oD8CziQTGx5KTLKaTY2kXmIF7dBB5r8EyJp/TeYeW1pxbnO1C92bnjS6kSrGt6pg5ng5NB
/48olaYh51g37dQVWb+VLS7DB38RTWHhGUu4fX54sdlTmWj+x3WVyntZOb1ezEewQez/scv30BUN
cKWgl69LOjtkU3bYuyiaorEcvKSN1WUQigCj6ss9mI1Y8+Oe7hfGsFLWROfVRjER4lHtBUo+IPbs
ni9NPap+QmE67kX6PML/ypjtU0fsuuu5ynXHMJeutFAGu3/QpPNWxR/+tBDAx8RkitQKNlmOZcIn
+6iYSGjqG4aHjPJNKV9sXAvyk6usVINhvOV7jbLLQPcxGGPYrjyVXbrCBhEvwlpvv6oxcaGDTiPJ
Gwj3Zkz2vj8Z81Ra7thLiXdmuW5KvuF8yRj41qBE1LtgFAVYEUlKt947+yD5mgfXji2nb9pP4A8o
nJkZuzaDCYKyHTcq2hHObaz7VClLBNzcNKZ5nIQgk7XwZ8/hREYI/0Bqfek5SkAEBpzmTUrTgMmJ
kFiZ/RKft3VR2vmdspYsZdfMvZSGDUtJI8WQEW7tz3KtVtyWqz5/Zc22AIKc8TL2fZVCvtDvXiL0
KjOp8189tRsqChAepESjJTOO3mjrHTdW3qrhV+Nj5T1aWwws+jKQZpOzrO+NLmgnaSxfJC04cvO5
zfO4gssKnVj0zn9CCmdUneEwrSCpMTGHnHTMlMcN+wGViG/9ZbscjcgeIfAokD8kR9TfvAHGjayE
qoy6Nj7MwV8yOimBtewpXDWlZnyg0Et+0nZCym2B7L+37vYNfGEmNyElS5X8rv5O82FMegnvbkQm
APTICCfVPtOmdFvOzvwVe+6IZ9X9IHG5P2pgWd9IWOeLPyqwNLmzZNoRuB6gKqNbwxxVBTmeIOkT
OIzaraEhxD3mRhm4RRwZe2XamqiIBU3Y0mj2Bh1sAAygo3ffkjNc6ak3e9Zi7r+OW5TUOStCfab7
Bs1pl+ZLa+/DdzaEyRDN3iX1ya+lkUMM8CIS+pQo2kIrFquYqEjhyGq0t1bvPYN9pj+mMCuc9UaA
Dmf3B8g7EzwobH5KsFwd7yjEWpujUdutSZY/rkKoK6chByBrr6tjARuC0PnppRPNZgyiXwG6TVq9
vZxVR0PuT1bhxNG6JiC0oMUqwYIcPB/idLOPTCCHudgPExfGOilhsxrj6QY4bTc1J3zk1rn1BTdv
GZGrjxNs81E8VkECZUhSYspdcG2X+zKjfwMx9yHXWQETFIbMj4UOg1TkO2tobYWyd2sb+67OrDBX
yy2iEOLKC24OuPci5Kmeic1DKT2/b7rqii48BYMMx0Jp946Twgc+yyHRHV7JTJg1WHubOWYVOEOz
G/dKfXmUAB59RJu//+r34huKCuXy2XnjASEmsI4eD7zE27dqo7F8NqqsX9B5u9cJHtpAFFuDw/eI
kgmI4ROkKdRZGBMj7lc1GuBo/AhaHEjfvwPYo91j+1JEw/9wedmEiWrXEzOCOq8Ogo+bx4YBnCH8
rStu8FlGuFED8lBL3qMUy3sGfEq+AWKqst422rQBThWw1MUtkzr/9CAvI3dB0p4o+Si1QjkW2ctZ
kT2yN/3O+npdP0lB/xHVbwuJCcpDDpc1gSfPlVfIZEyRjg3WUaUAh9l4FJoCeu13LK488I2KPx7I
NABdAQJPzmbzoN2zx/aH1pDBaIUgjVjNaNlvZ/Uvk3SgKSyTaoLvTCE/YTfLrdgkhsYiaCPQHrQK
iPml3h7IhiFENaRanja8JKjJE4M9e/NZh4bhGCYGcTIEucXdrk2xyEWC9LCCVu8/q1BvtNDUKu4f
adih6GrtxJIW13i5P+GAjWgAWLXarh+q076ITO+9tk4t7bk7oH4XsMyl5tRGcNSpqCspSERttuM8
7m29OcFwbSKpc5v21dTC06rx0YEQlfF4C8mrBWvDv0iVPKepVq5EmUc+l4v0YEbGpr3iz2Tmw3hW
euu7gpCZlocMh3vBuav4teckYU3er5QpRh21xdEWy8Ilf1NXIPZ0TAxw9FN3JA6kQ+Q71CSZWG7O
URfHcOcqKLQVdZyhrruxxPE6u5ma+w131f4CUoxAV2XBkK2mCLEcG/aRjHt3QUdCrHRqM02aZukF
8LtPBN5us7OKrEPOnqY48KyT3ynB4C8VIqIc6Ezq5cgDeBBfEk6O64wapJw1oV+8bClp13yf2AWc
PEZETyD97ELlxy8xKtmFtX5VqDXCwtjNE5XzARbWsGw452ugMDVGjXqDi8Hr/GcSLZ51CEA+vP4m
QbhAcM3ggWUAnIG78ob66iqYCeFbp580wBneMU72PNAk5FDkufBStnhgvJvmbEu8EpnNY4IBlJB2
zNp16ySeteg6O0Iyt4iVBTqbWO0kHZw/VjslhdwgZXM2ieHcVqbspkK87KKSMVmSjv2V02XFD2BZ
x01X1mMQiRBfzcRDrgfoDM+1b82w20+YErQXKPbsU8fujZ6iCiCrWzcNw5a3f6Eti6DhXYI81nVZ
TsfTpnfyNnDd0ku0UuD2g5u3uHvuZcV6eXFjBN/naK5vdiF4daFwOJ0INFcdr3LKCuVd+B2v/zx1
5gK4gAZhBiA5kZqrjOZor9Vn/3i0Wf/rtI3OAmQpXW/oJzpGjuaVbsN/AscP6uZ6NxSlnS+Gltbq
fRj47UB4Yx91dMY6QJz43JpZAkJLb/2AQnt8uJ6ikQ7tfSM7IHLVgVfNFLVuEIJTL+ZRIrdQzZ7T
47EE+EtPDJ1NqK3V6H8g1AgWaQQGKl042NPp7IlBQ+XmmSzzlS3QMljcm2DtKzkF8D7JSbWUFMre
0HYUOraVlKY3zgO0DQwqfMR4r5pYvv9tGiTPR66rf1L+/Uc0RGI7cQd/5BcrhuoQGT/6xhQ4RMDi
YYAjXhS4deD7WzrmO8cDyzEhszUW+k/NIlxQDFDbfiSkI41DWN56CZMCaFZjpg3V391BRdAL9fZR
YP18og97uoYAG322sMK4qjJDu83Ab4pnrwkADhm3lKDpCag0BnRKZAL7KtHPTvJjubOWVV7v8wxv
TYt1eaDtuoxE4oWoYHxwYXuPft6kjdKEjg47tu0Mk/IXa5TOrbUDb7ehMGW2hIELPOMjA+gseXOo
R3hSbTBcLgsbSuErFy8cL47yATw1QfOUx9NdfHdgPeGUTRcANpLiXTUX0pGMl9TH6iyKwUzqzrka
LiqTkv7ooPUGLe3yr4cXSA7hkP1Rl3ijeG8xG+kT4hJx+AzzuTjIuIUVHWgXOpiBamD/Sq1pR/E/
AxQ56Po7ue5HrAxyet77ltIpgmkWMXQj+g+rBDJcB6ExwPtUoL4C7UxWRJ3tmLCyhOyue+pmvFvv
l1+phhK7J8JX/zWBr2HKK18RlYISqPFVu4/o5XbgdH+EVeukAy0JIxRBaZdBwkSZ2KMaUDoBbHsN
3H1wf6PkkKHslt0TiGvXJR+NujcbU9iJU0sH09Vgp7nE3XMvWQvGuJgQNX+Ib54aHaYy2KmNTpfS
gti4hpEmziI4qiP4nKrJCFy9gz09/8W6RM1gAtZeBAe62b95cTcPgNPPzCVjexzrJPcqHmtUZ9vq
f1KZOimpPSZSBkA5j+AvR1GDuOEm3PdI3qyw+EwyguRvOiFvFOxO6M/UE9dLqtJPxKGtYWjYpFVd
8Jr+jrnJNYHXFSLLNHLdmrYF+SiX4AaKw4NZMIxzHlbBoShluxUZB8ubtGtdoVeL/dgLOllz/gH6
VggdPPfE0KQnMp5XiX1IPeY9tD4qSnlzr/bywrPplxl+sH4VDj2nUdk8HeYHTq9k+rgf2rM4kxyK
JKoFmgXFEMP98SKe2IlZsVw7PvMshEYgfwWdj1ae2HZjIk9iMzBrdvhm/sq1UvGwwJ+yDn3pcX00
vj22pAN7tzg74iGYCUQl5QiHd2+si9YriXqStG1dXvYLase3/laSBgeBjeMfTidLtiBLer+QrGV7
7tZarWm894ckF4B7C1EfagbOO2RWXlVmHU6ELGDMITJPnCgc5o5sCaBdP1arPjZbWzfWTAKV8Op5
ED3QIPXz9cOq90zCNaHoYU9hv6yvY4OKHUkm+fbjKchXjfovend3xdTLKX1pw5D6whtM/re9xceJ
NPxhN3s07hJXq8GtHYFhFs2I8gGVEpLg8SCvSYht89dvS+aZEC40eJD0SqE9nxjFmV9kqPfgNsOh
tDlS5ay2dDatGDj3RPoz5sY7DPO1qJ+/UfRzg8ghXFK/Z+xV6B+kYiIGYJB+Dyxc1Kb7py2gIQZ+
8tMGbXc/uLBHoFsVneyXqO/JAJpaLznHXOxZ2KMu16ktzWigbkm/MlNjRo15e7FEPm3uVW3mrW+y
Qk5pD+/c5o2nvnNt/tdQCMn9zPD/OFmydLzKWdMoxEwzpES18K8V2Tpz0NJoGQQU0vpKRyOnWwDU
vxyaomXW2qLgfnuS8VjDVrM/KOSQgq3dkAeMYmzKBusVWdB/VTzGJNkJ4+Szc2WRfCApavZGolqt
iyoamvOlbLEYJyEaTzeHWGTOacl8XPlU0ea6Lniw72eX1H+N5SS8dRlvCutbSJBQslY3JhsZ1m32
uSjIE4bk8oaaXSt1Jw/p5x1xfXpe5dPUwygFZacaJJ4pQ29B4Xe4+7WTxpQIoeBedGNTFnfzbbp8
kejJ0EVYzwFuG2P4eYmiKmC5MSBDkJlGZdk53rUUBTTeK1rVMqwqjQfloogT6cbd6Stx+fO0g239
4BiU83dKkgtlhH2vz/zOrBqrSDUJaphp0AM8akLBZRKv8GqIavoJ80aUbEmE7jxbxWSK943gcqIx
e2w134zyysBnzMy5JnGkmJan/xjNnkDiIFyoBMjIchfTqUmCFzH2Aqn3PAu2B/9rNiMHnFX1ZK8W
N0YMQ3UM5cSC/Oge+AAPdrvxwqn5nigdNHHb3IXThI6y7gm7kVQHRmFIjRtEPFQt3LCSd4KWvsBc
9FVSL/XhQ/z8x314MzFouJ8ilJbhOphcBqr/JyHz8W+I7k368g/hwshJUqy4+CdquU8jiMcDYx06
9BgmepKeAylLJeCTluoDHLNZCEM2fZdqpP76detVpXWEZwDE/yd5MDzzQtyVpjI8bA3g+6O/hgj6
l3q0dh/swngs6SRIhrteENrK6W0kixTj3W00oE8YntzCIIj0rfMAWvaixW+/Ezy3cZPDP3sqVIoM
i6OkxdNc4BbJYpbtNN4bzkWpARpIKPSTMhRtS+haJ0ovyCmQjcJe4MMW1yDzUc07gnt8mteofygu
xTmsbw1VHOA+VsC5w0JXt6pyn72RVP1OlLn0VpKB3vkSMdg6SzyUb9W6YtcT7p5MF6+T3M0KCAp4
YrVdZHZ7Tuenv3y6Q1bgbHo7iRo1wpajZakUZntykwqcvajZ4bz/DZo+h8aAUSGqqjBQbkY+7qJE
f+BEYVjcP9pqZcXSUvyjhD8vsObx3glYC0WTwj45OQH0Sb5Rt+Ub1MVbNTGVYYUqi708iPtF1Ie/
ZCEj68GSFB4x9COv/r5LaYjx5QED78JVUUKm5RQzmB3HWmLwpIAO5uZj8I2sC8Io7XCYCBiFK0P+
kLkauNn5GSWWc4y89jep0eN9vAAp6Du0BdLsqX3URJdYqdUd+DE90+I8g8jIeUH4PCW5X/lG/FnB
WfC3Lx06khk9WDDhx4Jc+A/Ig/58A+CndD2D8SYRtxCxMFqlY5Z8np9ebvFsYotQkt/EiTyuTlNL
HkHfLMCD2i02N2jCQh5BCPFkBuGR6zlKqA/THEnEQu8w4Fk7qY6iPkzEmxrnOUaXQMUz68k+Rgud
Tmk+8JIHLYRVFtgvm2iSw4ZVnKu3+qQsMwsm5sdJsjlw/Rx4pEaZKf47yXsuW2hPhsDVEQYlWVVX
l508gurIEsYpdKDlmgrMv2QIckeE1ljtkU6Dcuih0wmrJDW2IuyAugKnszES/Vbtm+E13aXIJCYe
i+u3CbrO6nTNrQvrCXFAjMqc+GAPHA+O7hy6uhEykqCmZlnf6Sz/F7FWJ/Qc8Q+mLPS5llSxMbVH
4JY0BgCgCYlUjpSf5PBqmWqh9sFUe1DcKMvwOYHgL0tfHYeykzLSetkH0urQVVLy/0UMx/f9cszc
OdJZECANrLyce5f0kYBGDQvYRsytBjv5jmJJPKeH5Ji/2H64yV8iC2wrv+9mjFGUzDe5l3foEUeM
TpAD3iK18VA5YnpwPGOVC/zGR+gMRiLj9OBVj6jAf3psUNDh52B5Et7WLinqI3rkemKoar/719pj
FZUsIzAj8fSt+Z9UIWkAh6DMsMWOi7utz78xH7m3Zree5/ZPzDNtrKG63dDGq88Hm2bb3XubjjWI
c41HohofVoWC5qLVF9ldqiUEVel9nS+KYAj0lhp21lrLB2k+xS5cQVwuxLdFWxGQxm2SEkEmNOV7
d4rW2DpQwgkQhwoH7/nQoEqTA45S2BAWaHLsvIEza0H9ccfHLuTonWfLSj8BMmMdSmqL9TbhFI8o
Oz09TFbyKINNSGnFoZzImHsGtKeFds/hKmVgpOkpb+JXiKRvt9kyh2m1ga5GWBucU9p3Inf/GsCJ
49zLd4DNerRrAsfltrY0vTwMZZidsVFC2ZwxOTPlBaL0lpZ4B90JQqWY0eDycRQOV+MKfej4eJl1
4BRR7DgbrKVM7OrIkMhEDNby8MGtKxkuMJpzTUmxqClsDaK38MbBOkxROjuabNDo0v2kZI+nPcpU
pDVIqIaIiiOisxiqw83XSqIZBzZ4+5KROXlrXzUtTfNrBLORWu5uZNLgx2rzwGMnb477QYLiMaer
7F1yv3DsJcWXNllkG5IFGqAio8WB8Jd2hOBIH1h4oe1YvMNyjnPGgF0kF5KWC/wioGH2Emcc1iwj
GgfolG8egdRZVEVKEuqfu+MXbtjlpvLAcwn0e2DlakO6lH01LO8aqQjXYy1+v5hd3CD0bbmiybnx
GJBXmRMvst2SjHAIfEspkBhXOizb3/k3JDJwqvfEHOoWlLBbh6vjWFO7XTLTdJm+za8vCIENM0Bv
FttltI1rmKIb4aMd6v5kyr0fswi0g4zOPmZuD34dDT48zq9LWAJNP/BO/a1/kTaWIanH9EmNvcJU
dgUMYPSCfDUeHmX4sdKGTGyfewvckxBJ1Bh8rw+bV5XVn/3vNIXyWDOVY59MgH8TCdO56Y3tXOpp
2osYzw8k7C6QbwcUqmDMFoGivAbmdXwIRNaj9NWzuYu+hFzlZCVMzS5iqZFHPel6fPF1Qz/Ci/xz
hZ3RaV1Cjg1x/B1P0SLQwiI3Nmc6QDR0jLnb3OObnRTAi1r/b1yyGoI9rXKINT8WYxrezEss1P23
ylgYsNk4XWUDhrmkCTFZFuOuIO+ZJZKrztaYzZqZpIpxmfolCAaRKNMsmU/jVfIOTbJWm7531nZ/
67ObcIHUj62d4JZ1wu0RoIVJnYIiQusxQXgpgPcoURTIBbjla4Cn5+XpUltcSwUTuXejKALncdp3
CVZbO71Xt+EVo+du3lhx3L3wN3IEP+gD321uP7YhI1dQ1AoB8pgXMCskqlBA5rgM9HcnKVQLryEZ
fJc5GpGaDAT6JJoSlLYFP5HEm6naTUE1I6J2T4GPLkuKvWHRIyKk4esM2HOWCTg5cU4iWrxRkSoz
+R7C6Y5dFfX52SaOIT6DC/lCAzbeuqaXwFG0z98KPHqewl4Mv3C0nt2JiyOBcrSrS8CGNwfceJtV
gPp07RsmZcAh+LP3N7OLksXFHbVmBkHtFsF6e6X6ooNFYXnIfH4WbP8uvaB6Npk69VcHh+TkIg+l
qWAtn77KSyMAfkAdFXZMbct15DVL53E5H1Xsy7XxHXBb3Odh+tjIRbsuJwI3rEGKBmiqSE68H+Vy
chhzMecHvhCC3Oj1F044LpenRHxf6ut++atAMBz/spIrP/paiobfLiriTS/qtQTtar6j9ouyjAJI
rgTw2jW90+E0dF2QfSIH1a7OYgSDiSBFrGaRfsrjvT+2hr/JWhUfs2Z/eAGCw7tjwUt4jK8pS13n
YogpFnLPjqoM1yhd5M4Ae9+z7VsTV9Lo2obtHbEadBcMvAHpnaLR4IN3rqjcaqefzb9kY3Yw+IVm
bxfYVsaxTfSoj5YVI/7QcMn4TNCNqZgYrUclLxJ3Xf/djkUmbK6okhU8Wt4RD0u/07O+ItPU3RKW
Soruk2W2noAFjjWyo86R7Bfu/6Nq8kW4k4Mi4RpumYCEuhDjwSLQD3GYCHd3szEcKUruC+wyoSQ2
uE+bhvJ0EdP7/FckZou1YOHeccoV5Sy0kJvBisOmVGzgdiyGkzEe4sZRMZvPDS0iCShHI7j9o0Ay
OzDM9Mh8XeCeDtniVrk/N1j0i/IwTGXfNWLKaugsIisGWjWG4wv32pVJjwBJQ/7o3layBPmTaEWl
q4kXEx4UO0RA/vGV/nhFuizcwgxKZeOk47BKjGnQiSEvAp9eYP9hSFmqLldCCacbmKGm3fqtJ5oR
fXninKppcTtjcLvAhAvgj7p90zkIhmJdvuK1dEPY0y6xtpfzP2xMRFLP6PRzYxHPuqcr984wskzo
8S+GDtEYeSn/SuKi16k1/m5fZl0iz/hyxn+DN5/ac8MP/88yXlDs6wu15I+uNeOhQpEZY+9mGv4q
Qbm8YT8EAw5sR46K/Zi5lT2VRRnuVDu8yOO07dQ/G2exE/uK6N6WDEoh6VayHnEuGx0zwUbPLj5e
fmNXD+Uawq/SdhkCNEc7QyfLkPHpCavwyxVCd20HuKDAT1wNl13zamEZ76Z4yQYMZxDvzWD/UpvC
dx/gMxDPoULlIgfvg6Qxy82I4XoEzTieZW+rS7rT5/pMBDx3c4avjIcpjE9H7wG8a2SYkQ1Ojpdw
IwPVq6QlYqEskt1Lo1bxT0DUsFJbbsJa0lfpjtPcJseM6qUzXbhcT9h3wLN+e3QbjBaBvT1AMtwO
NG+FXMJNEPg+cfb3Vep331L9LZ0IIOUHcdgsJD4oeOJ6WdPBqLQABBHMLCjMkFtTvRPHDPHZrpcQ
cXVGSPXUHIE95dNRnH8ipbSKnRCdVC1qw517j5X3wBOAaizemVfxJXeHon5owa1cdZTBQcZYRfoL
YvJRqy7uzRrIgJj5yQSSYKnQbp7GLHPj8kwUEqLXknLv2AdJJyN08Uz1NPqnrEhDEvCH9ykwHLoH
Kfd9ARvb4nAXoJy2gOD+JBEKTzjvn3a0M+Lx884MyDV/s4qAI/PPZBkICvIHbSweYUXD4G3yWveu
ywQXKl509uQdarg3Wlo054UW6JWlxXu6R1Eu0Pc/hFyBMMUkJq4t/oHB4ODMzez5ipaucUpdAkY9
Ft7LM1QSQLU9c9cvxU4BP3SSgxDXsfq8TFIx9spJzU0SOszcUb8+pRXf+kh6kwFa4yb/7uBbspoT
yd+pWHAf848QFOGXnfYqrREoavDlWyWjboxebTW3iO7vslOGO6ZMsRdc7LUx/ZF5nXpnCNtgTp5Q
GSXhRCNUVtNNXTQkr/AIsMRGVR0JxU+ZfUKNqfJFcXH+JN6hBXuqVo+zz8Q74J7t8Dp7y10wVqn1
6mxHQbdHSa9NNXZZtdzlkwE+djpDngAqtRUs5TQPqKhYpww+lOKhlC/iI3/pyiFla4raw0b8X4am
I+V8QelIFnzfwllkS4QHMiG+rC6qLbVpsm02pD1ux5Pu2yV9LnC+xUtIE+lTcrfPTrtAX1tVEbc5
OEdUmgAweHDErm0D6NGQ8ZtQKmu+Tncg4bygMPX6MqsT+EVwqu4C9byyIWDBFAYuAJeCfUtURHc1
6KyB0HffE16WiDmCyBcnFeGvyK5TomBtMgAmLwGRsdu7GeR7Zchwgu8y6jEWFaOjmR+L2SSzb4cj
GT8ZJHStVkV+WDK9AQNznSkDBpo0iEopX7qA6qEXkekgAoxdoahb7VjBMtjHsWGF0Gz58xQV8Cdg
tZ75EDkrc0fCJ9Zd3ps+5JiYaanTl4EKsrUR4+RXpo+R3nbbo9lqP6EXOgpvkIb5DrP8kFCWNtzp
fJs9+PHUFDJ3afkuSl946X33F65jK8YVxpFryAFMPLElki94v96j7SkvXTPDU/3HlmqfXkhYIFDy
cirbSn07HLcwd9nzlxVGn9+nrPOclKDO5Z2l6YUYvHsLaOSa4Eu+5B8TIE8iE5/E8vqklPOko/Lz
q8r2ZCTYT6h9ore1AfYCtppBrLDTUQpq0PoIqt09sQ6HtcwZCX14CVqM3hRCLBV0HjipewgCC973
B3/TfeqN5t4EuvxSM5tn05GfgGimSLDrk2i8ZQbi9Vs3YAUM6vjTHV/y+Aw5olvMaNgKqYL7jsKa
X9TusO3Gwr7rZP4Vv7nt/9tWS+yuAgySQ6T7kEAkBxdQmo5GiOIgZ8+2z/zrjYOfBrjUSVk/YH9D
Q0yHX84pYrBRX3+WbD2lvAlHIzj5l/sRXXN4HDEMPxlYP0YGMsFDgyaUiOoQfpwAz9XfaCv3qEAe
jkcdCg36/IU/lGp32Fhe8G+Jylxyc8Hlz+AD4F1+aneVVhXDwDFV70q8SoNzHeWmSbfFIFGDV3Fv
brIxmCVQkX47PUs8/LWTyJ2XhfslURD967f7ukmzhSaPMKS+7lndGBvPnAAWCfbGQ+hGlPc+Yk8b
pEKKC/GZgNpKmvbkSBKh2YVFk2YL/JvztKQmceGMm5ARcApkqCw2VnPVV4YzQ1IVO9EeW1P02pIF
hGO9rKy5/e3EXdVc6HgHy+RBXxvTaXa6c+dCEmKh3tqKzvrttUj8hZY4G3kYwOVmSYENFCJgmEDt
qt9NXeayIt1Q/N0oIsXg30pmIgCkA1C73R/t9bcCppoQ0awk/yHHY5BqMFDMIw/S9xSxGD22/Tcn
oiz1X0nZ4dFeCZ3t6f7T0Xc00bANcRwPtxIEdj7m2rLtY4H/ny+v6V7IYNs57I9znRz/vlMPaYjA
VfYyhMbDSafL0riOA0cb0VLCkFKKZcgVSoKHRpNWL9NoSF8KwXquGkB82F0C5vE0PAQK5nRpzxaw
+d9He3FL0WVQGxYEtSuEH3JvGZfpM+8qBK5OxHT7dtdxwTxq7dpaQZ9WVUrP56285lhqOcmvdFW9
lzRavCXqLe9uWDW11ylG9ICINMO0ya8873Jt33MT1lS2nEB8H34ofqxtWOaHdUNxsDX4EMxhS2jv
oCjd52U3MzDHigBYouP22/jGk62j3c2scSvIV8jSh0Je9ykdy9Q5dwcyGTQtb1r2wKESsTY6k3sk
igFrdpfwLwa37uyNecB9TQc+TiHgfVoZQwW08ft74hfyH//zrn6aOjzWHIDoL4dN64Ga0/oDftIQ
UKel83B7U7j9g5pYj3Tqa73ySS/zRLEvBIRZKH4l3DzJVpQMwsBR1fQ8izmCngwkvw0gwrt5zFRR
WBMasoGPg6eSKEtrnQDPebmX19XcJNL8kg5AR6PyPfarHGCXsF36vOiuK7Ab+Isu0BLULvffHf/S
IhBDkhfTVDb+gtH90KamzJrQJhDdpjPtvouoNYfaBfw05RtcP5qh5to24TC6BvcZANVO/wvNdJu6
/HvANBk7Cr0uM8WebfKJqPUzo1l6oZauUvEoAjuL7XjM4c4yhYBzk906oTvYFnYf0oqPm3aCeSQy
YruZTZP4o0Pw8X8k8kNE2x+KHz+hKoBF4MK+WTX60CJBcwRqNjA12lhKn+OH1HouzDfEd4bp4pcj
QxQqpT1Jd6E1NJu5mGjMJkEOlXUyYkGlhjCUYQ9mP7q30UIOYX/2iMPt4/0PE3oNtPyo63HUGeg2
bDyEsj+2MUK/cCGrny7X3bd4/h13Iu9T3JfDl5lXeZqXdZadccsLQQ0RPHr/PDSC9DUxn7uQT6Ll
cyCf8saoAlGsMxymU2wvRNMeGPVo6KQzIfPNbodtPFcaFbkiiebKXn3lyIMQ2FOeYccBsLyqFlV1
ApYqH6e8jIUCrL9mCPGT61OpL2ePNN40gUy22mPgvOK7Uh6mQcZVbmSkZw5p/UlB7KwuGes0dIyQ
8CBJ1tWEF81viseUY7kE37EFQ4piMa1Ex0g6opTyypKtntBgGbqd6uNZpUnfXn56AvCE7uApWETN
/in8caiSDEJPYxnJ46FCkEXgqyD6RKWb7STPtS8EPtVWeT9b/o6kRzrsI843ZK7OxA5r9CZrMOSl
ZUToU7f5/ZRumBsqdPspY7KYiRL0TMAyf/uG9EwCduxmbCDN+Um146ewdQuPHN3XIeS6+pXUj2rq
XY8P7+ZyC8jteZ7ajGLuEavFV/kIYlKhFND1wT1Yu37BLCUuRu4HcjS50CV9WX1QuQiAW2NilNt2
wd4UmHRdYdxJSyF/+NAbBxp/I/dH243OqvSs+EmHOg5z1XtAYtOP/Pz9bbSt3PmoPOmpis9ToRZP
xTc7whmz91gz7nQr6ESZCWt1pUwZHLKtrjjudxri+JfIx/4+nQY2GWCYEJRJ+sunvHQx28J8c6vG
qz8W2gf81fwJfIPiSIQfkcKDI1EUU8kuzOGRs+30mlCypifnJSz55vfvSmaRY6/0Z0M4b4YKiU5D
TtaawxhxyE9o5zEukK2+NcbG6TZKDBFIux7Nieh1+7X5/7zuH+nPZsqyfflGwEVW6/KrSPc6XW7D
VyK9VxSCzdyM2FPANfG/z5KngXV6FlOb4UnJ5/mA494viHmp/3+l30VlQOm9XzvUHufq1Zycnd/z
NIq7B/7dKZrI+txLZSziExDjggUYofXN0OZuj+BNcMIjV8fBGEFg3pkA9HwGdUVYG+184Mbt73wd
3OAavis+Knq70Sc61aFsiP5u3t2oJ1n7StPBlsqvqqwqU1ciYd606ScHq2MlpsXhiLKsEd0Jp1/D
Ax3AFhz8we/RdjJKVRWtha8AXjMcZ8T2Ih0x1+Dss/oEAkW1bTZ5u5xfp2fZEtRdEDtl1UgyDA8j
hovAP30LIkYKEsFsIoZCbbsUjKhZzAl19wQ7ebERGqhyRqL9TDKcRPX/S75sEH+xbMIw3Rq0OqBX
woThHu7W50vNUrvsQlR2ld9JF9tJj5pUumMSu1EYFDJ1D4C0VnQ1GmbVUpxpj/xz8nXfDBbza/pr
5EHDpVsKXjypfCR5rkc9SPEYNSr68bhTl61msCP7Z6HMTspj5gustrTKLWx4Z1vQXfU7N01LGSwH
QuLyfcHeC0djBcS8RnSqfeFV8FkoHMtt9h2awJ0azMHyh3u+ozL2T+LbOtCmSIcWHOJwpMuWSJIi
uc5Gla3NLNuklJakOK66k/6DwBAo6Zklpout9uIcDbnPgAsiCymSes1jYXf0iAengGRBagb54FE4
R1j+LWAMGHyBWKlumXP3vPwqcwZlqyaDJ/XJZaZLe+EayW+1pKi+OeFRlwP1d+3oRV5q4Dj2s+Gb
j40/7RcX5M/I2WkGeQXKkHn4rxEZz7kLY6UQghUfDKfnhSLVj3VwtVXfM5su/FGXkwy4BwFCwmWF
E9wQMU4Tzwq6OJE0CSR3uTmQ5/yzQwxDNqqmlgkvJeLucGoxU7Y07LFakuPxRmfu3mbH4twq8dWL
1YcqMKKilT4LWVDrbQLgQWsF/e3vzsU5LOWaNPm6c+a4d8RCAGfnOWkedMUF2GuMFJwqGCAfovmj
Qx97WXJqszsWrfMJAvI0LnYF0zR/xRQ7T1FzUntXK+ve3fUmYmvLB63HAS+z+aNATCG5+c3kWoTq
rjNGmf6NxNeAFopHEpJXL0LQGUFEgu7cJNoE/d5vtj9D2JlTtAWlgOsOiZ1GapdnxUgg/Nm7cgqN
VGOrFcmw+o46Xn+N5/uxMW9Vhs1YJCRWtoz8EFKvI5qIxf8SFU616c3+aY7Vruc2ICMxb6MBBNFo
XoEIr3C0bQNxyGyrzAj93A9k/KoxlMWtrmqgl+Y2obalqSgxMY8xOvn+z2zTXPNq6zSwfLCwc7TO
K94dLuokg05UQ/JgKxfKKQev2hN3MdXHItRX/d5OF9+/o15n+9rLUr3JwyHW5s3Mvabfl2Y4eOQK
7V/N3MoXE/YOKmdA7y403Fxg0gNwLiRfOj+EKknMua54xQ6oOs5mCJqu5mu3RjeXX/R8TgHtu6gA
rZVHAyHUov+pMoCB3SDV35pfT7uPLuVF2ISuevpkZ1B7kgp0w3yQNVC1bGcofGXVpvtWfhrHaK57
KOFuRByMi8ojJs75xe97mlgozL6VJ7oN7/j47wquN1yA7N/hgMZ/xYvT9tu4GcCKIajthMdhxhMu
XnJ0TEBtLDP1145muZFgd6kzO3zw09ir/GGeTx6GL37k5wVZsKgbudyABmb0iTOzym72+Gb/Hzpa
r1zsnkmze+3c5A4kgC8WmaKqfjVe+vgX7gUVRPTgFH0sTed54WyMlMAkC69eGNcUAWK02q2p9YP/
emywfTHpr88WQ/rT3kG3EnWyJaUohAouBO5iyFnwZbxwZO42W7wAV/AqJwyVY4JKFvgeK2VuLPGs
4ZqjanFk++2kPzPXyIcGAu1ci4wBpgKGXFUWKrY7CaCQQveiWADlHtOLbdKKnz6/AM+wU9r57Gw+
NlRspctbd2ygjXJf0jm1DbnvS90pE/OOJVQg4PDzIetnvCwyCF5iDA5IbTEL618Egb1PjNosSB9B
fwF/QJuuqOqKn/YFhTyxiCPfEdadLKu+8buBvxc+0eIw9tpmdXG83ggnyDRBBWqpFqtn/wDz8fGc
2hVaC3I6Ho3uiP+AUhhltUZannSbtm6Od8KFS4kfDlDWIh1DZeMba6nnUXuyJXWyQBvtje0XJu9c
ElIlqjEPustClKZJRnn9fupVniqXhOq139u0a2i+6YQYix/a70CVJVs94IXoeo1fsAH24FL5iMIo
V9DFJYCRnrCunZ0lCyT8+NzW1OAAk14WBICJbyapLJNhHJXyzHxYYOxg035pSXPZDge4AnAT+Kob
CdpCVgiGZpSxzF9XdwqMMj6oUsv/AWwf4HAYFqVn0iGiwLGQVOxLrjiZGyL1JRXjrAzNY4UQrH6j
UQG0ZgOfplvbx+tZ+q52tg5rcF/8kUGB4ExJ/l/qxb/Dn/HxL0oF315CI7XnYeO9v1kIitJ4YQr3
zJv4sjmJz4g+Z23drfqImYVYaYfQdS1mz/ReCw4v6pH/O3SpBD9YO0hzRh/T0N5xxd7+nX5zoF7R
XSyg9Ca/aNpQoMwpLZz14qhU6JPO8sSq/n2QVsF1Ld7lkPnhETx7FEtfZR+G97E4LftTBfRoO5JA
lgxJgoMS5QoX8rPFEdyBEub+aAXiY26YjhJV1KCGJDs0FVxYJPsuujod7Yc0rKR1Pk8GwoH77s2r
lDBIZNcIvymMqZFvQVdiOptLlNQBN4d3uMNsJXY2YQ64q7ayGla4/P+LL9EN5189XHdDZkoM/ObK
cSdCbJ+67vRPhv6gz90WPSPuNJHZ7V6XYKNYDKNgTmZ5HChHXyQ6n+BkHh1wI12PKI/earse1ZtO
FgK4Md509ucLiJJgnH6Z6vam92ZOAcK8p+GijEXLNp3+D8iLxidjTfXVD3r6+0E6tsT0oRRrhIBs
AQWo8GU+DO3bnc/4fxzEUEwYDf/xRrJ/WodB2x/Sb8C4i6yPKbXh7CqCjSr7fm5kqHemNorUIs90
evjNta1ZFV3K9i4q5CIechBcyIPCggBmqaXMk+kRASeLMQvcJGpMoeAD1CibaMS6VEGhMlxB01VL
wDPg4PejeuW42aw45en/BVxNUaUtDbEyH2r8X+zLqzzv5Rnr9bIhDzckhpXQQy7lkzCnUh0K20QP
EPG2AacyA9fAiUmJg03lX9YuIGSSjjBSyjjLfPJu9wIGPFPU1sD2preCuLFtz7LYwuDUNA6vlkou
M3thr8sPpfa1/rTb7cVOonakm1vczpyHltM6Ee6Pp5+3ADxIIrKMzzPMMTuE4RmZQOICmkpCsrG4
nLwmynbyhBKScXlVETtY0PPpB6foJkUIHTx0q/GLx38EY0xedqyy4oRTMulPC4k2STkTpoQ0V35h
9Ce+LPxzeD4Gwyze7JWicmUVJhSq7EYlYK2b7ED4AoRtthSnlj8N0jWuJKA82zKfazHBcBvQbzov
iSCg184kBqfxeAt0ChaObrC8zTVuOh71eb0boGcaqFxo53/PfQrYSGARu2gaEvGRen05KSAf9tqx
biKOWwSns/CQv6tRivORar/47cdrXsZhvGJ+uxnhzPAOkopq7uS5CET2lPON4yxwlShRdfJVF/2R
0io95sT6k6lnCq3Ss/bnDlOvuLpQrTZGUjlD2O+TRq9aRx6pX9wL56QhQ6padOZo2I/RHa1tBRfQ
JzcFXIukdXj50oosU2nHTthrMZhkuAiS44wDHuZW2FcNHaprxLpLK8m6uCcn/Mc+prb0kfLXUZQC
T+AY9wwXZqwPlz07HWYLb70+YpdxU8/Rvc+Ri2blfo1Logi/3rgiSV5COoJC54SrKhyKeifMbY9K
Y70W4qMshwbHUHwOFg5FBAfwVm6CATc/GQxHka0TbqDHGSjggCm6nqUzN8zi42HJ6LaHC1ZToDBZ
I6LC5kuLKRua+WDZ7bJzTYYyNoi88/aokbPt+f3leX5J9LSSs38n7vXMMvpbPw541enJ/JnGPcki
Mt80N9ex1AeJQsHD/AxtCVuMPF+MNlcAqpm3f7a5BMKDGRQgZ9Kc0YQLDBuoA9ZU6ZUN1MHNnqoK
1+SBuDhrLGHsW7DK2+FKGq1m46nOzvURQNKdzGXcSGRDknNbA5afcV+wTkCVgr5aeVJacmf2+XoV
6oWlFTfKXBlvhaXvWKx7xiGDPGIlIu7F3KLMe0hLSiiFxaesTxc1/nPpA2GKCBGm2uy9Ek4jQI2x
EtDpdB/n7GRTiY9wzUNBhbRxqKKtvFX2bg6Bl/aaj2I1rm+OD0BwXyBq3TQyoygdlDNH+sa7V8w/
Bh2KO4ZenAoCLsEQvkYRDZnWoSPDvGc/BBT87Agq0OU1baQLtO6dV0QvGsXGTwzHI/uYZP7mYwXd
shchHOUL8bUAs1uthKdUFfdtyFWx55YueeuKmWiviWiYMKGxC0ZGnw8xVHNTYiMiSKucJ5XS+NXB
JvixFb5kK6Cad+Kp+NU8XMq5iiA/I6/LRfiMuYISGeS2PEJ98VtJ9LWcrSvhmkX6UMNSPeYTMWF8
lR046iRAdZe+HU6egOqFpz6y1hhIUF7bbq/je7KMGJGsD6kkGX56DmWodVGRGJNCw0eIelNHurwh
3j7sooQU4gZ4RnsZpyFo0l4PRVkIRXLFHC1hZb91gLNlol/b0gw3L2cBxlSfVcKhDg9U8LfdYZCC
ersgUN83bI9UCN87v0I3+5O8WsKKNQvevevjJA+UHvR/2aciGOCFt+UXvZfi/6AQIF2cbMxqVfbx
wlGDpBTRqh8lrAi82y0JfZTOh0z56lDqkYztKbe88LU01puO09ici04Ee+S38VjJlqiZmDP3WH3q
/zDx7P1pOeFfXXPC4q7Mm8mxyYOg9frt3OsxsW5xIIpZGrjWDYAcHGNeqbXw1yMl2akkB9E+nwBk
MajFsuG4S+DmNe55PqcS69q2TMMji5nz2m9eel3IFWtQkEZ7ZO786LGusPQS7X/CUMuxq7iDI+Ny
IP1Rq58NQYK6ozE7iRRsz3SxICF0FwPe+7bHWqKI5DzW4rWefLEo62dpp9XSPWs/d7K2szesQJCE
UOHXshLk6bOLjaUb+aNn5ruZrdJ2lYgOvVN8KXEq8Z1NlT4AWgRSMQDDPSLPFLgc63E8QED9F2TR
oDfIj4UCH+r434hjeJBshjhI9qNKGdNp+fMRt1/3V+k9MKmcILUBMHMr2oczYHfaNfOE8Hq+kmuv
3i8BU0S3fD6FGpppAsFa0pZxX779rrsKrwiLovpQ1SSN3SqYLJtirZD1GliOG+NgKCWcpzLHfRQs
lXBaHHPnT/hPaWUVx8Ho5jU1wH3fsMXBEDcE0CnWMgizzFds7WznMBcz0g/iZmQd+/0fFszhHEU9
7dsyaqArBvAeEwi/Ng//CTCVoKWLvLpIVlXXjK6+QbqhdOHsfp9paNR8OXYaFc00ZbWdauVB2yob
XtSFzy6DLBjJJsLA6/mLe1z4tsbzSbaS7jPZNE2A2DP50FHxp790FSHmds3k2spKiIP5gFUJKYEZ
mEhc3Xcf3cvAU8qFmrc7unFU0H9V0dil88RboY60MVsP44YKgB/z83ZHZYnecS9rgpwAtAbXHjuC
FE8XI4HZD2iYmoA0ZSQjFSqbgVkJHPL8WJIZVwEvjd0RMQkEGyNTFMbzf3Z/wnGHnK4RkOXtW6c+
6InNc3ew0LvYUEZFagLROgNYK6mfIoN1zAnwOz9kvXzswRksCWMuVzPEb69mcnPCDsw/RY5lNUiA
3YwudKIzFf+VFIVbJ0w+14pU80VlX4F+Tic1HFdsj9kX8dN89WyUYhiFci2ZaJ+k4kmrxEGWVwGR
NiROs8CQmm1RbkaLaXSfXv+YQAJ+9QIBOwMqi02vjRzSEdgS946Jc90yGwHwrMYBz2hi/OBMa6ay
9AkaPu4RCHxPKY9bxH85fOVZ1V4eyQLlYtKN+JCj8c6ciNs16bA0drGGZpyInpAKB/7lrElER6md
DsFLd7TZgYOGbR6EcOjw1Q0qXJo2t1lXRfO9qfzkFYCNIkuuRq5tpXx4crYtXHKxObqd12k4K6SN
izqywo0yJwzXnvWdbeNtv5K5XYq870CwRXfHAsa2F0BKJFwrAdxb3GZnm64cw/KfkZikx+yKR2z6
dsO7Va9LlnkvRksZsuzTtozGxwcjCaK+wDA3sDhq8s8sUoDZR8L0dV+gNPMHqYMwQ2+qo95ouUlC
hdqLXme6ioZfwIFur2NxlAogZ3Q0EgrelaOrahOa27nGF4tC9fnoAKZ8rpXxbENrHRd5VQJUHXJt
fUwMMFXyHvQsMt/orKGtqbhfalMib3Po6o3NnTGB0s6Pb4cRNkJML/tP9mLO7c89pumkBnzrkPyg
BKfDKxU9r/JxP8j2fV5K9DExlChPtmhniW6xc2xidohsaV3NnJBWeCHMDx8tIiJL+WX391dJQqCr
9H5s8GWod9QYWW8DBm3/zd6gLvLfl9Bp54s7I8GZab5s+TlvurI3S1CnVRQb+CqwbpLCFTlUza4h
WCmRxBZCH5UiHaaw3Z4BN6vDOz36km1xFHtYbnkU+Rk10GEymhfxLfJ3RvRdL7u22WdnrVwMcxPZ
tgiPD3KludRA+BTL+7wiVrxUBJomxJ5tKC0CSNBioH3zJiigV9DWy0S0qvGRFjaQ+b0gOGzqS0Iz
RcNXG99ImoK0IPZJ/idMzxGho/YUV8Ot1nxRHyvyrKHM2K4IHQZSoq6zgOlXGH3A78YM58GHtotp
XgHvUHTh4XBUObJAw4G/97Ls7iznltrh838woFIxN9gKrXxabsKpCNWNLAQH4alJF4S1fobTrxP3
DHIf6wT2S/J2CkUYQdtM+7NeFpTvjmdRvpCR/rlYJMQOI1KoGGkvu/bLvCBK0gXnxoLy95njBYm6
Fi28xU5zIzOL/L3Bps9VdZWVqo0Empk6RmWTfmIWTDzL21eArr97z/avWRPrVHInwSvCS2t43WDq
o6KCuan3lwbnLbxJD8slDSwg7VZ4RomqlJrZooaTe+w0vrTbnVKNVIJ285fCFbGGRyeNV0WCJW95
vyzYcN6oJYFxR9SP+K28AS7dBHRMtZSmSzkYA81N8lF2Fh7BoF1Shb9lAhloP3xEzONS8DXGlkRY
Fqr2qVMp3OuokSm/1aAqlziALArJscGa8w/d+huidR23Qt3cWMhGdJl5zqpJlc2bTQU7H/loRzmh
E8/FMMJrnk/z904mWqU1hqCcuHibQjv4mkFQfoIaE3s+gqrTkNOKfS/PIsh3FOq9zQEoOJHHEYEr
e8M5/9jdMbYUDJ3uiXH91tDf4jmEqSbbDXhsMNc4jQKspHYUE3FrRz4WeOc4h486BJDjXzkOyohT
Kuic96Llsr0C0tNOJZj1LaLZxeznVgFPyGy6wjI6VZm48l2ycpxAPGYCwH4mwqz51xqU4l2k98CJ
yAH7AR1GxMdoUjR+2jzupbGnrl5cA6WxV9TaX6zSvLJKFNLA0iKqvMLhS2Eoe8xEH8jf3Zvtoynh
KWC6wCVv63Wz0y6GMmtZkL1+o2iYg/mbksep9HxJ+kgSppg69GjzIKvjPgFAukSR9xFxiEcwwQFe
IiZIhPZ/7s8YuA6kQ2bWxHs5Q2WdJklHnIGzC6TX8dv1nb5r58ylwDxZxqlhLy8snPpr1dpbGHcz
EJdYpzGFqtZAymie+n4h6oPZJvIZmS1mAJ6VoSn7+vDJ54RDs4nJphqn72ez1Pn2sPiRwDUMj3yK
1ZuUQsa26dwDzypALJQXdEAjO+pUok4vUimUncu6FT/kMGw/EW7w4nzSw7dkIaRgZc7dVqU1AJtL
men664WDpG30XfloyrxPCfHv+8ZxxzJjilA0ZfpYBHUste33ATdDDqgzVc5GB76cpIEQLHsflnzv
gJqZQDV3dbzN79nZl8Z3ln3DN5wUK01JgoBRmcCnWp04JpbAKzPzCMskxV80+s8HmGCqSTwfSevF
4q83Rtp7hr+ZIjwlCs4cShKyk7j49YxG524ml6mlTlB0KX9XvFDQgld6zcsVaWbmD+tL8zhwgfDi
HEsePZXxe8IB/O//xfaBMEDNyUMrLTqDwQh9HgLeUUGihaVwtXAhZmiB+POEqb5r0vU2QHkLwrWC
kxcwj/dwAvPYr+mTMaDsWA6P9Jy01nHnkoAgy3aO4J4yfzfVPnUkH19t66IL5pqevrdFURd1JWYA
urVBPKKKC3BT0ozeD1FvAP+7yiilWF2kA++T1xPkAb0JRAm2JAvMb3S6oc5prypcTpAfEG6oSCR+
YO9srxc4hOSeSErlC3IO0/VtYuLHrsUi4W6RcTWzKrk7oeNThGjQMuEX0caJ+D5Q/r5mmQkvE0nl
2NOlcecPmrMo84KNXWw3uEnfhQjlu7FKAMC9FVHC6C0WnO4lP42auKEQC1zHOpXkiBjQJ/iQRyBb
vg/Jv5/AVuk9EUagxmB4jz4llf6HojLfTAYcKYpzLQbEbqAY4q4MGxmHJ72QO+y4RRupTPgrGvGC
nrRnihUVsWej7Zxj72Tdn1gLhxQSgspkZIEek9Xj1qy1UPUBhsAEDQLw2sr8uTOfQElyY5cIOb6t
TiEOkgxtzQfOS98xkjfB2mbnTGNMp8LRRycTqF70/VtyeC8TJLJpzm0bBjOXFEBArQPrCxc/1b0b
v71yQquOiNFVID5ytRcJ885SySDk5Lz4jbTg/6Zncv1I8mPMbNvcF/x04+PQqnVTT4u5xt8ilr2a
cXunUdcO6urc1wuheXVBvZEl71vcz1qOPv1KAROeftoVvviYpSA5BHj4uQXeDBpCtxpKzAFpLtbO
4POWMBUNfEgSy8W72H9XxXXa6sioBmP18CPUYZxestOSeKkgW6Ntn6cZ+Abi1AlzbyQraGDCtGJ5
A0Aay2tV7QHLmhzouoKfhlicZKD3eGSPfqXuxYzHRjRcNh/6BJsnvINmICOtNgyHFgLeeAvq0VtO
A0S24J8ggtuDC1lIctgVf0xaOfUEOKiRgQebM8PXJ0ze9+o10ZARo27jmZR5E++FofDnYLrmYoHE
5PdG9buFnQj2pEWJ491R9hcEieluQAns0CwTZRLptZPaKdPdH58sX+knejigEmhBHakl0ODT9Kox
6rF4bBxkVBqKCZs+5thJdxCS9Y9LpWYxZeVfpvy2BOyCbbDQF98+hL/ykKxMI/X2F+ZnaRU1Vvqc
D0Y37XheaRwkcEG06RKwuMlwJu1H+EHQWmv9uufNv8Xgk9eBxSBT4wGFrJOCTLklmnjvewXMhHFn
+yrGX1DEyycJU1kDr3lrPmbhywAeWj6T2QHDh/e79gw1jpCUU57z1drXpnvCZxReCJhXwBIQ4UU/
gPjfWZKLWtLRMKkZwqGBEkq1P/tOcOkmKfuKzT7wXzHM1CX5osZ8Njm81714IwoNQXB7gIYHJl3y
l8Bidi/ZRzOJocyCJIOUyEMPFI4S9Zvu6Ga8zV8kD1u0Un6YacrcYXeoyZIt70jIjnbx4ccSlosZ
1qZpzoCI0/jTyCzmh5EIJx+gysAu+Wv1JtUfgrLRcHMTvRnTprz8qOHAfqJwc9J19GVxXSCPobbx
SwEomuryqYOmYqUPLgrFYvmzNu6JL+qFQ+yH3yr8bjLACFZf3VJr9Ky7Q6X5LJ/tROiDTdH5A2Tf
q50Y3ceri8Fwd/ewRNL0bUPtNn/Ui/BHQjWTZqKaj3KsEBAoJ4YcOSUJQnn6x+Ftc5NnkF6M7JAP
bfajZDUSy+2eOIQEbfU+6XBOkvUYRvxqGoO/UX7kJKh0YM23+lGXTKpywWst0wLC7Xe6SDE094hB
B4sy1Fbytk9Xyp5USzaKzXfiYq3mJYMzIhPKnJQox018ya3xweix40yCoexKSzJbEQqn1KJKbHH1
rY5b3VMRPriG/XYRJlu9iBTuNRWT5UwM/3kaP73p+qsi6R9VYWj4dcFWnoY4JZWfOMYfZ+ouoq6c
ym4XbVaKIA0xPYtvtkOFk4Cco93PnPmYeZsl9zdLckwHYzYnpD/Uvvq26l+XtoWLhT6Hl7/4c4rZ
OxjRvwUYkojwZ76VYSNMZ4Yb1DzWm9UrHY62aXpd9I/Tx5QAHQx8jZ9JKJI2iXHRD0MLQXho9S2d
8Do/AxY5LavHC5l8sVG3lqxc6rNsPpmesW4TjeolUMrAz8E45rzhHBbNjK4dRINCIEd+uZVR/lp4
bU0xmQ7VmsjH2zwoXRw/3IEw2Ao4UzkaU9NVqLJDuX6BZuehaoeVqlqCAdnTI7O0WCYlNPYEjyfK
mbhr5bAbpafQXHL3DHue0aa9+3yGclR07gaKYAak4tWqhWGKI3XHVcRP0HGl8eatoDnKy6ayGjP4
yFoBN3YWkQ5Bp8F9qs45iQSJ1jimLp+dpoyg3gHVL2HVvVjd1gFAS+13wUzhGtuJpUbNvzuQ0DSV
cFJv+utqacN2Ae4b5PZdRT/Q2NEw/7fetRyp+sBHjQ7cX50sAvC32qdpq8YSXosLlL7IL/zl7Yg9
gvLuutAXSeRammLF+NScPkHGHtAFWQ47sAKAlPGQbf/6ZB3IHks0bBXhhtkn3QdzRyuYrfd1cmnw
3qVMMnjiFafqXMAZuboLKCfolvcbM/t+clDgYMmFexyFrIUAw39OvgetWKZAJPZSRCcWFyP4aCYV
Z/JQGmwLWfsAvBLDbXkyyW66UI5oOtN8PpLOlLH7LKNIhX1EmbEN1LQJBMrMY/fGdX20dnJ2TBob
7W7lboPVaJEzPjBOD0xkgEBV22P9EdeSqlOoi2bF5+5cQA1gO9eDhcdkLTGPKCUbg7o6yVeBSMNc
gpYCAk7s31vsmAI2McWP5JNCPoCTkNK8mQhWxmyQG9xmEX8k3GL9O8zUQ+pe6wgX8sHKaNlJXt+O
MrF15a12/4dyosGVS4RhgN0Q20B6GwDRlPpbtXmaJvd0Dt06xMA4eZQ/XNHvpJ/8fL9chgXMizFW
7jF3lHqLv1momrU/Ng/1NI5llM4HfDuB+ryE67BZIIoBpBW+Mb5MnEmNDbMJ0wTLPXzk79QyYuU4
IDCqXsDygBjbrk4DS1SC+aq3iAw8ejK99sGx9dIPbjyK70RMCbKQLuDRuaU36+vlKX8b9noxcCSS
uNdGJ7h9n5ebQMVNws/Yy3OwOjxpr3zy7H1s/6N7pfKwMivl/AMACdfnmQ4guYBVDSwQGMSIYZJB
ceMzlAFETpWQ+Pbafx6syfk13/zZiBelBHuKEts+1UG41SOHGYfWOdD5onjcWnNhem+X2bPu1Nt3
3sCl/crdYbbQZ7XA7SJ6XlE8zYxcBVlP7So54lYXfbiEpIPHBqDLcP+Xx9JCjxpzpLXxEwENa6uh
0CvaDpQRkOpV0mhVDOHXeMW+BKxDhjtwwl4ZILdtJoEkNBSEXaodoJ58RStMx+qmomrR9m9pYfJl
gtYJuqI7iLXY24/M7aNsGuMqyPyc2rr3G4HZb/dXQQIiX9HKx+kAYxOBKR0sJ7xOsCLQb4x9TXuE
orxcgX4HsskBTullm5u5jWIlf5SPT5OHP6SknfS6NdVS/MSSsZzM+nUQ2pxXshyfpdxvqJVkyfxR
JdDg6GH3YR+Fa5tZoi4VEnlPFG6UC7Zzt1Zutlkq3EkNCk5Bf8x9JH8x0Pibf6A5TBpVcLDl9Jzi
GfS98ATGHLBdU12nXQqGm5mm8m6ZGV2zyB59dE3lwjE8anyUrO07irDQMRxN/U6SbjxN1ghZx8MB
0egRLj2tsSse8xjYjOpmFM3OY70fOMmhbhdwBYakMkuUNbr6hPXIGNcldGEkoNsj/Wz/Nj1EgrRJ
8ozLMzpNXKraO4mlEHf2mIDImRXU3KqBZswpJ1kl01a1UV3oAy+FdOcCp9SmdcExTTO8JnY+7Z1x
xItLVDG6fWFcuwhrCvu5u6mqPYwuSGIa4y3mJxeI071ChNjJVhc+m7qS42YBMGcfvfgMwzBnWZY2
zK/c3e3TSEHm70XnXuWv6tFhJQC/7hJ1C/R7ieorBl12Rm3bBNfQSo+8aUY/vQKAGoM/bjjLWrYC
jmd4hEfLeweCAKoKeHX4jzK2AqIB+ddhh9UlV5ZEHZbGY9bGBPczco/Cr4pIYKSe+ydydPKutFA/
towQfQIh7FQSAfAHntWvvj+6Kz7F2sL+abHKMOO6nIugLG6ynsvZzLbDMgJU66XNkAkJMIYDcHXq
G2/jJP+zETS8/Qem4YNdlcYVh9QUbDjgwR6E2c/+GSy4WwgWnMXt/xnVKWgObv1T1tNlRbFBW8mP
v/SaGwrFQWYFvMQ/GT1jGUcfF37BVQ+EeHz25um2dBozxufUpXV6J1f+xm37DP1UiMUoekiou8SS
3MQPzBjQOTEPkOIiUybD2IkXoh9pSE9CCtl4Q0Qgs/2kE39S+McYXQdAhFvY3NDnDR1PLMOv1vBK
8Mxhq3sjmwdGNlohx9d5j0bfXQwmM7ILyUWUh39T549GmM0hj1EhbE+4vaqi2YSx6XWc8SKf28lY
6eVaQF5MD3mxciK83B3kss3GLyo0FVkGYCObreWGXBVop7AJ8G4btXw1hapiFKtjHzlWiI9VkEYA
lshzdgz3AvMmEdWNcnk52ZYHWOumpADzV+LOn1JcmUt4hwAp9LlHyTicH+Nu+Xq9a2GyXe75VqcO
2WUr3oJa6bqhUmZpi+XOx3UZUn9xJrDZU+L79gzVowPXKXbDooqXYvxymSfP+WsWE3jT4HJv2Gqt
I4S9XYfyxuBLDo3qdY2dBwSTe4HUSTuCTExAB8dFAI4CMPB5Uqje72/mYQevvt65sbU5O/IC6xpQ
8KR9B8+KlzjVvwWmeQ+BHQVvVb9OAJzBORAChhFc8yI0yTyiz9cl52EBTPSFpwPAuMAm6Dsr8cj7
Hj0oHIkCe31/VIOWsqQsJw7GMfxFOWJqTnCTsH08wp9Gc9GH84liKD5DKWRkxnOXAWK8VlhzFrkL
H0F83PiBctibrsZOi2Tz+YemTKTeGuLLGJSEAeYPGFfWkcKAd5wiMxSqgOdjecjHATi8IYO/d69Q
gAU1xO5rQbrT9NvNZfV8e9syDIiV2QluAEujTp+n8GXaBWFXQtJAqiyWshJnygBnq/iVQ97kq6Hk
8GZO6VzZdJIlDtw0oTzzRhpmwNCiiLkYXrIGUqfJVYCLFOSUaGAv2Cso+1igs6eYON2KqagaaQaE
J+DwamD5acQICS2fUK9rqqmQOO5PGw9SvlD1yGQswzdto6nzJw/LC3HRG7Q5J0P6POidnBacNIK1
Ew2Xc/w6beR6dpGz0W3MEsm35RPGxGCYRwnhKZ2OosImCy9YtSHfgBumUAbhBJz5FMA96lFgj/jn
9jqdzeSbb9WNm5IYv/r2K+JuxRT7rP1bUQaIsHuIWaaPNMCyVLp+7eB67Ewa+cjDNRSugmIeExVN
0YbnrjhicZn9Q/JFcJ8FAdQ0Rn2mAdRZ0FwNiWhsl33fiQYnKEjntjyydM7T8de+y3F/4Oq4+vu7
sMYu9no+0mM7aO9OpHJ0435qHANNTsyEfTMNSXUQB5H2oGwTuXSN4t2ma8jOUO9sDzYBWIKcJc+s
kdQDBPk7yRPvx6t+w9QDoC6ja5bya0oJLs8Rdo/arW/oa+I3e9/JrzuU0dymiOdz79ziqLH4Plgv
x/TSmm0kByFhSDqyQ/eJ1IQDZwRngYtx3z5Tg7CY0+JXyxf4qn8c9QWdoBWIFZ9CqKNB2R1ENhm4
ujiz1ecn/tZR5dYL5xDvwrm1VP117GiHV7EhOriaugCAHemvmFWr76Jbreoxb26gaYaVYUMlOCaN
gi5wsB2khieB3T/N7PUXIJJc5UDI4O3KhOc3uMcnVIojW3E3NkIF/nrAR8eH9ML4006B4lZjKOom
aEKcqnRni/5xVBz3FqBijmSGevBLt2j+QmcFxedzKHhkHKMmnsb6W1K6DBBA/gTWLVdVYPOe+3eI
T7KG3Me+6HPOff8sRiXAoL/y1TiRSJI1WMeyZl7bLr9pU41YsvS5o+9BR0yezsfhi+stwrj8fyRL
E7WnGXfX+/QiLyHcyMQLspheVc3a365MrKzjdlFsMquM4xc8fyYeoCJ+hE1sRKf9SMzAgB03jcmC
3itpUTxIOzdsPqnysnUJJZSSOvvmauitqjB3DS5pC2ADDUj2WnxS3Fckejn6sR39dAc0tmO1A9NA
/WI7Vy3mMpjS21bCNqycezc7PGVWg0SefoW0VdQ/MaMeHKY5zjvw/jtC3NcNpancWTIAe0d7s83W
KIM59+Fhvo1H+85f71iQr15ejVSNurZ+nAz4lZb8B1re+Q6yu/b4+gMwIlkZfEb/kv2AyaNuZ/3t
Cef8+YGXyHfQK++hOTIoUWWpIkD2Xdmj7bGo/rWG5d5YtDenaoCfZbZ2iWm/GjTtGXvblsA5CBt+
towjNCX9i4XfgPEevfJJuOO7EI3Jxk2sd1v5nItRcY+Z8Fq36MqUf3IqNgpLuxYJ8GBz7fZsdJVP
xj+0mDoh9X10K6sxzM+mDC+20iLSb2gDMKP75IVjSOYp8Z/DnnsppWdxP91EK1AAtjAJ8XHABDcW
cEwAn2wWPuLX0sdu+EnLKPYAwgZ1UUJtPTpTQBrKg5WJVDNQRPxjYvhFMAecm1E4J284YRYiHKry
M3B4NkyQowB3AH+d03Q+uxvyGEix5LHTpnzj9oxTqkx/zSgtOaZIupTWXBkDI+wnebJ0E1aRLJtU
InTWqDL56PJcbUKTmSEc9clVtUEYWcKmG6FmFG3YhLx4fDlm5aI2W06Iyy4I6HiIWlXuBYcOIRYM
QHQ9mWz1tAXYVFufLpecHW25nTKL4CuTyAMSHNVDncCsa0omSk/ge2/s5L8wedxRDyrlVqMS6UGT
H+Sh++piK08xF5iHJhKqH3zAHc/qjBwuoanir7Fw3aOKexinonZW8+PdUD6+8qftceYSNakZdNJw
XVClBUM+I6acb4KUavnwFfl9pc94BVM65FUP0RYKloNKdgdAzVfRLrn7NHfrcKWgwHSk2wODNq2h
H/iMFlDV28maY7wVcqMdJNhejRaxHNvoAGGnVv6Ru5TYGckZkFnVHM2thYr9uDll5ZmP9mJTasGl
6V7240vJXvbpeHH5ga6xhTrsin/WInGQ8wLr6xYb50ik1XZoDuP0NpN4SpTD7inSw9W++KFG+N56
JG2AZqA+EZkR4duGvGk+/3m0u/vWxQS/thJg6V20OldlCO9mRWTviscSOoQvSZ7KvuxK2a5A6EnW
sLvUvPoy3fJn21MYmF+C7kmSlBvL7v/xnikfr8r1lq7bujUN3MwjYYP/zIEBjGOJ78GR4M+17zU1
8RWG3GOchFK4jjBt23hjcJf8iZI9gewYBDRkmas+qaRlCpfJ/PJeB8QGpl779N8wuz3TlfLzNC+l
0MFLH0n5mj5ra6Cx+DA3YumslwZhMDJ/6CgIGE89NzzXRkC3/cz+QRcqfPLMm6MGLKuF8fNW8Glu
i4lcK/y1wwmcUvWzuCm1iAfZ8xqY+sX/juf6avirYDrtoOYw7ymxTX13NT9iz6LZ5CwslJoWWxQ5
RR9A9O52KxG0nqoswfc5630pSLr5A4Z91zRJWI/ijca5BZWLEYiWKYDQQPsrHEJjNQ2f6ZRk544J
25TyAUdDYMeSjfKL3wDpqUWywQo+x50lWJwLs0KI/FSCnFkp7y0HR8HMACtjQfWMvLa6f85wopKr
iZ9+wJuf1vVwzr84ab9Q6uvbISg8OSbupX2iWYx9QfIEAHXvdbwxpVUlGOjDjbl1HwRL6F4GWf+Y
6MdNDZq/VfRcrP6PF5jZCzpoouuVDMC2T1+VQLfIEviNzZKeO6X8/lntyN70mBI+38mMQBhB9/G2
tGDd5c5iNMiRAOWMMMtv/7/XVFFzUL1vSsCN/mvovgNihc1+PKtNUqkYgDIoxmV+9TCbbWPLiBfp
3wCTkKsMXlb5ei8Cu/w7qTTbo3LZ1jqolniUvFzlaVCdAhjJ4i+vJKv/SRzt058n6MEwwwVNW35/
NiHkBOM0YMygN+6Mt5flV79igaqymwbRLcA5X5EUIrYXjhar++hDBayx22x+ufCQ1YsEizg5bhnH
7IeY1QGv9hBbLSlzWMMV3lEuLrpJEWZWLF7iIPjuz2SN5bAiZt10gtaYnzgO7CWyIEuLRoUn8S7m
SMXgLoFSmkJFWTrnpd5tNSahnxEk7iK+x30C1DkAA+IrHpcbWk99/VQenfFmv8KWF0eui7mHrcr9
W8xDvYo0LerK1hZyPwQv93hQ7EqGCfB8u6HWiy0bGIc+atUwphZxs1pSJIzUrvLu70quWF3QMUr0
epvf2HLyVS6bhFzRrp3oT6GQmUJJY7ZaViuCNBntIfO9m1Qp2CW42GWixGwDOuyfLaC90XbT41pp
FH6NlqMyCoLciSP2Z+soSm7WUG34nd961rXoEE2qlYmR7sWsH/SgK0GbnW0RzTwFALcqLrEIl7hD
aTnEAK5inXknINNFFBDWzVe/9gij15eLPKrEUix99NG9r69/9sycvraTV6L2Rf7xga5CaxvzgNMB
svDkxpXd7cG0+Env2UJw86ogqJmqRhZWiTZ8rFZKbNOPL02YYvuzLL67nt5XRKeaPweNRztrGjZX
JOVei2Gl0513O4If75k7C6moI1gIan9f48fpIA70q+nQ5EfLDsbcBHSPD/XXQDOwOLuTPfmbrVfw
Z2yGbCRq79u7BtXJom3XxkdioZUjZJfKjW4V8WyhdgXZaUc0lDttTy6y1OnZoMg/g00aIMfcxTdb
wAPkz1byRCzKwFo4D+7ce1gJ3HWQI9UPwaTB9PSonsMY5zao1LSXN5fP+8mNClP9EG8vLlaZNksg
RkwbuqovxylTkKze6Oy/AnJBF97K/HFSvoNciS0/QawL5I76dKQkxyENGh+IsH5IrLty4P8VWEn3
ouOKZH1bdKgFEBVbyhwbvU/rvOGHo7AZAhNxkLmehuyS/qRDPKTyaXjJjMghb/RMvEdlL3jdr3yx
b1Rv05V+157poh6+6Darssn2X7SEgVvJR4d73DgbCdNec/l+UvWEaNwvcbYFXj09UcLWioftFuU3
Yl0OmdF8IQ2YdHtusIWPusFvLwOBtjzOxbLMtDNcfEgJxPuHCzGpSnF1oe6z3hjWuyD7hZRjy5aw
uurpuGTN5zUwFfIhRgY2B30jBWAakhRJwhOYfghaYsvg6opt3XyCJL5y2h5amreQBvRPPMsXmuIc
xjjuWoJC/NkLUeMGqR89Prjd3qG25oyFRG51SVE2aUFXvD+kBy0n60+u3tF+MlJ9QmuFEY2m5ToS
rh2LQZV49m4X9MgCTx0t6xhtfv1wPJ9I7Z3srsGZTfB5YbN7EGg8JI+h9OIM72sVrKuY6dmCPWrh
fswLicNVOrI7ozUXkeErznyShCpX2MIXjS1bdjz0zTcjVqc8uHKfNW27sGpeIpYegTwVSFAtDkiR
Bi9KcB6YzzSysaemxb0YurRikFp9R01z3NLkK3k1gdyziLK0fVzf7lHVxySC6n9CKot3zTiyaPCG
E10/cUfTcWcS7UTi200c5HvOe6IzHI1iFRKdWp0zmmBXH7qiA8YsD1LDkiceShCil+Nxt3VGfmkW
2zlc1zFhkY1csY92ubxwIljdcQuhbBkuyFFOt6Fa42aZ7H46QEaSRtmmYO3mv133UCWsT/TGRR05
3vh3owoSmF+hMlCjaEvgdVYXg04vUmnCVsqBD5g8HhEtt8KWxRvQB79fJ7T6zDXzdAsnKGVnREvW
0Qkfm/vhMFbTGgFoGAXZ8e4WeKkZTpJ7rjLu1T6OR8AoL9D68ldD4AaTxTVeq80D78a6SFGGnnxx
ilw3F91Vew8v0sAq/5t4hT20NV6IsY4cqQ9GLE7HUo/uzpGzHXksH2a/g1lHpa+aX2CBCy/Ux2C4
CTJTvghvnB2CYuZPAksVeoL6dJt65iANneJLc8CnM4Ryvogb1f7xf01a3npvPkngGE1ySrIGUKAt
ZsbUg7orzIIfgLptkN68T0OP58gd8Ls5RQpswl1IbR9HuQbdQvt1lME6GqjfLBiIXZdenfQFHQPv
1l2T5wyhGUY0l+OTYK+wwt4buZP6vavlLwtR8+sFeKgbU6rmHbt52Eg8aZ7cZT1cjANTHg9AZ64M
C3VJLOwKt+CHK9p8SRbRvbTNFIZ4dDfThlkFL5ho7Kari9BYMXSAbIs6O2A2Ps8suX4ICOW3jg+l
Nc+3H+dvrznzSA/58UY88gVwNk/9HSzIdA40ag56Ct6kgR0Pe03LQUNBgYGYrwYSESJVnYFxMAYe
Zipfn5qIR5wQ8EK2+DKDCGps7BXk0+Q15N23sKLbPV02YEiuKsyBJpRnfQ8ASLoZjKOGnYbEPfUJ
JLIDQcMc7B7/crfWdI8KsVuB+lOuSVHDF9cHOudVvOBZIkq/VXxwRZKW8MdAGpHxP6peoWRgydoG
WCSP2+uTPHtVGmEA6j1zSxQVOj4OqijTEe8LSR61iqSWK/3wu9oLr1uUsV1D/1Zr90vFIyzkeAOa
WOb4KAMz0eNH6YnUs2vP1RYBhKpy0OGDTKbnaPAXIUquU/tC0qxIUYMTHitSo/CI76n9JPuKRbXn
e+BN68XAibh4t+Y1hE9T6qbf2pH8fo6ityqf3E4FBChj7CJXx7oyFkrGqfA6x0VLE5NRIVmsgo+T
vJzMQk6QA2M0xwILOOrGxbGqWNhwGwsZyyCWU33OMO9jugsW44UsSxP2HvrolngMghu1eIzAYSCQ
9s5VCviH6fJTC3NiTojoxdvx3w90DfiBrP50HEiKpbcLbryAMTuB7t0RkQAbYejQNwVG21W/mM7m
4Cp46YoAhU5EG038pKYGXpjqf2N1OuNRU41otsCpz18oeRXBL+4t1UClk8E8lrzW5DwdVc6N4twS
8RFZvn2n2bcDMNfG87q14HvKgZgscMpelOMH8KGXAK4qoq3eoemBU4sjDMXhBBo4L9VMqmIu3dXN
FLmQIYejIBNf0u3OIBWjYP4EfqQBWzKhGl3Ga2Fjy+Tc9KS9/SKhQb2x6Bi1AnHs36jWoiKB9j+X
F0eWE9ORXpgyFaWXtwF5p6FwgS15SMD66EB32NqMhJNO3QhxdTNSXwMZGjWwYsAiPc1zcyvPJ7WJ
BD5T1en1oVX7fW7Muc5o8iY5G0S7hsbo8uIkDres3/eJd4rslCPU0E+xkpTAGubMNBV2sjJL7rSa
GdAfVbkxLlA7hT57R6tHyLaUOZghyKSukgbGG1oRtmtDt8yC2TI1pvVOEtqp2g1TWWLmD+aO/77N
JM6/OjZyhWxgVORh4lSlrVQDh8oYzr+31cqMV8rN/NHIHsBeLxfMSuZANUXu1tKzKuZ8m8+c2vSY
wTIUTqNmlqZ/tjAgcpfz+v4vpimDxQ7TAipmYrMCi1Q2EciauAe2v1oNBCD/SttZYbqULFldM1Rt
li8op4HBcrk6VRP2ciaKR+m9mXLuoJ/CJEjXJjY6GXlhIuI4Hl3PcXpLLUyx+Rjeqr1Wc6X11SvB
7SgCWEixRGzCOcZESi8ycWQgXOqETyBkHxce2ydQ3emVLxucm0mSzYLz+1nmVSYdFQO4RcrYJ6WJ
rSl2zW2GJF0W62XhgcLYvNw9cZR/r0ekyypi6KvRBl2s1YXQAiy1z2kdoipFXRH3PBRWyO6WUDD2
1w5dIGEEjFHbZdH/cQBqaLhad7WTQ69QX/Im7svktuYTmga35OGDDzCXt88F8z3vaJ7uNEf3g4QZ
nY3PXOSTQKSssu7sx8Jv+dCgRMQOnVUA64YQAGUg6Zr22qOWl0JhB9v4htvWkrkUEbxTpznSrFoF
e4Ld1GVeS9NWLG9P9ILpXmC4iKf18fWyqJfeHXhWw51lbN2gQpqtreioV/9lUKQJptQ/CXbBCBKA
dP4FHVIb8sFPh0evlH71y8GMo8bW1XiYsVw4TbwFF+IJlBjJveMS9d1n3zWat98o1M7IINCuQkKI
3Zqw9zNR1+bJJIot/O91a1w8QvlH2k511tjI12ZzcTvgQB4lFSYWc9MWjoh5frXpEzFI2fySVIcm
KQeXpXu+p6K/eWwbg6STczBXQv+DU6jIlS/bHfYcFTRI4e8QqXBddaCZj040J+W2EfeJY2KkqzAH
rzV71kbFcRw3/L/YAe+5TGak32miylOXrBiweK4cSnsCutSqXWMswTH6CxIrS3rLrK7EPBeHLRA2
5rMbK6eD5EX9gaj8yXrynTAZlW6oL+ldK50GdwMHr4YLTRJYYonPNwjmVQZcj1OtcNnkfW5MB5ka
56/Ic9CtHsPvbDy3qUta0lT1jiFDgDeEXuxEGyJcEY6oHbhxXG+v1YGkjQbKSlyyycJB93DnsKl2
dZsZse6mO/yI8wzwQap5yN52doh9yZvjEYQODPw8gz2UGLpFxOpEaiYh3RUWT4J0x9XM/fFgTofC
l25XF7sphBDIVojqz5RWdh992QoVTYsJLgU1supuR6aaGlAQxEXTY20+2rgPJAuYcs3jtUHzvqgH
ixLQ+gxwGCMd9vJUb2g0PmEh+gYHf18h23PSzlPFtyeAiV1Od4ybTDxr67r3TKI7qJ3cKie+CvVD
Zfqcc8Z3IukIEzQ6c7ZSDmyKAPs45cOQEJEB2dCBibqiLx7Uf/1g5iD7FJxa+IUBy81OeQNyuf4G
KLzVwHOSTn83ILOp0CscmBE8NrZUQXdn7Ce7bRFcfODiSCc0mbguNTkz1wyxZrVSBlVXg7/n+NaW
igeiogAX4M3sCq8DqcpQ8m5BML7/8H0B5RRrcDy5DiW74Fdq35a8IVZKEhO+sdH6H+vQ80HXJ42c
xIpO3XCsnAsynFXKpmsop1weoqrKKMbF43nYM0weN5pDigJPPPWdnh7nXWS64v1YYjYaqNO31MPf
qYix6LI9ZkvzjvWRcpY0hSP54WlnmJbbyXLxG97C8ixOC6kN0Rwnu1JqVMmPK2nlvRZeeWhQYBdY
xw5H9wczSW2T9O3CFzRpyJERZXYrpLjx9LQNa8/bcmGcz71X5r5dsMkvl3zX90uRhOyuusXhmE0I
hewopUHHsTi5sV+ZYVUuQSqtkPzUAWqBIfOkk3fB9/asqA2FDIKByFtsMa983QHlmZcMDzADnn+z
hhf5tSYgicoijCwKZMSWwdKNC4NGoQgURsO24SHGQmuQhHFOmGOllFNFncrqWlTpNcol2dG3BX3n
b87uwZWS8gGGqG8+x60iLr6tx2QJS+tR7ebAjmLQMypvR8pzkMTzwfLChkfDK79cbq7rw82GYd+0
jRSYPdZpXJCzC9/oQfQhF7hhDEuS7h1NQLh0nDtQdpGRY1s1ZBfeHxodTM8fn9pB3lAN8zgHVZBH
AGcl9a2lFcL7xzwbSG77bL+5YKLRk6yuVfDQeYtfjJaHeFTDK7V5N6MO8hnsUAJ5mola5jpuO9KG
ZPURLtzfMaEcbPtM83ca61B+q7x3oh4S+QsHVbhaAkFFXkAKg2k4EcbVrJaXn62hONQWvjuJBDsZ
ux6UM0l8Cp4JGnECfKKAS0t4XQg4apalXFxe0d7ke7ovcFX56ou2dcltqt0cqlDCDOhEr/Q6PlFb
QRfD9fnwssiPOMjadt1CL6h0V6tCGc//wpRZ2kxNqApN97TdCmxQpwBaQYnKyJV6me6rHWoMqN5z
Nv9SJsFxQLHE9lFPqkEZjOEh+A0LbLdcID84WAJ2rkoKE8QK1yS9E031vaHSlrPq3YcUbzdJz2Uv
GnTusXv81pKybJ2rbgnhjNG0GK8sBNaZ/lSkvAPzHSUerb9f/x519gKOjQ9CGadfKuMhmwUAC2aq
Z3VfEEmPFnVTPWPV0ie0ji1YNL/1tOlN4OqMJWkLkkwPXSd/f+AAJQcVt9IVEvzyQfmEwj5G5jd/
WKvE8sypHdhPyzNLcYTBliUu7KtGI4aI/jxk0w2pyH/5pZ0mzBZhb/bLzJoBKDsFp7lFHBz5WcUF
Ju76yJhLgArdfEWXnzzYdTlLTIOUQQz0+zDj5MPl1CgZ1XEgoxQxiQH0gxNHhjaBj17SNr0Fuzgt
uPIUQWovkuUbiglru3SBHcU6P5iCJmJ4QjaFUjLip2h7F1WxnY+oyvS9YRWQlsH0Vw3bcXK+ViMl
T6xcDgnWpiDbivLF8wovtGmW9HQeICIRkttHPRWHeIpH48FVrLZTTJGy0Uz6vUItutvKGywCjFl3
1zZ9AuKyvPUCb+KQZfAQkNkEVILhpuxZ2d5k6RWCQFf7L1UJTvNzutvu/0ulLZihI4wM6m5afb6v
RyPAW6RTi5DABCv5J3GSylYqsRfQKC2MYdfopXt2KlcZVl+nCNISCDcJR+KVH11KJ7Zpf1O/Wmm+
7g+05GrTQEkxM6WUrT73wUKG45UtYgQFVqWM8lFtTMIAtuAnKuTX799wLP0Dywnqan4vVyc4oc4h
hnyyZ4J7w0xV1+Z4z9/w/goTkxdo+MVKFTr/R2gojxE4h1q5MVMy9N072fkuOsULsn+WC45M+2GA
79KyiuVdxDt6yyD/T3uT0sSvNsvHkqcCgLT5yGdQFNGw/oiFyHoQF4nU7jMxJj8t+RhvQKDfNKae
ZlgLzQ4U/KHYaq+EDv11BYXJcAFCSY8emHohd9g1abDX4cCIVCl2fLrli8A8n/NGbRCIqtj4wVxe
68URUZFl9VHJOvw2pBObGuTMgg3sQxaWARfhB0c9YKlb3GSgQsnJONB6V60hs6PQD3tzOMwpzY+S
oYBcxxL5bNHHbpq+UI/ubjY/dOwwogeJx4LVam/BxqZKE/IrvYCCDVA+JdusJvpOr3z06Ps2fH/U
ce1oXNM01BF+qsjqtbqxDtiHyS8DLJ5iucbAoL+p7HCLlSQFx5VkfbXnFdy/g89kKPnBdF6bJrLL
X4iAb0EkWtg3uIcgc9XRkf7epqeYC10vJ/l849Mqw9vc2k/ai6mPNKHfK8T3xbNTWeTU9Ko0bEQt
5Sr6KDvDloDxGChBDMsrrRhAgJkSpAsSRUS5TTPck90pr5ukyu4hGvt1FP30FgLFKG+VV94/AtFY
lwKNNKaE8Z1UmkGw3/xEj1kfykvw+Ba442UABYEFNEfEtzlsMwGlSxN75QBdKmGT7hB0O8ZsClXO
/mChgwb7xc4nDHfdiXcVeDlgPHoz8KcjRBWT7ftWrA8pGEMhaOgPzOizuJjTV+YX7mHF2wKMSFh7
ucPIx3L8ZJpf3oWv+cDYET3g5mdu4cIgcInXIEs3HZ5bIzK3Bo/gIP1sF1MvDUFGUjVD8emjGBgM
TZQpPwrX31MMfoOot3k5mkL3YfeXDaStlz9Mn3LqJMvKJfOhFkq2WxUZM54NsrSCjWm3tErILYS7
corhe0LJxwsUzQor4FBoGjOM2LhNSc4d5ufoUIgWKzImBi0jlJm0Roopss9mYIpMnMY9g07mGK66
aDg4uXxFLBDNbs7zhQcsixWRiYcf7qOFzf0XQOogL5NwVc8NYKwc2k7NUdkUs1w4HyffemxPjUfi
CP/gyV/TSHIQIJ5gHBJVmtnw3NWIFrMd4FoOm7+EJfPAtFYxDn6/vQESZPFyPTFHfIfk7he/Ymhp
Ak5RQvzRepnkI00SxMciNFGi+gw3UmVCqkj2P23QP9onFEnv3+9BzwDz1H6atBj0JbrahEW11bh6
tgSNqT5LNGNzwYa/JlPuZeXKJmuOvFDno/TS1xJUOLQdOJJjN9eSuaZWkX2g2vJlEPLDBGugpDYD
Y35OgVc/LQIskzkzejcuM0iuJOoyzGPdqOxBMwXEwfB0WgOWq1vQ9LHYWcASZEaXUGX7aIw6A4KC
lSSj6N0d0x4XwQ6BcWKGcpoelgks0Fmg/gLIqYeK5Hz64oHYV5eKgC7Za5GfrnOYc4GuRQQ0v8Jg
J3SxbXsoHcagnE91M1I0FyiA9I4xfdBbAUtO1SnrRxqcO4yXN2tVlo3/1O/7L/f/DtRYxwKnPU9W
SC9M1tfIWPxcgEDF8OM4Csi/hoINIfvjCS4F3VPMCL2UbMKAZ1m1bXJ/MGz8mAG3QWMJs1pvMKN+
JnA/GABYzyuOu+pLUnF5nLomtHfup8rIKbZ5ezjY0dJsCKn6qHUydZVLZoTK2GXVmCisRHoxUneK
eXi0xv9/H89rrnsEC4Avdc2OymJwyr1bf3ynXhaD8ne+jQh290o7VqQf3oeDnOEboFzcYcuAF5ML
8ClW6whfwEbQZL5CtxrK6uKs+tXm59khIQLI9Qw2++CgX8FQd6TPpYjamVjpNTL8WuQOJ2C/Jprs
p7+WemJEXuZx9LAJ1B8sP8QMSqYO+2Pq59soMMl9FJDnApYe6LZ/L1b/hsmJndko7gU8R3Y1k/6c
3z3KcpzqZL71Phi+RKxGUUcC1biRfZAUdWzsXXwAZRYpicoIO1N2mLvIiuQZkC3UIWbFudYtkaJs
pYEf3lmcKIw8EXhtyPnRUakzn2Uy3tzKemnUhnDzsV7mtahaDRzZQOBBnWMFkurdngw+CBrn1Cc0
GPgEpfkbT4zmSPsNtL7JdASv3ynKpKpXAk9fMPQiiP7FMEIuYEq+cHeyi03eC9n9BBnnpP7nRO9P
N+m9EBC3VLNlSXJQ1zjGKux+EyTbGcRLJWDv+z4CqlsTTRSBCnZifBSNJAM8pzgItpQvW0JZpinq
v5+qYGn0AkUW5pJnalWhOXjME4GOFXc2W7L4LcbBFhSY/maiiDbQiM4B/TDi3MIRk5DQTBHiUHIk
7mBVJ832UMiCxDIrwdEQuf4VJCo93TwpZGrWNwysLnbacT629gASbxnJhxI+jaElpoTj/z1a/ve2
rV5TC9aKIExGtJplEukdrLRT+Vveev6ZEaRmxqpBPZniGSwpqHNwtMzuq+c6i7lJZVWhnfPKItzU
NwRnOX6XCJdA2QoYMueBteemmHob0pZFrv1nIPMDMSUTJhH7RrWf304ti9JK2MTRtsVVfk3TxZ0H
77R4rQA40TB2AIB9d0wFtUiGL130uWEV3JDJ+Ce4RNzoIRDv2Gm/sq1aG6eFH0DKnlDBy3cFNfnj
vjqx1Giw9RQ9NbOA3zOuwfD1pdmA6XozvRCSa51OeAGnpNS+CZLjkDnHcLHrCTEU5E3mUEQRv1t6
WkiqQmKvvp/VglMhzz8KQ/20CJKnpS7jMg3ydgdyy4JDjcIIiBzxTQy+3DUsdM86giQR81NHgZE0
SJHI/6XGAwWoWZUWqjRvJ4DlL/3NQ+55o2iDENlCCMkmlD4ycpWEboF9DV9puRSdMyRQjFwrjDMb
nBtG/gHzeoFa90OEn3TfQ0WCrcXYFXLgmrWaDP516xkfhT2TrUzte5CY/fNugZD3iyTuIOKYgnHm
ZDPyzWmDarNRtVyBR3tPvuf/0cSMFR5P84IeYFHXTS81HH6Wnz9XuP6Q16+DGU4K+h9X61P21lTf
JmFF8+p2cYUSP6Ly4DLsqyG4h2K2HbO0FGFqgPdZBmfjNVBHm5mzsW4Mgsr2OpyPd/aCWiGke6yZ
IBdRQiR+tSKf7pMC34G0YLE7xxVBHRjM3WduJLQMsJW6OtNQWA2qk36dx15HFYMZp96lIXAmuEtn
DbilRl4UKHOBPAswrK3WFb7LlyTYkQlISB6QacpCPR9OhSWxTuUVgQkZaeHew4ap9dH+oPpAO4Zo
vSPgynf7iKBOgVo+iJkOPx1sw7rQbsJE/Lx/FyE8vTRdka+7TqY3wi45mG7G3/KWAUwCzdd66lmo
beB7uSZuL03JVIzZgpNc7EPXwtUuF6N9ANd/ysBMfXGVVJb8t5BWfY9fPl9OkKIGvk7e0nJcGJIe
F+c6qnnzIXNfy+z6lbIby41IuQAVOjQH2YyCr1DE92teTnsEgx7jkQcfNOW64JOavOlCt0cBuR8o
1PTlANAmMYsDPghA8gbVlssJUCCvah8gTW+avAhoBsOMnqkVcGGBqzDDoebBu9QG0pUqOYPIphOs
IZqPQkJoPnxTbkacSIhf47cH9u/mSTUnpAL13NZlTXgR4jpuBcFkjtNcHe0Ck6kQpCIpCGjrlpUu
zHGM/iN9RphK3QL/FTQvS3PZgVezURb5SbCExWll1C9e6bkQgoa8hrLp7UE8H9QiKk6ITsclfOay
4UYkqk/g3FMOn1HrIx9Zhgrl8QumJuX2SIW2VoHuJ2L7VMq0WXFtGdEEHY+QIYMiYA22goB8jrjS
gcT6nf8tXFWbKGgJV336davmbW8KXJnF5p3Tr+v6cRpxhud2uqWWVT6Eq94YttrSNYiq0ZcuGgHI
HdCJUbEuNorkrbl3xcyVT189LQn65lQTN/Rk7ttmvvEknWNZ7vqoJAJ7HumX+zEkHl4Tl0hHGoGQ
M//jHM7FLaBPfLxuEStWaiqUp0Vqb4SMJvd9jQOLfZr2R+mSLLeORh9ifUHgcmCuTCaINtbgoh7n
Ln9g3JFGrMz51BWIAjM/Ino1FHPktE6kjnJG0OwQzXlzkq1GZpf1jL6M30rP2xyN
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD_init is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD_init;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD_init is
  signal data0 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal gt0_cpllrefclklost_i : STD_LOGIC;
  signal gt0_cpllreset_t : STD_LOGIC;
  signal gt0_gtrxreset_in1_out : STD_LOGIC;
  signal gt0_gttxreset_in0_out : STD_LOGIC;
  signal gt0_rx_cdrlock_counter : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \gt0_rx_cdrlock_counter0_carry__0_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__0_n_3\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_1\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_2\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter0_carry__1_n_3\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_1 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_2 : STD_LOGIC;
  signal gt0_rx_cdrlock_counter0_carry_n_3 : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_2_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_3_n_0\ : STD_LOGIC;
  signal \gt0_rx_cdrlock_counter[13]_i_4_n_0\ : STD_LOGIC;
  signal gt0_rx_cdrlock_counter_0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal gt0_rx_cdrlocked_i_1_n_0 : STD_LOGIC;
  signal gt0_rx_cdrlocked_reg_n_0 : STD_LOGIC;
  signal gt0_rxuserrdy_t : STD_LOGIC;
  signal gt0_txuserrdy_t : STD_LOGIC;
  signal gtwizard_i_n_0 : STD_LOGIC;
  signal gtwizard_i_n_5 : STD_LOGIC;
  signal gtwizard_i_n_7 : STD_LOGIC;
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of gt0_rx_cdrlock_counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \gt0_rx_cdrlock_counter0_carry__2\ : label is 35;
begin
gt0_rx_cdrlock_counter0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(2) => gt0_rx_cdrlock_counter0_carry_n_1,
      CO(1) => gt0_rx_cdrlock_counter0_carry_n_2,
      CO(0) => gt0_rx_cdrlock_counter0_carry_n_3,
      CYINIT => gt0_rx_cdrlock_counter(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => gt0_rx_cdrlock_counter(4 downto 1)
    );
\gt0_rx_cdrlock_counter0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => gt0_rx_cdrlock_counter0_carry_n_0,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__0_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__0_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => gt0_rx_cdrlock_counter(8 downto 5)
    );
\gt0_rx_cdrlock_counter0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__0_n_0\,
      CO(3) => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(2) => \gt0_rx_cdrlock_counter0_carry__1_n_1\,
      CO(1) => \gt0_rx_cdrlock_counter0_carry__1_n_2\,
      CO(0) => \gt0_rx_cdrlock_counter0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => gt0_rx_cdrlock_counter(12 downto 9)
    );
\gt0_rx_cdrlock_counter0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \gt0_rx_cdrlock_counter0_carry__1_n_0\,
      CO(3 downto 0) => \NLW_gt0_rx_cdrlock_counter0_carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_gt0_rx_cdrlock_counter0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => data0(13),
      S(3 downto 1) => B"000",
      S(0) => gt0_rx_cdrlock_counter(13)
    );
\gt0_rx_cdrlock_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[0]_i_2_n_0\,
      I1 => gt0_rx_cdrlock_counter(0),
      O => gt0_rx_cdrlock_counter_0(0)
    );
\gt0_rx_cdrlock_counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I1 => gt0_rx_cdrlock_counter(4),
      I2 => gt0_rx_cdrlock_counter(5),
      I3 => gt0_rx_cdrlock_counter(7),
      I4 => gt0_rx_cdrlock_counter(6),
      I5 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      O => \gt0_rx_cdrlock_counter[0]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(10),
      O => gt0_rx_cdrlock_counter_0(10)
    );
\gt0_rx_cdrlock_counter[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(11),
      O => gt0_rx_cdrlock_counter_0(11)
    );
\gt0_rx_cdrlock_counter[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(12),
      O => gt0_rx_cdrlock_counter_0(12)
    );
\gt0_rx_cdrlock_counter[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(13),
      O => gt0_rx_cdrlock_counter_0(13)
    );
\gt0_rx_cdrlock_counter[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(1),
      I1 => gt0_rx_cdrlock_counter(12),
      I2 => gt0_rx_cdrlock_counter(13),
      I3 => gt0_rx_cdrlock_counter(3),
      I4 => gt0_rx_cdrlock_counter(2),
      O => \gt0_rx_cdrlock_counter[13]_i_2_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(4),
      I1 => gt0_rx_cdrlock_counter(5),
      I2 => gt0_rx_cdrlock_counter(7),
      I3 => gt0_rx_cdrlock_counter(6),
      O => \gt0_rx_cdrlock_counter[13]_i_3_n_0\
    );
\gt0_rx_cdrlock_counter[13]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => gt0_rx_cdrlock_counter(9),
      I1 => gt0_rx_cdrlock_counter(8),
      I2 => gt0_rx_cdrlock_counter(10),
      I3 => gt0_rx_cdrlock_counter(11),
      O => \gt0_rx_cdrlock_counter[13]_i_4_n_0\
    );
\gt0_rx_cdrlock_counter[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(1),
      O => gt0_rx_cdrlock_counter_0(1)
    );
\gt0_rx_cdrlock_counter[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(2),
      O => gt0_rx_cdrlock_counter_0(2)
    );
\gt0_rx_cdrlock_counter[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(3),
      O => gt0_rx_cdrlock_counter_0(3)
    );
\gt0_rx_cdrlock_counter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(4),
      O => gt0_rx_cdrlock_counter_0(4)
    );
\gt0_rx_cdrlock_counter[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(5),
      O => gt0_rx_cdrlock_counter_0(5)
    );
\gt0_rx_cdrlock_counter[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(6),
      O => gt0_rx_cdrlock_counter_0(6)
    );
\gt0_rx_cdrlock_counter[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(7),
      O => gt0_rx_cdrlock_counter_0(7)
    );
\gt0_rx_cdrlock_counter[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(8),
      O => gt0_rx_cdrlock_counter_0(8)
    );
\gt0_rx_cdrlock_counter[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => data0(9),
      O => gt0_rx_cdrlock_counter_0(9)
    );
\gt0_rx_cdrlock_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(0),
      Q => gt0_rx_cdrlock_counter(0),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(10),
      Q => gt0_rx_cdrlock_counter(10),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(11),
      Q => gt0_rx_cdrlock_counter(11),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(12),
      Q => gt0_rx_cdrlock_counter(12),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(13),
      Q => gt0_rx_cdrlock_counter(13),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(1),
      Q => gt0_rx_cdrlock_counter(1),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(2),
      Q => gt0_rx_cdrlock_counter(2),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(3),
      Q => gt0_rx_cdrlock_counter(3),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(4),
      Q => gt0_rx_cdrlock_counter(4),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(5),
      Q => gt0_rx_cdrlock_counter(5),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(6),
      Q => gt0_rx_cdrlock_counter(6),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(7),
      Q => gt0_rx_cdrlock_counter(7),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(8),
      Q => gt0_rx_cdrlock_counter(8),
      R => gt0_gtrxreset_in1_out
    );
\gt0_rx_cdrlock_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlock_counter_0(9),
      Q => gt0_rx_cdrlock_counter(9),
      R => gt0_gtrxreset_in1_out
    );
gt0_rx_cdrlocked_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0001"
    )
        port map (
      I0 => \gt0_rx_cdrlock_counter[13]_i_2_n_0\,
      I1 => \gt0_rx_cdrlock_counter[13]_i_3_n_0\,
      I2 => \gt0_rx_cdrlock_counter[13]_i_4_n_0\,
      I3 => gt0_rx_cdrlock_counter(0),
      I4 => gt0_rx_cdrlocked_reg_n_0,
      O => gt0_rx_cdrlocked_i_1_n_0
    );
gt0_rx_cdrlocked_reg: unisim.vcomponents.FDRE
     port map (
      C => independent_clock_bufg,
      CE => '1',
      D => gt0_rx_cdrlocked_i_1_n_0,
      Q => gt0_rx_cdrlocked_reg_n_0,
      R => gt0_gtrxreset_in1_out
    );
gt0_rxresetfsm_i: entity work.gig_ethernet_pcs_pma_0_RX_STARTUP_FSM
     port map (
      \FSM_sequential_rx_state_reg[0]_0\ => gt0_rx_cdrlocked_reg_n_0,
      SR(0) => gt0_gtrxreset_in1_out,
      data_in => rx_fsm_reset_done_int_reg,
      data_out => data_out,
      data_sync_reg1 => gtwizard_i_n_5,
      data_sync_reg1_0 => data_sync_reg1,
      data_sync_reg1_1 => gtwizard_i_n_0,
      data_sync_reg6 => gtxe2_i_4,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gtxe2_i => gtxe2_i_8,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \out\(0)
    );
gt0_txresetfsm_i: entity work.gig_ethernet_pcs_pma_0_TX_STARTUP_FSM
     port map (
      data_in => data_in,
      data_sync_reg1 => gtxe2_i_4,
      data_sync_reg1_0 => gtwizard_i_n_7,
      data_sync_reg1_1 => data_sync_reg1,
      data_sync_reg1_2 => gtwizard_i_n_0,
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtxe2_i => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0)
    );
gtwizard_i: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_multi_gt
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      SR(0) => gt0_gtrxreset_in1_out,
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      gt0_cpllrefclklost_i => gt0_cpllrefclklost_i,
      gt0_cpllreset_t => gt0_cpllreset_t,
      gt0_gttxreset_in0_out => gt0_gttxreset_in0_out,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gt0_rxuserrdy_t => gt0_rxuserrdy_t,
      gt0_txuserrdy_t => gt0_txuserrdy_t,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtwizard_i_n_0,
      gtxe2_i_0 => gtwizard_i_n_5,
      gtxe2_i_1 => gtwizard_i_n_7,
      gtxe2_i_10(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_2(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_4(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_5(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_7 => gtxe2_i_4,
      gtxe2_i_8(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_9(1 downto 0) => gtxe2_i_6(1 downto 0),
      independent_clock_bufg => independent_clock_bufg,
      reset => reset,
      reset_out => reset_out,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2024.1"
`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
afmoZr9skqIZsiBY7liE8HhN5PT9cSTlE7b7FkfYEnmAz8/sYewHAx0YfvnPtVZDeaUQ1+SA/YSK
81BVyxiW6Q1+w7v5E1h+YJTA02rq79x+8OQbPa0mLIjdXTtNmx+BnF2Qrq3QGTPNoSFginwNNM6R
DHZuBkJnITzBkDYC/to=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
oFhR7iEHB5cYBaPdDcFYa+fK3Gj/00Z1GiQ4ZVkb1MGu+Q6dRi7Lgvhbwd39WT10uvi5m95gOjlX
gGa0fqW2XuRf5YGzXsxqVh0xVmshpf7o2GLRr2CmqpGBtEUVv4LbXJ4WiaEo9wVMVr2rBg6MtuPB
CQBdJcTka6nWZivdNblYGd53J9DObofIqJ9W/NUzFP/DmzVmGbctoa4amp6KUecoKxnO4tESoQzm
lz7s+Zslv3tCv5wVOwV3SAR+Y5ul9hQX8OljyF82Z31ipd+bzojhPg9RhmrvIMbFqzM9eU82pEIT
IZzSVKYMd1P6t1qORfwdgodKT0ZApoyFVxfVeQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
FJ5kCtzgKdagetGF6bdHAPKIu2cq7BcnQFRTWMN8j6rUq4ZaiydIPtRo6ybDhCzgm0ZRPth8Lgd5
+HXk3g/SdsTLZoOUuXnEz+TkfRJdu8kdJ+0y8sJMNK21IEW3pSWsj/n4toksSjLJ1wENuoq4XqDK
9VQo0Gobmx7OOqOYFuY=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
d/VWmC3TdLGZzbtN4vTxv6kYGkcDx9TNbvTz1ALcTDtGFLEUmm0Z0/puE1NDYgBTJquGDZ+j8d2H
/il9HfJ/2n/hTxPKUXm83KvLOZ3eaRdjaEk1wHy+UsjHJ1VztyOuT/nrMaT3sUq1kpP7wkC5I2EK
lQZbp4ngkuiSjWCad10Yoj0TX6DfE58qXjaybvWsINo7H6gMzi4JvYHWbBnCYQS/RkvaQvSsne0i
YvfJuYy63HVFs+Afh6hOO34nbqzXvlsXI7SIicsCUu21AgLUNio4IsL+Uj92NEPjaJnw2wTXrMuH
A1a3i03K7vrZ94AIVwtSXRwfN4q33BWP9bjYdQ==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
3xYA7hcw+q7P0QSxtus9heaAij7kiNzMgPuCXFQTIhWPWN0atyWMkDqoxytTvKyCd0IM0n0DheIN
XngnbZmTQpitsZh5f/e8iD/+iWxl+7S7I6wyIivHhUA/yP87+KG/vFGPXu7WexVyPmZuOgSS7sS/
Fnk+QS1l3qltq84DH59g9EBTV7YkcgLEdICkCR0tgP3K8aSVjCptgXzA0onYGrkQESEdw8nPfEvR
g/5lKhHOmmE6kTh/XfO6eQpuQBKOlD7RaVWrPxnTkM9lxrjRZZb3OSfaBLMKhMGrOdEY4RIqfHMG
MBOH8gAUoIRrgyLztGTWc+RsxUUYHq9Hwixqkg==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2023_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RyOZOz+qJ5WH/47JnOQuMFPCUAyXetmhoeJiNflULv2xv5JJeXWSOURz6kOeJ5/Jbz2KA/psbOVw
rwZKAlTJoEVMq47lcrgzkIZPdllrnwbNl8hiziEb/YEq2vvpC9EJzY7LlRbQINiQY3hAzP5B/s+N
lYRpAUjOhEst2xZEiFz6cr7zAMz2jn7lHWYuvu75NGUv8JJ7PZ6vUISUeHOPW9ljH0TH4CnOaTpM
Mey1hosrMsp78mwEmy3l4n56Khrq2FOUzGZbi5lhqEebDEU/90LVUpUSzOkhJ03OfvAgBuj9ejqz
WgZRD6DpCk8BVUByKAGJqQZsJ0NcEnE8/btwhQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Q98gl70eQ6J1AYIClxRNKl48Tz7BoxVypGMCmvUy8HqLl3jjFsIYy9amcj+/0Qts3PcCHOutO4e+
ptR4cjDneD+41DiKHcGc2sgfdKq4hG60mda13C9qtjKCuBhg+Zg1jHi3TOh4bVtsLuydPMwaILNj
7ORZo/jzBQ4OUPNFy2tkOMJArdeTOIuydxhUFtpy6FAABlc1SE1Mer24GTLYw0pJtOOJOX61uJuc
ghuBuHaXV8kExCP3IU1qx/oyfIpY9TQiqLKEf0YUrmEzBzV+JXQqn3Jom9PXTay5YJ+PqraK9iFt
ai1YIDay5F0ncLDXiT7OMRFjFG2mL+C96DFEmg==

`protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`protect key_block
DgXXbyNB3XMOH/gmmWb6uBYjn7XQuHgaTP3eav9Y+qTkta/ItNcuqONFgEZl6duKUkQI/YVDFLf5
JwZHmI7U9IACSpdRAsITpVXnabTJhDmcKIxi5anNhIeSW9RerBcxlMYtuoI5FgWJKz6h+0yZmes6
xYMyXTT3gJS5ttPc1n6kBHNZpxPgl/Yv979A0/ZxYkRTfuWrCyaS5T08AajGwOyC+2sH4FjL2xLX
uR9a04wQespOimJ+InC0FAfYhqUcjs5b18x0hSfPhe4G0xKNvmpukNOhIflD4bBgKXl0kqXnpatt
7ejKBpSWKRjk/M7rYOxErj8EahuRFH07pBR9b3XyXF3MfQBwAzV2GIz+A/XzIUDQOcBbojfGwrVA
2HhHfn8sKZB5ktpwF8MBjdlwrtn+kdQ8ux0ZNrgndrvw4nS4TWxZtQeRc+wfb7Q/lIzw6Pc+Ifnn
b7UJlJvNr5ZF2XNxuHGz5L19UYgB0gpH8xWvYpn0Od797z3FfYIiGMNP

`protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
YW19UgddTwwnJVcciGeEKvH9ZuBWuoo4LgtOhQvaoLIGVBAQAVfk5VY0pzvluZUn2+81fwbTPSLA
O+/RYs39A761jlViEBPYfmojlgjyiOTK9aHXNLsD3WBxfTXGOS3fHhhwgCmiZ0OncriPRHcNAK7N
4cyj3anKb5y4g9gzt9YeJ7T/zkLQuzT4zqnb9ijTisbH6HxpAVXI1ydKtGzVETw+s55Jb0gVXyGi
Q1hVe4oSDBTETiD85J2/A1KF+2DHKkzOJhr53qxvjND2l/LqsA0G/4J35IdLcmuYWAoCJS7cHkTk
wnGOjEnNbP+gboLu5UrXAFwXCo+jsZLVqIzGqg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
58apVjNnALRGpDI2bjJhSWJPQlj57Kat42daGg5JiM8yXDkjxm/BmzBRp9oBmOurwCnZOZcKlj3m
HlexbUqO32l40m0mMLkLzlGYPsduAUijMIb7DMl5HUf9XunE4fC69CyB0KyecsJZI9Ru4XxZrniR
CYqqjLtNjfFEppiRGRN6bXqv2T5Vgbh9qrMDIrkZnrIUB7RBd6xTPZ0xU1V6/f7Oftp0kyRv3C0z
HF7RFTnZ4y90YOsvuCM5CoAyyE/0gTXvBlB4icz61XmHsIlwfUu/xC6yAitX3dikrJ3DWeMC5Soe
vAifs/1pf/61AlzL055eu8EpDBQRH32ikamDmA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GkhzmJz9o93IJx+1EnrQfOwa/OOrgHQT2+XhwLsfJNRk9bxhLz472g/aTSOruRXBYVSa7twJJ6+r
lf0nvm4VQfQ9bG+fbYjOsryrSSKlth3/Bq54UcBz0wLrrf59Jt2KtKCOUQojHAIinG8CLxMBgQJR
wqwzAYtmffGsCYbobk0Y4IPkjQhtQamQd1BTI8ZwvF5zPN8kBYuoFLMFqwet6s+kcH/tQSrB0o/K
lHILn0b8uovehAHNWllP4phB4MeAOgMZbbsniJgqPQEhSd4nOUZs4WnWtYOsf+rCLliHTc9c6HIS
0u5f7mdvO+uC8Pi/nwNFr026nnYUX3YcG4zdBQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 17776)
`protect data_block
2zMXQCRGNp+uZFJJPkiE2aqxfeY9DhdXjfsxhz9PIMgAOcJlBEdR1D/8K8l/cUVGuhT2j6KLKWRa
wBEa1q+YhT4qyfvB0dv3bk2ZpFnE/0EOj9UkKSOfn1uGNrOZslDfmF5IzQkH6XkXT9veLUCXudh7
49p9o7aZgUCD0I7H+aklDjVJWc/OKCyzDcWs7XBNJbWJg++cai53w8GkU56NneavApFVwNZHc+MR
VU9zpLdvuy1vPM2kekwOyHuPZ3+7dBFATUZrat6/dw0xMalKs/W4pjQ29RQZF4xvLtMWAVUXjIwZ
uave9QbEAwRmCp8vkgc6EDS1j0rktxFiD0kRAMpm7Q21bXWvC8wcs0Ti/+q8vqGUjDRsSOXOnF3s
uGx1R4LiQxGBXvJ2by/LLvLR9sFuSZW0vGSe6gtTB/1y0f6S7R+X9jzPGFcZA5kj9KsqAiQMBC8O
SMBB9jVw8ntapLC5m+H///YSHehqAUCPExdi0eDQtVCRrI0xo/7cS0ik14+ff5lZquCq9/IWIJeK
J1HU82HamF/q7yMZZNp3RsJXTmdq9FV9/jorOcsg35lC88w4xLBaEYBd+5DTo6dVmQgp/9Kc5dCb
yUOAxN07Y5G+FC6dIdYQetvDLSvgAeyzECxf5Ssb/zkhGzuXScklibFxZu4+01lFfa7CWqNZqWCn
c+W0/uwDjz+kU8CjC/KuQBEJIVFFjbm3KIj8AXuq6tNQ/m8QxFrg2EMLWFGsChV72IAkXE90R7BR
5a/Fipi5gQtcPac0c3UDNJgBtIZKoJP9ZoSubrbhiiUNwHsn4ekNRvO53qm4gAKacmwflw8sDPZt
rd4swa25Y7VaDrEUz4BPfTLdKYpKCB+FR+mYJNPYIF5bZbodWMpRKuQhM1OP5JbpnUF7r/gEjJ+5
naWXFdI7PtIODSDUEZfqPenl/BY+2CalOIjR/UapriW6yl0/ciw45Awls/6bGU+Ru4RqagRRWIkt
qwnQj9XvwKVvi7Y2XeW0YA5R1aeyWd0ZmkKLNUJm4XIsaO9e9kcDP6Cr+GLHkNCR2vblLP3gAVFM
35APpEbbYrU8EXG57WgatSKE9h67qNQ2M/a1m3mtvGJGcO0iT6ELZZIEKEnc4pNTurvBJynS0/cM
ARPjeGMc7BmrjFPJLLjVIWFDJ2azpMhGn+YEgvoBb4Z/1+r/Hlgb2HAARfXB6TigqvEEy4FxUTO0
XWBBYwr50XO2oAccHwR9NLNpYXYuZDW9jtY35AJjDhe3gLvIr/KnRaAV99f7OvvuQ7eGUpQtsvSl
ddWq7Bi90o+EFL/F0O+/bhZ0dJSl8uJVzp/oc0vL6NPcwIgDlFvB6zMQ0BEdqMKswoZdqzVed8bO
sDTixe55DTOwQX4pvrCRZO75qY1AdfS6pPyal8XSxa+um6HDQJ0kHzHTYLiHNN7uNBTAZyPbcktV
+aTEjmVN/6jXPodfFeOyNXmwN+97gvz5kVfgSxrKalcj+BGiyMQQK5jjkVpGEf7kSNyNiIGKdhxS
30kFsid+GF6i8uT7fxRenFlMtpOXX5VMnHSILDbF5F64DI8gLLkU1U8ZFabqPt/bfsDXIYoOYBDA
ghUUjA3bomrjjCJXYCwf4QHBo9ZvnnsJ68qIGVjWwNZO/k4IHLW757x+bPS8QwqVVqwLAyiKkrqX
Y4a+EGAr0YEUKXBosaiCNTPG5rLAAil6qaQURURSXW8H40pG6JoBVS1pUERYq0VDo0urmX2t/Jd5
bXOojzLzSRg52cy+3NA3s7YQySCb+u+zCXXeYhrHrV1y5xFyyaty/xrVdGpCH9Z9t84In8KRFNWI
9Cx78WODLewaRl7+dlcnPb9XHqH47ERAcEbjcuGu02SVFAV3mcY7/eP2S1oXMYYLNsIM2IuhJQA8
ftTeBLVSYrg1t93Yl2f9tdTDhJUkTRsH6Glks4KCwTi7V99wq3jkxA4P0m25BdQL3vzgsWCyj6+j
EE5PYh27BSHbcZ8QGlgoeZMdwUZ6R05+Gx9AKBHUxXAUjiCTgWJmOlzWC8/1/yZ1FDGRJ+fYrvwp
WSZYoMyMby39UC8jpl7PrxTbe3Xvw6IxJJ4dC8TRnFbyfzxwSCg0lvJpYpiqqxAmgX1fcNaPZrr9
YNts9K3/7DQE70qNKulCKPnvnw5EoOZzJcsbED/Bjrq7pdhQQBIzCcfkDcTbBhBaEbTVNOr94M63
iofQzBRat0YqAtopPP3rdFV6ZQ0cXbBIu1CsRg3hBl6Emvrlf2PZZiO6MaUsxiww4bz5lTn+uqas
P0QbftVGbDirGofHIs8Hy3ZdEJ/oEzRDm1TeeqTClSvRMzq/dQLBwx1rukaVgz2Hzxqu5oy5onhT
XrZoGWE/VXiF7XwJWFTOtY5tQr09aWuqM6/ulGq3CE9yKZhR/rg/yCnjZFjWwXMV1ag8B21kbg4b
xWRExnVUFmXAvta3TLb6ipfQ0ZGBxPRzcEdTOoYZ6v2B2l8bQ71gZ8LtDR1+e5MxS6ZMZj6bvVAp
d+P5NVSzt2FF+M5FID3VlkS8FaBO9MJUWHxrE9eEzWCb1FlBCafNF6oCKef4TYMfMxvc9D+8zd0n
GGUrE/V1UEd4E8qw4STnCydLAZI+sZcysePaqQxjksCldr0jyBEbhOa9ZfxsX8e/kUjNTAOD2UOT
a4Xe8S/pm0cCTkcvv4eN6mkkqr72cBXVg0r0GSMpRZXz9O6DA6z8eg/p4Pqi2iRCuz076moA9wZC
VVM9L4Vx7B2iqgeQrW1LN2l9EyYndSFQQGnZuMjeFoWtVhax4W8GUstznlzqCQtz4PU08kqkAozo
nd6XYKOkkwv+Y5QYZhg3p9DbXrDwvU3F+0rTAkiJJJFekTNCSAnONvrAzMJ0YvEcBFw6ciSQSD/t
PxUOmCM6Q0na7QNFespPC/G/mvu6SynRrmTZuXWyL3LkDeHoToGX+6ePs6FA/x99y9CTSawMW8aU
o/gfoAbA+iqFnF5iJRgsCnuUVy14VH3ydtEWhl1sBu9aoLak+AHLzES88SMGVXJ2uVyw+ir2LD6P
CPzNkdjidXarMKTvOFLMUwI4P7a2fkXooT/AjrBY/mvB7rJYNwXa8VIKCer49sW95LQ4T6gJ7P7T
q9WQTobhRoGmzt2byIP4IvcAVl8TaqFN0gLRBVs6J2ERJmLo0vIZ+dy4cv1DDJOQda/X6ebeHuys
ajjEepHcpZBQuaQLKgxhT22cbSmP27ff/u9ChSFuW0X/EfzIl+Kr7X16yKDmXdQ3KN2me9Um0n8t
O9NWY6/8Q7GP1oJtEJqtpLrXEryaD6rQ2p9ve2gb2A+CPRsRB8fObWUIndw5rDo/hT1RwV6WpyQm
j0/NiQ1fqcDXvLC1u9AS3BjvYfXZZcH7/Ei8YkfDGxXq1wTHNCkZa4BwRCY4hzF+yi8IlSpyQ3yH
Ow5jfcAQwiQbNV+sivu4Ks4YUOrduWqNj8ywGLaCiZRjQSm7XdXUaktTAT0sPXhqQt5/CdtjXFsW
2CUnPSuHZUuPACheXlzqFR90YpGn1oAy9My+31EYnSSy+C7OTRSmQ4XMD+5lP+vL+s+AFhQozNG9
3H/3hsG4Cnl4UQWNh0+uucOwFA+rbq3BnNOmXElr4lKp/BOSZQG2LEb2bcHzonCeBy/xI3erya7F
WCRW+eCXRmtm14eBXUEOtPwJRb1dxIVM/RdY7tNx87a0jdMN9pjdEYJ300Izki4l1b8nCjsJqyzq
OpQVf9XvyhTTeddFJ5p3t9158HTrYKUbPXCaFvPtun8PMde2Dv/kgk8qGq7Bd+BoiC2gco8Os5Fk
HMNTg0PMHM2KpvC6/ocH4BTHdxqKlUI8ykxeLbZMeFUkXUlMCYelV1XevPhNd4sT/kf6UKFQZi0+
yMC4qvmaU8i7uWiQvSTKqt/xb4nEkEQiUhrChQQBzPc7z4qUd0Xlo3Gu9sFvfogq268+7b1n+xyc
yTlCd1Dp4zsdn2CaBVNqcJskAIfmKVMiZbp2Vc6HPRKwov2L4z7Am7hEnvkvcoFq7vw1OclHV0bC
IrqJR7AAYCTjcSVa6snM03F/9f4YodqXLwyKLALCOcbG1eUelQei9//Z1m9Fd/Dcx+5LKlBs9HUg
1j7CLd05xKFHKKlvfZH9gIcjRGGsNrWoqozFqKZY4xACy0rgtLx+COrCoSME0o4tMt48NDCchQGx
VGe2+ikbBYVkCGzJXeEC5OJGmqoX9oVnxbMSg1CGAlkYbHGDh7tYgCmakwZ44jJ2FR4mn5GWjaHz
Zf38EsOxofl0s+N80NpkbydUl9LlZ3KfhWWXxh2NpBWvu54CDDYJ/F7M0Bl4jzySHhBnbf3bshPv
hMF0wPLbEBISY4kbf6CXFIo1MaQh2ID9B2yje9m7HZKR9FNy69bjxZh6o5+bltHN+/2MkW7v9c1e
aobDaOXFzpntCssi+9yxtX0pIHTi8GYKvOAxvzCpErhCNAHhnxDW8Q4ySWlpsMBlONybH8f3LQi/
dNiEB15W+29Ba9JRwEKdv87AVXtbaoe2OoU+WZ4KfwQPJNytq5qTZIhfRMLrZNZoyv0wnw/eHOM3
9PT7iV9g4aZfQ6Td3JdA44e0S3FKtsoj09aDDBwJcWtylV92hFjYebdjqDysLzCeqMTSTxCF18kl
hf5GxxVdxuO/GGTc0S9p9gMa+1y9ebW9qI6A4peZyvU4ihtc9KjYNmR2HrWcsgwXcZrQjwtnwgMT
JRMyybh3FUBEwGg2GitwacP0ExdXFTG9vOPBnUPivWvBP8vs+P2KZVuXnLTmr+i75iIgNNt+5wff
EYI/acwI3bNyMPF0Us1JCAdyOVEwkSarmBHaDDSbMRoM9rydjOnkcanC8bXfb/QGxsQGgTzD3xn2
MIVNnsTa8+PO4FNmZ2xwpXraxTAZ3fTbGEbd0Z+LesGvGVqKujXOFto5DcaWhazLZ5dH832PedT8
eC9Xa/IddBuq+cbuBQ/MeB5MQBjBLixQpGvUIjENwHxDYNX5MBbiS94qJG6or8vvmovjLPqs427J
wY7lCy38YHugK+yaxFFdrqlUb7tloDr7YmIZZ1B+h6QFoti+NzjVLleOQuZynklKsfPw6kSePv6u
E5R0FJ15oXbcKOErPhpPJCw9HKL6FMgChAwtQCkB0QOSdN+DJxb/d2vjvKUXIrclQ9NKT17xtC0j
gx62855iG9VoIcGipexFDqgGDIBmkcZAmYRRpHCVUXHgka1XZfCUTNFUfmLR/Nhyuowv4LTdXEf6
NYfw+tA70yN6JlrJaCokrE2dVooKi8iKFeqmf7Jt4KahiwVcizNaGDGIC3s+Ft9GFRjCuA1vAjMx
ZsqQxxKuZXOOhxaFglcZP90awmtwu8Ye3OWZzfePLEhKGuAguG2iNNc+3FYob9BqwNCcQzBCwppT
IOMksUsrkMJh2GNWUk/aKZJVlgI9T5vCeagd/rrZESzzalhUSJrjbZBdYIAiEdqrqi7MrDLSvQbf
5BDeRaPSIYQvEhtwUsqfI8XN78QyREMTRt8XSuoQ+VC4ctQtHH3bnZVhfSvfPzjekkm8CsCrBJMG
BOKwziQYO/BNDfZf9BPS3c/EHjJqBdR9CyHhzJGFILXmSIQWgAQugGcgZw9Ay+LFcI+wJ+85y0BL
eXd1PBKiF2NZxYdh77TPh2IDKmX2DGRgSq1cNdPWxbCZXJyWPeVE74JVGzwAwpMEReHt7H8q4DJV
KqoHnPonS0ZUgtDbYZaDSPNMYR8Qp3GdOn/HEdvrkQhGe5kfnhcwovWNLpxDrQy2GBBdSu2MhZAm
BMAg1XgyFpX4p/RvhWLiakNuUrBDeqQkOwSr3gdjXEslbbaHquwfY18hp7PLcgNeQ06HbJw6hLYJ
bZJEomtJ90syE8uHQvCNjAG68rmCykCLebVPk/bN5krQXmrUqzjOhQ+cfpdZ1YLFJdmg0NgGX4mp
ySo1NE5OTnmIxU6Ru1xoxIzTLtYVPA+ygrXg5607Vals747YObXmLqdHgSN4ruW37CB7h2JlNKWG
WPkSTTLF7KLEjXONmBf5GeG9CRY1cBE1nVTIWyu6Nyc9vAMnHE77epzRRrrKtVj910caKPZ/7k8g
msUAhdCeSmBDjs5YPH2bHn4FHWB8dgJGDzBj3NjXDRwqUKfwx9l9GWPdyRAz+8ScRJBNSZVcK282
JE0xgYc5RyYSpTwN4todDfHiGnBz+v0f/9tR33fi1Y+Q740wU0RKIFahJYRQ65B1t5GPX3hAaELo
DCoxDUi8qhlN3uuihAXubHF4y5yu8KI+hz/+q+fqbK+MUud1JxCyhf4pGEixcjiJKlXQv6+/g8B0
GguurjIQxLgCB23HAI6brJd3iQAgWVJvdXP1Z6AWq6ZVIvcmDApinqKztcUmt4I4C4abps4QCk+/
DTlTXPqaiSkkl04v6RvqIx4TwrZiY+eH/tIs+eLA7w4qaykXpACJ7sfgITJcB8LrWcNXVTWN/7qj
NgqOGWet+mZceVphqQJQnLTmnb19UH36vTNpGucEDGJ4UzPf7+tfFJVMIQe4D8qidIoEThdV2281
AQuxGyFOO5bpju+hzoRSETWhxRnsFM7E5dOs44hVFoCtP+oEMaeSe4+O6y0BJBRqUQIddYVpq7PH
JsuzzeGByuu9XdSKRtXyvvlkiJAYb8NjR+BgK0Eh2FmXj3PvYstKaxrYRoL3s3eMt5OfMCVyCQOr
r/KClYhxbeBSaaoZFUast64pg6ZrkEaxhEut7yaK09IXVZTXGQmaRn42gxpwTXElrFaThot9z+b0
RkKt0CjdU5dbSHpnB3SA5AG7kpoPj+doq1KxvsOkN1Sr8P5F1zn719FPetBtMaI5achF5AXS5/4w
xLWgqwrAqKsgTwFVBQShW0eACvCdHz+oMbkgpuNkavf5TKM3AB0jZ7zd8J1Zm4dHKx5Um1lAp3v6
XA+vTd2HugoPKEtXRPcrLHlHBdIsIKG9Gwv/8fXshD5HxCQoDriIf+hHfA8y1188zwRnh66b4QHH
2SlGm7kU7ukaaFxcGb/AXgY3dUPrg77N0KM0DhadTuImcOvnqIp4CwNp4tR1DXwfviSbolpWbvvb
pC0m9DDQCVYEsctmJObIR3HtJ85X73H36VC4GazWNT/Mx4jkd1s0Ys6JE7M6z9kBDqBObM42Z/8X
jFTkHc1BafWxmSM8DvTLRnTkz5UrJLA4SXQJ4j/5Rshrm46wNxr/U6HtLQ2JS335qJ+WRF0NMzVM
ueG35K47QyUHRAGF3Pa5djKhsQn/zQsj3gUodwPxxCVpOt/PNWpxUkv6RTCS5y1RGEEdjrQBaZw2
IoGxuuAYr+N0GDrpAf4t3owzvPpZYjNAb7lU3AHcXpaNaRh4eaDnKmgBA2O/aup+gS2mP+OqdBPn
LKyJOWISqfD20Acxu9hZRN+11lXdY7YbrFHNExHq5n8TEXCTjcL3zL34KHU5u9AtVtV7ksZ6TuKf
ZlM3X/8kVAvjE1+wD9yg0zOmPbF+hDB4+/7BJU8sQ5JsxYX2YSo+QxLihhyjO4HMbKDMR1LbgYWY
RiIZCwFN5uCn6x/csCiD44vKsnY2cgmK3pcvymir5jI6paDY9LWin48NZ8pbNFUc5hwwnb1V2mHD
UYjpD/pVdiI0DOk9K4njMpLg7siIrKqyjEdHMiipL0fXqG5Qv7rtoKbjfNM+nnx70z7vHAwWFyO9
P0l2VbwkJTWeewFRRdZbHzAWCBK8FnLACNLQYrcNtd+05NRAXJn4hkoG7zZLNEFfs0O+zlrLsMPO
4SwAHSIo+YnzKsJrqdQspNEwQKcCACIBqswKF3a7d1hXXkGQdrcyr/+rCxhc3oEAz0OLjT4LOpJn
1huDbx10cAYDfvrfdH04m7WDmxh5SiSu1+yiaYUDQuUix/6ZxtHHjhrnH/P+dDZltHyKCWYxbDRO
Eu7tp0BbkLPDMwtBx464CzwQfhzB0h+jqpGYurKC9xZB7pEcLxGW3ovLgM7LoSElm+IrSZmoCJvQ
ErIny0USB8JIw4MbEla+4EcF7kc0qiIwR+KBKGXrv7TI9rR4vKgFWueVO6HP+MHynaz+9x2x2vTU
Acr83q8oEWtSM5nVS+d/mkBsr88qB7GvyENLo6p905UYDVGfQDnmxe0yXv7tv72AGp9PvQxSJah7
lbKEQ8AYu6M5ByjRyqvBOarGb1/SknkuHZb1g8guZjiObRVfMQC01GyBBcqAx3RJgLtFCyuO4dBq
B22po3ZXZzmuzDb7lnHJcFfBaQTKMhRePgfdUUkolvTrB9tFyzcmEi3obTZu7Uqxf45rd9LYwFTN
f7RHKKym3w+Sd8DarPQ+yjtzl3MMsGzTEqwqpe7ruAZ8XNn0ilb0vrRzyG6/GeFR6t4yo9d7vERH
3SXuDdHyh/2ouiSuaCEOUzLp/sVOJVIol+U+Y9ucR4isQJjuk8vw94M9s5RpLwIQqNptmnKIE4Rw
Vr4E3FWLEO6W6AIBq6IZWtZ5z6kXaW7o9clDTtmVUWxIWSmHG/jAXKQzGwA4azvA0OpLTR+ukyrH
lyY3Z8KfmEnOO1TVMREUNeNvOtAB9pcu5qM0EJXjyGbw+gZXatw6Xw7UNPQ64XRpbTLAZ/RGdfA5
3BagSWAicLDd066tTg5q4SUCjFl+Shmmy8PSCt6ASdtr82r5xbG4Cz1WSDZm4TrR1NBq28oDuESn
Ppi7rzBcPOdolvsVnAgXbXdZJa1QhJ8JaGHD+Ah38uep62TxpswY+lS3sL+AI9rMgMDwCbuK0FZN
8rwrvmehuoVKmYV1fkbRi4Rfiu83OHv6BcfHDbYbpz0ODwyAc0HhHhJw/qBvao/qNnnYn8qR063h
j+JvAWD0fkdst3mNAso81MoPajlU6UcFu4aONGaLbf9ImHMWZko7VggOUW5R2fjeVx5dpjer5vjv
ybJ/DFaBg6WDmQoxnvC4jr4rUj+M4o2YPcto20awU23Y2r8Bqh3P/sa6RfEYt6nke1mniOA7rYtD
U5co5DLNrYsTFf7PAz1JllmG5gy2DZKviZVG0qM0UEKRgmFVHWDDKcDB5JfqPF2tCKNTdJfg6mkm
E76TJNwbemmk2upN0y8GTT7vTMfiyQINdNHzjE+m7wMmlC67foIz66wVlp+pl2nMBOynaBo1bQFt
dL/i5nl8rlQx8My8tN0Ao0Cvi6E/aYPciM5grMbmsc8nYANaA2LhCmjx531x/UKLPww6bII4P3Rh
enkkcYDpIN/p7d3C6b8doH95RSw+uugqBC7XFSJJFGayQV4QTcifPauILrGR61STQN0ypi44RIBL
yjR+NOIh+YdbMw6E3PsZFTm0kKX0Brk2vVU7rwxoPsg8Iddl3ZDQ9Z/JwBffW5nOSsqskV917n4b
vngOcbchC9j7PgAMiIbLgEDsOr9khSwu8Nl8CjkGb2lCNxlA1sb+axjS/3dc8xJjNCI2hxF9eZg1
6772oT5cQol1LB5089ckL6qeGgdDlXZG5ifcmRph/SETS0DAaxd8Q16cfEE9SxevMo7CYS7cCwFs
LIEeTl9bl507fqDTA5WfikuIxLKITBO4kzBxyOGCESqPJ+NPADZbC9ZL66erLX6t2tcyJLCW7xr+
kKeuU6B2n7LChegTreXkje90AkshCxCeVPRXtFczFUb7nJSOweIDMKcnyqiohjn1WJShkcvjmDij
jNnjej6J8vFWD6ICUkVFbCaZS+pgjRVASgA7X/E8e+56wTk0RLil5qoR6ASFHmxUDFJl6gugndWF
x+OoqCQIZF0SrmK+e47xVpnQmR2ELZt7JhtTr+wFgQMtZTftDM2TXbtXE+EAUvCW1PgXgZFApQDP
XvWZhoqYtKIAMDMD62szsQgRiIRUFtHTrNg9QF+M2+oImrRjITG2CAz22DdcLVssfCbCuXrE1rrP
rFiTziOTj8oUc1rwJgMvKiBHg9RojbGLoIycZBor9ePLssInRErgul3EdyC9eUyEWe0norzbDwEI
MIhed6t7ArFdYZ4FgHNMoNOyFJ2SKeWqbybeYOs2O/rIZMizgq6ZIwvMai6jd3T7vKkAWmFTOAnq
67zab7WCv6PU/+GbKx5nPeNXuV5uMLdfjR8ECgWBW7MD247V/st7oD6KlF2kT7A9Soouy6yxWKBX
EG9598TanUoNmQFGSLm+FIr4N8hBKzhTopuZqRUX4cEDufMyaBCK334Bd7bMQ4RzxS9dZ460ACoI
qICrawF8joCOoCnGZsstyqiS0SyyjWOMq716PscobCAnf5wow7F453Dwsz1mUNR2rC0MCCu4XFwi
tJGx8ZzBQcxPkpZSxb5HzZWQkWxp7XSkLgECsnXprP0uJR6d3BRZnZE/qvfxunsiHrss02FlRbHY
vC/vG2CF46Oc20W8tH+uGzcTL/curzei4mQMNh98ewuXlDXCEZMWjzK6o4UQWZldSm8YYoo+3K9p
2SZh9tOwZdnaL2So/xtKFKk76pQwQK/NEhae8xldtFfTcxxf/T0bgAS+XlCrDdBeGCru3KcteTpD
8HwfmHNxG0IB/sRaq4xB6nO7sYcbZQP3A3G3jyFlTAVGGe7ycg5powRmbpvDuUuUrQM7BrvGrqEO
AIP0Cwho/DHD0hN3296s/vFPlTWaCOmYF/wy7jeRx/2+5ZIyzZ6I4f4z13aMEKtUBoRWGLChu/9c
sABPVuj/16Y0CY7dvZd/bq7Dcu3GBQFr8UXgkLqLAfwbMHk1UBRq3CPzYygiYam0GWN+3bi6eAKX
uS7iHMgCGyWGyqa3D0tBoKDDT8sd2uyisp8FqQLgCaFR6SPk2fnzmU70+LuT5W+UuQdPNSBojx1A
/Ur4Uji8edemf4VbY+0Ep+tX/gIO85UOoYpJbL92MFfLU3ARDs9fIRHg+moXINZcK+Zi9xZvqv0H
JadE0KUwxtoxMRhAehT/8My84MmfNU4RF8S/782yQ6eOlcp6AGQQKVNhm5GKjBCbMvRgMj7v1vLj
BDL3XOWgQsZylp4Afd8XbR0FjjPiwRJBt6PkO7lipFmcDzV10OHfRlFT4kuep0YXMTswonI9OP+o
QHOlVs3O5OOcXvBJyye2LuPMMkpQNLFe4NFn7WUAfqNMwws4NqTPgJDCnKSdmf8ifHIL76434FBo
r02OzfKpgABZUb8m+3ggGClyUHJnTtks138yflUy4thFRZJaw7Lft4D7Pt3j1aVlpwqv8IzeQpOO
0IXdhjCibFkGVubkGPJCvl+gqHF0LgBwVhuDEpiNvjpL0w5Az3F1f2BGbHqbdLzBjM5PUuEYldop
/4nIrSFgaT4IhxfKuIohCmC4mYcdmHMjulyxKfVMKHKwWz9NHSSSBp4g7CJAoAdGjneB7w1jdECn
OgWTu58XPjyN7P42TyI3JeQkcWBPZh7MB16IcjJFQW+FOKxoYJLsBv4LYjgvCjY2Py2wC/Pl4rCk
w/s1leZOIx80aTXhKFodAULs9y1nrM7l+PdEnoos1qtDonMBo2shiXZptEjZX8uTsoT7Sj5Yk7aD
nsYOzW1rZo77/8S/shhziMcAgznl6auskFae7AEUyNxxcJdFCNa0hNQ054eTnpd5VLDQMKLW8oZs
0vFpHEhfVqdkpw5haxvk5g53Szq1wxCAKJxTd2AMCkY0Qh10ht7n0AHdE0I9DjowLN4wckiT04h2
YKwYQ6v8bZPP/4S5jS95lwx+Oqs3dfjlUnN4ghwoqcSC++nvileg3DbYpZmTn/DtAwN6eAK+FdE9
pFY6Ye3fEFIy7QWA/809VfmwTFcE+KQg9bUnth3wMKD+VcKTWN1MNogFrFQgIetGLccutAl+lnm6
WJzh9iJ/jV8grXZbmUXzu3VhJYc2QeFv0gImt/v32Myr8gpl5hjO7KURQ4c1fFtOA/MmcnBgHmbV
r7jYeBVlpZNPwr8ByZz9KcR2Z0Mpn/hoAxWOnnaykwCoxVEFgs6f1/BM6WrjAa6pVcDG9r1Bo/fC
LlL536DMxDKbz4/fPEGPqnmkBtciYBdb2Miipr/ccLZnNMSbupwM8JQx8Tj8iSrlN56P873uxuoL
7DlWSMkC9okOo9fkwzBlXQXmGS5JSlfYWTf3RC6NKacO8Gzz/0xOPf5nzOZuDUj9GyGUQ0u0lZr8
jD44KAvBE3xEzUo3fUVsv22jeuLFGN0fqfWFdcEMKvyIXKrmBetL7EiWof4RHwHH7nXxAwWdSSdb
B0AnANplz2D0jZZhl5N+IR+CLG+3FlHP9Fd470FlEyWDKI/F38CMrxyqR/SOi5XL0fTfwhT+EhAK
s56XdFFDlKyqFAss7SZdx7j2XV2X4v8dgzKqULtXbzJcE4Hi3lnZfp0NDbLwuU71RVS6U3DqJawk
AOlThvBCauoQdnd6HZTzpzSk2UaokocDEpGq78bRLPq73uGrpkRnSn1XWunG8hT4xMME/HmYEvEb
6fxS6kMmRwdp1VDJrvxDhChqflZF3470Jo/95KIlQVj9WcMTmCnKeWqSsl/kVuzObyypGH8/RPOn
7lgCOZbxuXWYmKpgd8kNjL4FYZu8gIN7totdjcmDJtkqyJpxZVXY0QsHzFOaQ2UxnG7oK+FmF3Nx
C4+8SwXckZGGHmae7Mys8D0MADoQEIu2TWXfGlmAr7/qL2Fafh/oNIDAdG2SBd/DmY06gv/vsj+Z
dafaH2fG7u/H2Wf6BLmD46MRVpN7Mx6YQhQ9ObUPAJsuzMmEUgGtEod+0vZwq1UtOua/uqVcCaHQ
7U9IQPR8bSx/M95FeHY5BeR4HRsHanXRTZVJAbQmPdNkv2Q23Xl5GQ9+hgQG6NRC87xVk2STSNwc
JhDCvt2kLwNNjYOV5hLp9TcnZ3JRnoZHiouBqoVIRGECf2TIgjnp+35B4E7sfEawyFX7S4Ebuknp
TAEv3epV7zCY3KYADRynBwnqB92OtRTJIaLQfK9ydiSSC23pbHrd3SI0ESRUnMi4/OJt5To/N9Iz
AgT/+c07d8FOZxWeQcp0hKWaIBLnU+3TgInjN0ZUcQHyevpDFVQ9QfxKKo3ui86b1d+NxTCkZiRh
zsS8Ep66kvQflJq5ZqQuGwSBFUqJqMmtJLDnU1+2QwRUMZL9bs5iCpvYfUXrpI8ZUCj5FAJ65JYV
P3AP5uulTm4SKBGB2CuXPsECJr1usA2gVgC6wKpX7hCqKDsLTvYyvEZ6soXoi3NoXHhfCcXiVDxm
zLJuLCdox58ESb4UNNmM2QhVi1f2PRVPlnT5X2p68DCp14BwY9t6bq0qJk2G3QPK1omocirOPCT8
0Afg0pETUHDJgNpqoDvdc6HfgDXU1IRlB/OyJ1zn2LKhuokegx7ddAabi0w7Cn8wfZZYuHjftHBR
DFfu8sPAAj4sG7r9MqlnnkTEMg/m4au0x49jHUtg8KvjpZMKBjwDQLTZsnVkKei10Q7aLnO+dvqP
e22y+y2W7rezPFlz/0O8DjRJur3vkKrrMBcgW/5ze2lJG2TefiYdP+jKZF96jbXZKMF6N7sreIk1
CTP/FsI1bUDfV8wvoeDRrSPFhzrNKScT1KIhqQ9466za9Pz57keD6Zw5/mGSpmofFnRNVKuWzh+K
e4o0/mW9xQsFJVbHR7e3cOe4R9rW+JCgfb4Rt5MYTwPlkjIGkVHYE8SYaG6EEbiUbZ7t2shvPS+Q
g4B4fuzs+nkbrAXh6O7bLPNAQM6z81OaUVt1rdEtZ+Pqa8B5TRe/Usl+r2//m1XWZBcTBdzw7egk
yZn5+dPkfQDkBKT085rULJE+EGfmTh/umF/FblDrKFVmx9QV3kZ8kuEsNXTipQoYw8ilkJqAYuGq
LzquAHSMLXya0LRxwQtn/ZsIaQm8DUG71mzmX/atKsDIKuxyAIB9W604U+I20w15Y+pNORWmKBBY
mS8ztv4/MqeYioQ2Dfct7wvFw75jdvoC4N1yv45/CX2orM7+HiKlrTDs+DPVbTsTj3MEjNCQSRmC
7/xBGBNOWeoSM191bgUIqY4EYPZL6F7xsKGdKCeY7aEwVJhUMR2c8a3YQyIiprZ0auJz8dGK8MhL
DiDShge7Rca5yrrArj3WXPMkB4WnVDZNJo+goqNYswigu5/pmfeSHILlPCMHgMxBwzrHRtJElDC+
XYW15J3gfojOKk6MQZXiHwWKBobEBGUKye2F1C/Xor1xa8FqKP1M5nPytB5VtNbsXlwt4i5BDgTv
RtCBsRuEeYAoBXPEkl1NNTg3OX7XRzksbiVUWDgbCSE+w8ngJCXFOVF1gspKmcChiaIG+odq1lZR
px4zbLYQnsB3BT9h+kv5uyD8zi0XnJ1EIPmv8QwOW00R0RH6ydfskVtL5+25rL0szbwL5i8zIKn+
7Zs8ZBz/pwRspbUCc/Z7BOmKR8/n2CAEx9IVMdVptwKPPjawDZF1vspcePIQwjqk9rg2k/RpK9aW
+vZ16puPKgchan0goAd6bl63PKh5BWxvBXJaR4gqOjW3DqKp1bxoQFYhwuBOugd+24g8pzM7viDi
BJsmCDz4VHaZ8M/9gL/jnZEXtGDM7OZR+e38jMfze7Qi9IReO92t+bvAbIn/02MhAE1bqjHcw7Dp
MGDrK9HYRwX4wMySoxRD2w/5ZWjaTviGm7WEWWkSp7YlnwMDmzwxwkynjc0MdPQBy8XIZ+DZrDtT
dPHo+YWW5h6YM7unrGGiI2MPf6RnfUVVb1Zwu7KzTFkSt31brwacRtF1NkdNjG3LEM6KF24hof1R
Vdl2YlGcfDSA2AxGS+qw/wrIOWKanjoExiZmn3w4IMYQ7KDcsFIJSX3m+U16iiJMiCMFLh4ly/uD
pqA7klcQj45a6kRM+vpiZvYmQI7fItfp4M3f3en6RnSqbKrJmvlwrhlPV9yB7rrpCGDL4CH6hJ2e
7myWSydmgvlj+mwj27/hZk+hZk2J36raZr9FxVxt7OUDiwHcTGCRKzTS81acRm4ZECtr6WmjfMI2
0H3oX9/DeEyWiS3+gWJQwNzoDt6zg9o1EYyWdlyoCQcsAA0AY/XfazNJGTgQP9416obymy3nkfr6
jJ/4niI+Cs5HEd8KFP9zMRLqA1WQ9c1dtHaU9SxU1WnACWt0ByTWRzLiAF6+QcPxfyJ2IMCK/Bbl
+TBiQ0zyliGQvYdhLENi4do9SrmIadOgh7NWn5hILlBe6vkIQ6PP6yfIti8nONfUDoituS15lMX6
TZUrHXUqtEhy9fQXRyiSzUy4ExQE2/Tp18fpYB2gc7Trs5bnjkkqn/nYprR6/z2jKrkitM7HBOUe
OBzUH2WV5rWfm6QmQLbJQYsns0ZLVYmSK82613qGnyMYdl+wzMCDzaBAa9rpVpHpgVW9bgivjxqw
qslxPIrQDbQBqV15H4F9RUC+IeUsgG41nSS8DhEyvvRcJ69xsIB3ipnvbb8KeU9rUnGZfT4egVmU
RyQ6qYyrq38ZSCWSZV5xIazQsrb+SPQsFdkg56YbXUM/s2UYJzP3vHQVGsq6UoVp6mCPxfm0gUrI
TgoOiQHqKlY3NNEh2TiKUpLNrl0fUdj1vYJvMFT2NvFyBIraQzvyoRxdM8lTJgc7u3rB9ajFgAAA
+bo/eMcJXNHl5bWsym6sZPTFJ1ua+8Y7Lk7QBiHMBz0zY8wgUhvJzMKXaHdnLWFbuvzb5VO/snxQ
uumD098PifMY4PKAloRmsHmBdlMKNxl/XEBVwnDZvZ7/b24I1DcPOCJmwFQ6kSmaPJ1leytnexBu
1JzM5JACM3qqW2L0nUkBB2lHN8sx2og6QWCqXzbmK/Q23VTppfot3dQFf6pE73F/LC8JLZDxCLak
1xsjdho2NAoMLmJ7NjTgeMQrjNV8y8B2gcjK2M78WeQyWgh3UMF95x4N0pF1WMOw4Tpvf9bRCzSb
vpbaNJ1MS1AO7b/oh3okrV8AZXsUZOO8S8b4jiqzZyE2dxZDxXQ0wJyW6FpNnS2Smo2cUnq4wGbR
MxTEinFyOQev5vRrYYSH1yFUhu592E1REFG4SXRSANyyU5ebp60UB+Es+Q9DgRP/nM/K6nRj/ooc
WITIIAdwaM+2uxZBE46LbybRaTpvX+oudtk+vOecEjmG/uV+wbJ3bhxbNzZimL+CSp71TNJVeJge
RiRm60WsPvK9XMHSTV3cx1da3bz/1GMOmSDfBnuFq4ZVC8fHeWuVfcxYex2SFhxyqciK4+/fVTwr
5yVHkIK1B+MI/eVgwu4+dHsU7B5WPV9ZedCGHffFKIOagAYwDuymidmPFs2GESkxaI1pakpRx7ON
s/sVGxRd/ZjT4A9Pz5jMpIkSADzpKPGCXZju8Y4H8kuv9fkbDnHrMc0hVUtyTwlPoPAkw08T7FoX
xIn7FlSHyoCFD6pL7Gmq8+9U6bopJemJtEWXJE+WRkmCB2jI5RoOVQ1KN8x3Mcs4/hQeLHmmAk5E
kfhJtvG1LQbCglteVQiwUhCy1lCgfJHvmRelSgHZlyTGWWZIseZkFwA9CxBRhKCAGVOFB2/Ye+pc
20RuX2cEZAgnL4CyymR/wBfsJn7HnFLMyWqWK4V4GkKK3VEM+EhZNGN/r8JTRd110oIRNY06SiVb
zELz8y6jfZ9c0izr7F/+NBlhlFpluzGr1sjn1qYw1G0vNb8e+oibiDcqC8uVLRmBJJNgCMWbDUxz
ymhDrhvl4w8YYHIOniojbauh/NSvI4dAP+itY3F8mMPjbTBM1xJ5OWI/YhsJVcOiaBTPZEyX5w5W
+lnBN1b/AiAGYSx0ZQvHTxfuMecO4RiPGGqgUS5SjDH2p5ui1vmxtXkZRhS3N58unoJkwEp8yCcf
pQNi4w4OvFgLoU9hH5hfj3j6Pprsdlk6mQ1FeU3wYAS9b0WFZIY+G/hvJjGm9ZowWa/6j1sGPUMm
A6Uc1QB63MMU9ZgZ5FxY7FzU+iaz83JfJ+j6Z858A4c0luNd5kGO/BcXSe3lR6FeXqFISp27p2o6
tq+Uu8ArRqFHcYqJIRsXpFCuEn4NCFHaBSc807aHjsUYW/bmq2K8hqU9Wozo9s3FWxwf3RDaT6fU
N6B5GXa8ogurTZ+he+34RDPsQ/1Wh5A/0ah+HtUCiXK8m8JtB08ZvSUkBnc+n/hZinPLCJ56rh+h
UgIJmIFXJ9cQuEiUlEmEFad4zSaw/+LVLcvzerz3j1+Wi+fApoj48i+05rLW/n4HzZbdaqxhaVmP
R1cXyCy5ehwPvQEeBWQtEtZLUkayMWlF2uG+IS+SgI8gQLbIUFRns8++zNhS/RdQteirWG/YuLLM
Ln1s36AGZm4IrydCcXckldaf/DfIv0KdEMx/pkxsoOrrzI77VeDq7mc4TlRQf/a+uzmckXMvsUKx
eTVszbszl+Ht6YhZH+tPUg9UIHuEEismc2R7enKRNSyZ+C/r5g4hQUfPERBhJ6jQ7oOFEbCR3LP6
BSIN1oqZNdaTS71g9dRl4t6cG7ERjtX1A+N4xTbIfAl3c2ykzFy3hppE1KajpXHJ+EBXbHBJgKLN
imxmVafDTrVC65HSucL72xZ5xDLocadbXVdwAWqIF00+S+1klJlbfJyeslzdK9BnFF5xGCpoaLof
0r0PH8DAWDyKgqjdEYDsF57VhZxo1zbLSddzObwJQfbj/7wbfjOtuaF/r8nFt93PoZliTYwRHUjf
KS6NooWDUmcCNtvAQA9h/Wi7C8Q2dUgKZ0E5GNuUl970J5dtFIqYVnZD7IDwvcrSJkL2vWoz71sG
mRj4sgYQQj365bMTCBGa8ZEQTi9Jrm/SWqP4FuTfS+Cr5eaVXOCxh52LLtcS2TgvWZ7ZjFu5/3Ih
wHhmQlZcMFQv0jT8HEZGVQ7iJhSEJjGgz1R5bywmNvB6PGJN9ga7rCkocka5epO44s4b2pdIbI9E
YscNBspwc9x47gxxxglT/Yz122mPUhtikcd4h2qsTgz88vABaltqcTXYZ2H0Exj7aBf5GPwISMNv
2Q4ckmGPmEXeZ/CCeh8yHqxy+9AC0Mt+tf1nSX9tRqXh1MParVCQdS1gKEPEP5NL8xwANuPgTKOp
Os6H6bU8A3NBJuleZpLoJwMKvcTUuIXg3v/acM5haCU0T5F01V1UfI4qCOfOcbEaLJTNqiEO0z2h
IBDoSMEqOOHdsfebrI6HGXUd3wiSZF95Yj+vD2BrGv+59gQVS4PFI9eeDdS46jYjQDrUb5eSOy8S
O5GPwX4JBlHZIMQAj0mcPo0XmRuVtSEWBYdbTmA3Tm/Fv/S/4LLOK/JV4267vRu+SQP5/gxWN/pC
Ujv3K9M8+xxyg1WgdZZ4ieIGWzLbpvG9oNYtcosSUR3px6USFHRyxZ5e5OuA+86niyXazy8yCu7q
6CNMNlvsmDmr4HYRLyx8Ge2hRdjUX0+AleH97/aCwWkIzi74GaWqAApMx8US2QxEcku/a3FiLn8b
YjPoNlDfdeQM/0zhmFYrCLe+um8fn/0BnFqw+4cDGaTd4riqti+I5kQX2xyhBOSXXXZC90ZJLlbj
uPAoszZHJ1AF/us0YO1GKb/fZ+cTh+uTdlJKcFJGrWC1Dgq8Zr/7vq8rBwujT84UIIbRjWMyQ3c6
6HJmpXzSO9qpYXm/OWD4dldCQ003vCUXenH9b+5mGqghRJ64PYZz52iWvuqCGbyV+GY889Jv8cSn
iLBL68vzWXkZxIh5xXMA/BSmacy5BvU/ILNnGyzz2Oka+rv4RHJDgRaTUcgxrkMg8MVyfMkaZo8e
gGEDp2PMW6otnGGh2Dvr0ilxXOlCXoX5G+WSv1l7hu+R/dhuTf0NR5Lr9MKP0sKt1UMQvudRRNM6
Ypwhk99B4gzn8Kt5r4Opui/S0+tmCJcbU7S1sruETRBgtpWNGl41LSu/4Cdh6vTdZ0Yrpsin6JXq
TQMh9ICWHhYZ2ISW/KRHdrOgPQbKDF24+D+tniuYQYo9hvD4Qz+bqmVMAZRdWAAPxvv/o/g0B2G9
GOpqZiEwjJ36l2YdVOE5YTc972K3R5oHDUyilcuSsLwmgqCnyn13DQEPfAgDdjHj9tRDmdVK7MLR
XLIvbuChrwTybjMeI8XEgBDvTBjptnR8YlH7pUfQVNVhHKwHsb9/JxLWN1EZY/8TIPZcFmk4fkiD
cBpbpVVr5DGcTDzTG3u1SodyYP6r84Kg86xPNyxeSzPkYcEYap4OUVmxkZaeJ63VF4fsU3Rtu8av
cywzrb0hAaxTaVxn+svkQJiK32CDAv1338hPiTC5cySThVumFv3e2GyWC0FGcJ4SxVO0sBYnCwrX
Os022Tv1dOA4bhsSsXCilnTXKVRu8v5LuK+Gx0uyOKsdqE9LERZ5qKilN9qlQfqRpJd5AfcGnlFh
S4OXU4aihtGl4C4q/oeFx8DjOGkoRzhIWrE1KP/Yl1cHj4dMGBKA7I4rzRTKrC3r7dt4Dg+VN9+T
vIqgJFfQLzRgmn4HIWxMsIdyFxhSMRrcP0YdIxZKcH4Ucg0Ft0nuX/kb8Jw6YU0huUQCbrTX+5So
pB98eWL+ZQYRIKRmhsTC9U933l1L/kxEVtbTg5b1LmJK2d6Mvv5b+1Q9vXuKTj0e6aVKjaF0IVVu
UA5W97coUaDqq14vP6eZQPc0/oI5FwZUSMXPsvm3k3GVxbv0waZiF/kejhdzsPNMdSnKmv06MK25
obJVltoqmbA91/7BlzV+fKgLXAZbHqLFDD9eXvWEQQPJ20oXfdOZXGnHNFeH4aWOFoSzoX8wSoRf
BqYlFGiODzS4cCr2nXLA9ArRt85Aoq5/jTPA+L5A6aMPKBahofv59HtS0V1psbSPIeU4VXY9MZwc
FlMdGvZrN35wpzdal1Cm3/9W3WP0SL4Be7uwql6tcMLWGNkdvbWOOFaykzHtZtLRYOIgxsVWOIxC
cp3Jc/tGBkfgRPhpZa26B94PjptVeSaEoibBFNPeN68qxc3sGVr+G5NO7mLVM68WkM56d3a92dAp
5rhz77yIr15AtPTieDN2u3gLHJ0Bq2NVv+weXOzcLGnLggD3T6gqXPH/qvDsa2xHl6uL34cIJMhg
ZlkZUU7+Sx/vZNOUclcPaqZYDYPeWc/lSC4/0S4alpWBtOyvNYmsPhhiBzWNGssw9a+B5w7iH06W
kR5uhqlm+ZVHFIh7VtxIcmqFJZ3lhkLMMT/3nVGFr8aAB6uE1I/G4la8X8WcFhBExjXURlChkw7I
yYkNUevCX6zHf5GaF6tPQEuD5GDgOK8SuROvJwS6ubwET4QA/0PUbJ/k8yj+X27WYACwxZm0anGg
HDrJablkwQ2Mfhrr8cwUDRW6o3r8ct9Vl8lALe30Cz3sEDqW9J7GLh28c5unhkEWJ2dPqvrOnATA
+bALbyL/L7yt968o0bnxTjH0Xtke70+SSg90VElOwWDvc47qJUG93osOgLLT/cLGqR/DZOoDVD99
fqlo7JsRRKkO0025XhHXcz1VOfopRIM08lfB/2E7oufuBJ+c0uISpx4Ef0hR0ffzccGLduZZKG+y
YZ9gcy8cLBmGUHsMfLmJZJW8/b6WELd55d/MnQaiZaUk7MfZez445HY1/x/d0h8IF2ten3qyQiKH
//JpJBunFjWaQTssu9bvcs1UGISosU4Ny68XGvRx6BKVPIYEpIZXfl2XwQfLhTw/5SC74G6moppg
mRTIfHfSdItoVEFVtW9RFYDP4b6/3wQySQ/BdfeMfhYkLUEtn34Z+/tghcDVIJPnYRCupLMBnSr4
hY+sxPatGKXI5JSMz7CyEU52rrITv/oelVtppOxAZMgVov8qpCxMmbJ+E7JEdh9LMNJ4TSgzb6HC
EpnjuE5Z3zm781zt5viCXE6V4OOKA4g+Wfcsm1yashpRWB10gVwMSL8+d0FQGbtk4rFayVZIDzwF
/Ewjxi/VzN2TpJEfmzelN3jmWO1QeCPzQROOC7dILkoH4mIEVxUTgeT+wsvwx+SL4AcPvL5Hm/9v
esjnA3fUqkP2egVUSkbUu43J3EvwKcEl2WaGC4YqH/8zMkWgYSWcxTgeQ9oFn/vkYrhTM6ID7V2W
b5oxusvua3BDRzPr60dNcfz6KcPnqX0hSvbv4+9+ESTupejwNyBtUSVEAYX40vXv8T/CWovIn0Zq
7E6zLxyeKpihSzAG18QrIIfDeNU/urVoMcbMAD+eMRoQeQoC/AfYjAoR0maCvlEdOhKkePP7n9Jf
IDb9RyELQwYtekpEH8qOvoqo1Y5D/BNMHf/MI3d1bIKfKDQjpHjEPiAyQd/dewBIasjuIPypte+p
JOl4JAtP1KPe1/HPiOs9OTpW8GmabqODwV8a76BFIQCEkWQ3tqq1GixxVAFZy4YScQsTl9hK0EKP
k7TbXm4RNSAsEUlDgv6A0dAZ3mw4ICE+9lrN8G7JODhF747s/6wBVYuqtBd5Dy67eKtCERKkNnJw
4paZ8ykmDmdLdMhF8Qyakb+EhzMovFn50v4JgGCLKi8sydqRkv9B0XjxiojbdwhzDKOCc3Dv40bz
ZPK609oGZY5qwKC/H2sLElYHSkMtH+I427fEfiuZkZ1ICy0lvL7yI+t4K1cnGfT0vR5DhT6FGq0b
wAGikfyFPBMSqL9XzQIoidKlySnEMeMV4GF352fgtztGw0JhVTHyC3DYs23xtBhT8xBv21dne5St
LKyFGbuU2sTi0Esex+RhTBJjFOTY86JhJBDwwfMJiry4n9eLUdpOlBn2Di5iNhC+fbMxfL3yDV0y
ip7pBL/tj+5JsgTLC0rSTGJBvFOLoPgrjNlXixWdkaVxuh0Fd1F0rOPDQ/0eh919M8TbzjO7PlIp
zuouLvemDlblaMdibfXqWQESQUP46HfpYR5vgNaiAakKY+tImDEHXvSq4ThuR1zzjTOD7IQmAJ/n
jC4WuP2rAH/OB3+Fanffo0FFOHAPT4MVKDhYZXiAeBEgMgisRGYrOYC1rDSc0iGWXG0zSDuoHzqV
Zk8xnvJOlt/RrT2Bk/Vip6ztAUKUQqt7NNz4p8NnFB0qN855lyIBUX/hvVmIlKllM/pFYKltcMXH
1K5X4/rqXtLzmk85R2iSpRdKowvrKg1m5nb27zz6+Fbti5zM+lppEuDKvxZyDnF57FecEhQeQ6V+
IUVKWkWMfMZj6nuCXUt8Pr/5UjPU5qg1vv+KItEWA8erdzmCQ6IfZvJoVRdeWO4HtrL2GasMh6Yd
T6OabK/Vj+96AipK2U3Tfdn1I22/sq1saYN9ot7lBub42dT+TNyouKYyH2KfqKquNn8XuBgRhNfO
oujolV+xYpJU2ooFdT9rJ92QY+kWZOAjPxIAhfHayU9eYYwiJKC8ef4bE36SD4STngq7TNXbSPAw
FsDiArH6HI2En7Xuo+aKjgbI9JubhsDluSR4zqrqscPtb1VWkmbTt2biQUwq0jlKmeSyQcnLzyQF
47HQgI0PFcDYap+vzh/LZU/M50lR5xPE5B9uzj8AQXCCw3dMZMMZW58TyNar6eOV3bjREjxATOOe
r4FuYdNvGqTc8JfDGIcuHDzEze/uyqFCsmtG3eaaUdc+iHDpHpVz2/dIdQPGtEdrZfuOlSMjqjQt
SFxPz5fz6FC6dQ5qV1wg6P580/OEFFHuGdRlTxiza94JxPh8DSLXS5beWsQwOPvGx63a33JAZeXl
e7iYsUotW8eeU5z6ND9Wr0megAQvHv8ggJ4a+Vw/kIXzmXihCOCsMAuo+BbYpu1QD6pK4nOWGWnm
+lQaJZ4BsrUIxnaNl/KNkycl80eZPYVD+xsyfdkbLqd/MmvAzxSqkMkcSYKPAdT/M4Ivsl45sGSM
kyhKzMRGLsPn0safqjCTEV4Hc6FIqBjNrUm0rPP8rw6HIYpG4OOHvWzEVR1e7llUuiuXPX2WS38I
AleWUuZHoZAHXGStY1mDVbOdIiyGrfCvwoFf2abAFUmn4BuCrW41RLATCzTZIYGDb0PH14nPmm9s
MjhOeuT+/lBWHLS2csR6QYWYMXjy6ilPNxdK5uTt/kX8DO5YJDLWnR2F1rEfsworPvWDCxsqUEm+
HrUVqSzALasdTATJPhAD3iqwXowrI7+/osMdf6uyOzkpwySIbBxOh4tQRUogS+/0rr/Q380ZswyK
ZGR8qZUmYM02wU6UUXpo8EMn4TBf9Q3QEOU/LyxHoJ9F9Vvf1r0wIX7UggCVVShLkWLT2u2Kwevg
/TRg36lw8Ytbiq2upaA+nuQ3XXWKukcjYMXcV+ton2m4HT2UB9pXhJcz7Tf0QLJSwZLj5Ondddjn
L5bjBjE4s+L2R5kWGv3q4sxpJkBRhXaVrZ/h9ls+SoF3f7rUsKx3x54d37dxL15igjXIq6kbU+Wt
+ESvNjD9P4jYCeQPpmA0Z1GJMm4pxgxV8jhtiDpgYAry36pPblEsSmVwF4YhwWYw4uRB8geiE4sR
mPHASWYjAGrFxxmuBT4BdsgdGeRuCuB+5kw99SeCJzuPNMnNjtemWALOO1tZp20an2lyxORUAV7c
Orq1+uKHD/0gOVNY/elD821oZ9Xw4Al7O/51jrHU9qGKWu+t2bEZJKCJ6hyRcFnLOAbTQAB2kv+Q
LUktQNx+F4O48p30Pv6WWuMvwO4bYvOvt7pUCa7CsIMZZ5w3FFpKuPbxeujH9oldIrBKxBNhXHUX
8hRQp7vtR41Y8I4dWCw2LQ28v7m9TeuiF1GL0RgTU68AVtl+RfTPa/tkC2XGpIge5QNco6Yc6cjV
U4pWDGFevB31Lr4v6HBmLGwzSXCoOOwkuYi+dz6cuH2gSggWzkpaTL2jNCcn7Lr+LCfZbfFUfziw
DgADV6E3WXBsziCxxkkl1JuCsy3IaEYyPfsDhURZwZ2SBC6E1b/OgPc+lEf/5BQO2Q==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_GTWIZARD is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    TXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    RXBUFSTATUS : out STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    reset_out : in STD_LOGIC;
    reset : in STD_LOGIC;
    gtxe2_i_4 : in STD_LOGIC;
    TXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    RXPD : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gtxe2_i_5 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_6 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    gtxe2_i_7 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    gtxe2_i_8 : in STD_LOGIC;
    gtxe2_i_9 : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    data_out : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_GTWIZARD;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_GTWIZARD is
begin
U0: entity work.gig_ethernet_pcs_pma_0_GTWIZARD_init
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(15 downto 0) => Q(15 downto 0),
      RXBUFSTATUS(0) => RXBUFSTATUS(0),
      RXPD(0) => RXPD(0),
      TXBUFSTATUS(0) => TXBUFSTATUS(0),
      TXPD(0) => TXPD(0),
      data_in => data_in,
      data_out => data_out,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => gtxe2_i(15 downto 0),
      gtxe2_i_0(1 downto 0) => gtxe2_i_0(1 downto 0),
      gtxe2_i_1(1 downto 0) => gtxe2_i_1(1 downto 0),
      gtxe2_i_2(1 downto 0) => gtxe2_i_2(1 downto 0),
      gtxe2_i_3(1 downto 0) => gtxe2_i_3(1 downto 0),
      gtxe2_i_4 => gtxe2_i_4,
      gtxe2_i_5(1 downto 0) => gtxe2_i_5(1 downto 0),
      gtxe2_i_6(1 downto 0) => gtxe2_i_6(1 downto 0),
      gtxe2_i_7(1 downto 0) => gtxe2_i_7(1 downto 0),
      gtxe2_i_8 => gtxe2_i_8,
      gtxe2_i_9 => gtxe2_i_9,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => reset_out,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_transceiver is
  port (
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rxchariscomma : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxcharisk : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxdisperr : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxnotintable : out STD_LOGIC_VECTOR ( 0 to 0 );
    rxbufstatus : out STD_LOGIC_VECTOR ( 0 to 0 );
    txbuferr : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    data_in : out STD_LOGIC;
    rx_fsm_reset_done_int_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \rxdata_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    powerdown : in STD_LOGIC;
    reset_sync5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    txchardispval_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    txcharisk_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    status_vector : in STD_LOGIC_VECTOR ( 0 to 0 );
    enablealign : in STD_LOGIC;
    data_sync_reg1 : in STD_LOGIC;
    \txdata_reg_reg[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end gig_ethernet_pcs_pma_0_transceiver;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_transceiver is
  signal data_valid_reg2 : STD_LOGIC;
  signal encommaalign_int : STD_LOGIC;
  signal gtwizard_inst_n_6 : STD_LOGIC;
  signal gtwizard_inst_n_7 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal rxchariscomma_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxchariscomma_i_1_n_0 : STD_LOGIC;
  signal rxchariscomma_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxchariscomma_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxcharisk_i_1_n_0 : STD_LOGIC;
  signal rxcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxcharisk_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxclkcorcnt_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[2]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[3]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[4]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[5]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[6]_i_1_n_0\ : STD_LOGIC;
  signal \rxdata[7]_i_1_n_0\ : STD_LOGIC;
  signal rxdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdata_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal rxdisperr_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdisperr_i_1_n_0 : STD_LOGIC;
  signal rxdisperr_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxdisperr_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxnotintable_i_1_n_0 : STD_LOGIC;
  signal rxnotintable_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \rxnotintable_reg__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxpowerdown : STD_LOGIC;
  signal rxpowerdown_double : STD_LOGIC;
  signal \rxpowerdown_reg__0\ : STD_LOGIC;
  signal rxreset_int : STD_LOGIC;
  signal toggle : STD_LOGIC;
  signal toggle_i_1_n_0 : STD_LOGIC;
  signal toggle_rx : STD_LOGIC;
  signal toggle_rx_i_1_n_0 : STD_LOGIC;
  signal txbufstatus_reg : STD_LOGIC_VECTOR ( 1 to 1 );
  signal txchardispmode_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispmode_reg : STD_LOGIC;
  signal txchardispval_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txchardispval_reg : STD_LOGIC;
  signal txcharisk_double : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_int : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal txcharisk_reg : STD_LOGIC;
  signal txdata_double : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_int : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal txdata_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal txpowerdown : STD_LOGIC;
  signal txpowerdown_double : STD_LOGIC;
  signal \txpowerdown_reg__0\ : STD_LOGIC;
  signal txreset_int : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of rxchariscomma_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of rxcharisk_i_1 : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \rxdata[0]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[1]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \rxdata[2]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[3]_i_1\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \rxdata[4]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[5]_i_1\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \rxdata[6]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \rxdata[7]_i_1\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of rxdisperr_i_1 : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of rxnotintable_i_1 : label is "soft_lutpair79";
begin
gtwizard_inst: entity work.gig_ethernet_pcs_pma_0_GTWIZARD
     port map (
      D(1 downto 0) => rxclkcorcnt_int(1 downto 0),
      Q(15 downto 0) => txdata_int(15 downto 0),
      RXBUFSTATUS(0) => gtwizard_inst_n_7,
      RXPD(0) => rxpowerdown,
      TXBUFSTATUS(0) => gtwizard_inst_n_6,
      TXPD(0) => txpowerdown,
      data_in => data_in,
      data_out => data_valid_reg2,
      data_sync_reg1 => data_sync_reg1,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i(15 downto 0) => rxdata_int(15 downto 0),
      gtxe2_i_0(1 downto 0) => rxchariscomma_int(1 downto 0),
      gtxe2_i_1(1 downto 0) => rxcharisk_int(1 downto 0),
      gtxe2_i_2(1 downto 0) => rxdisperr_int(1 downto 0),
      gtxe2_i_3(1 downto 0) => rxnotintable_int(1 downto 0),
      gtxe2_i_4 => gtxe2_i,
      gtxe2_i_5(1 downto 0) => txchardispmode_int(1 downto 0),
      gtxe2_i_6(1 downto 0) => txchardispval_int(1 downto 0),
      gtxe2_i_7(1 downto 0) => txcharisk_int(1 downto 0),
      gtxe2_i_8 => rxreset_int,
      gtxe2_i_9 => txreset_int,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      reset => reset,
      reset_out => encommaalign_int,
      rx_fsm_reset_done_int_reg => rx_fsm_reset_done_int_reg,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
reclock_encommaalign: entity work.gig_ethernet_pcs_pma_0_reset_sync
     port map (
      CLK => CLK,
      enablealign => enablealign,
      reset_out => encommaalign_int
    );
reclock_rxreset: entity work.gig_ethernet_pcs_pma_0_reset_sync_1
     port map (
      SR(0) => SR(0),
      independent_clock_bufg => independent_clock_bufg,
      reset_out => rxreset_int
    );
reclock_txreset: entity work.gig_ethernet_pcs_pma_0_reset_sync_2
     port map (
      independent_clock_bufg => independent_clock_bufg,
      reset_out => txreset_int,
      reset_sync5_0(0) => reset_sync5(0)
    );
reset_wtd_timer: entity work.gig_ethernet_pcs_pma_0_reset_wtd_timer
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      reset => reset
    );
rxbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => p_0_in,
      Q => rxbufstatus(0),
      R => '0'
    );
\rxbufstatus_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_7,
      Q => p_0_in,
      R => '0'
    );
\rxchariscomma_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(0),
      Q => rxchariscomma_double(0),
      R => SR(0)
    );
\rxchariscomma_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxchariscomma_reg__0\(1),
      Q => rxchariscomma_double(1),
      R => SR(0)
    );
rxchariscomma_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxchariscomma_double(1),
      I1 => toggle_rx,
      I2 => rxchariscomma_double(0),
      O => rxchariscomma_i_1_n_0
    );
rxchariscomma_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxchariscomma_i_1_n_0,
      Q => rxchariscomma(0),
      R => SR(0)
    );
\rxchariscomma_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(0),
      Q => \rxchariscomma_reg__0\(0),
      R => '0'
    );
\rxchariscomma_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxchariscomma_int(1),
      Q => \rxchariscomma_reg__0\(1),
      R => '0'
    );
\rxcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(0),
      Q => rxcharisk_double(0),
      R => SR(0)
    );
\rxcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxcharisk_reg__0\(1),
      Q => rxcharisk_double(1),
      R => SR(0)
    );
rxcharisk_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxcharisk_double(1),
      I1 => toggle_rx,
      I2 => rxcharisk_double(0),
      O => rxcharisk_i_1_n_0
    );
rxcharisk_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxcharisk_i_1_n_0,
      Q => rxcharisk(0),
      R => SR(0)
    );
\rxcharisk_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(0),
      Q => \rxcharisk_reg__0\(0),
      R => '0'
    );
\rxcharisk_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxcharisk_int(1),
      Q => \rxcharisk_reg__0\(1),
      R => '0'
    );
\rxclkcorcnt_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(0),
      Q => rxclkcorcnt_double(0),
      R => SR(0)
    );
\rxclkcorcnt_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxclkcorcnt_reg(1),
      Q => rxclkcorcnt_double(1),
      R => SR(0)
    );
\rxclkcorcnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(0),
      Q => Q(0),
      R => SR(0)
    );
\rxclkcorcnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxclkcorcnt_double(1),
      Q => Q(1),
      R => SR(0)
    );
\rxclkcorcnt_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(0),
      Q => rxclkcorcnt_reg(0),
      R => '0'
    );
\rxclkcorcnt_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxclkcorcnt_int(1),
      Q => rxclkcorcnt_reg(1),
      R => '0'
    );
\rxdata[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(8),
      I1 => toggle_rx,
      I2 => rxdata_double(0),
      O => \rxdata[0]_i_1_n_0\
    );
\rxdata[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(9),
      I1 => toggle_rx,
      I2 => rxdata_double(1),
      O => \rxdata[1]_i_1_n_0\
    );
\rxdata[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(10),
      I1 => toggle_rx,
      I2 => rxdata_double(2),
      O => \rxdata[2]_i_1_n_0\
    );
\rxdata[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(11),
      I1 => toggle_rx,
      I2 => rxdata_double(3),
      O => \rxdata[3]_i_1_n_0\
    );
\rxdata[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(12),
      I1 => toggle_rx,
      I2 => rxdata_double(4),
      O => \rxdata[4]_i_1_n_0\
    );
\rxdata[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(13),
      I1 => toggle_rx,
      I2 => rxdata_double(5),
      O => \rxdata[5]_i_1_n_0\
    );
\rxdata[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(14),
      I1 => toggle_rx,
      I2 => rxdata_double(6),
      O => \rxdata[6]_i_1_n_0\
    );
\rxdata[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdata_double(15),
      I1 => toggle_rx,
      I2 => rxdata_double(7),
      O => \rxdata[7]_i_1_n_0\
    );
\rxdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(0),
      Q => rxdata_double(0),
      R => SR(0)
    );
\rxdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(10),
      Q => rxdata_double(10),
      R => SR(0)
    );
\rxdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(11),
      Q => rxdata_double(11),
      R => SR(0)
    );
\rxdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(12),
      Q => rxdata_double(12),
      R => SR(0)
    );
\rxdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(13),
      Q => rxdata_double(13),
      R => SR(0)
    );
\rxdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(14),
      Q => rxdata_double(14),
      R => SR(0)
    );
\rxdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(15),
      Q => rxdata_double(15),
      R => SR(0)
    );
\rxdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(1),
      Q => rxdata_double(1),
      R => SR(0)
    );
\rxdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(2),
      Q => rxdata_double(2),
      R => SR(0)
    );
\rxdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(3),
      Q => rxdata_double(3),
      R => SR(0)
    );
\rxdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(4),
      Q => rxdata_double(4),
      R => SR(0)
    );
\rxdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(5),
      Q => rxdata_double(5),
      R => SR(0)
    );
\rxdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(6),
      Q => rxdata_double(6),
      R => SR(0)
    );
\rxdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(7),
      Q => rxdata_double(7),
      R => SR(0)
    );
\rxdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(8),
      Q => rxdata_double(8),
      R => SR(0)
    );
\rxdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => rxdata_reg(9),
      Q => rxdata_double(9),
      R => SR(0)
    );
\rxdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[0]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(0),
      R => SR(0)
    );
\rxdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[1]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(1),
      R => SR(0)
    );
\rxdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[2]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(2),
      R => SR(0)
    );
\rxdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[3]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(3),
      R => SR(0)
    );
\rxdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[4]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(4),
      R => SR(0)
    );
\rxdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[5]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(5),
      R => SR(0)
    );
\rxdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[6]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(6),
      R => SR(0)
    );
\rxdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \rxdata[7]_i_1_n_0\,
      Q => \rxdata_reg[7]_0\(7),
      R => SR(0)
    );
\rxdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(0),
      Q => rxdata_reg(0),
      R => '0'
    );
\rxdata_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(10),
      Q => rxdata_reg(10),
      R => '0'
    );
\rxdata_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(11),
      Q => rxdata_reg(11),
      R => '0'
    );
\rxdata_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(12),
      Q => rxdata_reg(12),
      R => '0'
    );
\rxdata_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(13),
      Q => rxdata_reg(13),
      R => '0'
    );
\rxdata_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(14),
      Q => rxdata_reg(14),
      R => '0'
    );
\rxdata_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(15),
      Q => rxdata_reg(15),
      R => '0'
    );
\rxdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(1),
      Q => rxdata_reg(1),
      R => '0'
    );
\rxdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(2),
      Q => rxdata_reg(2),
      R => '0'
    );
\rxdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(3),
      Q => rxdata_reg(3),
      R => '0'
    );
\rxdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(4),
      Q => rxdata_reg(4),
      R => '0'
    );
\rxdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(5),
      Q => rxdata_reg(5),
      R => '0'
    );
\rxdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(6),
      Q => rxdata_reg(6),
      R => '0'
    );
\rxdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(7),
      Q => rxdata_reg(7),
      R => '0'
    );
\rxdata_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(8),
      Q => rxdata_reg(8),
      R => '0'
    );
\rxdata_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdata_int(9),
      Q => rxdata_reg(9),
      R => '0'
    );
\rxdisperr_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(0),
      Q => rxdisperr_double(0),
      R => SR(0)
    );
\rxdisperr_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxdisperr_reg__0\(1),
      Q => rxdisperr_double(1),
      R => SR(0)
    );
rxdisperr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxdisperr_double(1),
      I1 => toggle_rx,
      I2 => rxdisperr_double(0),
      O => rxdisperr_i_1_n_0
    );
rxdisperr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxdisperr_i_1_n_0,
      Q => rxdisperr(0),
      R => SR(0)
    );
\rxdisperr_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(0),
      Q => \rxdisperr_reg__0\(0),
      R => '0'
    );
\rxdisperr_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxdisperr_int(1),
      Q => \rxdisperr_reg__0\(1),
      R => '0'
    );
\rxnotintable_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(0),
      Q => rxnotintable_double(0),
      R => SR(0)
    );
\rxnotintable_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxnotintable_reg__0\(1),
      Q => rxnotintable_double(1),
      R => SR(0)
    );
rxnotintable_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => rxnotintable_double(1),
      I1 => toggle_rx,
      I2 => rxnotintable_double(0),
      O => rxnotintable_i_1_n_0
    );
rxnotintable_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => rxnotintable_i_1_n_0,
      Q => rxnotintable(0),
      R => SR(0)
    );
\rxnotintable_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(0),
      Q => \rxnotintable_reg__0\(0),
      R => '0'
    );
\rxnotintable_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => rxnotintable_int(1),
      Q => \rxnotintable_reg__0\(1),
      R => '0'
    );
rxpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => toggle_rx,
      D => \rxpowerdown_reg__0\,
      Q => rxpowerdown_double,
      R => SR(0)
    );
rxpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => rxpowerdown_double,
      Q => rxpowerdown,
      R => '0'
    );
rxpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \rxpowerdown_reg__0\,
      R => SR(0)
    );
sync_block_data_valid: entity work.gig_ethernet_pcs_pma_0_sync_block_3
     port map (
      data_out => data_valid_reg2,
      independent_clock_bufg => independent_clock_bufg,
      status_vector(0) => status_vector(0)
    );
toggle_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle,
      O => toggle_i_1_n_0
    );
toggle_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_i_1_n_0,
      Q => toggle,
      R => reset_sync5(0)
    );
toggle_rx_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => toggle_rx,
      O => toggle_rx_i_1_n_0
    );
toggle_rx_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => toggle_rx_i_1_n_0,
      Q => toggle_rx,
      R => SR(0)
    );
txbuferr_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txbufstatus_reg(1),
      Q => txbuferr,
      R => '0'
    );
\txbufstatus_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => gtwizard_inst_n_6,
      Q => txbufstatus_reg(1),
      R => '0'
    );
\txchardispmode_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispmode_reg,
      Q => txchardispmode_double(0),
      R => reset_sync5(0)
    );
\txchardispmode_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => D(0),
      Q => txchardispmode_double(1),
      R => reset_sync5(0)
    );
\txchardispmode_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(0),
      Q => txchardispmode_int(0),
      R => '0'
    );
\txchardispmode_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispmode_double(1),
      Q => txchardispmode_int(1),
      R => '0'
    );
txchardispmode_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => txchardispmode_reg,
      R => reset_sync5(0)
    );
\txchardispval_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg,
      Q => txchardispval_double(0),
      R => reset_sync5(0)
    );
\txchardispval_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_double(1),
      R => reset_sync5(0)
    );
\txchardispval_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(0),
      Q => txchardispval_int(0),
      R => '0'
    );
\txchardispval_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txchardispval_double(1),
      Q => txchardispval_int(1),
      R => '0'
    );
txchardispval_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txchardispval_reg_reg_0(0),
      Q => txchardispval_reg,
      R => reset_sync5(0)
    );
\txcharisk_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg,
      Q => txcharisk_double(0),
      R => reset_sync5(0)
    );
\txcharisk_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_double(1),
      R => reset_sync5(0)
    );
\txcharisk_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(0),
      Q => txcharisk_int(0),
      R => '0'
    );
\txcharisk_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txcharisk_double(1),
      Q => txcharisk_int(1),
      R => '0'
    );
txcharisk_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => txcharisk_reg_reg_0(0),
      Q => txcharisk_reg,
      R => reset_sync5(0)
    );
\txdata_double_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(0),
      Q => txdata_double(0),
      R => reset_sync5(0)
    );
\txdata_double_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_double(10),
      R => reset_sync5(0)
    );
\txdata_double_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_double(11),
      R => reset_sync5(0)
    );
\txdata_double_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_double(12),
      R => reset_sync5(0)
    );
\txdata_double_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_double(13),
      R => reset_sync5(0)
    );
\txdata_double_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_double(14),
      R => reset_sync5(0)
    );
\txdata_double_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_double(15),
      R => reset_sync5(0)
    );
\txdata_double_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(1),
      Q => txdata_double(1),
      R => reset_sync5(0)
    );
\txdata_double_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(2),
      Q => txdata_double(2),
      R => reset_sync5(0)
    );
\txdata_double_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(3),
      Q => txdata_double(3),
      R => reset_sync5(0)
    );
\txdata_double_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(4),
      Q => txdata_double(4),
      R => reset_sync5(0)
    );
\txdata_double_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(5),
      Q => txdata_double(5),
      R => reset_sync5(0)
    );
\txdata_double_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(6),
      Q => txdata_double(6),
      R => reset_sync5(0)
    );
\txdata_double_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => txdata_reg(7),
      Q => txdata_double(7),
      R => reset_sync5(0)
    );
\txdata_double_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_double(8),
      R => reset_sync5(0)
    );
\txdata_double_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => toggle_i_1_n_0,
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_double(9),
      R => reset_sync5(0)
    );
\txdata_int_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(0),
      Q => txdata_int(0),
      R => '0'
    );
\txdata_int_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(10),
      Q => txdata_int(10),
      R => '0'
    );
\txdata_int_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(11),
      Q => txdata_int(11),
      R => '0'
    );
\txdata_int_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(12),
      Q => txdata_int(12),
      R => '0'
    );
\txdata_int_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(13),
      Q => txdata_int(13),
      R => '0'
    );
\txdata_int_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(14),
      Q => txdata_int(14),
      R => '0'
    );
\txdata_int_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(15),
      Q => txdata_int(15),
      R => '0'
    );
\txdata_int_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(1),
      Q => txdata_int(1),
      R => '0'
    );
\txdata_int_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(2),
      Q => txdata_int(2),
      R => '0'
    );
\txdata_int_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(3),
      Q => txdata_int(3),
      R => '0'
    );
\txdata_int_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(4),
      Q => txdata_int(4),
      R => '0'
    );
\txdata_int_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(5),
      Q => txdata_int(5),
      R => '0'
    );
\txdata_int_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(6),
      Q => txdata_int(6),
      R => '0'
    );
\txdata_int_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(7),
      Q => txdata_int(7),
      R => '0'
    );
\txdata_int_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(8),
      Q => txdata_int(8),
      R => '0'
    );
\txdata_int_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => gtxe2_i,
      CE => '1',
      D => txdata_double(9),
      Q => txdata_int(9),
      R => '0'
    );
\txdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(0),
      Q => txdata_reg(0),
      R => reset_sync5(0)
    );
\txdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(1),
      Q => txdata_reg(1),
      R => reset_sync5(0)
    );
\txdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(2),
      Q => txdata_reg(2),
      R => reset_sync5(0)
    );
\txdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(3),
      Q => txdata_reg(3),
      R => reset_sync5(0)
    );
\txdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(4),
      Q => txdata_reg(4),
      R => reset_sync5(0)
    );
\txdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(5),
      Q => txdata_reg(5),
      R => reset_sync5(0)
    );
\txdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(6),
      Q => txdata_reg(6),
      R => reset_sync5(0)
    );
\txdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => CLK,
      CE => '1',
      D => \txdata_reg_reg[7]_0\(7),
      Q => txdata_reg(7),
      R => reset_sync5(0)
    );
txpowerdown_double_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \txpowerdown_reg__0\,
      Q => txpowerdown_double,
      R => reset_sync5(0)
    );
txpowerdown_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => gtxe2_i,
      CE => '1',
      D => txpowerdown_double,
      Q => txpowerdown,
      R => '0'
    );
txpowerdown_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => powerdown,
      Q => \txpowerdown_reg__0\,
      R => reset_sync5(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_block is
  port (
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    status_vector : out STD_LOGIC_VECTOR ( 6 downto 0 );
    resetdone : out STD_LOGIC;
    txn : out STD_LOGIC;
    txp : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    mmcm_reset : out STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    signal_detect : in STD_LOGIC;
    CLK : in STD_LOGIC;
    data_in : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 2 downto 0 );
    independent_clock_bufg : in STD_LOGIC;
    gtrefclk_bufg : in STD_LOGIC;
    gtrefclk_out : in STD_LOGIC;
    rxn : in STD_LOGIC;
    rxp : in STD_LOGIC;
    gt0_qplloutclk_out : in STD_LOGIC;
    gt0_qplloutrefclk_out : in STD_LOGIC;
    gtxe2_i : in STD_LOGIC
  );
end gig_ethernet_pcs_pma_0_block;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_block is
  signal enablealign : STD_LOGIC;
  signal mgt_rx_reset : STD_LOGIC;
  signal mgt_tx_reset : STD_LOGIC;
  signal powerdown : STD_LOGIC;
  signal \^resetdone\ : STD_LOGIC;
  signal rx_reset_done_i : STD_LOGIC;
  signal rxbuferr : STD_LOGIC;
  signal rxchariscomma : STD_LOGIC;
  signal rxcharisk : STD_LOGIC;
  signal rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rxdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rxdisperr : STD_LOGIC;
  signal rxnotintable : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal transceiver_inst_n_11 : STD_LOGIC;
  signal transceiver_inst_n_12 : STD_LOGIC;
  signal txbuferr : STD_LOGIC;
  signal txchardispmode : STD_LOGIC;
  signal txchardispval : STD_LOGIC;
  signal txcharisk : STD_LOGIC;
  signal txdata : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  signal NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute B_SHIFTER_ADDR : string;
  attribute B_SHIFTER_ADDR of gig_ethernet_pcs_pma_0_core : label is "10'b0101001110";
  attribute C_1588 : integer;
  attribute C_1588 of gig_ethernet_pcs_pma_0_core : label is 0;
  attribute C_2_5G : string;
  attribute C_2_5G of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_COMPONENT_NAME : string;
  attribute C_COMPONENT_NAME of gig_ethernet_pcs_pma_0_core : label is "gig_ethernet_pcs_pma_0";
  attribute C_DYNAMIC_SWITCHING : string;
  attribute C_DYNAMIC_SWITCHING of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_ELABORATION_TRANSIENT_DIR : string;
  attribute C_ELABORATION_TRANSIENT_DIR of gig_ethernet_pcs_pma_0_core : label is "BlankString";
  attribute C_FAMILY : string;
  attribute C_FAMILY of gig_ethernet_pcs_pma_0_core : label is "kintex7";
  attribute C_HAS_AN : string;
  attribute C_HAS_AN of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_AXIL : string;
  attribute C_HAS_AXIL of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_MDIO : string;
  attribute C_HAS_MDIO of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_HAS_TEMAC : string;
  attribute C_HAS_TEMAC of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_IS_SGMII : string;
  attribute C_IS_SGMII of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_RX_GMII_CLK : string;
  attribute C_RX_GMII_CLK of gig_ethernet_pcs_pma_0_core : label is "TXOUTCLK";
  attribute C_SGMII_FABRIC_BUFFER : string;
  attribute C_SGMII_FABRIC_BUFFER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute C_SGMII_PHY_MODE : string;
  attribute C_SGMII_PHY_MODE of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_LVDS : string;
  attribute C_USE_LVDS of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TBI : string;
  attribute C_USE_TBI of gig_ethernet_pcs_pma_0_core : label is "FALSE";
  attribute C_USE_TRANSCEIVER : string;
  attribute C_USE_TRANSCEIVER of gig_ethernet_pcs_pma_0_core : label is "TRUE";
  attribute GT_RX_BYTE_WIDTH : integer;
  attribute GT_RX_BYTE_WIDTH of gig_ethernet_pcs_pma_0_core : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of gig_ethernet_pcs_pma_0_core : label is "soft";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_core : label is "yes";
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of gig_ethernet_pcs_pma_0_core : label is "true";
begin
  resetdone <= \^resetdone\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
gig_ethernet_pcs_pma_0_core: entity work.gig_ethernet_pcs_pma_0_gig_ethernet_pcs_pma_v16_2_18
     port map (
      an_adv_config_val => '0',
      an_adv_config_vector(15 downto 0) => B"0000000000000000",
      an_enable => NLW_gig_ethernet_pcs_pma_0_core_an_enable_UNCONNECTED,
      an_interrupt => NLW_gig_ethernet_pcs_pma_0_core_an_interrupt_UNCONNECTED,
      an_restart_config => '0',
      basex_or_sgmii => '0',
      configuration_valid => '0',
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(2 downto 0),
      configuration_vector(0) => '0',
      correction_timer(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      dcm_locked => data_in,
      drp_daddr(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_daddr_UNCONNECTED(9 downto 0),
      drp_dclk => '0',
      drp_den => NLW_gig_ethernet_pcs_pma_0_core_drp_den_UNCONNECTED,
      drp_di(15 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_drp_di_UNCONNECTED(15 downto 0),
      drp_do(15 downto 0) => B"0000000000000000",
      drp_drdy => '0',
      drp_dwe => NLW_gig_ethernet_pcs_pma_0_core_drp_dwe_UNCONNECTED,
      drp_gnt => '0',
      drp_req => NLW_gig_ethernet_pcs_pma_0_core_drp_req_UNCONNECTED,
      en_cdet => NLW_gig_ethernet_pcs_pma_0_core_en_cdet_UNCONNECTED,
      enablealign => enablealign,
      ewrap => NLW_gig_ethernet_pcs_pma_0_core_ewrap_UNCONNECTED,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gtx_clk => '0',
      link_timer_basex(9 downto 0) => B"0000000000",
      link_timer_sgmii(9 downto 0) => B"0000000000",
      link_timer_value(9 downto 0) => B"0000000000",
      loc_ref => NLW_gig_ethernet_pcs_pma_0_core_loc_ref_UNCONNECTED,
      mdc => '0',
      mdio_in => '0',
      mdio_out => NLW_gig_ethernet_pcs_pma_0_core_mdio_out_UNCONNECTED,
      mdio_tri => NLW_gig_ethernet_pcs_pma_0_core_mdio_tri_UNCONNECTED,
      mgt_rx_reset => mgt_rx_reset,
      mgt_tx_reset => mgt_tx_reset,
      phyad(4 downto 0) => B"00000",
      pma_rx_clk0 => '0',
      pma_rx_clk1 => '0',
      powerdown => powerdown,
      reset => \out\(0),
      reset_done => \^resetdone\,
      rx_code_group0(9 downto 0) => B"0000000000",
      rx_code_group1(9 downto 0) => B"0000000000",
      rx_gt_nominal_latency(15 downto 0) => B"0000000011111000",
      rxbufstatus(1) => rxbuferr,
      rxbufstatus(0) => '0',
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      rxclkcorcnt(2) => '0',
      rxclkcorcnt(1 downto 0) => rxclkcorcnt(1 downto 0),
      rxdata(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxnotintable(0) => rxnotintable,
      rxphy_correction_timer(63 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_correction_timer_UNCONNECTED(63 downto 0),
      rxphy_ns_field(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_ns_field_UNCONNECTED(31 downto 0),
      rxphy_s_field(47 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_rxphy_s_field_UNCONNECTED(47 downto 0),
      rxrecclk => '0',
      rxrundisp(0) => '0',
      s_axi_aclk => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_arready_UNCONNECTED,
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_awready_UNCONNECTED,
      s_axi_awvalid => '0',
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(31 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rdata_UNCONNECTED(31 downto 0),
      s_axi_resetn => '0',
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_gig_ethernet_pcs_pma_0_core_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_wready => NLW_gig_ethernet_pcs_pma_0_core_s_axi_wready_UNCONNECTED,
      s_axi_wvalid => '0',
      signal_detect => signal_detect,
      speed_is_100 => '0',
      speed_is_10_100 => '0',
      speed_selection(1 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_speed_selection_UNCONNECTED(1 downto 0),
      status_vector(15 downto 7) => NLW_gig_ethernet_pcs_pma_0_core_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      systemtimer_ns_field(31 downto 0) => B"00000000000000000000000000000000",
      systemtimer_s_field(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      tx_code_group(9 downto 0) => NLW_gig_ethernet_pcs_pma_0_core_tx_code_group_UNCONNECTED(9 downto 0),
      txbuferr => txbuferr,
      txchardispmode => txchardispmode,
      txchardispval => txchardispval,
      txcharisk => txcharisk,
      txdata(7 downto 0) => txdata(7 downto 0),
      userclk => '0',
      userclk2 => CLK
    );
sync_block_rx_reset_done: entity work.gig_ethernet_pcs_pma_0_sync_block
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_12,
      data_out => rx_reset_done_i
    );
sync_block_tx_reset_done: entity work.gig_ethernet_pcs_pma_0_sync_block_0
     port map (
      CLK => CLK,
      data_in => transceiver_inst_n_11,
      resetdone => \^resetdone\,
      resetdone_0 => rx_reset_done_i
    );
transceiver_inst: entity work.gig_ethernet_pcs_pma_0_transceiver
     port map (
      CLK => CLK,
      D(0) => txchardispmode,
      Q(1 downto 0) => rxclkcorcnt(1 downto 0),
      SR(0) => mgt_rx_reset,
      data_in => transceiver_inst_n_11,
      data_sync_reg1 => data_in,
      enablealign => enablealign,
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg => gtrefclk_bufg,
      gtrefclk_out => gtrefclk_out,
      gtxe2_i => gtxe2_i,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \out\(0),
      powerdown => powerdown,
      reset_sync5(0) => mgt_tx_reset,
      rx_fsm_reset_done_int_reg => transceiver_inst_n_12,
      rxbufstatus(0) => rxbuferr,
      rxchariscomma(0) => rxchariscomma,
      rxcharisk(0) => rxcharisk,
      \rxdata_reg[7]_0\(7 downto 0) => rxdata(7 downto 0),
      rxdisperr(0) => rxdisperr,
      rxn => rxn,
      rxnotintable(0) => rxnotintable,
      rxoutclk => rxoutclk,
      rxp => rxp,
      status_vector(0) => \^status_vector\(1),
      txbuferr => txbuferr,
      txchardispval_reg_reg_0(0) => txchardispval,
      txcharisk_reg_reg_0(0) => txcharisk,
      \txdata_reg_reg[7]_0\(7 downto 0) => txdata(7 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0_support is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    resetdone : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of gig_ethernet_pcs_pma_0_support : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0_support : entity is "yes";
end gig_ethernet_pcs_pma_0_support;

architecture STRUCTURE of gig_ethernet_pcs_pma_0_support is
  signal \<const0>\ : STD_LOGIC;
  signal \^gt0_qplloutclk_out\ : STD_LOGIC;
  signal \^gt0_qplloutrefclk_out\ : STD_LOGIC;
  signal \^gtrefclk_bufg_out\ : STD_LOGIC;
  signal \^gtrefclk_out\ : STD_LOGIC;
  signal \^mmcm_locked_out\ : STD_LOGIC;
  signal mmcm_reset : STD_LOGIC;
  signal \^pma_reset_out\ : STD_LOGIC;
  signal rxoutclk : STD_LOGIC;
  signal \^rxuserclk2_out\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal txoutclk : STD_LOGIC;
  signal \^userclk2_out\ : STD_LOGIC;
  signal \^userclk_out\ : STD_LOGIC;
begin
  gt0_qplloutclk_out <= \^gt0_qplloutclk_out\;
  gt0_qplloutrefclk_out <= \^gt0_qplloutrefclk_out\;
  gtrefclk_bufg_out <= \^gtrefclk_bufg_out\;
  gtrefclk_out <= \^gtrefclk_out\;
  mmcm_locked_out <= \^mmcm_locked_out\;
  pma_reset_out <= \^pma_reset_out\;
  rxuserclk2_out <= \^rxuserclk2_out\;
  rxuserclk_out <= \^rxuserclk2_out\;
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
  userclk2_out <= \^userclk2_out\;
  userclk_out <= \^userclk_out\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
core_clocking_i: entity work.gig_ethernet_pcs_pma_0_clocking
     port map (
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => \^gtrefclk_out\,
      gtrefclk_p => gtrefclk_p,
      mmcm_locked => \^mmcm_locked_out\,
      mmcm_reset => mmcm_reset,
      rxoutclk => rxoutclk,
      rxuserclk2_out => \^rxuserclk2_out\,
      txoutclk => txoutclk,
      userclk => \^userclk_out\,
      userclk2 => \^userclk2_out\
    );
core_gt_common_i: entity work.gig_ethernet_pcs_pma_0_gt_common
     port map (
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_out => \^gtrefclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\
    );
core_resets_i: entity work.gig_ethernet_pcs_pma_0_resets
     port map (
      independent_clock_bufg => independent_clock_bufg,
      \out\(0) => \^pma_reset_out\,
      reset => reset
    );
pcs_pma_block_i: entity work.gig_ethernet_pcs_pma_0_block
     port map (
      CLK => \^userclk2_out\,
      configuration_vector(2 downto 0) => configuration_vector(3 downto 1),
      data_in => \^mmcm_locked_out\,
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => \^gt0_qplloutclk_out\,
      gt0_qplloutrefclk_out => \^gt0_qplloutrefclk_out\,
      gtrefclk_bufg => \^gtrefclk_bufg_out\,
      gtrefclk_out => \^gtrefclk_out\,
      gtxe2_i => \^userclk_out\,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_reset => mmcm_reset,
      \out\(0) => \^pma_reset_out\,
      resetdone => resetdone,
      rxn => rxn,
      rxoutclk => rxoutclk,
      rxp => rxp,
      signal_detect => signal_detect,
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txoutclk => txoutclk,
      txp => txp
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gig_ethernet_pcs_pma_0 is
  port (
    gtrefclk_p : in STD_LOGIC;
    gtrefclk_n : in STD_LOGIC;
    gtrefclk_out : out STD_LOGIC;
    gtrefclk_bufg_out : out STD_LOGIC;
    txp : out STD_LOGIC;
    txn : out STD_LOGIC;
    rxp : in STD_LOGIC;
    rxn : in STD_LOGIC;
    resetdone : out STD_LOGIC;
    userclk_out : out STD_LOGIC;
    userclk2_out : out STD_LOGIC;
    rxuserclk_out : out STD_LOGIC;
    rxuserclk2_out : out STD_LOGIC;
    pma_reset_out : out STD_LOGIC;
    mmcm_locked_out : out STD_LOGIC;
    independent_clock_bufg : in STD_LOGIC;
    gmii_txd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_tx_en : in STD_LOGIC;
    gmii_tx_er : in STD_LOGIC;
    gmii_rxd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gmii_rx_dv : out STD_LOGIC;
    gmii_rx_er : out STD_LOGIC;
    gmii_isolate : out STD_LOGIC;
    configuration_vector : in STD_LOGIC_VECTOR ( 4 downto 0 );
    status_vector : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    signal_detect : in STD_LOGIC;
    gt0_qplloutclk_out : out STD_LOGIC;
    gt0_qplloutrefclk_out : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of gig_ethernet_pcs_pma_0 : entity is true;
  attribute EXAMPLE_SIMULATION : integer;
  attribute EXAMPLE_SIMULATION of gig_ethernet_pcs_pma_0 : entity is 0;
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of gig_ethernet_pcs_pma_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of gig_ethernet_pcs_pma_0 : entity is "gig_ethernet_pcs_pma_v16_2_18,Vivado 2024.1";
end gig_ethernet_pcs_pma_0;

architecture STRUCTURE of gig_ethernet_pcs_pma_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^status_vector\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal NLW_U0_status_vector_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 7 );
  attribute EXAMPLE_SIMULATION of U0 : label is 0;
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
  status_vector(15) <= \<const0>\;
  status_vector(14) <= \<const0>\;
  status_vector(13) <= \<const0>\;
  status_vector(12) <= \<const0>\;
  status_vector(11) <= \<const0>\;
  status_vector(10) <= \<const0>\;
  status_vector(9) <= \<const0>\;
  status_vector(8) <= \<const0>\;
  status_vector(7) <= \<const0>\;
  status_vector(6 downto 0) <= \^status_vector\(6 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.gig_ethernet_pcs_pma_0_support
     port map (
      configuration_vector(4) => '0',
      configuration_vector(3 downto 1) => configuration_vector(3 downto 1),
      configuration_vector(0) => '0',
      gmii_isolate => gmii_isolate,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,
      gmii_rxd(7 downto 0) => gmii_rxd(7 downto 0),
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_txd(7 downto 0) => gmii_txd(7 downto 0),
      gt0_qplloutclk_out => gt0_qplloutclk_out,
      gt0_qplloutrefclk_out => gt0_qplloutrefclk_out,
      gtrefclk_bufg_out => gtrefclk_bufg_out,
      gtrefclk_n => gtrefclk_n,
      gtrefclk_out => gtrefclk_out,
      gtrefclk_p => gtrefclk_p,
      independent_clock_bufg => independent_clock_bufg,
      mmcm_locked_out => mmcm_locked_out,
      pma_reset_out => pma_reset_out,
      reset => reset,
      resetdone => resetdone,
      rxn => rxn,
      rxp => rxp,
      rxuserclk2_out => rxuserclk2_out,
      rxuserclk_out => rxuserclk_out,
      signal_detect => signal_detect,
      status_vector(15 downto 7) => NLW_U0_status_vector_UNCONNECTED(15 downto 7),
      status_vector(6 downto 0) => \^status_vector\(6 downto 0),
      txn => txn,
      txp => txp,
      userclk2_out => userclk2_out,
      userclk_out => userclk_out
    );
end STRUCTURE;
