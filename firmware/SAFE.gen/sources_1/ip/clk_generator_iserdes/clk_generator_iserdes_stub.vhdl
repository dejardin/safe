-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- Copyright 2022-2024 Advanced Micro Devices, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2024.1 (lin64) Build 5076996 Wed May 22 18:36:09 MDT 2024
-- Date        : Fri Jul 12 09:11:02 2024
-- Host        : spppcj166.extra.cea.fr running 64-bit Fedora release 38 (Thirty Eight)
-- Command     : write_vhdl -force -mode synth_stub
--               /data/cms/ecal/fe/SAFE/firmware/SAFE.gen/sources_1/ip/clk_generator_iserdes/clk_generator_iserdes_stub.vhdl
-- Design      : clk_generator_iserdes
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k70tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_generator_iserdes is
  Port ( 
    clk_20 : out STD_LOGIC;
    clk_40 : out STD_LOGIC;
    clk_160 : out STD_LOGIC;
    clk_640 : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC;
    clk_160_in : in STD_LOGIC
  );

end clk_generator_iserdes;

architecture stub of clk_generator_iserdes is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_20,clk_40,clk_160,clk_640,reset,locked,clk_160_in";
begin
end;
